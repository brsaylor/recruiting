UAA Experimental Econ Lab's version of Caltech's MooreRecruiting subject
database.

Caltech's original version is here:
http://moorerecruiting.ssel.caltech.edu:8000/mr/

Our version is based on MooreRecruiting 1.0. As of this writing, the latest
upstream version is 1.1.1, released on 3/14/2011. No upstream changes since 1.0
have been integrated into our version.

================================================================================

AppFuse Basic Spring MVC Archetype
--------------------------------------------------------------------------------
If you're reading this then you've created your new project using Maven and
appfuse-basic-spring.  You have only created the shell of an AppFuse Java EE
application.  The project object model (pom) is defined in the file pom.xml.
The application is ready to run as a web application. The pom.xml file is
pre-defined with Hibernate as a persistence model and Spring MVC as the web
framework.

To get started, please complete the following steps:

1. Download and install a MySQL 5.x database from 
   http://dev.mysql.com/downloads/mysql/5.0.html#downloads.

2. Run "mvn jetty:run-war" and view the application at http://localhost:8080.

3. More information can be found at:

   http://appfuse.org/display/APF/QuickStart+Guide
4. Generate schema: mvn test-compile hibernate3:hbm2ddl 

**** Recruiting System Development Notes (add to top) ****

10/23/2007 - poneil
Integration with Payment Gateway and Recruiting system has been done, as well
as a rework of the experiment creation flow.  Tests are broken for now.
run "mvn -Dmaven.test.skip=true jetty:run-war" to build and run the app.
paygatesoapclient-1.0.1.jar is now needed to build. the jar can be found in the
third_party directory at the root of the project.


