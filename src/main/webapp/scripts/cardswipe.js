// BRS Nov 2010
//
// Process input from a card reader (MagTek Mini Swipe Magnetic Strip Reader
// with keyboard emulation) to allow students to sign in for an experiment by
// swiping their ID cards.
//
// Assumes the existence of:
//  - <form id="cardSwipeForm" action="participantrecordform.html">
//  - <input type="hidden" name="id"/>
//  - <input type="hidden" name="status_name"/>
//  - <input type="hidden" name="subject_id"/>
//  - <input id="cardSwipeInput">
//  - <span id="cardSwipeMessage">
//  - An array called participantRecords, each element of which is an object
//    with attributes "id" (the id of the ParticipantRecord) and "schoolId" (the
//    schoolId attribute of the Subject associated with that ParticipantRecord)
//    and "subject_id"
//
// My UAA ID card produces the following string:
// ;303100303538631?
// and my ID number is 30353863
// UPDATE 2012-10-26: New WolfCards are different; see onCardSwiped()
//
// This uses JQuery (since I'm already familiar with it), while everything else
// uses Prototype.  To avoid conflict, using $J instead of $ (see
// http://docs.jquery.com/Using_jQuery_with_Other_Libraries)

var $J = jQuery.noConflict();

$J(document).ready(function() {
    $J("#cardSwipeInput").focus();
    setInterval(focusCardSwipeInput, 1000); // keep card swipe input focused
    $J("#cardSwipeMessage").text("Ready");
    $J("#cardSwipeForm").submit(onCardSwiped);
});

function focusCardSwipeInput() {
    $J("#cardSwipeInput").focus();
}

function onCardSwiped() {

    // Parse the card data
    //
    // Old WolfCard
    //var schoolIdStr = $J("#cardSwipeInput").val().substring(7, 15);
    //
    // New WolfCard
    var schoolIdStr = $J("#cardSwipeInput").val().substring(6, 14);
    //
    // Replace text box value with the parsed version
    $J("#cardSwipeInput").val(schoolIdStr);
    var schoolId = parseInt(schoolIdStr);

    // Validate the ID
    if (schoolIdStr.length != 8 || isNaN(schoolId)) {
        $J("#cardSwipeMessage").text("Invalid ID - try again");
        // Select all, so it's replaced with next scan
        $J("#cardSwipeInput").select();
        return false;
    }

    // Find the corresponding participant record
    var prec = null;
    for (var i = 0; i < participantRecords.length; i++) {
        if (participantRecords[i].schoolId == schoolId) {
            prec = participantRecords[i];
            break;
        }
    }
    if (prec == null) {
        $J("#cardSwipeMessage").text("No participant with this ID. ");
        var link = $J("<a href='participantrecordform.html?schoolId="
                + schoolId + "'>Click here to add.</a>");
        $J("#cardSwipeMessage").append(link);
        // Select all, so it's replaced with next scan
        $J("#cardSwipeInput").select();
        return false;
    }

    // OK, we have a participating ID.
    
    // Set the necessary form fields
    $J("[name=id]").val(prec.id);
    $J("[name=subject_id]").val(prec.subject_id);
    $J("[name=status_name]").val("STATUS_1"); // "show-up" status

    // submit the form
    return true;
}
