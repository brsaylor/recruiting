// BRS Nov, Dec 2011

var slideTime = 300;
var showingFilterOptionsButtons = false;

jQuery.noConflict();
jQuery(document).ready(function($) {

    $('#checkedIRB').change(function() {
        $('#checkedIRB_bottom').get(0).checked = this.checked;
    });
    $('#checkedIRB_bottom').change(function() {
        $('#checkedIRB').get(0).checked = this.checked;
    });

    // Display the count of eligible subjects asynchronously, since it can
    // take a bit of time to count them.
    var conditionIds = [];
    for (var i = 0; i < conditions.length; i++) {
        conditionIds.push(conditions[i].id);
    }
    conditionManagerDWR.getEligibleUserStatisticsByFilterConditions(
        conditionIds, function(str) {
        $('#eligibleCount').html(str);
    });

    // The user selected a different filter option.
    $('input:radio[name=useFilter]').change(function() {
        var useFilter = $('input:radio[name=useFilter]:checked').val();
        
        // If the original filter option was re-selected, treat it the same as
        // if the user had clicked Cancel.
        if (useFilter == useFilter_orig) {
            onCancelClicked();
            return true;
        }

        // Delete all filter options buttons except the cancel button.
        $('#filterOptionsButtons input').not('#optCancel').remove();

        // Create buttons for the available actions, depending on the original
        // filter option and new filter option.  The Cancel button is already
        // in the HTML.
        var buttonDiv = $('#filterOptionsButtons');
        if (useFilter == 'none') {
            if (useFilter_orig == 'default') {
                buttonDiv.prepend(
                    "<input type='submit' name='apply' value='Apply'/>");
            } else { // useFilter_orig == 'separate'
                buttonDiv.prepend("<input type='submit' name='discard' " +
                        "value='Discard current filter'/>");
            }
        } else if (useFilter == 'separate') {
            buttonDiv.prepend("<input type='submit' name='createNew' " +
                    "value='Create new filter'/>");
            buttonDiv.prepend("<input type='submit' name='copyDefault' " +
                    "value='Copy default filter'/>");
        } else { // useFilter == 'default'
            if (useFilter_orig == 'none') {
                buttonDiv.prepend(
                    "<input type='submit' name='apply' value='Apply'/>");
            } else { // useFilter_orig == 'separate'
                buttonDiv.prepend("<input type='submit' name='discard' " +
                        "value='Discard current filter'/>");
                buttonDiv.prepend("<input type='submit' name='saveAsDefault' " +
                        "value='Save current filter as default'/>");
            }
        }

        // If not already showing the filter options buttons, show them, disable
        // editing of the filter conditions, and disable the bottom buttons.
        if (!showingFilterOptionsButtons) {
            $('#filterOptionsButtons').slideDown(slideTime);

            disableConditionList();

            // Disable bottom buttons
            $('#buttons input').attr('disabled', 'disabled');

            showingFilterOptionsButtons = true;
        }
    });

    // Common handler for when a filter options button is clicked.
    // The cancel button is handled separately, and so doesn't bubble up to
    // #filterOptionsButtons here.
    $('#filterOptionsButtons').on('click', 'input', function() {
        return true;
    });

    // The user cancelled the selection of a different filter option.
    $('#optCancel').click(onCancelClicked);

    $('#filterForm').submit(function() {
        if (!$('#doNotUseFilter').get(0).checked &&
            !$('#checkedIRB').get(0).checked && (
                $(this).attr('name') == 'back' ||
                $(this).attr('name') == 'next' ||
                $(this).attr('name') == 'savenow'
            )) {
            alert("The filter must conform to the IRB agreement.");
            return false;
        }
        return true;
    });

    function onCancelClicked() {
        // Restore the original filter option
        $('[name=useFilter][value=' + useFilter_orig + ']').attr(
            'checked', 'checked');

        // Hide the filter options buttons
        $('#filterOptionsButtons').slideUp(slideTime, function() {
            // Delete all filter options buttons except the cancel button.
            $('#filterOptionsButtons input').not('#optCancel').remove();
        });

        // Re-enable editing of filter conditions
        enableConditionList();

        // Re-enable the bottom buttons
        $('#buttons input').removeAttr('disabled');

        showingFilterOptionsButtons = false;

        return false;
    }

    /////////////////////////////////////////
    // Sub-forms for adding/editing conditions

    // Detach condition subforms from the DOM
    var conditionSubforms = $('#conditionSubforms').detach();

    // Initialize ExperimentTypeCondition subform
    conditionSubforms.find('#addExperimentType').change(function() {
        var typeId = $(this).val();

        // If the experiment type is already on the list, don't add it.
        if ($('#experimentTypeList').find('[value=' +typeId+ ']').length > 0) {
            $(this).val('');
            return false;
        }
        $(this).val('');

        addExperimentType(typeId);
    });

    // Initialize ExperimentCondition subform
    conditionSubforms.find('#experimentCondition_type').change(function() {
        // A different experiment type was selected
        var typeId = $(this).val();
        populateExperimentSelect(typeId);
    });
    
    // Initialize SubjectPoolCondition subform
    conditionSubforms.find('#addSubjectPool').change(function() {
        var poolId = $(this).val();

        // If the experiment pool is already on the list, don't add it.
        if ($('#subjectPoolList').find('[value=' +poolId+ ']').length > 0) {
            $(this).val('');
            return false;
        }
        $(this).val('');

        addSubjectPool(poolId);
    });

    // Initialize QuestionResultCondition subform
    conditionSubforms.find('[name=question]').change(function() {
        // Hide and clear any visible question options, then show the question
        // options for the selected question.
        $('#questionOptionsContainer div').hide();
        $('#questionOptionsContainer input').removeAttr('checked');
        $('#questionOptionsContainer div[questionId=' + $(this).val() + ']')
            .show();
    });

    // User selected a type of condition to add.  Show a blank condition subform
    // for the selected type.
    $('#addCondition').change(function() {
        // User selected a condition type to add from the <select>
        showConditionSubform($(this).val());
    });

    // User clicked "Edit" next to a condition.  Show a populated condition
    // subform (unless the link is disabled, in which case do nothing).
    $('.editLink').click(function() {
        if ($(this).hasClass('disabled')) {
            return false;
        }
        $(this).closest('li').addClass('editedCondition');
        var condition = conditions[$(this).attr('conditionIndex')];
        showConditionSubform(condition.type, condition);
        return false;
    });

    // User clicked "Delete" next to a condition.  Confirm the operation before
    // performing a form submission (unless the link is disabled, in which case
    // do nothing).
    $('.deleteLink').click(function() {
        if ($(this).hasClass('disabled')) {
            return false;
        }
        if (confirm("Are you sure you want to delete this condition?")) {
            $(this).after(
                "<input type='hidden' name='deleteCondition' value='1'/>" +
                "<input type='hidden' name='deleteConditionId' value='" +
                $(this).attr('conditionId') + "'/>");
            $('#filterForm').submit();
        }
        return false;
    });
    
    $('#cancelConditionEdit').click(function() {
        // User canceled adding/editing a condition
        $('#conditionFieldset').slideUp(function() {
            // Remove the (cloned) condition sub-form from the DOM
            $('#conditionFieldset > div').remove();
        });

        // Unset the conditionId field
        $('#conditionFieldset input[name=conditionId]').attr('value', '')

        $('#addCondition').val("");
        $('#filterOptions input').removeAttr('disabled');
        enableConditionList();
        $('#addConditionContainer').slideDown(slideTime);
        $('#buttons input').removeAttr('disabled');
        $('#filterConditions .editedCondition').removeClass('editedCondition');
    });

    // Validate condition when Save is clicked.
    $('#saveCondition').click(function() {
        var type = $('[name=conditionType]').attr('value');
        if (type == 'ExperimentTypeCondition') {
            if ($('#experimentTypeList').children().length == 0) {
                alert("Please select at least one experiment type.");
                return false;
            }
        } else if (type == 'ExperimentCondition') {
            if ($('#experimentCondition_experiment').val() == '') {
                alert("Please select an experiment.");
                return false;
            }
        } else if (type == 'PoolCondition') {
            if ($('#subjectPoolList').children().length == 0) {
                alert("Please select at least one subject pool.");
                return false;
            }
        } else if (type == 'QuestionResultCondition') {
            if ($('[name=question]').val() == '') {
                alert("Please select a question.");
                return false;
            }
            if ($('input[name=questionOptions]:checked').length == 0) {
                alert("Please check at least one question option.");
                return false;
            }
        }
        return true;
    });

    // Disable editing of filter conditions
    function disableConditionList() {
        $('#conditionList').addClass('disabled');
        $('#conditionList').find('input, select').attr('disabled', 'disabled');
        $('#conditionList').find('a').addClass('disabled');
        $('#addCondition').attr('disabled', 'disabled');
    }

    // Enable editing of filter conditions
    function enableConditionList() {
        $('#conditionList').removeClass('disabled');
        $('#conditionList').find('input, select').removeAttr('disabled');
        $('#conditionList').find('a').removeClass('disabled');
        $('#addCondition').removeAttr('disabled');
    }

    // Show the condition subform for the given condition type, populating it
    // with data from the given condition, if supplied.
    function showConditionSubform(type, /* optional */ condition) {
        var typeLabel = $('#addCondition').find('[value=' + type + ']').html();
        
        // Disable the filter options radio buttons
        $('#filterOptions input').attr('disabled', 'disabled');
        
        // Disable the condition list
        disableConditionList();

        // Hide the "Add condition" label and <select>
        $('#addConditionContainer').slideUp(slideTime);

        // Disable the bottom buttons
        $('#buttons input').attr('disabled', 'disabled')

        if (condition) {
            $('#conditionFieldset legend').html("Edit " + typeLabel);
        } else {
            $('#conditionFieldset legend').html("Add " + typeLabel);
        }

        $('#conditionFieldset input[name=conditionType]').attr(
                'value', type);
        $('#conditionFieldset input[name=conditionType]').removeAttr(
                'disabled');

        // Make a clone of the subform <div>, keeping event handlers, and put it
        // into the DOM, hidden
        var div = conditionSubforms.find('[conditionType=' + type + ']')
            .clone(true);
        $('#conditionFieldset').prepend(div);

        // Populate form if a condition argument was supplied
        if (condition) {
            $('#conditionFieldset input[name=conditionId]').attr(
                    'value', condition.id)
            $('#conditionFieldset input[name=conditionId]').removeAttr(
                    'disabled');
            if (type == 'ExperimentTypeCondition') {
                div.find('[name=onList]').val('' + condition.onList);
                div.find('[name=history]').val('' + condition.history);
                for (var i = 0; i < condition.types.length; i++) {
                    addExperimentType(condition.types[i]);
                }
            } else if (type == 'ExperimentCondition') {
                div.find('[name=onList]').val('' + condition.onList);
                $('#experimentCondition_type').val(condition.expType);
                populateExperimentSelect(condition.expType,
                        condition.experiment);
            } else if (type == 'NoShowCondition') {
                div.find('[name=maxNoShows]').val(condition.maxNoShows);
            } else if (type == 'PoolCondition') {
                div.find('[name=memberOf]').val('' + condition.memberOf);
                for (var i = 0; i < condition.pools.length; i++) {
                    addSubjectPool(condition.pools[i]);
                }
            } else if (type == 'QuestionResultCondition') {
                div.find('[name=filterType]').val(condition.filterType);
                div.find('[name=question]').val(condition.question);
                var questionOptionsDiv = $(
                        '#questionOptionsContainer [questionId=' +
                        condition.question + ']');
                questionOptionsDiv.show();
                for (var i = 0; i < condition.filterOptions.length; i++) {
                    questionOptionsDiv.find('[value=' +
                            condition.filterOptions[i] + ']').attr(
                                'checked', 'checked');
                }
            }
        }

        // Show the subform
        $('#conditionFieldset').slideDown(slideTime);
    }

    // for ExperimentTypeCondition subform: add an experiment type to the list.
    function addExperimentType(typeId) {
        var typeSpan = $("<span></span");

        // Insert a comma if the experiment type list is not empty
        if ($('#experimentTypeList').children().length > 0) {
            typeSpan.append(', ');
        }

        var typeName = $('#addExperimentType').find(
                '[value=' + typeId + ']').html();
        var a = $("<a href='#'>[x] </a>");
        a.click(function() {
            $(this).closest('span').remove();
            return false;
        });
        typeSpan.append(a);
        typeSpan.append("<input type='hidden' name='experimentTypeIds'"
                + "value='" + typeId + "'>" + typeName);
        $("#experimentTypeList").append(typeSpan);
    }

    // for ExperimentCondition subform: populate the experiment <select> with
    // experiments of the given type.
    // Optional parameter "expId" causes the <select> to be set to that value.
    function populateExperimentSelect(typeId, expId) {
        $('#experimentCondition_experiment').find('option').not(':first')
            .remove();
        $('#experimentCondition_loading').html("Getting experiment list...");
        experimentManagerDWR.getExperimentListByType(typeId,
                function(experiments) {
            $('#experimentCondition_loading').empty();
            sel = $('#experimentCondition_experiment');
            for (var i = 0; i < experiments.length; i++) {
                var e = experiments[i];
                sel.append("<option value='" + e.id + "'>"
                    + "#" + e.id + ": " + e.startDateTime + "</option>");
            }
            if (expId != undefined) {
                sel.val(expId);
            }
        });
    }

    // for SubjectPoolCondition subform: add a subject pool to the list.
    function addSubjectPool(poolId) {
        var poolSpan = $("<span></span");

        // Insert a comma if the experiment pool list is not empty
        if ($('#subjectPoolList').children().length > 0) {
            poolSpan.append(', ');
        }

        var poolName = $('#addSubjectPool').find(
                '[value=' + poolId + ']').html();
        var a = $("<a href='#'>[x] </a>");
        a.click(function() {
            $(this).closest('span').remove();
            return false;
        });
        poolSpan.append(a);
        poolSpan.append("<input type='hidden' name='subjectPoolIds'"
                + "value='" + poolId + "'>" + poolName);
        $("#subjectPoolList").append(poolSpan);
    }
});
