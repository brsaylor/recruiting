// BRS May 2010
// Javascript common to user profile pages (signup.jsp and userForm.jsp)
// This uses JQuery (since I'm already familiar with it), while everything else
// uses Prototype. 
// http://docs.jquery.com/Using_jQuery_with_Other_Libraries)

jQuery.noConflict();

jQuery(document).ready(function($) {

    // Subject pool checkboxes
    $("#uaaYesRadioButton").change(function() {
        if (this.checked) {
            $("#uaaNoRadioButton").removeAttr("checked");
            $("#uaaCheckboxes input").removeAttr("checked");
            $("#uaaCheckboxes").slideDown();
        }
    });
    $("#uaaNoRadioButton").change(function() {
        if (this.checked) {
            $("#uaaYesRadioButton").removeAttr("checked");
            $("#uaaCheckboxes input").removeAttr("checked");
            $("#uaaCheckboxes").slideUp();
        }
    });
    // Check the boxes corresponding to the user's current subject pools
    for (var i = 0; i < userPools.length; i++) {
        var poolId = userPools[i];
        $("[name=userPools][value="+poolId+"]").attr("checked", "checked");
    }
    // Check the "yes" box if user is in any of the UAA subject pools.
    // Otherwise, hide the UAA subject pools checkboxes.
    if ($("#uaaCheckboxes input:checked").length > 0) {
        $("#uaaYesRadioButton").attr("checked", "checked");
    } else {
        $("#uaaCheckboxes").hide();
    }
    // On form submission, remove the "unspecified" pool if any other pools are
    // selected.
    $("form").submit(function() {
        if ($("[name=userPools]:checked").length > 0) {
            $("#userPoolUnspecified").remove();
        }
    });

    $("#travelTableAddRowButton").click(addTravelRow);

    // Remove row when remove button is clicked
    $("#travelTable button").live("click", function() {
        $(this).closest("tr").remove();
    });

    // Enable the Signup button when the consentCheckbox is checked.
    $("#consentCheckbox").change(function() {
        if (this.checked) {
            $("#signupButton").removeAttr("disabled");
        } else {
            $("#signupButton").attr("disabled", "disabled");
        }
    });

    $('[name=usCitizen]').change(onUsCitizenChanged);
    onUsCitizenChanged();

    function addTravelRow() {
        var newRow = $("#hiddenPrototypeTravelRow").clone().removeAttr("id");

        $("#travelTable tbody").append(newRow);
    }

    function onUsCitizenChanged() {
        // BRS 2010-04-27
        // 2012-02-02 converted to JQuery
        // Hide/unhide form elements that are required only for non-US citizens.
        var form = $('form');
        var checkedRadioButton = form.find('[name=usCitizen]:checked');
        if (checkedRadioButton.length == 0 || checkedRadioButton.val() == 'true') {
            $('.nonUsCitizen').hide();
        } else {
            $('.nonUsCitizen').show();
        }
    }
});

