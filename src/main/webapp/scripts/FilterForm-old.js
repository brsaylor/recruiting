function formSubmit() {
    //alert("In formSubmit()");
    
    var form = $("filterForm");
    addHiddenChild(form, "fcnt", fIdList.length);
    for(j = 0; j < fIdList.length; j++) {
        addHiddenChild(form, "filter_id_"+j, fIdList[j]);
    }
    
    return true;
}

function formLoad() {
    //Populate condition select
    filterManagerDWR.getByIdString(FILTER_ID, init_filter);
}

function init_filter(filter) {
    var lst = filter.conditions;
    fIdList = new Array(lst.length);
    var select = $("conditionSelect");
    var opt;
    for(j = 0; j < fIdList.length; j++) {
        fIdList[j] = lst[j].id;
        opt = appendOption(select, lst[j].name);
        opt.value = j;
    }
}

function radioChange(num) {
    var remDiv = $("remDiv");
    var addNewDiv = $("addNewDiv");
    var addOldDiv = $("addOldDiv");
    var viewEligDiv = $("viewEligDiv");
    if(num == 1) {
        remDiv.style.display="block";
        addNewDiv.style.display="none";
        addOldDiv.style.display="none";
        viewEligDiv.style.display="none";
    } else if (num == 2) {
        remDiv.style.display="none";
        addNewDiv.style.display="none";
        addOldDiv.style.display="block";
        viewEligDiv.style.display="none";
    } else if (num == 3) {
        remDiv.style.display="none";
        addNewDiv.style.display="block";
        addOldDiv.style.display="none";
        viewEligDiv.style.display="none";
    } else if (num == 4) {
        remDiv.style.display="none";
        addNewDiv.style.display="none";
        addOldDiv.style.display="none";
        viewEligDiv.style.display="block";
        conditionManagerDWR.getEligibleUserEmailsByFilterConditions(fIdList, setEligible);
    }
    
    clearMessage();
}

function clearMessage() {
    var div = $("messageDiv");
    div.className="";
    clearNodeChildren(div);
}

function conditionSelectChange() {
    var sel = $("conditionSelect");
    var fs = $("existentFilterFS");
    
    //If --Select filter-- is selected
    if(sel.value == 'NEW_FILTER') {
        fs.style.display="none";
    } else {
        fs.style.display="block";
        conditionManagerDWR.getByIdString(fIdList[sel.value], function(data) {  $("removeFilterButton").onclick=function(){removeFilter(sel.value)}; displayInfo(data, $("filterInfoSpanText"));});
    }
    
    clearMessage();
}

function oldConditionSelectChange() {
    var sel = $("oldConditionSelect");
    var fs = $("oldExistentFilterFS");
    
    //If --Select filter-- is selected
    if(sel.value == 'NEW_FILTER') {
        fs.style.display="none";
    } else {
        fs.style.display="block";
        conditionManagerDWR.getByIdString(sel.value, function(data) { $("addOldButton").onclick=function(){addOldFilter(data)}; displayInfo(data, $("oldFilterInfoSpanText"));});
    }
    
    clearMessage();
}

function addOldFilter(cond) {
    //Reset the old filter fields
    var sel=$("oldConditionSelect");
    sel.selectedIndex = 0;
    oldConditionSelectChange();//Update the condition select
    
    addFilter(cond);//, "<fmt:message key='filterForm.addError'/>", "<fmt:message key='filterForm.addSuccess'/>");//Performs the adding to the actual filter
}

function addFilter(cond) { //, errMsg, succMsg) {
    errMsg = "<fmt:message key='filterForm.addError'/>";
    succMsg = "<fmt:message key='filterForm.addSuccess'/>";
    var sel = $("conditionSelect");
    var opt = appendOption(sel, cond.name);
    opt.value = fIdList.length;
    
    fIdList.length++;
    fIdList[fIdList.length-1] = cond.id;
    
    
    if(!validLists()) {
        setMessage(" " + errMsg, false);
    } else {
        setMessage(" " + succMsg + " " + fIdList.length + " <fmt:message key='filterForm.filters'/>", true);
    }
    /*var div = $("messageDiv");
    div.className="message";
    clearNodeChildren(div);
    var img = addChild(div, "img");
    img.className = "icon";
     {
        img.src = "/images/iconWarning.gif";
        img.alt = "Error";
        addTextChild(div, " " + errMsg);
    } else {
        img.src = "/images/iconInformation.gif";
        img.alt = "Information";
        addTextChild(div, " " + succMsg + " " + fIdList.length + " <fmt:message key='filterForm.filters'/>");
    }
    addChild(div, "br");*/
}

function validLists() {
    var sel = $("conditionSelect");
    if(fIdList.length != (sel.length-1)) {
        alert("The lists do not match in length");
        return false;
    }
    for(j = 1; j < sel.length; j++) {
        if(sel.options[j].value != (j-1)) {
            alert("The options list has an error at index " + (j-1));
            return false;
        }
    }
    return true;
}

function displayInfo(cond, txt) {
    clearNodeChildren(txt);
    var ul = addChild(txt, "ul");
    var li;
    var str;
    var PAST = '<fmt:message key="filterForm.past"/>';
    var FUTURE = '<fmt:message key="filterForm.future"/>';
    var ON = '<fmt:message key="filterForm.on"/>';
    var OFF = '<fmt:message key="filterForm.off"/>';
    if(cond.conditionType == "QuestionResultCondition") {
        li = addChild(ul, "li");
        addTextChild(li, "<fmt:message key='filterForm.question'/>: " + cond.question.question);
        li = addChild(ul, "li");
        str = "<fmt:message key='filterForm.filteredOptions'/>: ";
        for(j = 0; j < cond.filterOptions.length; j++) {
            str += cond.question.optionList[cond.filterOptions[j]];
            if(j != cond.filterOptions.length - 1) {
                str += ", "
            }
        }
        addTextChild(li, str);
        li = addChild(ul, "li");
        addTextChild(li, "<fmt:message key='filterForm.filterType'/>: " + cond.filterType.stringType);
    } else if(cond.conditionType == "ExperimentTypeCondition") {
        li = addChild(ul, "li");
        addTextChild(li, "<fmt:message key='filterForm.filterOn'/> " + (cond.history ? PAST : FUTURE) + " <fmt:message key='filterForm.experiments'/>");
        li = addChild(ul, "li");
        str = "<fmt:message key='filterForm.filteredTypes'/>: ";
        for(j = 0; j < cond.types.length; j++) {
            str += cond.types[j].name;
            if(j != cond.types.length-1) {
                str += ", ";
            }
        }
        addTextChild(li, str);
        li = addChild(ul, "li");
        addTextChild(li, "<fmt:message key='filterForm.shouldBe'/> " + (cond.onList ? ON : OFF) + " <fmt:message key='filterForm.list'/>");
    } else if(cond.conditionType == "PoolCondition") {
        li = addChild(ul, "li");
        str = (cond.memberOf ? "<fmt:message key='filterForm.In'/>" : "<fmt:message key='filterForm.NotIn'/>") + " ";
        var sz = cond.pools.length;
        for(j = 0; j < sz; j++) {
            str += cond.pools[j].name;
            if(j == sz - 2) {
                str += " <fmt:message key='filterForm.or'/> ";
            } else if(j != sz - 1) {
                str += ", ";
            }
        }
        addTextChild(li, str);
    } else if(cond.conditionType == "BooleanCondition") {
        li = addChild(ul, "li");
        var str = "first_id = " + cond.firstCondition;
        var oldSel = $("oldConditionSelect");
        for(j = 1; j < oldSel.length; j++) {
            if(oldSel.options[j].value == cond.firstCondition) {
                str = oldSel.options[j].text;
                break;
            }
        }
        addTextChild(li, str);
        li = addChild(ul, "li");
        addTextChild(li, (cond.and ? "<fmt:message key='filterForm.AND'/>" : "<fmt:message key='filterForm.OR'/>"));
        li = addChild(ul, "li");
        str = "second_id = " + cond.secondCondition;
        for(j = 1; j < oldSel.length; j++) {
            if(oldSel.options[j].value == cond.secondCondition) {
                str = oldSel.options[j].text;
                break;
            }
        }
        addTextChild(li, str);
    }
}

function removeFilter(index) {
    var sel = $("conditionSelect");
    
    var ind = index*1;//Cast to a number
    //Need to add 1 to indices because we have the first option "--Choose--"
    sel.remove(ind + 1);
    for(j = ind + 1; j < sel.length; j++) {
        sel.options[j].value--;
        fIdList[j-1] = fIdList[j];
    }
    fIdList.length--;
    
    sel.selectedIndex = 0;
    conditionSelectChange();//Update the condition select
    
    if(!validLists()) {
        setMessage(" <fmt:message key='filterForm.removeError'/>", false);
        alert("j = " + j + ", sel.length = " + sel.length + ", fIdList.length = " + fIdList.length);
    } else {
        setMessage(" <fmt:message key='filterForm.removeSuccess'/> " + fIdList.length + " <fmt:message key='filterForm.filters'/>", true);
    }
    
    /*var div = $("messageDiv");
    div.className="message";
    clearNodeChildren(div);
    var img = addChild(div, "img");
    img.className = "icon";
    if(!validLists()) {
        img.src = "/images/iconWarning.gif";
        img.alt = "Error";
        addTextChild(div, " <fmt:message key='filterForm.removeError'/>");
        alert("j = " + j + ", sel.length = " + sel.length + ", fIdList.length = " + fIdList.length);
    } else {
        img.src = "/images/iconInformation.gif";
        img.alt = "Information";
        addTextChild(div, " <fmt:message key='filterForm.removeSuccess'/> " + fIdList.length + " <fmt:message key='filterForm.filters'/>");
    }
    addChild(div, "br");*/
}

function changeNewFilterType() {
    var lst = $("QRC", "ETC", "SPC", "BC", "newCondFS", "nameDiv");
    for(j = 0; j < lst.length; j++) {
        lst[j].style.display="none";
    }
    var sel = $("newFilterTypeSelect");
    if(sel.value != "NEW") {
        $(sel.value).style.display = "block";
        $("newCondFS").style.display = "block";
        $("nameDiv").style.display = "block";
        initFilterType(sel.value);
    }
    clearMessage();
}

function initFilterType(type) {
    var addButton = $("addExpButton");
    var saveButton = $("saveLaterButton");
    if(type == "QRC") {
        addButton.onclick = function() {saveQRC(true);};
        saveButton.onclick = function() {saveQRC(false);};
        addButton.disabled=true;
        saveButton.disabled=true;
    } else if(type == "ETC") {
        addButton.onclick = function() {saveETC(true);};
        saveButton.onclick = function() {saveETC(false);};
        addButton.disabled=false;
        saveButton.disabled=false;
        init_etc();
    } else if(type == "SPC") {
        addButton.onclick = function() {saveSPC(true);};
        saveButton.onclick = function() {saveSPC(false);};
        addButton.disabled=false;
        saveButton.disabled=false;
        init_spc();
    } else {
        addButton.onclick = function() {saveBC(true);};
        saveButton.onclick = function() {saveBC(false);};
        addButton.disabled=true;
        saveButton.disabled=true;
    }
}

function saveQRC(addToExp) {
    //Save relevant values for DWR
    var name = $("nameInput").value;
    var q_id = $("questionSelect").value;
    var type = $("eligibleCriterion").value;
    var sel = $("selectedOptions");
    var opts = new Array(sel.length);
    for(j = 0; j < sel.length; j++) {
        opts[j] = sel.options[j].value;
    }
    
    //Reset relevant fields
    $("nameInput").value = "";
    $("questionSelect").selectedIndex=0;
    questionSelectChange();
    
    //Do DWR, pass result to handleCondition(cond, addToExp)
    //public QuestionResultCondition createQuestionResultCondition(String name, String q_id, String[] opts, String type) throws Exception;
    conditionManagerDWR.createQuestionResultCondition(name, q_id, opts, type, function(cond){handleNewCondition(cond, addToExp);});
}

function saveETC(addToExp) {
    //Save relevant values for DWR
    var name = $("nameInput").value;
    var history = ($("historySelect").selectedIndex == 0);
    var selIn = $("select_in1");
    var selOut = $("select_out1");
    var typeIdsIn = new Array(selIn.length);
    var typeIdsOut = new Array(selOut.length);
    for(j = 0; j < selIn.length; j++) {
        typeIdsIn[j] = selIn.options[j].value;
    }
    for(j = 0; j < selOut.length; j++) {
        typeIdsOut[j] = selOut.options[j].value;
    }
    
    //Reset relevant fields
    if(selIn.length == 0 && selOut.length == 0) {
        setMessage("<fmt:message key='filterForm.mustSelect'/>", false);
    } else {
        $("nameInput").value = "";
        $("newFilterTypeSelect").selectedIndex=0;
        changeNewFilterType();
        
        //Do DWR, pass result to handleNewCondition(cond, addToExp)
        //public ExperimentTypeCondition createExperimentTypeCondition(String name, String [] typeIds, String onList, String history) throws Exception;
        
        if(selIn.length != 0)
            conditionManagerDWR.createExperimentTypeCondition(name, typeIdsIn, "true", history, function(cond){handleNewCondition(cond, addToExp);});
            
            if(selOut.length != 0)
                conditionManagerDWR.createExperimentTypeCondition(name, typeIdsOut, "false", history, function(cond){handleNewCondition(cond, addToExp);});
    }
}

function saveSPC(addToExp) {
    //Save relevant values for DWR
    var name = $("nameInput").value;
    var selIn = $("select_in2");
    var selOut = $("select_out2");
    var poolIdsIn = new Array(selIn.length);
    var poolIdsOut = new Array(selOut.length);
    for(j = 0; j < selIn.length; j++) {
        poolIdsIn[j] = selIn.options[j].value;
    }
    for(j = 0; j < selOut.length; j++) {
        poolIdsOut[j] = selOut.options[j].value;
    }
    
    if(selIn.length == 0 && selOut.length == 0) {
        setMessage("<fmt:message key='filterForm.mustSelect'/>", false);
    } else {
        //Reset relevant fields
        $("nameInput").value = "";
        $("newFilterTypeSelect").selectedIndex=0;
        changeNewFilterType();
        
        //Do DWR, pass result to handleNewCondition(cond, addToExp)
        if(selIn.length != 0)
            conditionManagerDWR.createPoolCondition(name, poolIdsIn, "true",  function(cond){handleNewCondition(cond, addToExp);});
            
            if(selOut.length != 0)
                conditionManagerDWR.createPoolCondition(name, poolIdsOut, "false",  function(cond){handleNewCondition(cond, addToExp);});
    }
}

function saveBC(addToExp) {
    alert("In saveBC(" + addToExp + ")");
    //Save relevant values for DWR
    var name = $("nameInput").value;
    var first_cond_id = $("firstConditionSelect").value;
    var second_cond_id = $("secondConditionSelect").value;
    var op = $("bcOpSelect").value;
    
    //Reset relevant fields
    $("nameInput").value = "";
    $("firstConditionSelect").selectedIndex = 0;
    $("secondConditionSelect").selectedIndex = 0;
    bcSelectChange();
    
    //Do DWR, pass result to handleCondition(cond, addToExp)
    conditionManagerDWR.createBooleanCondition(name, first_cond_id, second_cond_id, op,  function(cond){handleNewCondition(cond, addToExp);});
}

function handleNewCondition(cond, addToExp) {
    //Add to lists of all filters
    var selList = $("oldConditionSelect", "firstConditionSelect", "secondConditionSelect");
    var opt;
    for(j = 0; j < selList.length; j++) {
        opt = appendOption(selList[j], cond.name);
        opt.value = cond.id;
    }
    
    //If addToExp, add to experiment (using function above)
    if(addToExp) {
        addFilter(cond);
    } else {    //If not, display successful save message
        setMessage("<fmt:message key='filterForm.createSuccess'/>", true);
    }
}

function questionSelectChange() {
    var div = $("qrcInternalDiv");
    var sel = $("questionSelect");
    var lst = $("addExpButton", "saveLaterButton");
    clearNodeChildren(div);
    clearMessage();
    if(sel.value != "NEW") {
        for(j = 0; j < lst.length; j++)
            lst[j].disabled=false;
        questionManagerDWR.getByIdString(sel.value, init_qrc);
    } else {
        for(j = 0; j < lst.length; j++)
            lst[j].disabled=true;
    }
}

function init_qrc(q) {
    var div = $("qrcInternalDiv");
    addTextChild(div, q.question);
    var ul = addChild(div, "ul");
    var li, check, select, label;
    var optlst = q.optionList;
    if(q.styleString == "DROP_LIST") {
        li = addChild(ul, "li");
        select = addChild(li, "select");
        for(j = 0; j < optlst.length; j++) {
            appendOption(select, optlst[j]);
        }
    }
    else {
        var type = (q.styleString == "RADIO") ? "radio" : "checkbox";
        for(j = 0; j < optlst.length; j++) {
            li = addChild(ul, "li");
            check = document.createElement("input");
            check.type = type;
            li.appendChild(check);
            label = addChild(li, "label");
            addTextChild(label, optlst[j]);
        }
    }
    
    var leftList = optlst;
    var rightList = null;
    var leftId = "availableOptions";
    var rightId = "selectedOptions";
    var listCount="";
    var tab = getPickListTable(leftList, rightList, leftId, rightId, listCount);
    div.appendChild(tab);
    //********** THESE WIDTHS SHOULD NOT HAVE TO BE HARD-CODED (but they'll work for now) ***************
    $(leftId).style.width="200px";
    $(rightId).style.width="200px";
    
    var div2 = addChild(div, "div");
    addTextChild(div2, "Eligible subjects ");//*****NOT LANGUAGE SAFE********
    
    //alert(7);
    var opt;
    select = addChild(div2, "select");
    select.id = "eligibleCriterion";
    select.name="eligibleCriterion";
    if(q.styleString=="CHECK_BOX") {
        opt = appendOption(select, "check all");//*****NOT LANGUAGE SAFE********
        opt.value = "CHECK_ALL";
        opt = appendOption(select, "check some");//*****NOT LANGUAGE SAFE********
        opt.value = "CHECK_SOME";
        opt = appendOption(select, "don't check some");//*****NOT LANGUAGE SAFE********
        opt.value = "CHECK_NOT_SOME";
        opt = appendOption(select, "check none");//*****NOT LANGUAGE SAFE********
        opt.value = "CHECK_NONE";
    } else {
        opt = appendOption(select, "select one");//*****NOT LANGUAGE SAFE********
        opt.value = "SELECT";
        opt = appendOption(select, "select none");//*****NOT LANGUAGE SAFE********
        opt.value = "NO_SELECT";
    }
    addTextChild(div2, " of the filtered options");//*****NOT LANGUAGE SAFE********
    
}

function init_etc() {
    moveAllOptions($('select_in1'), $('avail1'), true);
    moveAllOptions($('select_out1'), $('avail1'), true);
}

function init_spc() {
    moveAllOptions($('select_in2'), $('avail2'), true);
    moveAllOptions($('select_out2'), $('avail2'), true);
}

function historySelectChange() {
    var sel = $("historySelect");
    var head = $("tripick_head_types");
    //Set the proper heading for the pick list
    if(sel.selectedIndex == 0) {
        head.firstChild.nodeValue="<fmt:message key='eligText.typePickPast'/>";
    } else {
        head.firstChild.nodeValue="<fmt:message key='eligText.typePickFuture'/>";
    }
}

function setMessage(msgTxt, isSuccess) {
    var div = $("messageDiv");
    div.className="message";
    clearNodeChildren(div);
    var img = addChild(div, "img");
    img.className = "icon";
    if(!isSuccess) {
        img.src = "/images/iconWarning.gif";
        img.alt = "Error";
        addTextChild(div, " " + msgTxt);
    } else {
        img.src = "/images/iconInformation.gif";
        img.alt = "Information";
        addTextChild(div, " " + msgTxt);
    }
    addChild(div, "br");
}

function bcSelectChange() {
    var lst = $("addExpButton", "saveLaterButton");
    var lst2 = $("firstConditionSelect", "secondConditionSelect");
    var dis = false;
    
    for(j = 0; j < lst2.length; j++) {
        if(lst2[j].selectedIndex == 0) {
            dis = true;
        }
    }
    
    for(j = 0; j < lst.length; j++) {
        lst[j].disabled = dis;
    }
}

function setEligible(lst) {
    var cntSpan = $("eligCntSpan");
    var lstSel = $("eligEmailList");
    
    clearNodeChildren(cntSpan);
    clear(lstSel);
    
    addTextChild(cntSpan, lst.length);
    for(j = 0; j < lst.length; j++) {
        appendOption(lstSel, lst[j]);
    }
    
    lstSel.size = min(EMAIL_LIST_MAX_SIZE, lst.length);
}

function min(a, b) {
    return (a < b) ? a : b;
}

