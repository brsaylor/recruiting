//Code for getQuestionTable requirest that common.js be included

dispMess = false;        

var qIdList = new Array();        

//Actually adds the question to the survey (both locally and in database) and updates numList
function doadd(q) {
    //alert(dwr.util.toDescriptiveString(q, 2));
    var div = $("surveyPreviewDiv");
    //If there aren't any questions, display the title portion of the survey preview section
    if(qcnt == 0) {
        addChild(div, "hr");
        addChild(div, "br");
        var h2 = addChild(div, "h2");
        addTextChild(h2, "Preview Survey");//*****NOT LANGUAGE SAFE********
        addChild(div, "br");
    }
    
    var afterSel = $("afterSelect");
    var afterVal = afterSel.options[afterSel.selectedIndex].value;
    var ind;
    if(afterVal == ADD_END) {
        ind = qcnt;
    } else if (afterVal == ADD_FRONT) {
        ind = 0;
    } else {
        ind = afterVal;
    }
    //alert(ind);
    ind = Number(ind);
    
    for(j = qcnt-1; j >= ind; j--)
        renumber(j, j+1);
    
    var table = getQuestionTable(ind, q, table);
    if(ind == qcnt) {
        //alert("appending child");
        div.appendChild(table);
    }
    else {
        //alert("q"+(ind+1)+"table");
        div.insertBefore(table, $("q"+(ind+1)+"table"));
    }
    
    //Set the fields to be disabled
    if(!NO_FILTER)
        filterchange(ind);
    
    /*var elts = document.getElementsByTagName("td");
    for(var m = 0; m < elts.length; m++) {
        if(elts[m].id.length > 0) {
            alert(elts[m].id + ": " + elts[m].firstChild.nodeValue);
        }
    }*/
    
    //alert("At this point we would add DWR function to add it dynamically to database");
    //surveyManagerDWR.addQuestionToSurvey(SURVEY_ID, q.id, ind, check_survey);  
    
    //Rather than actually adding the questions to the database survey,
    //we simply add them to the list of question id's
    for(var j = qcnt - 1; j >= ind; j--) {
        qIdList[j+1] = qIdList[j];
    }
    qIdList[ind] = q.id;
    
    qcnt++;
    updateNumList();
    
    if(qcnt != qIdList.length) {
        alert("Error! qcnt (" + qcnt + ") is not equal to qIdList.length (" + qIdList.length + ")");//*****NOT LANGUAGE SAFE********
    } else if(dispMess) {
        var span = $("successSpan");
        span.className="message";
        clearNodeChildren(span);
        var img = addChild(span, "img");
        img.src = "/images/iconInformation.gif";
        img.alt = "Information";
        img.className = "icon";
        addTextChild(span, " Successfully added question: Survey now has " + qcnt + " questions...");//*****NOT LANGUAGE SAFE********
        addChild(span, "br");
        //alert("Survey is now: " + qIdList);
    }
}

function renumber(a, b) {
    //alert("In renumber(" + a + "," + b + ")");
    //alert($("questionTextData"+a));
    var leftL = $("availableOptions"+a);
    leftL.id = "availableOptions"+b;
    leftL.name = leftL.id;
    
    //alert(2);
    var rightL = $("selectedOptions"+a);
    rightL.id = "selectedOptions"+b;
    rightL.name = rightL.id;
    
    //alert(3);
    var el = $("moveRight"+a);
    el.id = "moveRight"+b;
    el.onclick = function(){moveSelectedOptions(leftL,rightL,true)};
    
    //alert(4);
    el = $("moveAllRight"+a);
    el.id = "moveAllRight"+b;
    el.onclick=function() {moveAllOptions(leftL,rightL,true)};
    
    //alert(5);
    el = $("moveLeft"+a)
    el.id = "moveLeft"+b;
    el.onclick=function() {moveSelectedOptions(rightL,leftL,true)};
    
    //alert(6);
    el = $("moveAllLeft"+a);
    el.id = "moveAllLeft"+b;
    el.onclick=function() {moveAllOptions(rightL,leftL,true)};
    
    //alert(7);
    el = $("q"+a+"table");
    el.id = "q"+b+"table";
    
    el = $("filter_pickList_" + a);
    el.id = "filter_pickList_" + b;
    
    //alert(8);
    el = $("questionTextData"+a);
    el.id = "questionTextData"+b;
    
    //alert(9);
    q = el.firstChild.nodeValue;
    q = trimLT(q);
    el.firstChild.nodeValue = (" " + (b+1) + q.substring(String(a).length));
    
    //alert(10);
    el = $("filter"+a);
    el.id = "filter"+b;
    el.name = el.id;
    el.onclick = function() {filterchange(b);};
    
    //alert(11);
    el=$("eligibleCriterion"+a);
    el.id = "eligibleCriterion"+b;
    el.name = el.id;
    
    //alert(12);
    el=$("remove"+a);
    el.id="remove"+b;
    el.onclick=function() {removeQuestion(b);};
    
    el = $("filterName"+a);
    el.id="filterName"+b;
    el.name="filterName"+b;
}

function filterchange(num) {
    filterCheck = $("filter" + num);
    //filterNodes = $("availableOptions"+num, "moveRight"+num, "moveAllRight"+num, "moveLeft"+num, "moveAllLeft"+num, "selectedOptions"+num, "eligibleCriterion"+num);
    filterNode = $("filter_pickList_"+num);
    disab = !(filterCheck.checked);
    //for(var j = 0; j < filterNodes.length; j++) {
    if(disab) {
        //filterNodes[j].disabled = true;
        filterNode.style.display="none";
    }
    else {
        //filterNodes[j].disabled = false;
        filterNode.style.display="block";
    }
    //}
}

function formSubmit() {
    var j;
    if(!NO_FILTER) {
        //Pass in all of the selected filter options
        for(j = 0; j < qcnt; j++) {
            if($("filter"+j).checked) {
                selectAll("selectedOptions" + j);
            }
        }
    }
    
    form = $("surveyForm");
    //Pass in the list of questions for the survey
    addHiddenChild(form, "qcnt", qcnt);
    for(j = 0; j < qcnt; j++) {
        addHiddenChild(form, "questionId_" + j, qIdList[j]);
    }
    
    return true;
}

function getQuestionTable(num, q) {
    //alert("In getQuestionTable");
    
    //alert(1);
    
    var j, k, text;
    
    var table = document.createElement("table");
    table.id = "q"+num+"table";
    table.className="questionTable";
    
    //This is required for IE (though not for Firefox)
    var tab = document.createElement("tbody");
    table.appendChild(tab);
    
    //alert(2);
    var tr = addChild(tab, "tr");
    
    var td = addChild(tr, "td");
    //td.id="questionTextData"+num;
    //td.setAttribute("valign", "top");
    td.style.verticalAlign = "top";
    
    var input_img = document.createElement("input");
    input_img.id = "remove"+num;
    input_img.type="image";
    input_img.src = "/images/trash-icon.png";
    input_img.alt = "Remove question";//*****NOT LANGUAGE SAFE********
    input_img.onclick=function() {removeQuestion(num);};
    td.appendChild(input_img);
    
    var span = addChild(td, "span");
    span.id = "questionTextData"+num;
    addTextChild(span, " " + (num + 1) + ". " + q.question);
    
    //addTextChild(td, (num + 1) + ". " + q.question);
    
    //alert(3);
    var ul = addChild(td, "ul");
    var li;
    var check, label;
    var select;
    
    //alert(1);
    var optlst = q.optionList;
    if(q.styleString == "DROP_LIST") {
        li = addChild(ul, "li");
        select = addChild(li, "select");
        for(j = 0; j < optlst.length; j++) {
            appendOption(select, optlst[j]);
        }
    }
    else {
        var type = (q.styleString == "RADIO") ? "radio" : "checkbox";
        for(j = 0; j < optlst.length; j++) {
            li = addChild(ul, "li");
            check = document.createElement("input");
            check.type = type;
            li.appendChild(check);
            label = addChild(li, "label");
            addTextChild(label, optlst[j]);
        }
    }
    
    if(!NO_FILTER) {
        td = addChild(tr, "td");
        //td.className="vtop";
        //td.setAttribute("valign", "top");
        td.style.verticalAlign = "top";
        td.style.textAlign = "right";
        
        
        //alert(2);
        check=document.createElement("input");
        check.type="checkbox";
        td.appendChild(check);
        check.id="filter" + num;
        check.name="filter"+num;
        check.onclick = function() {filterchange(num);}
        
        //alert(5);
        label = addChild(td, "label");
        label.htmlFor=check.id;
        addTextChild(label, "Create filter");//*****NOT LANGUAGE SAFE********
        
        addChild(td, "br");
        
        var fs = addChild(td, "fieldset");
        fs.className="pickList";
        fs.id = "filter_pickList_" + num;
        
        //alert(6);
        var legend = addChild(fs, "legend");
        addTextChild(legend, "Filter settings");//*****NOT LANGUAGE SAFE********
        
        var div = addChild(fs, "div");
        addTextChild(div, "Filter name: ");//*****NOT LANGUAGE SAFE********
        var txt = document.createElement("input");
        txt.name="filterName"+num;
        txt.id="filterName"+num;
        txt.type="text";
        div.appendChild(txt);
        
        var leftList = optlst;
        var rightList = null;
        var leftId = "availableOptions"+num;
        var rightId = "selectedOptions"+num;
        var listCount=num;
        fs.appendChild(getPickListTable(leftList, rightList, leftId, rightId, listCount));
        
        
        div = addChild(fs, "div");
        div.className = "eligibleSubjectsDiv";
        addTextChild(div, "Eligible subjects ");//*****NOT LANGUAGE SAFE********
        
        //alert(7);
        var opt;
        select = addChild(div, "select");
        select.id = "eligibleCriterion"+num;
        select.name="eligibleCriterion"+num;
        if(q.styleString=="CHECK_BOX") {
            opt = appendOption(select, "check all");//*****NOT LANGUAGE SAFE********
            opt.value = "CHECK_ALL";
            opt = appendOption(select, "check some");//*****NOT LANGUAGE SAFE********
            opt.value = "CHECK_SOME";
            opt = appendOption(select, "don't check some");//*****NOT LANGUAGE SAFE********
            opt.value = "CHECK_NOT_SOME";
            opt = appendOption(select, "check none");//*****NOT LANGUAGE SAFE********
            opt.value = "CHECK_NONE";
        } else {
            opt = appendOption(select, "select one");//*****NOT LANGUAGE SAFE********
            opt.value = "SELECT";
            opt = appendOption(select, "select none");//*****NOT LANGUAGE SAFE********
            opt.value = "NO_SELECT";
        }
        addChild(div, "br");
        addTextChild(div, " of the filtered options");//*****NOT LANGUAGE SAFE********
    }
    
    return table;
}

function removeQuestion(num) {
    //alert("In removeQuestion(" + num + ")");
    
    var j;
    var div = $("surveyPreviewDiv");
    //If there aren't any questions left, clear the div
    if(qcnt == 1) {
        clearNodeChildren(div);
    }
    else {
        div.removeChild($("q"+num+"table"));
        for(j = num + 1; j < qcnt; j++)
            renumber(j, j-1);
    }
    
    //alert("At this point we would add DWR function to remove it dynamically from database");
    //surveyManagerDWR.removeQuestionFromSurvey(SURVEY_ID, num, check_survey);
    
    //Rather than actually adding the questions to the database survey,
    //we simply add them to the list of question id's
    for(j = num; j <= qcnt-2; j++) {
        qIdList[j] = qIdList[j+1];
    }
    qIdList.length--;
    
    qcnt--;
    updateNumList();
    
    if(qcnt != qIdList.length) {
        alert("Error! qcnt (" + qcnt + ") is not equal to qIdList.length (" + qIdList.length + ")");
    } else if(dispMess) {
        var span = $("successSpan");
        span.className="message";
        clearNodeChildren(span);
        var img = addChild(span, "img");
        img.src = "/images/iconInformation.gif";
        img.alt = "Information";
        img.className = "icon";
        addTextChild(span, " Successfully removed question: Survey now has " + qcnt + " question(s)...");//*****NOT LANGUAGE SAFE********
        addChild(span, "br");
    }
}

//Returns false to prevent submission
function addQuestion() {
    //alert("In addQuestion");
    
    select = $("questionSelect");
    styleSelect = $("styleSelect");
    oSelect = $("optionsSelect");
    form = $("surveyForm");
    var q;
    if(select.selectedIndex == 0) {
        if(oSelect.length == 0) {
            alert("Question must have one or more options");
            return false;
        }
        var olst = new Array(oSelect.length);
        for(var j = 0; j < oSelect.length; j++)
            olst[j] = oSelect.options[j].text;
        questionManagerDWR.saveQuestion($("labelText").value, $("questionText").value, styleSelect.options[styleSelect.selectedIndex].value, olst, function(data) {doadd(data); updateQuestionList(data);});
    } else {
        questionManagerDWR.getByIdString(questionSelect.options[questionSelect.selectedIndex].value, doadd);
    }
    
    //Reset the question select field
    select.selectedIndex = 0;
    setupQuestion();
    return false;
}

function check_survey(survey) {
    var qlst = survey.questions;
    
    var str = "After operations, the survey was: {\n";
    for(var j = 0; j < qlst.length; j++) {
        str += qlst[j].question + ", ";
    }
    str += "}";
    alert(str);
    
    if(qcnt != qlst.length) {
        alert("Error on page!\n\nThe list of questions is not the same length as that in the database.\nPlease restart the form");
        alert("database: " + qlst.length + ", page: " + qcnt);
        return;
    }
    
    var a, b;
    
    for(var j = 0; j < qcnt; j++) {
        a = trimLT(qlst[j].question);
        b = trimLT($("questionTextData" + j).firstChild.nodeValue);
        b = trimLT(b.substring(String(j+1).length + 2));
        if(a != b) {
            alert("Error on page!\n\nQuestion number " + (j+1) + " does not match that in database.\nPlease restart the form");
            alert("database: " + a + ", page: " + b);
            return;
        }
    }
    
    alert("Database has been changed successfully");
}

/*function testFN() {
    alert(qcnt);
    alert(qIdList);
}*/

function load() {
    surveyManagerDWR.getByIdString(SURVEY_ID, init_survey);
}

function init_survey(survey) {
    var lst = survey.questions;
    var j;    
    for(j = 0; j < lst.length; j++) {
        doadd(lst[j]);
    }
    
    dispMess = true;
    
    //Make sure that all the filters are disabled
    /*for(j = 0; $("filter" + j) != null; j++) {
        filterchange(j);
    }*/
}

function addOptIfEnter(event) {
    if(isEnterKey(event)) {
        addOption();
        return false;
    }
    return true;
}

function addQIfEnter(event) {
    if(isEnterKey(event)) {
        return addQuestion();
    }
    return true;
}

function updateFields(q) {
    labelText = $("labelText");
    questionText = $("questionText");
    styleSelect = $("styleSelect");
    newOptionText = $("newOptionText");
    optionsSelect = $("optionsSelect");
    
    //alert(dwr.util.toDescriptiveString(q, 2));
    
    //Setup the text input fields
    labelText.value=q.label;
    questionText.value=q.question;
    newOptionText.value="";
    
    //Setup the style select
    style = q.styleString;
    if(style == "CHECK_BOX") {
        styleSelect.selectedIndex = 0;
    }
    else if(style == "RADIO") {
        styleSelect.selectedIndex = 1;
    }
    else {
        styleSelect.selectedIndex = 2;
    }
    
    //Setup the options select
    clear(optionsSelect);
    options = q.optionList;
    for(var j = 0; j < options.length; j++) {
        appendOption(optionsSelect, options[j]);
    }
    
}

function updateQuestionList(q) {
    //alert("Need to write function to update question list");
    var sel = $("questionSelect");
    var opt = appendOption(sel, q.label);
    opt.value = q.id;
}

function updateNumList() {
    var sel = $("afterSelect");
    clear(sel);
    var opt = appendOption(sel, ADD_END);
    opt.value = ADD_END;
    if(qcnt > 0) {
        opt = appendOption(sel, ADD_FRONT);
        opt.value = ADD_FRONT;
    }
    
    for(var j = 1; j < qcnt; j++) {
        opt = appendOption(sel, j);
        opt.value = j;
    }
}

function addOption() {
    newOptionText = $("newOptionText");
    optionsSelect = $("optionsSelect");
    optionText = newOptionText.value;
    if(optionText.length > 0) {
        appendOption(optionsSelect, optionText);
        
        newOptionText.value = "";
    }
    return false;
}

function removeOptions() {
    optionsSelect = $("optionsSelect");
    while(optionsSelect.selectedIndex != -1) {
        optionsSelect.remove(optionsSelect.selectedIndex);
    }
    
    return false;
}

function setupQuestion() {
    questionSelect = $("questionSelect");
    labelText = $("labelText");
    questionText = $("questionText");
    styleSelect = $("styleSelect");
    newOptionText = $("newOptionText");
    optionsSelect = $("optionsSelect");
    addOpt = $("addopt");
    remOpt = $("remopt");
    
    if(questionSelect.selectedIndex == 0) {
        labelText.disabled=false;
        questionText.disabled=false;
        styleSelect.disabled=false;
        newOptionText.disabled=false;
        optionsSelect.disabled=false;
        addOpt.disabled=false;
        remOpt.disabled=false;
        
        //Clear the text input fields
        labelText.value="";
        questionText.value="";
        newOptionText.value="";
        
        //Clear the options select
        clear(optionsSelect);
    }
    else {
        labelText.disabled=true;
        questionText.disabled=true;
        styleSelect.disabled=true;
        newOptionText.disabled=true;
        optionsSelect.disabled=true;
        addOpt.disabled=true;
        remOpt.disabled=true;
        
        //Get the Question via DWR, response function updateFields will do the setup once AJAX is complete
        questionManagerDWR.getByIdString(questionSelect.options[questionSelect.selectedIndex].value, updateFields);
    }
}

function add_tripick(availSel, suffix) {
    alert("in add_tripick: " + suffix);
    var sel = $("select_" + suffix);
    var ind = availSel.selectedIndex;
    while(ind != -1) {
        sel.add(availSel.options[ind]);
        availSel.remove(ind);
        ind = availSel.selectedIndex;
    }
}

function rem_tripick(availSel, suffix) {
    alert("in rem_tripick: " + suffix);
}

