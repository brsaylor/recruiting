//Parses a list (in string form) into an array
function parseJSPList(str) {
    var str2 = "";
    var j;
    var lst;
    var rem = " []";//Characters that we want to remove: ' ', '[', and ']'
    for(j = 0; j < str.length; j++) {
        if(rem.indexOf(str.charAt(j)) == -1) {
            str2 += str.charAt(j);
        }
    }
    return str2.split(',');
}

function clear(select) {
    if(select == null)
        return ;
    while(select.length > 0)
        select.remove(0);
}

function appendOption(select, thetext) {    
    var o = new Option();
    o.text = thetext;
    try {
        select.add(o, null);
    } catch (e) {
        select.add(o);
    }
    return o;
}

function appendSplitOption(select, thetext) {    
    var o_details = thetext.split(",");
    var o = new Option(o_details[1], o_details[0]);
    try {
        select.add(o, null);
    } catch (e) {
        select.add(o);
    }
    return o;
}

function addChild(parent, childtype) {
    var child = document.createElement(childtype);
    parent.appendChild(child);
    return child;
}

function addTextChild(parent, text) {
    var child = document.createTextNode(text);
    parent.appendChild(child);
}

function trimLT(str) {
    var j, k;
    j = 0;
    while(j < str.length && " \n".indexOf(str.charAt(j)) != -1) {j++;}
    k = str.length - 1;
    while(k > -1 && " \n".indexOf(str.charAt(k)) != -1) {k--;}
    return str.substring(j, k+1);
}

function clearNodeChildren(node) {
    while(node.firstChild != null) {
        node.removeChild(node.firstChild);
    }
}

function addHiddenChild(par, label, value) {
    var hid = document.createElement("input");
    hid.type="hidden";
    hid.name=label;
    hid.value=value;
    par.appendChild(hid);
    return hid;
}

function getPickListTable(leftList, rightList, leftId, rightId, listCount) {
    //alert("In pickListTable");
    
    var j, k;
    
    var acttable = document.createElement("table");
    acttable.className="pickList";
    var table = document.createElement("tbody");
    acttable.appendChild(table);
    //addChild(acttable, "tbody");//Needed in IE
    
    var tr = addChild(table, "tr");
    tr.className="pickList";
    var th = addChild(tr, "th");
    th.className="pickLabel";
    var label = addChild(th, "label");
    label.className="required";
    addTextChild(label, "Available Options");//*****NOT LANGUAGE SAFE******** - Should be surveyCreationForm.availableOptions
    
    addChild(tr, "td");
    
    th = addChild(tr, "th");
    th.className="pickLabel";
    label = addChild(th, "label");
    label.className="required";
    addTextChild(label, "Filtered Options");//*****NOT LANGUAGE SAFE******** - Should be surveyCreationForm.filteredOptions
    
    tr = addChild(table, "tr");
    tr.className="pickList";
    td = addChild(tr, "td");
    var select = addChild(td, "select");
    select.name=leftId;
    select.multiple=true;
    select.onDblClick=function() {moveSelectedOptions(this,rightId,true)};
    select.id=leftId;
    select.size=5;
    var opt;
    if(leftList != null) {
        for(j = 0; j < leftList.length; j++) {
            opt = appendOption(select, leftList[j]);
            opt.value = j;
        }
    }
    
    td = addChild(tr, "td");
    td.className="moveOptions";
    
    var button = document.createElement("button");
    try{button.type="button";}catch(e){};//Yet another instance of IE sucking (it doesn't support W3C spec button.type)
    td.appendChild(button);
    button.name="moveRight";
    button.id="moveRight"+listCount;
    button.onclick=function() {moveSelectedOptions($(leftId),$(rightId),true)};
    button.className="moveOptions";
    addTextChild(button, ">>");
    //button.value = ">>";
    
    addChild(td, "br");
    
    button = document.createElement("button");
    try{button.type="button";}catch(e){};
    td.appendChild(button);
    button.name="moveAllRight";
    button.id="moveAllRight"+listCount;
    button.onclick=function() {moveAllOptions($(leftId),$(rightId),true)};
    button.className="moveOptions";
    addTextChild(button, "All >>");//*****NOT LANGUAGE SAFE********
    //button.value = "All >>";
    
    addChild(td, "br");
    
    button = document.createElement("button");
    try{button.type="button";}catch(e){};
    td.appendChild(button);
    button.name="moveLeft";
    button.id="moveLeft"+listCount;
    button.onclick=function() {moveSelectedOptions($(rightId),$(leftId),true)};
    button.className="moveOptions";
    addTextChild(button, "<<");
    //button.value = "<<";
    
    addChild(td, "br");
    
    button = document.createElement("button");
    try{button.type="button";}catch(e){};
    td.appendChild(button);
    button.name="moveAllLeft";
    button.id="moveAllLeft"+listCount;
    button.onclick=function() {moveAllOptions($(rightId),$(leftId),true)};
    button.className="moveOptions";
    addTextChild(button, "All <<");//*****NOT LANGUAGE SAFE********
    //button.value = "All <<";
    
    td = addChild(tr, "td");
    select = addChild(td, "select");
    select.name=rightId;
    select.multiple=true;
    select.id=rightId;
    select.size=5;
    if(rightList != null) {
        for(j = 0; j < rightList.length; j++) {
            opt = appendOption(select, rightList[j]);
            opt.value = j;
        }
    }
    
    return acttable;
}

function isEnterKey(event) {
    //The enter key should be the unicode key 13
    return (event.keyCode == 13);
}

