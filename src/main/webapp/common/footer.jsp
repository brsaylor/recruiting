<%@ include file="/common/taglibs.jsp" %>

    <div id="divider"><div></div></div>

    <span class="left">
        
        <%-- BRS removed 2012-01-24 --%>
        <%-- <fmt:message key="webapp.version"/> |
        <span id="validators">
            <a href="http://validator.w3.org/check?uri=referer">XHTML Valid</a> |
            <a href="http://jigsaw.w3.org/css-validator/validator-uri.html">CSS Valid</a>
        </span>
        --%>
        <c:if test="${pageContext.request.remoteUser != null}">
        <fmt:message key="user.status"/> ${pageContext.request.remoteUser}
        </c:if>
    </span>
    <span class="right">
        <fmt:message key="webapp.tagline"/> |
        &copy; <fmt:message key="copyright.year"/> <a href="<fmt:message key="copyright.url"/>"><fmt:message key="copyright.name"/></a>
    </span>
