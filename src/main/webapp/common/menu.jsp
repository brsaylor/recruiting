<%@ include file="/common/taglibs.jsp"%>
<ul id="primary-nav" class="menuList">
    <menu:useMenuDisplayer name="Velocity" config="cssHorizontalMenu.vm">
        <li class="pad">&nbsp;</li>
        <menu:displayMenu name="CalendarMenu"/>
        <%--<c:out value="nrl=${noRegOrLogin}"/>--%>
        <c:if test="${empty pageContext.request.remoteUser && empty noRegOrLogin}">
            <menu:displayMenu name="RegisterMenu"/>
            <menu:displayMenu name="Login"/>
            <%--<li><a href="<c:url value="/login.jsp"/>"><fmt:message key="login.title"/></a></li>--%>
        </c:if>
    </menu:useMenuDisplayer>
    <menu:useMenuDisplayer name="Velocity" config="cssHorizontalMenu.vm" permissions="rolesAdapter">
        <menu:displayMenu name="ExperimenterMenu"/>
        <menu:displayMenu name="AdminMenu"/>
        <menu:displayMenu name="SubjectMenu"/>
        <menu:displayMenu name="MessagesMenu"/>
        <menu:displayMenu name="Logout"/>
    </menu:useMenuDisplayer>
</ul>
