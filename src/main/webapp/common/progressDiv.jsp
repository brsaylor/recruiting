<%--
BRS 2011-10-28
Factored out of experimentform, surveycreationform, filterform, and experimentverificationform
and included in above.
--%>
<div id="progressDiv">
    <center>
        <table cellpadding="0" cellspacing="0" border="0"><tr>
            <c:forEach items="${progressList}" var="progressItem">
                <td align="center">

                <c:choose>
                    <c:when test="${currentStep == progressItem.label}"><b><c:out value="${progressItem.label}"/></b></c:when>
                    <c:otherwise>
                        <a href="${progressItem.value}"><c:out value="${progressItem.label}"/></a>
                    </c:otherwise>
                </c:choose>

                </td>
                <c:if test="${progressItem != 'Publish'}"><td width="60px" align="center"><img src="images/longarrow.gif" /></td></c:if>
            </c:forEach>
        </tr></table>
    </center>
</div>
