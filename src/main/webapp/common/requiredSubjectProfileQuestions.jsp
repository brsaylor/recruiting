<%--
Ben Saylor March 2010
required subject profile questions
included in signup.jsp and userForm.jsp
--%>

<li>
    <fieldset>
        <legend>Required Information</legend>

        <div>
            <appfuse:label styleClass="desc" key="user.email"/>
            <form:errors path="email" cssClass="fieldError"/>
            <form:input path="email" id="email" cssClass="text medium"/>
        </div>

        <table class="flush">
            <tr>
                <td>
                    <appfuse:label styleClass="desc" key="user.firstName"/>
                    <form:errors path="firstName" cssClass="fieldError"/>
                    <form:input path="firstName" id="firstName" cssClass="text medium"/>
                </td>
                <td>
                    <appfuse:label styleClass="desc" key="user.lastName"/>
                    <form:errors path="lastName" cssClass="fieldError"/>
                    <form:input path="lastName" id="lastName" cssClass="text medium"/>
                </td>
            </tr>
        </table>

        <c:if test="${isSubj || pageName == 'signup'}">
            <fieldset class="subfieldset">
                <legend>ID Number</legend>
                <c:choose>
                    <c:when test="${pageName == 'signup'}">
                        <form:errors path="schoolId" cssClass="fieldError"/>
                        <label class="desc" for="schoolId"><fmt:message key="user.schoolId"/>
                            <span class="req">*</span>
                        </label>
                        <form:input id="schoolId" path="schoolId"/>
                    </c:when>
                    <c:otherwise>
                        <label class="desc" for="schoolId"><fmt:message key="user.schoolId"/>
                            <span class="req">*</span></label>
                        <%-- This field will be automatically bound by Spring --%>
                        <input type="text" id="schoolId" name="schoolId"
                        <c:if test="${isSubj}">value="${user.schoolId}"</c:if>
                        <%-- <c:if test="${isAdmin != 'true'}">READONLY</c:if> --%> <%-- BRS 2012-02-03 why? removed. --%>
                        />
                    </c:otherwise>
                </c:choose>

                <div>
                    <form:errors path="idType" cssClass="fieldError"/>
                    <label class="desc"><fmt:message key="user.idType"/>
                    <span class="req">*</span></label>
                    <form:radiobutton path="idType" value="1"/> UA ID (School ID) <br/>
                    <form:radiobutton path="idType" value="2"/> SSN (Social Security Number) <br/>
                    <form:radiobutton path="idType" value="3"/> ITIN (Individual Taxpayer ID Number) <br/>
                    <form:radiobutton path="idType" value="4"/> driver's license number 
                </div>
                <%--
                <div>
                    <label class="desc"><fmt:message key="user.driversLicenseExpirationDate"/> </label>
                    <brs:dateFields
                    prefix="driversLicenseExpirationDate"
                    year="${user.driversLicenseExpirationDate.year+1900}"
                    month="${user.driversLicenseExpirationDate.month}"
                    day="${user.driversLicenseExpirationDate.date}"/>
                </div>
                --%>
            </fieldset>
        </c:if>

        <c:choose>
            <c:when test="${pageName == 'signup'}">
                <table class="flush">
                    <tr>
                        <td>
                            <label class="desc" for="password">
                                <fmt:message key="user.password"/>
                                <span class="req">*</span>
                            </label>
                            <form:errors path="password" cssClass="fieldError"/>
                            <form:password path="password" id="password" cssClass="text medium"
                                    showPassword="true"/>
                        </td>
                        <td>
                            <label class="desc" for="confirmPassword">
                                <fmt:message key="user.confirmPassword"/>
                                <span class="req">*</span>
                            </label>
                            <form:errors path="confirmPassword" cssClass="fieldError"/>
                            <form:password path="confirmPassword" id="confirmPassword"
                                    cssClass="text medium" showPassword="true"/>
                        </td>
                    </tr>
                </table>
            </c:when>
            <c:otherwise>
                <c:if test="${not myCookie}">
                    <div>
                        <c:if test="${not isNew}">
                            <input type="checkbox" onclick="setChangePassword();"
                                name="changePassword" id="changePassword"/>
                            <label for="changePassword">Change password</label>
                        </c:if>
                    </div>
                    <div id="changePasswordDiv" <c:if test="${not isNew}">style="display:none"</c:if>>
                        <div class="left">
                            <label class="desc" for="mypassword"><fmt:message key="user.password"/>
                                <span class="req">*</span>
                            </label>
                            <input type="password" id="mypassword" name="mypassword" class="text medium"/>
                        </div>
                        <div>
                            <label class="desc" for="myconfirmpassword"><fmt:message key="user.confirmPassword"/> <span class="req">*</span></label>
                            <input type="password" id="myconfirmpassword" name="myconfirmpassword" class="text medium"/>
                        </div>
                    </div>
                </c:if>
            
            </c:otherwise>
        </c:choose>

    </fieldset>
</li>
