<%--
Ben Saylor March 2010
additional subject profile questions
included in signup.jsp and userForm.jsp
--%>

<%--
public String makeDateFields(String prefix) {
    <select id="birthday_month" name="birthday_month" onchange="monthChange();">
                    <c:forEach begin="0" end="11" varStatus="status">
                        <option value="${status.index}"><c:out value="${monthList[status.index].label}"/></option>
                    </c:forEach>
                </select>
                <select id="birthday_day" name="birthday_day">
                    <c:forEach begin="1" end="31" varStatus="status">
                        <option value="${status.index}"><c:out value="${status.index}"/></option>
                    </c:forEach>
                </select>
                <input type="text" id="birthday_year" name="birthday_year" class="text small"/>
}
--%>

<%--
Subject Pools:
    1 UAA Students
    2 Non-UAA Subjects
    3 Unspecified
    4 UAA Staff
    5 UAA Faculty
    6 Seasonality Participants (UAA)
    7 Seasonality Participants (Providence)
--%>

<%-- Get the user's subject pools into JavaScript
    so that code in user.js can check the appropriate radio buttons below --%>
<script type="text/javascript">
    var userPools = [];
    <c:forEach var="item" items="${userLVPools}" varStatus="status">
        userPools.push(${item.value});
    </c:forEach>
</script>

<li>
    <label class="desc">Are you a UAA student, staff, or faculty member?</label>
    <input type="radio" id="uaaYesRadioButton"/>Yes
    <div id="uaaCheckboxes" style="display: block; margin-left: 4em;">
        <input type="checkbox" name="userPools" value="1"/>Student<br/>
        <input type="checkbox" name="userPools" value="4"/>Staff<br/>
        <input type="checkbox" name="userPools" value="5"/>Faculty<br/>
    </div>
    <input type="radio" id="uaaNoRadioButton" name="userPools" value="2"/>No<br/>
    <%-- It's required to select at least one subject pool,
        but we want to default to none selected.
        So have a 3rd pool called "Unspecified", with a hidden field.
        Removed in JS if other pools are selected. --%>
    <input type="hidden" id="userPoolUnspecified" name="userPools" value="3"/>

    <c:if test="${isAdmin}">
        <div>
            Subject pools: 
            <c:forEach var="item" items="${userLVPools}" varStatus="status">
                ${item.label}${!status.last ? ', ' : ''}
            </c:forEach>
        </div>
    </c:if>

</li>

<li>
<label class="desc">Signup Code (optional)</label>
<form:input path="signupCode"/>
</li>

<li class="highlighted">
The following questions are not required to sign up, but must be completed <b>before you can be paid for participating in experiments.</b>
</li>

<li>
    <div>
        <label class="desc"><fmt:message key="user.addressLine1"/> </label>
        <form:input path="addressLine1"/>
    </div>
</li>

<li>
    <div>
        <label class="desc"><fmt:message key="user.addressLine2"/> </label>
        <form:input path="addressLine2"/>
    </div>
</li>
<li>
    <div class="left">
        <label class="desc"><fmt:message key="user.city"/> </label>
        <form:input path="city"/>
    </div>
    <div class="left">
        <label class="desc"><fmt:message key="user.state"/> </label>
<%
String[] states = {"AL","AK","AS","AZ","AR","CA","CO","CT","DE","DC","FM","FL","GA","GU","HI","ID","IL","IN","IA","KS","KY","LA","ME","MH","MD","MA","MI","MN","MS","MO","MT","NE","NV","NH","NJ","NM","NY","NC","ND","MP","OH","OK","OR","PW","PA","PR","RI","SC","SD","TN","TX","UT","VT","VI","VA","WA","WV","WI","WY"};
pageContext.setAttribute("states",states);
%>
        <form:select path="state">
            <form:option value="" label="Select" />
            <c:forEach items="${states}" var="state">
                <form:option value="${state}" label="${state}"/>
            </c:forEach>
        </form:select>
    </div>
    <div>
        <div class="left">
            <label class="desc"><fmt:message key="user.zip"/> </label>
            <form:input path="zip"/>
        </div>
    </div>
</li>
<li>

                <div>
                    <appfuse:label styleClass="desc" key="user.phoneNumber"/>
                    <form:errors path="phoneNumber" cssClass="fieldError"/>
                    <form:input path="phoneNumber" id="phoneNumber" cssClass="text medium"/>
                </div>

    <c:choose>
        <c:when test="${pageName == 'signup'}">

                <div>
                <%--<appfuse:label styleClass="desc" key="user.birthday"/>--%>
                <label class="desc"><fmt:message key="user.birthday"/> </label>
                <select id="birthday_month" name="birthday_month" onchange="monthChange();">
                    <c:forEach begin="0" end="11" varStatus="status">
                        <option value="${status.index}"><c:out value="${monthList[status.index].label}"/></option>
                    </c:forEach>
                </select>
                <select id="birthday_day" name="birthday_day">
                    <c:forEach begin="1" end="31" varStatus="status">
                        <option value="${status.index}"><c:out value="${status.index}"/></option>
                    </c:forEach>
                </select>
                <input type="text" id="birthday_year" name="birthday_year" class="text small"/>
            </div>
        </c:when>
        <c:otherwise>
                <c:if test="${isSubj}">
                    <c:set var="mymonth" value="${user.birthday.month}"/>
                    <!--Birthday month is ${mymonth}-->
                </c:if>
                <c:set var="day_cnt" value="${(mymonth == null) ? (31) : (monthList[mymonth].value)}"/>
                <label class="desc"><fmt:message key="user.birthday"/> </label>
                <select id="birthday_month" name="birthday_month" onchange="monthChange();">
                    <c:forEach begin="0" end="11" varStatus="status">
                        <option value="${status.index}" <c:if test="${isSubj && mymonth==status.index}">selected</c:if>><c:out value="${monthList[status.index].label}"/></option>
                    </c:forEach>
                </select>
                <select id="birthday_day" name="birthday_day">
                    <c:forEach begin="1" end="${day_cnt}" varStatus="status">
                        <option value="${status.index}" <c:if test="${isSubj && user.birthday.date==status.index}">selected</c:if>><c:out value="${status.index}"/></option>
                    </c:forEach>
                </select>
                <input type="text" id="birthday_year" name="birthday_year" class="text small"<c:if test="${isSubj}">value="${user.birthday.year+1900}"</c:if> />
        </c:otherwise>
    </c:choose>



<li>

<li class="highlighted">
    <div>
        <%-- 2012-02-02 converted to JQuery in user.js
        <script type="text/javascript">

            function onUsCitizenChanged() {
                // BRS 2010-04-27
                // Hide/unhide form elements that are required only for non-US citizens.
                // This uses Prototype.
                var form = $('userForm');
                if (form == null) {
                    form = $('signupForm');
                }
                var checkedRadioButton = form.getInputs('radio','usCitizen').find(function(radio) { return radio.checked; });
                if (checkedRadioButton == undefined || checkedRadioButton.value == 'true') {
                    $$('.nonUsCitizen').each(function(el) {
                        el.hide();
                    });
                } else {
                    $$('.nonUsCitizen').each(function(el) {
                        el.show();
                    });
                }
            }

            document.observe("dom:loaded", function() {
                onUsCitizenChanged();
            });

        </script>
        --%>

        <label class="desc"><fmt:message key="user.usCitizen"/> </label>
        <form:radiobutton path="usCitizen" id="usCitizen_true" value="true"/> Yes
        <form:radiobutton path="usCitizen" id="usCitizen_false" value="false"/> No
    </div>

</li>

<%-- To admins only, display the passportOnFile field --%>
<c:if test="${isAdmin}">
    <li class="nonUsCitizen">
        <label class="desc"><fmt:message key="user.passportOnFile"/></label>
        <form:checkbox path="passportOnFile"/>
    </li>
</c:if>

<li class="nonUsCitizen">
    <div>
        <label class="desc"><fmt:message key="user.internationalNotice"/></label>
    </div>
</li>

<li class="nonUsCitizen">
    <div class="left">
        <label class="desc"><fmt:message key="user.citizenCountry"/> </label>
        <recruiting:country name="citizenCountry" prompt="" default="${user.citizenCountry}"/>
    </div>
    <div>
        <label class="desc"><fmt:message key="user.birthCountry"/> </label>
        <recruiting:country name="birthCountry" prompt="" default="${user.birthCountry}"/>
    </div>
</li>

<li class="nonUsCitizen">
    <div class="left">
        <label class="desc"><fmt:message key="user.priorCountry"/> </label>
        <recruiting:country name="priorCountry" prompt="" default="${user.priorCountry}"/>
    </div>
    <div>
        &nbsp;
    </div>
</li>

<li class="nonUsCitizen">
    <div class="left">
        <label class="desc"><fmt:message key="user.priorCountryStartDate"/> </label>
        <brs:dateFields
        prefix="priorCountryStartDate"
        year="${user.priorCountryStartDate.year+1900}"
        month="${user.priorCountryStartDate.month}"
        day="${user.priorCountryStartDate.date}"/>
    </div>
    <div>
        <label class="desc"><fmt:message key="user.priorCountryEndDate"/> </label>
        <brs:dateFields
        prefix="priorCountryEndDate"
        year="${user.priorCountryEndDate.year+1900}"
        month="${user.priorCountryEndDate.month}"
        day="${user.priorCountryEndDate.date}"/>
    </div>

</li>

<li class="nonUsCitizen">
    <div class="left">
        <label class="desc"><fmt:message key="user.firstUsEntryDate"/> </label>
        <brs:dateFields
        prefix="firstUsEntryDate"
        year="${user.firstUsEntryDate.year+1900}"
        month="${user.firstUsEntryDate.month}"
        day="${user.firstUsEntryDate.date}"/>
    </div>
    <div>
        <label class="desc"><fmt:message key="user.currentUsEntryDate"/> </label>
        <brs:dateFields
        prefix="currentUsEntryDate"
        year="${user.currentUsEntryDate.year+1900}"
        month="${user.currentUsEntryDate.month}"
        day="${user.currentUsEntryDate.date}"/>
    </div>
</li>

<li class="nonUsCitizen">
    <div class="left">
        <label class="desc"><fmt:message key="user.i94ExpirationDS"/> </label>
        <form:radiobutton path="i94ExpirationDS" value="true"/>D/S or
        <form:radiobutton path="i94ExpirationDS" value="false"/>Date
    </div>
    <div>
        <label class="desc"><fmt:message key="user.i94ExpirationDate"/> </label>
        <brs:dateFields
        prefix="i94ExpirationDate"
        year="${user.i94ExpirationDate.year+1900}"
        month="${user.i94ExpirationDate.month}"
        day="${user.i94ExpirationDate.date}"/>
    </div>
</li>

<li class="nonUsCitizen">
    <div class="left">
        <label class="desc"><fmt:message key="user.passportNumber"/> </label>
        <form:input path="passportNumber"/>
    </div>
    <div class="left">
        <label class="desc"><fmt:message key="user.passportCountryOfIssue"/> </label>
        <recruiting:country name="passportCountryOfIssue" prompt="" default="${user.passportCountryOfIssue}"/>
    </div>
    <div>
        <label class="desc"><fmt:message key="user.passportExpirationDate"/> </label>
        <brs:dateFields
        prefix="passportExpirationDate"
        year="${user.passportExpirationDate.year+1900}"
        month="${user.passportExpirationDate.month}"
        day="${user.passportExpirationDate.date}"/>
    </div>

</li>

<li class="nonUsCitizen">
    <div class="left">
        <label class="desc"><fmt:message key="user.visaType"/></label>
        <form:radiobutton path="visaType" value="B-1"/>B-1
        <form:radiobutton path="visaType" value="B-2"/>B-2
        <form:radiobutton path="visaType" value="J-1"/>J-1
        <form:radiobutton path="visaType" value="F-1"/>F-1
        <form:radiobutton path="visaType" value="WB/WT"/>WB/WT
        <form:radiobutton path="visaType" value="Other"/>Other
    </div>
    <div>
        <label class="desc"><fmt:message key="user.visaTypeOther"/></label>
        <form:input path="visaTypeOther"/>
    </div>
</li>

<li class="nonUsCitizen">
    <div class="left">
        <label class="desc"><fmt:message key="user.visaExpirationDate"/></label>
        <brs:dateFields
        prefix="visaExpirationDate"
        year="${user.visaExpirationDate.year+1900}"
        month="${user.visaExpirationDate.month}"
        day="${user.visaExpirationDate.date}"/>
    </div>
    <div>
        <label class="desc"><fmt:message key="user.jvisaCategory"/></label>
        <form:input path="jvisaCategory"/>
    </div>
</li>

<li class="nonUsCitizen">
    <label class="desc"><fmt:message key="user.travelTableInstructions"/></label>
    <label class="desc"><fmt:message key="user.visaPriorTo7Years"/></label>
    <form:checkbox path="visaPriorTo7Years"/>

    <table style="display: none">
        <!-- This hidden row will be copied to create the actual rows (see user.js) -->
        <tr id="hiddenPrototypeTravelRow">
            <td><button type="button">Remove</td>
            <td>
                <brs:dateFields
                prefix="arrivalDates"
                year=""
                month="0"
                day="1"/>
            </td>
            <td>
                <brs:dateFields
                prefix="departureDates"
                year=""
                month="0"
                day="1"/>
            </td>
            <td>
                <input type="text" name="visaTypes" value="" size="4" />
            </td>
            <td>
                <input type="text" name="purposes" value="" />
            </td>
        </tr>
    </table>

    <table id="travelTable">
        <thead>
            <tr>
                <th></th>
                <th>Arrival Date</th>
                <th>Departure Date</th>
                <th>Visa Type (ex: J-1, TN, B-1, etc.)</th>
                <th>Purpose of presence in U.S. (ex: study, tourist, conference, etc.)</th>
            </tr>
        </thead>
        <tbody>
            <!-- Populate with existing travel rows -->
            <c:if test="${(user != null) && (user.arrivalDates != null) && (not empty user.arrivalDates)}">
                <c:forEach begin="0" end="${fn:length(user.arrivalDates)-1}" varStatus="status">
                    <tr>
                        <td><button type="button">Remove</td>
                        <td>
                            <brs:dateFields
                            prefix="arrivalDates"
                            year="${user.arrivalDates[status.index].year+1900}"
                            month="${user.arrivalDates[status.index].month}"
                            day="${user.arrivalDates[status.index].date}"/>
                        </td>
                        <td>
                            <brs:dateFields
                            prefix="departureDates"
                            year="${user.departureDates[status.index].year+1900}"
                            month="${user.departureDates[status.index].month}"
                            day="${user.departureDates[status.index].date}"/>
                        </td>
                        <td>
                            <input type="text" name="visaTypes" value="${user.visaTypes[status.index]}" size="4" />
                        </td>
                        <td>
                            <input type="text" name="purposes" value="${user.purposes[status.index]}" />
                        </td>
                    </tr>
                </c:forEach>
            </c:if>
        </tbody>
    </table>
    <br/>
    <button type="button" id="travelTableAddRowButton">Add Row</button>
</li>
