#!/usr/bin/env python

"""
dump-database-to-csv.py
Dumps each table in the given MySQL database to a CSV file in the given output
directory.
Ben Saylor
February 2012
"""

import sys
import os.path
import MySQLdb
import csv

if len(sys.argv) != 6:
    print "Args: host user password database output_dir"
    exit()

host = sys.argv[1]
user = sys.argv[2]
password = sys.argv[3]
database = sys.argv[4]
output_dir = sys.argv[5]

db = MySQLdb.connect(host, user, password, database)
cursor = db.cursor()

# Get tables in DB
cursor.execute('show tables')
tables = [row[0] for row in cursor.fetchall()]

# Dump each table
for table in tables:
    f = open(os.path.join(output_dir, table + '.csv'), 'wb')
    writer = csv.writer(f)

    # Get columns, casting bit(1) columns to integers
    cursor.execute('show columns in ' + table)
    results = cursor.fetchall()
    header = [row[0] for row in results]
    columns = []
    for column_def in results:
        if column_def[1] == 'bit(1)':
            columns.append('cast(' + column_def[0] + ' as unsigned)')
        else:
            columns.append(column_def[0])

    # Write header row
    writer.writerow(header)

    # Write the data rows
    cursor.execute('select ' + ','.join(columns) + ' from ' + table)
    for row in cursor:
        writer.writerow(row)

    f.close()
