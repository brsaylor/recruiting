<html>
<body style="font-size: 14pt;">

<p>
The UAA Experimental Economics Lab invites you to participate in this paid
decision-making exercise:
</p>

<p style="text-align: center;">
$dateTool.format('EEEE, MMMM d, yyyy', ${exp.expDate})
</p>

<p style="text-align: center;">
@ $dateTool.format('h:mm a', ${exp.startTime}) - $dateTool.format('h:mm a', ${exp.endTime})
</p>

<p style="text-align: center;">
LOCATION:  ${exp.location.name}, ${exp.location.address}
</p>

<p style="text-align: center;">
No prior economics training is required. You must be at least 18 years
of age.
</p>

<p style="text-align: center;">
<b>go here to register:</b>
<a href="http://econlab.uaa.alaska.edu/recruiting/login/" target="_blank"
rel="nofollow">
<span style="color: blue;">http://econlab.uaa.alaska.edu/recruiting/login/</span>
</a>
</p>

<p>
The experiment can last up to ${duration}.
#if ($showUpFee != "$0.00")
You will be paid ${showUpFee} if you show up <u>on time</u>.
You will also be given an opportunity to earn additional
#else
You will be given an opportunity to earn 
#end
money based on your decisions and the decisions of others who are in
the experiment. You cannot lose money in the experiment. 
</p>

<p>
<b>EVERYONE</b> will be paid in cash at the conclusion of the experiment. If you
are not a US citizen or resident alien, you will need to enter additional
information on your profile page (see the information at the end of this
message) but you will be paid in cash after the experiment.
</p>

<p>
There are a limited number of seats available for each experiment. To sign-up
for an experiment please click the link below or paste it in to your browser.
</p>
 
<p>
<a href="http://econlab.uaa.alaska.edu/recruiting/login/" target="_blank"
rel="nofollow">
<span style="color: blue;">http://econlab.uaa.alaska.edu/recruiting/login/</span>
</a>
</p>

<p>
<b>NEW DATABASE</b>
</p>
<p style="margin-left: 4em;">
We are using a new system for experiment registration. If you decide to sign-up for this
experiment just follow the instructions at the webpage in order to log-in and register 
for the experiment. Please email us at
<a href="mailto:econlab@uaa.alaska.edu">econlab@uaa.alaska.edu</a>,
or call us at 786-4033,
if you have any problems registering.
</p>

<p>
<b>PLEASE SIGN UP ONLY IF YOU PLAN ON ATTENDING</b>.
</p>
<p style="margin-left: 4em;">
If you agree to participate, but then fail to show up or to arrive on time, we
may have to cancel the experiment. This costs us a lot of time and money, and
also creates an inconvenience for the other people who agreed to participate. If
your plans change and you cannot attend the  experiment you registered for,
please email us asap at
<a href="mailto:econlab@uaa.alaska.edu">econlab@uaa.alaska.edu</a>,
call us at 786-4033,
or log-in to the database and withdraw from the experiment by going to your
profile page.
</p>

<p style="margin-left: 4em;">
<b>Subjects who consistently fail to show up without prior notice will be
removed from the list of eligible participants.</b>  We will be running
many experiments this semester, so if you cannot make this session,
there will be plenty more opportunities. Thank you for your understanding.
</p>

<p>
<u><i><b>Information if you are NOT a U.S. citizen or resident
alien:</i></u></b>
</p>

<p style="margin-left: 4em;">
Please note this important change - <b>we are now able to pay you in cash
immediately after the experiment</b> if you have supplied the information below.
</p>

<p style="margin-left: 4em;">
Once a year, students who are NOT U.S. Citizens or resident aliens and
participate in experiments are required to:
</p>

<ul style="margin-left: 4em;">
<li>
  Complete the information on your User Profile page by logging in at
  <a href="http://econlab.uaa.alaska.edu/recruiting/login/" target="_blank"
rel="nofollow">
<span style="color: blue;">http://econlab.uaa.alaska.edu/recruiting/login/</span>
</a>
  clicking on "Subject Menu", clicking on Edit My Profile, and completing all
  requested information.
</li>
<li>
  Allow us to make a copy of your passport and visa.
</li>
</ul>

<p style="margin-left: 4em;">
The UAA experimental economics lab will be paying the tax associated
with your earnings (this allows us to pay you in cash at the end of the
experiment). Nevertheless, in order to be paid in cash you must complete
the required information in your user profile.
</p>

<p>
<u><i><b>Other Information:</i></u></b>
</p>

<p style="margin-left: 4em;">
If you have any questions please email us at
<a href="mailto:econlab@uaa.alaska.edu">econlab@uaa.alaska.edu</a>.
</p>

<p style="margin-left: 4em;">
To learn more about research at the UAA experimental economics lab please go to
our website.
<a href="http://econlab.uaa.alaska.edu/" target="_blank"
rel="nofollow">
<span style="color: blue;"> http://econlab.uaa.alaska.edu </span>
</a>
</p>

<p style="margin-left: 4em;">
If you have any questions about experimental economics or the lab please reply
to this email or contact the lab director, Lance Howe, at 786-5409.
</p>

<p>
You received this email because you signed up for the Experimental Economics Lab
email list. If you would like to be removed from this mailing list please
e-mail <a href="mailto:econlab@uaa.alaska.edu">econlab@uaa.alaska.edu</a>.
</p>

</body>
</html>
