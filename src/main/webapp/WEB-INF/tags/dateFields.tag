<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@ attribute name="prefix" required="true" %>
<%@ attribute name="month" required="false" %>
<%@ attribute name="day" required="false" %>
<%@ attribute name="year" required="false" %>


<span style="white-space: nowrap">
	<select class="dateFieldMonth" name="${prefix}_month">
	    <c:forEach begin="0" end="11" varStatus="status">
		<option value="${status.index}"
			<c:if test="${month==status.index}">
				selected
			</c:if>
		>
		<c:out value="${monthList[status.index].label}"/></option>
	    </c:forEach>
	</select>
	<select class="dateFieldDay" name="${prefix}_day">
	    <c:forEach begin="1" end="31" varStatus="status">
		<option value="${status.index}"
			<c:if test="${day==status.index}">
				selected
			</c:if>
		>
		<c:out value="${status.index}"/></option>
	    </c:forEach>
	</select>
	<input type="text" name="${prefix}_year" class="text small"
		value="${year}" />
</span>
