<%@ include file="/common/taglibs.jsp"%>

<head>
    <title>What's New</title>
    <meta name="heading" content="What's New"/>
    <link rel="stylesheet" type="text/css" media="all" href="<c:url value='/styles/${appConfig["csstheme"]}/layout-1col-left.css'/>" />
    <style type="text/css">
        ul {
            width: 40em;
        }
        li {
            margin-bottom: 1em;
        }
        h2 {
            font-weight: bold;
        }
        p {
            width: 50em;
        }
    </style>
</head>

<p>
We've made several major revisions to our website that we think you'll like...
</p>

<ul>
    <li>
    No more having to fill out payment forms in the lab.  Once your information is in the database, we will print out payment forms on the day of the experiment - all you need to do is sign.
    </li>

    <li>
    You can revise your personal information at any time by logging in and clicking on "Edit My Profile" in the Subject Menu; also, your subject profile contains a history of experiments you've participated in, your earnings, and the associated consent form (if you are interested).
    </li>

    <li>
    If you'd like to receive text message reminders for experiments, you can provide a cell number in your Preferences and we'll send you text reminders. 
    </li>

    <li>
    If your situation changes and you'd no longer like to receive experiment announcements, you can easily opt-out in your subject profile.  
    </li>

    <li>
    When you log-in to the database, you'll see a list of all the upcoming experiments for which you are eligible. 
    </li>

    <li>
    When signing up to participate in a new experiment, you'll be asked to read the consent form online.  If you agree to participate, you don't need to provide consent again on the day of the experiment. 
    </li>
</ul>

<h2>Informed Consent Information</h2>

<p>
Previously you agreed to allow your information to be stored in the UAA
Experimental Economics Laboratory database. With this new data management
system, the information you provide is still securely stored and will be used
exclusively by the experimental economics lab. You may remove yourself from
future experiment notifications at any time by emailing
<a href="mailto:econlab@uaa.alaska.edu">econlab@uaa.alaska.edu</a>
or by selecting this option on the "Edit My Profile" page.
</p>

<p>
Something new... when signing up for any experiment you will receive information
about the experiment describing any potential risks or benefits of the study (in
the past you received this information on the day of the experiment). If still
want to participate, you will be asked to provide electronic consent online,
then you will be registered to participate in that experiment. On the day of the
experiment when you come to the lab, you will be reminded of your consent and
will be provided a paper copy of the consent form if you would like one.
Remember, your participation in any experiment is always voluntary and deception
is never used in the UAA Experimental Economics Laboratory.
</p>
