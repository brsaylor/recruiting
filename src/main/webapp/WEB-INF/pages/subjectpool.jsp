<%@ include file="/common/taglibs.jsp"%>

<head> 
    <title><fmt:message key="subjectPoolList.title"/></title> 
    <meta name="heading" content="<fmt:message key='subjectPoolList.heading'/>"/> 
    <meta name="menu" content="AdminMenu"/>
</head> 

<c:set var="buttons"> 
    <input type="button" style="margin-right: 5px" 
           onclick="location.href='<c:url value="/subjectpoolform.html"/>'" 
           value="<fmt:message key="button.add"/>"/> 
    
    <input type="button" onclick="location.href='<c:url value="/administrator.html"/>'" 
           value="<fmt:message key="button.done"/>"/> 
</c:set> 

<c:out value="${buttons}" escapeXml="false"/> 

<%--Functionality comes from the "name" attribute of the table--%>

<display:table name="subjectPoolList" cellspacing="0" cellpadding="0" requestURI="" 
               id="subjectPoolList" pagesize="25" class="table subjectPoolList" export="true"> 
    
    <display:column url="/subjectpoolform.html" property="name" paramId="id" paramProperty="id" escapeXml="true" sortable="true" titleKey="subjectPool.name"/> 
    <display:column property="descr" escapeXml="true" sortable="true" titleKey="subjectPool.descr"/> 
    <display:column escapeXml="true" sortable="true" titleKey="subjectPool.valid"><c:out value="${subjectPoolList.valid ? 'Yes' : 'No'}"/></display:column>
    <display:column sortProperty="regDate" escapeXml="true" sortable="true" titleKey="subjectPool.regDate"><fmt:formatDate value="${subjectPoolList.regDate}" pattern="MM/dd/yyyy"/></display:column>
    <display:column title="Signup code"><c:out value="${subjectPoolList.signupCode}"/></display:column>
    
    <display:setProperty name="paging.banner.item_name" value="subject pool"/> 
    <display:setProperty name="paging.banner.items_name" value="subject pools"/>
    
    <display:setProperty name="export.excel.filename" value="Subject Pool List.xls"/> 
    <display:setProperty name="export.csv.filename" value="Subject Pool List.csv"/> 
    <display:setProperty name="export.pdf.filename" value="Subject Pool List.pdf"/> 
</display:table> 

<c:out value="${buttons}" escapeXml="false"/> 

<script type="text/javascript"> 
    highlightTableRows("subjectPoolList"); 
</script> 
