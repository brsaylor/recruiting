<%@ include file="/common/taglibs.jsp"%> 
<%@ taglib prefix="mylib" uri="/WEB-INF/mylib.tld" %>

<head>
    <title><fmt:message key="survey.title"/></title>
    <meta name="heading" content="<fmt:message key='survey.heading'/>"/>
    <style type="text/css">
        #signupButton {
            font-weight: bold;
        }
    </style>
    <script type="text/javascript" src="<c:url value='/scripts/common.js'/>"></script>

    <%-- BRS 2012-03-20 - moving consent/signup buttons to surveyform when there is a survey --%>
    <script type="text/javascript" src="<c:url value='/scripts/jquery.js'/>"></script>
    <script type='text/javascript'>
        var J = jQuery.noConflict();

        J(document).ready(function() {
            J("[name=consent]").change(function() {
                if (J(this).val() == "false") {
                    J("#signupButton").attr("disabled",  "disabled");
                } else {
                    J("#signupButton").removeAttr("disabled");
                }
            });
        });
    </script>
</head>

<% pageContext.setAttribute("newLineChar", "\n"); %> 
            <c:out value="${fn:replace(introText, newLineChar, '<br/>')}" escapeXml="false" />
            <br/><br/>

<c:set var="qstring" value="question"/>

<form:form commandName="questionAndAnswerList" method="post" action="surveyform.html" id="qal" onsubmit="return onFormSubmit(this)"> 
    <form:hidden path="expId"/>
    <%--<form:hidden path="questions"/>
    <form:hidden path="answers"/>--%>
    <c:set var="i" value="0"/>
    <c:set var="j" value="0"/>
    <c:forEach items="${questionAndAnswerList.questions}" var="question">
        <%--Only display questions that are not in the profile--%>
        <c:if test="${not questionAndAnswerList.inProfile[i]}">
            <c:set var="myq" value="${question}" scope="request"/>
            <c:import url="/WEB-INF/pages/displayquestion.jsp">
                <c:param name="qnum" value="${j+1}"/>
                <c:param name="index" value="${i}"/>
                <c:param name="answer" value="${questionAndAnswerList.answers[j]}"/>
            </c:import>
            
            <%--<c:out value="${j+1}.  ${question.question}"/>
            <c:set var="k" value="0"/>
            <c:if test="${question.style == 'DROP_LIST'}">
                <ul>
                    <li>
                        <select id="${qstring}${i}" name="${qstring}${i}">
                            <c:forEach items="${question.optionList}" var="option">
                                <c:choose>
                                    <c:when test="${questionAndAnswerList.answers[j] == k}">
                                        <option selected id="${qstring}${i},${k}" value="${k}"><c:out value="${option}"/></option>
                                    </c:when>
                                    <c:otherwise>
                                        <option id="${qstring}${i},${k}" value="${k}"><c:out value="${option}"/></option>
                                    </c:otherwise>
                                </c:choose>
                                <c:set var="k" value="${k + 1}"/>
                            </c:forEach>
                        </select>
                    </li>
                </ul>
            </c:if>
            <c:if test="${question.style == 'RADIO'}">
                <ul>
                    <c:forEach items="${question.optionList}" var="option">
                        <li>
                            <c:choose>
                                <c:when test="${questionAndAnswerList.answers[j] == k}">
                                    <input checked type="radio" name="${qstring}${i}" value="${k}" id="${qstring}${i},${k}"/>
                                </c:when>
                                <c:otherwise>
                                    <input type="radio" name="${qstring}${i}" value="${k}" id="${qstring}${i},${k}"/>
                                </c:otherwise>
                            </c:choose>
                            <label for="${qstring}${i},${k}"><c:out value="${option}"/></label> 
                            <c:set var="k" value="${k + 1}"/>
                        </li>
                    </c:forEach>
                </ul>
            </c:if>
            <c:if test="${question.style == 'CHECK_BOX'}">
                <ul>
                    <c:forEach items="${question.optionList}" var="option">
                        <li>
                            <c:choose>
                                <%-- mylib:hasDigit tests whether an integer has 1 in binary digit 2^k --%>
            <%--<c:when test="${mylib:hasDigit(questionAndAnswerList.answers[j], k)}"> 
                                    <input checked type="checkbox" name="${qstring}${i},${k}" id="${qstring}${i},${k}"/>
                                </c:when>
                                <c:otherwise>
                                    <input type="checkbox" name="${qstring}${i},${k}" id="${qstring}${i},${k}"/>
                                </c:otherwise>
                            </c:choose>
                            <label for="${qstring}${i},${k}"><c:out value="${option}"/></label>
                            <c:set var="k" value="${k + 1}"/>
                        </li>
                    </c:forEach>
                </ul>
            </c:if>--%>
            <c:set var="j" value="${j + 1}"/>
        </c:if>
        <c:set var="i" value="${i+1}"/>
    </c:forEach> 

    <h2><b>Informed Consent Information</b></h2>
    <div>
        <c:out value="${fn:replace(experiment.type.consentText, newLineChar, '<br/>')}" escapeXml="false" />
    </div>
    <br/>
    <br/>
    
    <input type="radio" name="consent" value="true"/>
    I consent
    <br/>
    <input type="radio" name="consent" value="false" checked />
    I do not consent
    <br/><br/>
    <c:set var="okButtonText" value="Sign Up"/>
        <input type="submit" style="margin-right: 5px" disabled="disabled" id="signupButton" value="${okButtonText}"/>
    or
    <input type="button" style="margin-right: 5px"
        onclick="location.href='<c:url value="/calendar.html"/>'"
        value="Back to Calendar"/>
    
</form:form>    

<%--
<script type="text/javascript">
function onFormSubmit(theForm, len) {
    if(bCancel == true) {
        return true;
    }
    alert("This is a test of the emergency broadcast system\nSo beware of that");
    valid = false;
    return valid;
}
</script>
--%>
