<%@ include file="/common/taglibs.jsp"%>
<h3 class="${param.offset_class}" id="tripick_head_${param.name}"><c:out value="${param.eligText}"/></h3>

<table>
    <tr align="center">
        <td>
            <h3 class="${param.offset_class}">...at least one of these <c:out value="${param.name}"/></h3>
            <select id="select_in${param.num}" name="select_in${param.num}" multiple="true" size="3" class="standardListSelect">
            </select>
        </td>
        <td class="tripick_buttons bottom">
            <input type="button" value="<<" onclick="moveSelectedOptions($('avail${param.num}'), $('select_in${param.num}'))"/>
            <br>
            <input type="button" value=">>" onclick="moveSelectedOptions($('select_in${param.num}'), $('avail${param.num}'))"/>
        </td>
        <td rowspan="3" class="tripick_padding"></td>
        <td rowspan="3" valign="bottom">
            Available <c:out value="${param.name}"/>
            <br><br>
            <select id="avail${param.num}" multiple="true" size="9" class="standardListSelect">
                <c:forEach items="${availList}" var="lvpair">
                    <option value="${lvpair.value}"><c:out value="${lvpair.label}"/></option>
                </c:forEach>
            </select>
        </td>
    </tr>
    <tr align="center">
        <td>
            <h3><em>AND</em></h3>
        </td>
        <td>
        </td>
    </tr>
    <tr align="center">
        <td>
            <h3 class="${param.offset_class}">...none of these <c:out value="${param.name}"/></h3>
            <select id="select_out${param.num}" name="select_out${param.num}" multiple="true" size="3" class="standardListSelect">
            </select>
        </td>
        <td class="tripick_buttons">
            <input type="button" value="<<" onclick="moveSelectedOptions($('avail${param.num}'), $('select_out${param.num}'))"/>
            <br>
            <input type="button" value=">>" onclick="moveSelectedOptions($('select_out${param.num}'), $('avail${param.num}'))"/>
        </td>
    </tr>
</table>