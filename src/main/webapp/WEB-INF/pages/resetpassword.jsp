<%@ include file="/common/taglibs.jsp"%>

<head>
    <title><fmt:message key="resetPassword.title"/></title>
    <meta name="heading" content="<fmt:message key='resetPassword.heading'/>"/>
    <script type="text/javascript" src="<c:url value='/scripts/common.js'/>"></script>
    <script type="text/javascript">
var dayCntList = new Array(12);
        
function formSubmit(theform) {
    return validateResetPassword(theform);
}

function monthChange() {
    var monthSel = $("birthday_month");
    var daySel = $("birthday_day");
    var num = dayCntList[1*monthSel.value];
    
    //Remove extra days at the end
    while(daySel.length > num) {
        daySel.remove(daySel.length-1);
    }
    
    //Add to the end
    while(daySel.length < num) {
        var opt = appendOption(daySel, daySel.length + 1);
        opt.value=opt.text;
    }
}
    </script>
</head>

<p>
    <fmt:message key="resetPassword.instructions"/>
</p>

<form:form commandName="user" method="post" action="resetpassword.html" onsubmit="return formSubmit(this);" id="resetPassword" name="resetPassword">
    <ul>
        <li>
            <div>
                <!--<div class="left">
                    <appfuse:label styleClass="desc" key="user.username"/>
                    <form:input path="username" id="username" cssClass="text medium"/>
                </div>-->
                <div>
                    <appfuse:label styleClass="desc" key="user.email"/>
                    <form:errors path="email" cssClass="fieldError"/>
                    <form:input path="email" id="email" cssClass="text medium"/>
                </div>
                <div>
                    <appfuse:label styleClass="desc" key="user.schoolId"/>
                    <form:errors path="schoolId" cssClass="fieldError"/>
                    <form:input path="schoolId" id="schoolId" cssClass="text medium"/>
                </div>
            </div>
        </li>
        <li>
            <div class="left">
                <appfuse:label styleClass="desc" key="user.firstName"/>
                <form:errors path="firstName" cssClass="fieldError"/>
                <form:input path="firstName" id="firstName" cssClass="text medium" maxlength="50"/>
            </div>
            <div>
                <appfuse:label styleClass="desc" key="user.lastName"/>
                <form:errors path="lastName" cssClass="fieldError"/>
                <form:input path="lastName" id="lastName" cssClass="text medium" maxlength="50"/>
            </div>
        </li>

        <li class="buttonBar bottom">
            <input type="submit" class="button" name="submit" value="<fmt:message key='button.submit'/>" onclick="bCancel=false;"/>
            <input type="submit" class="button" name="cancel" onclick="bCancel=true;" value="<fmt:message key='button.cancel'/>"/>
        </li>
    </ul>
</form:form>

<script type="text/javascript">
    Form.focusFirstElement($('resetPassword'));
    highlightFormElements();
</script>

<v:javascript formName="resetPassword" staticJavascript="false"/>
<script type="text/javascript" src="<c:url value='/scripts/validator.jsp'/>"></script>
