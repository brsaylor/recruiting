<%@ include file="/common/taglibs.jsp"%>
<head>
    <title><fmt:message key="importC2.title"/></title>
    <meta name="heading" content="<fmt:message key='importC2.heading'/>"/>
    <meta name="menu" content="AdminMenu"/>
    
    <script type='text/javascript'>
    function formSubmit(theform) {
        var proceed = true;
        if(!bCancel) {
            var hostname = $("hostname_field").value;
            var port = $("port_field").value;
            if(port.length == 0)
                port = "3306";
            var c2name = $("c2name_field").value;
            proceed = confirm("Are you sure you want to import data from database: "+hostname+":"+port+"/"+c2name+"?");
        }
        
        if(!proceed || !validateImportC2(theform)) {
            return false;
        }
    }
    </script>
</head>

<h2>Prerequisites</h2>
<p>
To work correctly, the import procedure requires the following conditions to be true:
</p>
<ol>
    <li>The id field in both the CasselWeb2 experiments table and the experiment table in the current database
        starts at 1 and doesn't skip numbers (recruiting.experiment can be empty).</li>
    <li>The current value of the auto_increment in recruiting.experiment won't create a skip when a new record is inserted.</li>
    <li>In the CasselWeb2 experimenters and subjects tables, there are no duplicate email addresses
        (an email address in one of the tables must not be present in the other).</li>
</ol>
<p><b>Warning:</b> experiments existing in the current database will be overwritten in order to preserve the old ID numbers.</p>

<c:set var="importer" value="${applicationScope['importc2']}"/>
<form:form commandName="importC2Utility" method="post" action="importc2form.html" id="importC2Form" onsubmit="return formSubmit(this)"> 
    <appfuse:label key="importC2.targetinfo" styleClass="desc" colon="false"/>
    <appfuse:label key="importC2.url" styleClass="desc" colon="false"/><font style="font-size: 11pt; font-weight: bold">jdbc:mysql://</font> <input type="text" class="medium" name="hostname" id="hostname_field"/> <font style="font-size: 11pt; font-weight: bold">:</font> <input type="text" class="small" name="port" id="port_field"/> <font style="font-size: 11pt; font-weight: bold">/</font> <input type="text" class="medium" name="c2name" id="c2name_field"/>
    <appfuse:label key="importC2.username" styleClass="desc" colon="false"/><input type="text" class="medium" name="username"/>
    <appfuse:label key="importC2.password" styleClass="desc" colon="false"/><input type="text" class="medium" name="password"/>
    <hr/>
    <input type="submit" class="button" name="import" value="<fmt:message key='button.import'/>" onclick="bCancel=false;"/>&nbsp;
    <input type="submit" class="button" name="cancel" value="<fmt:message key='button.cancel'/>" onclick="bCancel=true;"/>&nbsp;
    <input type="submit" class="button" name="quit" style="width: 8em;" value="<fmt:message key='button.stopimport'/>" onclick="bCancel=true;"/> 
</form:form>
<c:if test="${importer != null}">
    <c:if test="${importer.ran}">
        <h1><fmt:message key='importC2.results'/></h1>
        <ul class="glassList">
            <li>Import process is: <c:choose><c:when test="${importer.done}"><font style="color: red">Stopped</font></c:when><c:otherwise><font style="color: green">Running</font></c:otherwise></c:choose></li>
            <li><b>Experimenters</b> Imported: ${importer.experimenterCount},&nbsp;&nbsp;Errors occurring: ${importer.experimenterErrorCount}</li>
            <li><b>Subjects</b> Imported: ${importer.subjectCount},&nbsp;&nbsp;Errors occurring: ${importer.subjectErrorCount}</li>
            <li><b>Experiments</b> Imported: ${importer.experimentCount},&nbsp;&nbsp;Errors occurring: ${importer.experimentErrorCount}</li>
            <li><b>Experiment Types</b> Imported: ${importer.experimentTypeCount},&nbsp;&nbsp;Errors occurring: ${importer.experimentTypeErrorCount}</li>
            <li><b>Participant Records</b> Imported: ${importer.participantRecordCount},&nbsp;&nbsp;Errors occurring: ${importer.participantRecordErrorCount}</li>
        </ul>
        <appfuse:label key="importC2.otherstats" styleClass="desc" colon="false"/>
        <ul>
            <li>Average <b>Experimenter</b> Creation Time: ${importer.avgExperimenterCreationTime} (ms) | time left: ${importer.experimenters2ImportTimeLeft} (min)</li>
            <li>Average <b>Subject</b> Creation Time: ${importer.avgSubjectCreationTime} (ms) | time left: ${importer.subjects2ImportTimeLeft} (min)</li>
            <li>Average <b>Experiment</b> Creation Time: ${importer.avgExpCreationTime} (ms) | time left: ${importer.experiments2ImportTimeLeft} (min)</li>
            <li>Average <b>ParticipantRecord</b> Creation Time: ${importer.avgPrecCreationTime} (ms) | time left: ${importer.participantRecords2ImportTimeLeft} (min)</li>
        </ul>
    </c:if>
</c:if>
<v:javascript formName="importC2" cdata="false" dynamicJavascript="true" staticJavascript="false"/>
<script type="text/javascript" src="<c:url value='/scripts/validator.jsp'/>"></script>

<script type="text/javascript">
    Form.focusFirstElement($('importC2Form'));
</script>
