<%@ include file="/common/taglibs.jsp"%>
<%@ taglib  uri="/WEB-INF/calendartag.tld" prefix="calendartag"%>

<head>
    <title><fmt:message key="calendar.title"/></title>
    <link rel="stylesheet" type="text/css" href="<c:url value='/styles/${appConfig["csstheme"]}/calendar.css'/>" />
    <link rel="stylesheet" type="text/css" media="all" href="<c:url value='/styles/${appConfig["csstheme"]}/layout-1col-left.css'/>" />
    <meta name="menu" content="CalendarMenu"/>
    <%--<meta name="heading" content="<fmt:message key='calendar.heading'/>"/>--%>
    <script type="text/javascript" src="<c:url value='/scripts/common.js'/>"></script>
    <script type='text/javascript' src='<c:url value="/dwr/engine.js"/>'></script>
    <script type='text/javascript' src='<c:url value="/dwr/util.js"/>'></script>
    <script type='text/javascript' src='<c:url value="/dwr/interface/experimentManagerDWR.js"/>'></script>
    <script type="text/javascript" src="<c:url value='/scripts/overlib/Mini/overlib_mini.js'/>">
        <!-- load Erik Bosrup''s overLIB library -->
    </script>
    <script type="text/javascript">
        // DWR callback
        // use overlib to display experiment details
        function exptooltip(experiment) {
            var expDate = experiment.expDate.getMonth()+"/"+experiment.expDate.getDate()+"/"+experiment.expDate.getFullYear();
            var expBtime = formatTime(experiment.startTime);
            var expEtime = formatTime(experiment.endTime);
            
            // build a table for the overlib call
            var s = "<table border=\"1\" cellpadding=\"0\" cellspacing=\"0\">"+
                    "   <tr>"+
                    "   <tr><td nowrap>Exp ID: "+experiment.id+"</td><td align=\"left\" nowrap>Status: "+experiment.status.name+"</td></tr>"+
                    "   <tr>"+
                    "       <td width=\"20%\" align=\"center\" rowspan=\"4\"><img src=\"images/smallflask.gif\" alt=\"tinyflask\"/></td>"+
                    "       <td align=\"left\" nowrap>Type: "+experiment.type.name+"</td>"+
                    "   </tr>"+
                    "   <tr><td align=\"left\" nowrap>Date: "+expDate+"</td></tr>"+
                    "   <tr><td align=\"left\" nowrap>Time: "+expBtime+" to "+expEtime+"</td></tr>"+
                    "   <tr><td align=\"left\" nowrap>Location: "+experiment.location.name+"</td></tr>"+
                    "   <tr><td align=\"left\" colspan=\"2\">Instructions: "+experiment.instruction+"</td></tr>"+
                    "   <tr><td nowrap>Survey: "+hasSurvey(experiment)+"</td><td nowrap>Filter: "+hasFilter(experiment)+"</td></tr>"+
                    "   </tr><td colspan=\"2\">Run By: "+experiment.runby.firstName+" "+experiment.runby.lastName+"</td></tr>"+
                    "   </tr><td colspan=\"2\">Reserved By: "+experiment.reservedBy.firstName+" "+experiment.reservedBy.lastName+"</td></tr>"+
                    "   </tr><td colspan=\"2\">Principal: "+experiment.principle.firstName+" "+experiment.principle.lastName+"</td></tr>"+
                    "</table>";
            overlib(s);
        }
        function hasSurvey(exp) {
            if(exp.survey  == undefined)
                return "none";
            else
                return exp.survey.name;
        }
        function hasFilter(exp) {
            if(exp.filter  == undefined)
                return "none";
            else
                return exp.filter.name;
        }
        function formatTime(date) {
            var a_p = "";
            var curr_hour = date.getHours();

            if (curr_hour < 12)
               {
               a_p = "AM";
               }
            else
               {
               a_p = "PM";
               }
            if (curr_hour == 0)
               {
               curr_hour = 12;
               }
            if (curr_hour > 12)
               {
               curr_hour = curr_hour - 12;
               }

            var curr_min = date.getMinutes();
            if(curr_min < 9)
                curr_min = "0"+curr_min;

            return curr_hour + ":" + curr_min + a_p;
        }
        function parseType(type) {
            // FULL, CLOSED, OPEN, FINISHED
            if(type == 0)
                return "Full";
            else if(type == 1)
                return "Closed";
            else if(type == 2)
                return "Open";
            else if(type == 3)
                return "Finished";
        }
        function changeDate() {
            var newMonth = $("monthSelector").value;
            var newYear = $("yearSelector").value;
            var today = new Date();
            var currDay = today.getDate();
            if(currDay <= 10)
                currDay = "0"+currDay;
            window.location.href="calendar.html?date="+newMonth+"-"+currDay+"-"+newYear;
        }
    </script>
</head>

<%
// BRS: Ask user to log in to see which experiments they are eligible for.
if (request.getRemoteUser() == null) {
%>
<appfuse:label styleClass="desc" key="calendar.pleaseLogin"/>
<% } %>

<select id="monthSelector" name="monthSelector" onchange="changeDate()">
    <c:forEach items="${monthList}" var="month">
        <option value="${month.value}" <c:if test="${month.label == selectedMonth}">selected</c:if>>${month.label}</option>
    </c:forEach>
</select>
<select id="yearSelector" name="yearSelector" onchange="changeDate()">
    <c:forEach items="${yearList}" var="year">
        <option value="${year}" <c:if test="${year == selectedYear}">selected</c:if>>${year}</option>
    </c:forEach>
</select>
        
<%@ page import="java.util.*" %>

<!--
isExperimenter=${isExperimenter}
isAdmin=${isAdmin}
-->

<%
Date d = new Date();
String s = "";
String t;
t = Integer.toString(d.getMonth() + 1);
if(t.length() == 1)
    t = "0" + t;
s += t;
s += "-";
t = Integer.toString(d.getDate());
if(t.length() == 1)
    t = "0" + t;
s += t;
s += "-";
s += Integer.toString(d.getYear() + 1900);
%>
<c:set var="today" value="<%= s %>"/>
<c:set var="date" value="${(empty param.date) ? today : param.date}"/>


<calendartag:calendar dayHeight="80" dayWidth="130" requestURI="calendar.html" date="${date}" decorator="edu.caltech.ssel.recruiting.webapp.decorator.RecruitingCalendarDecorator"/>
<c:if test="${isExperimenter || isAdmin}">
    <appfuse:label key="calendar.linklegend" styleClass="desc" colon="false"/>
    <table cellpadding="0" cellspacing="1" border="0">
        <tr>
            <td style="background-color: #009700;" width="16px"></td><td>&nbsp;<fmt:message key='calendar.published'/></td><td>&nbsp;</td>
            <td style="background-color: #ff0000;" width="16px"></td><td>&nbsp;<fmt:message key='calendar.saved'/></td><td>&nbsp;</td>
            <td style="background-color: #cccccc;" width="16px"></td><td>&nbsp;<fmt:message key='calendar.invisible'/></td>
        </tr>
    </table>
</c:if>

<%-- BRS --%>
<c:if test="${isSubject}">
    <appfuse:label key="calendar.linklegend" styleClass="desc" colon="false"/>
    <table cellpadding="0" cellspacing="1" border="0">
        <tr>
            <td style="background-color: #00583D;" width="16px"></td><td>&nbsp;<fmt:message key='calendar.eligible'/></td><td>&nbsp;</td>
            <td style="background-color: gray;" width="16px"></td><td>&nbsp;<i><fmt:message key='calendar.ineligible'/></i></td><td>&nbsp;</td>
        </tr>
    </table>
</c:if>
