<%@ include file="/common/taglibs.jsp"%> 

<head>
    <title><fmt:message key="modifyexperiment.title"/></title>
    <meta name="heading" content="<fmt:message key='modifyexperiment.heading'/>"/>
    <meta name="menu" content="ExperimenterMenu"/>

    <script type="text/javascript" src="<c:url value='/scripts/common.js'/>"></script>
    <script type="text/javascript">
        function setupExp() {
            var selObj = $("myExperimentSelect");
            var optAry = selObj.options;
            var idx = selObj.selectedIndex;
            if(optAry[idx].value != "new" && optAry[idx].value != "") {
                document.location.href = "modifyexperiment.html?id="+optAry[idx].value;
            } else {
                alert("<fmt:message key="experiment.select.invalid"/>");
            }
        }

        function modifyExperiment() {
            document.location.href="experimentform.html";
        }
        function cloneExperiment() {
            var regex = new RegExp("[\\?&]id=([^&#]*)");
            var qs = regex.exec(window.location.search);
            var expId = qs[1];
            document.location.href="experimentform.html?clone="+expId;
        }
        function cancelViewExperiment() {
            document.location.href="clearsession.html?view=modifyExperiment";
        }
    </script>
</head>

<table style="vertical-align:center;text-align:center;" cellspacing="5">
    <tr>
        <td>
            <a href="calendar.html">Select From Calendar</a>
        </td>
        <td><i>OR</i></td>
        <td>
            <select name="myExperiment" id="myExperimentSelect" onchange="setupExp();">
                <option value="new"><fmt:message key="experiment.select.choose"/></option>
                <c:forEach items="${expList}" var="expItem">
                    <option value="${expItem.id}"<c:if test="${(experiment != null) and (expItem.id == experiment.id)}">selected</c:if>><c:out value="${expItem.formattedDescription}"/></option>
                </c:forEach>
            </select>
            <!--
            &nbsp;<input type="button" onclick="setupExp()" value="<fmt:message key='button.view'/>">
            -->
        </td>
    </tr>
</table>

<c:if test="${experiment != null}">
    <c:set var="myusername" value="${pageContext.request.remoteUser}"/>
    <!--myusername = ${myusername}-->

    <%-- BRS 2011-02-15 wrong now that PIs are in ExperimentType.
    <c:if test="${myusername != experiment.runby.username && myusername != experiment.reservedBy.username && myusername != experiment.principle.username}">
        <div class="message">
            <img src="/images/iconWarning.gif">
            Warning!  You are not registered as running, creating, or being Principal of this experiment.<br>
            Please do not modify this experiment unless you have the proper permissions. <br>
        </div>
    </c:if>
    --%>

    <appfuse:label key="modifyexperiment.experimentsummary" colon="true" styleClass="desc"/>
    <ul class="glassList">
        <li>
            Experiment #${experiment.id}, "${experiment.type.name}" type, Status: ${experiment.status.name}
        </li>
        <li>
            <fmt:formatDate value="${experiment.startTime}" pattern="h:mma"/> - <fmt:formatDate value="${experiment.endTime}" pattern="h:mma"/>, <fmt:formatDate value="${experiment.expDate}" pattern="EEE MM/dd/yyyy"/> at ${experiment.location.name}
        </li>
        <li>
            Run by: ${experiment.runby.fullName}, Reserved by: ${experiment.reservedBy.fullName}, Principal: ${experiment.type.principalInvestigatorsStr}
        </li>
        <li>
            Setup Buffer Time: ${experiment.setupBuffer} min, Cleanup Buffer Time: ${experiment.cleanupBuffer} min
        </li>
        <li>
            <c:choose>
                <c:when test="${experiment.survey != null}">
                    Using survey "${experiment.survey.name}"
                </c:when>
                <c:otherwise>
                    Not using a survey
                </c:otherwise>
            </c:choose>
        </li>
        <li>
            <c:choose>
                <c:when test="${experiment.filter != null}">
                    Using filter "${experiment.filter.name}"
                </c:when>
                <c:otherwise>
                    Not using a filter
                </c:otherwise>
            </c:choose>
        </li>
        <li>
            <c:choose>
                <c:when test="${experiment.published}"><div style="color: green">Published</div></c:when>
                <c:otherwise><div style="color: red">Not Published</div></c:otherwise>
            </c:choose>
        </li>
    </ul>
    <br>
    <c:if test="${writable == true}">
        
        <input type="button" value="<fmt:message key='button.modifyExperiment'/>" onclick="modifyExperiment();"/>
        <input type="button" value="<fmt:message key='button.clone'/>" onclick="cloneExperiment();"/>
        <input type="button" value="<fmt:message key='button.cancel'/>" onclick="cancelViewExperiment();"/>
        <br>
        <br>
        <a href="announcementform.html"><fmt:message key="modifyexperiment.email"/></a>
        <br>
        <br>
        <a href="participantrecord.html"><fmt:message key="modifyexperiment.records"/></a>
        <br>
        <br>
        <a href="question.html?id=${experiment.id}"><fmt:message key="modifyexperiment.questions"/></a>
        <br>
    </c:if>
    <br>

</c:if>
