<%@ include file="/common/taglibs.jsp"%> 
<%@ taglib prefix="mylib" uri="/WEB-INF/mylib.tld" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<head>
    <title><fmt:message key="experimentVerificationForm.title"/></title>
    <meta name="heading" content="<fmt:message key='experimentVerificationForm.heading'/>"/>
    <meta name="menu" content="ExperimenterMenu"/>
    
    <link rel="stylesheet" type="text/css" href="/styles/common.css" />
    <link rel="stylesheet" type="text/css" href="/styles/tripick.css" />
    
    <script type="text/javascript" src="<c:url value='/scripts/selectbox.js'/>"></script>
    <script type="text/javascript" src="<c:url value='/scripts/common.js'/>"></script>
</head>

<%@ include file="/common/progressDiv.jsp"%>

<form:form commandName="experiment" id="experimentVerificationForm" action="experimentverificationform.html">
    <c:set var="MAX_NUM_EMAILS" value="20"/>
    <c:set var="MAX_DISP_LENGTH" value="150"/>
    
    <ul>
        <li>
            <appfuse:label key="experimentVerificationForm.experimenters" colon="true" styleClass="desc"/>
            <fmt:message key="experimentVerificationForm.runBy"/> ${experiment.runby.fullName}, <fmt:message key="experimentVerificationForm.resBy"/> ${experiment.reservedBy.fullName}, <fmt:message key="experimentVerificationForm.principle"/>: ${experiment.principle.fullName}
        </li>
        <li>
            <appfuse:label key="experimentVerificationForm.timeAndLoc" colon="true" styleClass="desc"/>
            <fmt:formatDate value="${experiment.startTime}" pattern="hh:mma"/> - <fmt:formatDate value="${experiment.endTime}" pattern="hh:mma"/>, <fmt:formatDate value="${experiment.expDate}" pattern="EEE MM/dd/yyyy"/> at ${experiment.location.name}
        </li>
        <li>
            <appfuse:label key="experimentVerificationForm.numSubj" colon="true" styleClass="desc"/>
            ${experiment.minNumSubjects} - ${experiment.maxNumSubjects}
        </li>
        <li>
            <appfuse:label key="experimentVerificationForm.showUpFee" colon="true" styleClass="desc"/>
            <fmt:formatNumber type="currency" value="${experiment.showUpFee}" currencySymbol="$"/>
        </li>
        <li>
            <appfuse:label key="experimentVerificationForm.expType" colon="true" styleClass="desc"/>
            ${experiment.type.name}
        </li>
        <li>
            <appfuse:label key="experimentVerificationForm.signupInfo" colon="true" styleClass="desc"/>
            Experiment signup is now ${fn:toLowerCase(experiment.status)} and will freeze ${experiment.signupFreezeTime} minutes before the experiment
        </li>
        <c:if test="${experiment.survey != null}">
            <li>
                <appfuse:label key="experimentVerificationForm.survey" colon="true" styleClass="desc"/>
                <ul>
                    <li>
                        Name: ${experiment.survey.name}
                    </li>
                    <li>
                        Number of questions: ${surveySize}
                    </li>
                </ul>
            </li>
        </c:if>
        <c:if test="${experiment.filter != null}">
            <li>
                <appfuse:label key="experimentVerificationForm.filter" colon="true" styleClass="desc"/>
                <ul>
                    <li>
                        Name: ${experiment.filter.name}
                    </li>
                    <li>
                        Number of conditions: ${filterSize}
                    </li>
                </ul>
            </li>
        </c:if>
        <li>
            <input type="checkbox" name="makeAnnouncement" id="makeAnnouncement" /><label for="makeAnnouncement"> <fmt:message key="experiment.announcement"/></label>
        </li>
    </ul>
    
    <div class="buttonbar bottom">
        <input type="submit" class="button" name="back" value="<fmt:message key='button.back'/>"/> 
        <input type="submit" class="button" name="savenow" value="<fmt:message key='button.saveNow'/>" onclick="bCancel=false"/>
        <input type="submit" class="button" name="publish" value="<fmt:message key='button.publish'/>"/> 
        <input type="submit" class="button" name="cancel" value="<fmt:message key='button.cancel'/>"/> 
    </div>
</form:form>
