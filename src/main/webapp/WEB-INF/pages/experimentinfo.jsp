<%@ include file="/common/taglibs.jsp"%> 

<head>
    <title><fmt:message key="experimentinfo.title"/></title>
    <meta name="heading" content="<fmt:message key='experimentinfo.heading'/>"/>
    <meta name="menu" content="SubjectMenu"/>

    <style type="text/css">
        #signupButton {
            font-weight: bold;
        }
    </style>

    <script type="text/javascript" src="<c:url value='/scripts/common.js'/>"></script>

    <script type="text/javascript" src="<c:url value='/scripts/jquery.js'/>"></script>
    <script type='text/javascript'>
        var J = jQuery.noConflict();

        J(document).ready(function() {
            J("[name=consent]").change(function() {
                if (J(this).val() == "false") {
                    J("#signupButton").attr("disabled",  "disabled");
                } else {
                    J("#signupButton").removeAttr("disabled");
                }
            });
        });
    </script>
</head>

<c:if test="${experiment != null}">

<table class="spaced">
    <tr><td class="sideheader">Experiment ID</td><td>${experiment.id}</td></tr>
    <tr><td class="sideheader">Status</td><td>${experiment.status.name}</td></tr>
    <tr><td class="sideheader">Date</td><td><fmt:formatDate value="${experiment.expDate}" pattern="EEE MM/dd/yyyy"/></td></tr>
    <tr><td class="sideheader">Start Time</td><td><fmt:formatDate value="${experiment.startTime}" pattern="h:mm a"/></td></tr>
    <tr><td class="sideheader">End Time</td><td><fmt:formatDate value="${experiment.endTime}" pattern="h:mm a"/></td></tr>
    <tr><td class="sideheader">Show-up Payment</td><td><fmt:formatNumber type="currency" value="${experiment.showUpFee}" currencySymbol="$"/></td></tr>
    <tr><td class="sideheader">Instructions</td>
        <td>
            <% pageContext.setAttribute("newLineChar", "\n"); %> 
            <c:out value="${fn:replace(experiment.type.instruction, newLineChar, '<br/>')}" escapeXml="false" />
            <br/><br/>
            <c:out value="${fn:replace(experiment.instruction, newLineChar, '<br/>')}" escapeXml="false" />
        </td>
    </tr>
</table>

<table class="spaced">
    <tr><td class="sideheader">Location</td><td>${experiment.location.name}</td></tr>
    <tr><td class="sideheader">Address</td><td>${experiment.location.address}</td></tr>
    <tr><td class="sideheader">Map</td><td><a href="${experiment.location.mapLink}">View Map</a></td></tr>
    <tr><td class="sideheader">Contact</td><td><a href="mailto:${experiment.location.contactEmail}">${experiment.location.contact}</a></td></tr>
    <tr><td class="sideheader">Phone</td><td>${experiment.location.phone}</td></tr>
</table>


<div>
<c:choose>
    <c:when test="${isSubject && empty experiment.survey}">

        <h2><b>Informed Consent Information</b></h2>
        <div>
            <c:out value="${fn:replace(experiment.type.consentText, newLineChar, '<br/>')}" escapeXml="false" />
        </div>
        <br/>
        <br/>

        <input type="radio" name="consent" value="true"/>
        I consent
        <br/>
        <input type="radio" name="consent" value="false" checked />
        I do not consent
        <br/>
        <br/>

        <c:set var="okButtonText" value="Sign Up"/>
        <input type="button" style="margin-right: 5px" disabled="disabled" id="signupButton"
            onclick="location.href='<c:url value="/onexpsel.html?expId=${experiment.id}"/>'"
            value="${okButtonText}"/>
    </c:when>

    <c:when test="${isSubject && not empty experiment.survey}">
        <p>
        To sign up, please click the <b>Continue</b> button below to take the required questionnaire.
        </p>
        <c:set var="okButtonText" value="Continue"/>
        <input type="button" style="margin-right: 5px" id="signupButton"
            onclick="location.href='<c:url value="/onexpsel.html?expId=${experiment.id}"/>'"
            value="${okButtonText}"/>

            or
    </c:when>

    <c:otherwise>
        <c:set var="okButtonText" value="Select This Experiment"/>
        <input type="button" style="margin-right: 5px"
            onclick="location.href='<c:url value="/onexpsel.html?expId=${experiment.id}"/>'"
            value="${okButtonText}"/>
    </c:otherwise>
</c:choose>

    <input type="button" style="margin-right: 5px"
        onclick="location.href='<c:url value="/calendar.html"/>'"
        value="Back to Calendar"/>

</div>

</c:if>
