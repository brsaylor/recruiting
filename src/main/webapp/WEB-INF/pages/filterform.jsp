<%@ include file="/common/taglibs.jsp"%> 
<%@ taglib prefix="mylib" uri="/WEB-INF/mylib.tld" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<head>
    <title><fmt:message key="filterForm.title"/></title>
    <meta name="heading" content="<fmt:message key='filterForm.heading'/>"/>
    <meta name="menu" content="ExperimenterMenu"/>
    
    <link rel="stylesheet" type="text/css" href="/styles/filterform.css" />
    <link rel="stylesheet" type="text/css" href="/styles/common.css" />
    <link rel="stylesheet" type="text/css" href="/styles/tripick.css" />

    <script type="text/javascript">
        var useFilter_orig = "<c:out value='${useFilter}'/>";
        var conditions = <c:out value='${conditionsJSON}' escapeXml="false" />;
    </script>
    <script type='text/javascript' src='<c:url value="/dwr/engine.js"/>'></script>
    <script type='text/javascript' src='<c:url value="/dwr/interface/conditionManagerDWR.js"/>'></script>
    <script type='text/javascript' src='<c:url value="/dwr/interface/experimentManagerDWR.js"/>'></script>
    <script type="text/javascript" src="<c:url value='/scripts/jquery.js'/>"></script>
    <script type="text/javascript" src="<c:url value='/scripts/filterform.js'/>"></script>

    <style type="text/css">
        .offset_title
        {
        color: #3300e0;
        }
        #filterOptionsButtons {
            margin: 1em;
        }
        #filterOptionsButtons input {
            margin-right: 1em;
        }
        fieldset {
            margin-bottom: 1em;
        }
        div#main form ul {
            border-top: 1px solid silver;
        }
        div#main form ul li {
            border-bottom: 1px solid silver;
            border-collapse: collapse;
            padding-bottom: 5px;
        }
        a.disabled {
            color: gray;
        }
        .disabled {
            color: gray;
        }
        #filterOptions {
            position: relative; /* enable absolute positioning of "Filter ID" */
        }
        #filterID {
            font-style: italic;
            text-align: right;
            position: absolute;
            right: 10px;
            bottom: 5px;
        }
        #conditionFieldset {
            display: none;
            border: 1px solid silver;
            line-height: 2em;
        }
        #conditionSubforms {
            display: none;
        }
        .editedCondition {
            background-color: #E5C9BE;
        }
        .conditionIdLabel {
            font-style: italic;
            text-align: right;
            display: block !important;
            padding-bottom: 0;
            marginp-bottom: 0;
        }
        #bottomPadding {
            height: 300px;
        }

    </style>
    
</head>


<%@ include file="/common/progressDiv.jsp"%>

<form:form id="filterForm" commandName="filter" name="filterForm" action="filterform.html">
    <form:errors path="name" cssClass="fieldError"/> 

    <input type="checkbox" name="checkedIRB" id="checkedIRB" />&nbsp;<appfuse:label key="filter.checkedIRB"/>  <br/> <br/>

    <fieldset id="filterOptions">
        <legend>Filter Options</legend>
        <input type="radio" id="doNotUseFilter" name="useFilter" value="none" <c:if test="${useFilter=='none'}">checked="checked"</c:if> /> Do not use filter <br/>
        <input type="radio" name="useFilter" value="default" <c:if test="${useFilter=='default'}">checked="checked"</c:if> /> Use default filter for this experiment type <br/>
        <input type="radio" name="useFilter" value="separate" <c:if test="${useFilter=='separate'}">checked="checked"</c:if> /> Use separate filter for this experiment <br/>
        <div id="filterOptionsButtons" style="display: none">
            <input type="submit" id="optCancel" value="Cancel"/>
        </div>
        <div id="filterID">Filter ID: <c:out value="${filter.id}"/></div>
    </fieldset>

    <c:if test="${useFilter!='none'}">
        <fieldset id="filterConditions">
            <legend>Filter Conditions</legend>
            <c:if test="${useFilter=='default'}">
                <b>Note: Any changes will affect all other experiments using the default filter for this experiment type.</b>
            </c:if>
            <c:choose>
                <c:when test="${empty filter.conditions}">
                    <p> The filter has no conditions.  </p>
                </c:when>
                <c:otherwise>
                    <ul id="conditionList">
                        <c:forEach var="cond" items="${filter.conditions}" varStatus="status">
                            <li>
                            <a href="#" conditionId="<c:out value='${cond.id}'/>" conditionIndex="<c:out value='${status.index}'/>" class="editLink">Edit</a> | 
                            <a href="#" conditionId="<c:out value='${cond.id}'/>" class="deleteLink">Delete</a>
                            &mdash; <c:out value="${cond.humanString}"/>
                            <div class="conditionIdLabel"> Condition ID: <c:out value="${cond.id}"/> </div>
                            </li>
                        </c:forEach>
                    </ul>
                </c:otherwise>
            </c:choose>
            <div id="addConditionContainer">
                Add new condition &mdash;
                <select id="addCondition">
                    <option disabled selected value="">Select condition type</option>
                    <c:forEach var="conditionType" items="${conditionTypes}">
                        <option value='<c:out value="${conditionType[0]}"/>'><c:out value="${conditionType[1]}"/></option>
                    </c:forEach>
                </select>
            </div>

            <fieldset id="conditionFieldset">
                <legend>Add/Edit Condition</legend>
                <input type="hidden" name="conditionId" value=""/>
                <input type="hidden" name="conditionType" value=""/>
                <br/>
                <input type="submit" name="saveCondition" id="saveCondition" value="Save" />
                <button type="button" id="cancelConditionEdit">Cancel</button>
            </fieldset>

        </fieldset>
    </c:if>

    <p>
        Eligible subjects: <span id="eligibleCount">Counting...</span> &nbsp;&nbsp;&nbsp;&nbsp;
        <br/>
        <em>Note: Question Result Conditions and Experiment Conditions for future experiments are ignored when counting eligible subjects.</em>
    </p>

    <input type="checkbox" name="checkedIRB_bottom" id="checkedIRB_bottom" />&nbsp;<appfuse:label key="filter.checkedIRB"/>  <br/> <br/>

    <div id="buttons" class="buttonBar bottom">
        <input type="submit" class="button" name="back" value="<fmt:message key='button.back'/>" onclick="bCancel=true"/>
        <input type="submit" class="button" name="next" value="<fmt:message key='button.next'/>" onclick="bCancel=false"/>
        <input type="submit" class="button" name="cancel" value="<fmt:message key='button.cancel'/>" onclick="bCancel=true"/> 
        &nbsp;<i>or</i>&nbsp;<input type="submit" class="button" name="savenow" value="<fmt:message key='button.saveNow'/>" onclick="bCancel=false"/> 
    </div>
</form:form>

<%--
This div is immediately detached from the DOM by filterform.js,
and the individual subforms are re-attached as needed within #conditionFieldset.
--%>
<div id="conditionSubforms">

    <div conditionType="ExperimentTypeCondition">
        <select name="onList">
            <option selected value="false">Exclude</option>
            <option value="true">Include only</option>
        </select>
        <div>subjects who have</div>
        <select name="history">
            <option selected value="true">participated in experiments</option>
            <option value="false">signed up for future experiments</option>
        </select>
        <div>of the following type(s):</div>
        <div id="experimentTypeList"> </div>
        <select id="addExperimentType">
            <option disabled selected value="">Add experiment type</option>
            <c:forEach var="experimentType" items="${experimentTypes}">
                <option value='<c:out value="${experimentType.id}"/>'><c:out value="${experimentType.name}"/></option>
            </c:forEach>
        </select>
    </div>

    <div conditionType="ExperimentCondition">
        <select name="onList">
            <option selected value="false">Exclude</option>
            <option value="true">Include only</option>
        </select>
        <div>subjects who have signed up for (in the case of a future experiment) or participated in (in the case of a past experiment) the following experiment:</div>
        <select id="experimentCondition_type">
            <option disabled selected value="">Select experiment type</option>
            <c:forEach var="experimentType" items="${experimentTypes}">
                <option value='<c:out value="${experimentType.id}"/>'><c:out value="${experimentType.name}"/></option>
            </c:forEach>
        </select>
        <select id="experimentCondition_experiment" name="experiment">
            <option disabled selected value="">Select experiment</option>
        </select>
        <span id="#experimentCondition_loading"></span>
    </div>

    <div conditionType="NoShowCondition">
        Exclude subjects with more than
        <input type="text" name="maxNoShows" size="2" />
        no-shows.
    </div>

    <div conditionType="PoolCondition">
        <select name="memberOf">
            <option selected value="true">Include only</option>
            <option value="false">Exclude</option>
        </select>
        <div>subjects who are members of the following subject pool(s):</div>
        <div id="subjectPoolList"> </div>
        <select id="addSubjectPool">
            <option disabled selected value="">Add subject pool</option>
            <c:forEach var="subjectPool" items="${subjectPools}">
                <option value='<c:out value="${subjectPool.id}"/>'><c:out value="${subjectPool.name}"/></option>
            </c:forEach>
        </select>
    </div>

    <div conditionType="QuestionResultCondition">
        <div>
            Include only subjects who
            <select name="filterType">
                <option value="CHECK_ALL">check all</option>
                <option value="CHECK_SOME">check some</option>
                <option value="CHECK_NONE">check none</option>
                <option value="CHECK_NOT_SOME">do not check all</option>
                <option value="SELECT">select one</option>
                <option value="NO_SELECT">select none</option>
            </select>
            of the following checked options for question
            <select name="question">
                <option disabled selected value="">Select question</option>
                <c:forEach var="q" items="${questions}">
                    <option value='<c:out value="${q.id}"/>'><c:out value="${q.label}"/></option>
                </c:forEach>
            </select>
        </div>
        <div id="questionOptionsContainer">
            <c:forEach var="q" items="${questions}">
                <div questionId='<c:out value="${q.id}"/>' style="display: none">
                    <c:forEach var="option" varStatus="status" items="${q.optionList}">
                        <input type="checkbox" name="questionOptions"
                            value='<c:out value="${status.index}"/>' />
                        <c:out value="${option}"/>
                    </c:forEach>
                </div>
            </c:forEach>
        </div>
    </div>

</div>

<%--
Add extra padding to the bottom of the page so it doesn't scroll up when the condition subform is removed.
--%>
<div id="bottomPadding">
</div>
