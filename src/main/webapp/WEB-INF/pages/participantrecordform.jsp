<%@ include file="/common/taglibs.jsp"%> 
<%@ page import="edu.caltech.ssel.recruiting.model.ParticipantRecord"%>

<head>
    <title><fmt:message key="participantRecord.title"/></title>
    <meta name="heading" content="<fmt:message key='participantRecord.heading'/>"/>
    <meta name="menu" content="ExperimenterMenu"/>
    
    <script type='text/javascript'>
function paymentMethodChange(paymentMethodSelect) {
    var selectedItem = paymentMethodSelect.selectedIndex;
    var div = $("paymentStatusDiv");
    var chkbox = $("generatepayment");
    if(selectedItem == 1) {
        div.style.display="none";
        chkbox.checked=false;
    }
    else {
        div.style.display="block";
    }
}
function formSubmit(theform) {
    //If cancel verification, then clear fields to prevent binding errors
    if(bCancel) {
        $("playerNum").value="";
        $("payoff").value="";
        return true;
    }

    return validateParticipantRecord(theform);
}
    </script>
</head>

<form:form commandName="participantRecord" method="post" action="participantrecordform.html" id="participantRecordForm" onsubmit="return formSubmit(this)"> 
    <form:hidden path="id"/> 
    <form:hidden path="eligible"/> 
    <input type="hidden" name="pmtId" value="${participantRecord.payment.id}"/>
    <ul>
        <li>
            <appfuse:label key="participantRecord.subject_id" styleClass="desc"/>

            <%-- BRS: if this is an existing ParticipantRecord, don't allow changing the subject id. --%>
            <c:choose>
                <c:when test="${(participantRecord.subject != null)}">
                    <c:out value="${participantRecord.subject.fullName}"/>
                    <input type="hidden" name="subject_id" value="${participantRecord.subject.id}"/>
                </c:when>
                <c:otherwise>
                    <select id="subject_id" name="subject_id">
                    <c:choose>
                        <%-- BRS 2011-03-02  If the schoolId parameter is supplied, select the subject with that schoolId. --%>
                        <c:when test="${empty param.schoolId}">
                            <c:forEach items="${subjectList}" var="subj">
                                <option value="${subj.id}"><c:out value="${subj.fullName}"/></option>
                            </c:forEach>
                        </c:when>
                        <c:otherwise>
                            <c:forEach items="${subjectList}" var="subj">
                            <option <c:if test="${subj.schoolId == param.schoolId}">selected</c:if> value="${subj.id}"><c:out value="${subj.fullName}"/></option>
                            </c:forEach>
                        </c:otherwise>
                    </c:choose>
                    </select>
                </c:otherwise>
            </c:choose>

        </li>
        <%-- BRS: remove, since it's redundant with status field
        <li>
            <form:checkbox path="participated"/><appfuse:label key="participantRecord.participated"/>
        </li>
        --%>
        <li>
            <form:checkbox path="consent"/><appfuse:label key="participantRecord.consent"/>
        </li>
        <li> 
            <div>
                <appfuse:label styleClass="desc" key="participantRecord.status_name"/> 
                <select id="status_name" name="status_name" class="medium">
                        <option value="">--Select Status--</option>
                    <c:forEach items="${statusList}" var="statusLV">
                        <option <c:if test="${statusLV.value == participantRecord.status.name}">selected</c:if> value="${statusLV.value}"><c:out value="${statusLV.label}"/></option>
                    </c:forEach>
                </select>
            </div>
            <div>
                <appfuse:label styleClass="desc" key="participantRecord.playerNum"/> 
                <form:input path="playerNum" cssClass="text medium"/>
            </div>
        </li> 
        <li> 
            <div class="left" <c:if test="${isPayGateConfigured == false}"> style="display:none" </c:if>> <%-- BRS: hide payment options select if PayGate isn't active --%>
                <appfuse:label styleClass="desc" key="participantRecord.paymentMethod_name"/> 
                <select id="paymentMethod_name" name="paymentMethod_name" class="medium" onclick="paymentMethodChange(this)">
                    <c:forEach items="${paymentMethodList}" var="paymentMethodLV">
                        <option <c:if test="${paymentMethodLV.value == participantRecord.paymentMethod.name}">selected</c:if>
                                value="${paymentMethodLV.value}"><c:out value="${paymentMethodLV.label}"/></option>
                    </c:forEach>
                </select>
            </div>
            <div>
                <appfuse:label styleClass="desc" key="participantRecord.payoff"/> 
                (Remember to include show-up payment)
                <form:input path="payoff" cssClass="text medium"/>
            </div>
        </li> 
        <li id="paymentStatusDiv"
            <c:if test="${participantRecord.paymentMethod.name == 'PAYGATE'}">style="display:block"</c:if>
            <c:if test="${participantRecord.paymentMethod.name == 'MANUAL'}">style="display:none"</c:if>
            >
            Current Payment Status:&nbsp;<b><c:out value="${participantRecord.payment.status}" /></b>
            &nbsp;<input type="checkbox" id="generatepayment" name="generatepayment" <c:if test="${isPayGateConfigured == false}">disabled</c:if>/><label for="generatepayment">Generate a Payment Notification</label>
        </li>
        <li class="buttonBar bottom" id="buttonbar"> 
            <div class="divider"></div>
            <input type="submit" class="button" name="save" value="<fmt:message key='button.save'/>" onclick="bCancel=false"/> 
            <c:if test="${not empty participantRecord.id}">
                <input type="submit" name="delete" id="delete" onclick="bCancel=true; return confirmDelete('participantRecord')"  
                       value="<fmt:message key='button.delete'/>" class="button"/> 
            </c:if>
            <input type="submit" class="button" name="cancel" value="<fmt:message key='button.cancel'/>" onclick="bCancel=true"/> 
        </li>
    </ul>
</form:form>    

<v:javascript formName="participantRecord" cdata="false" dynamicJavascript="true" staticJavascript="false"/>
<script type="text/javascript" src="<c:url value='/scripts/validator.jsp'/>"></script>

<script type="text/javascript">
    Form.focusFirstElement($('participantRecordForm'));
</script>
