<%@ include file="/common/taglibs.jsp"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="brs" %>

<head>
    <title><fmt:message key="signup.title"/></title>
    <meta name="heading" content="<fmt:message key='signup.heading'/>"/>
    <meta name="menu" content="RegisterMenu"/>
    <link rel="stylesheet" type="text/css" href="<c:url value='/styles/${appConfig["csstheme"]}/user.css'/>"/>
    <script type="text/javascript" src="<c:url value='/scripts/selectbox.js'/>"></script>
    <script type="text/javascript" src="<c:url value='/scripts/common.js'/>"></script>
    <script type="text/javascript" src="<c:url value='/scripts/jquery.js'/>"></script>
    <script type="text/javascript" src="<c:url value='/scripts/user.js'/>"></script>
    <script type="text/javascript">
var dayCntList = new Array(12);
        
function formSubmit(theform) {
    selectAll("userPools");

    return validateSignup(theform);
}

function monthChange() {
    var monthSel = $("birthday_month");
    var daySel = $("birthday_day");
    var num = dayCntList[1*monthSel.value];
    
    //Remove extra days at the end
    while(daySel.length > num) {
        daySel.remove(daySel.length-1);
    }
    
    //Add to the end
    while(daySel.length < num) {
        var opt = appendOption(daySel, daySel.length + 1);
        opt.value=opt.text;
    }
}
    </script>
</head>

<body id="signup"/>

<spring:bind path="user.*">
    <c:if test="${not empty status.errorMessages}">
        <div class="error">    
            <c:forEach var="error" items="${status.errorMessages}">
                <img src="<c:url value="/images/iconWarning.gif"/>"
                     alt="<fmt:message key="icon.warning"/>" class="icon" />
                <c:out value="${error}" escapeXml="false"/><br />
            </c:forEach>
        </div>
    </c:if>
</spring:bind>

<div class="separator"></div>


<form:form commandName="user" method="post" action="signup.html" onsubmit="return formSubmit(this);" id="signupForm">
    <ul>
        <li class="info">
            <fmt:message key="signup.message"/>
        </li>

        <%-- BRS March 2010
        Removed username field; have the controller automatically set it to email address,
        effectively using email addresses as usernames. --%>

        <input type="hidden" name="subscribed" value="true">

        <%-- BRS Aug 2010 --%>
        <c:set var="pageName" value="signup" scope="request" />
        <%@ include file="/common/requiredSubjectProfileQuestions.jsp"%>

        <%--<li>
        <appfuse:label styleClass="desc" key="user.passwordHint"/>
        <form:errors path="passwordHint" cssClass="fieldError"/>
        <form:input path="passwordHint" id="passwordHint" cssClass="text large"/>
    </li>--%>
        
        <li>
            <div>
                <div class="left">
                    <%--
                    <appfuse:label styleClass="desc" key="user.email"/>
                    <form:errors path="email" cssClass="fieldError"/>
                    <form:input path="email" id="email" cssClass="text medium"/>
                    --%>
                </div>
            </div>
        </li>
            

        <%-- BRS March 2010 --%>
        <%@ include file="/common/additionalSubjectProfileQuestions.jsp"%>

        <%-- BRS - replaced with radio buttons for selecting a single subject pool (in file included above)
        <li>
            <fieldset class="pickList">
                <legend><fmt:message key="user.pools"/></legend>
                <table class="pickList" cellpadding="5">
                    <tr>
                        <th class="pickLabel">
                            <appfuse:label key="user.availPools" colon="false" styleClass="required"/>
                        </th>
                        <th class="pickLabel">
                            <appfuse:label key="user.selPools" colon="false" styleClass="required"/>
                        </th>
                    </tr>
                    <c:set var="leftList" value="${availLVPools}" scope="request"/>
                    <c:set var="rightList" value="${userLVPools}" scope="request"/>
                    <c:import url="/WEB-INF/pages/pickList.jsp">
                        <c:param name="listCount" value="1"/>
                        <c:param name="leftId" value="availablePools"/>
                        <c:param name="rightId" value="userPools"/>
                    </c:import>
                </table>
            </fieldset>
        </li>
        --%>

        <c:forEach items="${profile_qs}" var="question" varStatus="status">
            <li>
                <c:set var="myq" value="${question}" scope="request"/>
                <c:import url="/WEB-INF/pages/displayquestion.jsp">
                    <c:param name="qnum" value="null"/>
                    <c:param name="questionClass" value="desc"/>
                    <c:param name="index" value="${status.index}"/>
                    <c:param name="answer" value="0"/>
                    <c:param name="required" value="true"/>
                </c:import>
            </li>
        </c:forEach>
        <li>
<H2>Informed Consent Information</H2>
<%@ include file="consent.html" %>
<tiles:insertAttribute name="consent" />
        </li>
        <li class="buttonBar bottom">
            <input style="float: left" type="checkbox" id="consentCheckbox"/>
            <appfuse:label styleClass="desc" key="user.consent"/>
            <input type="submit" id="signupButton" disabled="disabled" class="button" name="save" onclick="bCancel=false" value="<fmt:message key='button.register'/>"/>
            <input type="submit" class="button" name="cancel" onclick="bCancel=true" value="<fmt:message key='button.cancel'/>"/>
        </li>
    </ul>
</form:form>

<script type="text/javascript">
    Form.focusFirstElement($('signupForm'));
    highlightFormElements();
    <c:forEach items="${monthList}" var="monthLV" varStatus="status">
        dayCntList[${status.index}] = ${monthLV.value};
    </c:forEach>
</script>

<v:javascript formName="signup" staticJavascript="false"/>
<script type="text/javascript" src="<c:url value='/scripts/validator.jsp'/>"></script>

<p>
<i>Note: You must have cookies enabled in your browser to register.

    <%--
    If you are having problems logging in or registering, please
    <u><b><a href="https://www.akresearch.org/recruiting/signup.html" target="_blank">follow this link</a></b></u> and try again.
--%>
</i>

</p>
