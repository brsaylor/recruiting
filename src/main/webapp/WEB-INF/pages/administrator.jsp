<%@ include file="/common/taglibs.jsp"%> 

<head>
    <title><fmt:message key="administrator.title"/></title>
    <meta name="heading" content="<fmt:message key='administrator.heading'/>"/>
    <meta name="menu" content="AdminMenu"/>
</head>

<h2>User Management</h2>
<ul class="glassList">
    <li><a href="users.html">View/Edit Existing Users</a></li>
    <li><a href="userform.html?method=Add&amp;addUserType=Subject&amp;from=list">Add New Subject</a></li>
    <li><a href="userform.html?method=Add&amp;addUserType=Experimenter&amp;from=list">Add New Experimenter</a></li>
    <li><a href="userform.html?method=Add&amp;addUserType=Administrator&amp;from=list">Add New Administrator</a></li>
    <li><a href="userform.html">Update My Profile</a></li>
</ul>

<h2>Subject Pool Management</h2>
<ul class="glassList">
    <li><a href="subjectpool.html">View/Edit Existing Pools</a></li>
    <li><a href="subjectpoolform.html">Add New Pool</a></li>
</ul>

<h2>Location Management</h2>
<ul class="glassList">
    <li><a href="location.html">View/Edit Existing Locations</a></li>
    <li><a href="editlocation.html">Add New Location</a></li>
</ul>

<h2>System Statistics</h2>
<ul class="glassList">
    <li><a href="statisticshome.html">View System Statistics</a></li>
</ul>

<h2>Other</h2>
<ul class="glassList">
    <li><a href="clearsession.html?id=1&amp;view=survey">Modify Profile Questions</a></li>
    <li><a href="clearsession.html?view=announcement">Make Administrative Announcement</a></li>
    <%--
    <li><a href="clearsession.html?view=importc2">Import CasselWeb2 Data</a></li>
    --%>
</ul>
