<%@ include file="/common/taglibs.jsp"%> 

<head>
    <title><fmt:message key="participantRecordOptions.title"/></title>
    <meta name="heading" content="<fmt:message key='participantRecordOptions.heading'/>
        <c:if test='${isAdmin}'> &mdash; <c:out value='${subject.username} (${subject.firstName} ${subject.lastName})'/></c:if>"/> 
    <meta name="menu" content="SubjectMenu"/>
    
    <script type='text/javascript'>
function formSubmit(theform) {

    var chkbox=$("participatebox_id");
    if(!bCancel && chkbox.checked == true)
        return confirm("<fmt:message key='participantRecordOptions.confirmCancel'/>");
    
    return true;
}
    </script>
    <style type="text/css">
        #consentText {
            margin: 1em 1em 1em 2em;
            padding: 1em;
            border: 1px solid silver;
        }
    </style>
</head>
<form:form commandName="participantRecord" method="post" action="participantrecordoptionsform.html" id="participantRecordOptionsForm" onsubmit="return formSubmit(this)"> 

    <form:hidden path="id"/>
    <form:hidden path="eligible"/>
    <input type="hidden" name="pmtId" value="${participantRecord.payment.id}"/>

    <appfuse:label key="participantRecordOptions.experimentsummary" colon="true" styleClass="desc"/>
    <ul class="glassList">
        <li>
            Experiment #${participantRecord.exp.id}, Status: ${participantRecord.exp.status.name}
        </li>
        <c:if test="${isAdmin}">
            <li>
                Type: ${participantRecord.exp.type.name}
            </li>
        </c:if>
        <li>
            <fmt:formatDate value="${participantRecord.exp.startTime}" pattern="h:mma"/> - <fmt:formatDate value="${participantRecord.exp.endTime}" pattern="h:mma"/>, <fmt:formatDate value="${participantRecord.exp.expDate}" pattern="EEE MM/dd/yyyy"/> at ${participantRecord.exp.location.name}
        </li>
        <li>
            Run by: ${participantRecord.exp.runby.fullName}
        </li>
        <li>
            Location: ${participantRecord.exp.location.name}
        </li>
        <li>
            Instructions: <br> ${participantRecord.exp.instruction}
        </li>
    </ul>
    <appfuse:label key="participantRecordOptions.participantsummary" colon="true" styleClass="desc"/>
    <ul class="glassList">
        <c:if test="${isAdmin}">
            <li>
                Subject: ${participantRecord.subject.username} (${participantRecord.subject.firstName} ${participantRecord.subject.lastName})
            </li>
            <li>
                Status: <c:out value="${participantRecord.status}"/>
            </li>
        </c:if>
        <li>
            Earnings: <c:if test="${empty participantRecord.payoff}"><%--Not Set--%></c:if><fmt:formatNumber value="${participantRecord.payoff}" type="currency" currencySymbol="$"/>
        </li>
        <%-- BRS removed 2012-01-06
        <li>
            Payment Method: ${participantRecord.paymentMethod}
        </li>
        --%>
        <c:if test="${participantRecord.paymentMethod.name == 'PAYGATE'}">
            <li>
                <appfuse:label key="participantRecordOptions.accountInfo"/>: <input type="text" name="accountInfo" value="${participantRecord.payment.accountInfo}" class="text medium" <c:if test="${frozen}">disabled</c:if>>
            </li>
            <li>
                Payment Status: ${participantRecord.payment.status}
            </li>
        </c:if>
        <li>
            <input type="checkbox" id="participatebox_id" name="participatebox" <c:if test="${frozen || !unsubscribe}">disabled</c:if>><appfuse:label key="participantRecordOptions.participatebox"/>
        </li>
        <%-- BRS removed
        <li>
            <input type="checkbox" name="reminderbox" <c:if test="${frozen || !unsubscribe}">disabled</c:if>><appfuse:label key="participantRecordOptions.reminderbox"/>
        </li>
        --%>
    </ul>

    <appfuse:label key="experimentType.consentText" colon="true" styleClass="desc"/>

    <p>
    Our records indicate that you
    <em><c:choose><c:when test="${participantRecord.consent}">consented</c:when><c:otherwise>did not consent</c:otherwise></c:choose></em>
    to the following:
    </p>

    <div id="consentText">
        <% pageContext.setAttribute("newLineChar", "\n"); %> 
        <c:out value="${fn:replace(participantRecord.exp.type.consentText, newLineChar, '<br/>')}" escapeXml="false" />
    </div>

    <ul>
        <li class="buttonBar bottom" id="buttonbar"> 
            <div class="divider"></div>
            <input type="submit" class="button" name="save" value="<fmt:message key='button.save'/>" onclick="bCancel=false"/> 
            <input type="submit" class="button" name="cancel" value="<fmt:message key='button.cancel'/>" onclick="bCancel=true"/> 
        </li>
    </ul>
</form:form>    

<script type="text/javascript">
    Form.focusFirstElement($('participantRecordOptionsForm'));
</script>
