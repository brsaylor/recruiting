<%@ include file="/common/taglibs.jsp"%> 

<head>
    <title><fmt:message key="paymentOption.title"/></title>
    <meta name="heading" content="<fmt:message key='paymentOption.heading'/>"/>
    <!--
        <meta name="menu" content=""/>
    -->
    
    <script type='text/javascript'>
function paymentOptionChange(num) {
    var div = $("accountInfoDiv");    
    if(num == 1) {
        div.style.display="block";
    }
    else {
        div.style.display="none";
        $("accountInfo").value="";
    }
}
    </script>
</head>

<form:form
        commandName="paymentOption"
        method="post"
        action="paymentoptionform.html"
        id="paymentOptionForm"
        onsubmit="return validatePaymentOption(this)">
    
    <form:hidden path="id" />
    <c:set var="out_expId" value="${param.expId}" />
    <input type="hidden" name="expId" value="${out_expId}" />
    <c:set var="out_expDate" value="${param.expDate}" />
    <input type="hidden" name="expDate" value="${out_expDate}" />
    
    <ul id="ulst">
        <c:if test="${showPayGate}">
        <li>
            <input type="radio"
                   id="paymentOptionRadio1"
                   name="paymentOptionRadio"
                   value="PAYGATE"
                   onclick="paymentOptionChange(1)"
                   checked />
            <label for="paymentOptionRadio1"><fmt:message key="paymentOption.PAYGATE"/></label>
        </li>
        </c:if>
        <c:if test="${showManual}">
        <li>
            <input type="radio"
                   id="paymentOptionRadio2"
                   name="paymentOptionRadio"
                   value="MANUAL"
                   onclick="paymentOptionChange(2)" />
            <label for="paymentOptionRadio2"><fmt:message key="paymentOption.MANUAL"/></label>
        </li>
        </c:if>
        <li id="accountInfoDiv" style="display:block"> 
            <appfuse:label styleClass="desc" key="paymentOption.account"/> 
            <form:errors path="accountInfo" cssClass="fieldError"/> 
            <form:input path="accountInfo" id="accountInfo"  cssClass="text medium"/>
        </li> 
        <li class="buttonBar bottom" id="buttonbar"> 
            <input type="submit" class="button" name="save" value="<fmt:message key='button.save'/>"/> 
            <input type="submit" class="button" name="cancel" value="<fmt:message key='button.cancel'/>" onclick="bCancel=true"/> 
        </li>
    </ul>
</form:form>

<v:javascript formName="paymentOption" cdata="false" dynamicJavascript="true" staticJavascript="false"/>
<script type="text/javascript" src="<c:url value="/scripts/validator.jsp"/>"></script>

<script type="text/javascript">
    Form.focusFirstElement($('paymentOptionForm'));
</script>