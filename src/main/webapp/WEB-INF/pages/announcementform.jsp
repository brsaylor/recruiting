<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<!--
empty myAnn = ${empty sessionScope['myAnnouncement']}
id = ${id}
-->

<head>
    <title><fmt:message key="announcementForm.title"/></title>
    <meta name="heading" content="<fmt:message key='announcementForm.heading'/>"/>
    <meta name="menu" content="${(empty experiment) ? 'AdminMenu' : 'ExperimenterMenu'}"/>

    <%-- BRS 2012-02-06 --%>
    <script type="text/javascript" src="<c:url value='/scripts/zeroclipboard/ZeroClipboard.js'/>"></script>
    <script type="text/javascript" src="<c:url value='/scripts/jquery.js'/>"></script>
    <script type="text/javascript">
        jQuery.noConflict();
        jQuery(document).ready(function($) {

            ZeroClipboard.setMoviePath("<c:url value='/scripts/zeroclipboard/ZeroClipboard.swf'/>");
            var clip = new ZeroClipboard.Client();
            clip.setHandCursor( true );
            var html = clip.getHTML(26, 26);
            $('#copy').after(html);
            clip.addEventListener('onMouseOver', function(client) {
                $('#copy').attr('src', "<c:url value='/images/copy_hover.png'/>");
            });
            clip.addEventListener('onMouseOut', function(client) {
                $('#copy').attr('src', "<c:url value='/images/copy.png'/>");
            });
            clip.addEventListener('onMouseDown', function(client) {
                clip.setText($('#emailList_to').val().join(', '));
            });

            // Message template selection
            $('#template').val("<c:out value='${template}'/>");
            $('#template').change(function() {
                document.location = "?template=" + $('#template').val();
            });

        });
    </script>
    <style type="text/css">
        #copy_container {
            position: relative;
            display: block;
        }
        #copy_container embed {
            position: absolute;
            left: 0;
            top: 0;
            margin: 0;
            padding: 0;
        }

        <%-- BRS 2012-03-07 --%>
        #messageText {
            width: 100%;
        }

    </style>
    
    <script type="text/javascript" src="<c:url value='/scripts/common.js'/>"></script>
    <script type="text/javascript">
        var lst1, lst2, lst3, lst4, lst5, lst6, lst7, lst8, to_lst, cc_lst, bcc_lst;
        
        function changeSelect(type) {
            whichList = $("listSelect" + type);
            optionsSelect = $("emailList" + type);
            var lst;
            clear(optionsSelect);
            if(whichList == null)
                return;
            if(whichList.value == "0") {
                return;
            }
            else if(whichList.value == "1") {
                lst = lst1;
            }
            else if(whichList.value == "2") {
                lst = lst2;
            }
            else if(whichList.value == "3") {
                lst = lst3;
            }
            else if(whichList.value == "4") {
                lst = lst4;
            }
            else if(whichList.value == "5") {
                lst = lst5;
            }
            else if(whichList.value == "6") {
                lst = lst6;
            }
            else if(whichList.value == "7") {
                lst = lst7;
            }
            else if(whichList.value == "8") {
                lst = lst8;
            }
            else if(whichList.value == "9") {
                if(type == "_to") {
                    lst = to_lst;
                } else if(type == "_cc") {
                    lst = cc_lst;
                } else {
                    lst = bcc_lst;
                }
            }

            for(var j = 0; j < lst.length; j++) {
                appendOption(optionsSelect, lst[j]);
            }
        }
        
        function addOption(type) {
            newOptionText = $("addEmailText" + type);
            optionsSelect = $("emailList" + type);
            optionText = newOptionText.value;
            if(optionText.length > 0) {
                appendOption(optionsSelect, optionText);
                newOptionText.value = "";
            }

            //Select the option "Other"
            var sel = $("listSelect" + type);
            for(j = 0; j < sel.length; j++) {
                if(sel.options[j].value == '9') {
                    sel.selectedIndex = j;
                }
            }
        }
        
        function removeOptions(type) {
            optionsSelect = $("emailList" + type);
            while(optionsSelect.selectedIndex != -1) {
                optionsSelect.remove(optionsSelect.selectedIndex);
            }
            if(optionsSelect.length > 0) {
                //Select the option "Other"
                var sel = $("listSelect" + type);
                for(j = 0; j < sel.length; j++) {
                    if(sel.options[j].value == '9') {
                        sel.selectedIndex = j;
                    }
                }
            }
            else
                $("listSelect" + type).selectedIndex = 0;//Switch to "--Empty list--"
        }
        
        function formSubmit(theform) {
            //Select options and validate
            var someSelected = false;            
            var types = new Array(3);
            types[0] = "to";
            types[1] = "cc";
            types[2] = "bcc";
            
            if(!validateAnnouncement(theform))
                return false;
            
            for(j = 0; j < types.length; j++) {
                if($("using_" + types[j]).checked) {
                    selectAll("emailList_"+types[j]);
                    if($("emailList_"+types[j]).length > 0)
                        someSelected = true;
                }
            }

            
            
            if(!someSelected && !bCancel) {
                alert("The email must have at least one recipient");
                return false;
            }
            
            
            return true;
        }
        
        function formLoad() {
            lst1 = parseJSPList("${eligList}");
            lst2 = parseJSPList("${allParticipantsList}");
            lst3 = parseJSPList("${eligibleParticipantsList}");
            lst4 = parseJSPList("${ineligibleParticipantsList}");
            lst5 = parseJSPList("${allSubjects}");
            lst6 = parseJSPList("${allStandardUsers}");
            lst7 = parseJSPList("${allExperimenters}");

            //alert("Elig list len: "+lst1.length+" All List len: "+lst5.length);

        
            //Initialize the lists for To, CC, and BCC
            var types = new Array(3);
            types[0] = "_to";
            types[1] = "_cc";
            types[2] = "_bcc";
            for(var j = 0; j < types.length; j++) {
                changeSelect(types[j]);
                changeUsing(types[j]);
            }
        }
        
        function changeUsing(type) {
            var tab = $("innerTable"+type);
            var check = $("using"+type);
            if(check.checked) {
                tab.style.display="block";
            } else {
                tab.style.display="none";
            }
        }
        
        function changeNotifSize(type) {
            $("notifSize_" + type).disabled = !$("setNotifSize_"+type).checked;
        }
    </script>
</head>
<form:form commandName="announcement" action="announcementform.html" id="announcementform" onsubmit="return formSubmit(this);">
    
    <c:if test="${not empty id}">
        <input type="hidden" name="id" value="${id}"/>
    </c:if>
    
    <script type="text/javascript">
        to_lst = new Array(${fn:length(announcement.to)});
        <c:forEach items="${announcement.to}" var="str" varStatus="status">to_lst[${status.index}]="${str}";</c:forEach>
        cc_lst = new Array(${fn:length(announcement.cc)});
        <c:forEach items="${announcement.cc}" var="str" varStatus="status">cc_lst[${status.index}]="${str}";</c:forEach>
        bcc_lst = new Array(${fn:length(announcement.bcc)});
        <c:forEach items="${announcement.bcc}" var="str" varStatus="status">bcc_lst[${status.index}]="${str}";</c:forEach>
    </script>
    
    <ul>

        <%-- BRS 2012-02-06 --%>
        <c:if test="${not empty experiment}">
            <li>
                <appfuse:label key="announcementForm.template" styleClass="desc" colon="true"/>
                <select id="template">
                    <option value="">Blank</option>
                    <option value="newexp-announcement.vm">New Experiment Announcement</option>
                    <option value="exp-cancellation-notice.vm">Experiment Cancellation Notice</option>
                    <option value="experiment-reminder-message.vm">Experiment Reminder</option>
                </select>
            </li>
        </c:if>

        <li>
            <appfuse:label key="announcementForm.from" styleClass="desc" colon="true"/>
            <form:input path="from" cssClass="text large"/>
        </li>
        <li>
            <appfuse:label key="announcementForm.replyTo" styleClass="desc" colon="true"/>
            <form:input path="replyTo" cssClass="text large"/>
        </li>
        <c:set var="type" value="to"/>
        <c:set var="currList" value="${announcement[type]}"/>
        <li>
            <fieldset>
                <legend><input type="checkbox" id="using_${type}" <c:if test="${type == 'to' || currList != null}">checked</c:if> onclick="changeUsing('_${type}');"><c:out value="${fn:toUpperCase(type)}"/></legend>
                <div id="innerTable_${type}">
                    <table>
                        <tr>
                            <td>
                                <appfuse:label key="announcementForm.listSelect" styleClass="desc" colon="false"/>
                                <select name="listSelect_${type}" id="listSelect_${type}" onchange="changeSelect('_${type}');">
                                    <option value="0" <c:if test="${((empty currList) && (currList != null)) || ((type != 'to') && (currList == null))}">selected</c:if> ><fmt:message key="announcementForm.blankList"/></option>
                                    <c:if test="${not empty experiment}"> <%-- eligible subjects makes no sense for an adminstrative announcement--%>
                                        <option value="1" <c:if test="${(type == 'to') && (currList == null) && (empty allParticipantsList)}">selected</c:if> ><fmt:message key="announcementForm.eligibleSubjects"/></option>
                                        <option value="2" <c:if test="${(type == 'to') && (currList == null) && (not empty allParticipantsList)}">selected</c:if> ><fmt:message key="announcementForm.allParticipants"/></option>
                                        <option value="3"><fmt:message key="announcementForm.eligibleParticipants"/></option>
                                        <option value="4"><fmt:message key="announcementForm.ineligibleParticipants"/></option>
                                    </c:if>
                                    <option value="5"><fmt:message key="announcementForm.allSubjects"/></option>
                                    <option value="6"><fmt:message key="announcementForm.allStandardUsers"/></option>
                                    <option value="7"><fmt:message key="announcementForm.allExperimenters"/></option>
                                    <!--<option value="8" <c:if test="${(type == 'to') && (currList == null) && (empty experiment)}">selected</c:if> ><fmt:message key="announcementForm.allUsers"/></option>-->
                                    <option value="8" <c:if test="${not empty currList}">selected</c:if> ><fmt:message key="announcementForm.other"/></option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <appfuse:label key="announcementForm.addEmail" styleClass="desc" colon="false"/>
                                <input type="text" id="addEmailText_${type}" style="text medium"/>
                                &nbsp;<input type="button" id="addopt_${type}" name="addopt_${type}" onclick="addOption('_${type}')" value="<fmt:message key='announcementForm.addEmail'/>"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <select id="emailList_${type}" name="emailList_${type}" multiple="true" size="5">
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="button" id="remopt_${type}" name="remopt_${type}" onclick="removeOptions('_${type}')" value="<fmt:message key='announcementForm.removeEmail'/>"/>
                                <div id="copy_container">
                                    <img id="copy" src="<c:url value='/images/copy.png'/>"/> <sup>&larr; Copy selected email(s)</sup>
                                </div>
                            </td>
                        </tr>
                    </table>
                    <c:if test="${not empty experiment}"> <%-- admins should not have to use reduced notification size --%>
                        <input type="checkbox" id="setNotifSize_${type}" name="setNotifSize_${type}" onclick="changeNotifSize('${type}');"> Only notify this many people from the selected list at random: <input type="text" id="notifSize_${type}" name="notifSize_${type}" disabled="true" size="4">
                    </c:if>
                </div>
            </fieldset>
        </li>
        <li>
            <appfuse:label key="announcementForm.subject" styleClass="desc" colon="true"/>
            <form:input path="subject" cssClass="text large"/>
        </li>
        <li>
            <appfuse:label key="announcementForm.text" styleClass="desc" colon="true"/> <br>
            <form:textarea path="text" cssClass="text large" id="messageText" />
        </li>
        <li class="buttonBar bottom"> 
            <input type="submit" class="button" name="send" value="<fmt:message key='button.send'/>" onclick="bCancel=false;"/> 
            <input type="submit" class="button" name="cancel" value="<fmt:message key='button.cancel'/>" onclick="bCancel=true;"/> 
        </li>
    </ul>
</form:form>

<v:javascript formName="announcement" cdata="false" dynamicJavascript="true" staticJavascript="false"/>
<script type="text/javascript" src="<c:url value="/scripts/validator.jsp"/>"></script>

<script type="text/javascript">
    formLoad();
    Form.focusFirstElement($('announcementform'));
</script>
