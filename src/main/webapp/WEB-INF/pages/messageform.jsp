<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<head>
    <title><fmt:message key="messageForm.title"/></title>
    <meta name="heading" content="<fmt:message key='messageForm.heading'/>"/>
    <meta name="menu" content="${(empty experiment) ? 'AdminMenu' : 'ExperimenterMenu'}"/>
    
    <script type="text/javascript" src="<c:url value='/scripts/common.js'/>"></script>
    <script type="text/javascript">
        var lst1, lst2, lst3, lst4, lst5, lst6, lst7, lst8, to_lst;
        
        function changeSelect() {
            whichList = $("listSelect");
            optionsSelect = $("emailList");
            var lst;
            clear(optionsSelect);
            if(whichList.value == "0") {
                return;
            }
            else if(whichList.value == "1") {
                lst = lst1;
            }
            else if(whichList.value == "2") {
                lst = lst2;
            }
            else if(whichList.value == "3") {
                lst = lst3;
            }
            else if(whichList.value == "4") {
                lst = lst4;
            }
            else if(whichList.value == "5") {
                lst = lst5;
            }
            else if(whichList.value == "6") {
                lst = lst6;
            }
            else if(whichList.value == "7") {
                lst = lst7;
            }
            else if(whichList.value == "8") {
                lst = lst8;
            }
            else if(whichList.value == "9") {
                lst = to_lst;
            }

            for(var j = 0; j < lst.length; j++) {
                appendSplitOption(optionsSelect, lst[j]);
            }
        }
        
        function removeOptions() {
            optionsSelect = $("emailList");
            while(optionsSelect.selectedIndex != -1) {
                optionsSelect.remove(optionsSelect.selectedIndex);
            }
            if(optionsSelect.length > 0) {
                //Select the option "Other"
                var sel = $("listSelect");
                for(j = 0; j < sel.length; j++) {
                    if(sel.options[j].value == '9') {
                        sel.selectedIndex = j;
                    }
                }
            }
            else
                $("listSelect").selectedIndex = 0;//Switch to "--Empty list--"
        }
        
        function messageFormSubmit(theform) {
            //Select options and validate
            var someSelected = false;            
            
            if(!validateMessage(theform))
                return false;
            
            selectAll("emailList");
            if($("emailList").length > 0)
                someSelected = true;
            
            if(!someSelected && !bCancel) {
                alert("The email must have at least one recipient");
                return false;
            }
            
            
            return true;
        }
        
        function formLoad() {
        
            lst1 = new Array(${fn:length(eligList)});
            <c:forEach items="${eligList}" var="user" varStatus="status">
                lst1[${status.index}]="${user.id},${user.firstName} ${user.lastName}";
            </c:forEach>
            lst2 = new Array(${fn:length(allParticipantsList)});
            <c:forEach items="${allParticipantsList}" var="user" varStatus="status">
                lst2[${status.index}]="${user.id},${user.firstName} ${user.lastName}";
            </c:forEach>
            lst3 = new Array(${fn:length(eligibleParticipantsList)});
            <c:forEach items="${eligibleParticipantsList}" var="user" varStatus="status">
                lst3[${status.index}]="${user.id},${user.firstName} ${user.lastName}";
            </c:forEach>
            lst4 = new Array(${fn:length(ineligibleParticipantsList)});
            <c:forEach items="${ineligibleParticipantsList}" var="user" varStatus="status">
                lst4[${status.index}]="${user.id},${user.firstName} ${user.lastName}";
            </c:forEach>
            lst5 = new Array(${fn:length(allSubjects)});
            <c:forEach items="${allSubjects}" var="user" varStatus="status">
                lst5[${status.index}]="${user.id},${user.firstName} ${user.lastName}";
            </c:forEach>
            lst6 = new Array(${fn:length(allStandardUsers)});
            <c:forEach items="${allStandardUsers}" var="user" varStatus="status">
                lst6[${status.index}]="${user.id},${user.firstName} ${user.lastName}";
            </c:forEach>
            lst7 = new Array(${fn:length(allExperimenters)});
            <c:forEach items="${allExperimenters}" var="user" varStatus="status">
                lst7[${status.index}]="${user.id},${user.firstName} ${user.lastName}";
            </c:forEach>
            lst8 = new Array(${fn:length(allUsers)});
            <c:forEach items="${allUsers}" var="user" varStatus="status">
                lst8[${status.index}]="${user.id},${user.firstName} ${user.lastName}";
            </c:forEach>
        
            changeSelect();
        }
        
        function changeNotifSize() {
            $("notifSize").disabled = !$("setNotifSize").checked;
        }
    </script>
</head>
<form:form commandName="message" action="messageform.html" id="messageform" onsubmit="return messageFormSubmit(this);">
    <form:hidden path="id"/>
    
    <script type="text/javascript">
        to_lst = new Array(${fn:length(message.messageReferences)});
        <c:forEach items="${message.messageReferences}" var="um" varStatus="status">
            to_lst[${status.index}]="${um.recipient.id},${um.recipient.firstName} ${um.recipient.lastName}";
        </c:forEach>
    </script>
    
    <ul>
        <c:set var="currList" value="${message.messageReferences}"/>
        <li>
            <fieldset>
                <legend>TO</legend>
                <div id="innerTable" style="display:block">
                    <table cellpadding="10">
                        <tr>
                            <td>
                                <appfuse:label key="messageForm.listSelect" styleClass="desc" colon="false"/>
                                <select name="listSelect" id="listSelect" onchange="changeSelect();">
                                    <option value="0" <c:if test="${(empty currList) && (currList != null)}">selected</c:if> ><fmt:message key="messageForm.blankList"/></option>
                                    <c:if test="${not empty experiment}"> <%-- eligible subjects makes no sense for an adminstrative message--%>
                                        <option value="1" <c:if test="${(currList == null) && (empty allParticipantsList)}">selected</c:if> ><fmt:message key="messageForm.eligibleSubjects"/></option>
                                        <option value="2" <c:if test="${(currList == null) && (not empty allParticipantsList)}">selected</c:if> ><fmt:message key="messageForm.allParticipants"/></option>
                                        <option value="3"><fmt:message key="messageForm.eligibleParticipants"/></option>
                                        <option value="4"><fmt:message key="messageForm.ineligibleParticipants"/></option>
                                    </c:if>
                                    <option value="5"><fmt:message key="messageForm.allSubjects"/></option>
                                    <option value="6"><fmt:message key="messageForm.allStandardUsers"/></option>
                                    <option value="7"><fmt:message key="messageForm.allExperimenters"/></option>
                                    <option value="8"><fmt:message key="messageForm.allUsers"/></option>
                                    <option value="9" <c:if test="${(not empty currList)}">selected</c:if>><fmt:message key="messageForm.other"/></option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <select id="emailList" name="emailList" multiple="true" size="5">
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="button" id="remopt" name="remopt" onclick="removeOptions()" value="<fmt:message key='messageForm.removeEmail'/>"/>
                            </td>
                        </tr>
                    </table>
                    <c:if test="${not empty experiment}"> <%-- admins should not have to use reduced notification size --%>
                        <input type="checkbox" id="setNotifSize" name="setNotifSize" onclick="changeNotifSize();"> Only notify this many people from the selected list at random: <input type="text" id="notifSize" name="notifSize" disabled="true" size="4">
                    </c:if>
                </div>
            </fieldset>
        </li>
        <li>
            <appfuse:label key="messageForm.subject" styleClass="desc" colon="true"/>
            <form:input path="subject" cssClass="text medium"/>
        </li>
        <li>
            <appfuse:label key="messageForm.text" styleClass="desc" colon="true"/> <br>
            <form:textarea path="text" cssClass="text large"/>
        </li>
        <li class="buttonBar bottom"> 
            <input type="submit" class="button" name="send" value="<fmt:message key='button.send'/>" onclick="bCancel=false;"/> 
            <input type="submit" class="button" name="save" value="<fmt:message key='button.save'/>" onclick="bCancel=false;"/> 
            <input type="submit" class="button" name="cancel" value="<fmt:message key='button.cancel'/>" onclick="bCancel=true;"/> 
        </li>
    </ul>
</form:form>

<v:javascript formName="message" cdata="false" dynamicJavascript="true" staticJavascript="false"/>
<script type="text/javascript" src="<c:url value="/scripts/validator.jsp"/>"></script>

<script type="text/javascript">
    formLoad();
    Form.focusFirstElement($('messageform'));
</script>
