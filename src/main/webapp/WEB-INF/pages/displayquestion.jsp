<%--
Input:
    myq - The actual question (request scoped external variable)
    param.qnum = null | NUMBER - the number to be displayed before the myq
    param.index = NUMBER - The index to be used for parsing later
    param.answer = The myq's answer (will be selected)
    param.questionClass = The css class of the question part
--%>

<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="mylib" uri="/WEB-INF/mylib.tld" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<c:set var="qstring" value="question"/>

<%-- BRS 2011-06-06 - use div instead of label to allow for multi-line questions. --%>
<%--
 <c:if test='${param.qnum != "null"}'>
    <c:out value="${param.qnum}. "/>
</c:if>

<label class="${param.questionClass}"><c:out value="${myq.question}"/><c:if test="${param.required == 'true'}"> <span class="req">*</span></c:if></label>
--%>

<div class="${param.questionClass}">
    
 <c:if test='${param.qnum != "null"}'>
    <c:out value="${param.qnum}. "/>
</c:if>
<% pageContext.setAttribute("newline", "\n"); %>
<% pageContext.setAttribute("br", "<br/>"); %>
<c:out escapeXml="false" value="${fn:replace(myq.question, newline, br)}"/><c:if test="${param.required == 'true'}"> <span class="req">*</span></c:if></div>

<!--
style = ${myq.style}
optionList = ${myq.optionList}
-->
<c:set var="k" value="0"/>
<c:if test="${myq.style.name == 'DROP_LIST'}">
    <ul>
        <li>
            <select id="${qstring}${param.index}" name="${qstring}${param.index}">
                <c:forEach items="${myq.optionList}" var="option">
                    <c:choose>
                        <c:when test="${param.answer == k}">
                            <option selected id="${qstring}${param.index},${k}" value="${k}"><c:out value="${option}"/></option>
                        </c:when>
                        <c:otherwise>
                            <option id="${qstring}${param.index},${k}" value="${k}"><c:out value="${option}"/></option>
                        </c:otherwise>
                    </c:choose>
                    <c:set var="k" value="${k + 1}"/>
                </c:forEach>
            </select>
        </li>
    </ul>
</c:if>
<c:if test="${myq.style.name == 'RADIO'}">
    <ul>
        <c:forEach items="${myq.optionList}" var="option">
            <li>
                <c:choose>
                    <c:when test="${param.answer == k}">
                        <input checked type="radio" name="${qstring}${param.index}" value="${k}" id="${qstring}${param.index},${k}"/>
                    </c:when>
                    <c:otherwise>
                        <input type="radio" name="${qstring}${param.index}" value="${k}" id="${qstring}${param.index},${k}"/>
                    </c:otherwise>
                </c:choose>
                <label for="${qstring}${param.index},${k}"><c:out value="${option}"/></label> 
                <c:set var="k" value="${k + 1}"/>
            </li>
        </c:forEach>
    </ul>
</c:if>
<c:if test="${myq.style.name == 'CHECK_BOX'}">
    <ul>
        <c:forEach items="${myq.optionList}" var="option">
            <li>
                <c:choose>
                    <%-- mylib:hasDigit tests whether an integer has 1 in binary digit 2^k --%>
                    <c:when test="${mylib:hasDigit(param.answer, k)}"> 
                        <input checked type="checkbox" name="${qstring}${param.index},${k}" id="${qstring}${param.index},${k}"/>
                    </c:when>
                    <c:otherwise>
                        <input type="checkbox" name="${qstring}${param.index},${k}" id="${qstring}${param.index},${k}"/>
                    </c:otherwise>
                </c:choose>
                <label for="${qstring}${param.index},${k}"><c:out value="${option}"/></label>
                <c:set var="k" value="${k + 1}"/>
            </li>
        </c:forEach>
    </ul>
</c:if>
