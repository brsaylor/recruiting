<%@ include file="/common/taglibs.jsp"%> 

<head>
    <title><fmt:message key="statisticshome.title"/></title>
    <meta name="heading" content="<fmt:message key='statisticshome.heading'/>"/>
    
    <script type="text/javascript" src="<c:url value='/scripts/common.js'/>"></script>
</head>

<h2>New Statistics Pages</h2>

<ul class="glassList">
    <li><a href="systemstats.html?type=namedQuery&queryName=subjectStatistics">Subject Statistics</a></li>
    <li><a href="systemstats.html?type=namedQuery&queryName=experimentTypeStatistics">Experiment Type Statistics</a></li>
</ul>

<h2>Database Dump</h2>
<ul class="glasslist">
    <li><a href="systemstats.html?type=databaseDump">Download database dump (ZIP file containing one CSV file per table)</a></li>
</ul>

<h2>Old Statistics Pages</h2>

<i>Experimenter Statistics</i>
<ul class="glassList">
    <li><a href="systemstats.html?type=experimenter&role=runby">View Experimenter Statistics by run by</a></li>
    <li><a href="systemstats.html?type=experimenter&role=reservedBy">View Experimenter Statistics by reserved by</a></li>
    <li><a href="systemstats.html?type=experimenter&role=principle">View Experimenter Statistics by principal</a></li>
</ul>

<i>Subject Statistics</i>
<ul class="glassList">
    <li><a href="systemstats.html?type=subject">View Subject Statistics</a></li>
</ul>

<i>Experiment Type Statistics</i>
<ul class="glassList">
    <li><a href="systemstats.html?type=experimentType">View Experiment Type Statistics</a></li>
</ul>

<i>Experiment Location Statistics</i>
<ul class="glassList">
    <li><a href="systemstats.html?type=location">View Experiment Location Statistics</a></li>
</ul>
