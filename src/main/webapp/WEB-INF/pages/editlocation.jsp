<%@ include file="/common/taglibs.jsp"%>

<head>
    <title><fmt:message key="locationDetail.title"/></title>
    <meta name="heading" content="<fmt:message key='locationDetail.heading'/>"/>
    <meta name="menu" content="AdminMenu"/>
    
    <script type='text/javascript'>
function onLocationChange() {
    var locSel = $("locationSelect");
    var url = '<c:url value="/editlocation.html"/>';
    
    if(locSel.selectedIndex != 0) {
        url += "?id=" + locSel.value;
    }
    
    document.location.href = url;
}
    </script>
</head>


<form:form commandName="location" method="post" action="editlocation.html" id="editLocation" onsubmit="return validateLocation(this)"> 
    <form:hidden path="id"/> 
    
    <select id="locationSelect" onchange="onLocationChange()">
        <option name="new_location" value="new_loc">--New location--</option>
        <c:forEach items="${locList}" var="loc">
            <option <c:if test="${loc.id == location.id}">selected</c:if> value="${loc.id}"><c:out value="${loc.name}"/></option>
        </c:forEach>
    </select>
    
    <ul>
        <li> 
            <appfuse:label styleClass="desc" key="location.name"/> 
            <form:errors path="name" cssClass="fieldError"/> 
            <form:input path="name" id="name"  cssClass="text medium"/>
        </li> 
        <li> 
            <appfuse:label styleClass="desc" key="location.contact"/> 
            <form:errors path="contact" cssClass="fieldError"/> 
            <form:input path="contact" id="contact"  cssClass="text medium"/>
        </li> 
        <li> 
            <appfuse:label styleClass="desc" key="location.phone"/> 
            <form:errors path="phone" cssClass="fieldError"/> 
            <form:input path="phone" id="phone"  cssClass="text medium"/>
        </li> 
        <li> 
            <appfuse:label styleClass="desc" key="location.contactEmail"/> 
            <form:errors path="contactEmail" cssClass="fieldError"/> 
            <form:input path="contactEmail" id="contactEmail"  cssClass="text medium"/>
        </li> 
        <li> 
            <appfuse:label styleClass="desc" key="location.addr"/> 
            <ul>
                <li> 
                    <%--
                    <appfuse:label styleClass="desc" key="address.address"/>
                    This doesn't seem to do the right thing in terms of required start
                    --%>
                    <label class="desc" for="addr.address"><fmt:message key="address.address"/> <span class="req">*</span></label>
                    <form:input path="addr.address" cssClass="text medium"/>
                </li> 
                <li> 
                    <appfuse:label styleClass="desc" key="address.city"/> 
                    <form:input path="addr.city" cssClass="text medium"/>
                </li> 
                <li> 
                    <appfuse:label styleClass="desc" key="address.province"/> 
                    <form:input path="addr.province" cssClass="text medium"/>
                </li> 
                <li> 
                    <appfuse:label styleClass="desc" key="address.postalCode"/> 
                    <form:input path="addr.postalCode" cssClass="text medium"/>
                </li> 
                <li> 
                    <appfuse:label styleClass="desc" key="address.country"/> 
                    <form:input path="addr.country" cssClass="text medium"/>
                </li> 
            </ul>
        </li> 
        <li> 
            <appfuse:label styleClass="desc" key="location.mapLink"/> 
            <form:errors path="mapLink" cssClass="fieldError"/> 
            <form:input path="mapLink" id="mapLink"  cssClass="text medium"/>
        </li> 
        
        <li class="buttonBar bottom"> 
            <input type="submit" class="button" name="save" value="<fmt:message key='button.save'/>"/> 
            <c:if test="${not empty location.id}">
                <input type="submit" name="delete" id="delete" onclick="bCancel=true; return confirmDelete('location')"  
                       value="<fmt:message key='button.delete'/>" class="button"/> 
            </c:if>
            <input type="submit" class="button" name="cancel" value="<fmt:message key='button.cancel'/>" onclick="bCancel=true"/> 
        </li>
    </ul>
</form:form>    

<v:javascript formName="location" cdata="false" dynamicJavascript="true" staticJavascript="false"/>
<script type="text/javascript" src="<c:url value="/scripts/validator.jsp"/>"></script>

<script type="text/javascript">
    Form.focusFirstElement($('editLocation'));
</script>