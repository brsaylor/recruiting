<%@ include file="/common/taglibs.jsp"%>

<head> 
    <title><fmt:message key="participantRecordList.title"/></title> 
    <meta name="heading" content="<fmt:message key='participantRecordList.heading'/>"/> 
    <meta name="menu" content="ExperimenterMenu"/>
    <script type="text/javascript" src="<c:url value='/scripts/jquery.js'/>"></script>
    <script type="text/javascript" src="<c:url value='/scripts/cardswipe.js'/>"></script>
    <script type="text/javascript">
        var participantRecords = new Array();
        var prec;
        <c:forEach var="prec" items="${participantRecordList}" varStatus="status">
            prec = new Object();
            prec.id = <c:out value="${prec.prec.id}"/>;
            prec.schoolId = <c:out value="${prec.prec.subject.schoolId}"/>;
            prec.subject_id = <c:out value="${prec.prec.subject.id}"/>;
            participantRecords.push(prec);
        </c:forEach>
    </script>
</head> 

<c:set var="buttons"> 
    <input type="button" style="margin-right: 5px" 
           onclick="location.href='<c:url value="/participantrecordform.html"/>'" 
	   value="<fmt:message key="button.add"/>"/> 
    
    <input type="button" onclick="location.href='<c:url value="/modifyexperiment.html"/>'" 
	   value="<fmt:message key="button.done"/>"/> 
           &nbsp;<i>OR</i>&nbsp;
    <input type="button" style="margin-right: 5px" 
           onclick="location.href='<c:url value="/participantrecorduploadform.html"/>'" 
	   value="<fmt:message key="button.upload"/>"/> 

    <input type="button" style="margin-right: 5px" 
           onclick="location.href='<c:url value="/participantrecord.html?downloadReceipts=1"/>'" value="Print Receipts"/> 

    <%-- This causes the Excel file to be copied to the target dir during build, which is handy. --%>
    <a href='<c:url value="/spreadsheets/TSDF for Research Subject Payments.xls"/>'>Download blank receipt</a>
</c:set> 

<c:out value="${buttons}" escapeXml="false"/> 
<br><br>
<b><fmt:message key="participantRecord.pageSize"/></b>
<a href="participantrecord.html?size=25">25,</a>
<a href="participantrecord.html?size=50">50,</a>
<a href="participantrecord.html?size=100">100,</a>
<a href="participantrecord.html?size=All">All</a>

<%--Functionality comes from the "name" attribute of the table--%>

<%-- http://displaytag.sourceforge.net/1.2/displaytag/tagreference.html --%>

<display:table name="participantRecordList" cellspacing="0" cellpadding="0" requestURI="" 
	       id="participantRecord" pagesize="${pageSize}" class="table participantRecordList" export="true"> 
    
    <display:column url="/participantrecordform.html" paramId="id" paramProperty="prec.id" property="prec.subject.fullName" sortProperty="prec.subject.lastName" escapeXml="true" sortable="true" titleKey="participantRecord.name"/>

    <%-- BRS - show number of no-shows, number of bumps --%>
    <display:column property="noShowCount" escapeXml="true" sortable="true" titleKey="participantRecordList.noShowCount"/>
    <display:column property="bumpCount" escapeXml="true" sortable="true" titleKey="participantRecordList.bumpCount"/>

    <%-- BRS this column makes no sense - says Ineligible even for subjects that were able to sign up
    <display:column escapeXml="false" sortable="true" titleKey="participantRecord.eligible">
        <c:choose>
            <c:when test="${participantRecord.prec.eligible}">Eligible</c:when>
            <c:otherwise>
                <font color="red">Ineligible</font>
            </c:otherwise>
        </c:choose>
    </display:column>
    --%>

    <display:column sortProperty="prec.playerNum" escapeXml="true" sortable="true" titleKey="participantRecord.playerNum"><c:out value="${(empty participantRecord.prec.playerNum) ? 'Not set' : participantRecord.prec.playerNum}"/></display:column>

    <display:column property="prec.subject.schoolId" escapeXml="true" sortable="true" title="ID number"/>

    <%-- BRS 2012-02-21 added link to profile --%>
    <display:column url="/userform.html?from=list" paramId="id" paramProperty="prec.subject.id"  property="prec.subject.email" escapeXml="true" sortable="true" titleKey="Email (click for profile)"/>

    <display:column escapeXml="true" sortable="true" titleKey="participantRecord.payoff">
        <c:choose>
            <c:when test="${empty participantRecord.prec.payoff}">Not set</c:when>
            <c:otherwise>
                <fmt:formatNumber type="currency" value="${participantRecord.prec.payoff}" currencySymbol="$"/>
            </c:otherwise>
        </c:choose>
    </display:column>

    <%-- BRS removed - redundant with status=participated
    <display:column sortProperty="prec.participated" escapeXml="true" sortable="true" titleKey="participantRecord.participated"><c:out value="${(participantRecord.prec.participated) ? 'Yes' : 'No'}"/></display:column>
    --%>

    <display:column property="prec.status" escapeXml="true" sortable="true" titleKey="participantRecord.status_name"/>

    <%-- BRS removed since we're not using PayGate
    <display:column property="prec.paymentMethod" escapeXml="true" sortable="true" titleKey="participantRecord.paymentMethod_name"/>
    <display:column property="prec.payment.status" escapeXml="true" sortable="true" titleKey="participantRecord.payment.status"/>
    --%>

    <%-- BRS added 2012-02-21 --%>
    <display:column property="prec.subject.usCitizen" escapeXml="true" sortable="true" title="U.S. Citizen"/>

    <display:setProperty name="paging.banner.item_name" value="participant"/> 
    <display:setProperty name="paging.banner.items_name" value="participants"/> 
    
    <display:setProperty name="export.excel.filename" value="ParticipantList.xls"/> 
    <display:setProperty name="export.csv.filename" value="ParticipantList.csv"/> 
    <display:setProperty name="export.pdf.filename" value="ParticipantList.pdf"/>
</display:table> 

<c:out value="${buttons}" escapeXml="false"/> 

<%-- BRS 2012-02-21 removed - interferes with link to profile
<script type="text/javascript"> 
    highlightTableRows("participantRecord"); 
</script>
--%>

<br/>
<br/>

<form id="cardSwipeForm" action="participantrecordform.html" method="post">
    <input type="hidden" name="id"/>
    <input type="hidden" name="status_name"/>
    <input type="hidden" name="subject_id"/>
    Swipe ID Card: <input id="cardSwipeInput"/> <span id="cardSwipeMessage"></span>
</form>

<%-- BRS 2012-01-06 --%>
<br/>
<h2><b>Participants in previous experiments of this type</b></h2>

<b><fmt:message key="participantRecord.pageSize"/></b>
<a href="participantrecord.html?size=25">25,</a>
<a href="participantrecord.html?size=50">50,</a>
<a href="participantrecord.html?size=100">100,</a>
<a href="participantrecord.html?size=All">All</a>

<display:table name="prevParticipantRecordList" requestURI="" id="prevParticipantRecord" pagesize="${pageSize}" class="table participantRecordList" export="true">
    <display:column url="/participantrecordform.html" paramId="id" paramProperty="id" property="subject.fullName" sortProperty="subject.lastName" escapeXml="true" sortable="true" titleKey="participantRecord.name"/>
    <display:column property="exp.id" sortable="true" title="Experiment"/>
    <display:column property="exp.expDate" sortable="true" title="Date"/>
    <display:column property="exp.startTime" sortable="true" title="Time"/>
    <display:column property="status" sortable="true" title="Status"/>
</display:table> 

<%--
<table>
    <tr>
        <th>Experiment</th>
        <th>Participant</th>
        <th>Status</th>
    </tr>
    <c:forEach items="${prevParticipantRecordList}" var="prec">
        <tr>
            <td><c:out value="${prec.exp.id}: ${prec.exp.expDate} ${prec.exp.startTime}"/></td>
            <td><c:out value="${prec.subject.fullName}"/></td>
            <td><c:out value="${prec.status}"/></td>
        </tr>
    </c:forEach>
</table>
--%>
