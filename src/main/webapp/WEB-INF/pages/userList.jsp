<%@ include file="/common/taglibs.jsp"%>

<head>
    <title><fmt:message key="userList.title"/></title>
    <meta name="heading" content="<fmt:message key='userList.heading'/>"/>
    <meta name="menu" content="AdminMenu"/>

    <script type="text/javascript" src="<c:url value='/scripts/jquery.js'/>"></script>
    <script type="text/javascript">
        jQuery.noConflict();
        jQuery(document).ready(function($) {
            $("#addAdminButton").click(function() {
                if (confirm("Warning: Once created, an administrator cannot be given the experimenter role, but an experimenter can always be given the administrator role.  Are you sure you want to add an administrator?")) {
                    location.href='<c:url value="/userform.html?method=Add&from=list&addUserType=Administrator"/>';
                }
            });
        });
    </script>
</head>

<c:set var="buttons">
    <%-- BRS - changed from "Add" (which added a Subject) to 3 buttons, one for each role--%>
    <input type="button" style="margin-right: 5px"
        onclick="location.href='<c:url value="/userform.html?method=Add&amp;from=list&amp;addUserType=Subject"/>'"
        value="<fmt:message key="button.addSubject"/>"/>

    <input type="button" style="margin-right: 5px"
        onclick="location.href='<c:url value="/userform.html?method=Add&amp;from=list&amp;addUserType=Experimenter"/>'"
        value="<fmt:message key="button.addExperimenter"/>"/>

    <input type="button" id="addAdminButton" style="margin-right: 5px"
        value="<fmt:message key="button.addAdministrator"/>"/>

    <input type="button" onclick="location.href='<c:url value="/administrator.html"/>'"
        value="<fmt:message key="button.done"/>"/>
</c:set>

<!--<c:out value="${buttons}" escapeXml="false" />-->

<table style="margin-bottom:0">
    <tr>
        <form method="GET">
            <!--<td><appfuse:label styleClass="desc" key="userList.search"/></td>-->
            <td><input type="text" SIZE="25" name="searchQuery" value="${searchQuery}" /></td>
            <td>
                <select id="querySelected" name="querySelected">
                    <c:forEach items="${queryList}" var="query">
                        <option <c:if test="${query == queryField}">selected</c:if> value="${query}"><c:out value="${query}"/></option>
                    </c:forEach>
                </select>
            </td>
            <c:if test="${param.option != null}"><td><input type="hidden" name="option" value="${param.option}"/></td></c:if>
            <td><input type="submit" value="Search" /></td>

            <%-- BRS --%>
            <td>
                <c:choose>
                    <c:when test="${param.pageSize==0}">
                        <a href='<c:url value="/users.html"/>'>Show 25 users per page</a>
                    </c:when>
                    <c:otherwise>
                        <a href='<c:url value="/users.html?pageSize=0"/>'>Show all users on one page (warning: slow)</a>
                    </c:otherwise>
                </c:choose>
            </td>
        </form>
    </tr>
</table>
<%-- BRS 2012-04-18 --%>
Hint: You can use * as a wildcard.

<%-- BRS 2011-06-09 - take pagesize from request (also modified PaginateListFactory and ExtendedPaginatedListImpl) --%>
<c:set var="pageSize" value="${param.pageSize}"/>
<c:if test="${pageSize==null}">
    <c:set var="pageSize" value="25"/>
</c:if>

<display:table name="userList" cellspacing="0" cellpadding="0" requestURI="" sort="external"
defaultsort="1" id="users" pagesize="${pageSize}" class="table" export="true" partialList="true" size="resultSize">
        <display:caption>
            <c:choose>
                <c:when test="${param.option == 'users' || param.option == null}">
                    <fmt:message key='user.users'/>
                </c:when>
                <c:otherwise>
                    <a href="users.html?option=users"><fmt:message key='user.users'/></a>
                </c:otherwise>
            </c:choose>&nbsp;/
            <c:choose>
                <c:when test="${param.option == 'subjects'}">
                    <fmt:message key='user.subjects'/>
                </c:when>
                <c:otherwise>
                    <a href="users.html?option=subjects"><fmt:message key='user.subjects'/></a>
                </c:otherwise>
            </c:choose>&nbsp;/
            <c:choose>
                <c:when test="${param.option == 'experimenters'}">
                    <fmt:message key='user.experimenters'/>
                </c:when>
                <c:otherwise>
                    <a href="users.html?option=experimenters"><fmt:message key='user.experimenters'/></a>
                </c:otherwise>
            </c:choose>&nbsp;/
            <c:choose>
                <c:when test="${param.option == 'admins'}">
                    <fmt:message key='user.admins'/>
                </c:when>
                <c:otherwise>
                    <a href="users.html?option=admins"><fmt:message key='user.admins'/></a>
                </c:otherwise>
            </c:choose>
        </display:caption>
    <display:column property="username" sortName="username" escapeXml="true" sortable="true" titleKey="user.username" style="width: 25%"
        url="/userform.html?from=list" paramId="id" paramProperty="id"/>
    <display:column property="fullName" sortName="firstName" escapeXml="true" sortable="true" titleKey="activeUsers.fullName" style="width: 34%"/>
    <display:column property="firstName" sortName="firstName" escapeXml="true" sortable="true" titleKey="activeUsers.firstName" style="width: 34%"/>
    <display:column property="lastName" sortName="lastName" escapeXml="true" sortable="true" titleKey="activeUsers.lastName" style="width: 34%"/>
    <display:column property="email" sortName="email" sortable="true" titleKey="user.email" style="width: 25%" autolink="true" media="html"/>
    <display:column property="email" sortName="email" titleKey="user.email" media="csv xml excel pdf"/>

    <display:column sortProperty="enabled" sortable="true" titleKey="user.enabled" style="width: 16%; padding-left: 15px" media="html">
        <input type="checkbox" disabled="disabled" <c:if test="${users.enabled}">checked="checked"</c:if>/>
    </display:column>
    <display:column property="enabled" sortName="enabled" titleKey="user.enabled" media="csv xml excel pdf"/>

    <%-- BRS --%>
    <display:column sortable="true" titleKey="user.subscribedShort" style="width: 16%; padding-left: 15px" media="html">
        <c:catch var="e">
            <c:choose>
                <c:when test="${users.subscribed}">
                    <input type="checkbox" disabled="disabled" checked="checked"/>
                </c:when>
                <c:otherwise>
                    <input type="checkbox" disabled="disabled"/>
                </c:otherwise>
            </c:choose>
        </c:catch>
        <c:if test="${e!=null}">
            <%-- An exception was thrown, probably because the User was not a Subject and so doesn't have a "subscribed" attribute. --%>
        </c:if>
    </display:column>
    <display:column titleKey="user.subscribedShort" media="csv xml excel pdf">
        <c:catch var="e">
            <c:out value="${users.subscribed}"/>
        </c:catch>
        <c:if test="${e!=null}">
            <%-- An exception was thrown, probably because the User was not a Subject. --%>
        </c:if>
    </display:column>

    <display:setProperty name="paging.banner.item_name" value="user"/>
    <display:setProperty name="paging.banner.items_name" value="users"/>

    <display:setProperty name="export.excel.filename" value="User List.xls"/>
    <display:setProperty name="export.csv.filename" value="User List.csv"/>
    <display:setProperty name="export.pdf.filename" value="User List.pdf"/>
</display:table>

<c:out value="${buttons}" escapeXml="false" />

<script type="text/javascript">
    highlightTableRows("users");
</script>

<p>
</p>
<p>
<c:out value="${userCount}"/> user(s) currently logged in:
</p>

<ul>
    <c:forEach var="email" items="${userEmails}">
        <li><c:out value="${email}"/></li>
    </c:forEach>
</ul>
