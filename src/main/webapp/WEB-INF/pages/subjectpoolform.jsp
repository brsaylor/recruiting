<%@ include file="/common/taglibs.jsp"%> 

<head>
    <title><fmt:message key="subjectPoolDetail.title"/></title>
    <meta name="heading" content="<fmt:message key='subjectPoolDetail.heading'/>"/>
    <meta name="menu" content="AdminMenu"/>
    
    <script type='text/javascript'>
function onSubjPoolChange() {
    var subjPoolSel = $("subjPoolSelect");
    var url = '<c:url value="/subjectpoolform.html"/>';
    
    if(subjPoolSel.selectedIndex != 0) {
        url += "?id=" + subjPoolSel.value;
    }
    
    document.location.href = url;
}
    </script>
</head>


<form:form commandName="subjectPool" method="post" action="subjectpoolform.html" id="subjectPoolForm" onsubmit="return validateSubjectPool(this)"> 
    <form:hidden path="id"/> 
    
    <select id="subjPoolSelect" onchange="onSubjPoolChange()">
        <option name="new_pool" value="new_pool">--New subject pool--</option>
        <c:forEach items="${subjPoolList}" var="pool">
            <option <c:if test="${pool.id == subjectPool.id}">selected</c:if> value="${pool.id}"><c:out value="${pool.name}"/></option>
        </c:forEach>
    </select>
    
    <ul id="ulst">
        <li> 
            <appfuse:label styleClass="desc" key="subjectPool.name"/> 
            <form:errors path="name" cssClass="fieldError"/> 
            <form:input path="name" id="name"  cssClass="text medium"/>
        </li> 
        <li> 
            <appfuse:label styleClass="desc" key="subjectPool.consentInfo"/> 
            <form:errors path="consentInfo" cssClass="fieldError"/> 
            <form:textarea path="consentInfo" id="consentInfo"  cssClass="text large"/>
        </li> 
        <li> 
            <appfuse:label styleClass="desc" key="subjectPool.descr"/> 
            <form:errors path="descr" cssClass="fieldError"/> 
            <form:input path="descr" id="descr"  cssClass="text large"/>
        </li> 
        <li>
            <label class="desc">Signup Code</label>
            <form:input path="signupCode"/>
        </li>
        <c:if test="${not empty subjectPool.id}"> 
            <li id="validLI">
                <form:errors path="valid" cssClass="fieldError"/>
                <form:checkbox path="valid"/>Subject Pool Is Valid
            </li>
        </c:if>
        <li class="buttonBar bottom" id="buttonbar"> 
            <input type="submit" class="button" name="save" value="<fmt:message key='button.save'/>"/> 
            <c:if test="${not empty subjectPool.id}">
                <input type="submit" name="delete" id="delete" onclick="bCancel=true; return confirmDelete('subjectPool')"  
                       value="<fmt:message key='button.delete'/>" class="button"/> 
            </c:if>
            <input type="submit" class="button" name="cancel" value="<fmt:message key='button.cancel'/>" onclick="bCancel=true"/> 
        </li>
    </ul>
</form:form>    

<v:javascript formName="subjectPool" cdata="false" dynamicJavascript="true" staticJavascript="false"/>
<script type="text/javascript" src="<c:url value="/scripts/validator.jsp"/>"></script>

<script type="text/javascript">
    Form.focusFirstElement($('subjectPoolForm'));
</script>
