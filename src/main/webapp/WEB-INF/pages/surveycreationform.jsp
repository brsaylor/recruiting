<%@ include file="/common/taglibs.jsp"%> 
<%@ taglib prefix="mylib" uri="/WEB-INF/mylib.tld" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<c:set var="isProf" value="${survey.type == 'PROFILE'}"/>
<c:set var="noExp" value="${empty sessionScope['experiment']}"/> <%--Cannot use expId because of profile survey--%>
<c:set var="noFilter" value="${(noExp || not empty experiment.filter)}"/> <%-- if a filter exists it must be modified on the filters form --%>

<head>
    <title><fmt:message key="surveyCreationForm.title"/></title>
    <c:choose>
        <c:when test="${isProf}">
            <meta name="heading" content="<fmt:message key='surveyCreationForm.profile_heading'/>"/>
            <meta name="menu" content="AdminMenu"/>
        </c:when>
        <c:otherwise>
            <meta name="heading" content="<fmt:message key='surveyCreationForm.heading'/>"/>
            <meta name="menu" content="ExperimenterMenu"/>
        </c:otherwise>
    </c:choose>
    
    <link rel="stylesheet" type="text/css" href="/styles/surveycreationform.css" />
    <link rel="stylesheet" type="text/css" href="/styles/tripick.css" />
    <%--
    NOTE: CSS should be copied to surveycreationform.css and common.css for legibility,
    but while actively working on forms, having it directly in the jsp file is nice
    --%>
    <style type="text/css">
        .moveOptions {
        padding-bottom: 2px;
        font-size: 1em;
        }
        
        div.eligibleSubjectsDiv {
        text-align: center;
        }
    </style>
    
    <script type="text/javascript" src="<c:url value='/scripts/selectbox.js'/>"></script>
    <script type="text/javascript" src="<c:url value='/scripts/common.js'/>"></script>
    <script type='text/javascript' src='<c:url value="/dwr/engine.js"/>'></script>
    <script type='text/javascript' src='<c:url value="/dwr/interface/experimentManagerDWR.js"/>'></script>
    <script type='text/javascript' src='<c:url value="/dwr/interface/questionManagerDWR.js"/>'></script>
    <script type='text/javascript' src='<c:url value="/dwr/interface/surveyManagerDWR.js"/>'></script>
    <script type='text/javascript' src='<c:url value="/dwr/interface/QuestionDWR.js"/>'></script>
    <script type='text/javascript' src='<c:url value="/dwr/util.js"/>'></script>
    <script type="text/javascript">
var qcnt = 0;
var SURVEY_ID = -1;
var ADD_END = "--add to end--";
var ADD_FRONT = "--add to front--";
var NO_FILTER;
var ALLOW_MODIFY = false;
<c:choose>
    <c:when test="${not empty experiment.id}">
var EXP_ID = ${experiment.id};
    </c:when>
    <c:otherwise>
var EXP_ID = -1;
    </c:otherwise>
</c:choose>
//Code for getQuestionTable requirest that common.js be included

creating = false;

var qIdList = new Array();

function surveyChange(num) {
    ess = $("existingSurveySelect");
    nss = $("newSurveySelect");
    sdiv = $("surveyEditDiv");
    qdiv = $("questionEditDiv");
    surVis = $("surveyVisibility");
    name = $("name");
    
    if(creating)
        if(!confirm("Edits will be lost.  Please confirm.")) {
            $("surveyRadio3").checked=true;
            return false;
        }
        
    var span = $("notificationSpan");
    if(span != null) {
        span.className="none";
        clearNodeChildren(span);
    }
    
    span = $("successSpan");
    if(span != null) {
        span.className="none";
        clearNodeChildren(span);
    }
    
    if(num == 1) {
        ess.disabled=true;
        nss.disabled=true;
        sdiv.style.display="none";
        creating = false;
    }
    else if(num == 2) {
        idx = ess.selectedIndex;
        opts = ess.options;
        
        ess.disabled=false;
        nss.disabled=true;
        surveyManagerDWR.getByIdString(opts[idx].value, populateSurvey);
        experimentManagerDWR.getBySurveyId(opts[idx].value, otherExpWarning);

        sdiv.style.display="block";
        qdiv.style.display="block";
            
        creating = false;
    }
    else {
        idx = nss.selectedIndex;
        opts = nss.options;
        
        ess.disabled=true;
        nss.disabled=false;
        if(opts[idx].value == "new") {
            var div = $("surveyPreviewDiv");
            clearNodeChildren(div);
            SURVEY_ID = -1;
            name.value="";
            qcnt=0;
            qIdList = new Array();
            updateNumList();
        } else
            surveyManagerDWR.getByIdString(opts[idx].value, populateSurvey);
            
        sdiv.style.display="block";
        qdiv.style.display="block";
        name.readOnly = false;
        creating = true;
    }
}

function updateSurvey() {
    for(j = 1; j < 4; j++) {
        if($("surveyRadio"+j).checked) {
            surveyChange(j);
            break;
        }
    }
}
function previewDivHeader() {
    var div = $("surveyPreviewDiv");
    addChild(div, "hr");
    addChild(div, "br");
    var h2 = addChild(div, "h2");
    addTextChild(h2, "Preview Survey");//*****NOT LANGUAGE SAFE********
    addChild(div, "br");
}
function doadd(data) {
    doQ(data, true);
}
//Actually adds the question to the survey (both locally and in database) and updates numList
function doQ(q, dispMess) {
    //alert(dwr.util.toDescriptiveString(q, 2));
    var div = $("surveyPreviewDiv");
    //If there aren't any questions, display the title portion of the survey preview section
    /*
    if(qcnt == 0)
        previewDivHeader();
    */
    
    var afterSel = $("afterSelect");
    var afterVal = afterSel.options[afterSel.selectedIndex].value;
    var ind;
    if(afterVal == ADD_END) {
        ind = qcnt;
    } else if (afterVal == ADD_FRONT) {
        ind = 0;
    } else {
        ind = afterVal;
    }
    //alert(ind);
    ind = Number(ind);
    
    for(j = qcnt-1; j >= ind; j--)
        renumber(j, j+1);
    
    var table = getQuestionTable(ind, q, table);
    if(ind == qcnt) {
        //alert("appending child");
        div.appendChild(table);
    }
    else {
        //alert("q"+(ind+1)+"table");
        div.insertBefore(table, $("q"+(ind+1)+"table"));
    }
    
    //Set the fields to be disabled
    if(!NO_FILTER)
        filterchange(ind);
    
    /*var elts = document.getElementsByTagName("td");
    for(var m = 0; m < elts.length; m++) {
        if(elts[m].id.length > 0) {
            alert(elts[m].id + ": " + elts[m].firstChild.nodeValue);
        }
    }*/
    
    //alert("At this point we would add DWR function to add it dynamically to database");
    //surveyManagerDWR.addQuestionToSurvey(SURVEY_ID, q.id, ind, check_survey);  
    
    //Rather than actually adding the questions to the database survey,
    //we simply add them to the list of question id's
    for(var j = qcnt - 1; j >= ind; j--) {
        qIdList[j+1] = qIdList[j];
    }
    qIdList[ind] = q.id;
    
    qcnt++;
    updateNumList();

    if(qcnt != qIdList.length) {
        alert("Error! qcnt (" + qcnt + ") is not equal to qIdList.length (" + qIdList.length + ")");//*****NOT LANGUAGE SAFE********
    } else if(dispMess) {
        var span = $("successSpan");
        span.className="message";
        clearNodeChildren(span);
        var img = addChild(span, "img");
        img.src = "images/iconInformation.gif";
        img.alt = "Information";
        img.className = "icon";
        addTextChild(span, " Successfully added question: Survey now has " + qcnt + " questions...");//*****NOT LANGUAGE SAFE********
        addChild(span, "br");
        //alert("Survey is now: " + qIdList);
    }
}

function renumber(a, b) {
    //alert("In renumber(" + a + "," + b + ")");
    //alert($("questionTextData"+a));
    if(!NO_FILTER) {
        var leftL = $("availableOptions"+a);
        leftL.id = "availableOptions"+b;
        leftL.name = leftL.id;

        //alert(2);
        var rightL = $("selectedOptions"+a);
        rightL.id = "selectedOptions"+b;
        rightL.name = rightL.id;

        //alert(3);
        var el = $("moveRight"+a);
        el.id = "moveRight"+b;
        el.onclick = function(){moveSelectedOptions(leftL,rightL,true)};

        //alert(4);
        el = $("moveAllRight"+a);
        el.id = "moveAllRight"+b;
        el.onclick=function() {moveAllOptions(leftL,rightL,true)};

        //alert(5);
        el = $("moveLeft"+a)
        el.id = "moveLeft"+b;
        el.onclick=function() {moveSelectedOptions(rightL,leftL,true)};

        //alert(6);
        el = $("moveAllLeft"+a);
        el.id = "moveAllLeft"+b;
        el.onclick=function() {moveAllOptions(rightL,leftL,true)};

        el = $("filter_pickList_" + a);
        el.id = "filter_pickList_" + b;

        //alert(10);
        el = $("filter"+a);
        el.id = "filter"+b;
        el.name = el.id;
        el.onclick = function() {filterchange(b);};

        //alert(11);
        el=$("eligibleCriterion"+a);
        el.id = "eligibleCriterion"+b;
        el.name = el.id;

        el = $("filterName"+a);
        el.id="filterName"+b;
        el.name="filterName"+b;
    }
    
    //alert(7);
    el = $("q"+a+"table");
    el.id = "q"+b+"table";
    
    //alert(8);
    el = $("questionTextData"+a);
    el.id = "questionTextData"+b;
    
    //alert(9);
    q = el.firstChild.nodeValue;
    q = trimLT(q);
    el.firstChild.nodeValue = (" " + (b+1) + q.substring(String(a).length));
    
    //alert(12);
    el=$("remove"+a);
    el.id="remove"+b;
    el.onclick=function() {removeQuestion(b);};
}

function filterchange(num) {
    filterCheck = $("filter" + num);
    //filterNodes = $("availableOptions"+num, "moveRight"+num, "moveAllRight"+num, "moveLeft"+num, "moveAllLeft"+num, "selectedOptions"+num, "eligibleCriterion"+num);
    filterNode = $("filter_pickList_"+num);
    disab = !(filterCheck.checked);
    //for(var j = 0; j < filterNodes.length; j++) {
    if(disab) {
        //filterNodes[j].disabled = true;
        filterNode.style.display="none";
    }
    else {
        //filterNodes[j].disabled = false;
        filterNode.style.display="block";
    }
    //}
}

function formSubmit(form) {
    if(!validateSurveyCreationForm(form)) {
        return false;
    }
    
    if(!bCancel && !$("checkedIRB").checked && !$("surveyRadio1").checked) {
        alert("<fmt:message key="surveyCreationForm.uncheckedIRB"/>");
        return false;
    }

    var j;
    if(!NO_FILTER) {
        if(!bCancel && !validateFilters())
            return false;
    
        if(!bCancel) {
            //Pass in all of the selected filter options if we are continuing
            for(j = 0; j < qcnt; j++) {
                if($("filter"+j).checked) {
                    selectAll("selectedOptions" + j);
                }
            }
        }
    }
    
    //Pass in the list of questions for the survey
    addHiddenChild(form, "qcnt", qcnt);
    for(j = 0; j < qcnt; j++) {
        addHiddenChild(form, "questionId_" + j, qIdList[j]);
    }
    
    if($("surveyRadio2").checked == true) {
        var chkIdx = $("existingSurveySelect").selectedIndex;
        $("id").value = $("existingSurveySelect").options[chkIdx].value;
    } else
        $("id").value="";
        
    return true;
}

function getQuestionTable(num, q) {
    //alert("In getQuestionTable");
    
    //alert(1);
    
    var j, k, text;
    
    var table = document.createElement("table");
    table.id = "q"+num+"table";
    table.className="questionTable";
    
    //This is required for IE (though not for Firefox)
    var tab = document.createElement("tbody");
    table.appendChild(tab);
    
    //alert(2);
    var tr = addChild(tab, "tr");
    
    var td = addChild(tr, "td");
    //td.id="questionTextData"+num;
    //td.setAttribute("valign", "top");
    td.style.verticalAlign = "top";
    
    var surveyRadio3 = $("surveyRadio3");
    var input_img = document.createElement("input");
    input_img.id = "remove"+num;
    input_img.type="image";
    input_img.src = "<c:url value="/images/trash-icon.png"/>";
    input_img.alt = "Remove question";//*****NOT LANGUAGE SAFE********
    input_img.onclick=function() {removeQuestion(num); return false};
    td.appendChild(input_img);
    
    var span = addChild(td, "span");
    span.id = "questionTextData"+num;
    addTextChild(span, " " + (num + 1) + ". " + q.question);
    
    //addTextChild(td, (num + 1) + ". " + q.question);
    
    //alert(3);
    var ul = addChild(td, "ul");
    var li;
    var check, label;
    var select;
    
    //alert(1);
    var optlst = q.optionList;
    if(q.styleString == "DROP_LIST") {
        li = addChild(ul, "li");
        select = addChild(li, "select");
        for(j = 0; j < optlst.length; j++) {
            appendOption(select, optlst[j]);
        }
    }
    else {
        var type = (q.styleString == "RADIO") ? "radio" : "checkbox";
        for(j = 0; j < optlst.length; j++) {
            li = addChild(ul, "li");
            check = document.createElement("input");
            check.type = type;
            li.appendChild(check);
            label = addChild(li, "label");
            addTextChild(label, optlst[j]);
        }
    }
    
    if(!NO_FILTER) {
        td = addChild(tr, "td");
        //td.className="vtop";
        //td.setAttribute("valign", "top");
        td.style.verticalAlign = "top";
        td.style.textAlign = "right";
        
        
        //alert(2);
        check=document.createElement("input");
        check.type="checkbox";
        td.appendChild(check);
        check.id="filter" + num;
        check.name="filter"+num;
        check.onclick = function() {filterchange(num);}
        
        //alert(5);
        label = addChild(td, "label");
        label.htmlFor=check.id;
        addTextChild(label, "Create filter");//*****NOT LANGUAGE SAFE********
        
        addChild(td, "br");
        
        var fs = addChild(td, "fieldset");
        fs.className="pickList";
        fs.id = "filter_pickList_" + num;
        
        //alert(6);
        var legend = addChild(fs, "legend");
        addTextChild(legend, "Filter settings");//*****NOT LANGUAGE SAFE********
        
        var div = addChild(fs, "div");
        addTextChild(div, "Filter Condition name: ");//*****NOT LANGUAGE SAFE********
        var txt = document.createElement("input");
        txt.name="filterName"+num;
        txt.id="filterName"+num;
        txt.type="text";
        div.appendChild(txt);
        
        var leftList = optlst;
        var rightList = null;
        var leftId = "availableOptions"+num;
        var rightId = "selectedOptions"+num;
        var listCount=num;
        fs.appendChild(getPickListTable(leftList, rightList, leftId, rightId, listCount));
        
        
        div = addChild(fs, "div");
        div.className = "eligibleSubjectsDiv";
        addTextChild(div, "Eligible subjects ");//*****NOT LANGUAGE SAFE********
        
        //alert(7);
        var opt;
        select = addChild(div, "select");
        select.id = "eligibleCriterion"+num;
        select.name="eligibleCriterion"+num;
        if(q.styleString=="CHECK_BOX") {
            opt = appendOption(select, "check all");//*****NOT LANGUAGE SAFE********
            opt.value = "CHECK_ALL";
            opt = appendOption(select, "check some");//*****NOT LANGUAGE SAFE********
            opt.value = "CHECK_SOME";
            opt = appendOption(select, "don't check some");//*****NOT LANGUAGE SAFE********
            opt.value = "CHECK_NOT_SOME";
            opt = appendOption(select, "check none");//*****NOT LANGUAGE SAFE********
            opt.value = "CHECK_NONE";
        } else {
            opt = appendOption(select, "select one");//*****NOT LANGUAGE SAFE********
            opt.value = "SELECT";
            opt = appendOption(select, "select none");//*****NOT LANGUAGE SAFE********
            opt.value = "NO_SELECT";
        }
        addChild(div, "br");
        addTextChild(div, " of the filtered options");//*****NOT LANGUAGE SAFE********
    }
    
    return table;
}

function removeQuestion(num) {
    if(!ALLOW_MODIFY) {
        alert('<fmt:message key="surveyCreationForm.otherExpWarning" />');
        return;
    }
    //alert("In removeQuestion(" + num + ")");
    
    var j;
    var div = $("surveyPreviewDiv");
    //If there aren't any questions left, clear the div
    if(qcnt == 1) {
        clearNodeChildren(div);
    }
    else {
        div.removeChild($("q"+num+"table"));
        for(j = num + 1; j < qcnt; j++)
            renumber(j, j-1);
    }
    
    //alert("At this point we would add DWR function to remove it dynamically from database");
    //surveyManagerDWR.removeQuestionFromSurvey(SURVEY_ID, num, check_survey);
    
    //Rather than actually adding the questions to the database survey,
    //we simply add them to the list of question id's
    for(j = num; j <= qcnt-2; j++) {
        qIdList[j] = qIdList[j+1];
    }
    qIdList.length--;
    
    qcnt--;
    updateNumList();
    
    if(qcnt != qIdList.length) {
        alert("Error! qcnt (" + qcnt + ") is not equal to qIdList.length (" + qIdList.length + ")");
    } else {
        var span = $("successSpan");
        span.className="message";
        clearNodeChildren(span);
        var img = addChild(span, "img");
        img.src = "images/iconInformation.gif";
        img.alt = "Information";
        img.className = "icon";
        addTextChild(span, " Successfully removed question: Survey now has " + qcnt + " question(s)...");//*****NOT LANGUAGE SAFE********
        addChild(span, "br");
    }
}

//Returns false to prevent submission
function addQuestion() {
    if(!validateQuestion())
        return;

    select = $("questionSelect");
    styleSelect = $("styleSelect");
    oSelect = $("optionsSelect");
    form = $("surveyCreationForm");
    var q;
    if(select.selectedIndex == 0) {
        if(oSelect.length == 0) {
            alert("Question must have one or more options");
            return false;
        }
        var olst = new Array(oSelect.length);
        for(var j = 0; j < oSelect.length; j++)
            olst[j] = oSelect.options[j].text;
        questionManagerDWR.saveQuestion(
            $("labelText").value,
            $("questionText").value,
            styleSelect.options[styleSelect.selectedIndex].value,
            olst,
            function(data) {doadd(data); updateQuestionList(data);});
    } else {
        questionManagerDWR.getByIdString(questionSelect.options[questionSelect.selectedIndex].value, doadd);
    }
    creating=true;
    
    //Reset the question select field
    select.selectedIndex = 0;
    setupQuestion();
}

function check_survey(survey) {
    var qlst = survey.questions;
    
    var str = "After operations, the survey was: {\n";
    for(var j = 0; j < qlst.length; j++) {
        str += qlst[j].question + ", ";
    }
    str += "}";
    alert(str);
    
    if(qcnt != qlst.length) {
        alert("Error on page!\n\nThe list of questions is not the same length as that in the database.\nPlease restart the form");
        alert("database: " + qlst.length + ", page: " + qcnt);
        return;
    }
    
    var a, b;
    
    for(var j = 0; j < qcnt; j++) {
        a = trimLT(qlst[j].question);
        b = trimLT($("questionTextData" + j).firstChild.nodeValue);
        b = trimLT(b.substring(String(j+1).length + 2));
        if(a != b) {
            alert("Error on page!\n\nQuestion number " + (j+1) + " does not match that in database.\nPlease restart the form");
            alert("database: " + a + ", page: " + b);
            return;
        }
    }
    
    alert("Database has been changed successfully");
}

function formLoad() {
    if(SURVEY_ID != -1)
        surveyManagerDWR.getByIdString(SURVEY_ID, populateSurvey);
}

function otherExpWarning(data) {
    expMessage = "<fmt:message key="surveyCreationForm.otherExpWarning" />: exp(s) ";
    if(data != null && data.length > 0) {
        if(data.length == 1) {
            ess = $("existingSurveySelect");
            opts = ess.options;
            
            // don't show a warning when the only affected experiment is the current one
            if(data[0].id == EXP_ID) {
                ALLOW_MODIFY = true;
                $("questionEditDiv").style.display="block";
                $("name").readOnly = false;
                return;
            }
        }
        ALLOW_MODIFY = false;
        $("questionEditDiv").style.display="none";
        $("name").readOnly = true;
        for (var i = 0; i < data.length; i++) {
            expMessage = expMessage + data[i].id;
            if(i != data.length-1)
                expMessage = expMessage + ", ";
            else
                expMessage = expMessage + ".";
        }
        displayMessage(expMessage);
    } else {
        ALLOW_MODIFY = true;
        $("questionEditDiv").style.display="block";
        $("name").readOnly = false;
    }
}

function populateSurvey(survey) {
    name = $("name");
    name.value=survey.name;
    surVis = $("surveyVisibility");
    opts = surVis.options;
    for(var i = 0; i < opts.length; i++)
        if(survey.visibility == opts[i].value)
            surVis.selectedIndex = i;
            
    var span = $("successSpan");
    clearNodeChildren(span);
    span.className="none";
    
    var div = $("surveyPreviewDiv");
    clearNodeChildren(div);
    previewDivHeader();
    
    var lst = survey.questions;
    var j;
    qcnt=0;
    qIdList = new Array();
    for(j = 0; j < lst.length; j++) {
        doQ(lst[j], false);
    }
    
    //Make sure that all the filters are disabled
    /*for(j = 0; $("filter" + j) != null; j++) {
        filterchange(j);
    }*/
}

function displayMessage(messageToDisplay) {
    var span = $("notificationSpan");
    if(span != null) {
        span.className="message";
        clearNodeChildren(span);
        var img = addChild(span, "img");
        img.src = "images/iconInformation.gif";
        img.alt = "Information";
        img.className = "icon";
        addTextChild(span, messageToDisplay);//*****NOT LANGUAGE SAFE********
        addChild(span, "br");
    }
}

function addOptIfEnter(event) {
    if(isEnterKey(event)) {
        addOption();
        return false;
    }
    return true;
}

function addQIfEnter(event) {
    if(isEnterKey(event)) {
        return addQuestion();
    }
    return true;
}

function updateFields(q) {
    labelText = $("labelText");
    questionText = $("questionText");
    styleSelect = $("styleSelect");
    newOptionText = $("newOptionText");
    optionsSelect = $("optionsSelect");
    
    //alert(dwr.util.toDescriptiveString(q, 2));
    
    //Setup the text input fields
    labelText.value=q.label;
    questionText.value=q.question;
    newOptionText.value="";
    
    //Setup the style select
    style = q.styleString;
    if(style == "CHECK_BOX") {
        styleSelect.selectedIndex = 0;
    }
    else if(style == "RADIO") {
        styleSelect.selectedIndex = 1;
    }
    else {
        styleSelect.selectedIndex = 2;
    }
    
    //Setup the options select
    clear(optionsSelect);
    options = q.optionList;
    for(var j = 0; j < options.length; j++) {
        appendOption(optionsSelect, options[j]);
    }
    
}

function updateQuestionList(q) {
    //alert("Need to write function to update question list");
    var sel = $("questionSelect");
    var opt = appendOption(sel, q.label);
    opt.value = q.id;
}

function updateNumList() {
    var sel = $("afterSelect");
    clear(sel);
    var opt = appendOption(sel, ADD_END);
    opt.value = ADD_END;
    if(qcnt > 0) {
        opt = appendOption(sel, ADD_FRONT);
        opt.value = ADD_FRONT;
    }
    
    for(var j = 1; j < qcnt; j++) {
        opt = appendOption(sel, j);
        opt.value = j;
    }
}

function addOption() {
    newOptionText = $("newOptionText");
    optionsSelect = $("optionsSelect");
    optionText = newOptionText.value;
    if(optionText.length > 0) {
        appendOption(optionsSelect, optionText);
        
        newOptionText.value = "";
    }
    return false;
}

function removeOptions() {
    optionsSelect = $("optionsSelect");
    while(optionsSelect.selectedIndex != -1) {
        optionsSelect.remove(optionsSelect.selectedIndex);
    }
    
    return false;
}

function setupQuestion() {
    questionSelect = $("questionSelect");
    labelText = $("labelText");
    questionText = $("questionText");
    styleSelect = $("styleSelect");
    newOptionText = $("newOptionText");
    optionsSelect = $("optionsSelect");
    addOpt = $("addopt");
    remOpt = $("remopt");
    
    if(questionSelect.selectedIndex == 0) {
        labelText.disabled=false;
        questionText.disabled=false;
        styleSelect.disabled=false;
        newOptionText.disabled=false;
        optionsSelect.disabled=false;
        addOpt.disabled=false;
        remOpt.disabled=false;
        
        //Clear the text input fields
        labelText.value="";
        questionText.value="";
        newOptionText.value="";
        
        //Clear the options select
        clear(optionsSelect);
    }
    else {
        labelText.disabled=true;
        questionText.disabled=true;
        styleSelect.disabled=true;
        newOptionText.disabled=true;
        optionsSelect.disabled=true;
        addOpt.disabled=true;
        remOpt.disabled=true;
        
        //Get the Question via DWR, response function updateFields will do the setup once AJAX is complete
        questionManagerDWR.getByIdString(questionSelect.options[questionSelect.selectedIndex].value, updateFields);
    }
}

function add_tripick(availSel, suffix) {
    alert("in add_tripick: " + suffix);
    var sel = $("select_" + suffix);
    var ind = availSel.selectedIndex;
    while(ind != -1) {
        sel.add(availSel.options[ind]);
        availSel.remove(ind);
        ind = availSel.selectedIndex;
    }
}

function rem_tripick(availSel, suffix) {
    alert("in rem_tripick: " + suffix);
}

function validateQuestion() {
    if($("questionSelect").value == "new") {
        var str = "";
        var OK = true;
        var qlst = $("questionSelect");
        if($("questionText").value.length == 0) {
            str += "Question is a required field\n";
            OK = false;
        }
        if($("labelText").value.length == 0) {
            str += "Question Name is a required field\n";
            OK = false;
        }
        for(j = 1; j < qlst.length; j++) {
            if(qlst.options[j].text == $("labelText").value) {
                str += "Question name has already been used\n";
                OK = false;
            }
        }
        if($("optionsSelect").length == 0) {
            str += "Question must have at least one option\n";
            OK = false;
        }
        if(!OK) {
            alert(str);
            return false;
        }
    }

    return true;
}

function validateFilters() {
    for(j = 0; j < qcnt; j++) {
        if($("filter"+j).checked) {
            if($("filterName" + j).value.length == 0) {
                alert("All filters must be named for later reference");
                return false;
            }
            if($("selectedOptions" + j).length == 0) {
                alert("All filters must have at least one option selected for filtering");
                return false;
            }
        }
    }
    return true;
}

function markIRB(location) {
    var IRB_top = $("checkedIRB");
    var IRB_bottom = $("checkedIRB_bottom");
    if(location == 'top')
        IRB_bottom.checked = IRB_top.checked;
    else
        IRB_top.checked = IRB_bottom.checked;
}
    </script>
</head>

<%--<input type="button" value="Test button" onclick="testFN()"/>--%>

<c:if test="${!noExp}">
<%@ include file="/common/progressDiv.jsp"%>
</c:if>

<form:form action="surveycreationform.html" commandName="survey" id="surveyCreationForm" name="surveyCreationForm" onsubmit="return formSubmit(this);">
    <!-- Parameter values...
    isProf = <c:out value="${isProf}"/> 
    noFilter = <c:out value="${noFilter}"/>
    noExp = <c:out value="${noExp}"/> 
    -->
    <form:errors path="name" cssClass="fieldError"/> 
    
    <script type="text/javascript">SURVEY_ID=${(survey.id == null ? -1 : survey.id)};NO_FILTER=${noFilter};</script>

    <%-- survey setup brought over from old flow... may need formatting --%>
    <ul>
        <li>
            <input type="checkbox" name="checkedIRB" id="checkedIRB" onclick="markIRB('top')"/>&nbsp;<appfuse:label key="survey.checkedIRB"/>
        </li>
        <li <c:choose><c:when test="${noExp}">style="display:none"</c:when><c:otherwise>style="display:block"</c:otherwise></c:choose>>
            <label class="desc"><fmt:message key="experiment.survey"/> <span class="req">*</span></label>
            <ul>
                <li>
                    <input type="radio" id="surveyRadio1" name="surveyRadio" value="NO_SURVEY" onclick="surveyChange(1)" <c:if test="${empty survey.id}">checked</c:if>><label for="surveyRadio1"><fmt:message key="experiment.surveySelect.NO_SURVEY"/></label>
                </li>
                <li>
                    <input type="radio" id="surveyRadio2" name="surveyRadio" value="EXISTING_SURVEY" onclick="surveyChange(2)" <c:if test="${not empty survey.id || status.error}">checked</c:if>><label for="surveyRadio2"><fmt:message key="experiment.surveySelect.EXISTING_SURVEY"/></label>
                    <select name="existingSurvey" id="existingSurveySelect" disabled="true" onchange="surveyChange(2)">
                        <c:forEach items="${surveySet}" var="surveyItem">
                            <option value="${surveyItem.id}" <c:if test="${survey.id == surveyItem.id}">selected</c:if>><c:out value="${surveyItem.name}"/></option>
                        </c:forEach>
                    </select>
                </li>
                <li>
                    <input type="radio" id="surveyRadio3" name="surveyRadio" value="NEW_SURVEY" onclick="surveyChange(3)"><label for="surveyRadio3"><fmt:message key="experiment.surveySelect.NEW_SURVEY"/></label>
                    <select name="newSurvey" id="newSurveySelect" disabled="true" onchange="surveyChange(3)">
                        <option value="new"><fmt:message key="experiment.surveySelect.completelyNew"/></option>
                        <c:forEach items="${surveySet}" var="surveyItem">
                            <option value="${surveyItem.id}"><c:out value="${surveyItem.name}"/></option>
                        </c:forEach>
                    </select>
                </li>
                <!--script type="text/javascript">
                </script-->
            </ul>
        </li>
    </ul>
    <div id="notificationSpan"></div>
    <form:hidden path="id"/>
    <form:hidden path="type"/>
    <c:if test="${not empty expId}">
        <input type="hidden" name="expId" value="${expId}"/>
    </c:if>
    
<%-- show the rest of the elements only when an survey with an id is present --%>
<div id="surveyEditDiv" <c:choose><c:when test="${empty survey.id}">style="display:none"</c:when><c:otherwise>style="display:block"</c:otherwise></c:choose>>
    <h2>Survey Properties</h2>
    
    <c:choose>
        <c:when test="${isProf}">
            <form:hidden path="name"/>
        </c:when>
        <c:otherwise>
            <appfuse:label key="surveyCreationForm.name" colon="false" styleClass="desc"/>
            <form:input path="name"/> <br/>
        </c:otherwise>
    </c:choose>

    <appfuse:label key="surveyCreationForm.introText" colon="false" styleClass="desc"/>
    <form:textarea rows="12" cols="80" path="introText" id="introText" />

    <appfuse:label key="surveyCreationForm.surveyVisibility" colon="false" styleClass="desc"/>
    <select name="surveyVisibility" id="surveyVisibility">
        <c:choose>
            <c:when test="${survey.visibility.name == 'PUBLIC'}">
                <option value="public" selected>Public</option>
                <option value="private">Private</option>
            </c:when>
            <c:otherwise>
                <option value="public">Public</option>
                <option value="private" selected>Private</option>
            </c:otherwise>
        </c:choose>
    </select>
    <hr/>
    <br/>
    <br/>
    
<div id="questionEditDiv" style="display:block">
    <h2>Add Questions</h2>
    
    
    <select id="questionSelect" name="questionSelect" onchange="setupQuestion()">
        <option selected value="new">--New Question--</option>
        <c:forEach items="${questionList}" var="question">
            <option value="${question.id}"><c:out value="${question.label}"/></option>
        </c:forEach>
    </select>
    after question #
    <select name="afterSelect" id="afterSelect">
        <c:forEach items="${numList}" var="anum">
            <option value="${anum}"><c:out value="${anum}"/></option>
        </c:forEach>
    </select>
    
    <br/>
    <br/>
    <fieldset>
        <legend>Question properties</legend>
        <ul>
            <li>
                <label for="questionText" class="desc"><fmt:message key="surveyCreationForm.question"/> <span class="req">*</span></label>
                <%-- <input type="text" name="questionText" id="questionText" class="text large" onkeypress="return addQIfEnter(event);"/> --%>
                <textarea rows="12" cols="80" id="questionText" name="questionText"></textarea>
            </li>
            <li>
                <div class="left">
                    <label for="labelText" class="desc"><fmt:message key="surveyCreationForm.questionName"/> <span class="req">*</span></label>
                    <input type="text" id="labelText" name="labelText" class="text medium" onkeypress="return addQIfEnter(event);"/>
                </div>
                <div>
                    <label for="styleSelect" class="desc"><fmt:message key="surveyCreationForm.questionStyle"/> <span class="req">*</span></label>
                    <select name="styleSelect" id="styleSelect" onkeypress="return addQIfEnter(event);">
                        <c:forEach items="${styleList}" var="stl">
                            <option value="${stl.name}"><c:out value="${stl}"/></option>
                        </c:forEach>
                    </select>
                </div>
            </li>
            <li>
                <label class="desc"><fmt:message key="surveyCreationForm.questionOptions"/> <span class="req">*</span></label>
                <table border="0">
                    <tr>
                        <td valign="center" align="center">
                            <input type="text" id="newOptionText" onkeypress="return addOptIfEnter(event);"/>
                            <br/>
                            <br/>
                            <input type="button" id="addopt" name="addopt" onclick="addOption()" value="Add option >>"/>
                            <br/>
                            <input type="button" id="remopt" name="remopt" onclick="removeOptions()" value="<< Remove option(s)"/>
                        </td>
                        <td valign="center">
                            <select multiple="true" size="5" width="20" id="optionsSelect" class="wideListSelect">
                            </select>
                        </td>
                    </tr>
                </table>
            </li>
        </ul>
        <div align="right"><input type="button" name="addq" value="Add question" onclick="addQuestion();"/></div>
    </fieldset>
    
    <div id="successSpan"></div>
</div>    
    <div id="surveyPreviewDiv"></div>
</div>
<div>
    <input type="checkbox" name="checkedIRB_bottom" id="checkedIRB_bottom" onclick="markIRB('bottom')"/>&nbsp;<appfuse:label key="survey.checkedIRB"/>
</div>
<div id="buttons" class="buttonBar bottom">
    <c:choose>
        <c:when test="${noExp}">
            <input type="submit" class="button"  name="save" value="<fmt:message key='button.save'/>" onclick="bCancel=false"/>
            <input type="submit" class="button" name="cancel" value="<fmt:message key='button.cancel'/>" onclick="bCancel=true"/> 
        </c:when>
        <c:otherwise>
            <input type="submit" class="button" name="back" value="<fmt:message key='button.back'/>" onclick="bCancel=true"/>
            <input type="submit" class="button" name="next" value="<fmt:message key='button.next'/>" onclick="bCancel=false"/>
            <input type="submit" class="button" name="cancel" value="<fmt:message key='button.cancel'/>" onclick="bCancel=true"/> 
            &nbsp;<i>or</i>&nbsp;<input type="submit" class="button" name="savenow" value="<fmt:message key='button.saveNow'/>" onclick="bCancel=false"/> 
        </c:otherwise>
    </c:choose>
</div>
</form:form>

<v:javascript formName="surveyCreationForm" cdata="false" dynamicJavascript="true" staticJavascript="false"/>
<script type="text/javascript" src="<c:url value="/scripts/validator.jsp"/>"></script>

<script type="text/javascript">
    Form.focusFirstElement($('surveyCreationForm'));
    //One of these works for IE6, the other works for firefox
    /*document.body.onload=load;
    onload = load;*/
    // formLoad();
    updateSurvey();
</script>
