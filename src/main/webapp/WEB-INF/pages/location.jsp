<%@ include file="/common/taglibs.jsp"%>

<head> 
    <title><fmt:message key="locationList.title"/></title> 
    <meta name="heading" content="<fmt:message key='locationList.heading'/>"/> 
    <meta name="menu" content="AdminMenu"/>
</head> 

<c:set var="buttons"> 
    <input type="button" style="margin-right: 5px" 
	   onclick="location.href='<c:url value="/editlocation.html"/>'" 
	   value="<fmt:message key="button.add"/>"/> 
    
    <input type="button" onclick="location.href='<c:url value="/administrator.html"/>'" 
	   value="<fmt:message key="button.done"/>"/> 
</c:set> 

<c:out value="${buttons}" escapeXml="false"/> 

<%--Functionality comes from the "name" attribute of the table--%>

<display:table name="locationList" cellspacing="0" cellpadding="0" requestURI="" 
	       id="location" pagesize="25" class="table locationList" export="true" decorator="edu.caltech.ssel.recruiting.webapp.decorator.LocationTableDecorator"> 
    
    <display:column url="/editlocation.html" paramId="id" paramProperty="id" property="name" escapeXml="true" sortable="true" titleKey="location.name"/>
    <display:column property="contact" escapeXml="true" sortable="true" titleKey="location.contact"/> 
    <display:column property="contactEmail" escapeXml="true" sortable="true" titleKey="location.contactEmail"/> 
    <display:column property="phone" escapeXml="true" sortable="true" titleKey="location.phone"/>
    <display:column escapeXml="false" sortable="true" property="myAddr" titleKey="location.addr"/>

    
    <display:setProperty name="paging.banner.item_name" value="location"/> 
    <display:setProperty name="paging.banner.items_name" value="locations"/> 
    
    <display:setProperty name="export.excel.filename" value="Location List.xls"/> 
    <display:setProperty name="export.csv.filename" value="Location List.csv"/> 
    <display:setProperty name="export.pdf.filename" value="Location List.pdf"/> 
</display:table> 

<c:out value="${buttons}" escapeXml="false"/> 

<%--
<script type="text/javascript"> 
    highlightTableRows("locationList"); 
</script> --%>