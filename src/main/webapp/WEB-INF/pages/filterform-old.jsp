<%@ include file="/common/taglibs.jsp"%> 
<%@ taglib prefix="mylib" uri="/WEB-INF/mylib.tld" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<head>
    <title><fmt:message key="filterForm.title"/></title>
    <meta name="heading" content="<fmt:message key='filterForm.heading'/>"/>
    <meta name="menu" content="ExperimenterMenu"/>
    
    <link rel="stylesheet" type="text/css" href="/styles/filterform.css" />
    <link rel="stylesheet" type="text/css" href="/styles/common.css" />
    <link rel="stylesheet" type="text/css" href="/styles/tripick.css" />
    <%--
    NOTE: CSS should be copied to filterform.css and common.css for legibility,
    but while actively working on forms, having it directly in the jsp file is nice
    --%>
    <style type="text/css">
        .offset_title
        {
        color: #3300e0;
        }
    </style>
    
    <script type="text/javascript" src="<c:url value='/scripts/selectbox.js'/>"></script>
    <script type="text/javascript" src="<c:url value='/scripts/common.js'/>"></script>
    <script type='text/javascript' src='<c:url value="/dwr/engine.js"/>'></script>
    <script type='text/javascript' src='<c:url value="/dwr/interface/experimentManagerDWR.js"/>'></script>
    <script type='text/javascript' src='<c:url value="/dwr/interface/filterManagerDWR.js"/>'></script>
    <script type='text/javascript' src='<c:url value="/dwr/interface/conditionManagerDWR.js"/>'></script>
    <script type='text/javascript' src='<c:url value="/dwr/interface/questionManagerDWR.js"/>'></script>
    <script type='text/javascript' src='<c:url value="/dwr/util.js"/>'></script>
    <script type="text/javascript">
var FILTER_ID;
var fIdList;
var EMAIL_LIST_MAX_SIZE;
var FILTER_CURR = 1;
<c:choose>
    <c:when test="${not empty experiment.id}">
var EXP_ID = ${experiment.id};
    </c:when>
    <c:otherwise>
var EXP_ID = -1;
    </c:otherwise>
</c:choose>

<c:choose>
    <c:when test="${not empty experiment.type.id}">
var EXP_TYPE_ID = ${experiment.type.id};
    </c:when>
    <c:otherwise>
var EXP_TYPE_ID = -1;
    </c:otherwise>
</c:choose>

creating = false;

function filterChange(num) {
    ess = $("existingFilterSelect");
    nss = $("newFilterSelect");
    fdiv = $("filterEditDiv");
    
    if(creating && (num == 1 || num == 2))
        if(!confirm("Edits will be lost.  Please confirm.")) {
            $("filterRadio3").checked=true;
            return false;
        } else {
            // BRS 2011-04-29
            $("name").value = "Change it";
        }
        
    var span = $("notificationSpan");
    if(span != null) {
        span.className="none";
        clearNodeChildren(span);
    }
    
    if(num == 3) {
        idx = nss.selectedIndex;
        opts = nss.options;
        
        ess.disabled=true;
        nss.disabled=false;
        
        $("name").readOnly = false;
        for(var num = 1; num < 4; num++) {
            $("addRemRadio"+num).disabled = false;
            $("addRemRadio"+num).checked = false;
        }

        if(opts[idx].value == "new") {
            $("name").value="";
            if($("addRemRadio2") != null) {
                radioChange(1);
                $("addRemRadio2").checked=true;
            }
            populateFilter(null);
        } else
            filterManagerDWR.getByIdString(opts[idx].value, populateFilter);
        
        fdiv.style.display="block";
        creating = true;
    }
    else if(num == 2) {
        idx = ess.selectedIndex;
        opts = ess.options;
        
        ess.disabled=false;
        nss.disabled=true;

		conditionManagerDWR.setupBasicFilterById(opts[idx].value, EXP_TYPE_ID, populateFilter);
        //filterManagerDWR.getByIdString(opts[idx].value, populateFilter);
        experimentManagerDWR.getByFilterId(opts[idx].value, otherExpWarning);
        
        fdiv.style.display="none";
        creating = false;
    }
    else {
        ess.disabled=true;
        nss.disabled=true;
        fdiv.style.display="none";
        creating = false;
		conditionManagerDWR.getEligibleUserStatisticsByFilterConditions(fIdList, setEligible);
    }
    
    FILTER_CURR = num;
}

function updateFilter() {
    for(j = 1; j < 4; j++) {
        if($("filterRadio"+j).checked) {
            filterChange(j);
            break;
        }
    }
	
	conditionManagerDWR.getEligibleUserStatisticsByFilterConditions(fIdList, setEligible);
}

function formSubmit(theform) {
    if(!validateFilterForm(theform))
        return false;
    
    if(!bCancel && !$("checkedIRB").checked && !$("filterRadio1").checked) {
        alert("<fmt:message key="filterForm.uncheckedIRB"/>");
        return false;
    }
    
    var form = $("filterForm");
    addHiddenChild(form, "fcnt", fIdList.length);
    for(j = 0; j < fIdList.length; j++) {
        addHiddenChild(form, "filter_id_"+j, fIdList[j]);
    }
    
    if($("filterRadio2").checked == true) {
        var chkIdx = $("existingFilterSelect").selectedIndex;
        $("id").value = $("existingFilterSelect").options[chkIdx].value;
    } else
        $("id").value="";
        
    return true;
}

function formLoad() {
//Populate condition select
if(FILTER_ID != -1)
    filterManagerDWR.getByIdString(FILTER_ID, populateFilter);
}

function otherExpWarning(data) {
    expMessage = "<fmt:message key="filterForm.otherExpWarning" />: exp(s) ";
    if(data != null && data.length > 0) {
        if(data.length == 1) {
            ess = $("existingFilterSelect");
            opts = ess.options;
            // don't show a warning when the only affected experiment is the current one
            if(data[0].id == EXP_ID) {
                $("name").readOnly = false;
                for(var num = 1; num < 5; num++)
                    $("addRemRadio"+num).disabled = false;
                return;
            }
        }
        $("name").readOnly = true;
        for(var num = 1; num < 4; num++) {
            $("addRemRadio"+num).disabled = true;
            $("addRemRadio"+num).checked = false;
        }
        $("addRemRadio4").checked = true;
        radioChange(4);
        for (var i = 0; i < data.length; i++) {
            expMessage = expMessage + data[i].id;
            if(i != data.length-1)
                expMessage = expMessage + ", ";
            else
                expMessage = expMessage + ".";
        }
        displayMessage(expMessage);
    } else {
        $("name").readOnly = false;
        for(var num = 1; num < 5; num++)
            $("addRemRadio"+num).disabled = false;
    }
}

function populateFilter(filter) {
    var select = $("conditionSelect");
    // rebuild the condition select
    clearNodeChildren(select);
    var newOpt = addChild(select, "option");
    newOpt.value="NEW_FILTER";
    addTextChild(newOpt, "--Select Condition--");
    if(filter == null) {
        fIdList = new Array(0);
        return;
    }
    
    $("name").value=filter.name;
    var lst = filter.conditions;
    fIdList = new Array(lst.length);
    
    var opt;
    for(j = 0; j < fIdList.length; j++) {
        fIdList[j] = lst[j].id;
        opt = appendOption(select, lst[j].name);
        opt.value = j;
    }
    select.selectedIndex=0;
	
	conditionManagerDWR.getEligibleUserStatisticsByFilterConditions(fIdList, setEligible);
    
//    if($("addRemRadio1") != null) {
//        radioChange(1);
//        $("addRemRadio1").checked=true;
//    }
}

function displayMessage(messageToDisplay) {
    var span = $("notificationSpan");
    if(span != null) {
        clearNodeChildren(span);
        span.className="message";
        var img = addChild(span, "img");
        img.src = "images/iconInformation.gif";
        img.alt = "Information";
        img.className = "icon";
        addTextChild(span, messageToDisplay);//*****NOT LANGUAGE SAFE********
        addChild(span, "br");
    }
}

function radioChange(num) {
    var remDiv = $("remDiv");
    var addNewDiv = $("addNewDiv");
    var addOldDiv = $("addOldDiv");
    //var viewEligDiv = $("viewEligDiv");
    if(num == 1) {
        remDiv.style.display="block";
        addNewDiv.style.display="none";
        addOldDiv.style.display="none";
        //viewEligDiv.style.display="none";
    } else if (num == 2) {
        remDiv.style.display="none";
        addNewDiv.style.display="none";
        addOldDiv.style.display="block";
        //viewEligDiv.style.display="none";
    } else if (num == 3) {
        remDiv.style.display="none";
        addNewDiv.style.display="block";
        addOldDiv.style.display="none";
        //viewEligDiv.style.display="none";
    } else if (num == 4) {
        remDiv.style.display="none";
        addNewDiv.style.display="none";
        addOldDiv.style.display="none";
        //viewEligDiv.style.display="block";
    }
    
	conditionManagerDWR.getEligibleUserStatisticsByFilterConditions(fIdList, setEligible);
    clearMessage();
}

function clearMessage() {
    var div = $("messageDiv");
    div.className="";
    clearNodeChildren(div);
}

function conditionSelectChange() {
    var sel = $("conditionSelect");
    var fs = $("existentFilterFS");
    
    //If --Select filter-- is selected
    if(sel.value == 'NEW_FILTER') {
        fs.style.display="none";
    } else {
        fs.style.display="block";
        conditionManagerDWR.getByIdString(fIdList[sel.value], function(data) {  $("removeFilterButton").onclick=function(){removeFilter(sel.value)}; displayInfo(data, $("filterInfoSpanText"));});
    }
    
    clearMessage();
}

function oldConditionSelectChange() {
    var sel = $("oldConditionSelect");
    var fs = $("oldExistentFilterFS");
    
    //If --Select filter-- is selected
    if(sel.value == 'NEW_FILTER') {
        fs.style.display="none";
    } else {
        fs.style.display="block";
        conditionManagerDWR.getByIdString(sel.value, function(data) { $("addOldButton").onclick=function(){addOldFilter(data)}; displayInfo(data, $("oldFilterInfoSpanText"));});
    }
    
    clearMessage();
}

function addOldFilter(cond) {
    //Reset the old filter fields
    var sel=$("oldConditionSelect");
    sel.selectedIndex = 0;
    oldConditionSelectChange();//Update the condition select
    
    addFilter(cond);//, "<fmt:message key='filterForm.addError'/>", "<fmt:message key='filterForm.addSuccess'/>");//Performs the adding to the actual filter
}

function addFilter(cond) { //, errMsg, succMsg) {
    errMsg = "<fmt:message key='filterForm.addError'/>";
    succMsg = "<fmt:message key='filterForm.addSuccess'/>";
    var sel = $("conditionSelect");
    var opt = appendOption(sel, cond.name);
    opt.value = fIdList.length;
    
    fIdList.length++;
    fIdList[fIdList.length-1] = cond.id;
    
    
    if(!validLists()) {
        setMessage(" " + errMsg, false);
    } else {
        setMessage(" " + succMsg + " " + fIdList.length + " <fmt:message key='filterForm.filters'/>", true);
    }
	
	conditionManagerDWR.getEligibleUserStatisticsByFilterConditions(fIdList, setEligible);
    /*var div = $("messageDiv");
    div.className="message";
    clearNodeChildren(div);
    var img = addChild(div, "img");
    img.className = "icon";
     {
        img.src = "/images/iconWarning.gif";
        img.alt = "Error";
        addTextChild(div, " " + errMsg);
    } else {
        img.src = "images/iconInformation.gif";
        img.alt = "Information";
        addTextChild(div, " " + succMsg + " " + fIdList.length + " <fmt:message key='filterForm.filters'/>");
    }
    addChild(div, "br");*/
}

function validLists() {
    var sel = $("conditionSelect");
    if(fIdList.length != (sel.length-1)) {
        alert("The lists do not match in length");
        return false;
    }
    for(j = 1; j < sel.length; j++) {
        if(sel.options[j].value != (j-1)) {
            alert("The options list has an error at index " + (j-1));
            return false;
        }
    }
    return true;
}

function displayInfo(cond, txt) {
    clearNodeChildren(txt);
    var ul = addChild(txt, "ul");
    var li;
    var str;
    var PAST = '<fmt:message key="filterForm.past"/>';
    var FUTURE = '<fmt:message key="filterForm.future"/>';
    var ON = '<fmt:message key="filterForm.on"/>';
    var OFF = '<fmt:message key="filterForm.off"/>';
    if(cond.conditionType == "QuestionResultCondition") {
        li = addChild(ul, "li");
        addTextChild(li, "<fmt:message key='filterForm.question'/>: " + cond.question.question);
        li = addChild(ul, "li");
        str = "<fmt:message key='filterForm.filteredOptions'/>: ";
        for(j = 0; j < cond.filterOptions.length; j++) {
            str += cond.question.optionList[cond.filterOptions[j]];
            if(j != cond.filterOptions.length - 1) {
                str += ", "
            }
        }
        addTextChild(li, str);
        li = addChild(ul, "li");
        addTextChild(li, "<fmt:message key='filterForm.filterType'/>: " + cond.filterType.stringType);
    } else if(cond.conditionType == "ExperimentTypeCondition") {
        li = addChild(ul, "li");
        addTextChild(li, "<fmt:message key='filterForm.filterOn'/> " + (cond.history ? PAST : FUTURE) + " <fmt:message key='filterForm.experiments'/>");
        li = addChild(ul, "li");
        str = "<fmt:message key='filterForm.filteredTypes'/>: ";
        for(j = 0; j < cond.types.length; j++) {
            str += cond.types[j].name;
            if(j != cond.types.length-1) {
                str += ", ";
            }
        }
        addTextChild(li, str);
        li = addChild(ul, "li");
        addTextChild(li, "<fmt:message key='filterForm.shouldBe'/> " + (cond.onList ? ON : OFF) + " <fmt:message key='filterForm.list'/>");
    } else if(cond.conditionType == "PoolCondition") {
        li = addChild(ul, "li");
        str = (cond.memberOf ? "<fmt:message key='filterForm.In'/>" : "<fmt:message key='filterForm.NotIn'/>") + " ";
        var sz = cond.pools.length;
        for(j = 0; j < sz; j++) {
            str += cond.pools[j].name;
            if(j == sz - 2) {
                str += " <fmt:message key='filterForm.or'/> ";
            } else if(j != sz - 1) {
                str += ", ";
            }
        }
        addTextChild(li, str);
    } else if(cond.conditionType == "BooleanCondition") {
        li = addChild(ul, "li");
        var str = "first_id = " + cond.firstCondition;
        var oldSel = $("oldConditionSelect");
        for(j = 1; j < oldSel.length; j++) {
            if(oldSel.options[j].value == cond.firstCondition) {
                str = oldSel.options[j].text;
                break;
            }
        }
        addTextChild(li, str);
        li = addChild(ul, "li");
        addTextChild(li, (cond.and ? "<fmt:message key='filterForm.AND'/>" : "<fmt:message key='filterForm.OR'/>"));
        li = addChild(ul, "li");
        str = "second_id = " + cond.secondCondition;
        for(j = 1; j < oldSel.length; j++) {
            if(oldSel.options[j].value == cond.secondCondition) {
                str = oldSel.options[j].text;
                break;
            }
        }
        addTextChild(li, str);

    //BRS
    } else if(cond.conditionType == "NoShowCondition") {
        li = addChild(ul, "li");
        addTextChild(li, "Maximum no-shows: " + cond.maxNoShows);
    }
}

function removeFilter(index) {
    var sel = $("conditionSelect");
    
    var ind = index*1;//Cast to a number
    //Need to add 1 to indices because we have the first option "--Choose--"
    sel.remove(ind + 1);
    for(j = ind + 1; j < sel.length; j++) {
        sel.options[j].value--;
        fIdList[j-1] = fIdList[j];
    }
    fIdList.length--;
    
    sel.selectedIndex = 0;
    conditionSelectChange();//Update the condition select
    
    if(!validLists()) {
        setMessage(" <fmt:message key='filterForm.removeError'/>", false);
        alert("j = " + j + ", sel.length = " + sel.length + ", fIdList.length = " + fIdList.length);
    } else {
        setMessage(" <fmt:message key='filterForm.removeSuccess'/> " + fIdList.length + " <fmt:message key='filterForm.filters'/>", true);
    }
    
	conditionManagerDWR.getEligibleUserStatisticsByFilterConditions(fIdList, setEligible);
    /*var div = $("messageDiv");
    div.className="message";
    clearNodeChildren(div);
    var img = addChild(div, "img");
    img.className = "icon";
    if(!validLists()) {
        img.src = "/images/iconWarning.gif";
        img.alt = "Error";
        addTextChild(div, " <fmt:message key='filterForm.removeError'/>");
        alert("j = " + j + ", sel.length = " + sel.length + ", fIdList.length = " + fIdList.length);
    } else {
        img.src = "images/iconInformation.gif";
        img.alt = "Information";
        addTextChild(div, " <fmt:message key='filterForm.removeSuccess'/> " + fIdList.length + " <fmt:message key='filterForm.filters'/>");
    }
    addChild(div, "br");*/
}

function changeNewFilterType() {
    var lst = $("QRC", "ETC", "SPC", "BC", "NSC", "newCondFS", "nameDiv");
    for(j = 0; j < lst.length; j++) {
        lst[j].style.display="none";
    }
    var sel = $("newFilterTypeSelect");
    if(sel.value != "NEW") {
        $(sel.value).style.display = "block";
        $("newCondFS").style.display = "block";
        $("nameDiv").style.display = "block";
        initFilterType(sel.value);
    }
    clearMessage();
}

function initFilterType(type) {
    var addButton = $("addExpButton");
    var saveButton = $("saveLaterButton");
    if(type == "QRC") {
        addButton.onclick = function() {if(validateQuestionFilter()) {saveQRC(true);}};
        saveButton.onclick = function() {if(validateQuestionFilter()) {saveQRC(false);}};
        /*addButton.disabled=true;
        saveButton.disabled=true;*/
        questionSelectChange();//Should properly set disabled-ness of buttons
    } else if(type == "ETC") {
        addButton.onclick = function() {if(validateExperimentTypeFilter()) {saveETC(true);}};
        saveButton.onclick = function() {if(validateExperimentTypeFilter()) {saveETC(false);}};
        addButton.disabled=false;
        saveButton.disabled=false;
        init_etc();
    } else if(type == "SPC") {
        addButton.onclick = function() {if(validateSubjectPoolFilter()) {saveSPC(true);}};
        saveButton.onclick = function() {if(validateSubjectPoolFilter()) {saveSPC(false);}};
        addButton.disabled=false;
        saveButton.disabled=false;
        init_spc();
    } else if(type == "BC") {
        addButton.onclick = function() {if(validateBooleanFilter()) {saveBC(true);}};
        saveButton.onclick = function() {if(validateBooleanFilter()) {saveBC(false);}};
        /*addButton.disabled=true;
        saveButton.disabled=true;*/
        bcSelectChange();//Should properly set disabled-ness of buttons
    
    // BRS
    } else if(type == "NSC") {
        addButton.onclick = function() {if(validateNoShowCondition()) {saveNSC(true);}};
        saveButton.onclick = function() {if(validateNoShowCondition()) {saveNSC(false);}};
        addButton.disabled=false;
        saveButton.disabled=false;
    }
}

function saveQRC(addToExp) {
    //Save relevant values for DWR
    var name = $("nameInput").value;
    var q_id = $("questionSelect").value;
    var type = $("eligibleCriterion").value;
    var sel = $("selectedOptions");
    var opts = new Array(sel.length);
    for(j = 0; j < sel.length; j++) {
        opts[j] = sel.options[j].value;
    }
    
    //Reset relevant fields
    $("nameInput").value = "";
    $("questionSelect").selectedIndex=0;
    questionSelectChange();
    
    //Do DWR, pass result to handleCondition(cond, addToExp)
    //public QuestionResultCondition createQuestionResultCondition(String name, String q_id, String[] opts, String type) throws Exception;
    conditionManagerDWR.createQuestionResultCondition(name, q_id, opts, type, function(cond){handleNewCondition(cond, addToExp);});
}

function saveETC(addToExp) {
    //Save relevant values for DWR
    var name = $("nameInput").value;
    var history = ($("historySelect").selectedIndex == 0);
    var selIn = $("select_in1");
    var selOut = $("select_out1");
    var typeIdsIn = new Array(selIn.length);
    var typeIdsOut = new Array(selOut.length);
    for(j = 0; j < selIn.length; j++) {
        typeIdsIn[j] = selIn.options[j].value;
    }
    for(j = 0; j < selOut.length; j++) {
        typeIdsOut[j] = selOut.options[j].value;
    }
    
    //Reset relevant fields
    if(selIn.length == 0 && selOut.length == 0) {
        setMessage("<fmt:message key='filterForm.mustSelect'/>", false);
    } else {
        $("nameInput").value = "";
        $("newFilterTypeSelect").selectedIndex=0;
        changeNewFilterType();
        
        //Do DWR, pass result to handleNewCondition(cond, addToExp)
        //public ExperimentTypeCondition createExperimentTypeCondition(String name, String [] typeIds, String onList, String history) throws Exception;
        
        if(selIn.length != 0)
            conditionManagerDWR.createExperimentTypeCondition(name+"-In", typeIdsIn, "true", history, function(cond){handleNewCondition(cond, addToExp);});
            
            if(selOut.length != 0)
                conditionManagerDWR.createExperimentTypeCondition(name+"-Out", typeIdsOut, "false", history, function(cond){handleNewCondition(cond, addToExp);});
    }
}

function saveSPC(addToExp) {
    //Save relevant values for DWR
    var name = $("nameInput").value;
    var selIn = $("select_in2");
    var selOut = $("select_out2");
    var poolIdsIn = new Array(selIn.length);
    var poolIdsOut = new Array(selOut.length);
    for(j = 0; j < selIn.length; j++) {
        poolIdsIn[j] = selIn.options[j].value;
    }
    for(j = 0; j < selOut.length; j++) {
        poolIdsOut[j] = selOut.options[j].value;
    }
    
    if(selIn.length == 0 && selOut.length == 0) {
        setMessage("<fmt:message key='filterForm.mustSelect'/>", false);
    } else {
        //Reset relevant fields
        $("nameInput").value = "";
        $("newFilterTypeSelect").selectedIndex=0;
        changeNewFilterType();
        
        //Do DWR, pass result to handleNewCondition(cond, addToExp)
        if(selIn.length != 0)
            conditionManagerDWR.createPoolCondition(name+"-In", poolIdsIn, "true",  function(cond){handleNewCondition(cond, addToExp);});
            
        if(selOut.length != 0)
            conditionManagerDWR.createPoolCondition(name+"-Out", poolIdsOut, "false",  function(cond){handleNewCondition(cond, addToExp);});
    }
}

function saveBC(addToExp) {
    //alert("In saveBC(" + addToExp + ")");
    //Save relevant values for DWR
    var name = $("nameInput").value;
    var first_cond_id = $("firstConditionSelect").value;
    var second_cond_id = $("secondConditionSelect").value;
    var op = $("bcOpSelect").value;
    
    //Reset relevant fields
    $("nameInput").value = "";
    $("firstConditionSelect").selectedIndex = 0;
    $("secondConditionSelect").selectedIndex = 0;
    bcSelectChange();
    
    //Do DWR, pass result to handleCondition(cond, addToExp)
    conditionManagerDWR.createBooleanCondition(name, first_cond_id, second_cond_id, op,  function(cond){handleNewCondition(cond, addToExp);});
}

// BRS: save NoShowCondition
function saveNSC(addToExp) {
    var name = $("nameInput").value;
    var maxNoShows = $("maxNoShows").value;
    
    //Reset relevant fields
    $("nameInput").value = "";
    $("maxNoShows").value = "";

    //Do DWR, pass result to handleCondition(cond, addToExp)
    conditionManagerDWR.createNoShowCondition(name, maxNoShows, function(cond){handleNewCondition(cond, addToExp);});
}

function handleNewCondition(cond, addToExp) {
    //Add to lists of all filters
    var selList = $("oldConditionSelect", "firstConditionSelect", "secondConditionSelect");
    var opt;
    for(j = 0; j < selList.length; j++) {
        opt = appendOption(selList[j], cond.name);
        opt.value = cond.id;
    }
    
    //If addToExp, add to experiment (using function above)
    if(addToExp) {
        addFilter(cond);
    } else {    //If not, display successful save message
        setMessage("<fmt:message key='filterForm.createSuccess'/>", true);
    }
}

function questionSelectChange() {
    var div = $("qrcInternalDiv");
    var sel = $("questionSelect");
    var lst = $("addExpButton", "saveLaterButton");
    clearNodeChildren(div);
    clearMessage();
    if(sel.value != "NEW") {
        for(j = 0; j < lst.length; j++)
            lst[j].disabled=false;
        questionManagerDWR.getByIdString(sel.value, init_qrc);
    } else {
        for(j = 0; j < lst.length; j++)
            lst[j].disabled=true;
    }
}

function init_qrc(q) {
    var div = $("qrcInternalDiv");
    addTextChild(div, q.question);
    var ul = addChild(div, "ul");
    var li, check, select, label;
    var optlst = q.optionList;
    if(q.styleString == "DROP_LIST") {
        li = addChild(ul, "li");
        select = addChild(li, "select");
        for(j = 0; j < optlst.length; j++) {
            appendOption(select, optlst[j]);
        }
    }
    else {
        var type = (q.styleString == "RADIO") ? "radio" : "checkbox";
        for(j = 0; j < optlst.length; j++) {
            li = addChild(ul, "li");
            check = document.createElement("input");
            check.type = type;
            li.appendChild(check);
            label = addChild(li, "label");
            addTextChild(label, optlst[j]);
        }
    }
    
    var leftList = optlst;
    var rightList = null;
    var leftId = "availableOptions";
    var rightId = "selectedOptions";
    var listCount="";
    var tab = getPickListTable(leftList, rightList, leftId, rightId, listCount);
    div.appendChild(tab);
    //********** THESE WIDTHS SHOULD NOT HAVE TO BE HARD-CODED (but they'll work for now) ***************
    $(leftId).style.width="200px";
    $(rightId).style.width="200px";
    
    var div2 = addChild(div, "div");
    addTextChild(div2, "Eligible subjects ");//*****NOT LANGUAGE SAFE********
    
    //alert(7);
    var opt;
    select = addChild(div2, "select");
    select.id = "eligibleCriterion";
    select.name="eligibleCriterion";
    if(q.styleString=="CHECK_BOX") {
        opt = appendOption(select, "check all");//*****NOT LANGUAGE SAFE********
        opt.value = "CHECK_ALL";
        opt = appendOption(select, "check some");//*****NOT LANGUAGE SAFE********
        opt.value = "CHECK_SOME";
        opt = appendOption(select, "don't check some");//*****NOT LANGUAGE SAFE********
        opt.value = "CHECK_NOT_SOME";
        opt = appendOption(select, "check none");//*****NOT LANGUAGE SAFE********
        opt.value = "CHECK_NONE";
    } else {
        opt = appendOption(select, "select one");//*****NOT LANGUAGE SAFE********
        opt.value = "SELECT";
        opt = appendOption(select, "select none");//*****NOT LANGUAGE SAFE********
        opt.value = "NO_SELECT";
    }
    addTextChild(div2, " of the filtered options");//*****NOT LANGUAGE SAFE********
    
}

function init_etc() {
    moveAllOptions($('select_in1'), $('avail1'), true);
    moveAllOptions($('select_out1'), $('avail1'), true);
}

function init_spc() {
    moveAllOptions($('select_in2'), $('avail2'), true);
    moveAllOptions($('select_out2'), $('avail2'), true);
}

function historySelectChange() {
    var sel = $("historySelect");
    var head = $("tripick_head_types");
    //Set the proper heading for the pick list
    if(sel.selectedIndex == 0) {
        head.firstChild.nodeValue="<fmt:message key='eligText.typePickPast'/>";
    } else {
        head.firstChild.nodeValue="<fmt:message key='eligText.typePickFuture'/>";
    }
}

function setMessage(msgTxt, isSuccess) {
    var div = $("messageDiv");
    div.className="message";
    clearNodeChildren(div);
    var img = addChild(div, "img");
    img.className = "icon";
    if(!isSuccess) {
        img.src = "/images/iconWarning.gif";
        img.alt = "Error";
        addTextChild(div, " " + msgTxt);
    } else {
        img.src = "images/iconInformation.gif";
        img.alt = "Information";
        addTextChild(div, " " + msgTxt);
    }
    addChild(div, "br");
}

function bcSelectChange() {
    var lst = $("addExpButton", "saveLaterButton");
    var lst2 = $("firstConditionSelect", "secondConditionSelect");
    var dis = false;
    
    for(j = 0; j < lst2.length; j++) {
        if(lst2[j].selectedIndex == 0) {
            dis = true;
        }
    }
    
    for(j = 0; j < lst.length; j++) {
        lst[j].disabled = dis;
    }
}

function setEligible(str) {
    var cntSpan = $("eligCntSpan");
    
    clearNodeChildren(cntSpan);
    
    addTextChild(cntSpan, str);
}

function min(a, b) {
    return (a < b) ? a : b;
}

function validateFilterName() {
    if($("nameInput").value.length < 0) {
        alert("Filter name is a required field");
        return false;
    }
    
    var oldSel = $("oldConditionSelect");
    for(j = 1; j < oldSel.length; j++) {
        if(oldSel.options[j].text == $("nameInput").value) {
            alert("Name has already been used");
            return false;
        }
    }
    
    return true;
}

function validateQuestionFilter() {
    if(!validateFilterName()) {
        return false;
    }
    
    if($("questionSelect").value == "NEW") {
        alert("You must select a question");
        return false;
    }

    if($("selectedOptions").length == 0) {
        alert("You must select at least one option to filter on");
        return false;
    }
    
    return true;
}

function validateExperimentTypeFilter() {
    if(!validateFilterName()) {
        return false;
    }
    
    if($("select_in1").length == 0 && $("select_out1").length==0) {
        alert("You must select at least one type to filter on");
        return false;
    }
    
    return true;
}

function validateSubjectPoolFilter() {
    if(!validateFilterName()) {
        return false;
    }

    if($("select_in2").length == 0 && $("select_out2").length==0) {
        alert("You must select at least one pool to filter on");
        return false;
    }
    
    return true;
}

function validateBooleanFilter() {
    if(!validateFilterName()) {
        return false;
    }

    if($("firstConditionSelect").value == "NEW_FILTER" || $("secondConditionSelect").value == "NEW_FILTER") {
        alert("You must select conditions");
        return false;
    }
    
    return true;
}

// BRS: validate NoShowCondition
function validateNoShowCondition() {
    if(!validateFilterName()) {
        return false;
    }

    var maxNoShows = $("maxNoShows").value;
    if (isNaN(parseInt(maxNoShows))) {
        alert("Please enter a valid number of maximum no-shows.");
        return false;
    }
    
    return true;
}

function markIRB(location) {
    var IRB_top = $("checkedIRB");
    var IRB_bottom = $("checkedIRB_bottom");
    if(location == 'top')
        IRB_bottom.checked = IRB_top.checked;
    else
        IRB_top.checked = IRB_bottom.checked;
}
    </script>
</head>


<%@ include file="/common/progressDiv.jsp"%>

<form:form id="filterForm" commandName="filter" name="filterForm" onsubmit="return formSubmit(this);" action="filterform.html">
    <form:errors path="name" cssClass="fieldError"/> 
        <ul>
            <li>
                <input type="checkbox" name="checkedIRB" id="checkedIRB" onclick="markIRB('top')"/>&nbsp;<appfuse:label key="filter.checkedIRB"/>
            </li>
            <li>
                <label class="desc"><fmt:message key="experiment.filter"/> <span class="req">*</span></label>
                <ul>
                    <li>
                        <input type="radio" id="filterRadio1" name="filterRadio" value="NO_FILTER" onclick="filterChange(1)" <c:if test="${empty filter.id}">checked</c:if>><label for="filterRadio1"><fmt:message key="experiment.filterSelect.NO_FILTER"/></label>
                    </li>
                    <li>
                        <input type="radio" id="filterRadio2" name="filterRadio" value="EXISTING_FILTER" onclick="filterChange(2)" <c:if test="${not empty filter.id}">checked</c:if>><label for="filterRadio2"><fmt:message key="experiment.filterSelect.EXISTING_FILTER"/></label>
                        <select name="existingFilter" id="existingFilterSelect" disabled="true" onchange="filterChange(2)">
                            <c:forEach items="${filterList}" var="filterItem">
                                <c:if test="${filterItem.basic}"><option value="${filterItem.id}"
                                        <c:if test="${filter.id == filterItem.id}">selected</c:if>
                                ><c:out value="${filterItem.name}"/></option></c:if>
                            </c:forEach>
                        </select>
                    </li>
                    <li>
                        <input type="radio" id="filterRadio3" name="filterRadio" value="NEW_FILTER" onclick="filterChange(3)"><label for="filterRadio3"><fmt:message key="experiment.filterSelect.NEW_FILTER"/></label>
                        <select name="newFilter" id="newFilterSelect" disabled="true" onchange="filterChange(3)">
                            <option value="new"><fmt:message key="experiment.surveySelect.completelyNew"/></option>
                            <c:forEach items="${filterList}" var="filterItem">
                                <option value="${filterItem.id}"><c:out value="${filterItem.name}"/></option>
                            </c:forEach>
                        </select>
                    </li>
                </ul>
            </li>
        </ul>
    <div id="notificationSpan"></div>
    
    <form:hidden path="id"/>
    <c:if test="${not empty expId}">
        <input type="hidden" name="expId" value="${expId}"/>
    </c:if>
    
    <c:set var="noExp" value="${empty expId}"/>
    <c:set var="clst" value="${condList}"/>
    
    <script type="text/javascript">
        FILTER_ID = ${(filter.id == null ? -1 : filter.id)};
    </script>
    
<%-- show the rest of the elements only when an survey with an id is present --%>
<div id="filterEditDiv" <c:choose><c:when test="${empty filter.id}">style="display:none"</c:when><c:otherwise>style="display:block"</c:otherwise></c:choose>>
    <input type="radio" id="addRemRadio1" name="addRemRadio" value="1" onclick="radioChange(1);" <c:if test="${not empty filter.conditions}">checked</c:if>><label for="remRadio"><fmt:message key="filterForm.removeFilter"/></label>
    <br>
    <input type="radio" id="addRemRadio2" name="addRemRadio" value="2" onclick="radioChange(2);" style="display:none" <c:if test="${empty filter.conditions}">checked</c:if>><label for="addExistingRadio" style="display:none"><fmt:message key="filterForm.addOldFilter"/></label>
    <br>
    <input type="radio" id="addRemRadio3" name="addRemRadio" value="3" onclick="radioChange(3);"><label for="addNewRadio"><fmt:message key="filterForm.addNewFilter"/></label>
    <br>
    <input type="radio" id="addRemRadio4" name="addRemRadio" value="4" onclick="radioChange(4);" style="display:none"><label for="viewEligRadio" style="display:none"><fmt:message key="filterForm.viewEligible"/></label>
    
    <br>
    <p>
        <div id="remDiv">
            <ul>
                <li>
                    <select id="conditionSelect" onchange="conditionSelectChange();">
                        <option value="NEW_FILTER" selected><fmt:message key="filterForm.selectFilter"/></option>
                    </select>
                </li>
                <fieldset id="existentFilterFS" style="display:none">
                    <legend><fmt:message key="filterForm.filterInformation"/></legend>
                    <span id="filterInfoSpanText"></span>
                    <div align="right">
                        <input type="button" id="removeFilterButton" value="<fmt:message key='filterForm.remExp'/>"/>
                    </div>
                </fieldset>
                <%--<span id="existentFilterSpan" style="display:none">
                    <li>
                        <b><fmt:message key="filterForm.filterInformation"/>: </b><span id="filterInfoSpanText"></span>
                    </li>
                    <li>
                        <input type="button" id="removeFilterButton" value="<fmt:message key='filterForm.remExp'/>"/>
                    </li>
                </span>--%>
            </ul>
        </div>
        
        <div id="addOldDiv" style="display:none">
            <ul>
                <li>
                    <select id="oldConditionSelect" onchange="oldConditionSelectChange();">
                        <option value="NEW_FILTER" selected><fmt:message key="filterForm.selectFilter"/></option>
                        <c:forEach items="${condList}" var="cond">
                            <option value="${cond.id}"><c:out value="${cond.name}"/></option>
                        </c:forEach>
                    </select>
                </li>
                <fieldset id="oldExistentFilterFS" style="display:none">
                    <legend><fmt:message key="filterForm.filterInformation"/></legend>
                    <span id="oldFilterInfoSpanText"></span>
                    <div align="right">
                        <input type="button" id="addOldButton" value="<fmt:message key='filterForm.addExp'/>"/>
                    </div>
                </fieldset>
                <%--<span id="oldExistentFilterSpan" style="display:none">
                    <li>
                        <b><fmt:message key="filterForm.filterInformation"/>: </b><span id="oldFilterInfoSpanText"></span>
                    </li>
                    <li>
                        <input type="button" id="addOldButton" value="<fmt:message key='filterForm.addExp'/>"/>
                    </li>
                </span>--%>
            </ul>
        </div>
        
        <div id="addNewDiv" style="display:none">
            <select id="newFilterTypeSelect" onchange="changeNewFilterType();">
                <option selected value="NEW"><fmt:message key="filterForm.selectFilterType"/></option>
                <option value="QRC"><fmt:message key="filterForm.QRC"/></option>
                <option value="ETC"><fmt:message key="filterForm.ETC"/></option>
                <option value="SPC"><fmt:message key="filterForm.SPC"/></option>
                <option value="BC"><fmt:message key="filterForm.BC"/></option>
                <option value="NSC"><fmt:message key="filterForm.NSC"/></option>
            </select>
            <br>
            <br>
            <fieldset id="newCondFS" style="display:none">
                <legend id="newCondLegend"><fmt:message key="filterForm.settings"/></legend>
                <div id="nameDiv" style="display:none">
                    <fmt:message key="filterForm.filterName"/>: <input type="text" id="nameInput" class="text large"/>
                    <br>
                    <br>
                </div>
                <div id="QRC" style="display:none">
                    <select id="questionSelect" onchange="questionSelectChange();">
                        <option selected value="NEW"><fmt:message key="filterForm.selectQuestion"/></option>
                        <c:forEach items="${qlist}" var="question">
                            <option value="${question.id}"><c:out value="${question.label}"/></option>
                        </c:forEach>
                    </select>
                    <br>
                    <br>
                    <div id="qrcInternalDiv">
                    </div>
                </div>
                <div id="ETC" style="display:none">
                    <fmt:message key="filterForm.filterOn"/>
                    <select id="historySelect" onchange="historySelectChange()">
                        <option selected value="PAST"><fmt:message key="filterForm.past"/> <fmt:message key="filterForm.experiments"/></option>
                        <option value="FUTURE"><fmt:message key="filterForm.future"/> <fmt:message key="filterForm.experiments"/></option>
                    </select>
                    <br>
                    <br>
                    <c:set var="availList" value="${typeLVList}" scope="request"/>
                    <fmt:message var="txt" key="eligText.typePickPast"/>
                    <c:import url="/WEB-INF/pages/tripick.jsp">
                        <c:param name="offset_class" value="offset_title"/>
                        <c:param name="num" value="1"/>
                        <c:param name="name" value="types"/>
                        <c:param name="eligText" value="${txt}"/>
                    </c:import>
                </div>
                <div id="SPC" style="display:none">
                    <c:set var="availList" value="${poolLVList}" scope="request"/>
                    <c:import url="/WEB-INF/pages/tripick.jsp">
                        <c:param name="offset_class" value="offset_title"/>
                        <c:param name="num" value="2"/>
                        <c:param name="name" value="pools"/>
                        <c:param name="eligText" value="Eligible subjects must be in..."/>
                    </c:import>
                </div>
                <div id="BC" style="display:none">
                    <fmt:message key="filterForm.bcIntro"/>
                    <br>
                    <br>
                    <select id="firstConditionSelect" onchange="bcSelectChange();">
                        <option class="center" value="NEW_FILTER" selected><fmt:message key="filterForm.selectFilter"/></option>
                        <c:forEach items="${condList}" var="cond">
                            <option class="center" value="${cond.id}"><c:out value="${cond.name}"/></option>
                        </c:forEach>
                    </select>
                    <br>
                    <br>
                    <select id="bcOpSelect">
                        <option class="center" value="AND"><fmt:message key="filterForm.AND"/></option>
                        <option class="center" value="OR"><fmt:message key="filterForm.OR"/></option>
                    </select>
                    <br>
                    <br>
                    <select id="secondConditionSelect" onchange="bcSelectChange();">
                        <option class="center" value="NEW_FILTER" selected><fmt:message key="filterForm.selectFilter"/></option>
                        <c:forEach items="${condList}" var="cond">
                            <option class="center" value="${cond.id}"><c:out value="${cond.name}"/></option>
                        </c:forEach>
                    </select>
                </div>

                <%-- BRS --%>
                <div id="NSC" style="display:none">
                    Maximum no-shows: <input id="maxNoShows"/>
                </div>


                <div id="newCondButtons" class="buttonBar right">
                    <br>
                    <input type="button" disabled="true" id="addExpButton" value="<fmt:message key='filterForm.addExp'/>"/> 
                    <input type="button" disabled="true" id="saveLaterButton" value="<fmt:message key='filterForm.saveForLater'/>"/> 
                </div>
            </fieldset>
        </div>
    </p>
    <div id="messageDiv"></div>
    <br>
    
    <appfuse:label key="filterForm.name" styleClass="desc" colon="false"/>
    <form:input path="name"/>
</div>
<div id="viewEligDiv">
	<fieldset>
		<legend>Eligible subjects</legend>
		<ul>
			<li>
				<b><fmt:message key="filterForm.numElig"/>: </b><span id="eligCntSpan"></span>
			</li>
		</ul>
	</fieldset>
</div>
<div>
    <input type="checkbox" name="checkedIRB_bottom" id="checkedIRB_bottom" onclick="markIRB('bottom')"/>&nbsp;<appfuse:label key="filter.checkedIRB"/>
</div>
<div id="buttons" class="buttonBar bottom">
    <input type="submit" class="button" name="back" value="<fmt:message key='button.back'/>" onclick="bCancel=true"/>
    <input type="submit" class="button" name="next" value="<fmt:message key='button.next'/>" onclick="bCancel=false"/>
    <input type="submit" class="button" name="cancel" value="<fmt:message key='button.cancel'/>" onclick="bCancel=true"/> 
    &nbsp;<i>or</i>&nbsp;<input type="submit" class="button" name="savenow" value="<fmt:message key='button.saveNow'/>" onclick="bCancel=false"/> 
</div>
</form:form>

<v:javascript formName="filterForm" cdata="false" dynamicJavascript="true" staticJavascript="false"/>
<script type="text/javascript" src="<c:url value="/scripts/validator.jsp"/>"></script>

<script type="text/javascript">
    Form.focusFirstElement($('filterForm'));
    /*document.body.onload=formLoad;
    onload = formLoad;*/
    // formLoad();
    updateFilter();
</script>
