<%@ include file="/common/taglibs.jsp"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="brs" %>

<head>
    <title><fmt:message key="prefs.title"/></title>
    <meta name="heading" content="<fmt:message key='prefs.heading'/>"/>
    <%--
    Later we will need to set the menu dynamically, since it depends on what kind of user we have
    <meta name="menu" content="UserMenu"/>
    --%>
    <link rel="stylesheet" type="text/css" href="<c:url value='/styles/user.css'/>"/>
    <script type="text/javascript" src="<c:url value='/scripts/selectbox.js'/>"></script>
    <script type="text/javascript" src="<c:url value='/scripts/common.js'/>"></script>
    <script type="text/javascript" src="<c:url value='/scripts/jquery.js'/>"></script>
    <script type="text/javascript">

        var $J = jQuery.noConflict();

        $J(document).ready(function() {

            // When a provider is selected, display the SMS gateway in the text field
            $J("#smsGateway").change(function() {
                $J("#otherSMSGateway").val($J(this).val());
            });

            // When the SMS gateway is edited manually, reset the provider select
            $J("#otherSMSGateway").keydown(function() {
                 $J("#smsGateway").val("");
            });
            $J("#otherSMSGateway").change(function() {
                 $J("#smsGateway").val("");
            });
        });
    
    </script>
</head>

<spring:bind path="subject.*">
    <c:if test="${not empty status.errorMessages}">
        <div class="error">
            <c:forEach var="error" items="${status.errorMessages}">
                <img src="<c:url value="/images/iconWarning.gif"/>"
                     alt="<fmt:message key="icon.warning"/>" class="icon"/>
                <c:out value="${error}" escapeXml="false"/><br />
            </c:forEach>
        </div>
    </c:if>
</spring:bind>

<form:form commandName="subject" method="post" action="subjectpreferencesform.html">
    <form:hidden path="id"/>
    <input type="hidden" name="from" value="<c:out value='${param.from}'/>"/>
    
    <c:if test="${userType == 'Subject'}">
        <c:set var="isSubj" value="true"/>
    </c:if>
    
    <c:if test="${userType == 'Experimenter'}">
        <c:set var="isExptr" value="true"/>
    </c:if>
    
    <%-- So the buttons can be used at the bottom of the form --%>
    <c:set var="buttons">
        <input type="submit" class="button" name="save" onclick="bCancel=false" value="<fmt:message key="button.save"/>"/>
        <input type="submit" class="button" name="cancel" onclick="bCancel=true" value="<fmt:message key="button.cancel"/>"/>
    </c:set>
    
    <ul>
        <li class="buttonBar">
            <c:out value="${buttons}" escapeXml="false"/>
        </li>
        <li class="info">
            <c:choose>
                <c:when test="${param.from == 'list'}">
                    <p><fmt:message key="userProfile.admin.message"/></p>
                </c:when>
                <c:otherwise>
                    <p><fmt:message key="userProfile.message"/></p>
                </c:otherwise>
            </c:choose>
        </li>

        <li>
            <div>
                <label class="desc"><fmt:message key="user.subscribed"/></label>
                <form:radiobutton path="subscribed" value="true"/> Yes <br/>
                <form:radiobutton path="subscribed" value="false"/> No
            </div>
        </li>
        <li>
            <div>
                <label class="desc"><fmt:message key="prefs.receiveSMSReminders"/></label>
                <form:radiobutton path="receiveSMSReminders" value="true"/> Yes (please provide your number and provider below)<br/>
                <form:radiobutton path="receiveSMSReminders" value="false"/> No
            </div>
            <div>
                <label class="desc"><fmt:message key="prefs.mobileNumber"/></label>
                <form:input path="mobileNumber" cssClass="text medium"/>
            </div>
            <div>
                <label class="desc"><fmt:message key="prefs.smsGateway"/></label>
                <form:select path="smsGateway" id="smsGateway" items="${smsGatewayList}" itemLabel="label" itemValue="value" />
            </div>
            <div>
                <label class="desc"><fmt:message key="prefs.otherSmsGateway"/></label>
                <form:input path="smsGateway" id="otherSMSGateway" />
            </div>
            <div>
                <label class="desc"><fmt:message key="prefs.sendTestSMS"/></label>
                <input type="checkbox" name="sendTestSMS"/>
            </div>
        </li>

        <li class="buttonBar bottom">
            <br>
            <c:out value="${buttons}" escapeXml="false"/>
        </li>
    </ul>
</form:form>
