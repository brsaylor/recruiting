<%@ include file="/common/taglibs.jsp"%> 

<head>
    <title><fmt:message key="experimentTypeDetail.title"/></title>
    <meta name="heading" content="<fmt:message key='experimentTypeDetail.heading'/>"/>
    <meta name="menu" content="ExperimenterMenu"/>
    
    <script type='text/javascript'>
function onExpTypeChange() {
    var expTypeSel = $("expTypeSelect");
    var url = '<c:url value="/experimenttypeform.html"/>';
    
    if(expTypeSel.selectedIndex != 0) {
        url += "?id=" + expTypeSel.value;
    }
    
    document.location.href = url;
}
    </script>


    <script type="text/javascript" src="<c:url value='/scripts/jquery.js'/>"></script>
    <script type='text/javascript'>
    // BRS 2011-02-11

    var J = jQuery.noConflict();
    var experimenters = [];
    <c:forEach items="${experimenters}" var="experimenter">
        experimenters[<c:out value="${experimenter.id}"/>] = "<c:out value="${experimenter.fullName}"/>";
    </c:forEach>

    // To be populated within the form, since the command object is not
    // available until then
    var principalInvestigatorIDs = [];
    
    J(document).ready(function() {

        // Add all current PIs on this experiment type to the PI table.
        for (var i = 0; i < principalInvestigatorIDs.length; i++) {
            addPI(principalInvestigatorIDs[i]);
        }

        // Add a PI row when a PI is selected from the dropdown.
        J("#addPISelect").change(function() {
            addPI(J(this).val());
            J(this).val(0);
        });
    });

    // Add a PI row to the PITable.
    function addPI(id) {
        var tr = J("<tr></tr>");
        var td = J("<td></td>");
        var button = J("<button type='button'>Remove</button>");
        button.click(function() {
            // Remove the PI row when the remove button is clicked.
            J(this).closest("tr").remove();
        });
        td.append(button);
        tr.append(td);
        var sel = J("#addPISelect").clone();
        sel.removeAttr("id");
        sel.attr("name", "principalInvestigatorIDs");
        sel.val(id);
        td = J("<td></td>");
        td.append(sel);
        tr.append(td);
        J("#PITable").append(tr);
    }

    </script>
</head>


<form:form commandName="experimentType" method="post" action="experimenttypeform.html" id="experimentTypeForm" onsubmit="return validateExperimentType(this)"> 
    <form:hidden path="id"/> 

    <script type="text/javascript">
        // BRS 2011-02-11
        <c:forEach items="${experimentType.principalInvestigators}" var="pi">
            principalInvestigatorIDs.push(<c:out value="${pi.id}"/>);
        </c:forEach>
    </script>
    
    <select id="expTypeSelect" onchange="onExpTypeChange()">
        <option name="new_type" value="new_type">--New exp. type--</option>
        <c:forEach items="${expTypeList}" var="type">
            <option <c:if test="${type.id == experimentType.id}">selected</c:if> value="${type.id}"><c:out value="${type.name}"/></option>
        </c:forEach>
    </select>
    
    <ul id="ulst">
        <li> 
            <appfuse:label styleClass="desc" key="experimentType.name"/> 
            <form:errors path="name" cssClass="fieldError"/> 
            <form:input path="name" id="name"  cssClass="text medium"/>
        </li> 
        <li>
            <appfuse:label styleClass="desc" key="experimentType.pi"/>
            <table id="PITable">
                <tr>
                    <td></td>
                    <td>
                        <select id="addPISelect">
                            <option value="0">--Add Principal Investigator--</option>
                            <c:forEach items="${experimenters}" var="experimenter">
                                <option value="${experimenter.id}"><c:out value="${experimenter.fullName}"/></option>
                            </c:forEach>
                        </select>
                    </td>
                <tr>
            </table>
        </li>
        <li>
            <%-- BRS 2011-06-13 --%>
            <div class="left">
                <appfuse:label key="experimentType.contactPerson" styleClass="desc" colon="false"/>
                <select id="myContactPerson" name="myContactPerson">
                    <c:forEach items="${experimenters}" var="expr">
                        <c:choose>
                            <c:when test="${expr.id == experimentType.contactPerson.id}">
                                <option value="${expr.id}" selected><c:out value="${expr.fullName}"/></option>
                            </c:when>
                            <c:otherwise>
                                <option value="${expr.id}"><c:out value="${expr.fullName}"/></option>
                            </c:otherwise>
                        </c:choose>
                    </c:forEach>
                </select>
            </div>
            <div> 
                <appfuse:label styleClass="desc" key="experimentType.accountNumber"/> 
                <form:errors path="accountNumber" cssClass="fieldError"/> 
                <form:input path="accountNumber" id="accountNumber"  cssClass="text medium"/>
            </div> 
        </li>
        <li> 
            <appfuse:label styleClass="desc" key="experimentType.descr"/> 
            <form:errors path="descr" cssClass="fieldError"/> 
            <form:textarea rows="12" cols="80" path="descr" id="descr" />
        </li> 
        <li> 
            <appfuse:label styleClass="desc" key="experimentType.instruction"/> 
            <form:errors path="instruction" cssClass="fieldError"/> 
            <form:textarea rows="12" cols="80" path="instruction" id="instruction" />
        </li> 
        <li> 
            <appfuse:label styleClass="desc" key="experimentType.consentText"/> 
            <form:errors path="consentText" cssClass="fieldError"/> 
            <form:textarea rows="12" cols="80" path="consentText" id="consentText" />
        </li> 
        <c:if test="${not empty experimentType.id}"> 
            <li id="validLI">
                <form:errors path="valid" cssClass="fieldError"/>
                <form:checkbox path="valid"/>Experiment Type Is Valid
            </li>
        </c:if>
        <li class="buttonBar bottom" id="buttonbar"> 
            <input type="submit" class="button" name="save" value="<fmt:message key='button.save'/>"/> 
            <c:if test="${not empty experimentType.id}">
                <input type="submit" name="delete" id="delete" onclick="bCancel=true; return confirmDelete('experimentType')"  
                       value="<fmt:message key='button.delete'/>" class="button"/> 
            </c:if>
            <input type="submit" class="button" name="cancel" value="<fmt:message key='button.cancel'/>" onclick="bCancel=true"/> 
        </li>
    </ul>
</form:form>    

<v:javascript formName="experimentType" cdata="false" dynamicJavascript="true" staticJavascript="false"/>
<script type="text/javascript" src="<c:url value="/scripts/validator.jsp"/>"></script>

<script type="text/javascript">
    Form.focusFirstElement($('experimentTypeForm'));
</script>
