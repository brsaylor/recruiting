

<%-- 
    Document   : question
    Created on : Apr 14, 2009, 3:43:34 PM
    Author     : Richard Ho
--%>

<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="mylib" uri="/WEB-INF/mylib.tld" %>

<form:form>
    <head>
        <title><fmt:message key="question.title"/></title>
        <meta name="heading" content="<fmt:message key='question.heading'/>"/>
    </head>
    <body id="question">
        <c:if test="${empty questions}">
            <fmt:message key="question.noQuestion"/>
        </c:if>
        <c:if test="${not empty questions}">
            <c:forEach items="${questions}" var="questionSelected" varStatus="status">
                <li>
                    <c:set var="myq" value="${questionSelected}" scope="request"/>
                    <c:import url="/WEB-INF/pages/displayquestion.jsp">
                        <c:param name="qnum" value="null"/>
                        <c:param name="questionClass" value="desc"/>
                        <c:param name="index" value="${status.index}"/>
                        <c:param name="answer" value="0"/>
                        <c:param name="required" value="true"/>
                    </c:import>
                    <input type="button" class="button" onclick="location.href='<c:url value="/questionresult.html?eid=${id}&qid=${questionSelected.id}"/>'" 
                       value="<fmt:message key="question.viewResult"/>"/>
                </li>
                <li><br/><br/></li>
            </c:forEach>
        </c:if>
    </body>
</form:form>
