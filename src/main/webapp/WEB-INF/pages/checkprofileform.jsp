<%@ include file="/common/taglibs.jsp"%> 
<%@ taglib prefix="mylib" uri="/WEB-INF/mylib.tld" %>

<head>
    <title><fmt:message key="checkProfileForm.title"/></title>
    <meta name="heading" content="<fmt:message key='checkProfileForm.heading'/>"/>
</head>

<c:set var="qstring" value="question"/>
<form:form commandName="questionAndAnswerList" method="post" action="checkprofileform.html" id="qal"> 
    <form:hidden path="expId"/>
    <c:set var="j" value="0"/>
    <c:forEach items="${questionAndAnswerList.questions}" var="question">
        <c:set var="myq" value="${question}" scope="request"/>
        <c:import url="/WEB-INF/pages/displayquestion.jsp">
            <c:param name="qnum" value="null"/>
            <c:param name="index" value="${j}"/>
            <c:param name="answer" value="${questionAndAnswerList.answers[j]}"/>
            <c:param name="questionClass" value="desc"/>
        </c:import>
        <%--<label><c:out value="${j+1}.  ${question.question}"/></label>
            <c:set var="k" value="0"/>
            <c:if test="${question.style == 'DROP_LIST'}">
                <ul>
                    <li>
                        <select id="${qstring}${j}" name="${qstring}${j}">
                            <c:forEach items="${question.optionList}" var="option">
                                <c:choose>
                                    <c:when test="${questionAndAnswerList.answers[j] == k}">
                                        <option selected id="${qstring}${j},${k}" value="${k}"><c:out value="${option}"/></option>
                                    </c:when>
                                    <c:otherwise>
                                        <option id="${qstring}${j},${k}" value="${k}"><c:out value="${option}"/></option>
                                    </c:otherwise>
                                </c:choose>
                                <c:set var="k" value="${k + 1}"/>
                            </c:forEach>
                        </select>
                    </li>
                </ul>
            </c:if>
            <c:if test="${question.style == 'RADIO'}">
                <ul>
                    <c:forEach items="${question.optionList}" var="option">
                        <li>
                            <c:choose>
                                <c:when test="${questionAndAnswerList.answers[j] == k}">
                                    <input checked type="radio" name="${qstring}${j}" value="${k}" id="${qstring}${j},${k}"/>
                                </c:when>
                                <c:otherwise>
                                    <input type="radio" name="${qstring}${j}" value="${k}" id="${qstring}${j},${k}"/>
                                </c:otherwise>
                            </c:choose>
                            <label for="${qstring}${j},${k}"><c:out value="${option}"/></label> 
                            <c:set var="k" value="${k + 1}"/>
                        </li>
                    </c:forEach>
                </ul>
            </c:if>
            <c:if test="${question.style == 'CHECK_BOX'}">
                <ul>
                    <c:forEach items="${question.optionList}" var="option">
                        <li>
                            <c:choose>
                                <c:when test="${mylib:hasDigit(questionAndAnswerList.answers[j], k)}"> 
                                    <input checked type="checkbox" name="${qstring}${j},${k}" id="${qstring}${j},${k}"/>
                                </c:when>
                                <c:otherwise>
                                    <input type="checkbox" name="${qstring}${j},${k}" id="${qstring}${j},${k}"/>
                                </c:otherwise>
                            </c:choose>
                            <label for="${qstring}${j},${k}"><c:out value="${option}"/></label>
                            <c:set var="k" value="${k + 1}"/>
                        </li>
                    </c:forEach>
                </ul>
            </c:if>--%>
        <c:set var="j" value="${j+1}"/>
    </c:forEach> 
    
    <p>
    
    <ul>
        <li class="buttonBar bottom"> 
            <input type="submit" class="button" name="continue" value="<fmt:message key='button.continue'/>"/> 
        </li>
    </ul>
</form:form>
