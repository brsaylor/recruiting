<%@ include file="/common/taglibs.jsp"%>

<head> 
    <title><fmt:message key="experimentTypeList.title"/></title> 
    <meta name="heading" content="<fmt:message key='experimentTypeList.heading'/>"/> 
    <meta name="menu" content="ExperimenterMenu"/>
</head> 

<c:set var="buttons"> 
    <input type="button" style="margin-right: 5px" 
           onclick="location.href='<c:url value="/experimenttypeform.html"/>'" 
           value="<fmt:message key="button.add"/>"/> 
    
    <input type="button" onclick="location.href='<c:url value="/experimenter.html"/>'" 
           value="<fmt:message key="button.done"/>"/> 
</c:set> 

<c:out value="${buttons}" escapeXml="false"/> 

<%--Functionality comes from the "name" attribute of the table--%>

<display:table name="experimentTypeList" cellspacing="0" cellpadding="0" requestURI=""
               id="experimentType" pagesize="25" class="table experimentTypeList" export="true"> 
    
    <display:column url="/experimenttypeform.html" property="name" paramId="id" paramProperty="id" escapeXml="true" sortable="true" titleKey="experimentType.name"/> 
    <display:column property="principalInvestigatorsStr" escapeXml="true" sortable="true" titleKey="experimentType.pi"/>
    <display:column property="descr" escapeXml="true" sortable="true" titleKey="experimentType.descr"/>
    <%--<display:column property="instruction" escapeXml="true" sortable="true" titleKey="experimentType.instruction"/>--%>
    <display:column escapeXml="true" sortable="true" titleKey="experimentType.valid"><c:out value="${experimentType.valid ? 'Yes' : 'No'}"/></display:column>
    <display:column sortProperty="regDate" escapeXml="true" sortable="true" titleKey="experimentType.regDate"><fmt:formatDate value="${experimentType.regDate}" pattern="MM/dd/yyyy"/></display:column>
    <display:column property="creator.fullName" sortProperty="creator.lastName" escapeXml="true" sortable="true" titleKey="experimentType.creator"/>
    <display:setProperty name="paging.banner.item_name" value="experimentType"/> 
    <display:setProperty name="paging.banner.items_name" value="experimentTypes"/> 
    
    <display:setProperty name="export.excel.filename" value="Subject Pool List.xls"/> 
    <display:setProperty name="export.csv.filename" value="Subject Pool List.csv"/> 
    <display:setProperty name="export.pdf.filename" value="Subject Pool List.pdf"/> 
</display:table> 

<c:out value="${buttons}" escapeXml="false"/> 

<script type="text/javascript"> 
    highlightTableRows("experimentType"); 
</script> 
