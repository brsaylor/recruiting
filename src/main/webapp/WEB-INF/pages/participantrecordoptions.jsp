<%@ include file="/common/taglibs.jsp"%>

<head> 
    <title><fmt:message key="participantRecordOptions.title"/></title> 
    <meta name="heading" content="<fmt:message key='participantRecordOptions.title'/>
        <c:if test='${isAdmin}'> &mdash; <c:out value='${subject.username} (${subject.firstName} ${subject.lastName})'/></c:if>"/> 
    <meta name="menu" content="SubjectMenu"/>
</head> 

<display:table name="participantRecordList" cellspacing="0" cellpadding="0" requestURI="" 
	       id="participantRecord" pagesize="25" class="table participantRecordList" export="true" style="text-decoration:none;">
    
    <display:column url="/participantrecordoptionsform.html" paramId="id" paramProperty="id" property="exp.id" sortProperty="exp.expDate" escapeXml="true" sortable="true" titleKey="participantRecordOptions.experimentId"/>
    <c:if test="${isAdmin}">
        <display:column property="exp.type.name" escapeXml="true" sortable="true" titleKey="participantRecordOptions.experimentType"/>
    </c:if>
    <display:column property="exp.expDate" escapeXml="true" sortable="true" titleKey="participantRecordOptions.experimentDate"/>
    <display:column property="exp.startTime" escapeXml="true" sortable="true" titleKey="participantRecordOptions.experimentStartTime"/>
    <display:column property="exp.endTime" escapeXml="true" sortable="true" titleKey="participantRecordOptions.experimentEndTime"/>
    <display:column property="exp.runby.fullName" escapeXml="true" sortable="true" titleKey="participantRecordOptions.experimentRunby"/>
    <c:if test="${isAdmin}">
        <display:column property="status" escapeXml="true" sortable="true" titleKey="participantRecordOptions.status"/>
    </c:if>
    <display:column  escapeXml="true" sortable="true" titleKey="participantRecordOptions.payoff">
        <fmt:formatNumber type="currency" value="${participantRecord.payoff}" currencySymbol="$"/>
    </display:column>

    <%-- BRS removed 2012-03-20
    <display:column property="paymentMethod.name" escapeXml="true" sortable="true" titleKey="participantRecordOptions.paymentMethod"/>
    --%>

    <display:setProperty name="paging.banner.item_name" value="experiment"/> 
    <display:setProperty name="paging.banner.items_name" value="experiments"/> 
    
    <display:setProperty name="export.excel.filename" value="ParticipationList.xls"/> 
    <display:setProperty name="export.csv.filename" value="ParticipationList.csv"/> 
    <display:setProperty name="export.pdf.filename" value="ParticipationList.pdf"/>
</display:table> 

<script type="text/javascript"> 
    highlightTableRows("participantRecord"); 
</script>
