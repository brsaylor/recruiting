<%@ include file="/common/taglibs.jsp"%> 

<head>
    <title><fmt:message key="subject.title"/></title>
    <meta name="heading" content="<fmt:message key='subject.heading'/>"/>
    <meta name="menu" content="SubjectMenu"/>
</head>

<h2>Settings</h2>
<ul class="glassList">
    <li><a href="userform.html"><fmt:message key='menu.my.profile'/></a></li>
    <li><a href="subjectpreferencesform.html"><fmt:message key='menu.my.preferences'/></a></li>
</ul>

<h2>Experiments</h2>
<ul class="glassList">
    <li><a href="participantrecordoptions.html"><fmt:message key='menu.my.experiments'/></a></li>
</ul>
