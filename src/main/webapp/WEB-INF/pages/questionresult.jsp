<%-- 
    Document   : questionresult
    Created on : Apr 23, 2009, 12:39:54 PM
    Author     : Richard Ho
--%>

<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="mylib" uri="/WEB-INF/mylib.tld" %>


<head>
    <title><fmt:message key="questionResult.title"/></title> 
</head>
<body>
    <display:table name="questionResultList" cellspacing="0" cellpadding="0" requestURI="" 
           id="questionResult" pagesize="25" class="table questionResultList" export="true"> 
        <display:column paramId="id" paramProperty="id" property="surveyTaker.fullName" sortProperty="surveyTaker.lastName" escapeXml="true" sortable="true" titleKey="questionResult.name"/>
        <display:column sortName="answer" escapeXml="true" sortable="true" titleKey="questionResult.answer">
            <%-- BRS 2011-06-08  fix output for checkboxes --%>
            <c:choose>
                <c:when test="${question.style == 'CHECK_BOX'}">
                    <c:choose>
                        <c:when test="${questionResult.answerIdx == 1}">
                            1
                        </c:when>
                        <c:otherwise>
                            0
                        </c:otherwise>
                    </c:choose>
                </c:when>
                <c:otherwise>
                    <c:set var="k" value="0"/>
                    <c:forEach items="${question.optionList}" var="option">
                        <c:if test="${questionResult.answerIdx == k}">
                            <c:out value="${option}"/>
                        </c:if>
                        <c:set var="k" value="${k + 1}"/>
                    </c:forEach>
                </c:otherwise>
            </c:choose>
        </display:column>
        <display:column paramId="date" paramProperty="date" property="takenDate" sortProperty="takenDate" escapeXml="true" sortable="true" titleKey="questionResult.date"/>
    </display:table>
</body>
