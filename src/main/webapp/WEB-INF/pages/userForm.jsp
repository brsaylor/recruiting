<%@ include file="/common/taglibs.jsp"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="brs" %>

<head>
    <title><fmt:message key="userProfile.title"/></title>
    <meta name="heading" content="<fmt:message key='userProfile.heading'/>"/>
    <%--
    Later we will need to set the menu dynamically, since it depends on what kind of user we have
    <meta name="menu" content="UserMenu"/>
    --%>
    <link rel="stylesheet" type="text/css" href="<c:url value='/styles/${appConfig["csstheme"]}/user.css'/>"/>
    <script type="text/javascript" src="<c:url value='/scripts/selectbox.js'/>"></script>
    <script type="text/javascript" src="<c:url value='/scripts/common.js'/>"></script>
    <script type="text/javascript" src="<c:url value='/scripts/jquery.js'/>"></script>
    <script type="text/javascript" src="<c:url value='/scripts/user.js'/>"></script>
    <script type="text/javascript">
var dayCntList = new Array(12);
        
function setChangePassword() {
    var cb = $("changePassword");
    var div = $("changePasswordDiv");
    if(cb.checked) {
        div.style.display="block";
    } else {
        div.style.display="none";
        $("mypassword").value="";
        $("myconfirmpassword").value="";
    }
}

function passwordChanged(passwordField) {
    if (passwordField.id == "password") {
        var origPassword = "${user.password}";
    } else if (passwordField.id == "confirmPassword") {
        var origPassword = "${user.confirmPassword}";
    }

    if (passwordField.value != origPassword) {
        createFormElement("input", "hidden",  "encryptPass", "encryptPass",
                          "true", passwordField.form);
    }
}

<!-- This is here so we can exclude the selectAll call when roles is hidden -->
function formSubmit(theForm) {
    var valid = true;
    var value;
    
<c:if test="${(param.from == 'list') || (param.method == 'Add')}">
    selectAll('userRoles');
    
    //Check that there are at least one roles selected
    if(!bCancel && $("userRoles").length == 0) {
        alert("User must be assigned at least one role");
        valid = false;
    }
</c:if>

    var fromAdd = false;
<c:if test="${param.method == 'Add'}">
    fromAdd = true;
</c:if>

    //If we're adding a user, or if password is being changed, password must not be null
    if(!bCancel && (fromAdd || $("changePassword").checked)) {
        value = $("mypassword").value;
        if(trimLT(value).length == 0) {
            alert("Password is a required field");
            valid = false;
        }
    }
    
    //If user is in subject role
    if($("subjectSpan").style.display != "none") {
        selectAll("userPools");
        value = $("schoolId").value;
        if(!bCancel && trimLT(value).length == 0) {
            alert("School ID is a required field");
            valid = false;
        }

        value = $("birthday_year").value;
        if(!bCancel && trimLT(value) == 0 || value.length < 4) {
            alert("Invalid birthday - year should be at least 4 digits");
            valid = false;
        } else if(!bCancel && !isAllDigits(value)) {
            alert("Invalid birthday - year must be a number");
            valid = false;
        }
    }
        
    //If user is in experimenter role
    if($("experimenterSpan").style.display != "none") {
        value = $("affiliation").value;
        if(!bCancel && trimLT(value).length == 0) {
            alert("Affiliation is a required field");
            valid=false;
        }
    }
    
    return (valid && validateUser(theForm));
}

function roleChange() {
    var isExpr = false;
    var isSubj = false;
    var oList = $("userRoles").options;
    var saveButtons = document.getElementsByName("save");
    for(j = 0; j < oList.length; j++) {
        if(oList[j].value == "ROLE_SUBJECT") {
            isSubj = true;
        }
        if(oList[j].value == "ROLE_EXPERIMENTER") {
            isExpr = true;
        }
    }
    
    if(isSubj && isExpr) {
        $("roleMessageSpan").style.display="inline";
        $("subjectSpan").style.display="none";
        $("experimenterSpan").style.display="none";
        for(j = 0; j < saveButtons.length; j++) {
            saveButtons[j].disabled = true;
        }
    } else {
        for(j = 0; j < saveButtons.length; j++) {
            saveButtons[j].disabled = false;
        }
        $("roleMessageSpan").style.display="none";
        if(isSubj) {
            $("subjectSpan").style.display="inline";
        } else {
            $("subjectSpan").style.display="none";
        }
        if(isExpr) {
            $("experimenterSpan").style.display="inline";
        } else {
            $("experimenterSpan").style.display="none";
        }
    }
}

function monthChange() {
    var monthSel = $("birthday_month");
    var daySel = $("birthday_day");
    var num = dayCntList[1*monthSel.value];
    
    //Remove extra days at the end
    while(daySel.length > num) {
        daySel.remove(daySel.length-1);
    }
    
    //Add to the end
    while(daySel.length < num) {
        var opt = appendOption(daySel, daySel.length + 1);
        opt.value=opt.text;
    }
}
    </script>
</head>

<spring:bind path="user.*">
    <c:if test="${not empty status.errorMessages}">
        <div class="error">
            <c:forEach var="error" items="${status.errorMessages}">
                <img src="<c:url value="/images/iconWarning.gif"/>"
                     alt="<fmt:message key="icon.warning"/>" class="icon"/>
                <c:out value="${error}" escapeXml="false"/><br />
            </c:forEach>
        </div>
    </c:if>
</spring:bind>

<form:form commandName="user" method="post" action="userform.html" onsubmit="return formSubmit(this);" id="userForm">
    <form:hidden path="id"/>
    <form:hidden path="version"/>
    <input type="hidden" name="from" value="<c:out value='${param.from}'/>"/>
    
    <c:set var="myCookie" value="${cookieLogin == 'true' && editingMe == 'true'}"/>
    
    <c:if test="${myCookie}">
        <form:hidden path="password"/>
        <form:hidden path="confirmPassword"/>
    </c:if>
    
    <c:if test="${empty user.version}">
        <input type="hidden" name="encryptPass" value="true"/>
    </c:if>
    
    <c:if test="${userType == 'Subject'}">
        <c:set var="isSubj" value="true"/>
    </c:if>
    
    <c:if test="${userType == 'Experimenter'}">
        <c:set var="isExptr" value="true"/>
    </c:if>
    
    <c:set var="isNew" value="${param.method == 'Add'}"/>
    
    <!--
    cookieLogin = ${cookieLogin}
    editingMe = ${editingMe}
    myCookie = ${myCookie}
    isNew = ${isNew}
    -->
    <%-- So the buttons can be used at the bottom of the form --%>
    <c:set var="buttons">
        <input type="submit" class="button" name="save" onclick="bCancel=false" value="<fmt:message key="button.save"/>"/>

               <c:if test="${param.from == 'list' and param.method != 'Add'}">
            <input type="submit" class="button" name="delete" onclick="bCancel=true;return confirmDelete('user')"
                   value="<fmt:message key="button.delete"/>"/>
                   <c:if test="${userType == 'Subject'}">
                        <input type="button" class="button" onclick="location.href='<c:url value="/participantrecordoptions.html?id=${param.id}"/>'"
                                value="<fmt:message key="button.viewRecord"/>"/>
                   </c:if>
               </c:if>

        <input type="submit" class="button" name="cancel" onclick="bCancel=true" value="<fmt:message key="button.cancel"/>"/>
    </c:set>
    
    <ul>
        <li class="buttonBar">
            <c:out value="${buttons}" escapeXml="false"/>

            <%-- BRS --%>
            <c:if test="${isAdmin && userType == 'Subject'}">
                <a href='<c:url value="/subjectpreferencesform.html?id=${param.id}&from=list"/>' >Subject Preferences</a>
            </c:if>

        </li>
        <li class="info">
            <c:choose>
                <c:when test="${param.from == 'list'}">
                    <p><fmt:message key="userProfile.admin.message"/></p>
                </c:when>
                <c:otherwise>
                    <p><fmt:message key="userProfile.message"/></p>
                </c:otherwise>
            </c:choose>
        </li>

 <%-- BRS March 2010
        Remove username field; have the controller automatically set it to email address,
        effectively using email addresses as usernames. --%>

<%--
        <li>
            <appfuse:label styleClass="desc" key="user.username"/>
            <form:errors path="username" cssClass="fieldError"/>
            <c:choose>
                <c:when test="${isAdmin != 'true'}">
                    <form:input path="username" id="username" cssClass="text large" readonly="true" />
                </c:when>
                <c:otherwise>
                    <form:input path="username" id="username" cssClass="text large" />
                </c:otherwise>
            </c:choose>
        </li>
--%>

<%-- BRS --%>
<c:if test="${isExptr}">
<li>
<fieldset><legend>Principal Investigator on</legend>
<ul>
    <c:forEach items="${myExperimentTypes}" var="t">
    <li>
    <a href="<c:url value='/experimenttypeform.html?id=${t.id}'/>"><c:out value="${t.name}"/></a>
    </li>
    </c:forEach>
</ul>
</fieldset>
</li>
</c:if>
        
        <c:if test="${isSubj}">
            <li>
                <div>
                    <label class="desc"><fmt:message key="user.subscribed"/></label>
                    <form:radiobutton path="subscribed" value="true"/> Yes <br/>
                    <form:radiobutton path="subscribed" value="false"/> No
                        
                </div>
            </li>
        </c:if>

         <%-- BRS Aug 2010 --%>
         <c:set var="pageName" value="userForm" scope="request"/>
        <%@ include file="/common/requiredSubjectProfileQuestions.jsp"%>

        <%--<li>
            <appfuse:label styleClass="desc" key="user.passwordHint"/>
            <form:errors path="passwordHint" cssClass="fieldError"/>
            <form:input path="passwordHint" id="passwordHint" cssClass="text large"/>
        </li>--%>

        <c:choose>
            <c:when test="${param.from == 'list' or param.method == 'Add'}">
                <li>
                    <fieldset>
                        <legend><fmt:message key="userProfile.accountSettings"/></legend>
                        <form:checkbox path="enabled" id="enabled"/>
                        <label for="enabled" class="choice"><fmt:message key="user.enabled"/></label>
                        
                        <form:checkbox path="accountExpired" id="accountExpired"/>
                        <label for="accountExpired" class="choice"><fmt:message key="user.accountExpired"/></label>
                        
                        <form:checkbox path="accountLocked" id="accountLocked"/>
                        <label for="accountLocked" class="choice"><fmt:message key="user.accountLocked"/></label>
                        
                        <form:checkbox path="credentialsExpired" id="credentialsExpired"/>
                        <label for="credentialsExpired" class="choice"><fmt:message key="user.credentialsExpired"/></label>
                    </fieldset>
                </li>
                <li>
                    <fieldset class="pickList">
                        <legend><fmt:message key="userProfile.assignRoles"/></legend>
                        <table class="pickList" cellpadding="5">
                            <tr>
                                <th class="pickLabel">
                                    <appfuse:label key="user.availableRoles" colon="false" styleClass="required"/>
                                </th>
                                <th class="pickLabel">
                                    <appfuse:label key="user.roles" colon="false" styleClass="required"/>
                                </th>
                            </tr>
                            <c:set var="leftList" value="${availRoleLVList}" scope="request"/>
                            <c:set var="rightList" value="${userRoleLVList}" scope="request"/>
                            <c:import url="/WEB-INF/pages/pickList.jsp">
                                <c:param name="listCount" value="1"/>
                                <c:param name="leftId" value="availableRoles"/>
                                <c:param name="rightId" value="userRoles"/>
                            </c:import>
                        </table>
                    </fieldset>
                    <div id="roleMessageSpan" class="message" style="display:none"><img src="/images/iconWarning.gif" alt="Error"/>
                        <fmt:message key="userProfile.subjectAndExperimenter"/><br>    
                    </div>
                </li>
            </c:when>
            <c:when test="${not empty user.username}">
                <li>
                    <%--<strong><appfuse:label key="user.roles"/>:</strong>--%>
                    <c:forEach var="role" items="${user.roleList}" varStatus="status">
                        <%--<c:out value="${role.label}"/><c:if test="${!status.last}">,</c:if>--%>
                        <input type="hidden" name="userRoles" value="<c:out value="${role.label}"/>"/>
                           </c:forEach>
                    <form:hidden path="enabled"/>
                    <form:hidden path="accountExpired"/>
                    <form:hidden path="accountLocked"/>
                    <form:hidden path="credentialsExpired"/>
                </li>
            </c:when>
        </c:choose>
            <li id="experimenterSpan" <c:if test="${empty isExptr}">style="display:none"</c:if>>
                <div>
                    <div>
                        <label class="desc" for="affiliation"><fmt:message key="user.affiliation"/> <span class="req">*</span></label>
                        <%-- This field will be automatically bound by Spring --%>
                        <input type="text" id="affiliation" name="affiliation" <c:if test="${isExptr}">value="${user.affiliation}"</c:if>/>
                    </div>
                    <div>
                        <appfuse:label styleClass="desc" key="user.title"/>
                        <select id="title" name="title">
                            <c:forEach items="${titleList}" var="title">
                                <option value="${title.name}" <c:if test="${isExptr && user.title==title}">selected</c:if>><c:out value="${title}"/></option>
                            </c:forEach>
                        </select>
                    </div>
                </div>
                <div> <%-- BRS 2011-06-13 --%>
                    <div>
                        <label class="desc" for="department"><fmt:message key="user.department"/> <span class="req">*</span></label>
                        <%-- This field will be automatically bound by Spring --%>
                        <input type="text" id="department" name="department" <c:if test="${isExptr}">value="${user.department}"</c:if>/>
                    </div>
                    <div>
                        <label class="desc" for="phoneNumber"><fmt:message key="user.phoneNumber"/> <span class="req">*</span></label>
                        <%-- This field will be automatically bound by Spring --%>
                        <input type="text" id="phoneNumber" name="phoneNumber" <c:if test="${isExptr}">value="${user.phoneNumber}"</c:if>/>
                    </div>
                </div>
            </li>
        <span id="subjectSpan" <c:if test="${empty isSubj}">style="display:none"</c:if>>
            <%-- BRS - replaced with radio buttons for selecting a single subject pool (in file included below)
            <li>
                <fieldset class="pickList">
                    <legend><fmt:message key="user.pools"/></legend>
                    <table class="pickList" cellpadding="5">
                        <tr>
                            <th class="pickLabel">
                                <appfuse:label key="user.availPools" colon="false" styleClass="required"/>
                            </th>
                            <th class="pickLabel">
                                <appfuse:label key="user.selPools" colon="false" styleClass="required"/>
                            </th>
                        </tr>
                        <c:set var="leftList" value="${availLVPools}" scope="request"/>
                        <c:set var="rightList" value="${userLVPools}" scope="request"/>
                        <c:import url="/WEB-INF/pages/pickList.jsp">
                            <c:param name="listCount" value="2"/>
                            <c:param name="leftId" value="availablePools"/>
                            <c:param name="rightId" value="userPools"/>
                        </c:import>
                    </table>
                </fieldset>
            </li>
            --%>


            <%-- BRS March 2010 --%>
            <c:if test="${isSubj}">
                <%@ include file="/common/additionalSubjectProfileQuestions.jsp"%>
            </c:if>


            <c:forEach items="${profile_qs}" var="question" varStatus="status">
                <li>
                    <c:set var="myq" value="${question}" scope="request"/>
                    <c:import url="/WEB-INF/pages/displayquestion.jsp">
                        <c:param name="qnum" value="null"/>
                        <c:param name="questionClass" value="desc"/>
                        <c:param name="index" value="${status.index}"/>
                        <c:param name="answer" value="${profile_ans[status.index]}"/>
                        <c:param name="required" value="true"/>
                    </c:import>
                </li>
            </c:forEach>
            
        </span>

        <%-- BRS 2012-04-30, fixed for non-subjects 2012-09-07 --%>
        <c:if test="${isSubj}">
            <li>
                <label class="desc">Registration date</label>
                <c:if test="${empty user.registrationDate}">Unknown</c:if>
                <fmt:formatDate value="${user.registrationDate}" dateStyle="LONG" />
            </li>
        </c:if>

        <li class="buttonBar bottom">
            <br>
            <c:out value="${buttons}" escapeXml="false"/>
        </li>
    </ul>
</form:form>

<script type="text/javascript">
    Form.focusFirstElement($('userForm'));
    highlightFormElements();
    
<%--This is the only condition under which these buttons exist--%>
<c:if test="${param.from == 'list' or param.method == 'Add'}">
    $("moveRight1").onclick=function(){moveSelectedOptions($('availableRoles'),$('userRoles'),true); roleChange();};
    $("moveAllRight1").onclick=function(){moveAllOptions($('availableRoles'),$('userRoles'),true); roleChange();};
    $("moveLeft1").onclick=function(){moveSelectedOptions($('userRoles'),$('availableRoles'),true); roleChange();};
    $("moveAllLeft1").onclick=function(){moveAllOptions($('userRoles'),$('availableRoles'),true); roleChange();};
</c:if>
    
    <c:forEach items="${monthList}" var="monthLV" varStatus="status">
        dayCntList[${status.index}] = ${monthLV.value};
    </c:forEach>
</script>

<v:javascript formName="user" cdata="false" dynamicJavascript="true" staticJavascript="false"/>
<script type="text/javascript" src="<c:url value="/scripts/validator.jsp"/>"></script>

<%--    
Items that we no longer are using from the user class
<li>
    <appfuse:label styleClass="desc" key="user.website"/>
    <form:errors path="website" cssClass="fieldError"/>
    <form:input path="website" id="website" cssClass="text large"/>
</li>
<li>
    <label class="desc"><fmt:message key="user.address.address"/></label>
    <div class="group">
        <div>
            <form:input path="address.address" id="address.address" cssClass="text large"/>
            <form:errors path="address.address" cssClass="fieldError"/>
            <p><appfuse:label key="user.address.address"/></p>
        </div>
        <div class="left">
            <form:input path="address.city" id="address.city" cssClass="text medium"/>
            <form:errors path="address.city" cssClass="fieldError"/>
            <p><appfuse:label key="user.address.city"/></p>
        </div>
        <div>
            <form:input path="address.province" id="address.province" cssClass="text state" size="2"/>
            <form:errors path="address.province" cssClass="fieldError"/>
            <p><appfuse:label key="user.address.province"/></p>
        </div>
        <div class="left">
            <form:input path="address.postalCode" id="address.postalCode" cssClass="text zip"/>
            <form:errors path="address.postalCode" cssClass="fieldError"/>
            <p><appfuse:label key="user.address.postalCode"/></p>
        </div>
        <div>
            <appfuse:country name="address.country" prompt="" default="${user.address.country}"/>
            <p><appfuse:label key="user.address.country"/></p>
        </div>
    </div>
</li>
--%>
