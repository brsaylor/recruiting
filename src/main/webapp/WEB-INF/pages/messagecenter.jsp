<%@ include file="/common/taglibs.jsp"%>

<head> 
    <title><fmt:message key="messagecenter.title"/></title> 
    <meta name="heading" content="<fmt:message key='messagecenter.heading'/>"/> 
    <meta name="menu" content="MessagesMenu"/>
    <script type="text/javascript" src="<c:url value='/scripts/common.js'/>"></script>
    <script type='text/javascript' src='<c:url value="/dwr/engine.js"/>'></script>
    <script type='text/javascript' src='<c:url value="/dwr/util.js"/>'></script>
    <script type='text/javascript' src='<c:url value="/dwr/interface/messageManagerDWR.js"/>'></script>
    <script type='text/javascript' src='<c:url value="/dwr/interface/userMessageManagerDWR.js"/>'></script>
    <style type="text/css">
        .dataTable td.hidden {
            display: none;
        }
        .dataTable th.hidden {
            display: none;
        }
    </style>
    <script type='text/javascript'>
        var CURR_ID;
        var OPTION = "<c:out value="${param.option}"/>";
        
        function showMessage(msg) {
        
            CURR_ID=msg.id;
            var msg_ul = $("msg_ul");
            clearNodeChildren(msg_ul);
            var li1 = addChild(msg_ul, "li");
            addTextChild(li1, "Date: "+msg.whenCreated);
            
            var li2 = addChild(msg_ul, "li");
            if(OPTION == "inbox" || OPTION == "deleted") {
                addTextChild(li2, "From: "+msg.creator.firstName+" "+msg.creator.lastName);
            } else {
                // TODO... get recipient list
                var totext = "To: ";
                var ums = msg.messageReferences;
                for(i=0; i<ums.length; i++) {
                    var rcp = ums[i].recipient;
                    if(i>0)
                        totext = totext+"; ";
                    totext = totext+rcp.firstName+" "+rcp.lastName;
                }
                addTextChild(li2, totext);
            }
            
            var li3 = addChild(msg_ul, "li");
            addTextChild(li3, "Subject: "+msg.subject);
            
            var msg_para = $("msg_para");
            clearNodeChildren(msg_para);
            msg_para.appendChild(document.createTextNode(msg.text));
            
            var msg_div = $("messageDiv");
            msg_div.style.display = "block";
        }
        
        function addRowHandlers(tableId, columnIndex) {
            var previousClass = null;
            var table = $(tableId);
            var rows = table.getElementsByTagName("tr");
            for (i = 1; i < rows.length; i++) {
                rows[i].onclick = function () {
                    var cell = this.getElementsByTagName("td")[columnIndex];
                    var paramValue = cell.innerHTML;
                    if(paramValue != "Nothing found to display.")
                        messageManagerDWR.findByIdString(paramValue, showMessage);
                };
            }
        }
        
        function setMsgId() {
            var id_field = $("id");
            id_field.value = CURR_ID;
        }
    </script>
</head> 

<%--Functionality comes from the "name" attribute of the table--%>

<display:table name="displayList" cellspacing="0" cellpadding="0"
	       id="message" pagesize="25" class="table dataTable" export="false"> 
            <display:caption>
                <c:choose>
                    <c:when test="${param.option == 'inbox'}">
                        <fmt:message key='messagecenter.inboxHeader'/>
                    </c:when>
                    <c:otherwise>
                        <a href="messagecenter.html?option=inbox"><fmt:message key='messagecenter.inboxHeader'/></a>
                    </c:otherwise>
                </c:choose>&nbsp;/
                <c:choose>
                    <c:when test="${param.option == 'deleted'}">
                        <fmt:message key='messagecenter.deletedHeader'/>
                    </c:when>
                    <c:otherwise>
                        <a href="messagecenter.html?option=deleted"><fmt:message key='messagecenter.deletedHeader'/></a>
                    </c:otherwise>
                </c:choose>
                <c:if test="${canEdit}">
                    &nbsp;/<c:choose>
                        <c:when test="${param.option == 'sent'}">
                            <fmt:message key='messagecenter.sentHeader'/>
                        </c:when>
                        <c:otherwise>
                            <a href="messagecenter.html?option=sent"><fmt:message key='messagecenter.sentHeader'/></a>
                        </c:otherwise>
                    </c:choose>&nbsp;/
                    <c:choose>
                        <c:when test="${param.option == 'drafts'}">
                            <fmt:message key='messagecenter.draftsHeader'/>
                        </c:when>
                        <c:otherwise>
                            <a href="messagecenter.html?option=drafts"><fmt:message key='messagecenter.draftsHeader'/></a>
                        </c:otherwise>
                    </c:choose>
                    <div align="right"><a href="messageform.html"><fmt:message key="messagecenter.newmessage"/></a></div>
                </c:if>
            </display:caption>
    <display:column paramId="id" paramProperty="id" property="id" sortProperty="id" escapeXml="true" sortable="true" class="hidden" headerClass="hidden" />
    <display:column property="whenCreated" sortProperty="whenCreated" escapeXml="true" sortable="false" titleKey="messagecenter.whenCreated" style="width: 25%" />
    <display:column property="subject" escapeXml="false" sortable="false" titleKey="messagecenter.subject" style="width: 75%"/>

    <display:setProperty name="paging.banner.item_name" value="message"/> 
    <display:setProperty name="paging.banner.items_name" value="messages"/> 
</display:table> 
<!-- dynamically display messages -->
<div id="messageDiv" style="display:none">
    <fieldset>
        <legend>Message</legend>
        <ul id="msg_ul">
            <li></li>
            <li></li>
            <li></li>
        </ul>
        <p id="msg_para"></p>
        <div id="delDiv" align="right">
            <form id="messageCenter" name="message" method="post" action="messagecenter.html">
                <input type="hidden" id="id" name="id" value="" />
                <input type="hidden" name="option" value="<c:out value="${param.option}"/>"/>
                <c:if test="${param.option == 'inbox'}"><input type="submit" class="button" name="delete" value="<fmt:message key='button.delete'/>" onclick="setMsgId();bCancel=false;"/></c:if>
                <c:if test="${param.option == 'drafts'}"><input type="submit" class="button" name="edit" value="<fmt:message key='button.edit'/>" onclick="setMsgId();bCancel=false;"/></c:if>
            </form>
        </div>
    </fieldset>
</div>

<script type="text/javascript"> 
    highlightTableRows("message"); 
    addRowHandlers('message', 0);
</script>