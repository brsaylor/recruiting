<%@ include file="/common/taglibs.jsp"%> 

<head>
    <title><fmt:message key="systemStats.title"/></title> 
    <meta name="heading" content="<fmt:message key='systemStats.heading'/>"/> 
    <!-- meta name="menu" content="AdminMenu"/ -->
</head>
<input type="hidden" id="typeSelected" value="${type}"/>
<input type="hidden" id="roleSelected" value="${role}"/>
<a href="<c:url value='/statisticshome.html'/>">[back to statistics home]</a><br><br>

<c:if test="${param.type!='namedQuery'}">
    <table>
        <form method="POST">
            <td>
                <select id="yearSelected" name="yearSelected">
                    <c:forEach items="${statsYearList}" var="chosenYear">
                        <option <c:if test="${chosenYear == year}">selected</c:if> value="${chosenYear}"><c:out value="${chosenYear}"/></option>
                    </c:forEach>
                </select>
            </td>
            <td><input type="hidden" name="searchQuery" value="${searchQuery}"/></td>
            <td><input type="hidden" name="querySelected" value="${querySelected}"/></td>
            <td><input type="hidden" name="type" value="${type}"/></td>
            <td><input type="submit" value="Change Year" /></td>
        </form>
    </table>
</c:if>

<table <c:if test="${empty showSearch}"><c:out value="style=display:none"/></c:if> >
    <form method="GET">
        <td><input type="text" SIZE="25" name="searchQuery" value="${searchQuery}"/></td>
        <td>
            <select id="querySelected" name="querySelected">
                <c:forEach items="${queryList}" var="query">
                    <option <c:if test="${query == queryField}">selected</c:if> value="${query}"><c:out value="${query}"/></option>
                </c:forEach>
            </select>
        </td>
        <td><input type="hidden" name="year" value="${yearSelected}"/></td>
        <td><input type="hidden" name="type" value="${type}"/></td>
        <td><input type="submit" value="Search" /></td>
    </form>
</table>

<%-- BRS 2011-06-09 - take pagesize from request (also modified PaginateListFactory and ExtendedPaginatedListImpl) --%>
<c:set var="pageSize" value="${param.pageSize}"/>
<c:if test="${pageSize==null}">
    <c:set var="pageSize" value="25"/>
</c:if>


<%-- BRS 2012-02-07 --%>
<c:choose>

    <c:when test="${param.type=='namedQuery'}">

        <h2><b>Named Query: <c:out value="${param.queryName}"/></b></h2>

        <%-- BRS 2012-02-09 - Allow user to select pageSize. --%>
        <c:set var="pageSizes">
        10,25,50,100
        </c:set>
        <div>
            Results per page:
            <c:forEach var="s" items="${pageSizes}">
                <a href="systemstats.html?type=namedQuery&queryName=subjectStatistics&pageSize=<c:out value='${s}'/>"><c:out value='${s}'/></a>
            </c:forEach>
            <a href="systemstats.html?type=namedQuery&queryName=subjectStatistics&pageSize=100000">All</a>
        </div>

        <%-- BRS: tables for named query results --%>

        <display:table name="paginatedList" cellspacing="0" cellpadding="0" requestURI="" id="paginatedList" class="table" export="true">

            <c:choose>

                <c:when test="${param.queryName=='subjectStatistics'}">
                    <display:column property="first_name" sortable="true" title="First Name" url="/userform.html?from=list" paramId="id" paramProperty="id"/>
                    <display:column property="last_name" sortable="true" title="Last Name" url="/userform.html?from=list" paramId="id" paramProperty="id"/>
                    <display:column property="email" sortable="true" title="Email" url="/userform.html?from=list" paramId="id" paramProperty="id"/>
                    <display:column property="account_enabled" sortable="true" title="Enabled"/>
                    <display:column property="subscribed" sortable="true" title="Subscribed"/>
                    <display:column property="registrationDate" sortable="true" title="Registration Date"/>
                    <display:column property="lastLoginDate" sortable="true" title="Last Login Date"/>
                    <display:column property="total_experiments" sortable="true" title="Total Experiments"/>
                    <display:column property="future_experiments" sortable="true" title="Future Experiments"/>
                    <display:column property="cancelled_experiments" sortable="true" title="Cancelled Experiments"/>
                    <display:column property="participated_experiments" sortable="true" title="Participated Experiments"/>
                    <display:column property="bumps" sortable="true" title="Bumps"/>
                    <display:column property="noshows" sortable="true" title="No-shows"/>
                    <display:column property="total_payout" sortable="true" title="Total Payout"/>
                    <display:column property="avg_payout" sortable="true" title="Average Payout (total/participated)"/>
                </c:when>

                <c:when test="${param.queryName=='experimentTypeStatistics'}">
                    <display:column property="name" sortable="true" title="Experiment Type"/>
                    <display:column property="total_experiments" sortable="true" title="Total Experiments"/>
                    <display:column property="future_experiments" sortable="true" title="Future Experiments"/>
                    <display:column property="past_experiments" sortable="true" title="Past Experiments"/>
                    <display:column property="cancelled_experiments" sortable="true" title="Cancelled Experiments"/>
                    <display:column property="total_signedup" sortable="true" title="Total Signup"/>
                    <display:column property="total_participated" sortable="true" title="Total Participation"/>
                    <display:column property="avg_participated" sortable="true" title="Average Participation"/>
                    <display:column property="bumps" sortable="true" title="Bumps"/>
                    <display:column property="noshows" sortable="true" title="No-shows"/>
                    <display:column property="total_payout" sortable="true" title="Total Payout"/>
                    <display:column property="avg_payout" sortable="true" title="Average Payout (total/participated)"/>
                </c:when>

                <c:otherwise>
                    <%-- No valid named query given. --%>
                </c:otherwise>
            </c:choose>

            <display:setProperty name="paging.banner.item_name" value="result"/>
            <display:setProperty name="paging.banner.items_name" value="results"/>
            <display:setProperty name="export.excel.filename" value="${param.queryName}.xls"/>
            <display:setProperty name="export.csv.filename" value="${param.queryName}.csv"/>
            <display:setProperty name="export.pdf.filename" value="${param.queryName}.pdf"/>

        </display:table>

    </c:when>
    <c:otherwise>

        <%-- BRS: original stats table --%>
        <display:table name="statsList" cellspacing="0" cellpadding="0" requestURI="" sort="external"
        defaultsort="1" id="stats" pagesize="${pageSize}" class="table" export="true" partialList="true" size="resultSize">
            <display:column property="name" escapeXml="true" sortable="true" titleKey="statistic.title" />
            <display:column property="totalExperiments" escapeXml="true" sortable="false" titleKey="statistic.experiment.total" />
            <display:column property="openExperiments" escapeXml="true" sortable="false" titleKey="statistic.experiment.open" />
            <display:column property="cancelledExperiments" escapeXml="true" sortable="false" titleKey="statistic.experiment.cancelled" />
            <display:column property="finishedExperiments" escapeXml="true" sortable="false" titleKey="statistic.experiment.finished" />
            <display:column property="totalPayout" escapeXml="true" sortable="false" titleKey="statistic.totalPayout" />
            <display:column property="averagePayout" escapeXml="true" sortable="false" titleKey="statistic.averagePayout" />
            <display:column property="totalSignup" escapeXml="true" sortable="false" titleKey="statistic.totalSignup" />
            <display:column property="averageSignup" escapeXml="true" sortable="false" titleKey="statistic.averageSignup" />
            <display:column property="totalParticipated" escapeXml="true" sortable="false" titleKey="statistic.totalParticipated" />
            <display:column property="averageParticipated" escapeXml="true" sortable="false" titleKey="statistic.averageParticipated" />
            <c:if test="${param.type != 'subject'}">
                <display:column property="averageShowupFee" escapeXml="true" sortable="false" titleKey="statistic.averageShowupFee" />
            </c:if>

            <display:setProperty name="paging.banner.item_name" value="statistics"/>
            <display:setProperty name="paging.banner.items_name" value="statistics"/>
            <display:setProperty name="export.excel.filename" value="BaseStatistic StatisticList.xls"/>
            <display:setProperty name="export.csv.filename" value="BaseStatistic StatisticList.csv"/>
            <display:setProperty name="export.pdf.filename" value="BaseStatistic StatisticList.pdf"/>

        </display:table>
    </c:otherwise>
</c:choose>


<script type="text/javascript">
    highlightTableRows("stats");
</script>
