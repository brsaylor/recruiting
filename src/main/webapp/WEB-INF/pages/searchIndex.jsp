<%@ include file="/common/taglibs.jsp"%>
<head>
    <title><fmt:message key="searchIndex.title"/></title>
    <meta name="menu" content="AdminMenu"/>
</head>
<body id="searchIndex"/>

<P/>
<H2>Compass Index</H2>
<P/>Use the Index button to index the database using Compass::Gps.
The operation will delete the current index and reindex the database
based on the mappings and devices defined in the Compass::Gps
configuration context.
<FORM method="POST" action="<c:url value="/searchIndex.html"/>">
    <spring:bind path="command.doIndex">
        <INPUT type="hidden" name="doIndex" value="true" />
    </spring:bind> <INPUT type="submit" value="<fmt:message key="searchIndex.index"/>"/>
</FORM>
<c:if test="${! empty indexResults}">
    <P>Indexing took: <c:out value="${indexResults.indexTime}" />ms.
</c:if>
<P/>

