<%@ include file="/common/taglibs.jsp"%> 

<head>
    <title><fmt:message key="experimentForm.title"/></title>
    <meta name="heading" content="<fmt:message key='experimentForm.heading'/>"/>
    <meta name="menu" content="ExperimenterMenu"/>
    
    <script type="text/javascript" src="<c:url value='scripts/jscalendar/calendar.js'/>"></script>
    <script type="text/javascript" src="<c:url value='scripts/jscalendar/calendar-setup.js'/>"></script>
    <script type="text/javascript" src="<c:url value='scripts/jscalendar/lang/calendar-en.js'/>"></script>
    <script type='text/javascript' src='<c:url value="/dwr/engine.js"/>'></script>
    <script type='text/javascript' src='<c:url value="/dwr/interface/experimentManagerDWR.js"/>'></script>
    <script type='text/javascript' src='<c:url value="/dwr/util.js"/>'></script>
    <link rel="stylesheet" type="text/css" media="all" href="styles/jscalendar/skins/aqua/theme.css" title="Aqua" />
    
    <script type="text/javascript">
function setTime(hours, minutes, type) {
    var hourSel = $(type + "Hours");
    var minSel = $(type + "Minutes");
    var hour = (hours+11)%12 + 1;//So that hours = 0 ->12
    //alert("type="+type+"hours="+hours+"minutes="+minutes+"hour="+hour);
    for(j = 0; j < hourSel.length; j++) {
        if(hourSel.options[j].value == hour) {
            hourSel.selectedIndex = j;
        }
    }
    for(j = 0; j < minSel.length; j++) {
        if(minSel.options[j].value == minutes) {
            minSel.selectedIndex = j;
        }
    }
    
    if(hours >= 12 && hours != 24) {
        $(type + "AMPM").selectedIndex = 1;
    }
}

function formSubmit(theform) {
    if(!validateExperiment(theform)) {
        return false;
    }

    var i_minNumSubjects = parseInt($("minNumSubjects").value);
    var i_maxNumSubjects = parseInt($("maxNumSubjects").value);
    
    if(!bCancel && i_minNumSubjects > i_maxNumSubjects) {
        alert("<fmt:message key="experimentForm.invalidRange"/>");
        return false;
    }
    
    if(!bCancel && !$("checkedIRB").checked) {
        alert("<fmt:message key="experimentForm.uncheckedIRB"/>");
        return false;
    }
    
    if(!bCancel && $("payGatePayment1") == undefined) {
        $("manualPayment1").checked = true;
    } else if(!bCancel &&
            ($("manualPayment1").checked == false && $("payGatePayment1").checked == false)) {
        alert("Please select at least one payment option");
        return false;
    }

    // check that the start date is before the end date!
    //
    var myStartDate = new Date();
    var startHour24=hrTo24($("startAMPM"),$("startHours"));
    myStartDate.setFullYear($("myExpDate").value.substring(6,10));
    myStartDate.setMonth($("myExpDate").value.substring(0,2)-1);
    myStartDate.setDate($("myExpDate").value.substring(3,5));
    myStartDate.setHours(startHour24);
    myStartDate.setMinutes($("startMinutes").value);
    myStartDate.setSeconds(0);
    myStartDate.setMilliseconds(0);
    
    var myEndDate = new Date();
    var endHour24=hrTo24($("endAMPM"),$("endHours"));
    myEndDate.setFullYear($("myExpDate").value.substring(6,10));
    myEndDate.setMonth($("myExpDate").value.substring(0,2)-1);
    myEndDate.setDate($("myExpDate").value.substring(3,5));
    myEndDate.setHours(endHour24);
    myEndDate.setMinutes($("endMinutes").value);
    myEndDate.setSeconds(0);
    myEndDate.setMilliseconds(0);

    //alert("startDate: "+myStartDate+"\nendDate: "+myEndDate);
    if(!bCancel && myEndDate <= myStartDate) {
        alert("Invalid Experiment Scheduling:\nCheck that Start Time is before End Time");
        return false;
    }
        
    return true;
}

function hrTo24(selampm, selhourin) {
    
    var ampm = selampm.options[selampm.selectedIndex].value;
    if(ampm.indexOf('start') > -1)
        ampm = ampm.substring(5,7);
    else
        ampm = ampm.substring(3,5);
        
    var hourin = selhourin.options[selhourin.selectedIndex].value;
    
    if(hourin == 12 && ampm == "AM")
        return 0;
    else if(hourin == 12)
        return 12;
    if(ampm == "PM")
        return hourin+12;
    else
        return hourin;
}

function checkConflicts() {
    var dateString = $("myExpDate").value;
    var locId = $("myLoc").value;
    var expId = $("id").value;
    var reserveLaptops = $("reserveLaptops").checked ? "true" : "false";
    
    var startHoursSelect = $("startHours");
    var startHourSelected = startHoursSelect.options[startHoursSelect.selectedIndex].value;
    var startMinutesSelect = $("startMinutes");
    var startMinuteSelected = startMinutesSelect.options[startMinutesSelect.selectedIndex].value;
    var startCycle = $("startAMPM").value.substring(5,7);
    var startTimeString = startHourSelected+":"+startMinuteSelected+":"+startCycle;
    
    var endHoursSelect = $("endHours");
    var endHourSelected = endHoursSelect.options[endHoursSelect.selectedIndex].value;
    var endMinutesSelect = $("endMinutes");
    var endMinuteSelected = endMinutesSelect.options[endMinutesSelect.selectedIndex].value;
    var endCycle = $("endAMPM").value.substring(3,5);
    var endTimeString = endHourSelected+":"+endMinuteSelected+":"+endCycle;
    
    var setupBufferSelect = $("setupBuffer");
    var setupBufferString = setupBufferSelect.options[setupBufferSelect.selectedIndex].value;
    var cleanupBufferSelect = $("cleanupBuffer");
    var cleanupBufferString = cleanupBufferSelect.options[cleanupBufferSelect.selectedIndex].value;
    
    //alert("in checkConflicts: "+dateString+", "+startTimeString+", "+endTimeString+", "+expId+", "+locId);
    experimentManagerDWR.getConflicts(
        dateString, startTimeString, setupBufferString, endTimeString, cleanupBufferString, expId, locId, reserveLaptops, resolveConflicts);
}

function resolveConflicts(exps) {
    var conflicts = "Scheduled experiment conflicts with:";
    if(exps.length > 0) {
        for(i=0; i<exps.length; i++) {
            var expPublished = 
            conflicts = conflicts + "\n\tid: " + exps[i].id + ") reservedBy: " + exps[i].reservedBy.firstName +
                    " " + exps[i].reservedBy.lastName + ", " + (exps[i].published == "true" ? "PUBLISHED" : "NOT PUBLISHED");
        }
        alert(conflicts + "\n\n - Check the calendar.  Experiments that conflict but are NOT published should be negotiated with the experimenter"+
                "\n\n - Conflicts may occur when experiment setup or cleanup periods overlap");
    } else
        alert("No Conflicts");
}

function markIRB(location) {
    var IRB_top = $("checkedIRB");
    var IRB_bottom = $("checkedIRB_bottom");
    if(location == 'top')
        IRB_bottom.checked = IRB_top.checked;
    else
        IRB_top.checked = IRB_bottom.checked;
}
    </script>
</head>

<%--Trying to get calendar to show up for setting date, based on http://issues.appfuse.org/browse/APF-268 --%>

<%@ include file="/common/progressDiv.jsp"%>

<spring:bind path="experiment">
    <c:if test="${not empty status.errorMessage}">
        <div class="error">
            <img src="<c:url value="/images/iconWarning.gif"/>"
                 alt="<fmt:message key="icon.warning"/>" class="icon" />
            <c:out value="${status.errorMessage}" escapeXml="false"/><br />
        </div>
    </c:if>
</spring:bind>

<form:form commandName="experiment" method="post" action="experimentform.html" name="experiment" id="experimentForm" onsubmit="return formSubmit(this);">
    <form:hidden path="id"/>
    
    <%-- Need to pass in: List of experimenters (exrList), current experimenter (?),
    list of experiment types (expTypeList), list of locations (locList) --%>
    
    <ul>
        <li>
            <input type="checkbox" name="checkedIRB" id="checkedIRB" onclick="markIRB('top')"/>&nbsp;<appfuse:label key="experiment.checkedIRB"/>
        </li>
        <li>
            <div class="left">
                <appfuse:label key="experiment.myRunby" styleClass="desc" colon="false"/>
                <select id="myRunby" name="myRunby">
                    <c:forEach items="${exrList}" var="expr">
                        <c:choose>
                            <c:when test="${expr.id == experiment.runby.id}">
                                <option value="${expr.id}" selected><c:out value="${expr.fullName}"/></option>
                            </c:when>
                            <c:otherwise>
                                <option value="${expr.id}"><c:out value="${expr.fullName}"/></option>
                            </c:otherwise>
                        </c:choose>
                    </c:forEach>
                </select>
            </div>
            <%-- BRS 2011-02-15 remove now that we have ExperimentType.principalInvestigators
            <div>
                <appfuse:label key="experiment.myPrinciple" styleClass="desc" colon="false"/>
                <select id="myPrinciple" name="myPrinciple">
                    <c:forEach items="${exrList}" var="expr">
                        <c:choose>
                            <c:when test="${expr.id == experiment.principle.id}">
                                <option value="${expr.id}" selected><c:out value="${expr.fullName}"/></option>
                            </c:when>
                            <c:otherwise>
                                <option value="${expr.id}"><c:out value="${expr.fullName}"/></option>
                            </c:otherwise>
                        </c:choose>
                    </c:forEach>
                </select>
            </div>
            --%>
        </li>
        <li>
            <div class="left">
                <appfuse:label key="experiment.myType" colon="false" styleClass="desc"/>
                <select id="myType" name="myType">
                    <c:forEach items="${expTypeList}" var="etype">
                        <c:choose>
                            <c:when test="${etype.id == experiment.type.id}">
                                <option value="${etype.id}" selected><c:out value="${etype.name}"/></option>
                            </c:when>
                            <c:otherwise>
                                <option value="${etype.id}"><c:out value="${etype.name}"/></option>
                            </c:otherwise>
                        </c:choose>
                    </c:forEach>
                </select>
            </div>
            <div>
                <appfuse:label key="experiment.statusSelect" colon="false" styleClass="desc"/>
                <select id="statusSelect" name="statusSelect">
                    <c:forEach items="${statusList}" var="status">
                        <option value="${status.name}" 
                                <c:if test="${status.name == experiment.status.name}">selected</c:if> >
                        <c:out value="${status.string}"/></option>
                    </c:forEach>
                </select>
            </div>
        </li>
        <li>
            <div class="left">
                <appfuse:label key="experiment.myExpDate" styleClass="desc" colon="false"/>
                <input path="myExpDate" type="text" class="text small" id="myExpDate" maxlength="10" name="myExpDate" 
                    <c:if test="${experiment.expDate != null}">value="<fmt:formatDate value='${experiment.expDate}' type='date' pattern='MM/dd/yyyy'/>"</c:if>/>
            </div>
            <div>
                <appfuse:label key="experiment.myLoc" colon="false" styleClass="desc"/>
                <select id="myLoc" name="myLoc">
                    <c:forEach items="${locList}" var="loc">
                        <c:choose>
                            <c:when test="${loc.id == experiment.location.id}">
                                <option value="${loc.id}" selected><c:out value="${loc.name}"/></option>
                            </c:when>
                            <c:otherwise>
                                <option value="${loc.id}"><c:out value="${loc.name}"/></option>
                            </c:otherwise>
                        </c:choose>
                    </c:forEach>
                </select>
            </div>
        </li>
        <li>
            <div>
                <appfuse:label key="experiment.reserveLaptops" colon="false" styleClass="desc"/>
                <form:checkbox id="reserveLaptops" path="reserveLaptops"/>
            </div>
        </li>
        <li>
            <label class="desc"><fmt:message key="experiment.startTime"/> <span class="req">*</span></label>
            <select id="startHours" name="startHours">
                <c:forEach begin="1" end="12" varStatus="status">
                    <option value="${status.index}"><c:out value="${status.index}"/></option>
                </c:forEach>
            </select>
            <select id="startMinutes" name="startMinutes">
                <c:forEach begin="0" end="11" varStatus="status">
                    <option value="${status.index*5}">
                        <c:choose>
                            <c:when test="${status.index*5 < 10}">
                                <c:out value="0${status.index*5}"/>
                            </c:when>
                            <c:otherwise>
                                <c:out value="${status.index*5}"/>
                            </c:otherwise>
                        </c:choose>
                    </option>
                </c:forEach>
            </select>
            <select id="startAMPM" name="startAMPM">
                <option id="startAM" value="startAM">AM</option>
                <option id="startPM" value="startPM">PM</option>
            </select>
            <c:if test="${experiment.startTime != null}">
                <script type="text/javascript">
                    setTime(${experiment.startTime.hours}, ${experiment.startTime.minutes}, "start");
                </script>
            </c:if>
        </li>
        <li>
            <label class="desc"><fmt:message key="experiment.endTime"/> <span class="req">*</span></label>
            <select id="endHours" name="endHours">
                <c:forEach begin="1" end="12" varStatus="status">
                    <option value="${status.index}"><c:out value="${status.index}"/></option>
                </c:forEach>
            </select>
            <select id="endMinutes" name="endMinutes">
                <c:forEach begin="0" end="11" varStatus="status">
                    <option value="${status.index*5}">
                        <c:choose>
                            <c:when test="${status.index*5 < 10}">
                                <c:out value="0${status.index*5}"/>
                            </c:when>
                            <c:otherwise>
                                <c:out value="${status.index*5}"/>
                            </c:otherwise>
                        </c:choose>
                    </option>
                </c:forEach>
            </select>
            <select id="endAMPM" name="endAMPM">
                <option id="endAM" value="endAM">AM</option>
                <option id="endPM" value="endPM">PM</option>
            </select>
            <c:if test="${experiment.endTime != null}">
                <script type="text/javascript">
                    setTime(${experiment.endTime.hours}, ${experiment.endTime.minutes}, "end");
                </script>
            </c:if>
            &nbsp;<img src="images/arrow.gif" />&nbsp;<input type="button" onclick="checkConflicts();" value="<fmt:message key="button.checkConflicts"/>"/>&nbsp;<img src="images/leftarrow.gif" />
        </li>
        <li>
            <label class="desc"><fmt:message key="experiment.numSubjects"/> <span class="req">*</span></label>
            <form:input path="minNumSubjects" id="minNumSubjects"  cssClass="text small"/> - <form:input path="maxNumSubjects" id="maxNumSubjects"  cssClass="text small"/>
        </li>
        <li>
            <div class="left">
                <appfuse:label key="experiment.signupFreezeTime" styleClass="desc" colon="false"/>
                <form:input path="signupFreezeTime" id="signupFreezeTime"/>
            </div>
            <div>
                <appfuse:label key="experiment.showUpFee" styleClass="desc" colon="false"/>
                <form:input path="showUpFee" id="showUpFee"/>
            </div>
        </li>
        <li>
            <label class="desc"><fmt:message key="experiment.setupBuffer"/> <span class="req">*</span></label>
            <select id="setupBuffer" name="setupBuffer">
                <option value="0" <c:if test="${experiment.setupBuffer == null || experiment.setupBuffer == 0}">selected</c:if>>0 min</option>
                <option value="5" <c:if test="${experiment.setupBuffer == 5}">selected</c:if>>5 min</option>
                <option value="10" <c:if test="${experiment.setupBuffer == 10}">selected</c:if>>10 min</option>
                <option value="15" <c:if test="${experiment.setupBuffer == 15}">selected</c:if>>15 min</option>
                <option value="20" <c:if test="${experiment.setupBuffer == 20}">selected</c:if>>20 min</option>
                <option value="25" <c:if test="${experiment.setupBuffer == 25}">selected</c:if>>25 min</option>
                <option value="30" <c:if test="${experiment.setupBuffer == 30}">selected</c:if>>30 min</option>
            </select>
            <label class="desc"><fmt:message key="experiment.cleanupBuffer"/> <span class="req">*</span></label>
            <select id="cleanupBuffer" name="cleanupBuffer">
                <option value="0" <c:if test="${experiment.cleanupBuffer == null || experiment.cleanupBuffer == 0}">selected</c:if>>0 min</option>
                <option value="5" <c:if test="${experiment.cleanupBuffer == 5}">selected</c:if>>5 min</option>
                <option value="10" <c:if test="${experiment.cleanupBuffer == 10}">selected</c:if>>10 min</option>
                <option value="15" <c:if test="${experiment.cleanupBuffer == 15}">selected</c:if>>15 min</option>
                <option value="20" <c:if test="${experiment.cleanupBuffer == 20}">selected</c:if>>20 min</option>
                <option value="25" <c:if test="${experiment.cleanupBuffer == 25}">selected</c:if>>25 min</option>
                <option value="30" <c:if test="${experiment.cleanupBuffer == 30}">selected</c:if>>30 min</option>
            </select>
        </li>
        <li>
            <appfuse:label key="experiment.instruction" styleClass="desc" colon="false"/>
            <form:textarea path="instruction" id="instruction" cssClass="text large"/>
        </li>
        <li>
            <%-- BRS: if PayGate is not configured, don't display "Manual Payment" checkbox either. --%>
            <c:choose>
                <c:when test="${isPayGateConfigured}">
                    <form:checkbox path="manualPayment"/>&nbsp;<b><appfuse:label key="experiment.manualpayment"/></b>
                    <form:checkbox path="payGatePayment"/>&nbsp;<b><appfuse:label key="experiment.paygatepayment"/></b>
                </c:when>
                <c:otherwise>
                    <div style="display: none">
                        <form:checkbox path="manualPayment"/>&nbsp;<b><appfuse:label key="experiment.manualpayment"/></b>
                        <form:checkbox path="payGatePayment"/>&nbsp;<b><appfuse:label key="experiment.paygatepayment"/></b>
                    </div>
                </c:otherwise>
            </c:choose>
        </li>
        <li>
            <form:checkbox path="invisible"/>&nbsp;<b><appfuse:label key="experiment.invisible"/></b>
        </li>
        <li>
            <input type="checkbox" name="checkedIRB_bottom" id="checkedIRB_bottom" onclick="markIRB('bottom')"/>&nbsp;<appfuse:label key="experiment.checkedIRB"/>
        </li>
        <li class="buttonBar bottom"> 
            <br>
            <input type="submit" class="button" name="next" value="<fmt:message key='button.next'/>" onclick="bCancel=false;"/> 
            <input type="submit" class="button" name="cancel" value="<fmt:message key='button.cancel'/>" onclick="bCancel=true;"/> 
            &nbsp;<i>or</i>&nbsp;<input type="submit" class="button" name="savenow" value="<fmt:message key='button.saveNow'/>" onclick="bCancel=false"/> 
        </li>
    </ul>
</form:form>    

<v:javascript formName="experiment" cdata="false" dynamicJavascript="true" staticJavascript="false"/>
<script type="text/javascript" src="<c:url value='/scripts/validator.jsp'/>"></script>

<script type="text/javascript">
    if($("payGatePayment1") == undefined)
        $("manualPayment1").checked=true;
        
    Form.focusFirstElement($('experimentForm'));
    Calendar.setup({
        inputField     :    "myExpDate",      // id of the input field
        ifFormat       :    "%m/%d/%Y",       // format of the input field
        showsTime      :    false,            // will display a time selector
        button         :    "myExpDate",   // trigger for the calendar (button ID)
        singleClick    :    true,           // double-click mode
        step           :    1                // show all years in drop-down boxes (instead of every other year as default)
    });
</script>
