<%@ include file="/common/taglibs.jsp"%> 

<head>
    <title><fmt:message key="experimenter.title"/></title>
    <meta name="heading" content="<fmt:message key='experimenter.heading'/>"/>
    <meta name="menu" content="ExperimenterMenu"/>
    
    <script type="text/javascript" src="<c:url value='/scripts/common.js'/>"></script>
    <script type="text/javascript">
        
function setupExp() {
    var selObj = $("myExperimentSelect");
    var optAry = selObj.options;
    var idx = selObj.selectedIndex;
    if(optAry[idx].value != "new" && optAry[idx].value != "") {
        document.location.href = "modifyexperiment.html?id="+optAry[idx].value;
    } else {
        alert("<fmt:message key="experiment.select.invalid"/>");
    }
}
    </script>
</head>

<h2>Experiment Type Management</h2>
<ul class="glassList">
    <li><a href="experimenttype.html">View/Edit Existing Types</a></li>
    <li><a href="experimenttypeform.html">Add New Type</a></li>
</ul>

<h2>Experiment Management</h2>
<ul class="glassList">
    <li><a href="clearsession.html?view=experiment">Create New Experiment</a></li>
    <li><a href="calendar.html">Select From Calendar</a>&nbsp;<i>OR</i>&nbsp;
        <select name="myExperiment" id="myExperimentSelect">
            <option value="new"><fmt:message key="experiment.select.choose"/></option>
            <c:forEach items="${expList}" var="expItem">
                <c:if test="${expItem.status.name != 'FINISHED'}">
                    <option value="${expItem.id}"><c:out value="${expItem.formattedDescription}"/></option>
                </c:if>
            </c:forEach>
        </select>
        &nbsp;<input type="button" onclick="setupExp()" value="<fmt:message key='button.view'/>">
    </li>
</ul>

<h2>Personal Settings</h2>
<ul class="glassList">
    <li><a href="userform.html">Edit My Profile</a></li>
</ul>