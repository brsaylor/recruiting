/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package edu.caltech.ssel.recruiting.webapp.controller;

import edu.caltech.ssel.recruiting.model.Experiment;
import edu.caltech.ssel.recruiting.model.Question;
import edu.caltech.ssel.recruiting.model.QuestionResult;
import edu.caltech.ssel.recruiting.service.QuestionManager;
import edu.caltech.ssel.recruiting.service.QuestionResultManager;
import edu.caltech.ssel.recruiting.service.ExperimentManager;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.appfuse.service.GenericManager;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import java.util.List;
import java.util.ArrayList;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import org.appfuse.service.GenericManager;

/**
 *
 * @author Richard Ho
 */
public class QuestionController implements Controller {
    
    private QuestionManager questionManager;
    private QuestionResultManager questionResultManager;
    private ExperimentManager experimentManager;
    private Log log = LogFactory.getLog(QuestionController.class);
    
    /** Creates a new instance of QuestionController */
    public QuestionController() {
    }

    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
        Experiment e = null;
        List<QuestionResult> qr = new ArrayList();
        List<Question> questions = new ArrayList();
        
        String id = request.getParameter("id");
        request.setAttribute("id",request.getParameter("id"));
        e = experimentManager.get(Long.parseLong(id));
        try {
            if(e.getSurvey() != null)
                questions = e.getSurvey().getQuestions();
        } catch(Exception any) {
            log.warn("Improper experiment ID");
        }
        if(questions.isEmpty())
            log.debug("There are no questions in this survey");
        return new ModelAndView().addObject("questions", questions);
    }
    
    public QuestionManager getQuestionManager() {
        return questionManager;
    }

    public void setQuestionManager(QuestionManager questionManager) {
        this.questionManager = questionManager;
    }
    
    public QuestionResultManager getQuestionResultManager() {
        return questionResultManager;
    }
    
    public void setQuestionResultManager(QuestionResultManager questionResultManager) {
        this.questionResultManager = questionResultManager;
    }
    
    public ExperimentManager getExperimentManager() {
        return experimentManager;
    }
    
    public void setExperimentManager(ExperimentManager experimentManager) {
        this.experimentManager = experimentManager;
    }

}
