/*
 * Copyright (C) 1998-2009, <a href="http://www.ssel.caltech.edu">SSEL</a>,
 * at Caltech, Pasadena, CA 91125. 
 *
 * MooreRecruiting is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 * USA.
 *
 */

package edu.caltech.ssel.recruiting.webapp.controller;

import edu.caltech.ssel.recruiting.common.DebugHelper;
import edu.caltech.ssel.recruiting.common.QuestionResultHelper;
import edu.caltech.ssel.recruiting.common.RecruitingConstants;
import edu.caltech.ssel.recruiting.model.Experiment;
import edu.caltech.ssel.recruiting.model.Experimenter;
import edu.caltech.ssel.recruiting.model.Experimenter.Title;
import edu.caltech.ssel.recruiting.model.Question;
import edu.caltech.ssel.recruiting.model.QuestionResult;
import edu.caltech.ssel.recruiting.model.Subject;
import edu.caltech.ssel.recruiting.model.SubjectPool;
import edu.caltech.ssel.recruiting.model.ParticipantRecord;
import edu.caltech.ssel.recruiting.service.ExtendedUserManager;
import edu.caltech.ssel.recruiting.service.QuestionResultManager;
import edu.caltech.ssel.recruiting.service.SubjectPoolManager;
import edu.caltech.ssel.recruiting.service.SurveyManager;
import edu.caltech.ssel.recruiting.service.EmailManager;
import edu.caltech.ssel.recruiting.service.ParticipantRecordManager;
import edu.caltech.ssel.recruiting.service.ReminderManager;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import org.springframework.security.AccessDeniedException;
import org.springframework.security.Authentication;
import org.springframework.security.AuthenticationTrustResolver;
import org.springframework.security.AuthenticationTrustResolverImpl;
import org.springframework.security.context.SecurityContext;
import org.springframework.security.context.SecurityContextHolder;
import org.apache.commons.lang.StringUtils;
import org.appfuse.Constants;
import org.appfuse.model.Role;
import org.appfuse.model.User;
import org.appfuse.service.RoleManager;

import org.appfuse.webapp.util.RequestUtil;
import org.appfuse.webapp.controller.BaseFormController;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.List;
import java.util.Date;
import java.util.Calendar;
import java.util.GregorianCalendar;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.appfuse.model.LabelValue;
import org.appfuse.service.GenericManager;
import org.appfuse.service.UserExistsException;
import org.springframework.validation.Errors;
import org.springframework.mail.*;

// BRS
// based on UserFormController
public class SubjectPreferencesFormController extends BaseFormController {
    private ExtendedUserManager extendedUserManager;
    private EmailManager emailManager;
    private ParticipantRecordManager participantRecordManager;
    private ReminderManager reminderManager;
    
    private final Log log = LogFactory.getLog(SubjectPreferencesFormController.class);
    private List<LabelValue> smsGatewayList;
    
    private String subjectSuccessView;
    private String experimenterSuccessView;
    private String adminSuccessView;
    private String testSMSTemplateName;
    
    public SubjectPreferencesFormController() {
        setCommandClass(Subject.class);
        setCommandName("subject");
        
        smsGatewayList = new ArrayList();

        smsGatewayList.add(new LabelValue("--Select Provider--", ""));
        
        // Alaska providers
        smsGatewayList.add(new LabelValue("ACS", "msg.acsalaska.com"));
        smsGatewayList.add(new LabelValue("Alaska Digitel", "alaskadigitel.com"));
        smsGatewayList.add(new LabelValue("Alaska Wireless Communications", "sms.alaska-wireless.com"));
        smsGatewayList.add(new LabelValue("AT&T", "txt.att.net"));
        smsGatewayList.add(new LabelValue("Cellular One", "mobile.celloneusa.com"));
        smsGatewayList.add(new LabelValue("Copper Valley Wireless", "cvwsms.com"));
        smsGatewayList.add(new LabelValue("GCI", "mobile.gci.net"));
        smsGatewayList.add(new LabelValue("MTA Wireless", "sms.mtawireless.com"));

        // Large nationwide providers (more than 1 million subscribers)
        // (that aren't already in the Alaska list)
        // See:
        //   http://en.wikipedia.org/wiki/List_of_United_States_mobile_phone_companies
        //   http://en.wikipedia.org/wiki/List_of_SMS_gateways
        smsGatewayList.add(new LabelValue("Cricket Communications", "sms.mycricket.com"));
        smsGatewayList.add(new LabelValue("MetroPCS", "mymetropcs.com"));
        smsGatewayList.add(new LabelValue("Sprint Nextel", "page.nextel.com"));
        smsGatewayList.add(new LabelValue("T-Mobile", "tmomail.net"));
        smsGatewayList.add(new LabelValue("Tracfone", "mmst5.tracfone.com"));
        smsGatewayList.add(new LabelValue("US Cellular", "email.uscc.net"));
        smsGatewayList.add(new LabelValue("Verizon Wireless", "vtext.com"));
        //smsGatewayList.add(new LabelValue("Leap Wireless", "")); // can't find gateway
    }

    protected Object formBackingObject(HttpServletRequest request)
    throws Exception {
    
        log.debug("formBackingObject");
        
        if (!isFormSubmission(request)) {
            // If it is not a submission, setup the user from database or from scratch
            
            String userId = request.getParameter("id");//The id of the user being edited (unless we're adding)
            //If id is empty, set it to that of the current user
            if(StringUtils.isEmpty(userId))
                userId = extendedUserManager.getUserByUsername(request.getRemoteUser()).getId().toString();
            
            
            return extendedUserManager.getUser(userId);

        } else if (StringUtils.isNotEmpty(request.getParameter("id")) && request.getParameter("cancel") == null) {
            // populate user object from database, so all fields don't need to be hidden fields in form
            
            return extendedUserManager.getUser(request.getParameter("id"));
            
        }
        
        //For cancel submissions, just use default functions
        return super.formBackingObject(request);
    }

    public ModelAndView onSubmit(HttpServletRequest request,
            HttpServletResponse response, Object command,
            BindException errors)
            throws Exception {
        Subject user = (Subject) command;
        Locale locale = request.getLocale();

        // Both the mobile provider <select> and the manual SMS gateway <input> field are named smsGateway.
        // If the <select> value isn't blank, use that.
        // Otherwise, use the <input>.
        String smsGateway = request.getParameterValues("smsGateway")[0];
        if (smsGateway.trim().length() == 0)
            smsGateway = request.getParameterValues("smsGateway")[1];
        user.setSmsGateway(smsGateway);

        // Find all experiments for which this subject has signed up that haven't passed,
        // and re-create the Reminders.
        // Don't worry about having to process lots of ParticipantRecords.  Even if the user has participated in 100 experiments, how long could it take?
        List<ParticipantRecord> participantRecords = participantRecordManager.getBySubject(user);
        for (int i = 0; i < participantRecords.size(); i++) {
            Experiment e = participantRecords.get(i).getExp();
            if (!pastExperiment(e.getExpDate(), e.getStartTime())) {
                reminderManager.createExperimentReminders((User) user, e);
            }
        }

        try {
            log.debug("Trying to saveUser");
            user = (Subject) extendedUserManager.saveUser(user);
            log.debug("Saved user");
            saveMessage(request, getText("prefs.saved", user.getFullName(), locale));
        } catch (AccessDeniedException ade) {
            log.debug("AccessDenied");
            // thrown by UserSecurityAdvice configured in aop:advisor userManagerSecurity
            log.warn(ade.getMessage());
            response.sendError(HttpServletResponse.SC_FORBIDDEN);
            return null;
        }

        // Send test SMS message if requested
        if ("on".equals(request.getParameter("sendTestSMS"))) {
            log.debug("Sending test SMS message");
            Map model = new HashMap();
            model.put(Constants.EMAIL_TO, user.getMobileNumber() + "@" + user.getSmsGateway());
            model.put(Constants.EMAIL_SUBJECT, getText("prefs.testSMSSubject", locale));
            model.put("labName", getText("company.name", locale));
            try{
                emailManager.sendEmail(this.testSMSTemplateName, model);
            } catch (Exception e) {
                //log it and let it go
                log.debug("Ignoring Mail Errors");
            }
        } else {
            log.debug("NOT sending test SMS message");
        }
        
        return new ModelAndView(getSuccessView());
        //        return showForm(request, response, errors);
    }
    
    protected Map referenceData(HttpServletRequest request,
            Object command,
            Errors errors)
            throws Exception {
        
        Map m = new HashMap();
        
        m.put("smsGatewayList", smsGatewayList);
        
        return m;
    }



    protected ModelAndView handleRequestInternal(HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {
        DebugHelper.displayParams(request, log);

        log.debug("handleRequestInternal");
        
        User u = extendedUserManager.getUserByUsername(request.getRemoteUser());
        if("list".equals(request.getParameter("from"))) {
            setSuccessView(getAdminSuccessView());
        } else if(u instanceof Experimenter) {
            setSuccessView(getExperimenterSuccessView());
        } else {
            setSuccessView(getSubjectSuccessView());
        }
        return super.handleRequestInternal(request, response);
    }
    
    protected ModelAndView showForm(HttpServletRequest request,
            HttpServletResponse response,
            BindException errors)
            throws Exception {

        log.debug("showForm");
        
        // If not an adminstrator, make sure user is not trying to add or edit another user
        if (!request.isUserInRole(Constants.ADMIN_ROLE) && !isFormSubmission(request)) {
            if (request.getParameter("id") != null) {
                response.sendError(HttpServletResponse.SC_FORBIDDEN);
                log.warn("User '" + request.getRemoteUser() + "' is trying to edit user with id '" +
                        request.getParameter("id") + "'");
                
                throw new AccessDeniedException("You do not have permission to modify other users.");
            }
        }
        
        return super.showForm(request, response, errors);
    }
    
    //Cancel viewing should be handled fine by the views set in handleRequestInternal
    /*
    public ModelAndView processFormSubmission(HttpServletRequest request,
            HttpServletResponse response,
            Object command,
            BindException errors)
            throws Exception;*/

    // BRS copied from ReminderManagerImpl.java
    private boolean pastExperiment(Date expDate, Date expTime) {
        
        Date now = new Date();
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(expDate);
        
        Calendar timeCal = new GregorianCalendar();
        timeCal.setTime(expTime);
        
        calendar.set(Calendar.HOUR_OF_DAY, timeCal.get(Calendar.HOUR_OF_DAY));
        calendar.set(Calendar.MINUTE, timeCal.get(Calendar.MINUTE));

        if(now.after(calendar.getTime()))
            return true;
        else
            return false;
    }

    
    public String getSubjectSuccessView() {
        return subjectSuccessView;
    }
    
    public void setSubjectSuccessView(String subjectSuccessView) {
        this.subjectSuccessView = subjectSuccessView;
    }
    
    public String getExperimenterSuccessView() {
        return experimenterSuccessView;
    }
    
    public void setExperimenterSuccessView(String experimenterSuccessView) {
        this.experimenterSuccessView = experimenterSuccessView;
    }
    
    public String getAdminSuccessView() {
        return adminSuccessView;
    }
    
    public void setAdminSuccessView(String adminSuccessView) {
        this.adminSuccessView = adminSuccessView;
    }
    
    public void setExtendedUserManager(ExtendedUserManager extendedUserManager) {
        this.extendedUserManager = extendedUserManager;
    }

    public void setTestSMSTemplateName(String testSMSTemplateName) {
        this.testSMSTemplateName = testSMSTemplateName;
    }

    public void setEmailManager(EmailManager emailManager) {
        this.emailManager = emailManager;
    }

    public void setParticipantRecordManager(ParticipantRecordManager participantRecordManager) {
        this.participantRecordManager = participantRecordManager;
    }

    public void setReminderManager(ReminderManager reminderManager) {
        this.reminderManager = reminderManager;
    }
}
