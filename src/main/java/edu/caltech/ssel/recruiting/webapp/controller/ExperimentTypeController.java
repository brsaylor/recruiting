/*
 * Copyright (C) 1998-2009, <a href="http://www.ssel.caltech.edu">SSEL</a>,
 * at Caltech, Pasadena, CA 91125. 
 *
 * MooreRecruiting is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 * USA.
 *
 */

package edu.caltech.ssel.recruiting.webapp.controller;

import java.util.HashSet;
import java.util.Set;

import edu.caltech.ssel.recruiting.common.ExtendedPaginatedList;
import edu.caltech.ssel.recruiting.common.PaginateListFactory;
import edu.caltech.ssel.recruiting.service.PagingManager;
import edu.caltech.ssel.recruiting.model.ExperimentType;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.appfuse.service.GenericManager;
import org.displaytag.properties.SortOrderEnum;
import org.displaytag.tags.TableTagParameters;
import org.displaytag.util.ParamEncoder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

/**
* @author Michael Kolodrubetz, SSEL, Caltech
* @author <a href="mailto:wmyuan@hss.caltech.edu">Walter M. Yuan</a>, SSEL, Caltech
*/
public class ExperimentTypeController implements Controller {
    
    private final Log log = LogFactory.getLog(ExperimentTypeController.class);
    private GenericManager<ExperimentType, Long> manager = null;
    
    public void setExperimentTypeManager(GenericManager<ExperimentType, Long> mgr) {
        manager = mgr;
    }
    
    public ExperimentTypeController () {
    }
            
    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
        log.debug("entering 'handleRequest' method");
        Set<ExperimentType> typesSet = new HashSet();
        typesSet.addAll(manager.getAll()); // the backing List isn't unique based on ExperimentType.name
        return new ModelAndView().addObject(typesSet);
        /*ExtendedPaginatedList paginatedList = paginateListFactory.getPaginatedListFromRequest(request);
        paginatedList.setSearch(false);
        //default values
        int index = 0;
        String column = "name";
        SortOrderEnum order = SortOrderEnum.ASCENDING;
        //values derived from the table
        String stringIndex = request.getParameter(new ParamEncoder("experimentType").encodeParameterName(TableTagParameters.PARAMETER_PAGE));
        String stringCol = request.getParameter(new ParamEncoder("experimentType").encodeParameterName(TableTagParameters.PARAMETER_SORT));
        String stringOrder = request.getParameter(new ParamEncoder("experimentType").encodeParameterName(TableTagParameters.PARAMETER_ORDER));
        //Change values accordingly
        if(stringIndex != null) { index = (Integer.parseInt(stringIndex)) -1; }
        if(stringOrder != null && stringOrder.equals("2")) { order = SortOrderEnum.DESCENDING; }
        if(stringCol != null) { column = stringCol; }
        //Set values to paginatedList
        paginatedList.setIndex(index);
        paginatedList.setSortCriterion(column);
        paginatedList.setSortDirection(order);
        pmgr.getExperimentTypes(paginatedList);
        return new ModelAndView("experimentType", "experimentTypeList", paginatedList.getList()).addObject("resultSize", paginatedList.getFullListSize());
         */
    }

    /*public void setPagingManager(PagingManager pagingManager) {
        this.pmgr = pagingManager;
    }
    
    public void setPaginateListFactory(PaginateListFactory paginateListFactory) {
        this.paginateListFactory = paginateListFactory;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }*/
}