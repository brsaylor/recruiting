/*
 * Copyright (C) 1998-2009, <a href="http://www.ssel.caltech.edu">SSEL</a>,
 * at Caltech, Pasadena, CA 91125. 
 *
 * MooreRecruiting is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 * USA.
 *
 */

package edu.caltech.ssel.recruiting.webapp.controller;

import edu.caltech.ssel.recruiting.model.ExperimentType;
import edu.caltech.ssel.recruiting.model.Experimenter;
import edu.caltech.ssel.recruiting.model.Filter;
import edu.caltech.ssel.recruiting.model.Condition;
import edu.caltech.ssel.recruiting.model.ExperimentTypeCondition;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import org.appfuse.webapp.controller.BaseFormController;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang.StringUtils;
import org.appfuse.model.User;
import org.appfuse.service.GenericManager;
import edu.caltech.ssel.recruiting.service.ExtendedUserManager;
import edu.caltech.ssel.recruiting.service.FilterManager;
import edu.caltech.ssel.recruiting.service.ConditionManager;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;

/**
* @author Michael Kolodrubetz, SSEL, Caltech
* @author <a href="mailto:wmyuan@hss.caltech.edu">Walter M. Yuan</a>, SSEL, Caltech
*/
public class ExperimentTypeFormController extends BaseFormController {
    private GenericManager<ExperimentType, Long> experimentTypeManager = null;
    private ExtendedUserManager extendedUserManager = null;
    private FilterManager filterManager = null; // BRS
    private ConditionManager conditionManager = null; // BRS
    
    /** Creates a new instance of ExperimentTypeFormController */
    public ExperimentTypeFormController() {
        setCommandClass(ExperimentType.class);
        setCommandName("experimentType");
    }
    
    public void setExperimentTypeManager(GenericManager<ExperimentType, Long> experimentTypeManager) {
        this.experimentTypeManager = experimentTypeManager;
    }
    
    protected Object formBackingObject(HttpServletRequest request) throws Exception {
        String id = request.getParameter("id");
        if(StringUtils.isEmpty(id))
            return new ExperimentType();
        else
            return experimentTypeManager.get(Long.parseLong(id));
    }
    
    protected Map referenceData(HttpServletRequest request) throws Exception {
        Map m = new HashMap();
        Set<ExperimentType> typesSet = new HashSet();
        typesSet.addAll(experimentTypeManager.getAll()); // the backing List isn't unique based on ExperimentType.name
        m.put("expTypeList", typesSet);

        // BRS 2011-02-11
        m.put("experimenters", extendedUserManager.getExperimenters());

        return m;
    }
    
    public ModelAndView onSubmit(HttpServletRequest request,
            HttpServletResponse response, Object command,
            BindException errors)
            throws Exception {
        log.debug("Entering onSubmit...");
        String success = getSuccessView();
        ExperimentType type = (ExperimentType)command;
        boolean isNew = false;
        Locale locale = request.getLocale();
        
        if(type.getId() == null)
            isNew = true;
        
        if(request.getParameter("delete") != null) {
            log.debug("The id for the deleted type is " + type.getId() + "...");
            experimentTypeManager.remove(type.getId());
            saveMessage(request, getText("experimentType.deleted", locale));
        } else {
            String s = "experimentType.updated";
            if(isNew) {
                // check against existing names.  name is unique
                List<ExperimentType> expTypeList = experimentTypeManager.getAll();
                for(ExperimentType aType : expTypeList) {
                    if(aType.getName().equals(type.getName())) {
                        errors.rejectValue("name", "experimentType.duplicate",
                                new Object[]{type.getName()}, "duplicate name");
                        return showForm(request, response, errors);
                    }
                }
                type.setRegDate(new Date());
                type.setValid(true);
                //Set the current user as the creator
                String uname = request.getRemoteUser();
                User u = extendedUserManager.getUserByUsername(uname);
                type.setCreator(u);
                
                s = "experimentType.added";
                
            }
            
            // BRS 2011-02-11
            // Set the principalInvestigators of this ExperimentType
            String[] principalInvestigatorIDs = request.getParameterValues("principalInvestigatorIDs");
            ArrayList<Experimenter> principalInvestigators = new ArrayList();
            if (principalInvestigatorIDs != null) {
                for (int i = 0; i < principalInvestigatorIDs.length; i++) {
                    principalInvestigators.add((Experimenter) extendedUserManager.getExperimenterById(new Long(principalInvestigatorIDs[i])));
                }
            }
            type.setPrincipalInvestigators(principalInvestigators);

            // BRS 2011-06-13
            type.setContactPerson((Experimenter)extendedUserManager.getUser(request.getParameter("myContactPerson")));

            saveMessage(request, getText(s, locale));
            type = experimentTypeManager.save(type);

            // BRS 2011-12-06
            // Create the initial default filter for this experiment type,
            // which consists of two
            // ExperimentTypeConditions, one for past experiments and one for
            // future experiments, preventing the same subject from signing up
            // or participating more than once.
            Filter filter = type.getDefaultFilter();
            List<Condition> conditions;
            if (filter == null) {
                filter = new Filter();
                conditions = new ArrayList();
                conditions.add(new ExperimentTypeCondition());
                conditions.add(new ExperimentTypeCondition());
                ArrayList<ExperimentType> types = new ArrayList();
                types.add(type);
                for (int i = 0; i < conditions.size(); i++) {
                    ExperimentTypeCondition condition = (ExperimentTypeCondition) conditions.get(i);
                    condition.setTypes(types);
                    condition.setHistory(i == 0); // One condition for future experiments, one for past
                    condition.setOnList(false);
                    condition = (ExperimentTypeCondition) conditionManager.save(condition);
                    conditions.set(i, condition);
                }
                filter.setConditions(conditions);
                filter = filterManager.save(filter);
                type.setDefaultFilter(filter);
                experimentTypeManager.save(type);
            }
        }
        
        return new ModelAndView(success);
    }
    
    public void setExtendedUserManager(ExtendedUserManager extendedUserManager) {
        this.extendedUserManager = extendedUserManager;
    }

    // BRS 2011-10-28
    public void setFilterManager(FilterManager filterManager) {
        this.filterManager = filterManager;
    }
    public void setConditionManager(ConditionManager conditionManager) {
        this.conditionManager = conditionManager;
    }
    
}
