/*
 * Copyright (C) 1998-2009, <a href="http://www.ssel.caltech.edu">SSEL</a>,
 * at Caltech, Pasadena, CA 91125. 
 *
 * MooreRecruiting is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 * USA.
 *
 */

package edu.caltech.ssel.recruiting.webapp.controller;

import edu.caltech.ssel.recruiting.common.QuestionAndAnswerList;
import edu.caltech.ssel.recruiting.model.Experiment;
import edu.caltech.ssel.recruiting.model.SubjectPool;
import java.util.Date;
import java.util.Locale;
import org.appfuse.webapp.controller.BaseFormController;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang.StringUtils;
import org.appfuse.service.GenericManager;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;

/**
* @author Michael Kolodrubetz, SSEL, Caltech
* @author <a href="mailto:wmyuan@hss.caltech.edu">Walter M. Yuan</a>, SSEL, Caltech
*/
public class SubjectPoolFormController extends BaseFormController {
    private GenericManager<SubjectPool, Long> subjectPoolManager = null;
    private GenericManager<QuestionAndAnswerList, Long> questionAndAnswerListManager = null;
    private GenericManager<Experiment, Long> experimentManager = null;
            
    
    /** Creates a new instance of SubjectPoolFormController */
    public SubjectPoolFormController() {
        setCommandClass(SubjectPool.class);
        setCommandName("subjectPool");
    }
    
    public void setSubjectPoolManager(GenericManager<SubjectPool, Long> subjectPoolManager) {
        this.subjectPoolManager = subjectPoolManager;
    }
    
    protected Object formBackingObject(HttpServletRequest request) throws Exception {
        request.setAttribute("subjPoolList", subjectPoolManager.getAll());
        
        String id = request.getParameter("id");
        if(StringUtils.isEmpty(id))
            return new SubjectPool();
        else
            return subjectPoolManager.get(Long.parseLong(id));
    }
    
    public ModelAndView onSubmit(HttpServletRequest request,
            HttpServletResponse response, Object command,
            BindException errors)
            throws Exception {
        log.debug("Entering onSubmit...");
        String success = getSuccessView();
        SubjectPool pool = (SubjectPool)command;
        boolean isNew = false;
        Locale locale = request.getLocale();
        
        if(pool.getId() == null)
            isNew = true;
        
        if(request.getParameter("delete") != null) {
            log.debug("The id for the deleted pool is " + pool.getId() + "...");
            subjectPoolManager.remove(pool.getId());
            saveMessage(request, getText("subjectPool.deleted", locale));
        }
        else {
            String s = "subjectPool.updated";
            if(isNew) {
                pool.setRegDate(new Date());
                pool.setValid(true);
                s = "subjectPool.added";
            }
            saveMessage(request, getText(s, locale));
            subjectPoolManager.save(pool);
        }
        
        return new ModelAndView(success);
    }

    public void setQuestionAndAnswerListManager(GenericManager<QuestionAndAnswerList, Long> questionAndAnswerListManager) {
        this.questionAndAnswerListManager = questionAndAnswerListManager;
    }

    public void setExperimentManager(GenericManager<Experiment, Long> experimentManager) {
        this.experimentManager = experimentManager;
    }
    
}
