/*
 * Copyright (C) 1998-2009, <a href="http://www.ssel.caltech.edu">SSEL</a>,
 * at Caltech, Pasadena, CA 91125. 
 *
 * MooreRecruiting is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 * USA.
 *
 */

package edu.caltech.ssel.recruiting.webapp.controller;

import edu.caltech.ssel.recruiting.common.QuestionAndAnswerList;
import edu.caltech.ssel.recruiting.common.QuestionResultHelper;
import edu.caltech.ssel.recruiting.common.RecruitingConstants;
import edu.caltech.ssel.recruiting.model.Question;
import edu.caltech.ssel.recruiting.model.QuestionResult;
import edu.caltech.ssel.recruiting.service.QuestionResultManager;
import edu.caltech.ssel.recruiting.service.SurveyManager;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.ui.AbstractProcessingFilter;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.appfuse.model.User;
import org.appfuse.service.RoleManager;
import org.appfuse.service.UserManager;
import org.appfuse.webapp.controller.BaseFormController;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;

/**
* @author Michael Kolodrubetz, SSEL, Caltech
* @author <a href="mailto:wmyuan@hss.caltech.edu">Walter M. Yuan</a>, SSEL, Caltech
*/
public class CheckProfileFormController extends BaseFormController {
    
    private final Log log = LogFactory.getLog(CheckProfileFormController.class);
    private SurveyManager surveyManager;
    private QuestionResultManager questionResultManager;
    private UserManager userManager;
    private RoleManager roleManager;
    
    private String subjectDefaultView;
    private String adminDefaultView;
    private String experimenterDefaultView;
    
    /** Creates a new instance of CheckProfileFormController */
    public CheckProfileFormController() {
        setCommandClass(QuestionAndAnswerList.class);
        setCommandName("questionAndAnswerList");
    }
    
    protected ModelAndView handleRequestInternal(HttpServletRequest request, HttpServletResponse response) throws Exception {
        String s = AbstractProcessingFilter.obtainFullSavedRequestUrl(request);
        User u = userManager.getUserByUsername(request.getRemoteUser());
        if(u != null && u.getId() != null) {
            u.setLastLoginDate(new Date());
            userManager.saveUser(u);
        }
        
        //If we are supposed to redirect to the default page...
        if(s == null) {
            if(u.getRoles().contains(roleManager.getRole(RecruitingConstants.SUBJ_ROLE)))
                s = getSubjectDefaultView();
            else if(u.getRoles().contains(roleManager.getRole(RecruitingConstants.EXPTR_ROLE)))
                s = getExperimenterDefaultView();
            else if(u.getRoles().contains(roleManager.getRole(RecruitingConstants.ADMIN_ROLE)))
                s = getAdminDefaultView();
            else
                s = "calendar.html";
        }
        log.debug("Success url is " + s);
        setSuccessView("redirect:" + s);
        
        return super.handleRequestInternal(request, response);
    }
    
    protected ModelAndView showForm(
            HttpServletRequest request, HttpServletResponse response, BindException errors)
            throws Exception {
        //If there are no questions to ask, forward to the next page
        QuestionAndAnswerList qal = (QuestionAndAnswerList) errors.getModel().get(getCommandName());
        
        if(!request.isUserInRole(RecruitingConstants.SUBJ_ROLE) || qal.getQuestions().size() == 0) {
            if(!request.isUserInRole(RecruitingConstants.SUBJ_ROLE))
                log.debug("User is not in subject role. Continuing to next form");    
            else
                log.debug("There are no questions. Continuing to next form");    
            return new ModelAndView(getSuccessView());
        }
        
        return super.showForm(request, response, errors);
    }
    
    protected Object formBackingObject(HttpServletRequest request) throws Exception {
        log.debug("Entering formBackingObject...");
        
        //Set request attribute for possible forwarding in the html
        QuestionAndAnswerList qal = new QuestionAndAnswerList();
        List<Question> lst = new ArrayList();
        List<Question> prof = surveyManager.getProfileSurvey().getQuestions();
        List<Integer> ans = new ArrayList();
        int sz = prof.size();
        int j;
        
        User u = userManager.getUserByUsername(request.getRemoteUser());
        QuestionResult qr;
        Calendar checkDate = new GregorianCalendar();
        checkDate.setTime(new Date());
        checkDate.add(Calendar.MONTH, -2);
        
        for(j = 0; j < sz; j++) {
            qr = questionResultManager.getMostRecentAnswer(u, prof.get(j));
            Calendar qrDate = new GregorianCalendar();
            if(qr != null)
                qrDate.setTime(qr.getTakenDate());
            if(qr == null || qrDate.before(checkDate)) {
                lst.add(prof.get(j));
                ans.add(0);
            }
        }
        
        int [] ansarr = new int[ans.size()];
        for(j = 0; j < ans.size(); j++)
            ansarr[j] = ans.get(j).intValue();
        
        qal.setAnswers(ansarr);
        qal.setQuestions(lst);
        return qal;
    }
    
    public ModelAndView onSubmit(HttpServletRequest request,
            HttpServletResponse response, Object command,
            BindException errors)
            throws Exception {
        log.debug("Entering onSubmit...");
        String success = getSuccessView();
        QuestionAndAnswerList qal = (QuestionAndAnswerList)command;
        Locale locale = request.getLocale();
        
        //Parse the data
        String s, s2;
        Question q;
        QuestionResult qr;
        User u = userManager.getUserByUsername(request.getRemoteUser());
        Date d = new Date();
        List<Question> lst = qal.getQuestions();
        boolean eligible = true;
        
        for(int j = 0; j < lst.size(); j++) {
            q = lst.get(j);
            qr = new QuestionResult();
            qr.setQuestion(q);
            qr.setSurveyParent(null);
            qr.setSurveyTaker(u);
            qr.setTakenDate(d);
            
            //Parse the answer
            qr.setAnswerIdx(QuestionResultHelper.parseAnswer(request, q, j));
            log.debug("The answer to question number " + (j+1) + " was " + qr.getAnswerIdx());
            /*int ans = 0;
            int sz = q.getOptionList().size();
            switch (q.getStyle()) {
                case CHECK_BOX:
                    boolean [] blist = new boolean[sz];
                    for(int k = 0; k < sz; k++) {
                        s = "question" + j + "," + k;
                        if(request.getParameter(s) != null)
                            blist[k] = true;
                        else
                            blist[k] = false;
                    }
                    ans = QuestionResultHelper.setDigits(blist);
                    break;
                case RADIO:
                case DROP_LIST:
                    s = "question" + j;
                    ans = Integer.parseInt(request.getParameter(s));
                    break;
                default:
                    log.debug("There is some sort of problem, since no style was selected");
            }
            
            //log.debug("For question " + j + ", the answer was " + ans);
            qr.setAnswerIdx(ans);*/
            
            questionResultManager.save(qr);
        }
        
        return new ModelAndView(success);
    }
    
    public void setSurveyManager(SurveyManager surveyManager) {
        this.surveyManager = surveyManager;
    }
    
    public void setQuestionResultManager(QuestionResultManager questionResultManager) {
        this.questionResultManager = questionResultManager;
    }
    
    public void setUserManager(UserManager userManager) {
        this.userManager = userManager;
    }
    
    public void setSubjectDefaultView(String subjectDefaultView) {
        this.subjectDefaultView = subjectDefaultView;
    }
    
    public void setAdminDefaultView(String adminDefaultView) {
        this.adminDefaultView = adminDefaultView;
    }
    
    public void setExperimenterDefaultView(String experimenterDefaultView) {
        this.experimenterDefaultView = experimenterDefaultView;
    }
    
    public void setRoleManager(RoleManager roleManager) {
        this.roleManager = roleManager;
    }
    
    public String getSubjectDefaultView() {
        return subjectDefaultView;
    }
    
    public String getAdminDefaultView() {
        return adminDefaultView;
    }
    
    public String getExperimenterDefaultView() {
        return experimenterDefaultView;
    }
    
}
