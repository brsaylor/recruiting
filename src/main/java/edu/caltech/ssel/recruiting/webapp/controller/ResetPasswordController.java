/*
 * Copyright (C) 1998-2009, <a href="http://www.ssel.caltech.edu">SSEL</a>,
 * at Caltech, Pasadena, CA 91125. 
 *
 * MooreRecruiting is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 * USA.
 *
 */

package edu.caltech.ssel.recruiting.webapp.controller;

import edu.caltech.ssel.recruiting.model.Subject;
import edu.caltech.ssel.recruiting.service.EmailManager;
import edu.caltech.ssel.recruiting.service.ExtendedUserManager;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Random;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.appfuse.Constants;
import org.appfuse.model.User;
import org.appfuse.service.UserManager;
import org.appfuse.webapp.util.RequestUtil;
import org.appfuse.webapp.controller.BaseFormController;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author michaelk
 */
public class ResetPasswordController extends BaseFormController {
    //Just using this superclass for convenience methods
    
    private static final String charSet = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";
    private static final int RESET_PASSWORD_LENGTH = 8;
    private UserManager userManager;
    private ExtendedUserManager extendedUserManager;

    private EmailManager emailManager;
    private String calendarView;
    
    /** Creates a new instance of ResetPasswordController */
    public ResetPasswordController() {
        setCommandClass(Subject.class); // BRS changed from User to Subject, so we can use schoolId
        setCommandName("user");
    }
    
    //Let it form a default backing object
    
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response,
            Object command, BindException errors)
            throws Exception {
        Subject userParams = (Subject) command;
        Locale locale = request.getLocale();
        
        boolean valid = true;
        Subject user = null;
        List<Subject> userList = null;
        
        //Get the user specified from the database and check that it has the same parameters
        try {

            // BRS Aug 2010
            // If user has entered email address, use that to find their record.
            // Otherwise, assume they have entered their ID Number (schoolId) and use that instead.
            if (request.getParameter("email").trim().length() > 0) {
                user = extendedUserManager.getSubjectByEmail(userParams.getEmail());
                if(!user.getEmail().equalsIgnoreCase(userParams.getEmail()) ||
                        !user.getLastName().equalsIgnoreCase(userParams.getLastName()) ||
                        !user.getFirstName().equalsIgnoreCase(userParams.getFirstName())) {
                    valid = false;
                }
            } else {
                userList = extendedUserManager.getSubjectBySchoolID(userParams.getSchoolId());
                if (userList.isEmpty()) {
                    valid = false;
                } else {
                    user = userList.get(0);
                    if(!user.getSchoolId().equalsIgnoreCase(userParams.getSchoolId()) ||
                            !user.getLastName().equalsIgnoreCase(userParams.getLastName()) ||
                            !user.getFirstName().equalsIgnoreCase(userParams.getFirstName())) {
                        valid = false;
                    }
                }
            }
            
        } catch (Exception e) {
            valid = false;
        }
        
        if(!valid) {
            saveMessage(request, getText("resetPassword.invalid", locale));
            
            return showForm(request, response, errors);
        }
        
        //Set the password to some new random value
        String newPasswd = randomPassword();
        user.setPassword(newPasswd);
        user.setConfirmPassword(newPasswd);//Unencrypted password for later
        
        //Save the user with the new password
        log.debug("About to call extendedUserManager.saveUser(user);");
        user = (Subject) extendedUserManager.saveUser(user);
        // BRS 2012-01-20: userManager.saveUser() password changing is broken
        //log.debug("About to call userManager.saveUser(user);");
        //user = (Subject) userManager.saveUser((User) user);
        
        // Send an e-mail with the new password
        log.debug("Sending user '" + user.getUsername() + "' a new password e-mail");
        
        Map model = new HashMap();
        model.put(Constants.EMAIL_TO, user.getEmail());
        model.put(Constants.EMAIL_SUBJECT, getText("resetPassword.email.subject", locale));
        model.put(Constants.USER, user);
        model.put("calendarView", RequestUtil.getAppURL(request) + this.calendarView);
        model.put("applicationURL", RequestUtil.getAppURL(request));
        model.put("labName", getText("company.name", locale));
        model.put("labUrlName", getText("company.url", locale));
        model.put("labPhone", getText("company.phone", locale));
        emailManager.sendEmail(this.templateName, model);
        //sendUserMessage(user, getText("resetPassword.email.message", locale), RequestUtil.getAppURL(request));
        
        saveMessage(request, getText("resetPassword.success", user.getEmail(), locale));
        return new ModelAndView(getSuccessView());
    }
    
    public void setUserManager(UserManager userManager) {
        log.debug("setUserManager called");
        this.userManager = userManager;
    }
    
    public String randomPassword() {
        Random rand  = new Random();
        int sz = charSet.length();
        String passwd = "";
        for(int j = 0; j < RESET_PASSWORD_LENGTH; j++) {
            passwd += charSet.charAt(rand.nextInt(sz));
        }
        return passwd;
    }

    /**
     * @param emailManager the emailManager to set
     */
    public void setEmailManager(EmailManager emailManager) {
        this.emailManager = emailManager;
    }
    
    public void setExtendedUserManager(ExtendedUserManager extendedUserManager) {
        log.debug("setExtendedUserManager called");
        this.extendedUserManager = extendedUserManager;
    }

    /**
     * @param calendarView the calendarView to set
     */
    public void setCalendarView(String calendarView) {
        this.calendarView = calendarView;
    }
}

