/*
 * Copyright (C) 1998-2009, <a href="http://www.ssel.caltech.edu">SSEL</a>,
 * at Caltech, Pasadena, CA 91125. 
 *
 * MooreRecruiting is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 * USA.
 *
 */

package edu.caltech.ssel.recruiting.webapp.controller;

import edu.caltech.ssel.recruiting.common.DebugHelper;
import edu.caltech.ssel.recruiting.common.QuestionResultHelper;
import edu.caltech.ssel.recruiting.common.RecruitingConstants;
import edu.caltech.ssel.recruiting.model.Question;
import edu.caltech.ssel.recruiting.model.QuestionResult;
import edu.caltech.ssel.recruiting.model.Subject;
import edu.caltech.ssel.recruiting.model.SubjectPool;
import edu.caltech.ssel.recruiting.service.EmailManager;
import edu.caltech.ssel.recruiting.service.ExtendedUserManager;
import edu.caltech.ssel.recruiting.service.QuestionResultManager;
import edu.caltech.ssel.recruiting.service.SubjectPoolManager;
import edu.caltech.ssel.recruiting.service.SurveyManager;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import org.springframework.security.AccessDeniedException;
import org.springframework.security.context.SecurityContextHolder;
import org.springframework.security.providers.UsernamePasswordAuthenticationToken;
import org.appfuse.Constants;
import org.appfuse.service.RoleManager;
import org.appfuse.webapp.controller.BaseFormController;
import org.appfuse.service.UserExistsException;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.mail.MailSendException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Locale;
import java.util.Map;
import org.appfuse.model.LabelValue;
import org.appfuse.service.GenericManager;
import org.appfuse.webapp.util.RequestUtil;
import org.springframework.validation.Errors;

/**
 * Controller to signup new users.
 *
 */
public class SignupController extends BaseFormController {
    private RoleManager roleManager;
    private SurveyManager surveyManager;
    private SubjectPoolManager subjectPoolManager;
    private QuestionResultManager questionResultManager;
    private EmailManager emailManager;
    private ExtendedUserManager extendedUserManager;
    private List<LabelValue> monthList;
    //private List<QuestionResult> recentAnswers;

    private String calendarView;
    
    public void setRoleManager(RoleManager roleManager) {
        this.roleManager = roleManager;
    }
    
    public SignupController() {
        setCommandName("user");
        setCommandClass(Subject.class);
        
        monthList = new ArrayList(12);
        monthList.add(new LabelValue("Jan", "31"));
        monthList.add(new LabelValue("Feb", "29"));
        monthList.add(new LabelValue("Mar", "31"));
        monthList.add(new LabelValue("Apr", "30"));
        monthList.add(new LabelValue("May", "31"));
        monthList.add(new LabelValue("Jun", "30"));
        monthList.add(new LabelValue("Jul", "31"));
        monthList.add(new LabelValue("Aug", "31"));
        monthList.add(new LabelValue("Sep", "30"));
        monthList.add(new LabelValue("Oct", "31"));
        monthList.add(new LabelValue("Nov", "30"));
        monthList.add(new LabelValue("Dec", "31"));
    }
    
    protected Map referenceData(HttpServletRequest request,
            Object command,
            Errors errors)
            throws Exception {
        
        Subject u = (Subject) command;
        Map m = new HashMap();
        String s = null;
        
        //Set up subject pools
        List<SubjectPool> allPools = subjectPoolManager.getValidPools();
        List<LabelValue> availLVPools = new ArrayList(allPools.size());
        
        for(SubjectPool sp : allPools)
            availLVPools.add(new LabelValue(sp.getName(), sp.getId().toString()));
        
        m.put("availLVPools", availLVPools);
        
        //Setup the profile questions and answers
        List<Question> profQs = surveyManager.getProfileSurvey().getQuestions();
        
        m.put("profile_qs", profQs);
        
        m.put("monthList", monthList);
        
        //log.debug("Leaving referencing data with map == " + m);
        
        return m;
    }

    // BRS April 2010
    // (should be identically defined in UserFormController)
    // Make a Date object from the request parameteters <prefix>_year, <prefix>_month, and <prefix>_day,
    // returning null if those parameters cannot be parsed as numbers.
    private Date makeDate(HttpServletRequest request, String prefix) {
        Date d;

        try {
           d = new GregorianCalendar(
                        Integer.parseInt(request.getParameter(prefix + "_year")),
                        Integer.parseInt(request.getParameter(prefix + "_month")),
                        Integer.parseInt(request.getParameter(prefix + "_day"))
                ).getTime();

        } catch (NumberFormatException e) {
            log.debug("Caught NumberFormatException, setting " + prefix + " to null");
            d = null;
        }

        return d;
    }

    // BRS August 2010
    // (should be identically defined in UserFormController)
    // Simpler version that just takes year, month, and day as strings
    private Date makeDate2(String year, String month, String day) {
        Date d;

        try {
           d = new GregorianCalendar(
                        Integer.parseInt(year),
                        Integer.parseInt(month),
                        Integer.parseInt(day)
                ).getTime();

        } catch (NumberFormatException e) {
            log.debug("Caught NumberFormatException, setting date to null");
            d = null;
        }

        return d;
    }
    
    public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response,
            Object command, BindException errors)
            throws Exception {
        if (log.isDebugEnabled()) {
            log.debug("entering 'onSubmit' method...");
            DebugHelper.displayParams(request, log);
        }

        // BRS Aug 2010
        // Check that required fields are present.
        // (Unable to get commons validator working.)
        // COPY TO UserFormController.java - except the password field checks.
        if (request.getParameter("email").trim().length() == 0)
            errors.rejectValue("email", "errors.required", new Object[]{"E-Mail"}, "");
        if (request.getParameter("firstName").trim().length() == 0)
            errors.rejectValue("firstName", "errors.required", new Object[]{"First Name"}, "");
        if (request.getParameter("lastName").trim().length() == 0)
            errors.rejectValue("lastName", "errors.required", new Object[]{"Last Name"}, "");
        if (request.getParameter("schoolId").trim().length() == 0)
            errors.rejectValue("schoolId", "errors.required", new Object[]{"ID Number"}, "");
        if (request.getParameter("idType") == null)
            errors.rejectValue("idType", "errors.required", new Object[]{"ID Type"}, "");
        if (request.getParameter("password").trim().length() == 0)
            errors.rejectValue("password", "errors.required", new Object[]{"Password"}, "");
        if (request.getParameter("confirmPassword").trim().length() == 0)
            errors.rejectValue("confirmPassword", "errors.required", new Object[]{"Confirm Password"}, "");
        if (errors.hasErrors())
            return showForm(request, response, errors);
        
        Subject user = (Subject) command;
        Locale locale = request.getLocale();

        /* Taking all this out. Make sure this is fine?
        Boolean encrypt = (Boolean) getConfiguration().get(Constants.ENCRYPT_PASSWORD);
        
        if (encrypt != null && encrypt) {
            String algorithm = (String) getConfiguration().get(Constants.ENC_ALGORITHM);
            
            if (algorithm == null) { // should only happen for test case
                log.debug("assuming testcase, setting algorithm to 'SHA'");
                algorithm = "SHA";
            }
            
            user.setPassword(StringUtil.encodePassword(user.getPassword(), algorithm));
        }*/
        
        user.setEnabled(true);
        
        // Set the default user role on this new user
        user.addRole(roleManager.getRole(RecruitingConstants.SUBJ_ROLE));
        
        //Set subject-specific fields
        //Subject Pools
        String[] userPools = request.getParameterValues("userPools");
        List<SubjectPool> pools;

        if(userPools != null) {
            pools = new ArrayList(userPools.length);
            for(String s : userPools) {
                pools.add(subjectPoolManager.get(Long.parseLong(s)));
            }
            user.setPools(pools);
        } else {
            errors.reject("errors.noPool","No subject pool selected");
            user.setPassword(user.getConfirmPassword());
            return showForm(request, response, errors);
        }
        
        //UID
        if(extendedUserManager.getSubjectBySchoolID(user.getSchoolId().trim()).size() > 0) {
            errors.rejectValue("schoolId","errors.existing.schoolId", "School ID already registered");
            user.setPassword(user.getConfirmPassword());
            return showForm(request, response, errors);
        }
        
        //Birthday
        //Date bday = new Date();
        //testing using gregorian calendar to set it properly
        //GregorianCalendar calendar = new GregorianCalendar(Integer.parseInt(request.getParameter("birthday_year")), Integer.parseInt(request.getParameter("birthday_month")), Integer.parseInt(request.getParameter("birthday_day")));
        //bday = calendar.getTime();
        //user.setBirthday(bday);
        user.setBirthday(makeDate(request, "birthday"));
        
        SimpleDateFormat sdf = new SimpleDateFormat("MM-dd-yyyy");
        if (user.getBirthday() != null) {
            log.debug("The birthday was set to " + sdf.format(user.getBirthday()));
        } else {
            log.debug("The birthday was set to null");
        }
        String str = "";
        if(user.getPools() != null) {
            for(int j = 0; j < user.getPools().size(); j++)
                str += user.getPools().get(j).getName() + ", ";
            log.debug("The subject pools are " + str);
        }
        
        // BRS March/April 2010
        // Save email address as username
        user.setUsername(user.getEmail());
        Subject s = (Subject)user;

        // BRS March/April 2010
        // ** The following lines until "**END BRS" also need to be present in
        // UserFormController.java!

        // Set date fields

            s.setDriversLicenseExpirationDate(makeDate(request, "driversLicenseExpirationDate"));
            s.setPriorCountryStartDate(this.makeDate(request, "priorCountryStartDate"));
            s.setPriorCountryEndDate(this.makeDate(request, "priorCountryEndDate"));
            s.setFirstUsEntryDate(this.makeDate(request, "firstUsEntryDate"));
            s.setCurrentUsEntryDate(this.makeDate(request, "currentUsEntryDate"));
            s.setI94ExpirationDate(this.makeDate(request, "i94ExpirationDate"));
            s.setPassportExpirationDate(this.makeDate(request, "passportExpirationDate"));
            s.setVisaExpirationDate(this.makeDate(request, "visaExpirationDate"));

            // Get values from travel table, which has a variable number of rows.  
            //
            String[] arrivalDates_year = request.getParameterValues("arrivalDates_year");
            String[] arrivalDates_month = request.getParameterValues("arrivalDates_month");
            String[] arrivalDates_day = request.getParameterValues("arrivalDates_day");
            String[] departureDates_year = request.getParameterValues("departureDates_year");
            String[] departureDates_month = request.getParameterValues("departureDates_month");
            String[] departureDates_day = request.getParameterValues("departureDates_day");

            ArrayList arrivalDates = new ArrayList();
            ArrayList departureDates = new ArrayList();
            ArrayList visaTypes = new ArrayList();
            ArrayList purposes = new ArrayList();

            // i is row index.  Start at 1, because the first row is the hiddenPrototypeTravelRow.
            for (int i = 1; i < arrivalDates_year.length; i++) {
                try {
                    arrivalDates.add(makeDate2(
                                arrivalDates_year[i],
                                arrivalDates_month[i],
                                arrivalDates_day[i]));
                    departureDates.add(makeDate2(
                                departureDates_year[i],
                                departureDates_month[i],
                                departureDates_day[i]));
                    visaTypes.add(request.getParameterValues("visaTypes")[i]);
                    purposes.add(request.getParameterValues("purposes")[i]);

                } catch (Exception e) {
                    // Probably array index out of bounds
                    log.debug("Caught exception while processing row " + i + " of travel table");
                }
            }

            s.setArrivalDates(arrivalDates);
            s.setDepartureDates(departureDates);
            s.setVisaTypes(visaTypes);
            s.setPurposes(purposes);

            // BRS 2013-05-21
            // If the subject has entered a signup code, add it to any subject
            // pools with a matching code.
            if (s.getSignupCode() != null && s.getSignupCode().trim().length() > 0) {
                List<SubjectPool> curPools = s.getPools();
                List<SubjectPool> allPools = subjectPoolManager.getValidPools();
                for (SubjectPool pool : allPools) {
                    if (s.getSignupCode().equalsIgnoreCase(pool.getSignupCode())) {
                        curPools.add(pool);
                    }
                }
                s.setPools(curPools);
            }

        // **END BRS
        
        // BRS 2012-01-27, 2012-02-01
        user.setLastLoginDate(new Date());
        user.setRegistrationDate(new Date());
        user.setSignedUp(new Boolean(true));
        
        try {
            user = (Subject) getUserManager().saveUser(user);
            
            //Profile questions are only saved once user is saved (they reference user in db)
            List<Question> profQ = surveyManager.getProfileSurvey().getQuestions();
            int sz = profQ.size(), j;
            int ans;
            QuestionResult qr;
            
            for(j = 0; j < sz; j++) {
                qr = new QuestionResult();
                qr.setAnswerIdx(QuestionResultHelper.parseAnswer(request, profQ.get(j), j));
                qr.setQuestion(profQ.get(j));
                qr.setSurveyParent(null);
                qr.setSurveyTaker(user);
                qr.setTakenDate(new Date());
                questionResultManager.save(qr);
            }
        } catch (AccessDeniedException ade) {
            // thrown by UserSecurityAdvice configured in aop:advisor userManagerSecurity
            log.warn(ade.getMessage());
            response.sendError(HttpServletResponse.SC_FORBIDDEN);
            return null;

        // BRS copied from UserFormController 2012-01-26
        } catch (UserExistsException e) {
            log.debug("UserExists");
            errors.rejectValue("username", "errors.existing.user",
                    new Object[] {user.getUsername(), user.getEmail()}, "duplicate user");
         
            log.warn(e.getMessage());
         
            return showForm(request, response, errors);
            //return new ModelAndView(getSuccessView());//To prevent database issues
        }
        
        saveMessage(request, getText("user.registered", user.getUsername(), locale));
        request.getSession().setAttribute(Constants.REGISTERED, Boolean.TRUE);
        
        // log user in automatically
        //UsernamePasswordAuthenticationToken auth = new UsernamePasswordAuthenticationToken(user.getUsername(), user.getConfirmPassword(), user.getAuthorities());
        // // BRS 2012-02-13 fixed to correctly set "principal" arg for UserCounterListener to catch just-registered subjects
        UsernamePasswordAuthenticationToken auth = new UsernamePasswordAuthenticationToken(user, user.getConfirmPassword(), user.getAuthorities());
        auth.setDetails(user);
        SecurityContextHolder.getContext().setAuthentication(auth);
        
        // Send user an e-mail
        if (log.isDebugEnabled()) {
            log.debug("Sending user '" + user.getUsername() + "' an account information e-mail");
        }
        
        // Send an account information e-mail
        //message.setSubject(getText("signup.email.subject", locale));
        //sendUserMessage(user, getText("signup.email.message", locale), RequestUtil.getAppURL(request));
        Map model = new HashMap();
        model.put(Constants.EMAIL_TO, user.getEmail());
        model.put(Constants.EMAIL_BCC, getText("company.email", locale));
        model.put(Constants.EMAIL_SUBJECT, getText("signup.email.subject", locale));
        model.put(Constants.USER, user);
        model.put("calendarView", RequestUtil.getAppURL(request) + this.calendarView);
        model.put("applicationURL", RequestUtil.getAppURL(request));
        model.put("labName", getText("company.name", locale));
        model.put("labUrlName", getText("company.url", locale));
        model.put("labPhone", getText("company.phone", locale));
        try{
            emailManager.sendEmail(this.templateName, model);
        } catch (Exception e) {
            //log it and let it go
            log.debug("Ignoring Mail Errors");
        }
        return new ModelAndView(getSuccessView());
    }
    
    public void setSurveyManager(SurveyManager surveyManager) {
        this.surveyManager = surveyManager;
    }
    
    public void setSubjectPoolManager(SubjectPoolManager subjectPoolManager) {
        this.subjectPoolManager = subjectPoolManager;
    }
    
    public void setQuestionResultManager(QuestionResultManager questionResultManager) {
        this.questionResultManager = questionResultManager;
    }

    /**
     * @param emailManager the emailManager to set
     */
    public void setEmailManager(EmailManager emailManager) {
        this.emailManager = emailManager;
    }
    
    public void setExtendedUserManager(ExtendedUserManager extendedUserManager) {
        this.extendedUserManager = extendedUserManager;
    }

    /**
     * @param calendarView the calendarView to set
     */
    public void setCalendarView(String calendarView) {
        this.calendarView = calendarView;
    }
    
}
