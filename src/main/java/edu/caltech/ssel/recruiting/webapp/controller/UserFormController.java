/*
 * Copyright (C) 1998-2009, <a href="http://www.ssel.caltech.edu">SSEL</a>,
 * at Caltech, Pasadena, CA 91125. 
 *
 * MooreRecruiting is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 * USA.
 *
 */

package edu.caltech.ssel.recruiting.webapp.controller;

import edu.caltech.ssel.recruiting.common.DebugHelper;
import edu.caltech.ssel.recruiting.common.QuestionResultHelper;
import edu.caltech.ssel.recruiting.common.RecruitingConstants;
import edu.caltech.ssel.recruiting.model.Experimenter;
import edu.caltech.ssel.recruiting.model.ExperimentType;
import edu.caltech.ssel.recruiting.model.Experimenter.Title;
import edu.caltech.ssel.recruiting.model.Question;
import edu.caltech.ssel.recruiting.model.QuestionResult;
import edu.caltech.ssel.recruiting.model.Subject;
import edu.caltech.ssel.recruiting.model.SubjectPool;
import edu.caltech.ssel.recruiting.service.ExtendedUserManager;
import edu.caltech.ssel.recruiting.service.QuestionResultManager;
import edu.caltech.ssel.recruiting.service.SubjectPoolManager;
import edu.caltech.ssel.recruiting.service.SurveyManager;
import edu.caltech.ssel.recruiting.service.ExperimentTypeManager;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import org.springframework.security.AccessDeniedException;
import org.springframework.security.Authentication;
import org.springframework.security.AuthenticationTrustResolver;
import org.springframework.security.AuthenticationTrustResolverImpl;
import org.springframework.security.context.SecurityContext;
import org.springframework.security.context.SecurityContextHolder;
import org.apache.commons.lang.StringUtils;
import org.appfuse.Constants;
import org.appfuse.model.Role;
import org.appfuse.model.User;
import org.appfuse.service.RoleManager;

import org.appfuse.webapp.util.RequestUtil;
import org.appfuse.webapp.controller.BaseFormController;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.appfuse.model.LabelValue;
import org.appfuse.service.GenericManager;
import org.appfuse.service.UserExistsException;
import org.springframework.validation.Errors;
import org.springframework.mail.*;

/**
 * Implementation of <strong>SimpleFormController</strong> that interacts with
 * the {@link UserManager} to retrieve/persist values to the database.
 *
 * <p><a href="UserFormController.java.html"><i>View Source</i></a>
 *
 * @author <a href="mailto:matt@raibledesigns.com">Matt Raible</a>
 * @author Michael Kolodrubetz, SSEL, Caltech
 * @author <a href="mailto:wmyuan@hss.caltech.edu">Walter M. Yuan</a>, SSEL, Caltech

 */
public class UserFormController extends BaseFormController {
    private RoleManager roleManager;
    private SubjectPoolManager subjectPoolManager;
    private SurveyManager surveyManager;
    private QuestionResultManager questionResultManager;
    private ExtendedUserManager extendedUserManager;
    private ExperimentTypeManager experimentTypeManager;
    
    private final Log log = LogFactory.getLog(UserFormController.class);
    private List<LabelValue> monthList;
    private List<QuestionResult> recentAnswers;
    
    private String subjectSuccessView;
    private String experimenterSuccessView;
    private String adminSuccessView;
    
    public void setRoleManager(RoleManager roleManager) {
        this.roleManager = roleManager;
    }
    
    public UserFormController() {
        setCommandName("user");
        setCommandClass(User.class);
        
        monthList = new ArrayList(12);
        monthList.add(new LabelValue("Jan", "31"));
        monthList.add(new LabelValue("Feb", "29"));
        monthList.add(new LabelValue("Mar", "31"));
        monthList.add(new LabelValue("Apr", "30"));
        monthList.add(new LabelValue("May", "31"));
        monthList.add(new LabelValue("Jun", "30"));
        monthList.add(new LabelValue("Jul", "31"));
        monthList.add(new LabelValue("Aug", "31"));
        monthList.add(new LabelValue("Sep", "30"));
        monthList.add(new LabelValue("Oct", "31"));
        monthList.add(new LabelValue("Nov", "30"));
        monthList.add(new LabelValue("Dec", "31"));
    }
    
    protected ModelAndView handleRequestInternal(HttpServletRequest request,
            HttpServletResponse response)
            throws Exception {
        DebugHelper.displayParams(request, log);
        
        User u = extendedUserManager.getUserByUsername(request.getRemoteUser());
        if("list".equals(request.getParameter("from")) || isAdd(request)) {
            setSuccessView(getAdminSuccessView());
        } else if(u instanceof Experimenter) {
            setSuccessView(getExperimenterSuccessView());
        } else {
            setSuccessView(getSubjectSuccessView());
        }
        return super.handleRequestInternal(request, response);
    }
    
    protected ModelAndView showForm(HttpServletRequest request,
            HttpServletResponse response,
            BindException errors)
            throws Exception {
        
        // If not an adminstrator, make sure user is not trying to add or edit another user
        if (!request.isUserInRole(Constants.ADMIN_ROLE) && !isFormSubmission(request)) {
            if (isAdd(request) || request.getParameter("id") != null) {
                response.sendError(HttpServletResponse.SC_FORBIDDEN);
                log.warn("User '" + request.getRemoteUser() + "' is trying to edit user with id '" +
                        request.getParameter("id") + "'");
                
                throw new AccessDeniedException("You do not have permission to modify other users.");
            }
        }
        
        return super.showForm(request, response, errors);
    }
    
    protected Object formBackingObject(HttpServletRequest request)
    throws Exception {
        
        log.debug("Entering myformbackingobject...");
        
        if (!isFormSubmission(request)) {
            // If it is not a submission, setup the user from database or from scratch
            
            String userId = request.getParameter("id");//The id of the user being edited (unless we're adding)
            //If id is empty, set it to that of the current user
            if(StringUtils.isEmpty(userId))
                userId = extendedUserManager.getUserByUsername(request.getRemoteUser()).getId().toString();
            
            // if user logged in with remember me, display a warning that they can't change passwords
            log.debug("checking for remember me login...");
            
            AuthenticationTrustResolver resolver = new AuthenticationTrustResolverImpl();
            SecurityContext ctx = SecurityContextHolder.getContext();
            
            if (ctx.getAuthentication() != null) {
                Authentication auth = ctx.getAuthentication();
                
                if (resolver.isRememberMe(auth)) {
                    request.getSession().setAttribute("cookieLogin", "true");
                    
                    // add warning message and set edit own only if the command object is the current user
                    if(extendedUserManager.getUserByUsername(request.getRemoteUser()).getId().toString().equals(userId) &&
                            !isAdd(request)) {
                        request.setAttribute("editingMe", "true");
                        saveMessage(request, getText("userProfile.cookieLogin", request.getLocale()));
                    }
                }
            }
            
            if(!isAdd(request) && !"".equals(request.getParameter("version"))) {
                //If it is not an add and the version is valid, get from database
                
                return extendedUserManager.getUser(userId);
            } else {
                //Otherwise, set roles and class type by parameter addUserType
                
                String type = request.getParameter("addUserType");
                if(StringUtils.equals(type, "Administrator")) {
                    User u = new User();
                    u.addRole(new Role(RecruitingConstants.ADMIN_ROLE));
                    return u;
                } else if(StringUtils.equals(type, "Experimenter")) {
                    Experimenter exptr = new Experimenter();
                    exptr.addRole(new Role(RecruitingConstants.EXPTR_ROLE));
                    return exptr;
                } else {
                    Subject subj = new Subject();
                    subj.addRole(new Role(RecruitingConstants.SUBJ_ROLE));
                    return subj;
                }
            }
            
        } else if (StringUtils.isNotEmpty(request.getParameter("id")) && request.getParameter("cancel") == null) {
            // populate user object from database, so all fields don't need to be hidden fields in form
            
            return extendedUserManager.getUser(request.getParameter("id"));
            
        } else if (StringUtils.isEmpty(request.getParameter("id")) && request.getParameter("cancel") == null) {
            //For empty id submissions, parse class type by role
            
            String[] roles = request.getParameterValues("userRoles");
            if(roles != null) {
                for(int j = 0; j < roles.length; j++) {
                    if(roles[j].equals(RecruitingConstants.SUBJ_ROLE))
                        return new Subject();
                    if(roles[j].equals(RecruitingConstants.EXPTR_ROLE))
                        return new Experimenter();
                }
            }
        }
        
        //For cancel submissions, just use default functions
        return super.formBackingObject(request);
    }
    
    protected Map referenceData(HttpServletRequest request,
            Object command,
            Errors errors)
            throws Exception {
        
        log.debug("Entering myreferencedata...");
        
        User u = (User) command;
        Map m = new HashMap();
        String s = null;
        
        List<Role> allRoles = roleManager.getRoles(null);//For some reason this was throwing a null pointer exception
        log.debug("allRoles.size() is: "+allRoles.size());
        Set<Role> userRoles = u.getRoles();
        log.debug("userRoles.size() is: "+userRoles.size());
        allRoles.removeAll(userRoles);
        log.debug("allRoles.size() is now: "+allRoles.size());
        
        final Role expRole = new Role(RecruitingConstants.EXPTR_ROLE);
        final Role subjRole = new Role(RecruitingConstants.SUBJ_ROLE);
        final Role adminRole = new Role(RecruitingConstants.ADMIN_ROLE);
        
        List<SubjectPool> allPools = subjectPoolManager.getValidPools();
        List<LabelValue> availLVPools = new ArrayList(allPools.size());
        List<LabelValue> userLVPools = new ArrayList();
        
        for(SubjectPool sp : allPools)
            availLVPools.add(new LabelValue(sp.getName(), sp.getId().toString()));
        
        if(request.isUserInRole(RecruitingConstants.ADMIN_ROLE))
            m.put("isAdmin","true");
        
        if(u instanceof Subject) {
            m.put("userType", "Subject");
            
            //Setup the label-value pair lists of subject pools
            List<SubjectPool> userPools = ((Subject)u).getPools();
            if(userPools != null) {
                userLVPools = new ArrayList(userPools.size());
                for(SubjectPool sp : userPools)
                    userLVPools.add(new LabelValue(sp.getName(), sp.getId().toString()));
                availLVPools.removeAll(userLVPools);
            }
            
            //Subjects cannot take on experimenter role if saved to database (class already set to Subject)
            if(u.getId() != null) {
                allRoles.remove(expRole);
                // a Subject can never become an admin
                allRoles.remove(adminRole);
            }
            
            //Setup the profile questions and answers
            List<Question> profQs = surveyManager.getProfileSurvey().getQuestions();
            int[] answers = new int[profQs.size()];
            recentAnswers = new ArrayList(profQs.size());
            
            for(int j = 0; j < profQs.size(); j++) {
                QuestionResult qr = questionResultManager.getMostRecentAnswer(u, profQs.get(j));
                recentAnswers.add(qr);
                answers[j] = (qr == null) ? 0 : qr.getAnswerIdx();
            }
            
            m.put("profile_qs", profQs);
            m.put("profile_ans", answers);
            
        } else if(u instanceof Experimenter) {
            
            m.put("userType", "Experimenter");
            
            //Experimenters cannot take on subject role if saved to database (class already set to Experimenter)
            if(u.getId() != null)
                allRoles.remove(subjRole);

            // BRS 2011-02-15 find experiment types of which this experimenter is PI
            List<ExperimentType> experimentTypes = experimentTypeManager.getAllSorted();
            ArrayList<ExperimentType> myExperimentTypes = new ArrayList();
            for (ExperimentType t : experimentTypes) {
                List<Experimenter> principalInvestigators = t.getPrincipalInvestigators();
                if (principalInvestigators != null) {
                    for (Experimenter e : principalInvestigators) {
                        if (e.getId() == u.getId()) {
                            myExperimentTypes.add(t);
                        }
                    }
                }
            }
            m.put("myExperimentTypes", myExperimentTypes);


        } else if(u.getId() != null) {
            
            //Administrators cannot take on others roles if saved to database (class already set to User)
            allRoles.remove(subjRole);
            allRoles.remove(expRole);
        }
        
        m.put("availLVPools", availLVPools);
        m.put("userLVPools", userLVPools);
        
        m.put("titleList", Arrays.asList(Title.values()));
        
        //Setups roles as Label-Value pair lists
        List<LabelValue> availLVRoles = new ArrayList(allRoles.size());
        List<LabelValue> userLVRoles = null;
        if(userRoles != null) {
            userLVRoles = new ArrayList(userRoles.size());
            for(Role r : allRoles) {
                availLVRoles.add(roleToLVPair(r));
            }
            
            for(Role r : userRoles) {
                userLVRoles.add(roleToLVPair(r));
            }
        }
        
        m.put("availRoleLVList", availLVRoles);
        m.put("userRoleLVList", userLVRoles);
        
        m.put("monthList", monthList);
        
        return m;
    }
    
    //Cancel viewing should be handled fine by the views set in handleRequestInternal
    /*
    public ModelAndView processFormSubmission(HttpServletRequest request,
            HttpServletResponse response,
            Object command,
            BindException errors)
            throws Exception;*/

    // BRS April 2010
    // (should be identically defined in SignupController)
    // Make a Date object from the request parameteters <prefix>_year, <prefix>_month, and <prefix>_day,
    // returning null if those parameters cannot be parsed as numbers.
    private Date makeDate(HttpServletRequest request, String prefix) {
        Date d;

        try {
           d = new GregorianCalendar(
                        Integer.parseInt(request.getParameter(prefix + "_year")),
                        Integer.parseInt(request.getParameter(prefix + "_month")),
                        Integer.parseInt(request.getParameter(prefix + "_day"))
                ).getTime();

        } catch (NumberFormatException e) {
            log.debug("Caught NumberFormatException, setting " + prefix + " to null");
            d = null;
        }

        return d;
    }
    
    // BRS August 2010
    // (should be identically defined in UserFormController)
    // Simpler version that just takes year, month, and day as strings
    private Date makeDate2(String year, String month, String day) {
        Date d;

        try {
           d = new GregorianCalendar(
                        Integer.parseInt(year),
                        Integer.parseInt(month),
                        Integer.parseInt(day)
                ).getTime();

        } catch (NumberFormatException e) {
            log.debug("Caught NumberFormatException, setting date to null");
            d = null;
        }

        return d;
    }
    
    public ModelAndView onSubmit(HttpServletRequest request,
            HttpServletResponse response, Object command,
            BindException errors)
            throws Exception {
        log.debug("Entering myonsubmit...");
        User user = (User) command;

        // BRS Aug 2010
        // Check that required fields are present.
        // (Unable to get commons validator working.)
        // COPY TO UserFormController.java - except the password field checks.
        if (request.getParameter("email").trim().length() == 0)
            errors.rejectValue("email", "errors.required", new Object[]{"E-Mail"}, "");
        if (request.getParameter("firstName").trim().length() == 0)
            errors.rejectValue("firstName", "errors.required", new Object[]{"First Name"}, "");
        if (request.getParameter("lastName").trim().length() == 0)
            errors.rejectValue("lastName", "errors.required", new Object[]{"Last Name"}, "");

        if (user instanceof Subject) {
            if (request.getParameter("schoolId").trim().length() == 0)
                errors.rejectValue("schoolId", "errors.required", new Object[]{"ID Number"}, "");
            if (request.getParameter("idType") == null)
                errors.rejectValue("idType", "errors.required", new Object[]{"ID Type"}, "");
            if (errors.hasErrors())
                return showForm(request, response, errors);
        }
        
        
        //Set the unbound standard user properties 
        String mypasswd = request.getParameter("mypassword");
        
        if(!StringUtils.isEmpty(mypasswd))
            user.setPassword(mypasswd); // reset later after encrypting?
        
        boolean isNew = (user.getId() == null ? true : false);
        log.debug("user's id is " + user.getId());
        
        if(user instanceof Subject) {
            log.debug("user is class Subject");
        } else if(user instanceof Experimenter) {
            log.debug("user is class Experimenter");
        } else {
            log.debug("user is class Administrator");
        }
        
        Locale locale = request.getLocale();
        Integer originalVersion = user.getVersion();
        
        if (request.getParameter("delete") != null) {
            extendedUserManager.removeUser(user.getId().toString());
            saveMessage(request, getText("user.deleted", user.getFullName(), locale));
            
            return new ModelAndView(getSuccessView());
        }
        
        //Otherwise, it's a save
        
        log.debug("User's first name is " + user.getFirstName());
        log.debug("User's last name is " + user.getLastName());

        // BRS March/April 2010
        // Save email address as username
        user.setUsername(user.getEmail());
        
        
        
        //Save it first, to catch cases of same username/email
        //This prevents problems later if we've saved roles and other fields
        try {
            log.debug("Trying to saveUser");
            user = extendedUserManager.saveUser(user);
            log.debug("Saved user");
        } catch (AccessDeniedException ade) {
            log.debug("AccessDenied");
            // thrown by UserSecurityAdvice configured in aop:advisor userManagerSecurity
            log.warn(ade.getMessage());
            response.sendError(HttpServletResponse.SC_FORBIDDEN);
            return null;
        } catch (UserExistsException e) {
            log.debug("UserExists");
            errors.rejectValue("username", "errors.existing.user",
                    new Object[] {user.getUsername(), user.getEmail()}, "duplicate user");
         
            log.warn(e.getMessage());
         
            return showForm(request, response, errors);
            //return new ModelAndView(getSuccessView());//To prevent database issues
        }
        log.debug("No exception");
        
        /*//Check whether password has been changed and if so whether to encrypt it
        Boolean encrypt = (Boolean) getConfiguration().get(Constants.ENCRYPT_PASSWORD);
        log.debug("encrypt value: " + encrypt);
        if((isNew || request.getParameter("changePassword") != null)
                && encrypt != null && encrypt) {
            String algorithm = (String) getConfiguration().get(Constants.ENC_ALGORITHM);
            
            if (algorithm == null) { // should only happen for test case
                log.debug("assuming testcase, setting algorithm to 'SHA'");
                algorithm = "SHA";
            }
            
            user.setPassword(StringUtil.encodePassword(user.getPassword(), algorithm));
            log.debug("password encrypted: "+algorithm);
        } else
            log.debug("**** not encrypting! ****");*/
        
        //Flags to set for whether we have an experimenter or a subject
        boolean isExpr = false, isSubj = false;
        
        // only attempt to change roles if user is admin for other users,
        // formBackingObject() method will handle populating
        if (request.isUserInRole(Constants.ADMIN_ROLE)) {
            String[] userRoles = request.getParameterValues("userRoles");
            
            if (userRoles != null) {
                user.getRoles().clear();
                for (String roleName : userRoles) {
                    if(roleName.equals(RecruitingConstants.SUBJ_ROLE))
                        isSubj = true;
                    if(roleName.equals(RecruitingConstants.EXPTR_ROLE))
                        isExpr = true;
                    
                    user.addRole(roleManager.getRole(roleName));
                }
            }
        } else {
            //If the user is not in an administrative role, then they must be editing their own profile
            isSubj = request.isUserInRole(RecruitingConstants.SUBJ_ROLE);
            isExpr = request.isUserInRole(RecruitingConstants.EXPTR_ROLE);
        }
        
        //Parse info specific to subjects
        if(isSubj) {
            //Subject Pools
            String[] userPools = request.getParameterValues("userPools");
            List<SubjectPool> pools;
            
            if(userPools != null) {
                pools = new ArrayList(userPools.length);
                for(String s : userPools) {
                    pools.add(subjectPoolManager.get(Long.parseLong(s)));
                }
                ((Subject)user).setPools(pools);
            } else {
                ((Subject)user).setPools(new ArrayList());//to prevent null pointer exceptions later
            }
            
            //Birthday
            //Date bday = new Date();
            //bday.setDate(Integer.parseInt(request.getParameter("birthday_day")));
            //bday.setMonth(Integer.parseInt(request.getParameter("birthday_month")));
            //bday.setYear(Integer.parseInt(request.getParameter("birthday_year"))-1900);
            //((Subject)user).setBirthday(bday);
            ((Subject)user).setBirthday(makeDate(request, "birthday"));
            
            Subject s = (Subject)user;
            SimpleDateFormat sdf = new SimpleDateFormat("MM-dd-yyyy");
            log.debug("The birthday was set to " + sdf.format(s.getBirthday()));
            String str = "";
            for(int j = 0; j < s.getPools().size(); j++)
                str += s.getPools().get(j).getName() + ", ";
            log.debug("The subject pools are " + str);

            // BRS 2012-02-01
            if (isNew) {
                s.setRegistrationDate(new Date());
                s.setSignedUp(new Boolean(false));
            }

            // BRS March/April 2010
            // ** The following lines until "**END BRS" also need to be present in
            // SignupController.java!

            // Set date fields

            s.setDriversLicenseExpirationDate(makeDate(request, "driversLicenseExpirationDate"));
            s.setPriorCountryStartDate(this.makeDate(request, "priorCountryStartDate"));
            s.setPriorCountryEndDate(this.makeDate(request, "priorCountryEndDate"));
            s.setFirstUsEntryDate(this.makeDate(request, "firstUsEntryDate"));
            s.setCurrentUsEntryDate(this.makeDate(request, "currentUsEntryDate"));
            s.setI94ExpirationDate(this.makeDate(request, "i94ExpirationDate"));
            s.setPassportExpirationDate(this.makeDate(request, "passportExpirationDate"));
            s.setVisaExpirationDate(this.makeDate(request, "visaExpirationDate"));

            // Get values from travel table, which has a variable number of rows.  
            //
            String[] arrivalDates_year = request.getParameterValues("arrivalDates_year");
            String[] arrivalDates_month = request.getParameterValues("arrivalDates_month");
            String[] arrivalDates_day = request.getParameterValues("arrivalDates_day");
            String[] departureDates_year = request.getParameterValues("departureDates_year");
            String[] departureDates_month = request.getParameterValues("departureDates_month");
            String[] departureDates_day = request.getParameterValues("departureDates_day");

            ArrayList arrivalDates = new ArrayList();
            ArrayList departureDates = new ArrayList();
            ArrayList visaTypes = new ArrayList();
            ArrayList purposes = new ArrayList();

            // i is row index.  Start at 1, because the first row is the hiddenPrototypeTravelRow.
            for (int i = 1; i < arrivalDates_year.length; i++) {
                try {
                    arrivalDates.add(makeDate2(
                                arrivalDates_year[i],
                                arrivalDates_month[i],
                                arrivalDates_day[i]));
                    departureDates.add(makeDate2(
                                departureDates_year[i],
                                departureDates_month[i],
                                departureDates_day[i]));
                    visaTypes.add(request.getParameterValues("visaTypes")[i]);
                    purposes.add(request.getParameterValues("purposes")[i]);

                } catch (Exception e) {
                    // Probably array index out of bounds
                    log.debug("Caught exception while processing row " + i + " of travel table");
                }
            }

            s.setArrivalDates(arrivalDates);
            s.setDepartureDates(departureDates);
            s.setVisaTypes(visaTypes);
            s.setPurposes(purposes);

            // BRS 2013-05-21
            // If the subject has entered a signup code, add it to any subject
            // pools with a matching code.
            if (s.getSignupCode() != null && s.getSignupCode().trim().length() > 0) {
                List<SubjectPool> curPools = s.getPools();
                List<SubjectPool> allPools = subjectPoolManager.getValidPools();
                for (SubjectPool pool : allPools) {
                    if (s.getSignupCode().equalsIgnoreCase(pool.getSignupCode())) {
                        curPools.add(pool);
                    }
                }
                s.setPools(curPools);
            }
            
            // **END BRS
            
            //Profile questions must be setup after user is saved, since they reference user
            
        } else if(isExpr) {
            //Parse experimenter related fields
            
            //Title
            ((Experimenter)user).setTitle(Title.valueOf(request.getParameter("title")));
            log.debug("The title is " + ((Experimenter)user).getTitle());
        }
        
        log.debug(user);
        
        try {
            //user = extendedUserManager.saveUser(user);
            
            //If user is a subject, parse profile survey answers
            if(user instanceof Subject) {
                List<Question> profQ = surveyManager.getProfileSurvey().getQuestions();
                int sz = profQ.size(), j;
                int [] ans = new int[sz];
                QuestionResult qr;
                
                for(j = 0; j < ans.length; j++) {
                    ans[j] = QuestionResultHelper.parseAnswer(request, profQ.get(j), j);
                }
                if(recentAnswers != null && recentAnswers.size() != sz)
                    throw new Exception("Size mismatch in profile");
                
                boolean change;
                //Don't save results that haven't changed
                for(j = 0; j < sz; j++) {
                    qr = (recentAnswers != null) ? recentAnswers.get(j) : null;
                    change = false;
                    if(qr != null) {
                        if(qr.getAnswerIdx() != ans[j]) {
                            qr.setAnswerIdx(ans[j]);
                            change = true;
                        }
                    } else {
                        change = true;
                        qr = new QuestionResult();
                        qr.setAnswerIdx(ans[j]);
                        qr.setQuestion(profQ.get(j));
                        qr.setSurveyParent(null);
                        qr.setSurveyTaker(user);
                        qr.setTakenDate(new Date());
                    }
                    
                    if(change)
                        questionResultManager.save(qr);
                }
            }
        } catch (AccessDeniedException ade) {
            // thrown by UserSecurityAdvice configured in aop:advisor userManagerSecurity
            log.warn(ade.getMessage());
            response.sendError(HttpServletResponse.SC_FORBIDDEN);
            return null;
        }
        //UserExistsException should be caught above
        /*catch (UserExistsException e) {
            errors.rejectValue("username", "errors.existing.user",
                    new Object[] {user.getUsername(), user.getEmail()}, "duplicate user");
         
            // redisplay the unencrypted passwords
            user.setPassword(user.getConfirmPassword());
            // reset the version # to what was passed in
            user.setVersion(originalVersion);
         
            return showForm(request, response, errors);
        }*/
        
        /** note: when i put the following line at the beginning of the method
         * hibernate throws exceptions when UserFormControllerTest.testSubmitDuplicateUsername
         * is run!  frustrating...
         */
        User creator = extendedUserManager.getUserByUsername(request.getRemoteUser());        
        if (!StringUtils.equals(request.getParameter("from"), "list") && !isAdd(request)) {
            //If we're not doing administration on user, then we output saved message
            saveMessage(request, getText("user.saved", user.getFullName(), locale));
        } else {
            if (StringUtils.isBlank(request.getParameter("version"))) {
                //If we just added a new user, set message to added and send an email
                
                saveMessage(request, getText("user.added", user.getFullName(), locale));
                
                // Send an account information e-mail
                message.setSubject(getText("signup.email.subject", locale));
                // BRS removed (doesn't use EmailManager, which causes problems;
                // not necessary anyway)
                //try {
                //    sendUserMessage(user, getText("newuser.email.message",
                //        new Object[]{creator.getFullName(), mypasswd}, locale),
                //        RequestUtil.getAppURL(request));
                //}
                //catch (MailSendException mse) {
                //    log.warn(mse.getMessage());
                //}
            } else {
                //If we aren't adding a new user, set message to updated
                
                saveMessage(request, getText("user.updated.byAdmin", user.getFullName(), locale));
            }
        }
        
        return new ModelAndView(getSuccessView());
        //        return showForm(request, response, errors);
    }
    
    protected boolean isAdd(HttpServletRequest request) {
        String method = request.getParameter("method");
        return (method != null && method.equalsIgnoreCase("add"));
    }
    
    public String getSubjectSuccessView() {
        return subjectSuccessView;
    }
    
    public void setSubjectSuccessView(String subjectSuccessView) {
        this.subjectSuccessView = subjectSuccessView;
    }
    
    public String getExperimenterSuccessView() {
        return experimenterSuccessView;
    }
    
    public void setExperimenterSuccessView(String experimenterSuccessView) {
        this.experimenterSuccessView = experimenterSuccessView;
    }
    
    public String getAdminSuccessView() {
        return adminSuccessView;
    }
    
    public void setAdminSuccessView(String adminSuccessView) {
        this.adminSuccessView = adminSuccessView;
    }
    
    private LabelValue roleToLVPair(Role r) {
        String s = r.getName();
        if(RecruitingConstants.SUBJ_ROLE.equals(r.getName())) {
            s = "Subject";
        } else if(RecruitingConstants.ADMIN_ROLE.equals(r.getName())) {
            s = "Administrator";
        } else if(RecruitingConstants.EXPTR_ROLE.equals(r.getName())) {
            s = "Experimenter";
        }
        return new LabelValue(s, r.getName());
    }
    
    public void setSubjectPoolManager(SubjectPoolManager subjectPoolManager) {
        this.subjectPoolManager = subjectPoolManager;
    }
    
    public void setSurveyManager(SurveyManager surveyManager) {
        this.surveyManager = surveyManager;
    }
    
    public void setQuestionResultManager(QuestionResultManager questionResultManager) {
        this.questionResultManager = questionResultManager;
    }
    
    public void setExtendedUserManager(ExtendedUserManager extendedUserManager) {
        this.extendedUserManager = extendedUserManager;
    }

    // BRS 2011-02-15
    public void setExperimentTypeManager(ExperimentTypeManager experimentTypeManager) {
        this.experimentTypeManager = experimentTypeManager;
    }
}
