/*
 * Copyright (C) 1998-2009, <a href="http://www.ssel.caltech.edu">SSEL</a>,
 * at Caltech, Pasadena, CA 91125. 
 *
 * MooreRecruiting is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 * USA.
 *
 */

package edu.caltech.ssel.recruiting.webapp.controller;

import edu.caltech.ssel.recruiting.common.RecruitingConstants;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.appfuse.service.UserManager;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

/**
 * Simple page to perform forwarding from a calendar link that has been clicked
 * It can forward to one of 3 views - subject, experimenter, or administrator success views
 * Forwarding is based on roles of the user
 * @author michaelk
 */
public class OnExpSelController implements Controller {

    private UserManager userManager;
    
    private String subjectSuccessView;
    private String experimenterSuccessView;
    private String administratorSuccessView;
    
    /** Creates a new instance of OnExpSelController */
    public OnExpSelController() {
    }

    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
        String expId = request.getParameter("expId");
        if(request.isUserInRole(RecruitingConstants.SUBJ_ROLE)) {
            return new ModelAndView(getSubjectSuccessView()).addObject("expId", expId);
        } else if (request.isUserInRole(RecruitingConstants.EXPTR_ROLE) ||
                request.isUserInRole(RecruitingConstants.ADMIN_ROLE)) {
            return new ModelAndView(getExperimenterSuccessView()).addObject("id", expId);
        } else {
            throw new Exception("role not recognized");
        }
            
    }

    public void setUserManager(UserManager userManager) {
        this.userManager = userManager;
    }

    public String getSubjectSuccessView() {
        return subjectSuccessView;
    }

    public void setSubjectSuccessView(String subjectSuccessView) {
        this.subjectSuccessView = subjectSuccessView;
    }

    public String getExperimenterSuccessView() {
        return experimenterSuccessView;
    }

    public void setExperimenterSuccessView(String experimenterSuccessView) {
        this.experimenterSuccessView = experimenterSuccessView;
    }

    public String getAdministratorSuccessView() {
        return administratorSuccessView;
    }

    public void setAdministratorSuccessView(String administratorSuccessView) {
        this.administratorSuccessView = administratorSuccessView;
    }
    
}
