/*
 * Copyright (C) 1998-2009, <a href="http://www.ssel.caltech.edu">SSEL</a>,
 * at Caltech, Pasadena, CA 91125. 
 *
 * MooreRecruiting is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 * USA.
 *
 */

package edu.caltech.ssel.recruiting.webapp.controller;

import edu.caltech.ssel.recruiting.common.DebugHelper;
import edu.caltech.ssel.recruiting.model.Experiment;
import edu.caltech.ssel.recruiting.model.Filter;
import edu.caltech.ssel.recruiting.model.Message;
import edu.caltech.ssel.recruiting.model.ParticipantRecord;
import edu.caltech.ssel.recruiting.model.Subject;
import edu.caltech.ssel.recruiting.model.UserMessage;
import edu.caltech.ssel.recruiting.service.ConditionManager;
import edu.caltech.ssel.recruiting.service.ExtendedUserManager;
import edu.caltech.ssel.recruiting.service.MessageManager;
import edu.caltech.ssel.recruiting.service.ParticipantRecordManager;
import edu.caltech.ssel.recruiting.service.UserMessageManager;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import org.appfuse.model.User;
import org.appfuse.service.GenericManager;
import org.appfuse.webapp.controller.BaseFormController;

import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;

import org.springframework.security.context.SecurityContext;
import org.springframework.security.context.SecurityContextHolder;

/**
* @author Michael Kolodrubetz, SSEL, Caltech
* @author <a href="mailto:wmyuan@hss.caltech.edu">Walter M. Yuan</a>, SSEL, Caltech
*/
public class MessageFormController extends BaseFormController {
    
    private final Log log = LogFactory.getLog(MessageFormController.class);
    private ConditionManager conditionManager;
    private ExtendedUserManager extendedUserManager;
    private GenericManager<Message, Long> messageManager;
    private ParticipantRecordManager participantRecordManager;
    private UserMessageManager userMessageManager;
    
    public MessageFormController() {
        setCommandClass(Message.class);
        setCommandName("message");
    }
    
    protected Object formBackingObject(HttpServletRequest request) throws Exception {
        log.debug("Entering formBackingObject...");
        
        Message message = null;
        String id = request.getParameter("id");
        if(StringUtils.isNotEmpty(id)) {
            try {
                message = messageManager.get(Long.parseLong(id));
            } catch(Exception any) {
                saveMessage(request, "Unable to find message with the given ID");
            }
        }
        if(message == null)
            message = new Message();
        
        return message;
    }
    
    protected Map referenceData(HttpServletRequest request) throws Exception {
        Map m = new HashMap();
        
        Locale locale = request.getLocale();
        //Currently subject/admin/experimenter is done by class type - should be done by roles
        m.put("allSubjects", extendedUserManager.getSubjects());
        m.put("allStandardUsers", extendedUserManager.getStandardUsers());
        m.put("allExperimenters", extendedUserManager.getExperimenters());
        m.put("allUsers", extendedUserManager.getUsers(null));
        
        Filter filter = null;
        Experiment experiment = (Experiment)request.getSession().getAttribute("experiment");
        if(experiment != null) {
            filter = experiment.getFilter();
            if(filter != null) {
                m.put("eligList", conditionManager.getEligibleSubjectsByFilter(filter));
            } else {
                m.put("eligList", m.get("allSubjects"));
            }
            
            // create some participant based lists for emails
            List<ParticipantRecord> participantRecordList = participantRecordManager.getByExperiment(experiment);
            
            // create Participants List
            List<Subject> participantList = new ArrayList();
            for(int i=0; participantRecordList !=null && i<participantRecordList.size(); i++)
                participantList.add(participantRecordList.get(i).getSubject());
            m.put("allParticipantsList", participantList);
            
            // create Eligible Participants List
            participantList = new ArrayList();
            for(int i=0; participantRecordList !=null && i<participantRecordList.size(); i++) {
                ParticipantRecord participantRecord = participantRecordList.get(i);
                if(participantRecord.isEligible())
                    participantList.add(participantRecordList.get(i).getSubject());
            }
            m.put("eligibleParticipantsList", participantList);
            
            // create Ineligible Participants List
            participantList = new ArrayList();
            for(int i=0; participantRecordList !=null && i<participantRecordList.size(); i++) {
                ParticipantRecord participantRecord = participantRecordList.get(i);
                if(!participantRecord.isEligible())
                    participantList.add(participantRecordList.get(i).getSubject());
            }
            m.put("ineligibleParticipantsList", participantList);
            saveMessage(request, getText("messageForm.sessionExperiment",
                    new Object[]{experiment.getFormattedDescription()}, locale));
        }
        
        return m;
    }
    
    public ModelAndView processFormSubmission(HttpServletRequest request,
            HttpServletResponse response,
            Object command,
            BindException errors)
            throws Exception {
        
        log.debug("in processFormSubmission");
        DebugHelper.displayParams(request, log);
        Message message = (Message)command;
        ModelAndView mav = null;
        
        if(request.getParameter("cancel") != null) {
            mav = new ModelAndView(getCancelView());
            return mav;
        }
        return super.processFormSubmission(request, response, command, errors);
    }
    
    public ModelAndView onSubmit(HttpServletRequest request,
            HttpServletResponse response, Object command,
            BindException errors)
            throws Exception {
        
        log.debug("Entering onSubmit...");
        
        Message message = (Message)command;
        Locale locale = request.getLocale();
        
        SecurityContext context = SecurityContextHolder.getContext();
        User user = (User)context.getAuthentication().getPrincipal();
        message.setCreator(user);

        try {
            if(StringUtils.isNotEmpty(request.getParameter("id")))
                userMessageManager.deleteMessageRecipients(message); // recreate later
        } catch(Exception any) {
            log.warn("** yikes **", any);
            saveMessage(request, getText("messageForm.internalerror", any.getMessage(), locale));
        }
        
        String option = request.getParameter("option");
        
        String save = request.getParameter("save");
        String send = request.getParameter("send");
        
        if(StringUtils.isNotEmpty(save)) {
            try {
                message.setStatus(Message.Status.DRAFT);
                message.setWhenCreated(new Date());
                message = messageManager.save(message);
                saveMessage(request, getText("messageForm.saved", locale));
            } catch(Exception any) {
                log.warn("** yikes **", any);
                saveMessage(request, getText("messageForm.saveerror", any.getMessage(), locale));
            }
        } else if(StringUtils.isNotEmpty(send)) {
            try {
                message.setStatus(Message.Status.SENT);
                message.setWhenCreated(new Date());
                message = messageManager.save(message);
                saveMessage(request, getText("messageForm.sent", locale));
            } catch(Exception any) {
                log.warn("** yikes **", any);
                saveMessage(request, getText("messageForm.senderror", any.getMessage(), locale));
            }
        }
        
        //Parse the to, cc, and bcc lists
        //We ned to be able to differentiate betw. null to list (original) and blank list (second time through)
        String [] arr;
        arr = request.getParameterValues("emailList");
        if(arr != null) {
            if(StringUtils.isNotEmpty(request.getParameter("setNotifSize")))
                arr = randIfNecc(arr, Integer.parseInt(request.getParameter("setNotifSize")));
            for(int i=0; i<arr.length; i++) {
                UserMessage um = new UserMessage();
                um.setMessage(message);
                um.setRecipient(extendedUserManager.getUser(arr[i]));
                um.setStatus(UserMessage.Status.NEW);
                try {
                    userMessageManager.save(um);
                } catch(Exception any) {
                    log.warn("** yikes **", any);
                    saveMessage(request, getText("messageForm.umsaveerror",
                            new Object[]{any.getMessage(),
                            um.getRecipient().getFirstName() + " " +
                                    um.getRecipient().getLastName()}, locale));
                }
            }
        } else
            log.debug("emailList empty");
            
        // TODO model and view when an experiment was present
        Experiment experiment = (Experiment)request.getSession().getAttribute("experiment");
        
        return new ModelAndView(getSuccessView());
    }
    
    private String [] randIfNecc(String[] arr, int notifSize) {
        try {
            int num = notifSize;
            if(num >= arr.length)
                return arr;
            int sz = arr.length;
            boolean[] used = new boolean[sz];
            int j, k, l, r;
            for(j = 0; j < sz; j++)
                used[j] = false;
            Random gen = new Random();
            for(j = 0; j < num; j++) {
                r = gen.nextInt(sz-j);
                l=0;
                for(k = 0; k < sz; k++) {
                    if(!used[k]) {
                        if(l == r) {
                            used[k] = true;
                            break;
                        }
                        l++;
                    }
                }
                //System.out.println("For iteration " + j + ", r = " + r + " and k = " + k);
            }

            String[] arr2 = new String[num];
            for(j = 0, k=0; j < sz; j++) {
                if(used[j]) {
                    arr2[k] = arr[j];
                    k++;
                }
            }
            return arr2;
        } catch(Exception e) {
            //If there are any exceptions, throw a log statement but return the full array
            log.debug("Error thrown while setting notification size :" +e.getMessage());
        }
        return arr;
    }
    
    public void setExtendedUserManager(ExtendedUserManager extendedUserManager) {
        this.extendedUserManager = extendedUserManager;
    }

    public void setMessageManager(MessageManager messageManager) {
        this.messageManager = messageManager;
    }

    public void setUserMessageManager(UserMessageManager userMessageManager) {
        this.userMessageManager = userMessageManager;
    }

    public void setConditionManager(ConditionManager conditionManager) {
        this.conditionManager = conditionManager;
    }

    public void setParticipantRecordManager(ParticipantRecordManager participantRecordManager) {
        this.participantRecordManager = participantRecordManager;
    }
    
}
