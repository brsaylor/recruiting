/*
 * Copyright (C) 1998-2009, <a href="http://www.ssel.caltech.edu">SSEL</a>,
 * at Caltech, Pasadena, CA 91125. 
 *
 * MooreRecruiting is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 * USA.
 *
 */

package edu.caltech.ssel.recruiting.webapp.controller;

import edu.caltech.ssel.recruiting.common.QuestionAndAnswerList;
import edu.caltech.ssel.recruiting.common.QuestionResultHelper;
import edu.caltech.ssel.recruiting.model.BooleanCondition;
import edu.caltech.ssel.recruiting.model.Condition;
import edu.caltech.ssel.recruiting.model.Experiment;
import edu.caltech.ssel.recruiting.model.Filter;
import edu.caltech.ssel.recruiting.model.ParticipantRecord;
import edu.caltech.ssel.recruiting.model.ParticipantRecord.Status;
import edu.caltech.ssel.recruiting.model.Question;
import edu.caltech.ssel.recruiting.model.QuestionResult;
import edu.caltech.ssel.recruiting.model.QuestionResultCondition;
import edu.caltech.ssel.recruiting.model.Subject;
import edu.caltech.ssel.recruiting.model.Survey;
import edu.caltech.ssel.recruiting.service.ConditionManager;
import edu.caltech.ssel.recruiting.service.EmailManager;
import edu.caltech.ssel.recruiting.service.ExperimentManager;
import edu.caltech.ssel.recruiting.service.ParticipantRecordManager;
import edu.caltech.ssel.recruiting.service.PaymentManager;
import edu.caltech.ssel.recruiting.service.QuestionResultManager;
import edu.caltech.ssel.recruiting.service.SurveyManager;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.appfuse.Constants;
import org.appfuse.model.User;
import org.appfuse.service.GenericManager;
import org.appfuse.service.UserManager;
import org.appfuse.webapp.controller.BaseFormController;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.validation.Errors;

// BRS
import edu.caltech.ssel.recruiting.service.ReminderManager;

/**
* @author Michael Kolodrubetz, SSEL, Caltech
* @author <a href="mailto:wmyuan@hss.caltech.edu">Walter M. Yuan</a>, SSEL, Caltech
*/
public class SurveyFormController extends BaseFormController {
    private ExperimentManager experimentManager;
    private GenericManager<Question, Long> questionManager;
    private SurveyManager surveyManager;
    private UserManager userManager;
    private QuestionResultManager questionResultManager;
    private ConditionManager conditionManager;
    private ParticipantRecordManager participantRecordManager;
    private PaymentManager paymentManager;
    private EmailManager emailManager;
    private String ineligibleView;
    private String calendarView;
    private Log log = LogFactory.getLog(SurveyFormController.class);

    // BRS
    private ReminderManager reminderManager;
    
    /** Creates a new instance of SurveyFormController */
    public SurveyFormController() {
        setCommandClass(QuestionAndAnswerList.class);
        setCommandName("questionAndAnswerList");
    }
    
    protected ModelAndView handleRequestInternal(HttpServletRequest request, HttpServletResponse response) throws Exception {
        ModelAndView mv = super.handleRequestInternal(request, response);
        if(!isFormSubmission(request)) {
            log.debug("entering handleRequestInternal");
            //Check whether they have already signed up for the experiment
            Subject s = (Subject)userManager.getUserByUsername(request.getRemoteUser());
            Experiment e = experimentManager.get(Long.parseLong(request.getParameter("expId")));
            Locale locale = request.getLocale();

            if(!e.getStatus().equals(Experiment.Status.OPEN)) {
                saveMessage(request, getText("survey.full", locale));
                return new ModelAndView(calendarView);
            }
            
            if(participantRecordManager.isSignedUp(s, e)) {
                saveMessage(request, getText("survey.alreadySignedUp", locale));
                return new ModelAndView(calendarView);
            }

            // BRS
            // Check that this experiment doesn't overlap with another experiment the subject is already signed up for
            List<ParticipantRecord> precs = participantRecordManager.getBySubject(s);
            Experiment e2 = null;
            for (int i = 0; i < precs.size(); i++) {
                e2 = precs.get(i).getExp();
                if (experimentManager.experimentsOverlapWithoutBuffer(e, e2)) {
                    saveMessage(request, getText("survey.signupConflict", locale));
                    return new ModelAndView(calendarView);
                }
            }
            
            
            //Check whether there are any questions to display
            QuestionAndAnswerList qal = (QuestionAndAnswerList)mv.getModel().get(this.getCommandName());//Get the model
            boolean[] inProf = qal.getInProfile();
            //Make sure that they're not all profile questions
            for(int j = 0; j < inProf.length; j++)
                if(!inProf[j])
                    return mv;
            //Otherwise, just act as if there were a submit
            return this.onSubmit(request, response, qal, null);
        }
        return mv;
    }

    // BRS 2012-03-20
    protected Map referenceData(HttpServletRequest request, Object command, Errors errors) throws Exception {

        Experiment experiment = null;
        String s = request.getParameter("expId");
        String errmsg = getText("experiment.invalidId", request.getLocale());
        if(StringUtils.isNotEmpty(s)) {
            log.debug("looking up experiment for id " + s);
            try {
                experiment = experimentManager.get(Long.parseLong(s));
            } catch(Exception any) {
                log.info(any.getMessage());
            }
            if(experiment == null) {
                saveMessage(request, errmsg);
                throw new Exception(errmsg);
            }
        } else {
            saveMessage(request, errmsg);
            throw new Exception(errmsg);
        }

        // now we have an experiment
        Survey survey = experiment.getSurvey();

        Map m = new HashMap();
        if (survey != null) {
            m.put("introText", survey.getIntroText());
        }
        return m;
    }
    
    protected Object formBackingObject(HttpServletRequest request) throws Exception {
        
        Experiment experiment = null;
        String s = request.getParameter("expId");
        String errmsg = getText("experiment.invalidId", request.getLocale());
        if(StringUtils.isNotEmpty(s)) {
            log.debug("looking up experiment for id " + s);
            try {
                experiment = experimentManager.get(Long.parseLong(s));
            } catch(Exception any) {
                log.info(any.getMessage());
            }
            if(experiment == null) {
                saveMessage(request, errmsg);
                throw new Exception(errmsg);
            }
        } else {
            saveMessage(request, errmsg);
            throw new Exception(errmsg);
        }
        
        // now we have an experiment
        Survey survey = experiment.getSurvey();
        QuestionAndAnswerList qal = new QuestionAndAnswerList();
        List<Question> lst = new ArrayList();
        int sz = 0;
        if(survey != null && survey.getQuestions() != null)
            sz =survey.getQuestions().size();
        
        //log.debug("sz == " + sz);
        int [] ans = new int[sz];
        boolean[] isProf = new boolean[sz];
        User u = userManager.getUserByUsername(request.getRemoteUser());
        List<Question> profQ = surveyManager.getProfileSurvey().getQuestions();
        //Perform a deep copy of the Question list
        for(int j = 0; j < sz; j++) {
            Question q = survey.getQuestions().get(j).clone();
            lst.add(q);
            QuestionResult qr = questionResultManager.getMostRecentAnswer(u, q);
            if(qr == null)
                ans[j] = 0;
            else
                ans[j] = qr.getAnswerIdx();
            isProf[j] = profQ.contains(q);
        }
        
        // now we have questions and answers
        // we know whether those are in the profile
        
        // more frightening logic
        // we need to add missing filter questions
        Filter filter = experiment.getFilter();
        List<Integer> extAnsList = null;
        List<Boolean> extIsProfList = null;
        if(filter != null) {
            List<Condition> condList = filter.getConditions();
            
            extAnsList = new ArrayList();
            for(int i=0; i<ans.length; i++)
                extAnsList.add(ans[i]);
            extIsProfList = new ArrayList();
            for(int i=0; i<isProf.length; i++)
                extIsProfList.add(isProf[i]);
            
            for(int i=0; condList!=null && i<condList.size(); i++) {
                Condition condition = condList.get(i);
                if(condition instanceof QuestionResultCondition) {
                    log.debug("checking Filter question");
                    QuestionResultCondition qrc = (QuestionResultCondition) condition;
                    Question q = qrc.getQuestion().clone();
                    // add QuestionResultCondition questions not identical to a survey question
                    if(survey != null && !survey.getQuestions().contains(q)) {
                        lst.add(q);
                        QuestionResult qr = questionResultManager.getMostRecentAnswer(u, q);
                        if(qr == null)
                            extAnsList.add(new Integer(0));
                        else
                            extAnsList.add(new Integer(qr.getAnswerIdx()));
                            extIsProfList.add(new Boolean(profQ.contains(q)));
                    } else
                        log.debug("\tsurvey question matches question...");
                } else if(condition instanceof BooleanCondition) {
                    List<Question> qlist = inspectBooleanCondition(
                            u, profQ, (BooleanCondition)condition);
                    // add QuestionResultCondition questions not identical to a survey question
                    for(int j = 0; j<qlist.size(); j++) {
                        Question q = qlist.get(j);
                        if(survey != null && !survey.getQuestions().contains(q)) {
                            lst.add(q);
                            QuestionResult qr = questionResultManager.getMostRecentAnswer(u, q);
                            if(qr == null)
                                extAnsList.add(new Integer(0));
                            else
                                extAnsList.add(new Integer(qr.getAnswerIdx()));
                                extIsProfList.add(new Boolean(profQ.contains(q)));
                        } else
                            log.debug("\tsurvey question matches question...");
                    }
                }
            }
            int[] extAns = new int[extAnsList.size()];
            for(int i=0; i<extAns.length; i++)
                extAns[i] = extAnsList.get(i).intValue();
            qal.setAnswers(extAns);
            boolean[] extIsProf = new boolean[extIsProfList.size()];
            for(int i=0; i<extIsProf.length; i++)
                extIsProf[i] = extIsProfList.get(i).booleanValue();
            qal.setInProfile(extIsProf);
        } else { // no filter, easy
            qal.setAnswers(ans);
            qal.setInProfile(isProf);
        }
        // finally
        qal.setExpId(experiment.getId());
        qal.setQuestions(lst);
        
        return qal;
    }
    
    public ModelAndView onSubmit(HttpServletRequest request,
            HttpServletResponse response, Object command,
            BindException errors)
            throws Exception {
        log.debug("Entering onSubmit...");
        String success = getSuccessView();
        QuestionAndAnswerList qal = (QuestionAndAnswerList)command;
        Locale locale = request.getLocale();
        //DebugHelper.displayParams(request, log);
        
        //Parse the data
        Long expId = Long.parseLong(request.getParameter("expId"));
        Subject u = (Subject)userManager.getUserByUsername(request.getRemoteUser());
        Experiment e = experimentManager.get(expId);
        Date d = new Date();
        List<Question> lst = qal.getQuestions();
        boolean[] inProf = qal.getInProfile();
        boolean eligible = true;
        
        for(int j = 0; j < lst.size(); j++) {
            Question q = lst.get(j);
            if(!inProf[j]) {  // saving answers to questions NOT in the profile...
                QuestionResult qr = new QuestionResult();
                qr.setQuestion(q);
                qr.setSurveyParent(e);
                qr.setSurveyTaker(u);
                qr.setTakenDate(d);
                
                //Parse the answer
                qr.setAnswerIdx(QuestionResultHelper.parseAnswer(request, q, j));
                log.debug("The answer to question number " + (j+1) + " was " + qr.getAnswerIdx());
                
                questionResultManager.save(qr);
            }
            //Now we filter based on the question result (qr)
            //If it fails filter, then eligible = false;
        }
        ParticipantRecord prec = null;
        ModelAndView mav = null;
        if(conditionManager.isEligible(e.getFilter(), u)) {
            mav = new ModelAndView(success);
            // Add the subject to the experiment
            prec = new ParticipantRecord();
            prec.setEligible(true);
            prec.setExp(e);
            prec.setParticipated(false);

            // BRS 2011-06-07
            // The subject signed up for the experiment, which means (s)he must
            // have clicked the "I consent" radio button.
            prec.setConsent(true);

            //null indicates that number fields haven't been set
            //prec.setPayoff(null);
            //prec.setPlayerNum(null);
            
            //I have no idea what status to set
            //prec.setStatus(Status.STATUS_1);
            prec.setStatus(Status.STATUS_3); // BRS: should be 3 for "Sign-up"
            prec.setSubject(u);
            participantRecordManager.save(prec);

            // BRS: Schedule reminder
            log.debug("Subject signed up; scheduling reminders");
            reminderManager.createExperimentReminders(u, e);

            if(participantRecordManager.getByExperiment(e).size() >= e.getMaxNumSubjects()) {
                e.setStatus(Experiment.Status.FULL);
                e = experimentManager.save(e);
            }
            SimpleDateFormat sdf = new SimpleDateFormat("MM-dd-yyyy");
            mav.addObject("expId", expId);
            mav.addObject("expDate", sdf.format(e.getExpDate()));
            saveMessage(request, getText("survey.eligible", locale));

            // BRS - don't email unsubscribed users
            if (!u.getSubscribed()) {
                log.info("Not emailing unsubscribed subject " + u.getEmail());
                return mav;
            }

            Map model = new HashMap();
            model.put(Constants.EMAIL_TO, u.getEmail());
            model.put(Constants.EMAIL_REPLY_TO, e.getReservedBy().getEmail());
            Object [] params = new Object[1];
            params[0] = e.getId();
            model.put(Constants.EMAIL_SUBJECT, getText("experiment.signUpConfirmation", params, locale));
            model.put(Constants.USER, (User)u);
            model.put(Constants.EXPERIMENT, e);

            model.put("labName", getText("company.name", locale));
            model.put("labUrlName", getText("company.url", locale));
            model.put("labPhone", getText("company.phone", locale));
            
            emailManager.sendEmail(this.templateName, model);
        } else {
            mav = new ModelAndView(ineligibleView);
            saveMessage(request, getText("survey.ineligible", locale));
        }
        
        return mav;
    }
    
    /**
     * Down the rabbit hole
     * A BooleanCondition may contain inner QuestionResultCondition or even
     * other BooleanConditions that may, well, you know...
     */
    private List<Question> inspectBooleanCondition(
            User user,
            List<Question> profQ,
            BooleanCondition condition)
    throws Exception {
        
        log.debug("inspecting BooleanCondition");
        List<Question> questionList = new ArrayList();
        
        Condition innerCond = conditionManager.get(condition.getFirstCondition());
        if(innerCond instanceof QuestionResultCondition) {
            QuestionResultCondition qrc = (QuestionResultCondition) innerCond;
            Question q = qrc.getQuestion().clone();
            questionList.add(q);
        } else if(innerCond instanceof BooleanCondition)
            questionList.addAll(
                    inspectBooleanCondition(user, profQ, (BooleanCondition)innerCond));
        
        innerCond = conditionManager.get(condition.getSecondCondition());
        if(innerCond instanceof QuestionResultCondition) {
            QuestionResultCondition qrc = (QuestionResultCondition) innerCond;
            Question q = qrc.getQuestion().clone();
            questionList.add(q);
        } else if(innerCond instanceof BooleanCondition)
            questionList.addAll(
                    inspectBooleanCondition(user, profQ, (BooleanCondition)innerCond));
        
        return questionList;
    }
    
    public void setUserManager(UserManager userManager) {
        this.userManager = userManager;
    }
    
    public void setQuestionResultManager(QuestionResultManager questionResultManager) {
        this.questionResultManager = questionResultManager;
    }
    
    public void setQuestionManager(GenericManager<Question, Long> questionManager) {
        this.questionManager = questionManager;
    }
    
    public void setSurveyManager(SurveyManager surveyManager) {
        this.surveyManager = surveyManager;
    }
    
    public void setConditionManager(ConditionManager conditionManager) {
        this.conditionManager = conditionManager;
    }
    
    public void setParticipantRecordManager(ParticipantRecordManager participantRecordManager) {
        this.participantRecordManager = participantRecordManager;
    }

    public void setExperimentManager(ExperimentManager experimentManager) {
        this.experimentManager = experimentManager;
    }

    public void setPaymentManager(PaymentManager paymentManager) {
        this.paymentManager = paymentManager;
    }

    public void setIneligibleView(String ineligibleView) {
        this.ineligibleView = ineligibleView;
    }

    public void setCalendarView(String calendarView) {
        this.calendarView = calendarView;
    }

    /**
     * @param emailManager the emailManager to set
     */
    public void setEmailManager(EmailManager emailManager) {
        this.emailManager = emailManager;
    }
    
    // BRS
    public void setReminderManager(ReminderManager reminderManager) {
        this.reminderManager = reminderManager;
    }
}
