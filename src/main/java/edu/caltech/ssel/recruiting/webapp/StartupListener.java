/*
 * Copyright (C) 1998-2009, <a href="http://www.ssel.caltech.edu">SSEL</a>,
 * at Caltech, Pasadena, CA 91125. 
 *
 * MooreRecruiting is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 * USA.
 *
 */

package edu.caltech.ssel.recruiting.webapp;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextListener;
import javax.servlet.ServletContextEvent;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.context.ApplicationContext;

/**
 * Web application lifecycle listener.
 */
public class StartupListener implements ServletContextListener {
    
    private static final Log log = LogFactory.getLog(StartupListener.class);
    private static ApplicationContext ctx;

    /**
     * ### Method from ServletContextListener ###
     * 
     * Called when a Web application is first ready to process requests
     * (i.e. on Web server startup and when a context is added or reloaded).
     */
    public void contextInitialized(ServletContextEvent event) {
        
        if (log.isDebugEnabled()) {
            log.debug("-- Recruiting System StartupListener initialized --");
        }
        ServletContext context = event.getServletContext();
        ctx = WebApplicationContextUtils.getRequiredWebApplicationContext(context);

        // *** Execute startup tasks ***
    }

    /**
     * ### Method from ServletContextListener ###
     * 
     * Called when a Web application is about to be shut down
     * (i.e. on Web server shutdown or when a context is removed or reloaded).
     * Request handling will be stopped before this method is called.
     */
    public void contextDestroyed(ServletContextEvent evt) {
    }
    
    /**
     * Returns the Spring applicationContext.  The context is used
     * by non-managed beans that need access to managed beans
     */
    public static ApplicationContext getApplicationContext() {
        return ctx;
    }
}
