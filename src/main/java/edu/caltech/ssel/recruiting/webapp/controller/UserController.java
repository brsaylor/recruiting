/*
 * Copyright (C) 1998-2009, <a href="http://www.ssel.caltech.edu">SSEL</a>,
 * at Caltech, Pasadena, CA 91125. 
 *
 * MooreRecruiting is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 * USA.
 *
 */

package edu.caltech.ssel.recruiting.webapp.controller;

import java.util.Arrays;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.HashMap;
import java.util.TreeSet; // BRS  - implements SortedSet

import edu.caltech.ssel.recruiting.service.ExtendedUserManager;
import edu.caltech.ssel.recruiting.common.ExtendedPaginatedList;
import edu.caltech.ssel.recruiting.common.PaginateListFactory;
import edu.caltech.ssel.recruiting.service.PagingManager;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import org.appfuse.Constants;
import org.appfuse.model.User;

import org.displaytag.properties.SortOrderEnum;
import org.displaytag.tags.TableTagParameters;
import org.displaytag.util.ParamEncoder;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;

// BRS 2012-02-13
//import org.springframework.security.core.session.SessionRegistry;
//import org.springframework.security.concurrent.SessionRegistry;

import org.compass.core.CompassHit;
import org.compass.core.support.search.CompassSearchCommand;
import org.compass.core.support.search.CompassSearchHelper;
import org.compass.core.support.search.CompassSearchResults;
import org.compass.spring.web.mvc.AbstractCompassCommandController;
import org.compass.core.engine.SearchEngineQueryParseException; // BRS 2012-04-18


/**
 * Simple class to retrieve a list of users from the database.
 *
 * <p>
 * <a href="UserController.java.html"><i>View Source</i></a>
 * </p>
 *
 * @author <a href="mailto:matt@raibledesigns.com">Matt Raible</a>
 *
 * @author Michael Kolodrubetz, SSEL, Caltech
 * @author <a href="mailto:wmyuan@hss.caltech.edu">Walter M. Yuan</a>, SSEL, Caltech
 * @author Richard Ho, CASSEL, UCLA
*/
public class UserController extends AbstractCompassCommandController {
    private transient final Log log = LogFactory.getLog(UserController.class);
    private ExtendedUserManager mgr = null;
    private PagingManager pmgr = null;
    private PaginateListFactory paginateListFactory;
    private String searchView;
    private String searchResultsView;
    private String searchResultsName = "searchResults";
    private Integer pageSize;
    private CompassSearchHelper searchHelper;
    public static String [] queryList = new String []{ "Username",
                                                          "Email",
                                                          "Name"};

    public void setUserManager(ExtendedUserManager userManager) {
        this.mgr = userManager;
        setCommandClass(CompassSearchCommand.class);
    }
    
    public void setPagingManager(PagingManager pagingManager) {
        this.pmgr = pagingManager;
    }
    
    public void setPaginateListFactory(PaginateListFactory paginateListFactory) {
        this.paginateListFactory = paginateListFactory;
    }
    
    public String getSearchView() {
        return searchView;
    }

    public void setSearchView(String searchView) {
        this.searchView = searchView;
    }

    public String getSearchResultsName() {
        return searchResultsName;
    }

    public void setSearchResultsName(String searchResultsName) {
        this.searchResultsName = searchResultsName;
    }

    public String getSearchResultsView() {
        return searchResultsView;
    }

    public void setSearchResultsView(String resultsView) {
        this.searchResultsView = resultsView;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public void setSearchHelper(CompassSearchHelper searchHelper) {
        this.searchHelper = searchHelper;
    }
    
    @Override
    public void afterPropertiesSet() throws Exception {
        super.afterPropertiesSet();
        if (searchView == null) {
            throw new IllegalArgumentException(
                    "Must set the searchView property");
        }
        if (searchResultsView == null) {
            throw new IllegalArgumentException(
                    "Must set the serachResultsView property");
        }
        if (searchHelper == null) {
            searchHelper = new CompassSearchHelper(getCompass(), getPageSize());
        }
    }

    public ModelAndView handle(HttpServletRequest request,
                                      HttpServletResponse response, Object command, BindException errors)
    throws Exception {
        if (log.isDebugEnabled()) 
            log.debug("entering 'handleRequest' method...");
        
        final CompassSearchCommand searchCommand = (CompassSearchCommand) command;
        
        //Default search values
        String queryField = "Name";
        String searchQuery = "";
        
        if(request != null) {
            request.setAttribute("queryList",queryList);
            searchQuery = request.getParameter("searchQuery");
        }
        if(searchQuery != null)
            queryField = request.getParameter("querySelected");
        request.setAttribute("queryField",queryField);
        request.setAttribute("searchQuery", searchQuery);


        String option=request.getParameter("option");
        ExtendedPaginatedList paginatedList = paginateListFactory.getPaginatedListFromRequest(request);
        log.debug("getPageSize() = " + paginatedList.getPageSize());
        
        //Formatting the searchQuery
        paginatedList.setSearch(false);
        if(searchQuery != null) {
            searchQuery = searchQuery.replaceAll("\\b\\s\\b", ", ");

            // BRS 2012-04-18: Why? Probably to prevent exceptions due to
            // incorrect syntax. Let's handle them with a try/catch instead.
            //searchQuery = searchQuery.replaceAll("^\\(*\\)*\\}*\\{*\\!*\\^*\\**\\[*\\]*\\\\*", "");
        }
        if(StringUtils.hasText(searchQuery)) {
            searchQuery=queryField+":"+searchQuery;
            searchQuery = "+alias:User +"+searchQuery;
            searchQuery = searchQuery.trim();
            paginatedList.setSearch(true);
        }
        //Search values that were input
        searchCommand.setQuery(searchQuery);

        // BRS 2012-04-18: handle search errors by returning empty results
        CompassSearchResults searchResults;
        try {
            searchResults = searchHelper.search(searchCommand);
        } catch (SearchEngineQueryParseException e) {
            searchResults = new CompassSearchResults(new CompassHit[0], 0, 0);
        }

        List<CompassHit> clst = Arrays.asList(searchResults.getHits());
        Set<Long> hitIds = new HashSet();
        for(int i = 0; i < clst.size(); i++) {
            hitIds.add((Long)((User)clst.get(i).getData()).getId());
        }
        //Default table values
        int index = 0;
        String column = "username";
        SortOrderEnum order = SortOrderEnum.ASCENDING;
        //Values derived from the table
        String stringIndex = request.getParameter(new ParamEncoder("users").encodeParameterName(TableTagParameters.PARAMETER_PAGE));
        String stringCol = request.getParameter(new ParamEncoder("users").encodeParameterName(TableTagParameters.PARAMETER_SORT));
        String stringOrder = request.getParameter(new ParamEncoder("users").encodeParameterName(TableTagParameters.PARAMETER_ORDER));
        //Change values accordingly
        if(stringIndex != null) { index = (Integer.parseInt(stringIndex)) -1; }
        if(stringOrder != null && stringOrder.equals("2")) { order = SortOrderEnum.DESCENDING; }
        if(stringCol != null) { column = stringCol; }
        //Set values to paginatedList
        paginatedList.setIndex(index);
        paginatedList.setSortCriterion(column);
        paginatedList.setSortDirection(order);
        paginatedList.setSearchIds(hitIds);

        /*
        if(hitIds.size() != 0  ||  !paginatedList.getSearch()) {
            if(option == null || "users".equals(option))
                paginatedList = pmgr.getAllUsers(paginatedList);
            else if("subjects".equals(option))
                paginatedList = pmgr.getSubjects(paginatedList);
            else if("experimenters".equals(option))
                paginatedList = pmgr.getExperimenters(paginatedList);
            else if("admins".equals(option))
                paginatedList = pmgr.getAdmins(paginatedList);
            else
                return new ModelAndView();

            return new ModelAndView("userList", Constants.USER_LIST, paginatedList.getList()).addObject("resultSize", paginatedList.getFullListSize());
        }
        return new ModelAndView("userList", Constants.USER_LIST, new ArrayList()).addObject("resultSize", new Integer(0));
        * previous end of method - BRS 2012-02-13
        */

        // BRS 2012-02-13: adapt code to show who's logged in
        ModelAndView mav;
        if(hitIds.size() != 0  ||  !paginatedList.getSearch()) {
            if(option == null || "users".equals(option))
                paginatedList = pmgr.getAllUsers(paginatedList);
            else if("subjects".equals(option))
                paginatedList = pmgr.getSubjects(paginatedList);
            else if("experimenters".equals(option))
                paginatedList = pmgr.getExperimenters(paginatedList);
            else if("admins".equals(option))
                paginatedList = pmgr.getAdmins(paginatedList);
            else
                return new ModelAndView();

            mav = new ModelAndView("userList", Constants.USER_LIST, paginatedList.getList()).addObject("resultSize", paginatedList.getFullListSize());
        } else {
            mav = new ModelAndView("userList", Constants.USER_LIST, new ArrayList()).addObject("resultSize", new Integer(0));
        }

        // BRS 2012-02-13
        // Show who's logged in. Comes from UserCounterListener.
        TreeSet<String> userEmails = new TreeSet();
        for (User u : ((HashMap<Long,User>) request.getSession().getServletContext().getAttribute("users")).values()) {
            userEmails.add(u.getEmail());
        }
        mav.addObject("userEmails", userEmails);
        mav.addObject("userCount", userEmails.size());

        return mav;
    }
}
