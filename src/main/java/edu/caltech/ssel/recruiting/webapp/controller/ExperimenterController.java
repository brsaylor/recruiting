/*
 * Copyright (C) 1998-2009, <a href="http://www.ssel.caltech.edu">SSEL</a>,
 * at Caltech, Pasadena, CA 91125. 
 *
 * MooreRecruiting is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 * USA.
 *
 */

package edu.caltech.ssel.recruiting.webapp.controller;

import edu.caltech.ssel.recruiting.common.RecruitingConstants;
import edu.caltech.ssel.recruiting.model.Experimenter;
import edu.caltech.ssel.recruiting.service.ExperimentManager;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import org.appfuse.service.UserManager;
import org.appfuse.model.User;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

/**
* @author Michael Kolodrubetz, SSEL, Caltech
* @author <a href="mailto:wmyuan@hss.caltech.edu">Walter M. Yuan</a>, SSEL, Caltech
*/
public class ExperimenterController implements Controller {
    
    private static final Log log = LogFactory.getLog(ExperimenterController.class);
    private ExperimentManager experimentManager;
    private UserManager userManager;

    
    /** Creates a new instance of ExperimenterController */
    public ExperimenterController() {
    }

    public ModelAndView handleRequest(HttpServletRequest request,
            HttpServletResponse response) throws Exception {
        
        ModelAndView mav = new ModelAndView();
        User user = userManager.getUserByUsername(request.getRemoteUser());
        if(user != null &&
                (request.isUserInRole(RecruitingConstants.EXPTR_ROLE) ||
                request.isUserInRole(RecruitingConstants.ADMIN_ROLE))) {
            if(user instanceof Experimenter)
                mav.addObject("expList", experimentManager.getByRunby(user.getId()));
        }
        return mav;
    }

    public void setExperimentManager(ExperimentManager experimentManager) {
        this.experimentManager = experimentManager;
    }

    public void setUserManager(UserManager userManager) {
        this.userManager = userManager;
    }
}
