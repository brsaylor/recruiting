/*
 * Copyright (C) 1998-2009, <a href="http://www.ssel.caltech.edu">SSEL</a>,
 * at Caltech, Pasadena, CA 91125. 
 *
 * MooreRecruiting is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 * USA.
 *
 */

package edu.caltech.ssel.recruiting.webapp.controller;

import edu.caltech.ssel.recruiting.common.ExtendedPaginatedList;
import edu.caltech.ssel.recruiting.common.PaginateListFactory;
import edu.caltech.ssel.recruiting.model.Experiment;
import edu.caltech.ssel.recruiting.model.Experimenter;
import edu.caltech.ssel.recruiting.model.ExperimentType;
import edu.caltech.ssel.recruiting.model.Location;
import edu.caltech.ssel.recruiting.model.ParticipantRecord;
import edu.caltech.ssel.recruiting.model.Subject;
import edu.caltech.ssel.recruiting.service.ExperimentManager;
import edu.caltech.ssel.recruiting.service.ExperimentTypeManager;
import edu.caltech.ssel.recruiting.service.LocationManager;
import edu.caltech.ssel.recruiting.service.ExtendedUserManager;
import edu.caltech.ssel.recruiting.service.ParticipantRecordManager;
import edu.caltech.ssel.recruiting.service.PagingManager;
import java.text.SimpleDateFormat;

import java.util.Arrays;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import java.io.File;
import java.io.FileInputStream;
import java.io.OutputStream;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.ServletContext;

import org.appfuse.model.User;

import org.displaytag.properties.SortOrderEnum;
import org.displaytag.tags.TableTagParameters;
import org.displaytag.util.ParamEncoder;

import org.springframework.util.StringUtils;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import org.compass.core.CompassHit;
import org.compass.core.support.search.CompassSearchCommand;
import org.compass.core.support.search.CompassSearchHelper;
import org.compass.core.support.search.CompassSearchResults;
import org.compass.spring.web.mvc.AbstractCompassCommandController;

/**
* @author Michael Kolodrubetz, SSEL, Caltech
* @author Richard Ho, CASSEL, UCLA
* @author <a href="mailto:wmyuan@hss.caltech.edu">Walter M. Yuan</a>, SSEL, Caltech
*/
public class SystemStatsController extends AbstractCompassCommandController {

    private static final Log log = LogFactory.getLog(SystemStatsController.class);
    
    private ExperimentManager experimentManager;
    private ExperimentTypeManager experimentTypeManager;
    private LocationManager locationManager;
    private ExtendedUserManager extendedUserManager;
    private ParticipantRecordManager participantRecordManager;

    private String homeView;
    private String yearSelected;
    private String type;
    private static final SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd' 'HH:mm:ss");
    private int experimentCountOPEN;
    private int experimentCountFINISHED;
    private int experimentCountCLOSED;
    private int experimentCountCANCELLED;
    private float totalPayout;
    private float totalShowupFee;
    private int totalSignup;
    private int totalParticipated;

    private PagingManager pmgr = null;
    private PaginateListFactory paginateListFactory;
    private String searchView;
    private String searchResultsView;
    private String searchResultsName = "searchResults";
    private Integer pageSize;
    private CompassSearchHelper searchHelper;
    ExtendedPaginatedList paginatedList;

    public static String [] queryList = new String []{ "Username",
                                                          "Email",
                                                          "Name"};

    
    // BRS 2012-01-06: calculate statsYearList as in CalendarController, rather than // hard-coding
    int firstExpYear;
    public static List<String> statsYearList;
    
    public ModelAndView handle(HttpServletRequest request,
                                      HttpServletResponse response, Object command, BindException errors)
    throws Exception {
        //int year = Calendar.getInstance().get(Calendar.YEAR);
        //yearSelected = String.valueOf(year);

        // BRS 2012-01-06
        if(request.getSession().getAttribute("firstExpYear") == null)
            firstExpYear = experimentManager.getFirstExperimentDate().getYear()+1900;
        else
            firstExpYear = (Integer)request.getSession().getAttribute("firstExpYear");
        request.getSession().setAttribute("firstExpYear", firstExpYear);
        if(request.getSession().getAttribute("statsYearList") == null) {
            statsYearList = new ArrayList();
            statsYearList.add("All Years");
            Date today = new Date();
            for(int year=experimentManager.getFirstExperimentDate().getYear()+1900;year<=1900+today.getYear()+1;year++)
                statsYearList.add(Integer.toString(year));
            request.getSession().setAttribute("statsYearList", statsYearList);
        }

        yearSelected = "All Years";
        if(request.getParameter("yearSelected") != null) {
            yearSelected = (String)(request.getParameter("yearSelected"));
        }
        request.setAttribute("year",yearSelected);
        type = request.getParameter("type");
        request.setAttribute("type", type);

        if(type.compareTo("experimenter") == 0 || type.compareTo("subject") == 0)
            request.setAttribute("showSearch", true);

        paginatedList = paginateListFactory.getPaginatedListFromRequest(request);

        //====================================================================//
        //                               Search                               //
        //====================================================================//
        if("experimenter".equals(type) ||  "subject".equals(type)) {
            final CompassSearchCommand searchCommand = (CompassSearchCommand) command;

            //Default search values
            String queryField = "Name";
            String searchQuery = "";

            if(request != null) {
                request.setAttribute("queryList",queryList);
                searchQuery = request.getParameter("searchQuery");
            }
            if(searchQuery != null)
                queryField = request.getParameter("querySelected");
            request.setAttribute("queryField",queryField);
            request.setAttribute("searchQuery", searchQuery);



            //Formatting the searchQuery
            paginatedList.setSearch(false);
            if(searchQuery != null) {
                searchQuery = searchQuery.replaceAll("\\b\\s\\b", ", ");
                searchQuery = searchQuery.replaceAll("^\\(*\\)*\\}*\\{*\\!*\\^*\\**\\[*\\]*\\\\*", "");
            }
            if(StringUtils.hasText(searchQuery)) {
                searchQuery=queryField+":"+searchQuery;
                searchQuery = "+alias:User +"+searchQuery;
                searchQuery = searchQuery.trim();
                paginatedList.setSearch(true);
            }
            //Search values that were input
            searchCommand.setQuery(searchQuery);
            CompassSearchResults searchResults = searchHelper.search(searchCommand);
            List<CompassHit> clst = Arrays.asList(searchResults.getHits());
            Set<Long> hitIds = new HashSet();
            for(int i = 0; i < clst.size(); i++) {
                hitIds.add((Long)((User)clst.get(i).getData()).getId());
            }
            paginatedList.setSearchIds(hitIds);
        }
        //====================================================================//
        //                          End of Search                             //
        //====================================================================//


        
        //Default table values
        int index = 0;
        SortOrderEnum order = SortOrderEnum.ASCENDING;
        //Values derived from the table
        String stringIndex = request.getParameter(new ParamEncoder("stats").encodeParameterName(TableTagParameters.PARAMETER_PAGE));
        log.debug("stringIndex = " + stringIndex);
        String stringCol = request.getParameter(new ParamEncoder("stats").encodeParameterName(TableTagParameters.PARAMETER_SORT));
        String stringOrder = request.getParameter(new ParamEncoder("stats").encodeParameterName(TableTagParameters.PARAMETER_ORDER));
        //Change values accordingly
        if(stringIndex != null) { index = (Integer.parseInt(stringIndex)) -1; }
        if(stringOrder != null && stringOrder.equals("2")) { order = SortOrderEnum.DESCENDING; }
        //Set values to paginatedList
        paginatedList.setIndex(index);
        paginatedList.setSortDirection(order);
        


        if("experimenter".equals(type)) {
            paginatedList.setSortCriterion("username");
            return experimenter(request, response);
        }
        else if("experimentType".equals(type)) {
            paginatedList.setSortCriterion("name");
            return experimentType(request, response);
        }
        else if("location".equals(type)) {
            paginatedList.setSortCriterion("name");
            return location(request, response);
        }
        else if("subject".equals(type)) {
            paginatedList.setSortCriterion("username");
            return subject(request, response);
        }

        // BRS 2012-02-07
        else if ("namedQuery".equals(type)) {
            return namedQuery(request, response);
        }

        // BRS 2012-02-14
        else if ("databaseDump".equals(type)) {
            return databaseDump(request, response);
        }

        else
            return new ModelAndView(getHomeView());
    }

    // BRS 2012-02-08
    // Creates an ExtendedPaginatedList with the results of the given named query.
    // These named queries are in src/main/webapp/WEB-INF/classes/NamedQueries.hbm.xml
    // It also works differently from the other methods below in that it uses DisplayTag's capability of working with an instance of PaginatedList.
    // - See http://displaytag.sourceforge.net/1.2/tut_externalSortAndPage.html for details.
    public ModelAndView namedQuery(HttpServletRequest request, HttpServletResponse response) throws Exception {

        int index = 0;
        SortOrderEnum order = SortOrderEnum.ASCENDING;
        String stringIndex = request.getParameter("page");
        String stringCol = request.getParameter("sort");
        String stringOrder = request.getParameter("dir");
        if(stringIndex != null) { index = (Integer.parseInt(stringIndex)) -1; }
        if(stringOrder != null && stringOrder.equals("desc")) { order = SortOrderEnum.DESCENDING; }
        paginatedList.setIndex(index);
        paginatedList.setSortDirection(order);

        paginatedList = pmgr.getNamedQueryResults(request.getParameter("queryName"), paginatedList);
        return (new ModelAndView()).addObject("paginatedList", paginatedList);
    }

    // BRS 2012-02-14
    public ModelAndView databaseDump(HttpServletRequest request, HttpServletResponse response) throws Exception {

        // Create a temp directory into which to write the CSV files
        String tempDirName = System.getProperty("java.io.tmpdir");
        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss");
        String outDirName = "recruiting-db-" + formatter.format(new Date());
        File outDir = new File(tempDirName, outDirName);
        outDir.mkdir();

        // Run the python script to dump the database
        ServletContext context = request.getSession().getServletContext();
        String pythonScriptPath = context.getRealPath("/WEB-INF/python/dump-database-to-csv.py");
        String[] cmdarray = {"python", pythonScriptPath, "localhost", "recruiting", "recruiting", "recruiting", outDir.getPath()};
        Process pr = Runtime.getRuntime().exec(cmdarray);
        pr.waitFor();
        
        // Zip the directory
        File zipFile = new File(tempDirName, outDirName + ".zip");
        String[] cmdarray2 = {"zip", "-r", "--junk-paths", zipFile.getPath(), outDir.getPath()};
        pr = Runtime.getRuntime().exec(cmdarray2);
        pr.waitFor();

        // Send the zip file to the browser
        int read = 0;
        byte[] bytes = new byte[1024];
        response.setContentType("application/zip");
        response.addHeader("Content-Disposition", "attachment; filename=" + outDirName + ".zip");
        FileInputStream is = new FileInputStream(zipFile);
        OutputStream os = response.getOutputStream();
        while((read = is.read(bytes)) != -1) {
            os.write(bytes, 0, read);
        }
        os.flush();
        os.close();
        return null; // Dont re-render the view.
    }
    
    public ModelAndView subject(
            HttpServletRequest httpServletRequest,
            HttpServletResponse httpServletResponse)
    throws Exception {
        //List<Subject> subjects = extendedUserManager.getSubjects();
        List<Subject> subjects = pmgr.getSubjects(paginatedList).getList();
        List<Map> statsList = new ArrayList();
        for(Subject subject : subjects) {
            Map statsMap = new HashMap();
            statsMap.put("name", subject.getFullName());
            statsMap.put("userId", subject.getId());
            List<ParticipantRecord> records = participantRecordManager.getBySubject(subject);
            List<Experiment> experiments = new ArrayList<Experiment>();
            for(ParticipantRecord record : records) {
                experiments.add(record.getExp());
            }
            statsList.add(getExperimentStats(statsMap, experiments));
        }
        return (new ModelAndView()).addObject("statsList", statsList).addObject("resultSize", paginatedList.getFullListSize());
    }
    
    public ModelAndView experimenter(
            HttpServletRequest httpServletRequest,
            HttpServletResponse httpServletResponse)
    throws Exception {
        String role = httpServletRequest.getParameter("role");
        //List<Experimenter> experimenters = extendedUserManager.getExperimenters();
        List<Experimenter> experimenters = pmgr.getExperimenters(paginatedList).getList();
        List<Map> statsList = new ArrayList();
        for(Experimenter experimenter : experimenters)
        {
            log.debug("Getting stats for experimenter " + experimenter.getFullName());
            Map statsMap = new HashMap();
            statsMap.put("name", experimenter.getFullName());

            // BRS 2011-02-07 getExperimentStats() depends on userId being in the statsMap
            statsMap.put("userId", experimenter.getId());
            
            List<Experiment> experiments = null;
            if("runby".equals(role)) {
                experiments = experimentManager.getByRunby(experimenter.getId());
            }
            else if("reservedBy".equals(role))
                experiments = experimentManager.getByReservedBy(experimenter.getId());
            else if("principle".equals(role))
                experiments = experimentManager.getByPrinciple(experimenter.getId());
            else {
                experiments = new ArrayList(); // empty result
            }
            statsList.add(getExperimentStats(statsMap, experiments));
        }
        return (new ModelAndView()).addObject("statsList", statsList).addObject("resultSize", paginatedList.getFullListSize());
    }

    public ModelAndView experimentType(
            HttpServletRequest httpServletRequest,
            HttpServletResponse httpServletResponse)
    throws Exception {
        Set<ExperimentType> typesSet = new HashSet();
        //typesSet.addAll(experimentTypeManager.getAll()); // the backing List isn't unique based on ExperimentType.name
        typesSet.addAll(pmgr.getExperimentTypes(paginatedList).getList());
        List<Map> statsList = new ArrayList();
        for(ExperimentType et : typesSet) {
            Map statsMap = new HashMap();
            statsMap.put("name",et.getName());
            List<Experiment> experiments = experimentManager.getByExperimentType(et.getId());
            statsList.add(getExperimentStats(statsMap, experiments));
        }
        return new ModelAndView().addObject("statsList", statsList).addObject("resultSize", paginatedList.getFullListSize());
    }

    public ModelAndView location(
            HttpServletRequest httpServletRequest,
            HttpServletResponse httpServletResponse)
    throws Exception {
        List<Location> locations = locationManager.getAll();
        List<Map> statsList = new ArrayList();
        for(Location location : locations) {
            Map statsMap = new HashMap();
            statsMap.put("name",location.getName());
            List<Experiment> experiments = experimentManager.getByLocation(location.getId());
            statsList.add(getExperimentStats(statsMap, experiments));
        }
        return new ModelAndView().addObject("statsList", statsList).addObject("resultSize", paginatedList.getFullListSize());
    }
    
    private Map getExperimentStats(Map statsMap, List<Experiment> experiments) throws Exception {
        getExperimentStats((Long)statsMap.get("userId"),experiments);
        int expSize = 0;
        for(Experiment e : experiments) {
            if(!yearSelected.equals("All Years") && e.getExpDate().getYear()+1900 != Integer.parseInt(yearSelected))
                continue;
            expSize++;
        }
        statsMap.put("totalExperiments", expSize);
        statsMap.put("openExperiments", experimentCountOPEN);
        statsMap.put("cancelledExperiments", experimentCountCANCELLED);
        statsMap.put("finishedExperiments", experimentCountFINISHED);
        float total = totalPayout;
        statsMap.put("totalPayout", new Float(total));
        statsMap.put("averagePayout",
                (experiments.size() == 0 ? new Float(0) : new Float(total / (float)experiments.size())));
        statsMap.put("averageShowupFee",
                (experiments.size() == 0 ? new Float(0) : new Float(totalShowupFee / (float)experiments.size())));
        statsMap.put("totalSignup", new Integer(totalSignup));
        statsMap.put("averageSignup",
                (experiments.size() == 0 ? new Integer(0) : new Integer(totalSignup / experiments.size())));
        statsMap.put("totalParticipated", new Integer(totalParticipated));
        statsMap.put("averageParticipated",
                (experiments.size() == 0 ? new Integer(0) : new Integer(totalParticipated / experiments.size())));
        //log.debug("getExperimentStats(Map, List<Experiment>) is going to return " + statsMap.toString());
        return statsMap;
    }
    
    private void getExperimentStats(Long id,List<Experiment> experiments) {
        experimentCountOPEN = 0;
        experimentCountFINISHED = 0;
        experimentCountCLOSED = 0;
        experimentCountCANCELLED = 0;
        totalPayout = 0.0F;
        totalShowupFee = 0.0F;
        totalSignup = 0;
        totalParticipated = 0;
        List<Long> experimentIds = new ArrayList();
        for(Experiment e : experiments) {
            if(!yearSelected.equals("All Years") && e.getExpDate().getYear()+1900 != Integer.parseInt(yearSelected))
                continue;
            if(e.getStatus() == Experiment.Status.OPEN)
                experimentCountOPEN++;
            if(e.getStatus() == Experiment.Status.CANCELLED || e.getStatus() == Experiment.Status.CLOSED)
                experimentCountCANCELLED++;
            if(e.getStatus() == Experiment.Status.FINISHED)
                experimentCountFINISHED++;

            //If the type is 'subject'
            if(type.equals("subject")) {
                List<ParticipantRecord> prList = participantRecordManager.getByExperiment(e);
                prList.retainAll(participantRecordManager.getBySubject(extendedUserManager.getSubjectById(id)));
                ParticipantRecord pr = prList.get(0);
                if(pr.isParticipated())
                    totalParticipated++;
                else
                    totalSignup++;
                if (pr.getPayoff() != null)
                    totalPayout += pr.getPayoff();
            }
            experimentIds.add(e.getId());
            totalShowupFee += e.getShowUpFee();
        }
        //If the type is not 'subject'
        if(!type.equals("subject")) {
            totalSignup = participantRecordManager.getSignupByExperiments(experimentIds);
            totalPayout = participantRecordManager.getPayoffByExperiments(experimentIds);
            totalParticipated = participantRecordManager.getParticipatedByExperiments(experimentIds);
        }
    }

    private int getExperimentCountByStatus(List<Experiment> experiments, Experiment.Status status) {
        int count = 0;
        for(Experiment e : experiments)
            if(e.getStatus() == status)
                count++;
        return count;
    }

    private float getTotalPayout(List<Experiment> experiments) {
        float total = 0.0F;
        for(Experiment e : experiments) {
            List<ParticipantRecord> participants = participantRecordManager.getByExperiment(e);
            for(ParticipantRecord pr : participants)
                if(pr.getPayoff() != null)
                    total += pr.getPayoff().floatValue();
        }
        return total;
    }

    private float getTotalShowupFee(List<Experiment> experiments) {
        float total = 0.0F;
        for(Experiment e : experiments) {
            total += e.getShowUpFee();
        }
        return total;
    }

    private int getTotalSignup(List<Experiment> experiments) {
        int total = 0;
        for(Experiment e : experiments)
            total += participantRecordManager.getByExperiment(e).size();
        return total;
    }

    private int getTotalParticipated(List<Experiment> experiments) {
        int total = 0;
        for(Experiment e : experiments) {
            List<ParticipantRecord> participants = participantRecordManager.getByExperiment(e);
            for(ParticipantRecord pr : participants)
                if(pr.isParticipated())
                    total++;
        }
        return total;
    }

    public void setExperimentManager(ExperimentManager experimentManager) {
        this.experimentManager = experimentManager;
        setCommandClass(CompassSearchCommand.class);
    }

    public void setExperimentTypeManager(ExperimentTypeManager experimentTypeManager) {
        this.experimentTypeManager = experimentTypeManager;
    }

    public void setLocationManager(LocationManager locationManager) {
        this.locationManager = locationManager;
    }

    public void setExtendedUserManager(ExtendedUserManager extendedUserManager) {
        this.extendedUserManager = extendedUserManager;
    }

    public void setParticipantRecordManager(ParticipantRecordManager participantRecordManager) {
        this.participantRecordManager = participantRecordManager;
    }

    public String getHomeView() {
        return homeView;
    }

    public void setHomeView(String homeView) {
        this.homeView = homeView;
    }

    public void setPagingManager(PagingManager pagingManager) {
        this.pmgr = pagingManager;
    }

    public void setPaginateListFactory(PaginateListFactory paginateListFactory) {
        this.paginateListFactory = paginateListFactory;
    }

    public String getSearchView() {
        return searchView;
    }

    public void setSearchView(String searchView) {
        this.searchView = searchView;
    }

    public String getSearchResultsName() {
        return searchResultsName;
    }

    public void setSearchResultsName(String searchResultsName) {
        this.searchResultsName = searchResultsName;
    }

    public String getSearchResultsView() {
        return searchResultsView;
    }

    public void setSearchResultsView(String resultsView) {
        this.searchResultsView = resultsView;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public void setSearchHelper(CompassSearchHelper searchHelper) {
        this.searchHelper = searchHelper;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        super.afterPropertiesSet();
        if (searchView == null) {
            throw new IllegalArgumentException(
                    "Must set the searchView property");
        }
        if (searchResultsView == null) {
            throw new IllegalArgumentException(
                    "Must set the serachResultsView property");
        }
        if (searchHelper == null) {
            searchHelper = new CompassSearchHelper(getCompass(), getPageSize());
        }
    }
}
