/*
 * Copyright (C) 1998-2009, <a href="http://www.ssel.caltech.edu">SSEL</a>,
 * at Caltech, Pasadena, CA 91125. 
 *
 * MooreRecruiting is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 * USA.
 *
 */

package edu.caltech.ssel.recruiting.webapp.controller;

import edu.caltech.ssel.recruiting.common.ExperimentCreationHelper;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

/**
* @author Michael Kolodrubetz, SSEL, Caltech
*/
public class ClearSessionController implements Controller {
    
    public static Map<String,String> VIEW_MAP;
    private ExperimentCreationHelper ecHelper;
    
    /** Creates a new instance of ClearSessionController */
    public ClearSessionController() {
        VIEW_MAP = new HashMap();
        VIEW_MAP.put("experiment", "redirect:experimentform.html");
        VIEW_MAP.put("modifyExperiment", "redirect:modifyexperiment.html");
        VIEW_MAP.put("survey", "redirect:surveycreationform.html");
        VIEW_MAP.put("filter", "redirect:filterform.html");
        VIEW_MAP.put("announcement", "redirect:announcementform.html");
        VIEW_MAP.put("importc2", "redirect:importc2form.html");
    }

    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
        ecHelper.setSession(request.getSession());
        ecHelper.clearSession();
        request.getSession().removeAttribute("experiment");

        String view = VIEW_MAP.get(request.getParameter("view"));
        
        //Remove view, only save the first of the rest of the params
        Map m = request.getParameterMap();
        Map<String, String> m2 = new HashMap();
        Enumeration<String> keySet = (Enumeration<String>)request.getParameterNames();
        String s;
        while(keySet.hasMoreElements()) {
            s = keySet.nextElement();
            if(!s.equals("view"))
                m2.put(s, request.getParameter(s));
        }
        
        return new ModelAndView(view).addAllObjects(m2);
    }

    public void setExperimentCreationHelper(ExperimentCreationHelper ecHelper) {
        this.ecHelper = ecHelper;
    }
    
}
