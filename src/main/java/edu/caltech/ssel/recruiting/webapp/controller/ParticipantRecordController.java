/*
 * Copyright (C) 1998-2009, <a href="http://www.ssel.caltech.edu">SSEL</a>,
 * at Caltech, Pasadena, CA 91125. 
 *
 * MooreRecruiting is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 * USA.
 *
 */

package edu.caltech.ssel.recruiting.webapp.controller;

import edu.caltech.ssel.recruiting.common.ExtendedPaginatedList;
import edu.caltech.ssel.recruiting.common.PaginateListFactory;
import edu.caltech.ssel.recruiting.model.Experiment;
import edu.caltech.ssel.recruiting.service.ParticipantRecordManager;
import edu.caltech.ssel.recruiting.service.PagingManager;
import edu.caltech.ssel.recruiting.service.ExperimentManager;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.appfuse.service.GenericManager;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

// BRS
import java.io.InputStream;
import java.io.FileInputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Name;
import java.lang.Integer;
import java.util.Date;
import java.text.SimpleDateFormat;
import edu.caltech.ssel.recruiting.model.Subject;
import edu.caltech.ssel.recruiting.model.ParticipantRecord;
import edu.caltech.ssel.recruiting.model.ParticipantRecord.Status;
import java.util.Locale;
import java.text.DecimalFormat;
import java.text.NumberFormat;

/**
 *
 * @author michaelk
 */
public class ParticipantRecordController implements Controller {
    
    private ParticipantRecordManager participantRecordManager;
    private ExperimentManager experimentManager; // BRS 2012-01-06 changed from GenericManager<Experiment, Long>
    private PagingManager pmgr = null;
    private PaginateListFactory paginateListFactory;
    private Integer pageSize;
    private SimpleDateFormat dateFormat;
    private NumberFormat moneyFormat;
    
    /** Creates a new instance of ParticipantRecordController */
    public ParticipantRecordController() {
        dateFormat = new SimpleDateFormat("MM/dd/yyyy");
        moneyFormat = new DecimalFormat("#0.00");
    }

    // BRS
    // Given the Sheet and Excel-style cell reference string (e.g. "A1" or "G12") set the cell to the given String value.
    private void setCell(Sheet s, String cellRef, String value) {

        if (value == null) {
            return;
        }
        
        int c = (int) (cellRef.charAt(0) - 'A');        // column index. Assumes we're only using A-Z, not AA, etc.
        int r = Integer.parseInt(cellRef.substring(1)) - 1; // row index
        //System.out.println("Translated " + cellRef + " to col="+c+", row="+r);

        Row row = s.getRow(r);
        Cell cell = row.getCell(c);
        cell.setCellType(Cell.CELL_TYPE_STRING);
        cell.setCellValue(value);
    }

    private void setCell(Sheet s, String cellRef, int value) {
        setCell(s, cellRef, ""+value);
    }
    
    // Given a 2-letter country code, return the full English name of the country.
    private String countryName(String code) {
        if (code == null) {
            return "";
        }
        Locale l = new Locale("en", code); // just use English for the language; we only care about the country
        return l.getDisplayCountry();
    }

    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
        
        ModelAndView mav = new ModelAndView();
        List participantRecordList = null;
        ArrayList<ParticipantRecord> prevParticipantRecordList = null;
        Experiment experiment = (Experiment)request.getSession().getAttribute("experiment");
        if(experiment != null) {

            // BRS - now that we want to add some data to the <display:table> that isn't in the ParticipantRecords,
            // we can't use the List of ParticipantRecords directly any more - need a List of Maps,
            // each of which contains the ParticipantRecord, plus the extra data.
            List precList = participantRecordManager.getByExperiment(
                    experimentManager.get(experiment.getId()));
            participantRecordList = new ArrayList(precList.size());
            for (int i = 0; i < precList.size(); i++) {
                HashMap m = new HashMap();
                ParticipantRecord prec = (ParticipantRecord) precList.get(i);
                int noShowCount = participantRecordManager.getPastCountBySubjectAndStatus(prec.getSubject(), Status.STATUS_3); // Sign-up
                int bumpCount = participantRecordManager.getPastCountBySubjectAndStatus(prec.getSubject(), Status.STATUS_1); // Show-up
                m.put("prec", prec);
                m.put("noShowCount", noShowCount);
                m.put("bumpCount", bumpCount);
                participantRecordList.add(m);
            }

            // BRS 2012-01-06
            // Create list of participants in previous experiments of this type.
            //SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss z");
            prevParticipantRecordList = new ArrayList();
            List<Experiment> experiments = experimentManager.getByExperimentType(experiment.getType().getId());
            for (Experiment e : experiments) {
                if (e.getStartDateTime().before(experiment.getStartDateTime())) {
                    //System.out.println(sdf.format(e.getStartDateTime().getTime()) + " is before " + sdf.format(experiment.getStartDateTime().getTime()));
                    prevParticipantRecordList.addAll(participantRecordManager.getByExperiment(e));
                }
            }

            // BRS
            // If the page was requested by clicking the "Print Receipts" button,
            // generate an Excel file with the receipts and send it to the browser
            // instead of the usual jsp view.
            System.out.println(request.getParameter("downloadReceipts"));
            if (request.getParameter("downloadReceipts") != null) {
                System.out.println("downloadReceipts isn't null");
                response.setContentType("application/vnd.ms-excel");
                response.addHeader("Content-Disposition", "attachment; filename=receipts.xls");
                //String sourceXLSName = "/home/ben/Documents/exp econ/recruiting/recruiting/src/main/webapp/images/test.xls";
                String sourceXLSName = request.getSession().getServletContext().getRealPath("/spreadsheets/TSDF for Research Subject Payments.xls");
                //String sourceXLSName = request.getSession().getServletContext().getRealPath("/spreadsheets/test.xls");
                System.out.println("about to create FileInputStream");
                FileInputStream inputStream = new FileInputStream(sourceXLSName); 
                System.out.println("created FileInputStream");
                
                System.out.println("creating workbook");
                Workbook wb = WorkbookFactory.create(inputStream);


                System.out.println("copying sheets");
                // Duplicate the sheet so we have one sheet for each participant
                for (int i = 1; i < participantRecordList.size(); i++) {
                    Sheet sheet = wb.cloneSheet(0);
                    sheet.setSelected(true); // Select all sheets so they can be printed all at once.
                }

                System.out.println("filling out sheets");
                // For each participant record, get the subject, and get the next sheet, and fill it out.
                for (int i = 0; i < participantRecordList.size(); i++) {
                    System.out.println("filling out sheet for participant " + i);
                    ParticipantRecord prec = (ParticipantRecord) ((HashMap) participantRecordList.get(i)).get("prec");
                    Subject s = prec.getSubject();
                    Sheet sheet = wb.getSheetAt(i);

                    wb.setSheetName(i, s.getFirstName() + " " + s.getLastName());

                    // Today's date
                    setCell(sheet, "R4", dateFormat.format(new Date()).toString());

                    setCell(sheet, "E6", s.getFirstName());
                    setCell(sheet, "E8", s.getLastName());

                    int idType = 1;
                    if (s.getIdType() != null) {
                        idType = s.getIdType();
                    }
                    String idTypeRow = "6";
                    switch (idType) {
                        case 1:
                            idTypeRow = "6";
                            break;
                        case 2:
                            idTypeRow = "8";
                            break;
                        case 3:
                            idTypeRow = "9";
                            break;
                        case 4:
                            idTypeRow = "10";
                            break;
                    }
                    setCell(sheet, "P"+idTypeRow, "X");
                    setCell(sheet, "R"+idTypeRow, s.getSchoolId());

                    setCell(sheet, "E10", s.getAddressLine1() + ", " + s.getAddressLine2());
                    setCell(sheet, "E11", s.getCity() + ", " + s.getState() + " " + s.getZip());
                    setCell(sheet, "E13", s.getEmail());

                    if (s.getUsCitizen() != null && s.getUsCitizen() == true) {
                        setCell(sheet, "L16", "X");
                    } else if (s.getUsCitizen() != null && s.getUsCitizen() == false) {
                        setCell(sheet, "O16", "X");
                    }

                    // Need keep track of how many extra rows are inserted for travel table overflow.
                    int rowsAdded = 0;

                    if (s.getUsCitizen() != null && s.getUsCitizen() == false) {
                        // Do the non-US citizen part

                        setCell(sheet, "G19", countryName(s.getCitizenCountry()));
                        setCell(sheet, "Q19", countryName(s.getBirthCountry()));
                        setCell(sheet, "K20", countryName(s.getPriorCountry()));
                        setCell(sheet, "Q20", dateFormat.format(s.getPriorCountryStartDate()).toString());
                        setCell(sheet, "S20", dateFormat.format(s.getPriorCountryEndDate()).toString());
                        setCell(sheet, "M22", dateFormat.format(s.getFirstUsEntryDate()).toString());
                        setCell(sheet, "S22", dateFormat.format(s.getCurrentUsEntryDate()).toString());

                        if (s.getI94ExpirationDS() != null) {
                            if (s.getI94ExpirationDS()) {
                                setCell(sheet, "E24", "X");
                            } else {
                                setCell(sheet, "H24", "X");
                                setCell(sheet, "K24", dateFormat.format(s.getI94ExpirationDate()).toString());
                            }
                        }

                        setCell(sheet, "C26", s.getPassportNumber());
                        setCell(sheet, "K26", countryName(s.getPassportCountryOfIssue()));
                        setCell(sheet, "P26", dateFormat.format(s.getPassportExpirationDate()).toString());

                        if (s.getVisaType() != null) {
                            if (s.getVisaType().equals("B-1")) {
                                setCell(sheet, "D28", "X");

                            } else if (s.getVisaType().equals("B-2")) {
                                setCell(sheet, "F28", "X");

                            } else if (s.getVisaType().equals("J-1")) {
                                setCell(sheet, "H28", "X");

                            } else if (s.getVisaType().equals("F-1")) {
                                setCell(sheet, "J28", "X");

                            } else if (s.getVisaType().equals("WB/WT")) {
                                setCell(sheet, "L28", "X");
                            }
                        }

                        setCell(sheet, "N28", s.getVisaTypeOther());
                        setCell(sheet, "N30", s.getJvisaCategory());

                        if (s.getVisaPriorTo7Years() != null && s.getVisaPriorTo7Years() == true) {
                            setCell(sheet, "B34", "X");
                        }

                        // Enter the variable number of travel table rows
                        //
                        List<Date> arrivalDates = s.getArrivalDates();
                        List<Date> departureDates = s.getDepartureDates();
                        List<String> visaTypes = s.getVisaTypes();
                        List<String> purposes = s.getPurposes();
                        //
                        int rowNum = 39; // Number of first row in travel table in spreadsheet (Excel 1-based numbering)
                        int j = 0; // index into travel table rows in database
                        try {
                            for (j = 0; j < arrivalDates.size(); j++) {

                                // If we're past the end of the table, need to create a new row
                                if (rowNum > 46) {
                                    /*
                                       void shiftRows(int startRow,
                                       int endRow,
                                       int n,
                                       boolean copyRowHeight,
                                       boolean resetOriginalRowHeight)
                                       */
                                    // rowNum is now the row just below the bottom of the table.
                                    // Need to subtract 1 to get POI's 0-based row numbers.
                                    // Shift the rows down one.
                                    sheet.shiftRows(rowNum-1, sheet.getLastRowNum(), 1, true, false);
                                    // Create a new row in the vacated space.
                                    // (I don't see a way to copy an existing row)
                                    Row newRow = sheet.createRow(rowNum-1);
                                    // Fill the row with cells
                                    Row oldRow = sheet.getRow(rowNum-2);
                                    for (int k = 0; k < oldRow.getLastCellNum(); k++) {
                                        newRow.createCell(k);
                                    }

                                    rowsAdded++;
                                }

                                setCell(sheet, "B"+rowNum, dateFormat.format(arrivalDates.get(j)).toString());
                                setCell(sheet, "D"+rowNum, dateFormat.format(departureDates.get(j)).toString());
                                setCell(sheet, "H"+rowNum, visaTypes.get(j));
                                setCell(sheet, "J"+rowNum, purposes.get(j));
                                rowNum++;
                            }
                        } catch (Exception e) {
                            System.out.println("Exception processing travel table row " + j + ": " + e.toString());
                        }
                    } // Done with non-US citizen part

                    if (prec.getPayoff() != null) {

                        setCell(sheet, "E"+(49+rowsAdded), "$" + moneyFormat.format(prec.getPayoff()));
                    }

                    // BRS 2011-06-13: set "UA Staff Only" bottom-right fields
                    try {
                        setCell(sheet, "Q"+(49+rowsAdded), prec.getExp().getType().getAccountNumber());
                    } catch (Exception e) {
                        System.out.println("Exception setting account number cell");
                    }
                    try {
                        setCell(sheet, "Q"+(50+rowsAdded), prec.getExp().getType().getContactPerson().getDepartment());
                    } catch (Exception e) {
                        System.out.println("Exception setting department cell");
                    }
                    try {
                        setCell(sheet, "Q"+(51+rowsAdded), prec.getExp().getType().getContactPerson().getFullName());
                    } catch (Exception e) {
                        System.out.println("Exception setting contact person cell");
                    }
                    try {
                        setCell(sheet, "Q"+(52+rowsAdded), prec.getExp().getType().getContactPerson().getPhoneNumber());
                    } catch (Exception e) {
                        System.out.println("Exception setting phone number cell");
                    }
                }


                System.out.println("writing to response.getOutputStream()");
                OutputStream outputStream = response.getOutputStream(); 
                wb.write(outputStream);

                inputStream.close(); 
                inputStream = null;// no close twice 
                //String testString = new String("This is a test file."); 
                outputStream.flush(); 
                outputStream.close(); 
                outputStream = null;// no close twice
                System.out.println("done with downloadReceipts - returning from handleRequest()");
                return null; // Don't re-render the view.
            }
        }

        // BRS: Normal request (not receipts spreadsheet)
        setPageSize(25);
        mav.addObject("participantRecordList", participantRecordList);
        mav.addObject("prevParticipantRecordList", prevParticipantRecordList);
        if(request.getParameter("size") != null) {
            String size = (String)request.getParameter("size");
            if(size.compareTo("All") == 0)
               // setPageSize(participantRecordList.size());
               setPageSize(1000000); // BRS 2012-01-06: make sure all are shown in both tables
            else
                setPageSize(Integer.parseInt(size));
        }
        mav.addObject("pageSize", pageSize);
        return mav;
    }

    public void setParticipantRecordManager(ParticipantRecordManager participantRecordManager) {
        this.participantRecordManager = participantRecordManager;
    }

    public void setExperimentManager(ExperimentManager experimentManager) {
        this.experimentManager = experimentManager;
    }
    
    public void setPagingManager(PagingManager pagingManager) {
        this.pmgr = pagingManager;
    }
    
    public void setPaginateListFactory(PaginateListFactory paginateListFactory) {
        this.paginateListFactory = paginateListFactory;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }
    
}
