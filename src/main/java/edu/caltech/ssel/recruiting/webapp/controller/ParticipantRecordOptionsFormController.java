/*
 * Copyright (C) 1998-2009, <a href="http://www.ssel.caltech.edu">SSEL</a>,
 * at Caltech, Pasadena, CA 91125. 
 *
 * MooreRecruiting is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 * USA.
 *
 */

package edu.caltech.ssel.recruiting.webapp.controller;

import edu.caltech.ssel.recruiting.common.RecruitingConstants;
import edu.caltech.ssel.recruiting.common.DebugHelper;
import edu.caltech.ssel.recruiting.model.Experiment;
import edu.caltech.ssel.recruiting.model.ParticipantRecord;
import edu.caltech.ssel.recruiting.model.Payment;
import edu.caltech.ssel.recruiting.model.Subject;
import edu.caltech.ssel.recruiting.service.EmailManager;
import edu.caltech.ssel.recruiting.service.ExperimentManager;
import edu.caltech.ssel.recruiting.service.ExtendedUserManager;
import edu.caltech.ssel.recruiting.service.ParticipantRecordManager;
import edu.caltech.ssel.recruiting.service.PaymentManager;
import edu.caltech.ssel.recruiting.service.ReminderManager;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.io.InputStream;
import java.util.Properties;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.AccessDeniedException;
import org.apache.commons.lang.StringUtils;
import org.appfuse.Constants;
import org.appfuse.model.User;
import org.appfuse.webapp.controller.BaseFormController;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.servlet.ModelAndView;

/**
 * Provides a Subject the ablility to edit aspects of the participant record
 * including deleting it.
 */
public class ParticipantRecordOptionsFormController extends BaseFormController {
    
    private ParticipantRecordManager participantRecordManager;
    private PaymentManager paymentManager;
    private ExperimentManager experimentManager;
    private ExtendedUserManager extendedUserManager;
    private ReminderManager reminderManager;
    private EmailManager emailManager;
    private String invalidView;
    private static final String s_props = "subject.properties";
    private static Properties properties;
    private static boolean unsubscribe = true;
    
    static {
        properties = new Properties();
        try {
            InputStream is = null;
            ClassLoader cl = ParticipantRecordManager.class.getClassLoader();
            if(cl != null) is = cl.getResourceAsStream(s_props);
            if(is == null) is = ClassLoader.getSystemResourceAsStream(s_props);
            properties.load(is);
            unsubscribe = Boolean.parseBoolean(properties.getProperty("unsubscribe"));
            //log.debug("loaded");
        }
        catch(Exception any) {
            //log.error("failed to init ", any);
        }
    }
    
    /** Creates a new instance of ParticipantOptionsFormController */
    public ParticipantRecordOptionsFormController() {
        setCommandClass(ParticipantRecord.class);
        setCommandName("participantRecord");
    }
    
    protected ModelAndView showForm(HttpServletRequest request,
            HttpServletResponse response,
            BindException errors)
            throws Exception {
        
        // make sure the user is editting their own record
        String username = request.getRemoteUser();
        User user = extendedUserManager.getUserByUsername(username);
        if(!(user instanceof Subject)  &&  !(request.isUserInRole(RecruitingConstants.ADMIN_ROLE)))
            return new ModelAndView(getInvalidView());
        if(request.isUserInRole(RecruitingConstants.ADMIN_ROLE))
            return super.showForm(request, response, errors);
        
        Subject subject = (Subject)user;
        String id = request.getParameter("id");
        if(!StringUtils.isEmpty(id)) {
            ParticipantRecord prec = participantRecordManager.get(Long.parseLong(id));
            if(!subject.getId().equals(prec.getSubject().getId())) {
                response.sendError(HttpServletResponse.SC_FORBIDDEN);
                log.warn("User '" + subject.getId() + "' is trying to edit participant record with subject id '" +
                        prec.getSubject().getId() + "'");
                throw new AccessDeniedException("You do not have permission to edit the participant record.");
            }
        }
        
        return super.showForm(request, response, errors);
    }
    
    protected Object formBackingObject(HttpServletRequest request) throws Exception {
        String id = request.getParameter("id");
        if(!StringUtils.isEmpty(id)) {
            return participantRecordManager.get(Long.parseLong(id));
        }
        return super.formBackingObject(request);//Use the default constructor, for default settings change that constructor
    }
    
    protected Map referenceData(HttpServletRequest request, Object command, Errors errors) throws Exception {
        Map m = new HashMap();
        // add data
        ParticipantRecord prec = (ParticipantRecord)command;
        boolean frozen = prec.getExp().getFreezeTime().before(new GregorianCalendar());
        m.put("isAdmin", request.isUserInRole(RecruitingConstants.ADMIN_ROLE));
        m.put("subject", prec.getSubject());
        m.put("frozen", new Boolean(frozen));
        m.put("unsubscribe", unsubscribe);
        log.debug("frozen: " + frozen);
        log.debug("unsubscribe: "+unsubscribe);
        return m;
    }
    
    public ModelAndView processFormSubmission(HttpServletRequest request,
            HttpServletResponse response,
            Object command,
            BindException errors)
            throws Exception {
        
        DebugHelper.displayParams(request, log);
        
        if(request.getParameter("cancel") != null) {
            if(!request.isUserInRole(RecruitingConstants.ADMIN_ROLE))
                return new ModelAndView(getSuccessView());
            Long id = Long.parseLong(request.getParameter("id"));
            Long sid = extendedUserManager.getSubjectIDByParticipationID(id);
            return new ModelAndView(getSuccessView()+"?id="+sid);
        }
        
        return super.processFormSubmission(request, response, command, errors);
    }
    
    protected ModelAndView onSubmit(HttpServletRequest request,
            HttpServletResponse response, Object command,
            BindException errors)
            throws Exception {
        log.debug("Entering onsubmit...");
        
        ParticipantRecord rec = (ParticipantRecord)command;
        Locale locale = request.getLocale();
        ModelAndView mv = new ModelAndView(getSuccessView());
        if(request.isUserInRole(RecruitingConstants.ADMIN_ROLE)) {
            Long id = Long.parseLong(request.getParameter("id"));
            Long sid = extendedUserManager.getSubjectIDByParticipationID(id);
            mv = new ModelAndView(getSuccessView()+"?id="+sid);
        }
        
        //Delete the record if necessary
        if(request.getParameter("participatebox") != null) {
            participantRecordManager.remove(rec.getId());
            saveMessage(request, getText("participantRecordOptions.optOut", locale));
            
            // open experiment for additional Subjects if it is FULL
            GregorianCalendar now = new GregorianCalendar();
            Experiment experiment = rec.getExp();
            if(Experiment.Status.FULL == experiment.getStatus() &&
                    now.before(experiment.getFreezeTime())) {
                experiment.setStatus(Experiment.Status.OPEN);
                experimentManager.save(experiment);
            }
            
            // cancel experiment reminders for this user
            reminderManager.removeExperimentReminders(
                    rec.getSubject().getId(), rec.getExp());

            // BRS - don't email unsubscribed users
            if (!rec.getSubject().getSubscribed()) {
                log.info("Not emailing unsubscribed subject " + rec.getSubject().getEmail());
                return mv;
            }

            Map model = new HashMap();
            model.put(Constants.EMAIL_TO, rec.getSubject().getEmail());
            model.put(Constants.EMAIL_REPLY_TO, experiment.getReservedBy().getEmail());
            Object [] params = new Object[1];
            params[0] = experiment.getId();
            model.put(Constants.EMAIL_SUBJECT, getText("experiment.signUpRemovalConfirmation", params, locale));
            model.put(Constants.USER, (User)rec.getSubject());
            model.put(Constants.EXPERIMENT, experiment);

            model.put("labName", getText("company.name", locale));
            model.put("labUrlName", getText("company.url", locale));
            model.put("labPhone", getText("company.phone", locale));
            
            emailManager.sendEmail(this.templateName, model);

            return mv;
        }
        
        // update payment info
        String accountInfo = request.getParameter("accountInfo");
        if(ParticipantRecord.PaymentMethod.PAYGATE.equals(rec.getPaymentMethod())) {
            Payment payment = rec.getPayment();
            if(payment != null) {
                payment.setAccountInfo(accountInfo);
                paymentManager.save(payment);
            }
        }
        
        // create reminders if necessary
        if(request.getParameter("reminderbox") != null) {
            reminderManager.createExperimentReminders(rec.getSubject(), rec.getExp());
        }

        //Save the participant record?
        rec = participantRecordManager.save(rec);
        saveMessage(request, getText("participantRecordOptions.updated", locale));
        
        return mv;
    }
    
    public void setExtendedUserManager(ExtendedUserManager extendedUserManager) {
        this.extendedUserManager = extendedUserManager;
    }
    
    public void setParticipantRecordManager(ParticipantRecordManager participantRecordManager) {
        this.participantRecordManager = participantRecordManager;
    }
    
    public void setExperimentManager(ExperimentManager experimentManager) {
        this.experimentManager = experimentManager;
    }

    public void setPaymentManager(PaymentManager paymentManager) {
        this.paymentManager = paymentManager;
    }

    public void setReminderManager(ReminderManager reminderManager) {
        this.reminderManager = reminderManager;
    }

    public String getInvalidView() {
        return invalidView;
    }

    public void setInvalidView(String invalidView) {
        this.invalidView = invalidView;
    }
    
    public boolean getUnsubscribe() {
        return unsubscribe;
    }
    
    public void setUnsubscribe(boolean unsubscribe) {
        this.unsubscribe = unsubscribe;
    }

    /**
     * @param emailManager the emailManager to set
     */
    public void setEmailManager(EmailManager emailManager) {
        this.emailManager = emailManager;
    }
}
