/*
 * Copyright (C) 1998-2009, <a href="http://www.ssel.caltech.edu">SSEL</a>,
 * at Caltech, Pasadena, CA 91125. 
 *
 * MooreRecruiting is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 * USA.
 *
 */

package edu.caltech.ssel.recruiting.webapp.controller;

import edu.caltech.ssel.recruiting.model.Experiment;
import edu.caltech.ssel.recruiting.model.Experimenter;
import edu.caltech.ssel.recruiting.model.Subject;
import edu.caltech.ssel.recruiting.model.ParticipantRecord;
import edu.caltech.ssel.recruiting.model.ParticipantRecord.Status;
import edu.caltech.ssel.recruiting.model.ExperimentType;
import edu.caltech.ssel.recruiting.model.SubjectPool;
import java.math.BigDecimal;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import org.appfuse.Constants;
import org.appfuse.model.User;
import org.appfuse.service.UserExistsException;

/**
 * Utility for parsing input to import a CasselWeb2 database into CasselWeb3
 * The service assumes the standard CasselWeb2 schema in a MySQL Database.
 */

/**
* @author Richar Ho, CASSEL, UCLA
* @author <a href="mailto:wmyuan@hss.caltech.edu">Walter M. Yuan</a>, SSEL, Caltech
*/
public class ImportC2Utility implements Runnable {
    
    public static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    public static final String DEFAULT_PASSWORD = "123-abc";
    public static final String DEFAULT_EXPERIMENTER_PASSWORD = "ssel123";
    
    private int experimentCount = 0;
    private int experiments2ImportCount = 0;
    private BigDecimal experiments2ImportTimeLeft;
    private int experimentErrorCount = 0;
    
    private int experimentTypeCount = 0;
    private int experimentTypeErrorCount = 0;
    
    private int subjectPoolCount = 0;
    private int subjectPoolErrorCount = 0;
    
    private int participantRecordCount = 0;
    private int participantRecords2ImportCount = 0;
    private BigDecimal participantRecords2ImportTimeLeft;
    private int participantRecordErrorCount = 0;
    
    private int experimenterCount = 0;
    private int experimenters2ImportCount = 0;
    private BigDecimal experimenters2ImportTimeLeft;
    private int experimenterErrorCount = 0;
    
    private int subjectCount = 0;
    private int subjects2ImportCount = 0;
    private BigDecimal subjects2ImportTimeLeft;
    private int subjectErrorCount = 0;
    
    private ImportC2FormController controller;
    private boolean quit = false;
    private boolean ran = false;
    private boolean done = false;
    
    private final Log log = LogFactory.getLog(ImportC2Utility.class);
    private Connection conn;
    private String connErrMsg;
    
    private int expCreationTime = 0;
    private int avgExpCreationTime = 0;
    private int precCreationTime = 0;
    private int avgPrecCreationTime = 0;
    private int experimenterCreationTime;
    private int avgExperimenterCreationTime;
    private int subjectCreationTime;
    private int avgSubjectCreationTime;
    
    private static Date defaultRegDate;
   
    // Each entry in these Maps maps a CasselWeb2 record id to a MooreRecruiting record id or object
    // (i.e. look up using CW2 id to retrieve MR id) -BRS
    private Map<Long, Long> experimenterCache;
    private Map<Long, ExperimentType> experimentTypeCache;
    private Map<Long, Long> subjectCache;
    //private Map<Long, Experiment> experimentCache;  // BRS 2011-02-09 not needed, since experiment ids are preserved
    private Map<Long, SubjectPool> subjectPoolCache;
    
    static {
        GregorianCalendar cal = new GregorianCalendar(
                1970, 0, 1, 0, 0, 0); // epoch
        cal.setTimeZone(TimeZone.getTimeZone("GMT"));
        defaultRegDate = cal.getTime();
    }
    
    /** Creates a new instance of ImportC2Util */
    public ImportC2Utility() {};
    
    /** Creates a new instance of ImportC2Util */
    public ImportC2Utility(
            String hostname,
            String port,
            String dbname,
            String username,
            String password) {
        
        try {
            setConnection(
                    hostname,
                    port,
                    dbname,
                    username,
                    password);
        } catch(Exception any) {
            connErrMsg = any.getMessage();
            log.info("error creating connection to "+hostname+": "+any.getMessage());
        }
        
        // init caches
        experimenterCache = new HashMap();
        experimentTypeCache = new HashMap();
        subjectCache = new HashMap();
        //experimentCache = new HashMap();
        subjectPoolCache = new HashMap();
    }

    /**
     * Imports the data from a CasselWeb2 database to CasselWeb3
     * The imports can be called in any order. [Actually, let's keep it simple and take back that last sentence. -BRS]
     */
    public void run() {
        
        ran = true;
        try {
            if(!quit)
                importExperimenters();
            if(!quit)
                importSubjects();
            if(!quit)
                importExperiments();
            if(!quit)
                importParticipantRecords();
            cleanUp();
        } catch(Exception any) {
            log.error("an error occurred importing data", any);
        }
        if(conn!=null){try{conn.close();}catch(Exception ignore){}}
        done = true;
        experimenterCache = null;
        experimentTypeCache = null;
        subjectCache = null;
        //experimentCache = null;
        subjectPoolCache = null;
    }
    
    private void cleanUp() throws Exception {
        List<Experiment> experiments = this.controller.experimentManager.getAll();
         for(Experiment experiment : experiments) {
             if(experiment.getStatus() == null)
                 this.controller.experimentManager.remove(experiment.getId());
         }
        
    }
    
    private void importExperimenters()
    throws Exception {
        
        experimenterCount = 0;
        experimenterErrorCount = 0;
        experimenters2ImportCount = getRecordCount("experimenters");
        log.debug("records to import: "+experimenters2ImportCount);
        
        String QUERY = "select id, email, fname, lname, passwd, phone, "+
                "school, privilege, valid, regdate from experimenters";
        Statement stmt = null;
        ResultSet rs = null;
        try {
            // get a list of existing experimenters
            List<Experimenter> experimenters = this.controller.extendedUserManager.getExperimenters();
            stmt = conn.createStatement();
            rs = stmt.executeQuery(QUERY);
            while(rs.next()) {
                Experimenter experimenter = new Experimenter();
                try {
                    long beginTime = new Date().getTime();
                    //experimenter.setId(rs.getLong("id")); // BRS 2011-02-08 removed - don't clobber existing experimenters
                    experimenter.setFirstName(rs.getString("fname"));
                    experimenter.setLastName(rs.getString("lname"));
                    String email = rs.getString("email");
                    experimenter.setEmail(email);
                    experimenter.setPhoneNumber(rs.getString("phone"));
                    experimenter.setAccountLocked(rs.getBoolean("valid"));
                    experimenter.setEnabled(!rs.getBoolean("valid"));
                    
                    experimenter.setPassword(DEFAULT_EXPERIMENTER_PASSWORD); // skip C2 password
                    experimenter.setAffiliation(rs.getString("school"));
                    
                    experimenter.setUsername(rs.getString("email"));
                    experimenter.addRole(this.controller.roleManager.getRole("ROLE_EXPERIMENTER"));
                    Date regDate = null;
                    try {
                        regDate = new Date(rs.getDate("regdate").getTime());
                    } catch(Exception dte) {
                        regDate = defaultRegDate;
                    }
                    
                    boolean exists = false;
                    for(Experimenter existing : experimenters) {
                        if(existing.getEmail().equalsIgnoreCase(email)) {
                            log.info("experimenter already exists (OK): " + existing.getEmail());
                            experimenter = existing; // BRS 2011-02-08
                            exists = true;
                            break;
                        }
                    }
                    if(!exists) {
                        // BRS 2011-02-08 if the experimenter already exists, do everything but save it in the DB
                        experimenter = (Experimenter) this.controller.extendedUserManager.saveUser((User)experimenter);
                        experimenterCount++; // and only count if actually saved.
                    }
                    experimenterCache.put(new Long(rs.getLong("id")), experimenter.getId());
                    experimenterCreationTime += new Date().getTime() - beginTime;
                    if(experimenterCount > 0) {
                        avgExperimenterCreationTime = experimenterCreationTime / experimenterCount;
                        BigDecimal numerator = new BigDecimal(
                                (double) ((experimenters2ImportCount - experimenterCount) * avgExperimenterCreationTime));
                        experimenters2ImportTimeLeft = numerator.divide(new BigDecimal((double) (1000 * 60)), 2, BigDecimal.ROUND_UP);
                    }
                    log.info("imported experimenter as id: "+experimenter.getId());
                } catch(Exception any) {
                    log.warn("index: "+experimenterCount+".  Failed to import an experimenter", any);
                    experimenterErrorCount++;
                }
                if(quit)
                    break;
            }
            experimenters2ImportTimeLeft = new BigDecimal("0");
        }
        finally {
            if(rs!=null){try{rs.close();}catch(Exception ignore){}}
            if(stmt!=null){try{stmt.close();}catch(Exception ignore){}}
            //if(conn!=null){try{conn.close();}catch(Exception ignore){}}
        }
    }
    
    private void importSubjects()
    throws Exception {
        
        subjectCount = 0;
        subjectErrorCount = 0;
        subjects2ImportCount = getRecordCount("subjects");
        
        String QUERY = "select id, school_id, uid, email, fname, lname, phone, "+
                "regdate, valid from subjects";
        Statement stmt = null;
        ResultSet rs = null;
        try {

            // get a list of existing subjects
            //List<Subject> subjects = this.controller.extendedUserManager.getSubjects();

            stmt = conn.createStatement();
            rs = stmt.executeQuery(QUERY);
            while(rs.next()) {
                Subject subject = new Subject();
                try {
                    long beginTime = new Date().getTime();
                    // subject.setId(rs.getLong("id")); // BRS 2011-02-08 - Don't clobber existing subjects
                    subject.setFirstName(rs.getString("fname"));
                    subject.setLastName(rs.getString("lname"));
                    String email = rs.getString("email");
                    subject.setEmail(email);
                    subject.setPhoneNumber(rs.getString("phone"));
                    subject.setAccountLocked(rs.getBoolean("valid"));
                    subject.setCredentialsExpired(false);
                    subject.setEnabled(!rs.getBoolean("valid"));
                    subject.setSubscribed(!rs.getBoolean("valid")); // BRS 2011-02-16

                    String uId = rs.getString("uid");
                    String passwd = DEFAULT_PASSWORD;
                    //if(!StringUtils.isBlank(uId))
                    //    passwd = uId;

                    // BRS: If uid appears to be a valid UAA ID (8 digits), use
                    // that for the password.  Otherwise, use the lower-case
                    // last name.
                    // Also, set the idType to "UA ID".
                    if (uId.matches("\\d\\d\\d\\d\\d\\d\\d\\d")) {
                        passwd = uId;
                        subject.setIdType(1);
                    } else {
                        if (uId.matches("\\d\\d\\d\\d\\d\\d\\d")) {
                            // If it's 7 digits, it's probably a driver's license number
                            subject.setIdType(4);
                        } // If it's not a UA ID or DL# (the only 2 options given in CW2) we don't know what it is, so leave null.
                        passwd = subject.getLastName().toLowerCase();
                    }


                    // Note: subject.school_id in cassel is the subject pool id,
                    // while subject.schoolId in recruiting is the individual's school ID number / driver's license number / etc.
                    
                    subject.setPassword(passwd); // skip C2 password
                    subject.setSchoolId(uId);
                    
                    subject.setUsername(rs.getString("email"));
                    subject.addRole(this.controller.roleManager.getRole("ROLE_SUBJECT"));
                    List<SubjectPool> subjectPools = new ArrayList();
                    int schoolId = rs.getInt("school_id");
                    subjectPools.add(getSubjectPool(conn, new Long(schoolId)));
                    subject.setPools(subjectPools);
                    Date regDate = null;
                    try {
                        regDate = new Date(rs.getDate("regdate").getTime());
                    } catch(Exception dte) {
                        regDate = defaultRegDate;
                    }
                    

                    /*
                    boolean exists = false;
                    log.debug("looping through subjects to check whether " + email + " exists.");
                    for(Subject existing : subjects) {
                        log.debug("checking " + existing.getEmail());
                        if(existing.getEmail().equalsIgnoreCase(email)) {
                            log.info("subject already exists (OK): " + existing.getEmail());
                            subject = existing; // BRS 2011-02-08
                            exists = true;
                            break;
                        }
                        log.debug("Nope, " + existing.getEmail() + " != " + email);
                    }
                    */

                    // BRS 2011-02-08 if the subject already exists, do everything but save it in the DB
                    // BRS 2011-11-01: check DB here instead of looping through list returned by getSubjects(), which excludes disabled subjects.
                    Subject existing = this.controller.extendedUserManager.getSubjectByEmail(subject.getEmail());
                    if (existing != null) {
                        log.info("subject already exists (OK): " + existing.getEmail());
                        subject = existing;
                    } else {
                        try {
                            subject = (Subject) this.controller.extendedUserManager.saveUser((User)subject);
                            subjectCount++;
                        } catch (Exception e) {
                            // Oops, something happened.  Could it be...
                            User u = this.controller.extendedUserManager.getNonSubjectByEmail(subject.getEmail());
                            if (u != null) {
                                // The subject already exists as a non-subject in the recruiting database.
                                // Append "_subject" to the email address and username to avoid a duplicate.
                                log.info("subject already exists as non-subject (OK - creating with changed email: " + u.getEmail() + "_subject)");
                                subject.setEmail(u.getEmail() + "_subject");
                                subject.setUsername(u.getEmail() + "_subject");
                                // Before saving, check whether a subject with the *_subject email already exists,
                                // and if so, don't save, just continue.
                                existing = this.controller.extendedUserManager.getSubjectByEmail(subject.getEmail());
                                if (existing != null) {
                                    log.info("renamed subject already exists (OK): " + existing.getEmail());
                                    subject = existing;
                                } else {
                                    subject = (Subject) this.controller.extendedUserManager.saveUser((User)subject);
                                    subjectCount++;
                                }
                            } else {
                                // Nope, don't know what happened.  Re-throw the exception.
                                throw e;
                            }
                        }
                    }
                    subjectCache.put(new Long(rs.getLong("id")), subject.getId());
                    subjectCreationTime += new Date().getTime() - beginTime;
                    if(subjectCount > 0) {
                        avgSubjectCreationTime = subjectCreationTime / subjectCount;
                        BigDecimal numerator = new BigDecimal(
                                (double) ((subjects2ImportCount - subjectCount) * avgSubjectCreationTime));
                        subjects2ImportTimeLeft = numerator.divide(new BigDecimal((double) (1000 * 60)), 2, BigDecimal.ROUND_UP);
                    }
                    log.info("imported subject as id: "+subject.getId());
                } catch(Exception any) {
                    log.warn("index: "+subjectCount+".  Failed to import a subject", any);
                    subjectErrorCount++;
                    quit = true;
                }
                if(quit)
                    break;
            }
            subjects2ImportTimeLeft = new BigDecimal("0");
        }
        finally {
            if(rs!=null){try{rs.close();}catch(Exception ignore){}}
            if(stmt!=null){try{stmt.close();}catch(Exception ignore){}}
            //if(conn!=null){try{conn.close();}catch(Exception ignore){}}
        }
    }
    
    private void importExperiments()
    throws Exception {

        // Warning: Because id numbers are preserved, existing experiments in MR will be clobbered.
        //
        // This method makes the following assumptions:
        // - The id field in both cassel.experiments and recruiting.experiment starts at 1 and doesn't skip (recruiting.experiment can be empty).
        // - The current value of the auto_increment in recruiting.experiment won't create a skip when a new record is inserted.
        // -- BRS 2011-02-10
        
        experimentCount = 0;
        experimentErrorCount = 0;
        experiments2ImportCount = getRecordCount("experiments");
        int count = 0;
        String QUERY = "select id, runby, reservedby, date, start_time, end_time, "+
                "min_subj_needed, max_subj_needed, showup_fee, exp_status, instruction, signup_freezeTime, "+
                "signup_delay, type_id from experiments";
        Statement stmt = null;
        ResultSet rs = null;
        try {
            log.debug("Creating experiment placeholders to preserve experiment ids...");
            stmt = conn.createStatement();
            rs = stmt.executeQuery(QUERY);
            long expNum = 0;
            rs.next();
            long firstNum = rs.getLong("id");
            Date fillerStartTime = new Date(rs.getTime("start_time").getTime());
            Date fillerEndTime = new Date(rs.getTime("end_time").getTime());
            while(rs.next()) {
                expNum = rs.getLong("id");
            }
            // For each experiment in cassel, create a new experiment in recruiting with the same id.
            for(int i=1; i<=expNum; i++) {
                Experiment experiment = new Experiment();
                experiment.setId(new Long(i)); // BRS 2010-02-09 - make sure we're not unnecessarily creating a new record where one exists with the desired id.
                experiment.setStartTime(fillerStartTime);
                experiment.setEndTime(fillerEndTime);
                experiment  = this.controller.experimentManager.save(experiment);
            }
        } catch(Exception any) {
            log.warn("index: "+experimentCount+".  Failed to import an experiment", any);
            experimentErrorCount++;
        }
        for(int i=0; i<2; i++){
            QUERY = "select id, runby, reservedby, date, start_time, end_time, "+
                "min_subj_needed, max_subj_needed, showup_fee, exp_status, instruction, signup_freezeTime, "+
                "signup_delay, type_id from experiments";
            if(i==0){
                QUERY +=  " where date<'2006-01-01'"; 
            }else{
                QUERY +=  " where date>='2006-01-01'"; 
            }
            stmt = null;
            rs = null;
            try {
                // get a list of existing experiments
                List<Experiment> experiments = this.controller.experimentManager.getAll();
                stmt = conn.createStatement();
                rs = stmt.executeQuery(QUERY);
                while(rs.next()) {
                    Experiment experiment = this.controller.experimentManager.getByIdString(String.valueOf(rs.getLong("id")));
                    count++;
                    try {
                        long beginTime = new Date().getTime();
                        int status = rs.getInt("exp_status");
                        experiment.setId(rs.getLong("id"));
                        experiment.setEndTime(new Date(rs.getTime("end_time").getTime()));
                        experiment.setExpDate(new Date(rs.getDate("date").getTime()));
                        experiment.setInstruction(rs.getString("instruction"));
                        experiment.setLocation(this.controller.locationManager.get(new Long(1))); // fake
                        experiment.setMaxNumSubjects(rs.getInt("max_subj_needed"));
                        experiment.setMinNumSubjects(rs.getInt("min_subj_needed"));
                        experiment.setPublished(true);
                        experiment.setRunby(getExperimenter(conn, experimenterCache.get(rs.getLong("runby"))));
                        //log.debug(experimenterCache.get(rs.getLong("runby")));
                        experiment.setReservedBy(getExperimenter(conn, experimenterCache.get(rs.getLong("reservedby"))));
                        experiment.setPrinciple(getExperimenter(conn, experimenterCache.get(rs.getLong("reservedby")))); // dupe
                        experiment.setStartTime(new Date(rs.getTime("start_time").getTime()));
                        experiment.setShowUpFee(rs.getFloat("showup_fee"));
                        //experiment.setStatus(Experiment.Status.IMPORTED);
                        experiment.setSignupFreezeTime(rs.getFloat("signup_freezeTime"));
                        experiment.setType(getExperimentType(conn, new Long(rs.getLong("type_id"))));
                        if(status == -1) {experiment.setStatus(Experiment.Status.CANCELLED);}
                        if(status == 0 || status == 1) {experiment.setStatus(Experiment.Status.OPEN);}
                        if(status == 2) {experiment.setStatus(Experiment.Status.FULL);}
                        if(status == 3) {experiment.setStatus(Experiment.Status.CANCELLED);}
                        else {experiment.setStatus(Experiment.Status.FINISHED);}


                        EqualsBuilder exists = null;
                        for(Experiment existing : experiments) {
                            exists = new EqualsBuilder()
                                .append(experiment.getExpDate(), existing.getExpDate())
                                .append(experiment.getStartTime(), existing.getStartTime())
                                .append(experiment.getEndTime(), existing.getEndTime())
                                .append(experiment.getType(), existing.getType())
                                .append(experiment.getInstruction(), existing.getInstruction());
                            if(exists.isEquals())
                                break;
                        }

                        // BRS 2011-11-01: Save the experiment whether it exists or not, since we are clobbering them.
                        // Moreover, the comparison above doesn't catch any existing experiments.
                        //if(exists != null && !exists.isEquals()) {
                        if (true) {
                            experiment  = this.controller.experimentManager.save(experiment);
                            //experimentCache.put(new Long(rs.getLong("id")), experiment);
                            experimentCount++;
                            expCreationTime += new Date().getTime() - beginTime;
                            if(experimentCount > 0) {
                                avgExpCreationTime = expCreationTime / experimentCount;
                                BigDecimal numerator = new BigDecimal(
                                        (double) ((experiments2ImportCount - experimentCount) * avgExpCreationTime));
                                experiments2ImportTimeLeft = numerator.divide(new BigDecimal((double) (1000 * 60)), 2, BigDecimal.ROUND_UP);
                            }
                            log.info("imported experiment as id: "+experiment.getId());
                        }

                        /*experiments = this.controller.experimentManager.getAll();
                          for (Experiment existing : experiments) {
                          if (existing.getStatus() == null) {
                          this.controller.experimentManager.remove(existing.getId());
                          }
                          }      */
                    } catch(Exception any) {
                        log.warn("index: "+experimentCount+" id: "+rs.getLong("id") /* +" real id: "+experimentCache.get(rs.getLong("id")) */ // BRS 2011-02-09
                                +".  Failed to import an experiment", any);
                        experimentErrorCount++;
                        any.printStackTrace();
                    }
                    if(quit)
                        break;
                }
                //if(rs!=null){try{rs.close();}catch(Exception ignore){}}
                experiments2ImportTimeLeft = new BigDecimal("0");
            }

            finally {
                if(rs!=null){try{rs.close();}catch(Exception ignore){}}
                if(stmt!=null){try{stmt.close();}catch(Exception ignore){}}
                //if(conn!=null){try{conn.close();}catch(Exception ignore){}}


            }
        }
    }
    
    private void importParticipantRecords()
    throws Exception {
        
        participantRecordCount = 0;
        participantRecordErrorCount = 0;
        participantRecords2ImportCount = getRecordCount("participants");
        
        String QUERY = "select exp_id, subj_id, playernum, payoff, participation_status "+
                "from participants";
        Statement stmt = null;
        ResultSet rs = null;
        try {
            // get a list of existing participantRecords
            List<ParticipantRecord> participantRecords = this.controller.participantRecordManager.getAll();
            stmt = conn.createStatement();
            rs = stmt.executeQuery(QUERY);
            while(rs.next()) {
                ParticipantRecord prec = new ParticipantRecord();
                try {
                    long beginTime = new Date().getTime();
                    prec.setEligible(true);
                    prec.setExp(getExperiment(conn, new Long(rs.getLong("exp_id"))));
                    prec.setParticipated((rs.getInt("participation_status") == 0 ? false : true));

                    // BRS 2011-02-16
                    if (rs.getInt("participation_status") == 1) {
                        prec.setStatus(Status.STATUS_2); // Participated
                    } else {
                        // Assume if they got paid they showed up, and if not they didn't
                        if (rs.getFloat("payoff") > 0) {
                            prec.setStatus(Status.STATUS_1); // Show-up
                        } else {
                            prec.setStatus(Status.STATUS_3); // Sign-up
                        }
                    }

                    prec.setPaymentMethod(ParticipantRecord.PaymentMethod.MANUAL);
                    prec.setPayoff(rs.getFloat("payoff"));
                    prec.setPlayerNum(rs.getInt("playernum"));
                    prec.setSubject(getSubject(conn, subjectCache.get(rs.getLong("subj_id"))));
                    
                    EqualsBuilder exists = null;
                    for(ParticipantRecord existing : participantRecords) {
                        exists = new EqualsBuilder()
                                .append(prec.getExp().getId(), existing.getExp().getId())
                                .append(prec.getSubject().getId(), existing.getSubject().getId());
                        if(exists.isEquals()) {
                            break;
                        }
                    }
                    if(exists != null && !exists.isEquals()) {
                        if(prec.getSubject() != null) {
                            prec = this.controller.participantRecordManager.save(prec);
                            participantRecordCount++;
                            precCreationTime += new Date().getTime() - beginTime;
                            if(participantRecordCount > 0) {
                                avgPrecCreationTime = precCreationTime / participantRecordCount;
                                BigDecimal numerator = new BigDecimal(
                                        (double) ((participantRecords2ImportCount - participantRecordCount) * avgPrecCreationTime));
                                participantRecords2ImportTimeLeft = numerator.divide(new BigDecimal((double) (1000 * 60)), 2, BigDecimal.ROUND_UP);
                            }
                            log.info("imported participantRecord as id: "+prec.getId());
                        }
                    }
                } catch(Exception any) {
                    log.warn("index: "+participantRecordCount+".  Failed to import a participantRecord", any);
                    participantRecordErrorCount++;
                }
                if(quit)
                    break;
            }
            participantRecords2ImportTimeLeft = new BigDecimal("0");
        }
        finally {
            if(rs!=null){try{rs.close();}catch(Exception ignore){}}
            if(stmt!=null){try{stmt.close();}catch(Exception ignore){}}
            //if(conn!=null){try{conn.close();}catch(Exception ignore){}}
        }
    }
    
    // Return the Experimenter with the given MR id. -BRS
    private Experimenter getExperimenter(Connection conn, Long id)
    throws Exception {
        
        //Experimenter experimenter = experimenterCache.get(id);
        Experimenter experimenter = this.controller.extendedUserManager.getExperimenterById(id);
        if(experimenter != null)
            return experimenter;

        log.warn("experimenter not found in MR database - getting from CW2 database (should never happen!) --BRS");
        // because getExperimenter() is not called until after importExperimenters(), so they should all be in MR.
        
        //log.debug("fetching Experimenter: casselweb2 id: "+id);
        String QUERY = "select email, fname, lname, passwd, phone, "+
                "school, privilege, valid, regdate from experimenters where id = ?";
        PreparedStatement stmt = null;
        ResultSet rs = null;
        if(id == null)
            id=new Long(-12412);
        try {
            stmt = conn.prepareStatement(QUERY);
            stmt.setLong(1, id);
            rs = stmt.executeQuery();
            if(rs.next()) {
                String email = rs.getString("email");
                String fname = rs.getString("fname");
                String lname = rs.getString("lname");
                String passwd = rs.getString("passwd");
                if(passwd == null)
                    passwd = DEFAULT_PASSWORD;
                String phone = rs.getString("phone");
                String school = rs.getString("school");
                int priv = rs.getInt("privilege");
                boolean valid = rs.getBoolean("valid");
                Date regDate = null;
                try {
                    regDate = new Date(rs.getDate("regdate").getTime());
                } catch(Exception dte) {
                    regDate = defaultRegDate;
                }
                
                List<Experimenter> experimenters = this.controller.extendedUserManager.getExperimenters();
                for(Experimenter existing : experimenters) {
                    // email not null and unique
                    if(existing.getEmail().equalsIgnoreCase(email))
                        return existing;
                }
                
                // create a new experimenter
                experimenter = new Experimenter();
                experimenter.setId(rs.getLong("id"));
                experimenter.setFirstName(fname);
                experimenter.setLastName(lname);
                experimenter.setEmail(email);
                experimenter.setPhoneNumber(phone);
                experimenter.setAccountLocked(valid);
                experimenter.setPassword(DEFAULT_PASSWORD);
                experimenter.setAffiliation(school);
                experimenter.setCredentialsExpired(false);
                experimenter.setEnabled(true);
                experimenter.setUsername(email);
                experimenter.addRole(this.controller.roleManager.getRole("ROLE_EXPERIMENTER"));
                
                experimenter = (Experimenter) this.controller.extendedUserManager.saveUser((User)experimenter);
                experimenterCache.put(id, experimenter.getId());
                experimenterCount++;
                experimenter = null;
            }
        } catch(Exception any) {
            experimentErrorCount++;
            throw any;
        }
        finally {
            if(rs!=null){try{rs.close();}catch(Exception ignore){}}
            if(stmt!=null){try{stmt.close();}catch(Exception ignore){}}
            //if(conn!=null){try{conn.close();}catch(Exception ignore){}}
        }
        if(experimenter == null)
            log.warn("entity (experimenter) not found (may not exist)!");
        return experimenter;
    }
    
    // Return the Subject with the given MR id. -BRS
    private Subject getSubject(Connection conn, Long id)
    throws Exception {
        
        //Subject subject = subjectCache.get(id);
        Subject subject = this.controller.extendedUserManager.getSubjectById(id);
        if(subject != null)
            return subject;

        log.warn("subject not found in MR database - getting from CW2 database (should never happen!) --BRS");
        // because getSubject() is not called until after importSubjects(), so they should all be in MR.
        
        //log.debug("fetching Subject: casselweb2 id: "+id);
        String QUERY = "select school_id, email, fname, lname, phone, passwd, "+
                "regdate, valid from subjects where id = ?";
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            stmt = conn.prepareStatement(QUERY);
            stmt.setLong(1, id);
            rs = stmt.executeQuery();
            if(rs.next()) {
                long schoolId = rs.getLong("school_id");
                String email = rs.getString("email");
                String fname = rs.getString("fname");
                String lname = rs.getString("lname");
                String phone = rs.getString("phone");
                String passwd = rs.getString("passwd");
                if(passwd == null)
                    passwd = DEFAULT_PASSWORD;
                boolean valid = rs.getBoolean("valid");
                Date regDate = null;
                try {
                    regDate = new Date(rs.getDate("regdate").getTime());
                } catch(Exception dte) {
                    regDate = defaultRegDate;
                }
                
                List<Subject> subjects = this.controller.extendedUserManager.getSubjects();
                for(Subject existing : subjects) {
                    // email not null and unique
                    if(existing.getEmail().equalsIgnoreCase(email))
                        return existing;
                }
                
                // create a new subject
                subject = new Subject();
                subject.setId(rs.getLong("id"));
                subject.setFirstName(fname);
                subject.setLastName(lname);
                subject.setEmail(email);
                subject.setPhoneNumber(phone);
                subject.setAccountLocked(valid);
                subject.setPassword(String.valueOf(schoolId));
                subject.setSchoolId(String.valueOf(schoolId));
                subject.setCredentialsExpired(false);
                subject.setEnabled(true);
                subject.setUsername(email);
                subject.addRole(this.controller.roleManager.getRole("ROLE_SUBJECT"));
                List<SubjectPool> subjectPools = new ArrayList();
                subjectPools.add(getSubjectPool(conn, new Long(schoolId)));
                subject.setPools(subjectPools);
                
                subject = (Subject) this.controller.extendedUserManager.saveUser((User)subject);
                subjectCache.put(id, subject.getId());
                subjectCount++;
                subject = null;
            }
        } catch(Exception any) {
            subjectErrorCount++;
            throw any;
        }
        finally {
            if(rs!=null){try{rs.close();}catch(Exception ignore){}}
            if(stmt!=null){try{stmt.close();}catch(Exception ignore){}}
            //if(conn!=null){try{conn.close();}catch(Exception ignore){}}
        }
        if(subject == null)
            log.warn("entity (subject) not found (may not exist)!");
        return subject;
    }
    
    // Return the Experiment with the given MR id. -BRS
    private Experiment getExperiment(Connection conn, Long id)
    throws Exception {
        
        //Experiment experiment = experimentCache.get(id);
        Experiment experiment = this.controller.experimentManager.get(id);
        if(experiment != null)
            return experiment;

        log.warn("experiment not found in MR database - getting from CW2 database (should never happen!) --BRS");
        // because getExperiment() is not called until after importExperiments(), so they should all be in MR.
        
        //log.debug("fetching Experiment: casselweb2 id: "+id);
        String QUERY = "select runby, reservedby, date, start_time, end_time, "+
                "min_subj_needed, max_subj_needed, showup_fee, exp_status, instruction, signup_freezeTime, "+
                "signup_delay, type_id from experiments where id = ?";
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            stmt = conn.prepareStatement(QUERY);
            stmt.setLong(1, id);
            rs = stmt.executeQuery();
            if(rs.next()) {
                Long runbyId = new Long(rs.getLong("runby"));
                Long reservedById = new Long(rs.getLong("reservedby"));
                Date expDate = new Date(rs.getDate("date").getTime());
                Date startTime = new Date(rs.getTime("start_time").getTime());
                Date endTime = new Date(rs.getTime("end_time").getTime());
                int maxNumSubjects = rs.getInt("max_subj_needed");
                int minNumSubjects = rs.getInt("min_subj_needed");
                float showUpFee = rs.getFloat("showup_fee");
                //Experiment.Status status = Experiment.Status.IMPORTED;
                int status = rs.getInt("exp_status");
                String instruction = rs.getString("instruction");
                float signupFreezeTime = rs.getFloat("signup_freezeTime");
                ExperimentType type = getExperimentType(conn, new Long(rs.getLong("type_id")));
                
                List<Experiment> experiments = this.controller.experimentManager.getByDate(expDate);
                for(Experiment existing : experiments) {
                    EqualsBuilder exists = new EqualsBuilder()
                            .append(expDate, existing.getExpDate())
                            .append(startTime, existing.getStartTime())
                            .append(endTime, existing.getEndTime())
                            .append(type, existing.getType())
                            .append(instruction, existing.getInstruction());
                    if(exists.isEquals())
                        return existing;
                }
                
                // create a new experiment
                experiment = new Experiment();
                experiment.setEndTime(endTime);
                experiment.setExpDate(expDate);
                experiment.setInstruction(instruction);
                experiment.setMaxNumSubjects(maxNumSubjects);
                experiment.setMinNumSubjects(minNumSubjects);
                experiment.setPublished(true);
                experiment.setReservedBy(getExperimenter(conn, experimenterCache.get(reservedById)));
                experiment.setRunby(getExperimenter(conn, experimenterCache.get(runbyId)));
                experiment.setShowUpFee(showUpFee);
                experiment.setSignupFreezeTime(signupFreezeTime);
                experiment.setStartTime(startTime);
                //experiment.setStatus(status);
                experiment.setType(type);
                if(status == -1) {experiment.setStatus(Experiment.Status.CANCELLED);}
                if(status == 0 || status == 1) {experiment.setStatus(Experiment.Status.OPEN);}
                if(status == 2) {experiment.setStatus(Experiment.Status.FULL);}
                if(status == 3) {experiment.setStatus(Experiment.Status.CANCELLED);}
                else {experiment.setStatus(Experiment.Status.FINISHED);}
                
                experiment = this.controller.experimentManager.save(experiment);
                //experimentCache.put(id, experiment);
                experimentCount++;
                experiment = null;
            }
        } catch(Exception any) {
            experimentErrorCount++;
            throw any;
        }
        finally {
            if(rs!=null){try{rs.close();}catch(Exception ignore){}}
            if(stmt!=null){try{stmt.close();}catch(Exception ignore){}}
            //if(conn!=null){try{conn.close();}catch(Exception ignore){}}
        }
        if(experiment == null)
            log.warn("entity (experiment) not found (may not exist)!");
        return experiment;
    }
    
    // Return the ExperimentType corresponding to the given CW2 id. -BRS
    private ExperimentType getExperimentType(Connection conn, Long id)
    throws Exception {
        
        ExperimentType experimentType = experimentTypeCache.get(id); // This is the only place experimentTypeCache.get() is called. -BRS
        //ExperimentType experimentType = this.controller.experimentTypeManager.getById(id);
        if(experimentType != null)
            return experimentType;
        
        //log.debug("fetching ExperimentType: casselweb2 id: "+id);
        String QUERY = "select exp_name, description, instruction, "+
                "regdate, valid from experiment_types where id = ?";
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            stmt = conn.prepareStatement(QUERY);
            stmt.setLong(1, id);
            rs = stmt.executeQuery();
            if(rs.next()) {
                String name = rs.getString("exp_name");
                String descr = rs.getString("description");
                String instruction = rs.getString("instruction");
                Date regDate = null;
                try {
                    regDate = new Date(rs.getDate("regdate").getTime());
                } catch(Exception dte) {
                    regDate = defaultRegDate;
                }
                boolean valid = rs.getBoolean("valid");
                
                List<ExperimentType> experimentTypes = this.controller.experimentTypeManager.getAll();
                for(ExperimentType existing : experimentTypes) {
                    if((existing.getName() == null && name == null) ||
                            existing.getName().equals(name))
                        return existing;
                }
                
                // create a new experimentType
                experimentType = new ExperimentType();
                experimentType.setName(name);
                experimentType.setDescr(descr);
                experimentType.setInstruction(instruction);
                experimentType.setRegDate(regDate);
                experimentType.setValid(valid);
                
                experimentType = this.controller.experimentTypeManager.save(experimentType);
                experimentTypeCache.put(id, experimentType);
                experimentTypeCount++;
            }
        } catch(Exception any) {
            experimentTypeErrorCount++;
            throw any;
        }
        finally {
            if(rs!=null){try{rs.close();}catch(Exception ignore){}}
            if(stmt!=null){try{stmt.close();}catch(Exception ignore){}}
            //if(conn!=null){try{conn.close();}catch(Exception ignore){}}
        }
        if(experimentType == null)
            log.warn("entity (experimentType) not found (may not exist)!");
        return experimentType;
    }
    
    // Return the SubjectPool corresponding to the given CW2 id. -BRS
    private SubjectPool getSubjectPool(Connection conn, Long id)
    throws Exception {
        
        SubjectPool subjectPool = subjectPoolCache.get(id); // This is the only place subjectPoolCache.get() is called. -BRS
        //SubjectPool subjectPool = this.controller.subjectPoolManager.getById(id);
        if(subjectPool != null)
            return subjectPool;
        
        //log.debug("fetching SubjectPool: casselweb2 id: "+id);
        String QUERY = "select pool_name, consent_info, comments, "+
                "valid, regdate from pools where id = ?";
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            stmt = conn.prepareStatement(QUERY);
            stmt.setLong(1, id);
            rs = stmt.executeQuery();
            if(rs.next()) {
                String name = rs.getString("pool_name");
                String consentInfo = rs.getString("consent_info");
                String descr = rs.getString("comments");
                Date regDate = null;
                try {
                    regDate = new Date(rs.getDate("regdate").getTime());
                } catch(Exception dte) {
                    regDate = defaultRegDate;
                }
                boolean valid = rs.getBoolean("valid");
                
                List<SubjectPool> subjectPools = this.controller.subjectPoolManager.getAll();
                for(SubjectPool existing : subjectPools) {
                    if((existing.getName() == null && name == null) ||
                            existing.getName().equals(name))
                        return existing;
                }
                
                // create a new subjectPool
                subjectPool = new SubjectPool();
                subjectPool.setName(name);
                subjectPool.setConsentInfo(consentInfo);
                subjectPool.setDescr(descr);
                subjectPool.setRegDate(regDate);
                subjectPool.setValid(valid);
                
                subjectPool = this.controller.subjectPoolManager.save(subjectPool);
                subjectPoolCache.put(id, subjectPool);
                subjectPoolCount++;
            }
        } catch(Exception any) {
            subjectPoolErrorCount++;
            throw any;
        }
        finally {
            if(rs!=null){try{rs.close();}catch(Exception ignore){}}
            if(stmt!=null){try{stmt.close();}catch(Exception ignore){}}
            //if(conn!=null){try{conn.close();}catch(Exception ignore){}}
        }
        if(subjectPool == null)
            log.warn("entity (subjectPool) not found (may not exist)!");
        return subjectPool;
    }
    
    private void setConnection(
            String hostname,
            String port,
            String c2name,
            String username,
            String password)
    throws ClassNotFoundException, SQLException {
        
        String url = null;
        Class.forName(JDBC_DRIVER);
        if(StringUtils.isEmpty(port))
            url = "jdbc:mysql://"+hostname+
                    "/"+c2name+
                    "?user="+username+
                    "&password="+password;
        else
            url = "jdbc:mysql://"+hostname+
                    ":"+port+
                    "/"+c2name+
                    "?user="+username+
                    "&password="+password;
        conn = DriverManager.getConnection(url);
    }

    public int getExperimentCount() {
        return experimentCount;
    }

    public int getExperimentTypeCount() {
        return experimentTypeCount;
    }

    public int getParticipantRecordCount() {
        return participantRecordCount;
    }

    public int getExperimentErrorCount() {
        return experimentErrorCount;
    }

    public int getExperimentTypeErrorCount() {
        return experimentTypeErrorCount;
    }

    public int getParticipantRecordErrorCount() {
        return participantRecordErrorCount;
    }

    public int getExperimenterCount() {
        return experimenterCount;
    }

    public int getExperimenterErrorCount() {
        return experimenterErrorCount;
    }

    public int getSubjectCount() {
        return subjectCount;
    }

    public int getSubjectErrorCount() {
        return subjectErrorCount;
    }

    public void setController(ImportC2FormController controller) {
        this.controller = controller;
    }

    public boolean isQuit() {
        return quit;
    }

    public void setQuit(boolean quit) {
        this.quit = quit;
    }

    public boolean isRan() {
        return ran;
    }

    public boolean isDone() {
        return done;
    }

    public String getConnErrMsg() {
        return connErrMsg;
    }

    public int getAvgExpCreationTime() {
        return avgExpCreationTime;
    }

    public int getAvgPrecCreationTime() {
        return avgPrecCreationTime;
    }

    public int getAvgExperimenterCreationTime() {
        return avgExperimenterCreationTime;
    }

    public int getAvgSubjectCreationTime() {
        return avgSubjectCreationTime;
    }

    public BigDecimal getSubjects2ImportTimeLeft() {
        return subjects2ImportTimeLeft;
    }

    public BigDecimal getExperimenters2ImportTimeLeft() {
        return experimenters2ImportTimeLeft;
    }

    public BigDecimal getParticipantRecords2ImportTimeLeft() {
        return participantRecords2ImportTimeLeft;
    }

    public BigDecimal getExperiments2ImportTimeLeft() {
        return experiments2ImportTimeLeft;
    }

    private int getRecordCount(String table) {
        
        int recordCount = 0;
        Statement stmt = null;
        ResultSet rs = null;
        String query = "select count(*) from "+table;
        try {
            stmt = conn.createStatement();
            rs = stmt.executeQuery(query);
            if(rs.next())
                recordCount = rs.getInt(1);
        } catch(Exception any) {
            log.error("whoops: "+any.getMessage());
        }
        finally {
            if(rs!=null){try{rs.close();}catch(Exception ignore){}}
            if(stmt!=null){try{stmt.close();}catch(Exception ignore){}}
            //if(conn!=null){try{conn.close();}catch(Exception ignore){}}
        }
        return recordCount;
    }
}
