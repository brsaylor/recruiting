/*
 * Copyright (C) 1998-2009, <a href="http://www.ssel.caltech.edu">SSEL</a>,
 * at Caltech, Pasadena, CA 91125. 
 *
 * MooreRecruiting is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 * USA.
 *
 */

package edu.caltech.ssel.recruiting.webapp.decorator;
import edu.caltech.ssel.recruiting.model.Experiment;
import edu.caltech.ssel.recruiting.model.Experiment.Status;
import edu.caltech.ssel.recruiting.service.ExperimentManager;

// BRS
import org.appfuse.service.UserManager;
import edu.caltech.ssel.recruiting.service.ConditionManager;
import edu.caltech.ssel.recruiting.model.Subject;
import javax.servlet.http.HttpServletRequest;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.calendartag.decorator.*;

/**
 * @author Michael Kolodrubetz, SSEL, Caltech
 * @author <a href="mailto:wmyuan@hss.caltech.edu">Walter M. Yuan</a>, SSEL, Caltech
 */
public class RecruitingCalendarDecorator extends DefaultCalendarDecorator {
    
    private ExperimentManager manager = null;

    // BRS
    private UserManager userManager;
    private ConditionManager conditionManager;
    
    private Log log = LogFactory.getLog(RecruitingCalendarDecorator.class);
    
    private boolean sameDay(Date a, Date b) {
        return (a.getDate() == b.getDate() &&
                a.getMonth() == b.getMonth() &&
                a.getYear() == b.getYear());
    }
    
    public void initializeCalendar() {
        manager = (ExperimentManager) pageContext.getRequest().getAttribute("experimentManager");
        if(manager == null)
            log.error("Manager is null...");

        // BRS
        userManager = (UserManager) pageContext.getRequest().getAttribute("userManager");
        conditionManager = (ConditionManager) pageContext.getRequest().getAttribute("conditionManager");
    }
    
    public String getWeekdayTitle(int day) {
        switch(day) {
            case 1:
                return "Sun";
            case 2:
                return "Mon";
            case 3:
                return "Tue";
            case 4:
                return "Wed";
            case 5:
                return "Thr";
            case 6:
                return "Fri";
            case 7:
                return "Sat";
            default:
                return null;
        }
    }
    
    //We don't care which day is selected, just whether or not it is today
    public String getDayStyleClass(boolean isOddMonth, boolean isSelectedDay) {
        Date d = calendar.getTime();
        Date today = new Date();
        List<Experiment> lst = manager.getByDate(d);
        boolean isToday = sameDay(today, d);
        Boolean b = (Boolean)pageContext.getRequest().getAttribute("isExperimenter");
        Boolean b2 = (Boolean)pageContext.getRequest().getAttribute("isAdmin");
        boolean hasAuthority = (b != null && b) || (b2 != null && b2);
        
        if(lst == null || lst.size() == 0) {
            if(isToday)
                return "calendarActiveDayNoExperimentStyle";
            
            return "calendarNoExperimentStyle";
        }
        for(Experiment e : lst) {
            if((e.getPublished() && !e.getStatus().equals(Status.CANCELLED)  &&  !e.isInvisible()) || hasAuthority) {
                if(isToday)
                    return "calendarActiveDayExperimentStyle";

                return "calendarExperimentStyle";
            }
        }
        if(isToday)
            return "calendarActiveDayNoExperimentStyle";

        return "calendarNoExperimentStyle";
    }
    
    public String getCalendarTitle() {
        return "<h2 align=\"center\">" + super.getCalendarTitle() + "</h2>";//"<h1 align=\"center\">Experiment Calendar</h1><h2 align=\"center\">" + super.getCalendarTitle() + "</h2>";
    }
    
    public String getNextLink(String url) {
        return "<div align=\"right\">" + super.getNextLink(url) + "</div>";
    }
    
    public String getDay(String url) {
        String result = "<div class=\"calendarDateStyle\">" + Integer.toString(calendar.get(Calendar.DATE)) + " </div>";
        
        GregorianCalendar now = new GregorianCalendar();
        List<Experiment> lst = manager.getByDate(calendar.getTime());
        Experiment e;
        Boolean b = (Boolean)pageContext.getRequest().getAttribute("isExperimenter");
        Boolean b2 = (Boolean)pageContext.getRequest().getAttribute("isAdmin");
        Boolean isSubject = (Boolean)pageContext.getRequest().getAttribute("isSubject");
        boolean hasAuthority = (b != null && b) || (b2 != null && b2);

        // BRS
        String username = ((HttpServletRequest)pageContext.getRequest()).getRemoteUser();
        boolean logged_in = (username != null);
        Subject subject = null;
        if (isSubject) {
            subject = (Subject) userManager.getUserByUsername(username);
        }

        if(lst != null) {
            result += "<br>";
            SimpleDateFormat sdf = new SimpleDateFormat();
            sdf.applyPattern("HH:mm");
            for(int j = 0; j < lst.size(); j++) {
                e = lst.get(j);
                
                //If the experiment is open but should be closed, close it (need to check on other status options)
                if(e.getStatus().equals(Status.OPEN)
                        && e.getFreezeTime() != null // BRS 2012-04-04
                        && e.getFreezeTime().before(now)) {
                    e.setStatus(Status.CLOSED);
                    e = manager.save(e);
                }

                // BRS
                // If the current user is a Subject, determine eligibility for this experiment,
                // and add "ineligible" class if not eligible.
                String ineligibleClass = "";
                boolean eligible = true;
                try {
                    if (isSubject && !conditionManager.isEligible(e.getFilter(), subject)) {
                        eligible = false;
                        ineligibleClass = "ineligible";
                    }
                } catch (java.lang.Exception ex) {
                    // isEligible may throw an Exception (don't know why)
                    log.error("Error determining eligibility");
                }
                
                // BRS modified - don't link if ineligible or not logged in
                boolean linkExpt =
                        logged_in && (
                            isSubject && eligible && (
                                 (e.getPublished() && e.getStatus().equals(Status.OPEN))
                              || (e.getStatus().equals(Status.FINISHED))
                            )
                            || hasAuthority
                        );
                boolean showExpt = ((e.getPublished() && !e.getStatus().equals(Status.CANCELLED) && !e.isInvisible()) 
                        || e.getStatus().equals(Status.FINISHED) || hasAuthority);
                //Display a link for the user to click on to get to experiment
                if(linkExpt) {

                    // BRS: for subjects, link to new experimentinfo page; otherwise, link directly to onexpsel
                    String linkUrl = hasAuthority ? "onexpsel.html?expId="+e.getId() : "experimentinfo.html?id="+e.getId();

                    if(e.isInvisible())
                        result += "<a class=\"invisible\" href=\""+linkUrl+"\" ";
                    else if(e.getPublished() && hasAuthority)
                        result += "<a class=\"published\" href=\""+linkUrl+"\" ";
                    else if(e.getStatus().equals(Status.FINISHED))
                        result += "<a href=\""+linkUrl+"\" ";
                    else if(e.getPublished())
                        result += "<a href=\""+linkUrl+"\" ";
                    else
                        result += "<a class=\"saved\" href=\""+linkUrl+"\" ";
                    /*if(hasAuthority) {
                        result += "onmouseover=\"experimentManagerDWR.getByIdString("+e.getId()+", exptooltip);\" ";
                        result += "onmouseout=\"return nd();\"";
                    }*/
                    result += ">";
                }
                if(showExpt) {
                    result += "<div class=\""+ineligibleClass+"\">"; // BRS
                    result += ("ID: " + e.getId());
                    result += "<br>";
                    if (e.getStartTime() != null) { // BRS 2012-04-04 don't crash if startTime is null
                        result += ("Time: " + sdf.getTimeInstance(SimpleDateFormat.SHORT).format(e.getStartTime()) + '-'
                                + sdf.getTimeInstance(SimpleDateFormat.SHORT).format(e.getEndTime()));
                    }
                    result += "<br>";
                    result += "Location: " + e.getLocation().getName();
                    result += "<br>";
                    result += "Status: " + e.getStatus().toString();
                    result += "</div>"; // BRS
                }
                if(linkExpt)
                    result += "</a>";
                if(showExpt && j != lst.size() - 1)
                    result += "<br> -------- <br>"; //Horizontal rule doesn't work yet
                
            }
        }
        
        return result;
    }
    
    private boolean afterByDay(Date a, Date b) {
        return b.after(a) && !sameDay(a, b);
    }
    
    public void setExperimentManager(ExperimentManager manager) {
        this.manager = manager;
    }
}
