/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package edu.caltech.ssel.recruiting.webapp.controller;

import edu.caltech.ssel.recruiting.model.Experiment;
import edu.caltech.ssel.recruiting.model.ParticipantRecord;
import edu.caltech.ssel.recruiting.model.Question;
import edu.caltech.ssel.recruiting.model.QuestionResult;
import edu.caltech.ssel.recruiting.model.Subject;
import edu.caltech.ssel.recruiting.service.ExperimentManager;
import edu.caltech.ssel.recruiting.service.ExtendedUserManager;
import edu.caltech.ssel.recruiting.service.QuestionManager;
import edu.caltech.ssel.recruiting.service.QuestionResultManager;
import edu.caltech.ssel.recruiting.service.ParticipantRecordManager;

import java.util.List;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 *
 * @author Richard Ho
 */
public class QuestionResultController implements Controller {
    private QuestionManager questionManager;
    private QuestionResultManager questionResultManager;
    private ExtendedUserManager extendedUserManager;

    
    private Log log = LogFactory.getLog(QuestionResultController.class);
    
    public QuestionResultController() {
    }

    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
        ModelAndView mav = new ModelAndView();
        List<QuestionResult> answers = new ArrayList();
        String experimentId = request.getParameter("eid");
        String questionId = request.getParameter("qid");
        log.debug(experimentId+";"+questionId);
        Question question = questionManager.getByIdString(questionId);
        log.debug(question.getOptionList());
        
        answers = questionResultManager.getAnswersByexperimentIdAndQuestionId(Long.parseLong(experimentId), Long.parseLong(questionId));
        if(answers != null) {
            log.debug(answers.size());
            //for (int i = 0; i < answers.size(); i++) {
            //    log.debug(answers.get(i).toString());
            //}
        }
        mav.addObject("questionResultList",answers);
        mav.addObject("question",question);
        return mav;
    }
    
    public QuestionResultManager getQuestionResultManager() {
        return questionResultManager;
    }
    
    public void setQuestionResultManager(QuestionResultManager questionResultManager) {
        this.questionResultManager = questionResultManager;
    }
    
    public ExtendedUserManager getExtendedUserManager() {
        return extendedUserManager;
    }
    
    public void setExtendedUserManager(ExtendedUserManager extendedUserManager) {
        this.extendedUserManager = extendedUserManager;
    }
    
    public QuestionManager getQuestionManager() {
        return questionManager;
    }
    
    public void setQuestionManager(QuestionManager questionManager) {
        this.questionManager = questionManager;
    }

}
