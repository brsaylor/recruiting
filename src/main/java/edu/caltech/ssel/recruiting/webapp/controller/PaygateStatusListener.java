/*
 * Copyright (C) 1998-2009, <a href="http://www.ssel.caltech.edu">SSEL</a>,
 * at Caltech, Pasadena, CA 91125. 
 *
 * MooreRecruiting is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 * USA.
 *
 */

package edu.caltech.ssel.recruiting.webapp.controller;

import edu.caltech.ssel.recruiting.model.Payment;
import edu.caltech.ssel.recruiting.model.Payment.Status;
import edu.caltech.ssel.recruiting.service.PaymentManager;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;
import java.text.SimpleDateFormat;

/**
 *
 */
public class PaygateStatusListener implements Controller {
    
    private static final Log log = LogFactory.getLog(PaygateStatusListener.class);
    private PaymentManager paymentManager;
    
    private static final String SAX_DRIVER_CLASS =
    "org.apache.xerces.parsers.SAXParser";
    
    private static final SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd:HH:mm:ss");
    
    /** Creates a new instance of PaygateStatusListener */
    public PaygateStatusListener() {
    }

    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
        
        if(log.isDebugEnabled())
            log.debug("received request");
        
        String accountInfo = null;
        String transactionId = null;
        String status = null;
        String attemptDate = null;
        
        try {
            SAXBuilder builder = new SAXBuilder(SAX_DRIVER_CLASS);
            int contentLength = request.getContentLength();
            String contentType = request.getContentType();
            if(log.isDebugEnabled())
                log.debug("Content-type: ["+contentType+"], Content-length: ["+contentLength+"]");
            
            Document doc = builder.build(request.getReader());
            Element root = doc.getRootElement();
            
            accountInfo = root.getChildText("accountInfo");
            transactionId = root.getChildText("transactionId");
            status = root.getChildText("status");
            attemptDate = root.getChildText("attemptDate");
            
            if(log.isDebugEnabled()) {
                log.debug("accountInfo: " + accountInfo +
                        ", transactionId: " + transactionId +
                        ", status: " + status +
                        ", attemptDate: " + attemptDate);
            }
            
            Payment payment = paymentManager.get(new Long(transactionId));
            payment.setAccountInfo(accountInfo);
            payment.setStatusString(status);
            payment.setWhenUpdated(formatter.parse(attemptDate));
            paymentManager.save(payment);
            
        } catch(Exception any) {
            log.info("paygatestatuslistener error: " +
                    any.getMessage());
            return null;
        }
        
        return null;
    }    

    public void setPaymentManager(PaymentManager paymentManager) {
        this.paymentManager = paymentManager;
    }

}
