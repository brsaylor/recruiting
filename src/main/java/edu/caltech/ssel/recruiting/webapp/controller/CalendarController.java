/*
 * Copyright (C) 1998-2009, <a href="http://www.ssel.caltech.edu">SSEL</a>,
 * at Caltech, Pasadena, CA 91125. 
 *
 * MooreRecruiting is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 * USA.
 *
 */

package edu.caltech.ssel.recruiting.webapp.controller;

import edu.caltech.ssel.recruiting.common.RecruitingConstants;
import edu.caltech.ssel.recruiting.service.ExperimentManager;

// BRS
import org.appfuse.model.User;
import edu.caltech.ssel.recruiting.model.Subject;
import org.appfuse.service.UserManager;
import edu.caltech.ssel.recruiting.service.ConditionManager;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.appfuse.model.LabelValue;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

/**
* @author Michael Kolodrubetz, SSEL, Caltech
* @author Richard Ho, CASSEL, UCLA
* @author <a href="mailto:wmyuan@hss.caltech.edu">Walter M. Yuan</a>, SSEL, Caltech
*/
public class CalendarController implements Controller {

    private ExperimentManager experimentManager;

    // BRS
    private UserManager userManager;
    private ConditionManager conditionManager;
    private String whatsNewView;
    
    private String myparse(Date d) {
        String s = "";
        s += d.getMonth();
        s += "/";
        s += d.getDate();
        s += "/";
        s += d.getYear();
        return s;
    }
    
    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
        Boolean isSubject = new Boolean(request.isUserInRole(RecruitingConstants.SUBJ_ROLE));

        request.setAttribute("experimentManager", experimentManager);
        request.setAttribute("isSubject", isSubject);
        request.setAttribute("isExperimenter", new Boolean(request.isUserInRole(RecruitingConstants.EXPTR_ROLE)));
        request.setAttribute("isAdmin", new Boolean(request.isUserInRole(RecruitingConstants.ADMIN_ROLE)));

        // BRS
        request.setAttribute("userManager", userManager);
        request.setAttribute("conditionManager", conditionManager);

        // BRS 2012-01-27, 2012-02-01
        boolean redirectToWhatsNew = false;
        if (request.getRemoteUser() != null) {
            User user = userManager.getUserByUsername(request.getRemoteUser());
            if (isSubject
                    && ((Subject)user).getLastLoginDate() == null
                    && ((Subject)user).getSignedUp() == null) {
                // This is a subject from CasselWeb2 who has just logged in for the first time.
                redirectToWhatsNew = true;
            }
            //  record login date+time
            if (request.getSession().getAttribute("lastLoginDateSet") == null) {
                user.setLastLoginDate(new Date());
                userManager.saveUser(user);
                request.getSession().setAttribute("lastLoginDateSet", new Boolean(true));
            }
        }
        if (redirectToWhatsNew) {
            return new ModelAndView(getWhatsNewView());
        }
        
        Date today = new Date();
        List monthList = new ArrayList();
        List yearList = new ArrayList();
        int firstExpYear;

        //match up each month with it's respective number
        monthList.add(new LabelValue("January","01"));
        monthList.add(new LabelValue("February","02"));
        monthList.add(new LabelValue("March","03"));
        monthList.add(new LabelValue("April","04"));
        monthList.add(new LabelValue("May","05"));
        monthList.add(new LabelValue("June","06"));
        monthList.add(new LabelValue("July","07"));
        monthList.add(new LabelValue("August","08"));
        monthList.add(new LabelValue("September","09"));
        monthList.add(new LabelValue("October","10"));
        monthList.add(new LabelValue("November","11"));
        monthList.add(new LabelValue("December","12"));
        request.setAttribute("monthList",monthList);
        

        if(request.getSession().getAttribute("firstExpYear") == null)
            firstExpYear = experimentManager.getFirstExperimentDate().getYear()+1900;
        else
            firstExpYear = (Integer)request.getSession().getAttribute("firstExpYear");
        request.getSession().setAttribute("firstExpYear", firstExpYear);

        //getYear is deprecated but it counts the number of years after 1900
        //yearList contains years from the first experiment until the current year
        if(request.getSession().getAttribute("yearList") == null) {
            for(int year=experimentManager.getFirstExperimentDate().getYear()+1900;year<=1900+today.getYear()+1;year++)
                yearList.add(Integer.toString(year));
            request.getSession().setAttribute("yearList", yearList);
        }
        
        //Format the month with MMMMMMMMMMM to fit all month names
        String selectedMonth = new SimpleDateFormat("MMMMMMMMMMM").format(today);
        selectedMonth = selectedMonth.trim(); //trim leading and ending white spaces
        String selectedYear = Integer.toString(1900+today.getYear());
        if(request.getParameter("date") != null) {
            String date = request.getParameter("date");
            selectedMonth = ((LabelValue)monthList.get(Integer.parseInt(date.substring(0,2))-1)).getLabel();
            selectedYear = date.substring(6,10);
        }
        request.setAttribute("selectedMonth",selectedMonth);
        request.setAttribute("selectedYear",selectedYear);

        return new ModelAndView();
    }

    public void setExperimentManager(ExperimentManager experimentManager) {
        this.experimentManager = experimentManager;
    }

    //BRS
    public void setUserManager(UserManager userManager) {
        this.userManager = userManager;
    }
    public void setConditionManager(ConditionManager conditionManager) {
        this.conditionManager = conditionManager;
    }
    public String getWhatsNewView() {
        return whatsNewView;
    }
    public void setWhatsNewView(String whatsNewView) {
        this.whatsNewView = whatsNewView;
    }
    
}
