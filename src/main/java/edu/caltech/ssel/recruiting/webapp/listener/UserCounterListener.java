/*
 * BRS 2012-02-13
 * Based on org.appfuse.webapp.listener.UserCounterListener from AppFuse SVN
 * Fixed import statements to work with older version of Spring Security.
 * Original did not successfully remove users upon logout (not recognizing the User
 * logging out as the same as the one added to the Set upon login).
 * Uses Map<Long,User> (id->user) instead of Set<User>, so we can always match
 * user by ID and remove on logout.
 * Removed redundant counter.
 */


//package org.appfuse.webapp.listener;
package edu.caltech.ssel.recruiting.webapp.listener;

import org.appfuse.model.User;
import org.springframework.security.AuthenticationTrustResolver;
import org.springframework.security.AuthenticationTrustResolverImpl;
import org.springframework.security.Authentication;
import org.springframework.security.context.SecurityContext;
import org.springframework.security.context.SecurityContextHolder;
import org.springframework.security.context.HttpSessionContextIntegrationFilter;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.http.HttpSessionAttributeListener;
import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;
import java.util.HashMap;


/**
 * UserCounterListener class used to count the current number
 * of active users for the applications.  Does this by counting
 * how many user objects are stuffed into the session.  It also grabs
 * these users and exposes them in the servlet context.
 *
 * @author <a href="mailto:matt@raibledesigns.com">Matt Raible</a>
 * @author Ben Saylor
 */
public class UserCounterListener implements ServletContextListener, HttpSessionAttributeListener, HttpSessionListener {
    /**
     * Name of users Map in the ServletContext
     */
    public static final String USERS_KEY = "users";
    /**
     * The default event we're looking to trap.
     */
    public static final String EVENT_KEY = HttpSessionContextIntegrationFilter.SPRING_SECURITY_CONTEXT_KEY;
    private transient ServletContext servletContext;
    private HashMap<Long, User> users; // Maps user ID to User object

    /**
     * Initialize the context
     *
     * @param sce the event
     */
    public synchronized void contextInitialized(ServletContextEvent sce) {
        servletContext = sce.getServletContext();
    }

    /**
     * Set the servletContext, users and counter to null
     *
     * @param event The servletContextEvent
     */
    public synchronized void contextDestroyed(ServletContextEvent event) {
        servletContext = null;
        users = null;
    }

    @SuppressWarnings("unchecked")
    synchronized void addUser(User user) {
        users = (HashMap<Long,User>) servletContext.getAttribute(USERS_KEY);

        if (users == null) {
            users = new HashMap<Long,User>();
        }

        if (!users.containsKey(user.getId())) {
            users.put(user.getId(), user);
            servletContext.setAttribute(USERS_KEY, users);
        }
    }

    @SuppressWarnings("unchecked")
    synchronized void removeUser(User user) {
        users = (HashMap<Long,User>) servletContext.getAttribute(USERS_KEY);

        if (users != null) {
            users.remove(user.getId());
        }

        servletContext.setAttribute(USERS_KEY, users);
    }

    /**
     * This method is designed to catch when user's login and record their name
     *
     * @param event the event to process
     * @see javax.servlet.http.HttpSessionAttributeListener#attributeAdded(javax.servlet.http.HttpSessionBindingEvent)
     */
    public void attributeAdded(HttpSessionBindingEvent event) {
        if (event.getName().equals(EVENT_KEY) && !isAnonymous()) {
            SecurityContext securityContext = (SecurityContext) event.getValue();
            if (securityContext.getAuthentication().getPrincipal() instanceof User) {
                User user = (User) securityContext.getAuthentication().getPrincipal();
                addUser(user);
            }
        }
    }

    private boolean isAnonymous() {
        AuthenticationTrustResolver resolver = new AuthenticationTrustResolverImpl();
        SecurityContext ctx = SecurityContextHolder.getContext();
        if (ctx != null) {
            Authentication auth = ctx.getAuthentication();
            return resolver.isAnonymous(auth);
        }
        return true;
    }

    /**
     * When user's logout, remove their name from the hashMap
     *
     * @param event the session binding event
     * @see javax.servlet.http.HttpSessionAttributeListener#attributeRemoved(javax.servlet.http.HttpSessionBindingEvent)
     */
    public void attributeRemoved(HttpSessionBindingEvent event) {
        if (event.getName().equals(EVENT_KEY) && !isAnonymous()) {
            SecurityContext securityContext = (SecurityContext) event.getValue();
            Authentication auth = securityContext.getAuthentication();
            if (auth != null && (auth.getPrincipal() instanceof User)) {
                User user = (User) auth.getPrincipal();
                removeUser(user);
            }
        }
    }

    /**
     * Needed for Acegi Security 1.0, as it adds an anonymous user to the session and
     * then replaces it after authentication. http://forum.springframework.org/showthread.php?p=63593
     *
     * @param event the session binding event
     * @see javax.servlet.http.HttpSessionAttributeListener#attributeReplaced(javax.servlet.http.HttpSessionBindingEvent)
     */
    public void attributeReplaced(HttpSessionBindingEvent event) {
        if (event.getName().equals(EVENT_KEY) && !isAnonymous()) {
            final SecurityContext securityContext = (SecurityContext) event.getValue();
            if (securityContext.getAuthentication() != null
                    && securityContext.getAuthentication().getPrincipal() instanceof User) {
                final User user = (User) securityContext.getAuthentication().getPrincipal();
                addUser(user);
            }
        }
    }
    
    public void sessionCreated(HttpSessionEvent se) { 
        //System.out.println("session created, id = " + se.getSession().getId());
    }
    
    public void sessionDestroyed(HttpSessionEvent se) {
        //System.out.println("session destroyed, id = " + se.getSession().getId());
        Object obj = se.getSession().getAttribute(EVENT_KEY);
        if (obj != null) {
            se.getSession().removeAttribute(EVENT_KEY);
        }
    }
}
