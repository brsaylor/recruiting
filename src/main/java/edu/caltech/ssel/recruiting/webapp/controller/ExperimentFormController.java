/*
 * Copyright (C) 1998-2009, <a href="http://www.ssel.caltech.edu">SSEL</a>,
 * at Caltech, Pasadena, CA 91125. 
 *
 * MooreRecruiting is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 * USA.
 *
 */

package edu.caltech.ssel.recruiting.webapp.controller;

import edu.caltech.ssel.recruiting.common.DebugHelper;
import edu.caltech.ssel.recruiting.common.ExperimentCreationHelper;
import edu.caltech.ssel.recruiting.common.RecruitingConstants;
import edu.caltech.ssel.recruiting.model.Experiment;
import edu.caltech.ssel.recruiting.model.ExperimentType;
import edu.caltech.ssel.recruiting.model.Experiment.Status;
import edu.caltech.ssel.recruiting.model.Experimenter;
import edu.caltech.ssel.recruiting.model.Filter;
import edu.caltech.ssel.recruiting.model.Location;
import edu.caltech.ssel.recruiting.service.ExperimentManager;
import edu.caltech.ssel.recruiting.service.ExperimentTypeManager;
import edu.caltech.ssel.recruiting.service.ExtendedUserManager;
import edu.caltech.ssel.recruiting.service.ReminderManager;
import edu.caltech.ssel.recruiting.service.SurveyManager;
import edu.caltech.ssel.recruiting.service.PaymentManager;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.appfuse.model.LabelValue;
import org.appfuse.model.User;
import org.appfuse.service.GenericManager;
import org.appfuse.webapp.controller.BaseFormController;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;

/**
* @author Michael Kolodrubetz, SSEL, Caltech
* @author <a href="mailto:wmyuan@hss.caltech.edu">Walter M. Yuan</a>, SSEL, Caltech
*/
public class ExperimentFormController extends BaseFormController {
    
    private final Log log = LogFactory.getLog(ExperimentFormController.class);
    private ExperimentManager experimentManager;
    private ExtendedUserManager extendedUserManager;
    private ExperimentTypeManager experimentTypeManager;
    private ReminderManager reminderManager;
    private GenericManager<Location, Long> locationManager;
    private SurveyManager surveyManager;
    private PaymentManager paymentManager;
    private GenericManager<Filter, Long> filterManager;
    private ExperimentCreationHelper ecHelper;
    private String surveyContinueView;
    private String noSurveyContinueView;
    private String experimentView;
    private String nextStepView;
    private static List progressList;
    
    public ExperimentFormController() {
        setCommandClass(Experiment.class);
        setCommandName("experiment");
        
        progressList = new ArrayList();
        progressList.add(new LabelValue("Basic Information","experimentform.html"));
        progressList.add(new LabelValue("Attach Survey","surveycreationform.html"));
        progressList.add(new LabelValue("Define Filters","filterform.html"));
        progressList.add(new LabelValue("Publish","experimentverificationform.html"));
    }
    
    /* Need to pass in: List of experimenters (exrList), current experimenter (?),
    list of experiment types (expTypeList), list of locations (locList) */
    
    protected Object formBackingObject(HttpServletRequest request) throws Exception {
        log.debug("Entering formBackingObject...");
        Experiment experiment = null;
        String id = request.getParameter("id");
        String cloneId = request.getParameter("clone");
        Experiment cloneExperiment = null;
        boolean clone = false;

        if(StringUtils.isNotEmpty(id)) {
            try {
                experiment = experimentManager.get(Long.parseLong(id));
                //request.getSession().setAttribute("experiment", experiment);
            } catch(Exception any) {
                saveMessage(request, "Unable to find experiment with the given ID");
            }
        } else if(StringUtils.isNotEmpty(cloneId)) {
            try {
                cloneExperiment = experimentManager.get(Long.parseLong(cloneId));
                request.getSession().removeAttribute("experiment");
                clone = true;
            } catch(Exception any) {
                saveMessage(request, "Unable to find experiment with the given ID");
            }
        } else
            experiment = (Experiment)request.getSession().getAttribute("experiment");
        
        if(experiment == null) {//NEW EXPERIMENT
            experiment = new Experiment();
            String userName = request.getRemoteUser();
            
            //Set current user as reservedBy parameter
            User u = extendedUserManager.getUserByUsername(userName);
            if(u == null) {
                throw new Exception("Could not identify remote user");
            }
            if(!(request.isUserInRole(RecruitingConstants.EXPTR_ROLE) ||
                    request.isUserInRole(RecruitingConstants.ADMIN_ROLE)))
                throw new Exception("Remote user is not an experimenter");
            
            //Setting default values for objects that are necessary

            // BRS
            experiment.setManualPayment(true);

            Experimenter er = null;
            experiment.setReservedBy(u);
            if(u instanceof Experimenter) {
                er = (Experimenter)u;
                experiment.setRunby(er);
                experiment.setPrinciple(er);
            }

            experiment.setStatus(Experiment.Status.OPEN);
            experiment.setLocation(locationManager.get(locationManager.getAll().get(0).getId()));
            experiment.setExpDate(new Date());
            experiment.setShowUpFee(5.00f);
            experiment.setSetupBuffer(15);
            experiment.setCleanupBuffer(15);
            if(clone) {
                experiment.setCleanupBuffer(cloneExperiment.getCleanupBuffer());
                experiment.setFilter(cloneExperiment.getFilter());
                experiment.setInstruction(cloneExperiment.getInstruction());
                experiment.setLocation(cloneExperiment.getLocation());
                experiment.setManualPayment(cloneExperiment.isManualPayment());
                experiment.setMaxNumSubjects(cloneExperiment.getMaxNumSubjects());
                experiment.setMinNumSubjects(cloneExperiment.getMinNumSubjects());
                experiment.setPayGatePayment(cloneExperiment.isPayGatePayment());
                experiment.setSetupBuffer(cloneExperiment.getSetupBuffer());
                experiment.setShowUpFee(cloneExperiment.getShowUpFee());
                experiment.setSignupFreezeTime(cloneExperiment.getSignupFreezeTime());
                experiment.setSurvey(cloneExperiment.getSurvey());
                experiment.setType(cloneExperiment.getType());
                experiment.setPublished(false);

                // BRS 2012-04-04
                experiment.setRunby(cloneExperiment.getRunby());
                experiment.setReserveLaptops(cloneExperiment.getReserveLaptops());
                experiment.setInvisible(cloneExperiment.isInvisible());
                experiment.setExpDate(cloneExperiment.getExpDate());
                experiment.setStartTime(cloneExperiment.getStartTime());
                experiment.setEndTime(cloneExperiment.getEndTime());
                if (cloneExperiment.getFilter() != null) {
                    if (cloneExperiment.getFilter().getId() ==
                            cloneExperiment.getType().getDefaultFilter().getId()) {
                        // Default filter for experiment - set directly
                        experiment.setFilter(cloneExperiment.getFilter());
                    } else {
                        // Experiment-specific filter: clone it
                        Filter filter = cloneExperiment.getFilter().clone();
                        filter = filterManager.save(filter);
                        experiment.setFilter(filter);
                    }
                }

            } else {
                experiment.setType(experimentTypeManager.get(experimentTypeManager.getAll().get(0).getId()));
                //request.getSession().setAttribute("experiment", experiment);
            }

            // BRS 2012-04-04
            request.getSession().setAttribute("experiment", experiment);

        } else { //MODIFY EXPERIMENT
            Experiment e2update = new Experiment();
            e2update.setExpDate(experiment.getExpDate());
            e2update.setStartTime(experiment.getStartTime());
            request.setAttribute("e2update", e2update);
        }
        log.debug("Exiting formBackingObject...");
        return experiment;
    }
    
    protected Map referenceData(HttpServletRequest request) throws Exception {
        Map m = new HashMap();
        
        m.put("statusList", Arrays.asList(Status.values()));
        m.put("exrList", extendedUserManager.getExperimenters());
        Set<ExperimentType> typesSet = new HashSet();
        typesSet.addAll(experimentTypeManager.getAllSorted()); // the backing List isn't unique based on ExperimentType.name
        List<ExperimentType> expTypeList = new ArrayList();
        expTypeList = experimentTypeManager.getAllSorted();
        m.put("expTypeList", expTypeList);
        m.put("locList", locationManager.getAll());
        m.put("progressList", progressList);
        m.put("currentStep", "Basic Information");
        m.put("isPayGateConfigured", paymentManager.isPayGateConfigured());
        return m;
    }
    
    //We need to make sure that the experiment gets cleared if the user clicks "cancel"
    public ModelAndView processFormSubmission(HttpServletRequest request,
            HttpServletResponse response,
            Object command,
            BindException errors)
            throws Exception {
        
        log.debug("in processFormSubmission");
        DebugHelper.displayParams(request, log);
        Experiment experiment = (Experiment)command;
        ModelAndView mav = null;
        
        if(request.getParameter("cancel") != null) {
            if(experiment.getId() == null) {
                mav = new ModelAndView(getCancelView());
                request.getSession().removeAttribute("experiment");
            } else
                mav = new ModelAndView(getExperimentView());
            return mav;
        }
        return super.processFormSubmission(request, response, command, errors);
    }
    
    public ModelAndView onSubmit(HttpServletRequest request,
            HttpServletResponse response, Object command,
            BindException errors)
            throws Exception {
        
        log.debug("Entering onSubmit...");
        Experiment e = (Experiment)command;
        Experiment e2update = (Experiment)request.getAttribute("e2update");
        
        Locale locale = request.getLocale();
        
        //We don't need to look at request paramaters, because by this point we know it is a continue submit
        // (cancel submits are handled by processFormSubmission)
        
        String s = "experiment.added";
        
        // verify IRB checked
        if(StringUtils.isBlank(request.getParameter("checkedIRB"))) {
            errors.rejectValue("checkedIRB", "experimentForm.uncheckedIRB", "unchecked IRB");
            return showForm(request, response, errors);
        } else
            e.setCheckedIRB(true);
        
        //Parse the dates and times from the input fields
        SimpleDateFormat sdf = new SimpleDateFormat();
        sdf.applyPattern("MM/dd/yyyy");
        if(request.getParameter("myExpDate") == null)
            throw new Exception("myExpDate is null");
        Date d = sdf.parse(request.getParameter("myExpDate"));
        if(d == null)
            throw new Exception("Failed to parse expDate...");//Later will replace this by verification step
        else
            e.setExpDate(d);
        
        // set experiment start time using full date
        Calendar timeCal = new GregorianCalendar();
        timeCal.setTime(d);
        boolean isPM = request.getParameter("startAMPM").equals("startPM");
        int hour = Integer.parseInt(request.getParameter("startHours"))%12;
        int minute = Integer.parseInt(request.getParameter("startMinutes"));
        if(isPM) hour+=12;
        timeCal.set(Calendar.HOUR_OF_DAY, hour);
        timeCal.set(Calendar.MINUTE, minute);
        timeCal.set(Calendar.SECOND, 0);
        e.setStartTime(timeCal.getTime());
        
        // set experiment end time using full date
        isPM = request.getParameter("endAMPM").equals("endPM");
        hour = Integer.parseInt(request.getParameter("endHours"))%12;
        minute = Integer.parseInt(request.getParameter("endMinutes"));
        if(isPM) hour+=12;
        timeCal.set(Calendar.HOUR_OF_DAY, hour);
        timeCal.set(Calendar.MINUTE, minute);
        e.setEndTime(timeCal.getTime());
        
        String expId = request.getParameter("id");
        String dateString = request.getParameter("myExpDate");
        String startTimeString = request.getParameter("startHours")+":"+request.getParameter("startMinutes")
                +":"+(request.getParameter("startAMPM").equals("startAM") ? "AM" : "PM");
        String endTimeString = request.getParameter("endHours")+":"+request.getParameter("endMinutes")
                +":"+(request.getParameter("endAMPM").equals("endAM") ? "AM" : "PM");
        String locId = request.getParameter("myLoc");
        String reserveLaptops = request.getParameter("reserveLaptops");
        log.debug("reserveLaptops = " + reserveLaptops);

        
        String setupBuffer = request.getParameter("setupBuffer");
        if(StringUtils.isNotBlank(setupBuffer)) {
            try {
                e.setSetupBuffer(Integer.parseInt(setupBuffer));
            } catch(NumberFormatException nfe) {}
        }
        String cleanupBuffer = request.getParameter("cleanupBuffer");
        if(StringUtils.isNotBlank(cleanupBuffer)) {
            try {
                e.setCleanupBuffer(Integer.parseInt(cleanupBuffer));
            } catch(NumberFormatException nfe) {}
        }
        
        List conflicts = experimentManager.getConflicts(dateString, startTimeString, setupBuffer, endTimeString, cleanupBuffer, expId, locId, reserveLaptops);
        log.debug("Number of conflicts: "+conflicts.size());
        if(conflicts.size() == 1)
            log.debug("Current experiment id number is "+e.getId()+" and the first conflict id number is "+((Experiment)conflicts.get(0)).getId());
        if(conflicts.size() > 0 && ((Experiment)conflicts.get(0)).getId() != e.getId()) {
            errors.rejectValue(null,"errors.time.conflict", "time conflict");
            return showForm(request, response, errors);
        }
            
        
        //Set Invisibility
        String invisible = request.getParameter("invisible");
        if(StringUtils.isNotBlank(invisible))
            e.setInvisible(true);
        
        // double check that paygate is a valid service option
        String payGatePayment = request.getParameter("payGatePayment");
        if(StringUtils.isNotBlank(payGatePayment) && !paymentManager.isPayGateConfigured())
            e.setPayGatePayment(false);
        
        // reset experiment reminders if this is an update
        // and start date/time has changed
        try {
            if(compareStartDateTime(e, e2update) != 0) {
                log.debug("Experiment start time has changed - updating reminders");
                reminderManager.updateExperimentReminders(e);
            } else {
                log.debug("Experiment start time has NOT changed - not updating reminders");
            }
        } catch(Exception any) {
            log.warn("an error occurred updating reminders: "+any.getMessage());
        }
        
        //Only the ID will be properly set for the experimenters, location, and type
        e.setLocation(locationManager.get(Long.parseLong(request.getParameter("myLoc"))));
        e.setType(experimentTypeManager.get(Long.parseLong(request.getParameter("myType"))));
        e.setRunby((Experimenter)extendedUserManager.getUser(request.getParameter("myRunby")));

        // BRS 2011-02-15 set to null, now that PI is associated with ExperimentType
        //e.setPrinciple((Experimenter)extendedUserManager.getUser(request.getParameter("myPrinciple")));
        e.setPrinciple(null);

        e.setStatus(Status.valueOf(request.getParameter("statusSelect")));
        //log.debug("Status was set to " + e.getStatus().toString());
        
        if(StringUtils.isNotEmpty(request.getParameter("savenow"))) {
            e = experimentManager.save(e);
            request.getSession().setAttribute("experiment", e);
            return new ModelAndView(getExperimentView());
        } else {
            e = experimentManager.save(e); // BRS 2012-01-27 workaround
            request.getSession().setAttribute("experiment", e);
            return new ModelAndView(getNextStepView());
        }
    }
    
    private int compareStartDateTime(Experiment exp, Experiment e2update) {
        
        if(e2update == null || exp.getId() == null)
            return -1; // experiment doesn't have a database match
        
        Calendar expCal = getCalFromExpDateTime(
                exp.getExpDate(), exp.getStartTime());
        Calendar expCal2update = getCalFromExpDateTime(
                e2update.getExpDate(), e2update.getStartTime());
        
        // compare the two dates
        return expCal.compareTo(expCal2update);
    }
    
    private Calendar getCalFromExpDateTime(Date date, Date time) {
        Calendar cal = new GregorianCalendar();
        cal.setTime(date); // sets yyyy/MM/dd
        
        Calendar timeCal = new GregorianCalendar();
        timeCal.setTime(time); // sets HH:mm:ss (ignore ss)
        
        // add timeCal fields to cal for complete date
        cal.set(Calendar.HOUR_OF_DAY, timeCal.get(Calendar.HOUR_OF_DAY));
        cal.set(Calendar.MINUTE, timeCal.get(Calendar.MINUTE));
        
        if(log.isDebugEnabled()) {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss z");
            log.debug("getCalFromExpDateTime: "+sdf.format(cal.getTime()));
        }
        return cal;
    }
    
    public void setExperimentManager(ExperimentManager experimentManager) {
        this.experimentManager = experimentManager;
    }
    
    public void setExtendedUserManager(ExtendedUserManager extendedUserManager) {
        this.extendedUserManager = extendedUserManager;
    }
    
    public void setExperimentTypeManager(ExperimentTypeManager experimentTypeManager) {
        this.experimentTypeManager = experimentTypeManager;
    }
    
    public void setLocationManager(GenericManager<Location, Long> locationManager) {
        this.locationManager = locationManager;
    }
    
    public void setSurveyManager(SurveyManager surveyManager) {
        this.surveyManager = surveyManager;
    }
    
    public void setSurveyContinueView(String surveyContinueView) {
        this.surveyContinueView = surveyContinueView;
    }
    
    public void setNoSurveyContinueView(String noSurveyContinueView) {
        this.noSurveyContinueView = noSurveyContinueView;
    }
    
    public String getSurveyContinueView() {
        return surveyContinueView;
    }
    
    public String getNoSurveyContinueView() {
        return noSurveyContinueView;
    }
    
    public void setFilterManager(GenericManager<Filter, Long> filterManager) {
        this.filterManager = filterManager;
    }

    public void setReminderManager(ReminderManager reminderManager) {
        this.reminderManager = reminderManager;
    }
    
    public void setExperimentCreationHelper(ExperimentCreationHelper ecHelper) {
        this.ecHelper = ecHelper;
    }

    public void setNextStepView(String nextStepView) {
        this.nextStepView = nextStepView;
    }

    public void setExperimentView(String experimentView) {
        this.experimentView = experimentView;
    }

    public String getExperimentView() {
        return experimentView;
    }

    public String getNextStepView() {
        return nextStepView;
    }

    public void setPaymentManager(PaymentManager paymentManager) {
        this.paymentManager = paymentManager;
    }
    
}
