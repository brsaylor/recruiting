/*
 * Copyright (C) 1998-2009, <a href="http://www.ssel.caltech.edu">SSEL</a>,
 * at Caltech, Pasadena, CA 91125. 
 *
 * MooreRecruiting is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 * USA.
 *
 */

package edu.caltech.ssel.recruiting.webapp.controller;

import edu.caltech.ssel.recruiting.common.RecruitingConstants;
import edu.caltech.ssel.recruiting.model.Experimenter;
import edu.caltech.ssel.recruiting.model.Experiment;
import edu.caltech.ssel.recruiting.service.ExperimentManager;

import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.appfuse.model.User;

import org.appfuse.service.UserManager;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

/**
 *
 * @author michaelk
 */
public class ModifyExperimentController implements Controller {
    
    private static final Log log = LogFactory.getLog(ModifyExperimentController.class);
    private String postView;
    private ExperimentManager experimentManager;
    private UserManager userManager;
    private static final String MESSAGES_KEY = "successMessages";
    
    /** Creates a new instance of ModifyExperimentController */
    public ModifyExperimentController() {
    }

    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
        
        ModelAndView mv = new ModelAndView();
        User user = userManager.getUserByUsername(request.getRemoteUser());
        if(user != null &&
                (request.isUserInRole(RecruitingConstants.EXPTR_ROLE) ||
                request.isUserInRole(RecruitingConstants.ADMIN_ROLE)))
            mv.addObject("expList", experimentManager.getByRunby(user.getId()));
        
        String id = request.getParameter("id");
        request.setAttribute("id",request.getParameter("id"));
        Experiment sessionExperiment = (Experiment)request.getSession().getAttribute("experiment");
        boolean writable = request.isUserInRole(RecruitingConstants.ADMIN_ROLE);
        if(StringUtils.isNotEmpty(id)) {
            try {
                Experiment experiment = experimentManager.get(Long.parseLong(id));
                request.getSession().setAttribute("experiment", experiment);
                if(writable) {
                    request.getSession().setAttribute("writable", true);
                } else {
                    request.getSession().setAttribute("writable", experiment.isWritable(user.getUsername()));
                }
            } catch(Exception exc) {
                saveMessage(request, "Unable to find experiment with the given ID");//getText("modifyexperiment.fail", request.getLocale()));
            }
        } else if(sessionExperiment != null && sessionExperiment.getId() != null) {
            request.getSession().setAttribute("experiment", sessionExperiment);
            if(writable)
                request.getSession().setAttribute("writable", true);
            else
                request.getSession().setAttribute("writable", sessionExperiment.isWritable(user.getUsername()));
        }
        
        return mv;
    }

    //Stolen directly from BaseFormController
    public void saveMessage(HttpServletRequest request, String msg) {
        List messages = (List) request.getSession().getAttribute(MESSAGES_KEY);

        if (messages == null) {
            messages = new ArrayList();
        }

        messages.add(msg);
        request.getSession().setAttribute(MESSAGES_KEY, messages);
    }

    public String getPostView() {
        return postView;
    }

    public void setPostView(String postView) {
        this.postView = postView;
    }

    public void setExperimentManager(ExperimentManager experimentManager) {
        this.experimentManager = experimentManager;
    }

    public void setUserManager(UserManager userManager) {
        this.userManager = userManager;
    }
    
}
