/*
 * Copyright (C) 1998-2009, <a href="http://www.ssel.caltech.edu">SSEL</a>,
 * at Caltech, Pasadena, CA 91125. 
 *
 * MooreRecruiting is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 * USA.
 *
 */

package edu.caltech.ssel.recruiting.webapp.controller;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.appfuse.Constants;
import org.appfuse.webapp.controller.BaseFormController;
import org.appfuse.webapp.controller.FileUpload;
import org.springframework.validation.BindException;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import org.springframework.web.servlet.ModelAndView;

import edu.caltech.ssel.recruiting.common.DebugHelper;
import edu.caltech.ssel.recruiting.model.Experiment;
import edu.caltech.ssel.recruiting.model.ParticipantRecord;
import edu.caltech.ssel.recruiting.model.ParticipantRecord.PaymentMethod;
import edu.caltech.ssel.recruiting.model.ParticipantRecord.Status;
import edu.caltech.ssel.recruiting.model.Payment;
import edu.caltech.ssel.recruiting.model.Subject;
import edu.caltech.ssel.recruiting.service.ExperimentManager;
import edu.caltech.ssel.recruiting.service.ExtendedUserManager;
import edu.caltech.ssel.recruiting.service.ParticipantRecordManager;
import edu.caltech.ssel.recruiting.service.PaymentManager;
/**
 * Manages uploading Participant record files
 */
public class ParticipantRecordUploadFormController extends BaseFormController {
    
    private ExperimentManager experimentManager;
    private ExtendedUserManager extendedUserManager;
    private ParticipantRecordManager participantRecordManager;
    private PaymentManager paymentManager;
    
    private String experimentView;
    
    public ParticipantRecordUploadFormController() {
        setCommandClass(FileUpload.class);
        setCommandName("fileUpload");
    }
    public ModelAndView processFormSubmission(HttpServletRequest request,
                                              HttpServletResponse response,
                                              Object command,
                                              BindException errors)
    throws Exception {
        
        DebugHelper.displayParams(request, log);
        
        Locale locale = request.getLocale();
        Experiment experiment = (Experiment)request.getSession().getAttribute("experiment");
        if(request.getParameter("cancel") != null)
            return new ModelAndView(getCancelView());
        else if(experiment == null) {
            saveMessage(request, getText("participantRecordUpload.noExperiment", locale));
            return new ModelAndView(getExperimentView());
        }

        return super.processFormSubmission(request, response, command, errors);
    }

    public ModelAndView onSubmit(HttpServletRequest request,
                                 HttpServletResponse response, Object command,
                                 BindException errors)
    throws Exception {
        
        log.debug("in onSubmit");
        Locale locale = request.getLocale();
        Experiment experiment = (Experiment)request.getSession().getAttribute("experiment");
        
        FileUpload fileUpload = (FileUpload) command;

        // validate a file was entered
        if (fileUpload.getFile().length == 0) {
            Object[] args = 
                new Object[] { getText("uploadForm.file", request.getLocale()) };
            errors.rejectValue("file", "errors.required", args, "File");
            
            return showForm(request, response, errors);
        }

        MultipartHttpServletRequest multipartRequest =
            (MultipartHttpServletRequest) request;
        CommonsMultipartFile file =
            (CommonsMultipartFile) multipartRequest.getFile("file");

        //retrieve the file data
        BufferedReader br = new BufferedReader(
                new InputStreamReader(file.getInputStream()));
        int counter=1;
        boolean file_errors = false;
        List<ParticipantRecord> uploadList = new ArrayList();
        String line = br.readLine(); // assume first line is a header
        if(line != null) {
            while(true) {
                counter++;
                line = br.readLine();
                if(line != null) {
                    // parse it
                    try {
                        ParticipantRecord prec = parseParticipantRecord(
                                locale,
                                line,
                                counter);
                        prec.setExp(experiment);
                        uploadList.add(prec);
                    } catch(Exception any) {
                        file_errors = true;
                        saveMessage(request, any.getMessage());
                    }
                } else
                    break;
            }
            
            if(!file_errors) {
                log.info("no upload file errors. saving/updating participants");
                
                List<ParticipantRecord> precList = participantRecordManager.getByExperiment(experiment);
                int duplicates = 0;
                for(int i=0; i<uploadList.size(); i++) {
                    for(int j=0; j<precList.size(); j++) {
                        // replace if username is found in list of participants
                        if(uploadList.get(i).getSubject().getUsername()
                                .equals(precList.get(j).getSubject().getUsername())) {
                            participantRecordManager.remove(precList.get(j).getId());
                            duplicates++;
                            break;
                        }
                    }
                    participantRecordManager.save(uploadList.get(i));
                }
                // update experiment status to full if participant list exceeds max subjects
                precList = participantRecordManager.getByExperiment(experiment);
                if(precList.size() >= experiment.getMaxNumSubjects()) {
                    experiment.setStatusString("Full");
                    try {
                        experiment = experimentManager.save(experiment);
                        request.getSession().setAttribute("experiment", experiment);
                    } catch(Exception any) {
                        log.warn("failed to update experiment status to Full");
                    }
                }
                // miller time
                saveMessage(request, getText("participantRecordUpload.success",
                        new Object[]{new Integer(uploadList.size()),
                        new Integer(duplicates)},locale));
            } else
                return new ModelAndView(getFormView()).addObject("fileUpload", fileUpload);
        } else
            saveMessage(request, getText("participantRecordUpload.emptyFile", locale));
        
        br.close();
        return new ModelAndView(getSuccessView());
    }
    
    /**
     * Takes a comma separated participant record field with the following format:
     * : Subject,Participated,PlayerNum,PaymentMethod,Payment,Status,Payoff
     */
    private ParticipantRecord parseParticipantRecord(Locale locale, String line, int pos) throws Exception {
        
        String[] fields = line.split(",");
        if(fields == null || fields.length != 7)
            throw new Exception(getText("parseParticipantRecord.fieldLengthError",
                    new Object[]{new Integer(pos)}, locale));
        
        ParticipantRecord prec = new ParticipantRecord();
        prec.setEligible(true);
        
        // set Subject
        try {
            Subject user = (Subject)extendedUserManager.getUserByUsername(fields[0]);
            if(user != null) {
                prec.setSubject(user);
            } else
                throw new Exception();
        } catch(Exception any) {
            String msg = getText("parseParticipantRecord.subjectError",
                    new Object[]{new Integer(pos), fields[0]}, locale);
            throw new Exception(msg);
        }
        
        // set Participated
        try {
            Boolean participated = new Boolean(fields[1]);
            prec.setParticipated(participated.booleanValue());
        } catch(Exception any) {
            String msg = getText("parseParticipantRecord.participatedError",
                    new Object[]{new Integer(pos), fields[1]}, locale);
            throw new Exception(msg);
        }
        
        // set PlayerNum
        try {
            prec.setPlayerNum(new Integer(fields[2]));
        } catch(Exception any) {
            String msg = getText("parseParticipantRecord.playerNumError",
                    new Object[]{new Integer(pos), fields[2]}, locale);
            throw new Exception(msg);
        }
        
        // set PaymentMethod
        try {
            prec.setPaymentMethod(PaymentMethod.valueOf(fields[3]));
            // set Payment
            try {
                Payment payment = new Payment();
                payment.setAccountInfo(fields[4]);
                payment.setStatusString("NA");
                payment.setWhenCreated(new Date());
                payment = paymentManager.save(payment);
                prec.setPayment(payment);
            } catch(Exception any) {
                String msg = getText("parseParticipantRecord.paymentError",
                        new Object[]{new Integer(pos), fields[4]}, locale);
                throw new Exception(msg);
            }
        } catch(Exception any) {
            String msg = getText("parseParticipantRecord.paymentMethodError",
                    new Object[]{new Integer(pos), fields[3]}, locale);
            throw new Exception(msg);
        }
        
        // set Status
        try {
            prec.setStatus(Status.valueOf(fields[5]));
        } catch(Exception any) {
            String msg = getText("parseParticipantRecord.statusError",
                    new Object[]{new Integer(pos), fields[5]}, locale);
            throw new Exception(msg);
        }
        
        // set Payoff
        try {
            prec.setPayoff(new Float(fields[6]));
        } catch(Exception any) {
            String msg = getText("parseParticipantRecord.payoffError",
                    new Object[]{new Integer(pos), fields[6]}, locale);
            throw new Exception(msg);
        }
        
        return prec;
    }
    
    public void setExtendedUserManager(ExtendedUserManager extendedUserManager) {
        this.extendedUserManager = extendedUserManager;
    }

    public void setParticipantRecordManager(ParticipantRecordManager participantRecordManager) {
        this.participantRecordManager = participantRecordManager;
    }

    public void setPaymentManager(PaymentManager paymentManager) {
        this.paymentManager = paymentManager;
    }

    public String getExperimentView() {
        return experimentView;
    }

    public void setExperimentView(String experimentView) {
        this.experimentView = experimentView;
    }

    public void setExperimentManager(ExperimentManager experimentManager) {
        this.experimentManager = experimentManager;
    }
}
