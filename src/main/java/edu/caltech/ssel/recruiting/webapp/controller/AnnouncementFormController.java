/*
 * Copyright (C) 1998-2009, <a href="http://www.ssel.caltech.edu">SSEL</a>,
 * at Caltech, Pasadena, CA 91125. 
 *
 * MooreRecruiting is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 * USA.
 *
 */

package edu.caltech.ssel.recruiting.webapp.controller;

import edu.caltech.ssel.recruiting.common.DebugHelper;
import edu.caltech.ssel.recruiting.common.ExperimentCreationHelper;
import edu.caltech.ssel.recruiting.common.RecruitingConstants;
import edu.caltech.ssel.recruiting.model.Experiment;
import edu.caltech.ssel.recruiting.model.Filter;
import edu.caltech.ssel.recruiting.model.ParticipantRecord;
import edu.caltech.ssel.recruiting.model.Subject;
import edu.caltech.ssel.recruiting.service.ConditionManager;
import edu.caltech.ssel.recruiting.service.EmailManager;
import edu.caltech.ssel.recruiting.service.ExtendedUserManager;
import edu.caltech.ssel.recruiting.service.ParticipantRecordManager;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;
import java.util.Iterator;
import java.text.NumberFormat;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.appfuse.Constants;
import org.appfuse.model.User;
import org.appfuse.service.GenericManager;
import org.appfuse.webapp.controller.BaseFormController;
import org.appfuse.webapp.util.RequestUtil;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import java.text.SimpleDateFormat;

/**
* @author Michael Kolodrubetz, SSEL, Caltech
*/
public class AnnouncementFormController extends BaseFormController {
    
    private final Log log = LogFactory.getLog(AnnouncementFormController.class);
    private EmailManager emailManager;
 
    private ExtendedUserManager extendedUserManager;
    private ConditionManager conditionManager;
    private ParticipantRecordManager participantRecordManager;
    private GenericManager<Experiment, Long> experimentManager;
    private GenericManager<Filter, Long> filterManager;
    private ExperimentCreationHelper ecHelper;
    private String experimentView;
    private String experimenterView;
    private String adminView;
    private String calendarView;

    /** Creates a new instance of AnnouncementFormController */
    public AnnouncementFormController() {
        //Placeholders
        setCommandClass(SimpleMailMessage.class);
        setCommandName("announcement");
    }
    
    public Object formBackingObject(HttpServletRequest request) throws Exception {
        log.debug("Entering formBackingObject...");

        // BRS 2012-02-06 - allow user to choose template file via GET paramter
        String chosenTemplateName = request.getParameter("template");
        if (chosenTemplateName == null) chosenTemplateName = templateName;

        SimpleMailMessage msg = new SimpleMailMessage();
        Locale locale = request.getLocale();
        Experiment experiment = (Experiment)request.getSession().getAttribute("experiment");
        if(experiment != null) {
            //if(experiment.getReservedBy() != null)
            //    msg.setReplyTo(experiment.getReservedBy().getEmail());
            //    BRS 2012-02-03 removed

            msg.setFrom(getText("company.email", locale));

            // BRS 2012-01-06
            // Calculate the duration of the experiment and create a formatted string representation.
            GregorianCalendar startCal = new GregorianCalendar();
            GregorianCalendar endCal = new GregorianCalendar();
            startCal.setTime(experiment.getStartTime());
            endCal.setTime(experiment.getEndTime());
            long ms = endCal.getTimeInMillis() - startCal.getTimeInMillis();
            long minutes = ms / 1000 / 60;
            long hours = minutes / 60;
            minutes -= (hours * 60);
            StringBuilder duration = new StringBuilder();
            if (hours > 0) {
                duration.append("" + hours + (hours == 1 ? " hour" : " hours"));
                if (minutes > 0) {
                    duration.append(" and ");
                }
            }
            if (minutes > 0) {
                duration.append("" + minutes + (hours == 1 ? " minute " : " minutes"));
            }
            
            Map model = new HashMap();
            model.put(Constants.EXPERIMENT, experiment);
            model.put("duration", duration.toString()); // BRS
            model.put("calendarView", RequestUtil.getAppURL(request) + this.calendarView);
            model.put("labName", getText("company.name", locale));
            model.put("labUrlName", getText("company.url", locale));
            model.put("labPhone", getText("company.phone", locale));
            model.put("showUpFee", (NumberFormat.getCurrencyInstance().format(experiment.getShowUpFee())));

            String text = emailManager.getMsgFromTemplate(chosenTemplateName, model);

            msg.setText(text);
            Object [] params = new Object[1];

            // BRS 2012-03-20: pass day-of-week, startTime-endTime instead of experiment id
            //params[0] = experiment.getId();
            SimpleDateFormat dayFmt = new SimpleDateFormat("EEEE");
            SimpleDateFormat timeFmt = new SimpleDateFormat("h:mma");
            params[0] = dayFmt.format(experiment.getExpDate()) + ", "
                + timeFmt.format(experiment.getStartTime()).replace(":00", "").toLowerCase() + "-"
                + timeFmt.format(experiment.getEndTime()).replace(":00", "").toLowerCase();

            // BRS 2012-02-06
            // Set message subject
            if ("exp-cancellation-notice.vm".equals(chosenTemplateName)) {
                msg.setSubject(getText("experiment.cancellation", params, locale));
            } else if ("experiment-reminder-message.vm".equals(chosenTemplateName)) {
                msg.setSubject(getText("experiment.reminder", params, locale));
            } else if ("".equals(chosenTemplateName)) {
                // Blank template chosen - leave subject empty
            } else {
                // Default template when there is an experiment is the new // experiment announcement
                msg.setSubject(getText("experiment.newAnnouncement", params, locale));
            }

        } else {
            //msg.setReplyTo(extendedUserManager.getUserByUsername(request.getRemoteUser()).getEmail()); // BRS 2012-02-03 removed
        }


        return msg;
    }
    
    protected Map referenceData(HttpServletRequest request) throws Exception {
        Map m = new HashMap();
        
        //Currently subject/admin/experimenter is done by class type - should be done by roles
        m.put("allSubjects", extendedUserManager.getSubjectEmails());
        //m.put("allStandardUsers", extendedUserManager.getEnabledUserEmails());
        m.put("allExperimenters", extendedUserManager.getExperimenterEmails());
        //m.put("allUsers", toEmailList(extendedUserManager.getUsers(null)));
        
        Filter filter = null;
        
        Experiment experiment = (Experiment)request.getSession().getAttribute("experiment");
        if(experiment != null) {
            filter = experiment.getFilter();
            if(filter != null) {
                m.put("eligList", conditionManager.getEligibleUserEmailsByFilter(filter, false)); // BRS 2012-02-09: false to exclude unsubscribed
            } else {
                m.put("eligList", m.get("allSubjects"));
            }
            
            // create some participant based lists for emails
            List<ParticipantRecord> participantRecordList = participantRecordManager.getByExperiment(experiment);

            // Create participants lists
            // BRS 2011-02-15 reorganize and exclude unsubscribed
            List<Subject> allParticipantsList = new ArrayList();
            List<Subject> eligibleParticipantsList = new ArrayList();
            List<Subject> ineligibleParticipantsList = new ArrayList();
            for(int i=0; participantRecordList !=null && i<participantRecordList.size(); i++) {
                ParticipantRecord participantRecord = participantRecordList.get(i);
                Subject s = participantRecordList.get(i).getSubject();
                if (s.getSubscribed()) {
                    allParticipantsList.add(s);
                    if (participantRecord.isEligible()) {
                        eligibleParticipantsList.add(s);
                    } else {
                        ineligibleParticipantsList.add(s);
                    }
                }
            }
            m.put("allParticipantsList", toEmailList(allParticipantsList));
            m.put("eligibleParticipantsList", toEmailList(eligibleParticipantsList));
            m.put("ineligibleParticipantsList", toEmailList(ineligibleParticipantsList));

        }

        // BRS 2012-02-06
        if (request.getParameter("template") != null) {
            m.put("template", request.getParameter("template"));
        } else {
            if (experiment != null) {
                m.put("template", "newexp-announcement.vm");
            } else {
                m.put("template", "");
            }
        }

        // BRS: go through all the email lists and remove unsubscribed users.
        /* // BRS removed 2011-02-15 - fixed in the queries instead because this is very slow
        for (Object o : m.values()) { // for each list contained in the HashMap
            ArrayList<String> lst = (ArrayList<String>) o; // convert the Object returned by the HashMap to an ArrayList
            for (Iterator<String> iter = lst.iterator(); iter.hasNext();) { // for each email in the list
                String email = iter.next(); // get the address
                //log.debug("email = " + email);
                Subject subject;
                try {
                    subject = extendedUserManager.getSubjectByEmail(email);
                } catch (java.lang.ClassCastException e) {
                    // This user is not a Subject, so skip.
                    log.debug(email + "is not the email address of a Subject.");
                    continue;
                }
                //log.debug("subject.getUsername() = " + subject.getUsername());
                if (!subject.getSubscribed()) { // if the user is unsubscribed,
                                        // note: getSubjectByEmail will probably
                                        // get non-Subject users as well, which
                                        // is what we want.
                    iter.remove(); // remove the email address
                }
            }
        }
        */
        
        return m;
    }
    
    //We need to make sure that the experiment gets cleared if the user clicks "cancel"
    public ModelAndView processFormSubmission(HttpServletRequest request,
            HttpServletResponse response,
            Object command,
            BindException errors)
            throws Exception {
        
        DebugHelper.displayParams(request, log);
        
        Experiment experiment = (Experiment)request.getSession().getAttribute("experiment");
        if(request.getParameter("cancel") != null) {
            if(experiment != null)
                return new ModelAndView(getExperimentView());
            else if(request.isUserInRole(RecruitingConstants.ADMIN_ROLE))
                return new ModelAndView(getAdminView());
            else if(request.isUserInRole(RecruitingConstants.EXPTR_ROLE))
                return new ModelAndView(getExperimenterView());
            else
                return new ModelAndView(getCancelView());
        }
        return super.processFormSubmission(request, response, command, errors);
    }
    
    public ModelAndView onSubmit(HttpServletRequest request,
            HttpServletResponse response, Object command,
            BindException errors)
            throws Exception {
        
        SimpleMailMessage msg = (SimpleMailMessage) command;
        Experiment experiment = (Experiment)request.getSession().getAttribute("experiment");
        
        //Parse the to, cc, and bcc lists
        //We ned to be able to differentiate betw. null to list (original) and blank list (second time through)
        String [] arr;
        arr = request.getParameterValues("emailList_to");
        if(arr != null)
            msg.setTo(randIfNecc(arr, request, "to"));
        else
            msg.setTo(new String[0]);
        msg.setCc(randIfNecc(request.getParameterValues("emailList_cc"), request, "cc"));
        msg.setBcc(randIfNecc(request.getParameterValues("emailList_bcc"), request, "bcc"));
        
        /*try {
            mailEngine.send(msg);
        } catch(NullPointerException e) {
            log.debug("NullPointerException thrown: " + e.getMessage());
            log.debug("Emails must have at least one recipient");
            System.err.println("Recipientless emails should be checked for in validation");
        }*/
        
        /*Experiment e = ecHelper.getExperiment();
        String id = request.getParameter("id");
        if(e == null && StringUtils.isNotEmpty(id))
            e = experimentManager.get(Long.parseLong(id));*/
        
        ModelAndView mv = null;
        try {
            int [] stat = emailManager.sendEmail(msg);
            Object[] args = new Object[2];
            args[0] = stat[1];
            args[1] = stat[0];
            log.info("Message has been sent to " + stat[1] + " with total " + stat[0] + " requested");
            saveMessage(request, getText("announcementForm.sent", args, request.getLocale()));
        } catch(NullPointerException exc) {
            log.debug("NullPointerException thrown: " + exc.getMessage());
            log.debug("Emails must have at least one recipient");
            log.error("Recipientless emails should be checked for in validation");
            saveMessage(request, getText("announcementForm.failed", request.getLocale()));
        }
        if(experiment != null)
            mv = new ModelAndView(getExperimentView());
        else if(request.isUserInRole(RecruitingConstants.ADMIN_ROLE))
            mv = new ModelAndView(getAdminView());
        else if(request.isUserInRole(RecruitingConstants.EXPTR_ROLE))
            mv = new ModelAndView(getExperimenterView());
        else
            mv = new ModelAndView(getCancelView());
        
        return mv;
    }
    
    private String [] randIfNecc(String[] arr, HttpServletRequest request, String type) {
        if(request.getParameter("setNotifSize_"+type) != null) {
            try {
                int num = Integer.parseInt(request.getParameter("notifSize_"+type));
                if(num >= arr.length)
                    return arr;
                int sz = arr.length;
                boolean[] used = new boolean[sz];
                int j, k, l, r;
                for(j = 0; j < sz; j++)
                    used[j] = false;
                Random gen = new Random();
                for(j = 0; j < num; j++) {
                    r = gen.nextInt(sz-j);
                    l=0;
                    for(k = 0; k < sz; k++) {
                        if(!used[k]) {
                            if(l == r) {
                                used[k] = true;
                                break;
                            }
                            l++;
                        }
                    }
                    //System.out.println("For iteration " + j + ", r = " + r + " and k = " + k);
                }
                
                String[] arr2 = new String[num];
                for(j = 0, k=0; j < sz; j++) {
                    if(used[j]) {
                        arr2[k] = arr[j];
                        k++;
                    }
                }
                return arr2;
            } catch(Exception e) {
                //If there are any exceptions, throw a log statement but return the full array
                log.debug("Error thrown while setting notification size for type="+type+": " +e.getMessage());
            }
        }
        return arr;
    }
    
    private List<String> toEmailList(List ulst) {
        List<String> lst = new ArrayList();
        for(int j = 0; j < ulst.size(); j++)
            lst.add(((User)ulst.get(j)).getEmail());
        return lst;
    }
    
    public void setEmailManager(EmailManager mailEngine) {
        this.emailManager = mailEngine;
    }
    
    public void setExtendedUserManager(ExtendedUserManager extendedUserManager) {
        this.extendedUserManager = extendedUserManager;
    }
    
    public void setExperimentManager(GenericManager<Experiment, Long> experimentManager) {
        this.experimentManager = experimentManager;
    }
    
    public void setExperimentCreationHelper(ExperimentCreationHelper ecHelper) {
        this.ecHelper = ecHelper;
    }
    
    public void setConditionManager(ConditionManager conditionManager) {
        this.conditionManager = conditionManager;
    }
    
    public void setFilterManager(GenericManager<Filter, Long> filterManager) {
        this.filterManager = filterManager;
    }

    public void setExperimentView(String experimentView) {
        this.experimentView = experimentView;
    }

    public void setExperimenterView(String experimenterView) {
        this.experimenterView = experimenterView;
    }

    public void setAdminView(String adminView) {
        this.adminView = adminView;
    }

    public String getExperimentView() {
        return experimentView;
    }

    public String getExperimenterView() {
        return experimenterView;
    }

    public String getAdminView() {
        return adminView;
    }

    /**
     * @param calendarView the calendarView to set
     */
    public void setCalendarView(String calendarView) {
        this.calendarView = calendarView;
    }

    public void setParticipantRecordManager(ParticipantRecordManager participantRecordManager) {
        this.participantRecordManager = participantRecordManager;
    }

}
