/*
 * Copyright (C) 1998-2009, <a href="http://www.ssel.caltech.edu">SSEL</a>,
 * at Caltech, Pasadena, CA 91125. 
 *
 * MooreRecruiting is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 * USA.
 *
 */

package edu.caltech.ssel.recruiting.webapp.controller;

import java.util.HashMap;
import java.util.Map;
import org.apache.commons.lang.StringUtils;
import org.appfuse.webapp.controller.BaseFormController;
import org.appfuse.service.UserManager;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;
import java.util.Locale;

import edu.caltech.ssel.recruiting.common.DebugHelper;
import edu.caltech.ssel.recruiting.model.Experiment;
import edu.caltech.ssel.recruiting.model.Payment;
import edu.caltech.ssel.recruiting.model.ParticipantRecord;
import edu.caltech.ssel.recruiting.model.Subject;
import edu.caltech.ssel.recruiting.service.ExperimentManager;
import edu.caltech.ssel.recruiting.service.ParticipantRecordManager;
import edu.caltech.ssel.recruiting.service.PaymentManager;

/**
 *
 */
public class PaymentOptionFormController extends BaseFormController {
    
    private ExperimentManager experimentManager;
    private ParticipantRecordManager participantRecordManager;
    private PaymentManager paymentManager;
    private UserManager userManager;

    public PaymentOptionFormController() {
        setCommandClass(Payment.class);
        setCommandName("paymentOption");
    }
    
    protected ModelAndView handleRequestInternal(HttpServletRequest request, HttpServletResponse response) throws Exception {
        ModelAndView mv = super.handleRequestInternal(request, response);
        if(!isFormSubmission(request)) {
            log.debug("entering handleRequestInternal");
            
            Long expId = Long.parseLong(request.getParameter("expId"));
            Experiment e = experimentManager.get(expId);
            if(e != null && !e.isPayGatePayment()) {
                
                Payment payment = (Payment)mv.getModel().get(this.getCommandName());//Get the model
                request.setAttribute("paymentOptionRadio", "MANUAL");
                return this.onSubmit(request, response, payment, null);
            }
        }
        return mv;
    }

    protected Object formBackingObject(HttpServletRequest request)
    throws Exception {
        
        /**
         * A Subject can only come through this
         * path with a new Payment
         *
        String id = request.getParameter("id");
        if (!StringUtils.isBlank(id)) {
            return paymentManager.get(new Long(id));
        }
         */
        
        log.debug("returning default");
        return super.formBackingObject(request);
    }
    
    protected Map referenceData(HttpServletRequest request) throws Exception {
        Map m = new HashMap();
        
        Long expId = Long.parseLong(request.getParameter("expId"));
        Experiment e = experimentManager.get(expId);
        
        if(e != null && e.isPayGatePayment())
            m.put("showPayGate",true);
        else
            m.put("showPayGate",false);
        if(e != null && e.isManualPayment())
            m.put("showManual",true);
        else
            m.put("showManual",false);
        
        return m;
    }

    public ModelAndView onSubmit(HttpServletRequest request,
                                 HttpServletResponse response, Object command,
                                 BindException errors)
    throws Exception {
        log.debug("entering 'onSubmit' method...");
        DebugHelper.displayParams(request, log);

        Payment payment = (Payment) command;
        String expId = request.getParameter("expId");
        String expDate = request.getParameter("expDate");
        String paymentMethod = request.getParameter("paymentOptionRadio");
        if(StringUtils.isBlank(paymentMethod))
            paymentMethod = (String)request.getAttribute("paymentOptionRadio"); // see handleInternalRequest above
        Subject subject = (Subject)userManager.getUserByUsername(request.getRemoteUser());
        
        // retrieve the legitimate parent ParticipantRecord
        // based on the Experiment and Subject
        ParticipantRecord rec = null;
        if(subject != null &&
                !StringUtils.isBlank(expId)) {
            rec = participantRecordManager.getByExperimentAndSubject(
                    new Long(expId), subject.getId());
        }
        
        Locale locale = request.getLocale();
        if(rec != null) {
            payment.setStatusString("NA");
            payment.setWhenCreated(new Date());
            payment = paymentManager.save(payment);
            
            rec.setPayment(payment);
            rec.setPaymentMethod(ParticipantRecord.PaymentMethod.valueOf(paymentMethod));
            participantRecordManager.save(rec);
            // BRS removed 2012-03-20
            //saveMessage(request, getText("paymentOption.updated", locale));
        } else {
            //saveMessage(request, getText("paymentOption.updateFailed", locale));
        }
        
        return new ModelAndView(getSuccessView()).addObject("date", expDate);
    }

    public void setExperimentManager(ExperimentManager experimentManager) {
        this.experimentManager = experimentManager;
    }

    public void setParticipantRecordManager(ParticipantRecordManager participantRecordManager) {
        this.participantRecordManager = participantRecordManager;
    }

    public void setPaymentManager(PaymentManager paymentManager) {
        this.paymentManager = paymentManager;
    }

    public void setUserManager(UserManager userManager) {
        this.userManager = userManager;
    }
}
