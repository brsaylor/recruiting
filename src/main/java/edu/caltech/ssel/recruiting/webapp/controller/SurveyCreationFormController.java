/*
 * Copyright (C) 1998-2009, <a href="http://www.ssel.caltech.edu">SSEL</a>,
 * at Caltech, Pasadena, CA 91125. 
 *
 * MooreRecruiting is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 * USA.
 *
 */

package edu.caltech.ssel.recruiting.webapp.controller;

import edu.caltech.ssel.recruiting.common.DebugHelper;
import edu.caltech.ssel.recruiting.common.RecruitingConstants;
import edu.caltech.ssel.recruiting.common.ExperimentCreationHelper;
import edu.caltech.ssel.recruiting.model.Experiment;
import edu.caltech.ssel.recruiting.model.ExperimentType;
import edu.caltech.ssel.recruiting.model.Filter;
import edu.caltech.ssel.recruiting.model.Question;
import edu.caltech.ssel.recruiting.model.Question.QStyle;
import edu.caltech.ssel.recruiting.model.QuestionResultCondition;
import edu.caltech.ssel.recruiting.model.QuestionResultCondition.FilterType;
import edu.caltech.ssel.recruiting.model.SubjectPool;
import edu.caltech.ssel.recruiting.model.Survey;
import edu.caltech.ssel.recruiting.model.Survey.Visibility;
import edu.caltech.ssel.recruiting.service.ConditionManager;
import edu.caltech.ssel.recruiting.service.ExperimentManager;
import edu.caltech.ssel.recruiting.service.ExtendedUserManager;
import edu.caltech.ssel.recruiting.service.SubjectPoolManager;
import edu.caltech.ssel.recruiting.service.SurveyManager;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.appfuse.model.LabelValue;
import org.appfuse.model.User;
import org.appfuse.service.GenericManager;
import org.appfuse.webapp.controller.BaseFormController;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.servlet.ModelAndView;

/**
* @author Michael Kolodrubetz, SSEL, Caltech
* @author <a href="mailto:wmyuan@hss.caltech.edu">Walter M. Yuan</a>, SSEL, Caltech
*/
public class SurveyCreationFormController extends BaseFormController {
    private final Log log = LogFactory.getLog(SurveyCreationFormController.class);
    
    private static final String ADD_END = "--add to end--";
    private static final String ADD_FRONT = "--add to front--";
    
    private SurveyManager surveyManager;
    private ExtendedUserManager extendedUserManager;
    private ConditionManager conditionManager;
    private GenericManager<Question, Long> questionManager;
    private ExperimentManager experimentManager;
    private GenericManager<Filter, Long> filterManager;
    private GenericManager<ExperimentType, Long> experimentTypeManager;
    private ExperimentCreationHelper ecHelper;
    
    //private String surveySuccessView;
    private String profileSuccessView;
    private String noProfileSuccessView;
    private String nextStepView;
    private String previousStepView;
    private String experimentView;
    private String adminView;
    private static List progressList;
    
    private SimpleDateFormat placeholderFormatter = new SimpleDateFormat("HH:mm:ss");
    
    /** Creates a new instance of SurveyCreationFormController */
    public SurveyCreationFormController() {
        setCommandClass(Survey.class);
        setCommandName("survey");
        
        progressList = new ArrayList();
        progressList.add(new LabelValue("Basic Information","experimentform.html"));
        progressList.add(new LabelValue("Attach Survey","surveycreationform.html"));
        progressList.add(new LabelValue("Define Filters","filterform.html"));
        progressList.add(new LabelValue("Publish","experimentverificationform.html"));
    }
    
    protected Object formBackingObject(HttpServletRequest request) throws Exception {
        log.debug("in formBackingObject");
        
//ec        ecHelper.setSession(request.getSession());
        
        Survey survey = null;
        String id = request.getParameter("id");
        
        if(StringUtils.isNotEmpty(id)) {
            log.debug("looking up survey");
            try {
                survey = surveyManager.get(Long.parseLong(id));
            } catch(Exception any) {
                saveMessage(request, "Unable to find survey with the given ID");
            }
        } else {
            Experiment experiment = (Experiment)request.getSession().getAttribute("experiment");
            if(experiment != null)
                survey = experiment.getSurvey();
        }

        if(survey == null)
            survey = new Survey();

        return survey;
    }
    
    protected Map referenceData(HttpServletRequest request, Object command, Errors errors) throws Exception {
        log.debug("in referenceData");
        Map m = new HashMap();
        List<Survey> surveyList = surveyManager.getAll();
        
        // without a Set, getAll returns duplicate surveys on
        // (at least) multiple user (survey creator) roles
        HashSet<Survey> surveySet = new HashSet<Survey>(0);
        surveySet.addAll(surveyList);
        
        // remove the profile survey for experiment creation flow
        if(request.getSession().getAttribute("experiment") != null) // check if in flow
            surveySet.remove(surveyManager.getProfileSurvey());
        m.put("surveySet", surveySet);
        
        Survey survey = (Survey)command;
        
        int j;
        int sz = 0;
        if(survey != null && survey.getQuestions() != null)
            sz = survey.getQuestions().size();
        List<String> numList = new ArrayList();
        List<QStyle> qstyleList = Arrays.asList(QStyle.values());
        numList.add(ADD_END);
        if(sz != 0)
            numList.add(ADD_FRONT);
        for(j = 0; j < sz-1; j++)
            numList.add(Integer.toString(j+1));
        
        m.put("questionList", questionManager.getAll());
        m.put("numList", numList);
        m.put("styleList", qstyleList);
        m.put("progressList", progressList);
        m.put("currentStep", "Attach Survey");
        
        /** what is this for?
        List<SubjectPool> lst = subjectPoolManager.getAll();
        sz = lst.size();
        List<LabelValue> lvlst = new ArrayList(sz);
        for(j = 0; j < sz; j++)
            lvlst.add(new LabelValue(lst.get(j).getName(), lst.get(j).getId().toString()));
        m.put("poolLVList", lvlst);
        
        List<ExperimentType> lst2 = experimentTypeManager.getAll();
        sz = lst2.size();
        lvlst = new ArrayList(lst2.size());
        for(j = 0; j < lst2.size(); j++)
            lvlst.add(new LabelValue(lst2.get(j).getName(), lst2.get(j).getId().toString()));
        m.put("etypeLVList", lvlst);
         */
        
        return m;
    }
    
    //We need to make sure that the experiment gets cleared if the user clicks "cancel"
    public ModelAndView processFormSubmission(HttpServletRequest request,
            HttpServletResponse response,
            Object command,
            BindException errors)
            throws Exception {
        
        log.debug("in processFormSubmission");
        
        DebugHelper.displayParams(request, log);
        ModelAndView mav = null;
        
        if(request.getParameter("cancel") != null) {
            Experiment experiment = (Experiment)request.getSession().getAttribute("experiment");
            if(experiment == null) {
                if(request.isUserInRole(RecruitingConstants.ADMIN_ROLE))
                    mav = new ModelAndView(getAdminView());
                else if(request.isUserInRole(RecruitingConstants.EXPTR_ROLE))
                    mav = new ModelAndView(getCancelView());
            } else if(experiment.getId() == null) {
                mav = new ModelAndView(getCancelView());
                request.getSession().removeAttribute("experiment");
            } else
                mav = new ModelAndView(getExperimentView());
            return mav;
        } else if(request.getParameter("back") != null) {
            mav = new ModelAndView(getPreviousStepView());
            return mav;
        }
        return super.processFormSubmission(request, response, command, errors);
    }
    
    public ModelAndView onSubmit(HttpServletRequest request,
            HttpServletResponse response, Object command,
            BindException errors)
            throws Exception {
        log.debug("In onSubmit...");
        
        Survey survey = (Survey) command;
        Experiment experiment = (Experiment)request.getSession().getAttribute("experiment");
        
        String userName = request.getRemoteUser();
        //Set current user as reservedBy parameter
        User u = extendedUserManager.getUserByUsername(userName);
        
        // verify IRB checked
        if(StringUtils.isBlank(request.getParameter("checkedIRB")) &&
                !"NO_SURVEY".equals(request.getParameter("surveyRadio"))) {
            errors.rejectValue("checkedIRB", "surveyForm.uncheckedIRB", "unchecked IRB");
            return showForm(request, response, errors);
        }

        if(experiment == null) {
            // not in flow
            log.debug("no experiment... just saving survey");
            return saveSurvey(request, survey);
        }
        
        String sr = request.getParameter("surveyRadio");
        if("NO_SURVEY".equals(sr)) {
            experiment.setSurvey(null);
        } else {
            //fetch questions based on the qcnt and questionId_(s)
            List<Question> qlst = new ArrayList();
            int qcnt = Integer.parseInt(request.getParameter("qcnt"));
            Long id;
            for(int j = 0; j < qcnt; j++) {
                id = Long.parseLong(request.getParameter("questionId_"+j));
                qlst.add(questionManager.get(id));
            }
            //fetch loose parameters
            String name = request.getParameter("name");
            if(StringUtils.isBlank(name))
                errors.rejectValue("name", "surveyCreationForm.emptyName", "empty name");
            String new_flag = request.getParameter("newSurvey");
            
            survey.setQuestions(qlst);
            survey.setType(Survey.SurveyType.SURVEY);
            if("EXISTING_SURVEY".equals(sr)) {
                survey.setId(Long.parseLong(request.getParameter("existingSurvey")));
            } else {
                survey.setCreator(u);
                // check that the name is unique
                List<Survey> surveys = surveyManager.getAll();
                for(Survey existing : surveys) {
                    if(name.equals(existing.getName()))
                        errors.rejectValue("name", "surveyCreationForm.duplicate",
                                new Object[]{name}, "duplicate name");
                }
            }
            if(errors.getErrorCount() > 0)
                return showForm(request, response, errors);
            
            if("public".equals(request.getParameter("surveyVisibility")))
                survey.setVisibility(Visibility.PUBLIC);
            else
                survey.setVisibility(Visibility.PRIVATE);
            
            survey = surveyManager.save(survey);
            experiment.setSurvey(survey);
        
            //Setup temporary filter
            Filter filter;
            if(experiment.getFilter() == null) {
                filter = new Filter();
            } else {
                filter = experiment.getFilter();
            }
            if(filter.getName() == null)
                filter.setName("CHANGE NAME -- " +
                        placeholderFormatter.format(new Date()));
            if(filter.getConditions() == null)
                filter.setConditions(new ArrayList());

            int sz = survey.getQuestions().size();
            for(int j = 0; j < sz; j++) {
                if(request.getParameter("filter" + Integer.toString(j)) != null) {
                    QuestionResultCondition condition = new QuestionResultCondition();
                    condition.setFilterType(FilterType.valueOf(request.getParameter("eligibleCriterion" + j)));
                    condition.setName(request.getParameter("filterName"+j));

                    Question question = survey.getQuestions().get(j);
                    condition.setQuestion(question);

                    Set<Integer> options = new HashSet();
                    if(request.getParameterValues("selectedOptions" + j) != null) {
                        String[] ostrings = request.getParameterValues("selectedOptions" + j);
                        for(int k = 0; k < ostrings.length; k++) {
                            options.add(new Integer(ostrings[k]));
                        }
                    }
                    condition.setFilterOptions(options);

                    condition = (QuestionResultCondition) conditionManager.save(condition);
                    filter.getConditions().add(condition);
                }
            }

            if(filter.getConditions().size() > 0) {
                filter = filterManager.save(filter);
                experiment.setFilter(filter);
            }
        }
        
        //Set the success view
        ModelAndView mav = null;
        if(request.getParameter("next") != null) {

            // BRS 2012-01-27 workaround
            experiment = experimentManager.save(experiment);
            request.getSession().setAttribute("experiment", experiment);

            mav = new ModelAndView(getNextStepView());
        } else if(StringUtils.isNotEmpty(request.getParameter("savenow"))) {
            experiment = experimentManager.save(experiment);
            request.getSession().setAttribute("experiment", experiment);
            return new ModelAndView(getExperimentView());
        } else { // bail out
            log.warn("unexpected submit value for onSubmit");
            mav = new ModelAndView(getCancelView());
        }
        
        return mav;
    }
    
    private ModelAndView saveSurvey(HttpServletRequest request, Survey survey)
    throws Exception {
        
        if(request.getParameter("surveyVisibility").equals("public"))
            survey.setVisibility(Visibility.PUBLIC);
        else
            survey.setVisibility(Visibility.PRIVATE);

        //Parse the question list from javascript, and save the survey
        List<Question> qlst = new ArrayList();
        int qcnt = Integer.parseInt(request.getParameter("qcnt"));
        Long id;
        for(int j = 0; j < qcnt; j++) {
            id = Long.parseLong(request.getParameter("questionId_"+j));
            qlst.add(questionManager.get(id));
        }
        survey.setQuestions(qlst);
        survey = surveyManager.save(survey);
        return new ModelAndView(getProfileSuccessView());
    }
    
    public void setQuestionManager(GenericManager<Question, Long> questionManager) {
        this.questionManager = questionManager;
    }
    
    public void setExtendedUserManager(ExtendedUserManager extendedUserManager) {
        this.extendedUserManager = extendedUserManager;
    }
    
    public String getProfileSuccessView() {
        return profileSuccessView;
    }
    
    public void setProfileSuccessView(String profileSuccessView) {
        this.profileSuccessView = profileSuccessView;
    }
    
    public void setConditionManager(ConditionManager conditionManager) {
        this.conditionManager = conditionManager;
    }
    
    public void setFilterManager(GenericManager<Filter, Long> filterManager) {
        this.filterManager = filterManager;
    }
    
    public void setSurveyManager(SurveyManager surveyManager) {
        this.surveyManager = surveyManager;
    }
    
    public void setExperimentTypeManager(GenericManager<ExperimentType, Long> experimentTypeManager) {
        this.experimentTypeManager = experimentTypeManager;
    }
    
    public void setExperimentCreationHelper(ExperimentCreationHelper ecHelper) {
        this.ecHelper = ecHelper;
    }
    
    public String getNoProfileSuccessView() {
        return noProfileSuccessView;
    }
    
    public void setNoProfileSuccessView(String noProfileSuccessView) {
        this.noProfileSuccessView = noProfileSuccessView;
    }
    
    public ExperimentManager getExperimentManager() {
        return experimentManager;
    }
    
    public void setExperimentManager(ExperimentManager experimentManager) {
        this.experimentManager = experimentManager;
    }

    public void setNextStepView(String nextStepView) {
        this.nextStepView = nextStepView;
    }
    
    public void setPreviousStepView(String previousStepView) {
        this.previousStepView = previousStepView;
    }

    public void setExperimentView(String experimentView) {
        this.experimentView = experimentView;
    }

    public void setAdminView(String adminView) {
        this.adminView = adminView;
    }

    public String getNextStepView() {
        return nextStepView;
    }

    public String getPreviousStepView() {
        return previousStepView;
    }

    public String getExperimentView() {
        return experimentView;
    }

    public String getAdminView() {
        return adminView;
    }
    
}
