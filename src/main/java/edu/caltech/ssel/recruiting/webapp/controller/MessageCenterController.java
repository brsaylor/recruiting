/*
 * Copyright (C) 1998-2009, <a href="http://www.ssel.caltech.edu">SSEL</a>,
 * at Caltech, Pasadena, CA 91125. 
 *
 * MooreRecruiting is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 * USA.
 *
 */

package edu.caltech.ssel.recruiting.webapp.controller;

import edu.caltech.ssel.recruiting.common.DebugHelper;
import edu.caltech.ssel.recruiting.common.RecruitingConstants;
import edu.caltech.ssel.recruiting.model.Message;
import edu.caltech.ssel.recruiting.model.UserMessage;
import edu.caltech.ssel.recruiting.service.ExtendedUserManager;
import edu.caltech.ssel.recruiting.service.MessageManager;
import edu.caltech.ssel.recruiting.service.UserMessageManager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import org.appfuse.model.User;
import org.appfuse.webapp.controller.BaseFormController;

import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;

import org.springframework.security.context.SecurityContext;
import org.springframework.security.context.SecurityContextHolder;
/**
 * MessageCenterController
 */

/**
* @author Michael Kolodrubetz, SSEL, Caltech
* @author <a href="mailto:wmyuan@hss.caltech.edu">Walter M. Yuan</a>, SSEL, Caltech
*/
public class MessageCenterController extends BaseFormController {
    
    private final Log log = LogFactory.getLog(MessageCenterController.class);
    private ExtendedUserManager extendedUserManager;
    private MessageManager messageManager;
    private UserMessageManager userMessageManager;
    private String inboxView;
    private String deletedView;
    private String sentView;
    private String draftsView;
    private String editView;
    
    public MessageCenterController() {
        setCommandClass(Message.class);
        setCommandName("message");
    }
    
    protected Object formBackingObject(HttpServletRequest request) throws Exception {
        log.debug("Entering formBackingObject...");
        
        Message message = null;
        String id = request.getParameter("id");
        if(StringUtils.isNotEmpty(id)) {
            try {
                message = messageManager.get(Long.parseLong(id));
            } catch(Exception any) {
                saveMessage(request, "Unable to find message with the given ID");
            }
        }
        if(message == null)
            message = new Message();
        
        return message;
    }
    
    protected Map referenceData(HttpServletRequest request) throws Exception {
        
        User user = extendedUserManager.getUserByUsername(request.getRemoteUser());
        
        String option=request.getParameter("option");
        
        Map m = new HashMap();
        if(user != null) {
            // messages created
            if("inbox".equals(option)) {
                // messages received
                List<UserMessage> ul = userMessageManager.findInboxByRecipient(user);
                List<Message> msgList = new ArrayList();
                for(int i=0; ul!=null && i<ul.size(); i++)
                    msgList.add(ul.get(i).getMessage());
                m.put("displayList", msgList);            
            } else if("deleted".equals(option)) {
                // messages deleted
                List<UserMessage> ul = userMessageManager.findDeletedByRecipient(user);
                List<Message> msgList = new ArrayList();
                for(int i=0; ul!=null && i<ul.size(); i++)
                    msgList.add(ul.get(i).getMessage());
                m.put("displayList", msgList);
            } else if("sent".equals(option))
                m.put("displayList", messageManager.findSentByCreator(user));
            else if("drafts".equals(option))
                m.put("displayList", messageManager.findDraftsByCreator(user));
            
            boolean canEdit = (request.isUserInRole(RecruitingConstants.ADMIN_ROLE) ||
                    request.isUserInRole(RecruitingConstants.EXPTR_ROLE));
            m.put("canEdit", new Boolean(canEdit));
        }
        return m;
    }
    
    public ModelAndView processFormSubmission(HttpServletRequest request,
            HttpServletResponse response,
            Object command,
            BindException errors)
            throws Exception {
        
        log.debug("in processFormSubmission");
        DebugHelper.displayParams(request, log);
        Message message = (Message)command;
        ModelAndView mav = null;
        
        if(request.getParameter("cancel") != null) {
            mav = new ModelAndView(getCancelView());
            return mav;
        }
        return super.processFormSubmission(request, response, command, errors);
    }
    
    public ModelAndView onSubmit(HttpServletRequest request,
            HttpServletResponse response, Object command,
            BindException errors)
            throws Exception {
        
        log.debug("Entering onSubmit...");
        
        Message message = (Message)command;
        Locale locale = request.getLocale();
        
        SecurityContext context = SecurityContextHolder.getContext();
        User user = (User)context.getAuthentication().getPrincipal();

        String msgId = request.getParameter("id");
        String option = request.getParameter("option");
        
        String delete = request.getParameter("delete");
        String edit = request.getParameter("edit");
        
        if(StringUtils.isNotEmpty(delete)) {
            try {
                Set<UserMessage> msgSet = messageManager.get(new Long(msgId)).getMessageReferences();
                for(Iterator<UserMessage> it = msgSet.iterator(); it.hasNext();) {
                    UserMessage um = it.next();
                    if(user.getEmail().equals(um.getRecipient().getEmail())) {
                        um.setStatus(UserMessage.Status.DELETED);
                        userMessageManager.save(um);
                        break;
                    }
                }
            } catch(Exception any) {
                log.warn("** yikes **",any);
                saveMessage(request, getText("messagecenter.deleteerror", any.getMessage(), locale));
            }
        } else if(StringUtils.isNotEmpty(edit))
            return new ModelAndView(getEditView()+"?id="+msgId);
        
        if("inbox".equals(option))
            return new ModelAndView(getInboxView());
        else if("deleted".equals(option))
            return new ModelAndView(getDeletedView());
        else if("drafts".equals(option))
            return new ModelAndView(getDraftsView());
        else if("sent".equals(option))
            return new ModelAndView(getSentView());
            
        return new ModelAndView(getSuccessView());
    }
    
    public void setExtendedUserManager(ExtendedUserManager extendedUserManager) {
        this.extendedUserManager = extendedUserManager;
    }

    public void setMessageManager(MessageManager messageManager) {
        this.messageManager = messageManager;
    }

    public void setUserMessageManager(UserMessageManager userMessageManager) {
        this.userMessageManager = userMessageManager;
    }

    public String getInboxView() {
        return inboxView;
    }

    public void setInboxView(String inboxView) {
        this.inboxView = inboxView;
    }

    public String getDeletedView() {
        return deletedView;
    }

    public void setDeletedView(String deletedView) {
        this.deletedView = deletedView;
    }

    public String getSentView() {
        return sentView;
    }

    public void setSentView(String sentView) {
        this.sentView = sentView;
    }

    public String getDraftsView() {
        return draftsView;
    }

    public void setDraftsView(String draftsView) {
        this.draftsView = draftsView;
    }

    public String getEditView() {
        return editView;
    }

    public void setEditView(String editView) {
        this.editView = editView;
    }
    
}
