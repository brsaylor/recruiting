/*
 * Copyright (C) 1998-2009, <a href="http://www.ssel.caltech.edu">SSEL</a>,
 * at Caltech, Pasadena, CA 91125. 
 *
 * MooreRecruiting is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 * USA.
 *
 */

package edu.caltech.ssel.recruiting.webapp.controller;

import edu.caltech.ssel.recruiting.common.RecruitingConstants;
import edu.caltech.ssel.recruiting.model.Experiment;
import edu.caltech.ssel.recruiting.model.Subject;
import edu.caltech.ssel.recruiting.model.ParticipantRecord;
import edu.caltech.ssel.recruiting.service.ParticipantRecordManager;
import edu.caltech.ssel.recruiting.service.ExtendedUserManager;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.appfuse.service.GenericManager;
import org.appfuse.model.User;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

/**
 * Returns Subject participant information for managing options like
 * reminders, cancelling participation, etc.
 */
public class ParticipantRecordOptionsController implements Controller {
    
    private ParticipantRecordManager participantRecordManager;
    private ExtendedUserManager extendedUserManager;
    private GenericManager<Experiment, Long> experimentManager;
    private String invalidView;
    /** Creates a new instance of ParticipantRecordController */
    public ParticipantRecordOptionsController() {
    }

    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
        
        String username = request.getRemoteUser();
        User user = extendedUserManager.getUserByUsername(username);
        if(!(user instanceof Subject) && !request.isUserInRole(RecruitingConstants.ADMIN_ROLE))
            return new ModelAndView(getInvalidView());
        
        ModelAndView mav = new ModelAndView();
        if(user instanceof Subject) {
            List<ParticipantRecord> participantRecordList = participantRecordManager.getBySubject((Subject)user);
            mav.addObject("participantRecordList", participantRecordList);
        }
        else if (request.isUserInRole(RecruitingConstants.ADMIN_ROLE)) {
            Long id = Long.parseLong(request.getParameter("id"));
            if(extendedUserManager.getSubjectById(id) != null) {
                Subject subject = extendedUserManager.getSubjectById(id);
                List<ParticipantRecord> participantRecordList = participantRecordManager.getBySubject(subject);
                mav.addObject("isAdmin", request.isUserInRole(RecruitingConstants.ADMIN_ROLE));
                mav.addObject("subject", subject);
                mav.addObject("participantRecordList", participantRecordList);
            }
        }
        return mav;
    }

    public void setParticipantRecordManager(ParticipantRecordManager participantRecordManager) {
        this.participantRecordManager = participantRecordManager;
    }

    public void setExperimentManager(GenericManager<Experiment, Long> experimentManager) {
        this.experimentManager = experimentManager;
    }

    public void setExtendedUserManager(ExtendedUserManager extendedUserManager) {
        this.extendedUserManager = extendedUserManager;
    }

    public String getInvalidView() {
        return invalidView;
    }

    public void setInvalidView(String invalidView) {
        this.invalidView = invalidView;
    }
    
}
