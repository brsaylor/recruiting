/*
 * Copyright (C) 1998-2009, <a href="http://www.ssel.caltech.edu">SSEL</a>,
 * at Caltech, Pasadena, CA 91125. 
 *
 * MooreRecruiting is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 * USA.
 *
 */

package edu.caltech.ssel.recruiting.webapp.controller;

import edu.caltech.ssel.recruiting.common.DebugHelper;
import edu.caltech.ssel.recruiting.model.Experiment;
import edu.caltech.ssel.recruiting.model.ParticipantRecord;
import edu.caltech.ssel.recruiting.model.ParticipantRecord.PaymentMethod;
import edu.caltech.ssel.recruiting.model.ParticipantRecord.Status;
import edu.caltech.ssel.recruiting.model.Payment;
import edu.caltech.ssel.recruiting.model.Subject;
import edu.caltech.ssel.recruiting.service.ConditionManager;
import edu.caltech.ssel.recruiting.service.ExperimentManager;
import edu.caltech.ssel.recruiting.service.ExtendedUserManager;
import edu.caltech.ssel.recruiting.service.ParticipantRecordManager;
import edu.caltech.ssel.recruiting.service.PaymentManager;
import edu.caltech.ssel.recruiting.service.ReminderManager;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang.StringUtils;
import org.appfuse.model.LabelValue;
import org.appfuse.webapp.controller.BaseFormController;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author michaelk
 */
public class ParticipantRecordFormController extends BaseFormController {
    
    private ParticipantRecordManager participantRecordManager;
    private PaymentManager paymentManager;
    private ExperimentManager experimentManager;
    private ExtendedUserManager extendedUserManager;
    private ConditionManager conditionManager;
    private ReminderManager reminderManager;
    
    /** Creates a new instance of ParticipantRecordFormController */
    public ParticipantRecordFormController() {
        setCommandClass(ParticipantRecord.class);
        setCommandName("participantRecord");
    }
    
    protected Object formBackingObject(HttpServletRequest request) throws Exception {
        String id = request.getParameter("id");
        if(!StringUtils.isEmpty(id)) {
            return participantRecordManager.get(Long.parseLong(id));
        }
        return super.formBackingObject(request);//Use the default constructor, for default settings change that constructor
    }
    
    protected Map referenceData(HttpServletRequest request, Object command, Errors errors) throws Exception {
        Map m = new HashMap();
        int j;

        String id = request.getParameter("id");
        if(!StringUtils.isEmpty(id)) {
            Experiment experiment = (Experiment)request.getSession().getAttribute("experiment");
            m.put("participantList", participantRecordManager.getByExperiment(experiment));
        } else {
            m.put("subjectList",extendedUserManager.getSubjects());
        }
        
        Status[] statusArr = Status.values();
        List<LabelValue> statusList = new ArrayList(statusArr.length);
        for(j = 0; j < statusArr.length; j++)
            statusList.add(new LabelValue(statusArr[j].toString(), statusArr[j].name()));
        m.put("statusList", statusList);
        
        PaymentMethod[] paymentMethodArr = PaymentMethod.values();
        List<LabelValue> paymentMethodList = new ArrayList(paymentMethodArr.length);
        for(j = 0; j < paymentMethodArr.length; j++)
            paymentMethodList.add(new LabelValue(paymentMethodArr[j].toString(), paymentMethodArr[j].name()));
        m.put("paymentMethodList", paymentMethodList);
        
        m.put("isPayGateConfigured", paymentManager.isPayGateConfigured());
        return m;
    }
    
    public ModelAndView processFormSubmission(HttpServletRequest request,
            HttpServletResponse response,
            Object command,
            BindException errors)
            throws Exception {
        
        DebugHelper.displayParams(request, log);
        
        if(request.getParameter("cancel") != null)
            return new ModelAndView(getSuccessView());
        
        return super.processFormSubmission(request, response, command, errors);
    }
    
    protected ModelAndView onSubmit(HttpServletRequest request,
            HttpServletResponse response, Object command,
            BindException errors)
            throws Exception {
        log.debug("Entering onsubmit...");
        
        ParticipantRecord rec = (ParticipantRecord)command;
        Locale locale = request.getLocale();
        String pmtId = request.getParameter("pmtId");
        ModelAndView mv = new ModelAndView(getSuccessView());
        
        //Delete the record if necessary
        if(request.getParameter("delete") != null) {
            participantRecordManager.remove(rec.getId());
            saveMessage(request, getText("participantRecord.deleted", locale));

            // (BRS copied from ParticipantRecordOptionsFormController)
            // cancel experiment reminders for this user
            reminderManager.removeExperimentReminders(
                    rec.getSubject().getId(), rec.getExp());
            
            // open experiment for additional Subjects if it is FULL
            GregorianCalendar now = new GregorianCalendar();
            Experiment experiment = rec.getExp();
            if(Experiment.Status.FULL == experiment.getStatus() &&
                    now.before(experiment.getFreezeTime())) {
                experiment.setStatus(Experiment.Status.OPEN);
                experimentManager.save(experiment);
                saveMessage(request, getText("participantRecord.experimentStatusChanged", locale));
            }
            return mv;
        }
        
        Experiment experiment = (Experiment)request.getSession().getAttribute("experiment");
        Subject user = (Subject)extendedUserManager.getUser(request.getParameter("subject_id"));
        //Set the parameters that are non auto-linked
        rec.setExp(experiment);
        rec.setSubject(user);
        if(StringUtils.isNotEmpty(pmtId)) {
            Payment payment = paymentManager.get(Long.parseLong(pmtId));
            rec.setPayment(payment);
        }
        rec.setStatus(Status.valueOf(request.getParameter("status_name")));

        // BRS 2011-06-08 the "participated" attribute is no longer present in
        // the form since it's redundant, but it is still used elsewhere in the
        // application.  Set it to true if the "status" attribute is
        // "Participated".
        rec.setParticipated(rec.getStatus() == Status.STATUS_2);

        if (request.getParameter("paymentMethod_name") != null) // BRS
            rec.setPaymentMethod(PaymentMethod.valueOf(request.getParameter("paymentMethod_name")));
        rec.setEligible(false);
        if(experiment.getFilter() != null)
            if(conditionManager.isEligible(experiment.getFilter(), user))
                rec.setEligible(true);
        //If the player number and payoff have not been set yet, set them to -1
        /*if(StringUtils.isEmpty("playerNum"))
            rec.setPlayerNum(-1);
        if(StringUtils.isEmpty("payoff"))
            rec.setPayoff(-1);*/
        
        boolean isNew = (rec.getId() == null);
        
        //Save the participant record
        rec = participantRecordManager.save(rec);
        
        if(paymentManager.isPayGateConfigured() && request.getParameter("generatepayment") != null) {
            try {
                String trackingId = paymentManager.sendPayment(rec,
                        getText("paymentGeneration.emailSubject", locale));
                saveMessage(request, getText("paymentGeneration.SUCCESS", locale));
                log.debug("payment generated. trackingId returned: " + trackingId);
            } catch(Exception any) {
                log.error(any.getMessage());
                saveMessage(request, getText("paymentGeneration.FAILED", locale));
            }
        }
        
        if(isNew) {
            saveMessage(request, getText("participantRecord.added", locale));
            if(participantRecordManager.getByExperiment(experiment).size() >= experiment.getMaxNumSubjects()) {
                experiment.setStatus(Experiment.Status.FULL);
                experiment = experimentManager.save(experiment);
            }
        }
        else {
            saveMessage(request, getText("participantRecord.updated", user.getFirstName() + " " + user.getLastName(), locale));
        }
        
        return mv;
    }
    
    public void setConditionManager(ConditionManager conditionManager) {
        this.conditionManager = conditionManager;
    }
    
    public void setExtendedUserManager(ExtendedUserManager extendedUserManager) {
        this.extendedUserManager = extendedUserManager;
    }
    
    public void setParticipantRecordManager(ParticipantRecordManager participantRecordManager) {
        this.participantRecordManager = participantRecordManager;
    }
    
    public void setExperimentManager(ExperimentManager experimentManager) {
        this.experimentManager = experimentManager;
    }

    public void setPaymentManager(PaymentManager paymentManager) {
        this.paymentManager = paymentManager;
    }

    public void setReminderManager(ReminderManager reminderManager) {
        this.reminderManager = reminderManager;
    }
}
