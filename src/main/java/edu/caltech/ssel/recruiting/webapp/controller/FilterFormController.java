/*
 * Copyright (C) 1998-2009, <a href="http://www.ssel.caltech.edu">SSEL</a>,
 * at Caltech, Pasadena, CA 91125. 
 *
 * MooreRecruiting is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 * USA.
 *
 */

package edu.caltech.ssel.recruiting.webapp.controller;

import edu.caltech.ssel.recruiting.common.DebugHelper;
import edu.caltech.ssel.recruiting.common.ExperimentCreationHelper;
import edu.caltech.ssel.recruiting.model.Condition;
import edu.caltech.ssel.recruiting.model.ExperimentTypeCondition;
import edu.caltech.ssel.recruiting.model.ExperimentCondition;
import edu.caltech.ssel.recruiting.model.NoShowCondition;
import edu.caltech.ssel.recruiting.model.PoolCondition;
import edu.caltech.ssel.recruiting.model.QuestionResultCondition;
import edu.caltech.ssel.recruiting.model.Experiment;
import edu.caltech.ssel.recruiting.model.ExperimentType;
import edu.caltech.ssel.recruiting.model.Filter;
import edu.caltech.ssel.recruiting.model.ParticipantRecord;
import edu.caltech.ssel.recruiting.model.Question;
import edu.caltech.ssel.recruiting.model.SubjectPool;
import edu.caltech.ssel.recruiting.service.ConditionManager;
import edu.caltech.ssel.recruiting.service.ExperimentManager;
import edu.caltech.ssel.recruiting.service.ExtendedUserManager;
import edu.caltech.ssel.recruiting.service.ParticipantRecordManager;
import edu.caltech.ssel.recruiting.service.SubjectPoolManager;
import edu.caltech.ssel.recruiting.service.SurveyManager;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.appfuse.model.LabelValue;
import org.appfuse.model.User;
import org.appfuse.service.GenericManager;
import org.appfuse.webapp.controller.BaseFormController;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.servlet.ModelAndView;

/**
* @author Michael Kolodrubetz, SSEL, Caltech
* @author <a href="mailto:wmyuan@hss.caltech.edu">Walter M. Yuan</a>, SSEL, Caltech
* @author Ben Saylor BRS (mostly rewritten)
*/
public class FilterFormController extends BaseFormController {
    
    private GenericManager<ExperimentType, Long> experimentTypeManager;
    private SubjectPoolManager subjectPoolManager;
    private ConditionManager conditionManager;
    private ExtendedUserManager extendedUserManager;
    private ExperimentManager experimentManager;
    private ParticipantRecordManager participantRecordManager;
    private GenericManager<Filter, Long> filterManager;
    private GenericManager<Question, Long> questionManager;
    private SurveyManager surveyManager;
    private static final Log log = LogFactory.getLog(FilterFormController.class);
    private String previousStepView;
    private String nextStepView;
    private String experimentView;
    private boolean useSurveyForm;
    private ExperimentCreationHelper ecHelper;
    private static List progressList;
    
    /** Creates a new instance of FilterFormController */
    public FilterFormController() {
        setCommandClass(Filter.class);
        setCommandName("filter");
        
        progressList = new ArrayList();
        progressList.add(new LabelValue("Basic Information","experimentform.html"));
        progressList.add(new LabelValue("Attach Survey","surveycreationform.html"));
        progressList.add(new LabelValue("Define Filters","filterform.html"));
        progressList.add(new LabelValue("Publish","experimentverificationform.html"));
    }

    //Backing object is a Filter that is passed in via the id
    protected Object formBackingObject(HttpServletRequest request) throws Exception {
//ec        ecHelper.setSession(request.getSession());
        
        log.debug("Entering formBackingObject...");
        
        Filter filter = null;
        String id = request.getParameter("id");
        
        if(StringUtils.isNotEmpty(id)) {
            log.debug("looking up filter by given id");
            try {
                filter = filterManager.get(Long.parseLong(id));
                log.debug("found filter with given id");
            } catch(Exception any) {
                saveMessage(request, "Unable to find filter with the given ID");
            }
        } else {
            log.debug("Filter id not given, looking up filter from Experiment");
            Experiment experiment = (Experiment)request.getSession().getAttribute("experiment");
            if(experiment != null) {
                log.debug("The session contained an experiment (id="+experiment.getId()+")");
                filter = experiment.getFilter();
                log.debug("got filter from experiment");
            } else {
                log.debug("The session did not contain an experiment");
            }
        }
        
        if(filter == null) {
            log.debug("The filter was null - creating a new one");
            filter = new Filter();
            filter.setName("Change It"); // satisfy validator
        } else {
            log.debug("The filter was not null");
            log.debug("Filter id is " + filter.getId());
        }
        
        return filter;
    }
    
    protected Map referenceData(HttpServletRequest request, Object command, Errors errors) throws Exception {
        log.debug("Entering referenceData...");
        
        Map m = new HashMap();
        Filter f = (Filter)command;
        
//ec        Experiment e = ecHelper.getExperiment();
        Experiment experiment = (Experiment)request.getSession().getAttribute("experiment");

        String[][] conditionTypes = {
            {"ExperimentTypeCondition", "Experiment Type Condition"},
            {"ExperimentCondition", "Experiment Condition"},
            {"NoShowCondition", "No-Show Condition"},
            {"PoolCondition", "Pool Condition"},
            {"QuestionResultCondition", "Question Result Condition"}
        };
        m.put("conditionTypes", conditionTypes);

        // 3 options for what filter to use for an experiment:
        // 1.  no filter
        // 2.  default filter of the experiment type
        // 3.  separate filter associated only with this experiment
        String useFilter;
        if (experiment.getFilter() == null) { // instead of checking for f==null, because f is never null
            useFilter = "none";
        } else if (f.getId() == experiment.getType().getDefaultFilter().getId()) {
            useFilter = "default";
        } else {
            useFilter = "separate";
        }
        m.put("useFilter", useFilter);

        // Experiment types
        // getAll() returns duplicates, so need to filter them out
        List<ExperimentType> experimentTypesAll = experimentTypeManager.getAll();
        ArrayList<ExperimentType> experimentTypes = new ArrayList();
        Long prevId = new Long(0);
        for (ExperimentType type : experimentTypesAll) {
            if (type.getId() != prevId) {
                experimentTypes.add(type);
                prevId = type.getId();
            }
        }
        m.put("experimentTypes", experimentTypes);

        // Subject pools
        List<SubjectPool> subjectPools = subjectPoolManager.getValidPools();
        m.put("subjectPools", subjectPools);
        
        // Survey questions
        List<Question> questions;
        if (experiment.getSurvey() != null) {
            questions = experiment.getSurvey().getQuestions();
            if (questions == null) {
                questions = new ArrayList();
            }
        } else {
            questions = new ArrayList();
        }
        m.put("questions", questions);

        // Conditions, as a JSON string.
        if (f == null || f.getConditions() == null) {
            m.put("conditionsJSON", "[]");
        } else {
            StringBuilder json = new StringBuilder();
            json.append("[");
            boolean first = true;
            for (Condition c : f.getConditions()) {
                //log.debug(c.toString());
                if (!first) {
                    json.append(",");
                }
                String type = c.getConditionType();
                json.append("{" +
                            jsonPair("id", c.getId()) + "," +
                            jsonPair("type", c.getConditionType()) + ",");
                if ("ExperimentTypeCondition".equals(type)) {
                    ArrayList<Long> typeIdList = new ArrayList();
                    for (ExperimentType t : ((ExperimentTypeCondition) c).getTypes()) {
                        typeIdList.add(t.getId());
                    }
                    json.append(
                            jsonPair("onList", ((ExperimentTypeCondition) c).isOnList()) + "," +
                            jsonPair("history", ((ExperimentTypeCondition) c).isHistory()) + "," +
                            jsonPair("types", typeIdList));

                } else if ("ExperimentCondition".equals(type)) {
                    json.append(
                            jsonPair("experiment", ((ExperimentCondition) c).getExperiment().getId()) + "," +
                            jsonPair("expType", ((ExperimentCondition) c).getExperiment().getType().getId()) + "," +
                            jsonPair("onList", ((ExperimentCondition) c).isOnList()));

                } else if ("NoShowCondition".equals(type)) {
                    json.append(jsonPair("maxNoShows", ((NoShowCondition) c).getMaxNoShows()));

                } else if ("PoolCondition".equals(type)) {
                    ArrayList<Long> poolIdList = new ArrayList();
                    for (SubjectPool p : ((PoolCondition) c).getPools()) {
                        poolIdList.add(p.getId());
                    }
                    json.append(
                            jsonPair("pools", poolIdList) + "," +
                            jsonPair("memberOf", ((PoolCondition) c).isMemberOf()));

                } else if ("QuestionResultCondition".equals(type)) {
                    json.append(
                            jsonPair("question", ((QuestionResultCondition) c).getQuestion().getId()) + ", " +
                            jsonPair("filterOptions", new ArrayList(((QuestionResultCondition) c).getFilterOptions())) + ", " +
                            jsonPair("filterType", ((QuestionResultCondition) c).getFilterType().name()));
                }

                json.append("}");
                first = false;
            }
            json.append("]");
            m.put("conditionsJSON", json);
        }

        List<Question> profQs = surveyManager.getProfileSurvey().getQuestions();
        Set<Question> survQs;
        if(experiment == null)
            m.put("qlist", questionManager.getAll());
        else {
            //They can filter on the profile questions as well as the survey questions
            if(experiment.getSurvey() != null)
                survQs = new HashSet(experiment.getSurvey().getQuestions());
            else
                survQs = new HashSet();
            survQs.addAll(profQs);
            m.put("qlist", survQs);
        }
        
        m.put("filterList", filterManager.getAll());
        m.put("condList", conditionManager.getAll());
        m.put("progressList", progressList);
        m.put("currentStep", "Define Filters");
        
        return m;
    }

    // Convenience method that returns a JSON key-value pair from the given key and value.
    private String jsonPair(String key, String value) {
        return "\"" + key + "\": \"" + value + "\"";
    }
    private String jsonPair(String key, int value) {
        return "\"" + key + "\": " + value;
    }
    private String jsonPair(String key, Long value) {
        return "\"" + key + "\": " + value.toString();
    }
    private String jsonPair(String key, List values) {
        return "\"" + key + "\": " + "[" + StringUtils.join(values, ",") + "]";
    }
    private String jsonPair(String key, boolean value) {
        return "\"" + key + "\": " + (new Boolean(value)).toString();
    }
    
    //We need to make sure that the experiment gets cleared if the user clicks "cancel"
    public ModelAndView processFormSubmission(HttpServletRequest request,
            HttpServletResponse response,
            Object command,
            BindException errors)
            throws Exception {
        
        log.debug("in processFormSubmission");
        DebugHelper.displayParams(request, log);
        
        ModelAndView mav = null;
        
        if(request.getParameter("cancel") != null) {
            Experiment experiment = (Experiment)request.getSession().getAttribute("experiment");
            if(experiment == null)
                mav = new ModelAndView(getCancelView());
            else if(experiment.getId() == null) {
                mav = new ModelAndView(getCancelView());
                request.getSession().removeAttribute("experiment");
            } else
                mav = new ModelAndView(getExperimentView());
            return mav;
        } else if(request.getParameter("back") != null) {
            mav = new ModelAndView(getPreviousStepView());
            return mav;
        }
        return super.processFormSubmission(request, response, command, errors);
    }
    
    protected ModelAndView onSubmit(HttpServletRequest request,
            HttpServletResponse response, Object command,
            BindException errors)
            throws Exception {
        log.debug("In onSubmit...");
        
        Filter filter = (Filter) command;
        Experiment experiment = (Experiment)request.getSession().getAttribute("experiment");
        
        String userName = request.getRemoteUser();
        //Set current user as reservedBy parameter
        User u = extendedUserManager.getUserByUsername(userName);

        // If user changed the value of useFilter, then they clicked on one of
        // the following submit buttons to confirm the action or choose among
        // different actions.
        String useFilter = request.getParameter("useFilter");
        boolean useFilterChanged = false;
        if (request.getParameter("apply") != null) {
            useFilterChanged = true;
            if ("none".equals(useFilter)) {
                filter = null;
            } else { // useFilter == default
                filter = experiment.getType().getDefaultFilter();
            }
        } else if (request.getParameter("discard") != null) {
            useFilterChanged = true;
            if ("none".equals(useFilter)) {
                filter = null;
            } else { // useFilter == default
                filter = experiment.getType().getDefaultFilter();
            }
        } else if (request.getParameter("createNew") != null) {
            useFilterChanged = true;
            filter = new Filter();
        } else if (request.getParameter("copyDefault") != null) {
            useFilterChanged = true;
            filter = experiment.getType().getDefaultFilter().clone();
            filter.setName(null);
        } else if (request.getParameter("saveAsDefault") != null) {
            useFilterChanged = true;
            Filter defaultFilter = experiment.getType().getDefaultFilter();
            defaultFilter.setConditions(filter.getConditions());
            filter = defaultFilter;
        }

        if (useFilterChanged) {
            // The user selected different option for the filter, so reload the filter view.
            
            if (filter != null) {
                filter = filterManager.save(filter);
            }
            experiment.setFilter(filter);
            experiment = experimentManager.save(experiment);

            return new ModelAndView("redirect:filterform.html");
        }

        if (request.getParameter("saveCondition") != null) {

            // Save a condition.

            Condition savedCondition = null;
            Long id;
            try {
                id = Long.parseLong(request.getParameter("conditionId"));
            } catch (Exception e) {
                id = null;
            }
            log.debug("** id = " + id);
            String type = request.getParameter("conditionType");

            // Save an ExperimentTypeCondition
            if ("ExperimentTypeCondition".equals(type)) {
                ExperimentTypeCondition c = id == null ? new ExperimentTypeCondition() : (ExperimentTypeCondition) conditionManager.get(id);
                c.setOnList(new Boolean(request.getParameter("onList")));
                c.setHistory(new Boolean(request.getParameter("history")));
                ArrayList<ExperimentType> types = new ArrayList();
                if (request.getParameterValues("experimentTypeIds") != null) {
                    for (String typeId : request.getParameterValues("experimentTypeIds")) {
                        types.add(experimentTypeManager.get(Long.parseLong(typeId)));
                    }
                }
                c.setTypes(types);
                savedCondition = conditionManager.save(c);
                log.debug("savedCondition = " + savedCondition.toString());

            // Save an ExperimentCondition
            } else if ("ExperimentCondition".equals(type)) {
                ExperimentCondition c = id == null ? new ExperimentCondition() : (ExperimentCondition) conditionManager.get(id);
                c.setOnList(new Boolean(request.getParameter("onList")));
                c.setExperiment(experimentManager.get(Long.parseLong(request.getParameter("experiment"))));
                savedCondition = conditionManager.save(c);
                log.debug("savedCondition = " + savedCondition.toString());

            // Save a NoShowCondition
            } else if ("NoShowCondition".equals(type)) {
                NoShowCondition c = id == null ? new NoShowCondition() : (NoShowCondition) conditionManager.get(id);

                int maxNoShows;
                try {
                    maxNoShows = Integer.parseInt(request.getParameter("maxNoShows"));
                } catch (Exception e) {
                    maxNoShows = 0;
                }
                c.setMaxNoShows(maxNoShows);
                savedCondition = conditionManager.save(c);
                log.debug("savedCondition = " + savedCondition.toString());

            // Save a PoolCondition
            } else if ("PoolCondition".equals(type)) {
                PoolCondition c = id == null ? new PoolCondition() : (PoolCondition) conditionManager.get(id);
                c.setMemberOf(new Boolean(request.getParameter("memberOf")));
                ArrayList<SubjectPool> pools = new ArrayList();
                if (request.getParameterValues("subjectPoolIds") != null) {
                    for (String poolId : request.getParameterValues("subjectPoolIds")) {
                        pools.add(subjectPoolManager.get(Long.parseLong(poolId)));
                    }
                }
                c.setPools(pools);
                savedCondition = conditionManager.save(c);
                log.debug("savedCondition = " + savedCondition.toString());

            // Save a QuestionResultCondition
            } else if ("QuestionResultCondition".equals(type)) {
                QuestionResultCondition c = id == null ? new QuestionResultCondition() : (QuestionResultCondition) conditionManager.get(id);
                c.setQuestion(questionManager.get(Long.parseLong(request.getParameter("question"))));
                Set<Integer> filterOptions = new HashSet();
                if (request.getParameterValues("questionOptions") != null) {
                    for (String optionIndex: request.getParameterValues("questionOptions")) {
                        filterOptions.add(Integer.parseInt(optionIndex));
                    }
                }
                c.setFilterOptions(filterOptions);
                c.setFilterType(QuestionResultCondition.FilterType.valueOf(request.getParameter("filterType")));
                savedCondition = conditionManager.save(c);
                log.debug("savedCondition = " + savedCondition.toString());
            }

            if (id == null) {
                // New condition: add it to the filter.
                if (savedCondition != null) {
                    filter.addCondition(savedCondition);
                }
                filter = filterManager.save(filter);
            } else {
                // Existing condition: reload the filter to bring in the saved condition from the database.
                filter = filterManager.get(filter.getId());
            }

            experiment.setFilter(filter);

            return new ModelAndView("redirect:filterform.html");
        }

        if (request.getParameter("deleteCondition") != null) {

            log.debug("deleteCondition");

            // Delete a condition.

            Long id;
            try {
                id = Long.parseLong(request.getParameter("deleteConditionId"));
            } catch (Exception e) {
                id = null;
            }
            
            if (id == null) {
                // Invalid condition
                log.warn("Invalid condition id - can't delete");
                return new ModelAndView("redirect:filterform.html");
            } else {
                log.debug("id = " + id.toString());
            }

            List<Condition> conditions = filter.getConditions();
            for (int i = 0; i < conditions.size(); i++) {
                log.debug("i = " + i + ", id = " + conditions.get(i).getId().toString());
                if (id.equals(conditions.get(i).getId())) {
                    log.debug("found condition; removing");
                    conditions.remove(i);
                    break;
                }
            }

            filter.setConditions(conditions);
            filter = filterManager.save(filter);
            experiment.setFilter(filter);
            return new ModelAndView("redirect:filterform.html");
        }

        // verify IRB checked
        if (StringUtils.isBlank(request.getParameter("checkedIRB")) && !("none".equals(useFilter))) {
            errors.rejectValue("checkedIRB", "filterForm.uncheckedIRB", "unchecked IRB");
            return showForm(request, response, errors);
        }
        
        //Set the success view
        ModelAndView mav = null;
        if(request.getParameter("next") != null) {

            // BRS 2012-01-27 workaround
            experiment = experimentManager.save(experiment);
            request.getSession().setAttribute("experiment", experiment);

            mav = new ModelAndView(getNextStepView());
        } else if(StringUtils.isNotEmpty(request.getParameter("savenow"))) {
            experiment = experimentManager.save(experiment);
            request.getSession().setAttribute("experiment", experiment);
            return new ModelAndView(getExperimentView());
        } else { // bail out
            log.warn("unexpected submit value for onSubmit");
            mav = new ModelAndView(getCancelView());
        }
        
        return mav;
    }
    
    public void setExperimentTypeManager(GenericManager<ExperimentType, Long> experimentTypeManager) {
        this.experimentTypeManager = experimentTypeManager;
    }
    
    public void setSubjectPoolManager(SubjectPoolManager subjectPoolManager) {
        this.subjectPoolManager = subjectPoolManager;
    }
    
    public void setFilterManager(GenericManager<Filter, Long> filterManager) {
        this.filterManager = filterManager;
    }
    
    public void setExperimentManager(ExperimentManager experimentManager) {
        this.experimentManager = experimentManager;
    }

    public void setParticipantRecordManager(ParticipantRecordManager participantRecordManager) {
        this.participantRecordManager = participantRecordManager;
    }
    
    public void setConditionManager(ConditionManager conditionManager) {
        this.conditionManager = conditionManager;
    }
    
    public void setExperimentCreationHelper(ExperimentCreationHelper ecHelper) {
        this.ecHelper = ecHelper;
    }
    
    public void setQuestionManager(GenericManager<Question, Long> questionManager) {
        this.questionManager = questionManager;
    }
    
    public void setSurveyManager(SurveyManager surveyManager) {
        this.surveyManager = surveyManager;
    }

    public void setExtendedUserManager(ExtendedUserManager extendedUserManager) {
        this.extendedUserManager = extendedUserManager;
    }
    
    public void setNextStepView(String nextStepView) {
        this.nextStepView = nextStepView;
    }

    public void setPreviousStepView(String previousStepView) {
        this.previousStepView = previousStepView;
    }

    public void setExperimentView(String experimentView) {
        this.experimentView = experimentView;
    }

    public String getPreviousStepView() {
        return previousStepView;
    }

    public String getNextStepView() {
        return nextStepView;
    }

    public String getExperimentView() {
        return experimentView;
    }
    
}
