/*
 * Copyright (C) 1998-2009, <a href="http://www.ssel.caltech.edu">SSEL</a>,
 * at Caltech, Pasadena, CA 91125. 
 *
 * MooreRecruiting is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 * USA.
 *
 */

package edu.caltech.ssel.recruiting.webapp.controller;

import edu.caltech.ssel.recruiting.common.DebugHelper;
import edu.caltech.ssel.recruiting.model.Location;
import edu.caltech.ssel.recruiting.service.ExperimentManager;
import edu.caltech.ssel.recruiting.service.ExperimentTypeManager;
import edu.caltech.ssel.recruiting.service.ExtendedUserManager;
import edu.caltech.ssel.recruiting.service.ParticipantRecordManager;
import edu.caltech.ssel.recruiting.service.SubjectPoolManager;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.appfuse.service.GenericManager;
import org.appfuse.service.RoleManager;
import org.appfuse.webapp.controller.BaseFormController;

import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.security.context.SecurityContextHolder;

/**
 * Form Controller for parsing input to import a CasselWeb2 database into CasselWeb3
 * The service assumes the standard CasselWeb2 schema in a MySQL Database.
 * The command object is an application scope ImportC2Utility object, which has
 * it's own thread of execution, since it's job is to manage a potentially long running
 * operation (importing data from CasselWeb2 to CasselWeb3).
 */

/**
* @author Richar Ho, CASSEL, UCLA
* @author <a href="mailto:wmyuan@hss.caltech.edu">Walter M. Yuan</a>, SSEL, Caltech
*/
public class ImportC2FormController extends BaseFormController {
    
    protected ExperimentManager experimentManager;
    protected ExtendedUserManager extendedUserManager;
    protected ParticipantRecordManager participantRecordManager;
    protected ExperimentTypeManager experimentTypeManager;
    protected GenericManager<Location, Long> locationManager;
    protected RoleManager roleManager;
    protected SubjectPoolManager subjectPoolManager;

    public static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";

    
    /** Creates a new instance of ExperimentTypeFormController */
    public ImportC2FormController() {

        // BRS
        // Importing was mostly failing with org.springframework.security.AuthenticationCredentialsNotFoundException.
        // This was because the ImportC2UtilityThread was not inheriting the
        // SecurityContext.  This should fix that.
        SecurityContextHolder.setStrategyName( SecurityContextHolder.MODE_INHERITABLETHREADLOCAL );

        setCommandClass(ImportC2Utility.class);
        setCommandName("importC2Utility");
    }
    
    protected Object formBackingObject(HttpServletRequest request) throws Exception {
        log.debug("Entering formBackingObject...");
        ImportC2Utility importc2 = (ImportC2Utility)
                request.getSession().getServletContext().getAttribute("importc2");
        if(importc2 == null) {
            log.debug("importc2 NOT found in application context");
            importc2 = new ImportC2Utility();
        } else
            log.debug("found importc2 in application context");
        
        return importc2;
    }
    
    protected Map referenceData(HttpServletRequest request) throws Exception {
        log.debug("Entering referenceData...");
        Map m = new HashMap();
        //m.put("something", something);
        return m;
    }
    
    public ModelAndView processFormSubmission(HttpServletRequest request,
            HttpServletResponse response,
            Object command,
            BindException errors)
            throws Exception {
        
        log.debug("in processFormSubmission");
        DebugHelper.displayParams(request, log);
        Locale locale = request.getLocale();
        ImportC2Utility importc2 = (ImportC2Utility) command;
        
        if(request.getParameter("cancel") != null)
            return new ModelAndView(getCancelView());
        else if(request.getParameter("quit") != null) {
            if(importc2 != null) {
                importc2.setQuit(true);
                saveMessage(request, getText("importC2.stopped", locale));
            } else {
                saveMessage(request, getText("importC2.notrunning", locale));
            }
            return new ModelAndView(getCancelView());
        } else
            return super.processFormSubmission(request, response, command, errors);
    }
    
    public ModelAndView onSubmit(HttpServletRequest request,
            HttpServletResponse response, Object command,
            BindException errors)
            throws Exception {
        
        Locale locale = request.getLocale();

        // There can only be one import running. So, if the command object
        // represents a live import, kill it
        ImportC2Utility importc2 = (ImportC2Utility) command;
        if(importc2 != null && importc2.isRan()) {
            saveMessage(request, getText("importC2.oldstopped", locale));
            importc2.setQuit(true);
        }
        
        // make database connection...
        String hostname = request.getParameter("hostname");
        String port = request.getParameter("port");
        String c2name = request.getParameter("c2name");
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        
        try {
            importc2 = new ImportC2Utility(
                        hostname,
                        port,
                        c2name,
                        username,
                        password);
            if(importc2.getConnErrMsg() != null) {
                saveMessage(request, getText("importC2.failed", importc2.getConnErrMsg(), locale));
                return new ModelAndView(getSuccessView()); // return to form
            }
            
            importc2.setController(this);
            Thread importc2Thread = new Thread(importc2);
            importc2Thread.setName("ImportC2Utility");
            importc2Thread.start();

            request.getSession().getServletContext().setAttribute("importc2", importc2);
            saveMessage(request, getText("importC2.started", locale));
            
        } catch(Exception any) {
            saveMessage(request, getText("importC2.failed", any.getMessage(), locale));
            return new ModelAndView(getSuccessView()); // return to form
        }
        
        return new ModelAndView(getSuccessView());
    }

    public void setExperimentManager(ExperimentManager experimentManager) {
        this.experimentManager = experimentManager;
    }

    public void setExtendedUserManager(ExtendedUserManager extendedUserManager) {
        this.extendedUserManager = extendedUserManager;
    }

    public void setExperimentTypeManager(ExperimentTypeManager experimentTypeManager) {
        this.experimentTypeManager = experimentTypeManager;
    }

    public void setRoleManager(RoleManager roleManager) {
        this.roleManager = roleManager;
    }

    public void setParticipantRecordManager(ParticipantRecordManager participantRecordManager) {
        this.participantRecordManager = participantRecordManager;
    }

    public void setLocationManager(GenericManager<Location, Long> locationManager) {
        this.locationManager = locationManager;
    }

    public void setSubjectPoolManager(SubjectPoolManager subjectPoolManager) {
        this.subjectPoolManager = subjectPoolManager;
    }
}
