/*
 * Copyright (C) 1998-2009, <a href="http://www.ssel.caltech.edu">SSEL</a>,
 * at Caltech, Pasadena, CA 91125. 
 *
 * MooreRecruiting is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 * USA.
 *
 */

package edu.caltech.ssel.recruiting.webapp.controller;

import edu.caltech.ssel.recruiting.common.DebugHelper;
import edu.caltech.ssel.recruiting.common.ExperimentCreationHelper;
import edu.caltech.ssel.recruiting.model.Experiment;
import edu.caltech.ssel.recruiting.model.Filter;
import edu.caltech.ssel.recruiting.model.Survey;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.appfuse.model.LabelValue;
import org.appfuse.service.GenericManager;
import org.appfuse.webapp.controller.BaseFormController;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.servlet.ModelAndView;

/**
* @author Michael Kolodrubetz, SSEL, Caltech
* @author <a href="mailto:wmyuan@hss.caltech.edu">Walter M. Yuan</a>, SSEL, Caltech
*/
public class ExperimentVerificationFormController extends BaseFormController {
    
    private GenericManager<Experiment, Long> experimentManager;
    private GenericManager<Filter, Long> filterManager;
    private GenericManager<Survey, Long> surveyManager;
    private ExperimentCreationHelper ecHelper;
    private String previousStepView;
    private String experimentView;
    private String nextStepView;
    private static List progressList;
    
    /**
     * Creates a new instance of ExperimentVerificationFormController
     */
    public ExperimentVerificationFormController() {
        setCommandClass(Experiment.class);
        setCommandName("experiment");
        
        progressList = new ArrayList();
        progressList.add(new LabelValue("Basic Information","experimentform.html"));
        progressList.add(new LabelValue("Attach Survey","surveycreationform.html"));
        progressList.add(new LabelValue("Define Filters","filterform.html"));
        progressList.add(new LabelValue("Publish","experimentverificationform.html"));
    }
    
    public void setExperimentManager(GenericManager<Experiment, Long> experimentManager) {
        this.experimentManager = experimentManager;
    }
    
    public Object formBackingObject(HttpServletRequest request) {
//        ecHelper.setSession(request.getSession());
        
//        return ecHelper.getExperiment();
        
        log.debug("Entering formBackingObject...");
        
        return request.getSession().getAttribute("experiment");
    }
    
    protected Map referenceData(HttpServletRequest request, Object command, Errors errors) throws Exception {
        Experiment e = (Experiment)command;
        Map m = new HashMap();
        
        if(e.getFilter() != null) {
            Filter f = filterManager.get(e.getFilter().getId());
            m.put("filterName", f.getName());
            m.put("filterSize", Integer.toString(f.getConditions().size()));
        }
        if(e.getSurvey() != null) {
            Survey s = surveyManager.get(e.getSurvey().getId());
            m.put("surveyName", s.getName());
            m.put("surveySize", Integer.toString(s.getQuestions().size()));
        }
        m.put("progressList", progressList);
        m.put("currentStep", "Publish");
        
        return m;
    }
    
    public ModelAndView processFormSubmission(HttpServletRequest request,
            HttpServletResponse response,
            Object command,
            BindException errors)
            throws Exception {
        
        log.debug("in processFormSubmission");
        DebugHelper.displayParams(request, log);
        
        ModelAndView mav = null;
        
        if(request.getParameter("cancel") != null) {
            Experiment experiment = (Experiment)request.getSession().getAttribute("experiment");
            if(experiment.getId() == null) {
                mav = new ModelAndView(getCancelView());
                request.getSession().removeAttribute("experiment");
            } else
                mav = new ModelAndView(getExperimentView());
            return mav;
        } else if(request.getParameter("back") != null) {
            mav = new ModelAndView(getPreviousStepView());
            return mav;
        }
        return super.processFormSubmission(request, response, command, errors);
    }
    
    public ModelAndView onSubmit(HttpServletRequest request,
            HttpServletResponse response, Object command,
            BindException errors)
            throws Exception {
        log.debug("In onSubmit...");
        
        SimpleDateFormat sdf = new SimpleDateFormat("MM-dd-yyyy");
        Experiment experiment = (Experiment)request.getSession().getAttribute("experiment");
        boolean publish = (request.getParameter("publish") != null);
        boolean makeAnnouncement = (request.getParameter("makeAnnouncement") != null);
        
        if(publish)
            experiment.setPublished(new Boolean(true));
        
        experiment = experimentManager.save(experiment);
        request.getSession().setAttribute("experiment", experiment);
        saveMessage(request, getText("experimentVerificationForm.success", request.getLocale()));
        
        //Set the success view
        ModelAndView mav = null;
        if(makeAnnouncement && publish)
            mav = new ModelAndView(getNextStepView());
        else if(publish || request.getParameter("savenow") != null)
            mav = new ModelAndView(getSuccessView()).addObject(
                    "date", sdf.format(experiment.getExpDate()));
        else { // bail out
            log.warn("unexpected submit value for onSubmit");
            mav = new ModelAndView(getCancelView());
        }
        
        return mav;
    }
    
    public void setExperimentCreationHelper(ExperimentCreationHelper ecHelper) {
        this.ecHelper = ecHelper;
    }
    
    public void setFilterManager(GenericManager<Filter, Long> filterManager) {
        this.filterManager = filterManager;
    }
    
    public void setSurveyManager(GenericManager<Survey, Long> surveyManager) {
        this.surveyManager = surveyManager;
    }
    
    public void setNextStepView(String nextStepView) {
        this.nextStepView = nextStepView;
    }

    public void setPreviousStepView(String previousStepView) {
        this.previousStepView = previousStepView;
    }

    public void setExperimentView(String experimentView) {
        this.experimentView = experimentView;
    }

    public String getPreviousStepView() {
        return previousStepView;
    }

    public String getExperimentView() {
        return experimentView;
    }

    public String getNextStepView() {
        return nextStepView;
    }
    
}
