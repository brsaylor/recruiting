/*
 * Copyright (C) 1998-2009, <a href="http://www.ssel.caltech.edu">SSEL</a>,
 * at Caltech, Pasadena, CA 91125. 
 *
 * MooreRecruiting is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 * USA.
 *
 */

package edu.caltech.ssel.recruiting.service.impl;

import edu.caltech.ssel.recruiting.dao.ExperimentDao;
import edu.caltech.ssel.recruiting.dao.ReminderDao;
import edu.caltech.ssel.recruiting.dao.ParticipantRecordDao;
import edu.caltech.ssel.recruiting.model.Experiment;
import edu.caltech.ssel.recruiting.model.ParticipantRecord;
import edu.caltech.ssel.recruiting.model.Reminder;
import edu.caltech.ssel.recruiting.model.Subject;
import edu.caltech.ssel.recruiting.service.EmailManager;
import edu.caltech.ssel.recruiting.service.ReminderManager;

import org.appfuse.model.User;
import org.appfuse.service.impl.GenericManagerImpl;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import org.appfuse.Constants;
import org.springframework.context.support.ResourceBundleMessageSource;

/**
 *
 */
public class ReminderManagerImpl extends GenericManagerImpl<Reminder, Long> implements ReminderManager {
    
    ReminderDao dao;
    private ExperimentDao experimentDao;
    private ParticipantRecordDao participantRecordDao;
    private EmailManager emailManager;
    private ResourceBundleMessageSource messageSource;
    private String templateName;
    private String smsTemplateName;
   
    private static final Log staticlog = LogFactory.getLog(ReminderManager.class);
        
    /** Creates a new instance of ReminderManagerImpl */
    public ReminderManagerImpl(ReminderDao dao) {
        super(dao);
        this.dao = dao;
    }
    
    public void processExperimentReminders()
    throws Exception {
        
        staticlog.info("*** processing experiment reminders ***");
        Date now = new Date();
        List<Reminder> reminders = dao.findRemindersToProcess();
        for(Reminder reminder : reminders) {
            if(reminder.getReminderType().equals(Reminder.ReminderType.EXPERIMENT)) {
                Experiment experiment = (Experiment)experimentDao.get(reminder.getRefId());
                Date expDate = experiment.getExpDate();
                Date expTime = experiment.getStartTime();

                if (!pastExperiment(expDate, expTime) && experiment.getStatus() != Experiment.Status.CANCELLED) {
                    log.debug("*** sending "+reminder.getReminderType()+" reminder to: "+reminder.getUser().getFullName());
                    // is the reminder user still a participant?
                    ParticipantRecord prec = participantRecordDao.getByExperimentAndSubject(
                            experiment.getId(), reminder.getUser().getId());

                    Map model = new HashMap();
                    Object[] params = new Object[1];
                    params[0] = experiment.getId();
                    model.put(Constants.EMAIL_SUBJECT, this.messageSource.getMessage("experiment.reminder", params, Locale.getDefault()));

                    model.put(Constants.USER, reminder.getUser());
                    model.put(Constants.EXPERIMENT, experiment);
                    model.put("signedup", (prec != null ? "signed up" : "NOT signed up"));
                    model.put("labName", this.messageSource.getMessage("company.name", null, Locale.getDefault()));
                    model.put("labUrlName", this.messageSource.getMessage("company.url", null, Locale.getDefault()));
                    model.put("labPhone", this.messageSource.getMessage("company.phone", null, Locale.getDefault()));

                    // BRS - if it's an SMS reminder, send SMS message
                    if (reminder.getSms()) {
                        log.debug("it's an SMS reminder");
                        try {
                            model.put(Constants.EMAIL_TO, ((Subject)reminder.getUser()).getMobileNumber() + "@" + ((Subject)reminder.getUser()).getSmsGateway());
                            log.debug("About to call sendEmail() for SMS reminder");
                            emailManager.sendEmail(smsTemplateName, model);
                            reminder.setStatus(Reminder.Status.PROCESSED);
                        } catch(Exception any) {
                            log.warn("an experiment SMS reminder failed: "+any.getMessage());
                            reminder.setStatus(Reminder.Status.FAILED);
                        }

                    } else {
                        log.debug("it's an email reminder");
                        // It's an email reminder

                        // BRS - don't email unsubscribed users
                        if (!((Subject)reminder.getUser()).getSubscribed()) {
                            reminder.setStatus(Reminder.Status.WONTPROCESS);
                            log.info("Not emailing unsubscribed subject " + reminder.getUser().getEmail());
                        } else {
                            try {
                                model.put(Constants.EMAIL_TO, reminder.getUser().getEmail());
                                log.debug("About to call sendEmail() for email reminder");
                                emailManager.sendEmail(templateName, model);
                                reminder.setStatus(Reminder.Status.PROCESSED);
                            } catch(Exception any) {
                                log.warn("an experiment email reminder failed: "+any.getMessage());
                                reminder.setStatus(Reminder.Status.FAILED);
                            }
                        }
                    }


                } else {
                    // Mark reminders for past and cancelled experiments as WONTPROCESS
                    reminder.setStatus(Reminder.Status.WONTPROCESS);
                }
                reminder = dao.save(reminder);
            }
        }
    }

    public void removeExperimentReminders(Long userId, Experiment experiment) throws Exception {
        List<Reminder> reminders = dao.findUnprocessedRemindersByUserId(userId);
        for(Reminder reminder : reminders) {
            if(reminder.getReminderType().equals(Reminder.ReminderType.EXPERIMENT)) {
                Experiment reminderExperiment = (Experiment)experimentDao.get(reminder.getRefId());
                if(reminderExperiment.equals(experiment))
                    dao.remove(reminder.getId());
            }
        }
    }

    public List<Reminder> createExperimentReminders(User user, Experiment experiment) throws Exception {

        log.debug("createExperimentReminders");
        
        // cancel existing reminders
        removeExperimentReminders(user.getId(), experiment);
        
        // create date representing startDate
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(experiment.getExpDate());
        
        Calendar timeCal = new GregorianCalendar();
        timeCal.setTime(experiment.getStartTime());
        
        calendar.set(Calendar.HOUR_OF_DAY, timeCal.get(Calendar.HOUR_OF_DAY));
        calendar.set(Calendar.MINUTE, timeCal.get(Calendar.MINUTE));
        
        // for creating date/times offset from the experiment date/time without
        // modifying the original
        GregorianCalendar tmpCal;

        List<Reminder> reminderList = new ArrayList();
        
        // set first reminder for -1 day
        /*calendar.add(Calendar.HOUR_OF_DAY, -24);
        Reminder reminder = new Reminder(
                user, Reminder.ReminderType.EXPERIMENT, experiment.getId(),
                calendar.getTime());
        reminderList.add(dao.save(reminder));
        */
        //@modified by wmyuan: one reminder is sufficient
        // set second reminder for -12 hours
        // BRS: replace 12 with -12 (want to schedule before, not after
        // experiment!)
        tmpCal = (GregorianCalendar) calendar.clone();
        tmpCal.add(Calendar.HOUR_OF_DAY, -12);
        Reminder reminder = new Reminder(
                user, Reminder.ReminderType.EXPERIMENT, experiment.getId(),
                tmpCal.getTime());
        reminderList.add(dao.save(reminder));

        // BRS: if subject wants SMS reminders, create an SMS reminder for 1 hour prior to experiment.
        if (((Subject)user).getReceiveSMSReminders() != null && ((Subject)user).getReceiveSMSReminders()) {
            tmpCal = (GregorianCalendar) calendar.clone();
            tmpCal.add(Calendar.HOUR_OF_DAY, -1);
            reminder = new Reminder(
                    user, Reminder.ReminderType.EXPERIMENT, experiment.getId(),
                    tmpCal.getTime());
            reminder.setSms(true);
            reminderList.add(dao.save(reminder));
        }
        
        // set final reminder for -6 hours
        /*
        calendar.add(Calendar.HOUR_OF_DAY, 6);
        reminder = new Reminder(
                user, Reminder.ReminderType.EXPERIMENT, experiment.getId(),
                calendar.getTime());
        reminderList.add(dao.save(reminder));
        */
        return reminderList;
    }

    public void updateExperimentReminders(Experiment experiment) throws Exception {
        List<Reminder> reminders = dao.findUnprocessedExperimentRemindersByExperimentId(experiment.getId());
        for(Reminder reminder : reminders) {
            User user = reminder.getUser();
            createExperimentReminders(user, experiment);
        }
    }

    private boolean pastExperiment(Date expDate, Date expTime) {
        
        Date now = new Date();
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(expDate);
        
        Calendar timeCal = new GregorianCalendar();
        timeCal.setTime(expTime);
        
        calendar.set(Calendar.HOUR_OF_DAY, timeCal.get(Calendar.HOUR_OF_DAY));
        calendar.set(Calendar.MINUTE, timeCal.get(Calendar.MINUTE));

        if(now.after(calendar.getTime()))
            return true;
        else
            return false;
    }
    
    /**
     * @param emailManager the emailManager to set
     */
    public void setEmailManager(EmailManager emailManager) {
        this.emailManager = emailManager;
    }

    /**
     * @param messageSource the messageSource to set
     */
    public void setMessageSource(ResourceBundleMessageSource messageSource) {
        this.messageSource = messageSource;
    }

    /**
     * @param templateName the templateName to set
     */
    public void setTemplateName(String templateName) {
        this.templateName = templateName;
    }

    /**
     * @param smsTemplateName the templateName to set
     */
    public void setSmsTemplateName(String smsTemplateName) {
        this.smsTemplateName = smsTemplateName;
    }

    /**
     * @param experimentDao the experimentDao to set
     */
    public void setExperimentDao(ExperimentDao experimentDao) {
        this.experimentDao = experimentDao;
    }

    /**
     * @param participantRecordDao the participantRecordDao to set
     */
    public void setParticipantRecordDao(ParticipantRecordDao participantRecordDao) {
        this.participantRecordDao = participantRecordDao;
    }
    
}
