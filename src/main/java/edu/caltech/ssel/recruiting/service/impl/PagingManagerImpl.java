/*
 * Copyright (C) 1998-2009, <a href="http://www.ssel.caltech.edu">SSEL</a>,
 * at Caltech, Pasadena, CA 91125. 
 *
 * MooreRecruiting is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 * USA.
 *
 */

package edu.caltech.ssel.recruiting.service.impl;

import edu.caltech.ssel.recruiting.common.ExtendedPaginatedList;
import edu.caltech.ssel.recruiting.dao.PagingDao;
import edu.caltech.ssel.recruiting.service.PagingManager;
import org.appfuse.service.impl.UniversalManagerImpl;

/**
 *
 * @author Richard Ho, CASSEL, UCLA
 */
public class PagingManagerImpl extends UniversalManagerImpl implements PagingManager {
    private PagingDao pagingDao;

    /**
     * Method that allows setting the DAO to talk to the data store with.
     * 
     * @param dao the dao implementation
     */
    public void setLookupDao(PagingDao pagingDao) {
        super.dao = pagingDao;
        this.pagingDao = pagingDao;
    }

    public ExtendedPaginatedList getNamedQueryResults(String queryName, ExtendedPaginatedList page) {
        return pagingDao.getNamedQueryResults(queryName, page);
    }

    @SuppressWarnings("unchecked")
    public ExtendedPaginatedList getAllUsers(ExtendedPaginatedList page) {
       return pagingDao.getAllUsers(page);
    }
    
    @SuppressWarnings("unchecked")
    public ExtendedPaginatedList getSubjects(ExtendedPaginatedList page) {
        return pagingDao.getSubjects(page);
    }
    
    @SuppressWarnings("unchecked")
    public ExtendedPaginatedList getExperimenters(ExtendedPaginatedList page) {
        return pagingDao.getExperimenters(page);
    }
    
    @SuppressWarnings("unchecked")
    public ExtendedPaginatedList getAdmins(ExtendedPaginatedList page) {
        return pagingDao.getAdmins(page);
    }
    
    @SuppressWarnings("unchecked")
    public ExtendedPaginatedList getExperimentTypes(final ExtendedPaginatedList page) {
        return pagingDao.getExperimentTypes(page);
    }

    public PagingDao getPagingDao() {
        return pagingDao;
    }

    public void setPagingDao(PagingDao pagingDao) {
        this.pagingDao = pagingDao;
    }
}
