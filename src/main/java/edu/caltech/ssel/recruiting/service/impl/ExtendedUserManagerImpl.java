/*
 * Copyright (C) 1998-2009, <a href="http://www.ssel.caltech.edu">SSEL</a>,
 * at Caltech, Pasadena, CA 91125. 
 *
 * MooreRecruiting is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 * USA.
 *
 */

package edu.caltech.ssel.recruiting.service.impl;

import edu.caltech.ssel.recruiting.dao.ExtendedUserDao;
import edu.caltech.ssel.recruiting.model.Experimenter;
import edu.caltech.ssel.recruiting.model.Subject;
import edu.caltech.ssel.recruiting.service.ExtendedUserManager;
import java.util.List;
import org.appfuse.model.User;
import org.appfuse.service.impl.UserManagerImpl;
import org.appfuse.service.UserService;

// BRS 2010-08-19
import org.appfuse.service.UserExistsException;
import org.springframework.dao.DataIntegrityViolationException;
import javax.persistence.EntityExistsException;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.security.providers.encoding.PasswordEncoder;

/**
* @author Michael Kolodrubetz, SSEL, Caltech
*/
public class ExtendedUserManagerImpl extends UserManagerImpl implements ExtendedUserManager, UserService {
    
    ExtendedUserDao dao;

    
    /** Creates a new instance of ExtendedUserManagerImpl */
    public ExtendedUserManagerImpl(ExtendedUserDao dao) {
        super.setUserDao(dao);
        this.dao = dao;
    }

    // BRS 2010-08-19
    private PasswordEncoder passwordEncoder;
    /**
     * Set the PasswordEncoder used to encrypt passwords.
     * @param passwordEncoder the PasswordEncoder implementation
     */
    @Required
    public void setPasswordEncoder(PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }

    public List<Experimenter> getExperimenters() {
        return dao.getExperimenters();
    }

    public List<String> getExperimenterEmails() {
        return dao.getExperimenterEmails();
    }
    
    public Experimenter getExperimenterById(Long id) {
        return dao.getExperimenterById(id);
    }

    public List<Subject> getSubjects() {
        return dao.getSubjects();
    }

    public List<String> getSubjectEmails() {
        return dao.getSubjectEmails();
    }
    
    public Subject getSubjectById(Long id) {
        return dao.getSubjectById(id);
    }
    
    public Long getSubjectIDByParticipationID(Long id) {
        return dao.getSubjectIDByParticipationID(id);
    }
    
    public List<Subject> getSubjectBySchoolID(String sid) {
        return dao.getSubjectBySchoolID(sid);
    }
    
    public Subject getSubjectByEmail(String email) {
        return dao.getSubjectByEmail(email);
    }

    public List<User> getAdmins() {
        return dao.getAdmins();
    }

    public List<User> getStandardUsers() {
        return dao.getStandardUsers();
    }

    public List<User> getEnabledUsers() {
        return dao.getEnabledUsers();
    }

    public List<String> getEnabledUserEmails() {
        return dao.getEnabledUserEmails();
    }

    // BRS 2011-11-01
    public User getNonSubjectByEmail(String email) {
        return dao.getNonSubjectByEmail(email);
    }
    
    // BRS 2010-08-19: copied from UserManagerImpl.java, because changes to that
    // file are not being picked up.
    /**
     * {@inheritDoc}
     */
    public User saveUser(User user) throws UserExistsException {

        //log.warn("BRS - saveUser()");

        if (user.getVersion() == null) {
            // if new user, lowercase userId
            user.setUsername(user.getUsername().toLowerCase());
        }
        
        // Get and prepare password management-related artifacts
        boolean passwordChanged = false;
        log.warn("About to check if passwordEncoder is null");
        if (passwordEncoder != null) {
            log.warn("passwordEncoder is NOT null");
            // Check whether we have to encrypt (or re-encrypt) the password
            if (user.getVersion() == null) {
                // New user, always encrypt
                passwordChanged = true;
                log.warn("New user");
            } else {
                // Existing user, check password in DB
                // String currentPassword = dao.getUserPassword(user.getUsername());
                //String currentPassword = dao.get(user.getId()).getPassword(); // BRS 2010-03-30 - see http://issues.appfuse.org/browse/APF-980

                // BRS 2011-02-03: get the current password from the DB, bypassing the cache.
                String currentPassword = dao.getCurrentPasswordById(user.getId());

                log.warn("currentPassword = " + currentPassword);

                /*
                log.warn("sleeping for 10 secs");
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex) {
                    System.out.println("Error! :(");
                }
                */

                if (currentPassword == null) {
                    passwordChanged = true;
                } else {
                    log.warn("user.getPassword() = " + user.getPassword());
                    if (!currentPassword.equals(user.getPassword())) {
                        passwordChanged = true;
                    }
                }
            }

            if (passwordChanged) {
                log.warn("Password has changed");
            } else {
                log.warn("Password has NOT changed");
            }

            // If password was changed (or new user), encrypt it
            if (passwordChanged) {
                user.setPassword(passwordEncoder.encodePassword(user.getPassword(), null));
            }
        } else {
            log.warn("passwordEncoder is null");
            log.warn("PasswordEncoder not set, skipping password encryption...");
        }
        log.warn("End of password encryption section");
        
        try {
            return dao.saveUser(user);
        } catch (DataIntegrityViolationException e) {
            e.printStackTrace();
            log.warn(e.getMessage());
            throw new UserExistsException("User '" + user.getUsername() + "' already exists!");
        } catch (EntityExistsException e) { // needed for JPA
            e.printStackTrace();
            log.warn(e.getMessage());
            throw new UserExistsException("User '" + user.getUsername() + "' already exists!");
        }
    }
}
