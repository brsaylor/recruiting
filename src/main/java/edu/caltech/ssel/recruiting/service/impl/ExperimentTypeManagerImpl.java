/*
 * Copyright (C) 1998-2009, <a href="http://www.ssel.caltech.edu">SSEL</a>,
 * at Caltech, Pasadena, CA 91125. 
 *
 * MooreRecruiting is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 * USA.
 *
 */

package edu.caltech.ssel.recruiting.service.impl;

import edu.caltech.ssel.recruiting.dao.ExperimentTypeDao;
import edu.caltech.ssel.recruiting.model.ExperimentType;
import edu.caltech.ssel.recruiting.service.ExperimentTypeManager;
import org.appfuse.service.impl.GenericManagerImpl;

import java.util.List;

/**
* @author Michael Kolodrubetz, SSEL, Caltech
*/
public class ExperimentTypeManagerImpl extends GenericManagerImpl<ExperimentType, Long> implements ExperimentTypeManager {
    
    ExperimentTypeDao dao;
    
    /** Creates a new instance of ExperimentTypeManagerImpl */
    public ExperimentTypeManagerImpl(ExperimentTypeDao d) {
        super(d);
        dao = d;
    }
 
    // Might add new functions here later

    public ExperimentType getByIdString(String id) throws Exception {
        return super.get(Long.parseLong(id));
    }

    public List<ExperimentType> getAllSorted() {
        return dao.getAllSorted();
    }
    
    public ExperimentType getById(Long id) {
        return dao.getExperimentTypeById(id);
    }

}
