/*
 * Copyright (C) 1998-2009, <a href="http://www.ssel.caltech.edu">SSEL</a>,
 * at Caltech, Pasadena, CA 91125. 
 *
 * MooreRecruiting is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 * USA.
 *
 */

package edu.caltech.ssel.recruiting.service.impl;

import edu.caltech.ssel.recruiting.dao.ExperimentDao;
import edu.caltech.ssel.recruiting.model.Experiment;
import edu.caltech.ssel.recruiting.model.Filter;
import edu.caltech.ssel.recruiting.model.Survey;
import edu.caltech.ssel.recruiting.service.ExperimentManager;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.appfuse.service.impl.GenericManagerImpl;

/**
* @author Michael Kolodrubetz, SSEL, Caltech
*/
public class ExperimentManagerImpl extends GenericManagerImpl<Experiment, Long> implements ExperimentManager {
    
    ExperimentDao dao;
    private static final SimpleDateFormat dateformatter = new SimpleDateFormat("MM/dd/yyyy");
    
    /** Creates a new instance of ExperimentManagerImpl */
    public ExperimentManagerImpl(ExperimentDao dao) {
        super(dao);
        this.dao = dao;
    }

    public List<Experiment> getByDate(Date d) {
        return dao.getByDate(d);
    }

    public List<Experiment> getByDateAndLocationId(Date d, Long locId) {
        return dao.getByDateAndLocationId(d, locId);
    }

    public List<Experiment> getBySurvey(Survey s) {
        return dao.getBySurvey(s);
    }

    public List<Experiment> getBySurveyId(Long id) {
        return dao.getBySurveyId(id);
    }

    public List<Experiment> getByFilter(Filter f) {
        return dao.getByFilter(f);
    }

    public List<Experiment> getByFilterId(Long id) {
        return dao.getByFilterId(id);
    }

    public List<Experiment> getByRunby(Long runbyId) {
        return dao.getByRunby(runbyId);
    }

    public List<Experiment> getByReservedBy(Long reservedBy) {
        return dao.getByReservedBy(reservedBy);
    }

    public List<Experiment> getByPrinciple(Long principle) {
        return dao.getByPrinciple(principle);
    }

    public List<Experiment> getByExperimentType(Long expTypeId) {
        List<Experiment> experiments = dao.getByExperimentType(expTypeId);
        //log.debug("getByExperimentType -> " + experiments.toString());
        return experiments;
    }

    // BRS 2011-12-08
    // Return some basic info about the experiments of the given type
    public Map[] getExperimentListByType(Long expTypeId) {
        List<Experiment> experiments = dao.getByExperimentType(expTypeId);
        Map[] mapArray = new Map[experiments.size()];
        SimpleDateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy, h:mm a");
        for (int i = 0; i < mapArray.length; i++) {
            Experiment e = experiments.get(i);
            HashMap map = new HashMap();
            map.put("id", e.getId());
            map.put("type", e.getType().getId());
            map.put("startDateTime", dateFormat.format(e.getStartDateTime().getTime()));
            mapArray[i] = map;
        }
        return mapArray;
    }

    public List<Experiment> getByLocation(Long locationId) {
        return dao.getByLocation(locationId);
    }
    
    public List<Experiment> getById(Long id) {
        return dao.getById(id);
    }
    
    public Date getFirstExperimentDate() {
        return dao.getFirstExperimentDate();
    }

    public List<Experiment> getByRunbyAndType(Long runbyId, Long expTypeId) {
        return dao.getByRunbyAndType(runbyId, expTypeId);
    }
    
    /** for DWR convenience
     */
    public Experiment getByIdString(String expId) {
        return dao.get(new Long(expId));
    }

    // BRS 2010-09-24
    // Given start and end times for two time periods, determine whether they overlap.
    public boolean periodsOverlap(
            Date st1, // period 1 start date+time
            Date et1, // period 1 end date+time
            Date st2, // period 2 start date+time
            Date et2  // period 2 end date+time
            ) {
        /*
         *   9 possibilities for overlap of two periods p1 and p2:
         *        1       2       3       4       5       6       7       8       9
         *   p1   |---|   |---|   |---|   |---|   |---|   |---|   |---|   |---|   |---|
         *   p2   |--|    |---|   |----| |---|   |----|  |-----|   |-|     |--|    |---|
         *
         *   These can be reduced to the 3 following conditions.
         *   If and only if any of these conditions are true, then there is an overlap.
         *
         *   A.  p1 and p2 start at the same time (takes care of 1, 2, 3)
         *   B.  p1 starts during p2, exclusive of start and end times (takes care of 4, 5, 6)
         *   C.  p2 starts during p1, exclusive of start and end times (takes care of 7, 8, 9)
         */

        log.debug("st1 = " + st1 + "(" + st1.getTime() + ")");
        log.debug("et1 = " + et1 + "(" + et1.getTime() + ")");
        log.debug("st2 = " + st2 + "(" + st2.getTime() + ")");
        log.debug("et2 = " + et2 + "(" + et2.getTime() + ")");

        if (st1.equals(st2)) {
            log.debug("p1 and p2 start at the same time");
            return true; // A
        }

        if (st1.after(st2) && st1.before(et2)) {
            log.debug("p1 starts during p2, exclusive of start and end times");
            log.debug("that is, " + st1 + " is after " + st2 + " and before " + et2);
            return true; // B
        }

        if (st2.after(st1) && st2.before(et1)) {
            log.debug("p2 starts during p1, exclusive of start and end times");
            log.debug("that is, " + st2 + " is after " + st1 + " and before " + et1);
            return true; // C
        }

        log.debug("periods don't overlap");
        return false;
    }

    // BRS 2010-09-24
    public boolean experimentsOverlapWithoutBuffer(Experiment e1, Experiment e2) {
        return periodsOverlap(
                convertExpDate(e1.getExpDate(), e1.getStartTime(), 0),
                convertExpDate(e1.getExpDate(), e1.getEndTime(), 0),
                convertExpDate(e2.getExpDate(), e2.getStartTime(), 0),
                convertExpDate(e2.getExpDate(), e2.getEndTime(), 0));
    }
    
    /** returns a list of experiments conflicting with schedule
     * called via DWR
     *
     * @param dateString format MM/dd/yyyy
     * @param startTimeString format[0]0:[0]0:[AM|PM]
     * @param endTimeString format[0]0:[0]0:[AM|PM]
     * @param expId the experiment id
     * @param locId the location id
     * @param reserveLaptops (BRS)
     * @throws Exception if anything goes wrong
     */
    public List<Experiment> getConflicts(
            String dateString,
            String startTimeString,
            String setupBufferString,
            String endTimeString,
            String cleanupBufferString,
            String expId,
            String locId,
            String reserveLaptops)
    throws Exception {
        
        List<Experiment> conflictList = new ArrayList();
        Experiment exp = null;
        try {
            exp = get(Long.parseLong(expId));
        } catch(Exception ignore) {}

        //List<Experiment> scheduledList = getByDateAndLocationId(
        //        dateformatter.parse(dateString), new Long(locId));
        // BRS: just get by date, because we need to check all locations for
        // conflicts with the laptop reservations.
        List<Experiment> scheduledList = getByDate(dateformatter.parse(dateString));

        scheduledList.remove(exp);
        
        int setupBuffer = 0;
        int cleanupBuffer = 0;
        try {
            setupBuffer = Integer.parseInt(setupBufferString);
            cleanupBuffer = Integer.parseInt(cleanupBufferString);
        } catch(NumberFormatException nfe) {}
        
        Date startDate = parseExpTime(dateString, startTimeString, -setupBuffer);
        Date endDate = parseExpTime(dateString, endTimeString, cleanupBuffer);
        //log.debug("startDate: "+startDate);
        //log.debug("endDate: "+endDate);
        
        SimpleDateFormat logformatter = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        log.debug("startDate: "+logformatter.format(startDate) + ", endDate: "+logformatter.format(endDate));
        for(Experiment e: scheduledList) {
            try {
                Date expDate = e.getExpDate();
                Date expStartDate = convertExpDate(e.getExpDate(), e.getStartTime(), -e.getSetupBuffer());
                Date expEndDate = convertExpDate(e.getExpDate(), e.getEndTime(), e.getCleanupBuffer());
                log.debug("\texpStartDate: "+logformatter.format(expStartDate) + ", expEndDate: "+logformatter.format(expEndDate));

                if(
                        // they have the same location or are both reserving laptops (BRS)
                        (e.getLocation().getId().equals(new Long(locId)) ||
                        (e.getReserveLaptops() && new Boolean(reserveLaptops)))
                         &&
                        // and their times overlap,
                        periodsOverlap(startDate, endDate, expStartDate, expEndDate)) {

                    conflictList.add(e);
                    log.debug("add to conflictList: "+e.getId()+") "+e.getInstruction());
                }
            } catch (Exception bumExperiment) {
                // BRS 2012-04-04: Don't let one bad experiment crash the conflict check
                continue;
            }
        }
        log.debug("conflictList: "+conflictList.size());
        return conflictList;
    }
    
    /** append a start or end time into a complete experiment Date
     */
    private Date convertExpDate(Date expDate, Date expTime, int buffer) {
        Calendar dateCal = new GregorianCalendar();
        dateCal.setTime(expDate);
        Calendar timeCal = new GregorianCalendar();
        timeCal.setTime(expTime);
        
        dateCal.set(Calendar.HOUR_OF_DAY, timeCal.get(Calendar.HOUR_OF_DAY));
        dateCal.set(Calendar.MINUTE, timeCal.get(Calendar.MINUTE));
        
        dateCal.add(Calendar.MINUTE, buffer);
        return dateCal.getTime();
    }
    
    /** expected format is MM/dd/yyyy and [0]0:[0]0:[AM|PM]
     */
    private Date parseExpTime(String dateString, String timeString, int buffer)
    throws Exception {
        
        String[] dateElements = dateString.split("/");
        int month = Integer.parseInt(dateElements[0]);
        int day = Integer.parseInt(dateElements[1]);
        int year = Integer.parseInt(dateElements[2]);
        
        String[] timeElements = timeString.split(":");
        int hour = Integer.parseInt(timeElements[0])%12;
        int min = Integer.parseInt(timeElements[1]);
        String cycle = timeElements[2];
        
        if("PM".equals(cycle))
            hour = hour + 12;
        Calendar cal = new GregorianCalendar();
        cal.setTimeInMillis(0); // BRS: avoid leaving "random" number of milliseconds in the date!
        cal.set(year, month-1, day, hour, min,0);
        cal.add(Calendar.MINUTE, buffer);
        log.debug("going to return a Date that has getTime() = " + cal.getTime().getTime());
        return cal.getTime();
    }

    // BRS 2011-10-28
    public Object getCurrentValue(Long id, String fieldName) {
        return dao.getCurrentValue(id, fieldName);
    }
}
