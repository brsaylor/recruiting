/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.caltech.ssel.recruiting.service.impl;

import edu.caltech.ssel.recruiting.service.EmailManager;
import edu.caltech.ssel.recruiting.service.ParticipantRecordManager;
import java.io.InputStream;
import java.util.Map;
import java.util.Properties;
import javax.mail.internet.MimeMessage;
import javax.mail.MessagingException;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.exception.VelocityException;
import org.appfuse.Constants;
import org.springframework.mail.MailParseException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.ui.velocity.VelocityEngineUtils;

// BRS
import org.apache.velocity.tools.generic.DateTool;

/**
 *
 * @author wmyuan
 */
public class EmailManagerImpl implements EmailManager {

    private static final Log log = LogFactory.getLog(EmailManager.class);
    private static final String m_props = "mail.properties";
    private static Properties properties;
    private static boolean testMode;


    static {
        properties = new Properties();
        try {
            InputStream is = null;
            ClassLoader cl = ParticipantRecordManager.class.getClassLoader();
            if (cl != null) {
                is = cl.getResourceAsStream(m_props);
            }
            if (is == null) {
                is = ClassLoader.getSystemResourceAsStream(m_props);
            }
            properties.load(is);
            testMode = Boolean.parseBoolean(properties.getProperty("mail.debug"));
            log.info("mail properties loaded..");
            log.info("email system running in test mode: " + testMode);
        } catch (Exception any) {
            log.error("failed to load mail properties ", any);
        }
    }
    private JavaMailSender mailSender;
    private VelocityEngine velocityEngine;

    public int[] sendEmail(String template, Map model) {
        log.debug("entering sendEmail(String template, Map model)");
        int [] stat = new int [2];
        if (!testMode) {
            int total =0;
            int success =0;

            if (model.get(Constants.EMAIL_TO) instanceof String) {
                total =1; 
                success =total;
                try {
                    MimeMessagePreparator preparator = new EmailMessage(template, model);
                    mailSender.send(preparator);
                } catch (Exception any) {
                    success--;
                    log.warn("Failed to send email message to: " + model.get(Constants.EMAIL_TO)+ " : " + any.getMessage() );
                }
            }else if (model.get(Constants.EMAIL_TO) instanceof String[]) {
                String[] toList = (String[]) model.get(Constants.EMAIL_TO);
                total = toList.length;
                success = total;
                for (String addr : toList) {
                    model.put(Constants.EMAIL_TO, addr);
                    try {
                        MimeMessagePreparator preparator = new EmailMessage(template, model);
                        mailSender.send(preparator);
                    } catch (Exception any) {
                        success--;
                        log.warn("Failed to send email message to: " + addr + " : " + any.getMessage());
                    }
                }
            }
            stat[0] =total;
            stat[1] = success;
        }
        return stat;
    }

    /**
     * @return the mailSender
     */
    public JavaMailSender getMailSender() {
        return mailSender;
    }

    /**
     * @param mailSender the mailSender to set
     */
    public void setMailSender(JavaMailSender mailSender) {
        this.mailSender = mailSender;
    }

    /**
     * @return the velocityEngine
     */
    public VelocityEngine getVelocityEngine() {
        return velocityEngine;
    }

    /**
     * @param velocityEngine the velocityEngine to set
     */
    public void setVelocityEngine(VelocityEngine velocityEngine) {
        this.velocityEngine = velocityEngine;
    }

    // BRS 2012-02-03
    private MimeMessage simpleMailMessage2HTMLMessage(SimpleMailMessage msg) {
        MimeMessage mm = mailSender.createMimeMessage();
        MimeMessageHelper mh = null;
        try {
            mh = new MimeMessageHelper(mm, true); // 2012-02-15: true means multipart
        } catch (MessagingException e) {
            log.warn("MessagingException caught: " + e.getMessage());
            return mm;
        }
        try {
            mh.setFrom(msg.getFrom());
            //mh.setText(msg.getText(), true); // true means HTML
            
            // 2012-02-15: setText(String plainText, String htmlText)
            // Plain text version generated by stripping html tags.
            String plainText = msg.getText().replaceAll("(?s)\\<.*?>","");
            mh.setText(plainText, msg.getText());
            
            //if (msg.getReplyTo() != null && !StringUtils.isBlank(msg.getReplyTo())) mh.setReplyTo(msg.getReplyTo());
            if (msg.getSubject() != null) mh.setSubject(msg.getSubject());
            if (msg.getTo() != null) mh.setTo(msg.getTo());
            if (msg.getCc() != null) mh.setCc(msg.getCc());
            if (msg.getBcc() != null) mh.setBcc(msg.getBcc());
        } catch (MessagingException e) {
            log.warn("MessagingException caught: " + e.getMessage());
        }
        return mh.getMimeMessage();
    }

    // BRS 2012-02-03
    // Return true if the given message body string appears at a glance to be HTML, else false.
    private boolean isHTML(String content) {
        if (content.indexOf("<html>") != -1 || content.indexOf("<HTML>") != -1) {
            log.debug("looks like HTML");
            return true;
        }
        log.debug("Doesn't look like HTML");
        return false;
    }

    public int[] sendEmail(SimpleMailMessage msg) {
        int[] stat = new int[2];
        if (!testMode) {
            int total = 0;
            int success = 0;
            if (msg != null) {
                if (StringUtils.isBlank(msg.getFrom())) {
                    msg.setFrom(properties.getProperty("mail.default.from"));
                }
                if (StringUtils.isBlank(msg.getReplyTo())) {
                    msg.setReplyTo(null); // BRS 2012-02-03
                }
                String[] toList = msg.getTo();
                total = toList.length;
                success = total;
                if (toList.length == 1) {
                    try {
                        if (isHTML(msg.getText())) {
                            mailSender.send(simpleMailMessage2HTMLMessage(msg));
                        } else {
                            mailSender.send(msg);
                        }
                    } catch (Exception any) {
                        success--;
                        log.warn("Failed to send email message to: " + toList[0] + " : " + any.getMessage());
                    }
                } else {
                    for (String addr : toList) {
                        msg.setTo(addr);
                        try {
                            if (isHTML(msg.getText())) {
                                mailSender.send(simpleMailMessage2HTMLMessage(msg));
                            } else {
                                mailSender.send(msg);
                            }
                        } catch (Exception any) {
                            success--;
                            log.warn("Failed to send email message to: " + toList[0] + " : " + any.getMessage());
                        }
                    }
                }
                stat[0] =total;
                stat[1] = success;
            }
        }
        return stat;
    }

    public String getMsgFromTemplate(String template, Map model) {
        // BRS: make org.apache.velocity.tools.generic.DateTool available
        // for date/time formatting
        log.debug("putting dateTool into model");
        model.put("dateTool", new DateTool());
        try {
            return VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, template, model);
        } catch (VelocityException ex) {
            log.warn("Failed to generate email message from template: " + template);
        }
        return null;
    }

    /**
     * Inner class implementing Spring MimeMessagePreparator
     * for custom mail messages using an email template
     */
    class EmailMessage implements MimeMessagePreparator {

        private Map model;
        private String template;

        EmailMessage(String template, Map model) {
            this.model = model;
            this.template = template;

            // BRS: make org.apache.velocity.tools.generic.DateTool available
            // for date/time formatting
            log.debug("EmailMessage constructor: putting dateTool into model");
            model.put("dateTool", new DateTool());
        }

        public void prepare(MimeMessage mimeMessage) throws Exception {
            log.debug("entering EmailMessage.prepare");
            if (!model.containsKey(Constants.EMAIL_TO)) {
                throw new MailParseException("Failed to construct email: missing TO address..");
            }

            MimeMessageHelper message = new MimeMessageHelper(mimeMessage);

            message.setTo((String) model.get(Constants.EMAIL_TO));
            message.setSubject((String) model.get(Constants.EMAIL_SUBJECT));
            String from = (String) model.get(Constants.EMAIL_FROM);
            if (StringUtils.isBlank(from)) {
                from = properties.getProperty("mail.default.from");
            }

            message.setFrom(from);

            if (model.containsKey(Constants.EMAIL_CC)) {
                if (model.get(Constants.EMAIL_CC) instanceof String) {
                    message.setCc((String) model.get(Constants.EMAIL_CC));
                }
                if (model.get(Constants.EMAIL_CC) instanceof String[]) {
                    message.setCc((String[]) model.get(Constants.EMAIL_CC));
                }
            }

            if (model.containsKey(Constants.EMAIL_BCC)) {
                if (model.get(Constants.EMAIL_BCC) instanceof String) {
                    message.setBcc((String) model.get(Constants.EMAIL_BCC));
                }
                if (model.get(Constants.EMAIL_BCC) instanceof String[]) {
                    message.setBcc((String[]) model.get(Constants.EMAIL_BCC));
                }
            }
            
            // BRS 2012-02-03 removed: doesn't seem like a good idea...
            /* else {
                message.setBcc(from);
            }
            */

            // BRS 2012-02-03 removed
            //String reply_to = (String) model.get(Constants.EMAIL_REPLY_TO);
            //if (StringUtils.isBlank(reply_to)) {
            //    message.setReplyTo(from);
            //} else {
            //    message.setReplyTo(reply_to);
            //}

            String text = VelocityEngineUtils.mergeTemplateIntoString(
                    velocityEngine, template, model);
            message.setText(text, isHTML(text)); // "true" argument indicates HTML -BRS
            
            // BRS Aug 2010
            //log.debug("About to print out message content.  model.get(\"dateTool\") = " + model.get("dateTool").toString());
            //log.debug(mimeMessage.getContent());
        }
    }
}
