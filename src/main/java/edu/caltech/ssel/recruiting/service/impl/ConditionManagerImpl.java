/*
 * Copyright (C) 1998-2009, <a href="http://www.ssel.caltech.edu">SSEL</a>,
 * at Caltech, Pasadena, CA 91125. 
 *
 * MooreRecruiting is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 * USA.
 *
 */

package edu.caltech.ssel.recruiting.service.impl;

import edu.caltech.ssel.recruiting.dao.ConditionDao;
import edu.caltech.ssel.recruiting.dao.ExtendedUserDao;
import edu.caltech.ssel.recruiting.dao.ParticipantRecordDao;
import edu.caltech.ssel.recruiting.dao.QuestionResultDao;
import edu.caltech.ssel.recruiting.dao.SurveyDao;
import edu.caltech.ssel.recruiting.dao.ExperimentTypeDao;
import edu.caltech.ssel.recruiting.model.BooleanCondition;
import edu.caltech.ssel.recruiting.model.Condition;
import edu.caltech.ssel.recruiting.model.ExperimentType;
import edu.caltech.ssel.recruiting.model.ExperimentTypeCondition;
import edu.caltech.ssel.recruiting.model.ExperimentCondition;
import edu.caltech.ssel.recruiting.model.Filter;
import edu.caltech.ssel.recruiting.model.ParticipantRecord;
import edu.caltech.ssel.recruiting.model.PoolCondition;
import edu.caltech.ssel.recruiting.model.Question;
import edu.caltech.ssel.recruiting.model.QuestionResult;
import edu.caltech.ssel.recruiting.model.QuestionResultCondition;
import edu.caltech.ssel.recruiting.model.NoShowCondition;
import edu.caltech.ssel.recruiting.model.QuestionResultCondition.FilterType;
import edu.caltech.ssel.recruiting.model.Subject;
import edu.caltech.ssel.recruiting.model.SubjectPool;
import edu.caltech.ssel.recruiting.service.ConditionManager;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.appfuse.dao.GenericDao;
import org.appfuse.service.impl.GenericManagerImpl;

/**
* @author Michael Kolodrubetz, SSEL, Caltech
*/
public class ConditionManagerImpl extends GenericManagerImpl<Condition, Long> implements ConditionManager {
    
    private final Log log = LogFactory.getLog(ConditionManagerImpl.class);
    private ConditionDao dao;
    private QuestionResultDao qrdao;
    private SurveyDao surveyDao;
    private ParticipantRecordDao participantRecordDao;
    private ExtendedUserDao extendedUserDao;
    private GenericDao<Question, Long> questionDao;
    private GenericDao<Filter, Long> filterDao;
    private GenericDao<SubjectPool, Long> subjectPoolDao;
    private ExperimentTypeDao experimentTypeDao;
    
    /** Creates a new instance of ConditionManagerImpl */
    public ConditionManagerImpl(ConditionDao cd) {
        super(cd);
        this.dao = cd;
    }
    
    public boolean isEligible(Condition c, Subject u) throws Exception {
        //log.debug("Entering isEligible(Condition, User), condition_id = " + c.getId());
        if(c instanceof QuestionResultCondition) {
            QuestionResultCondition qrc = (QuestionResultCondition)c;
            QuestionResult qr = qrdao.getMostRecentAnswer(u, qrc.getQuestion());
            return ((qr==null) ? true : qrc.isEligible(qr));
        } else if(c instanceof ExperimentTypeCondition) {
            ExperimentTypeCondition etc = (ExperimentTypeCondition)c;
            List<ParticipantRecord> lst = participantRecordDao.getBySubject(u);
            return etc.isEligible(lst);
        } else if(c instanceof ExperimentCondition) { // BRS 2011-12-09
            ExperimentCondition ec = (ExperimentCondition)c;
            List<ParticipantRecord> lst = participantRecordDao.getBySubject(u);
            return ec.isEligible(lst);
        } else if(c instanceof PoolCondition) {
            return ((PoolCondition)c).isEligible(u);
        } else if(c instanceof BooleanCondition) {
            BooleanCondition bc = (BooleanCondition)c;
            Long first_id = bc.getFirstCondition(), second_id = bc.getSecondCondition();
            if(!this.exists(first_id) || !this.exists(second_id)) {
                log.debug("The conditions references by this boolean condition no longer exist - please delete it");
                return true;//The default
            }
            return bc.isEligible(this.isEligible(get(first_id), u), this.isEligible(get(second_id), u));
        
        // BRS
        } else if(c instanceof NoShowCondition) {
            NoShowCondition nsc = (NoShowCondition)c;
            List<ParticipantRecord> lst = participantRecordDao.getBySubject(u);
            return nsc.isEligible(lst);
        } else {

            log.debug("Not able to check eligibility for condition type " + c.getConditionType());
            return true;
        }
    }
    
    public void setQuestionResultDao(QuestionResultDao qrdao) {
        this.qrdao = qrdao;
    }
    
    public boolean isEligible(Filter f, Subject u) throws Exception {
        //If there is no filter, then everyone's eligible
        if(f == null)
            return true;
        
        List<Condition> lst = f.getConditions();
        for(int j = 0; j < lst.size(); j++)
            if(!isEligible(lst.get(j), u))
                return false;
        return true;
    }
    
    public boolean isPreEligible(Filter f, Subject u) throws Exception {
        //If there is no filter, then everyone's eligible
        if(f == null)
            return true;
        
        List<Condition> lst = f.getConditions();
        for(int j = 0; j < lst.size(); j++)
            if(!isPreEligible(lst.get(j), u))
                return false;
        return true;
    }
    
    public void checkEligible(Condition c, List<Subject> lst) throws Exception {
        Set<Subject> set = new HashSet();
        for(Subject s : lst)
            if(isEligible(c, s))
                set.add(s);
        lst.retainAll(set);
    }
    
    //Checks whether user is eligible, ignoring non-profile QuestionResultCondition's
    public boolean isPreEligible(Condition c, Subject u) throws Exception {
        //Immediately return true if we have a non-profile QRCondition, since we are ignoring these
        if(c instanceof QuestionResultCondition) {
            QuestionResultCondition qrc = (QuestionResultCondition) c;
            List<Question> lst = surveyDao.getProfileSurvey().getQuestions();
            if(lst == null || !lst.contains(qrc.getQuestion()))
                return true;
        }
        //Otherwise perform standard eligibility test
        return isEligible(c, u);
    }
    
    public void setSurveyDao(SurveyDao surveyDao) {
        this.surveyDao = surveyDao;
    }
    
    public void setParticipantRecordDao(ParticipantRecordDao participantRecordDao) {
        this.participantRecordDao = participantRecordDao;
    }
    
    public Condition getByIdString(String id) throws Exception {
        return dao.get(Long.parseLong(id));
    }
    
    public QuestionResultCondition createQuestionResultCondition(String name, String q_id, String[] opts, String type) throws Exception {
        QuestionResultCondition qrc = new QuestionResultCondition();
        qrc.setName(name);
        qrc.setQuestion(questionDao.get(Long.parseLong(q_id)));
        qrc.setFilterType(FilterType.valueOf(type));
        Set<Integer> optSet = new HashSet();
        for(int j = 0; j < opts.length; j++)
            optSet.add(new Integer(opts[j]));
        qrc.setFilterOptions(optSet);
        qrc = (QuestionResultCondition) dao.save(qrc);
        return qrc;
    }
    
    public PoolCondition createPoolCondition(String name, String[] poolIds, String onList) throws Exception {
        PoolCondition pc = new PoolCondition();
        pc.setMemberOf(onList.equals("true"));
        pc.setName(name);
        List<SubjectPool> lst = new ArrayList(poolIds.length);
        for(int j = 0; j < poolIds.length; j++)
            lst.add(subjectPoolDao.get(Long.parseLong(poolIds[j])));
        pc.setPools(lst);
        return (PoolCondition) dao.save(pc);
    }
    
    public ExperimentTypeCondition createExperimentTypeCondition(String name, String[] typeIds, String onList, String history) throws Exception {
        // TODO: fix multiple conditions with same name
        List<ExperimentTypeCondition> nameLst = dao.getExperimentTypeConditionByName(name);
        if(nameLst.size() > 0)
            return nameLst.get(0);
        ExperimentTypeCondition etc = new ExperimentTypeCondition();
        log.debug("history = " + history);
        log.debug("onList = " + onList);
        etc.setOnList(onList.equals("true"));
        etc.setHistory(history.equals("true"));
        etc.setName(name);
        List<ExperimentType> lst = new ArrayList(typeIds.length);
        for(int j = 0; j < typeIds.length; j++)
            lst.add(experimentTypeDao.get(Long.parseLong(typeIds[j])));
        log.debug("name set to " + name);
        etc.setTypes(lst);
        /*ExperimentTypeCondition ret = new ExperimentTypeCondition();
        try {
            ret = (ExperimentTypeCondition) dao.save(etc);
        } catch (DataIntegrityViolationException e) {
            errors.rejectValue("name", "filterForm.duplicate",
                                new Object[]{name}, "duplicate name");
        }
        return ret;*/
        return (ExperimentTypeCondition) dao.save(etc);
    }
    
    public BooleanCondition createBooleanCondition(String name, String first_cond_id, String second_cond_id, String op) throws Exception {
        BooleanCondition bc = new BooleanCondition();
        bc.setName(name);
        bc.setFirstCondition(Long.parseLong(first_cond_id));
        bc.setSecondCondition(Long.parseLong(second_cond_id));
        bc.setAnd(op.equals("AND"));
        return (BooleanCondition) dao.save(bc);
    }

    // BRS
    public NoShowCondition createNoShowCondition(String name, String maxNoShows) throws Exception {
        NoShowCondition nsc = new NoShowCondition();
        nsc.setName(name);
        nsc.setMaxNoShows(Integer.parseInt(maxNoShows));
        return (NoShowCondition) dao.save(nsc);
    }

    public void setQuestionDao(GenericDao<Question, Long> questionDao) {
        this.questionDao = questionDao;
    }
    
    public void setSubjectPoolDao(GenericDao<SubjectPool, Long> subjectPoolDao) {
        this.subjectPoolDao = subjectPoolDao;
    }
    
    public void setExperimentTypeDao(ExperimentTypeDao experimentTypeDao) {
        this.experimentTypeDao = experimentTypeDao;
    }

    // BRS 2012-02-09 added optional "includeUnsubscribed" parameter,
    // which defaults to true for the original behavior
    // (include all eligible users regardless of subscription status).
    public List<String> getEligibleUserEmailsByFilter(Filter f) throws Exception {
        return getEligibleUserEmailsByFilter(f, true);
    }
    public List<String> getEligibleUserEmailsByFilter(Filter f, boolean includeUnsubscribed) throws Exception {
        log.debug("* entering getEligibleUserEmailsByFilter");
        List<String> eligibleEmails = new ArrayList();
        //List<Subject> subjectList = extendedUserDao.getSubjects();
        
        List<Condition> conditions = new ArrayList();
        conditions = f.getConditions();
        int etCount = 0, qrCount = 0;
        for(int i = 0; i < conditions.size(); i++) {
            if(conditions.get(i) instanceof ExperimentTypeCondition)
                etCount++;
            if(conditions.get(i) instanceof QuestionResultCondition)
                qrCount++;
        }
        Set<Long> subjectIds = new HashSet();
        log.debug("* Number of conditions: "+conditions.size());


        /*
         * BRS 2011-08-29: Using new database-level method instead of all this.
         * See right below all this commented-out code.
         *
         *
        // BRS 2011-08-08: Don't do special-case eligibility check below for when all conditions are ETCs.
        // This is because the code below calls methods of ConditionDaoHibernate which seem not to work correctly.
        int on = 0;

        if(etCount+qrCount == conditions.size()  &&  on==1) {
            log.debug("* All Filter types are ETCondition Types");
            for(int i = 0; i < conditions.size(); i++) {
                Set<Long> temp;
                if(conditions.get(i) instanceof QuestionResultCondition)
                    continue;
                //Check on past experiments
                if( ((ExperimentTypeCondition)conditions.get(i)).isHistory() == true) {
                    //Check for on/off list
                    log.debug("***History == true (Filter on the past)");
                    if( ((ExperimentTypeCondition)conditions.get(i)).isOnList() == true )
                        temp = dao.getEligibleSubjectByETC(conditions.get(i).getId());
                    else
                        temp = dao.getEligibleSubjectByNotETC(conditions.get(i).getId());
                }
                else { //Check on future experiments
                    log.debug(new java.sql.Date(new Date().getTime()));
                    log.debug(conditions.get(i).getId());
                    log.debug("***History == false (Filter on the future)");
                    //Check for on/off list
                    if( ((ExperimentTypeCondition)conditions.get(i)).isOnList() == true )
                        temp = dao.getEligibleSubjectByFutureETC(conditions.get(i).getId(),new java.sql.Date(new Date().getTime()));
                    else
                        temp = dao.getEligibleSubjectByNotFutureETC(conditions.get(i).getId(), new java.sql.Date(new Date().getTime()));
                }
                log.debug("*Temp size: "+temp.size());
                if(i == 0)
                    subjectIds.addAll(temp);
                else
                    subjectIds.retainAll(temp);
                //subjectIds.addAll(temp);
                //log.debug(temp.size());
            }
            log.debug("* subject id size "+subjectIds.size());
            //log.debug(extendedUserDao.getSubjectEmailsByIdSet(subjectIds).size());
            if(subjectIds.size() != 0)
                return extendedUserDao.getSubjectEmailsByIdSet(subjectIds);
            return null;
        }
        else {
            List<Subject> subjectList = extendedUserDao.getSubjects();
            Subject s;
            for(int j = 0; j < subjectList.size(); j++) {
                s = subjectList.get(j);
                if(isPreEligible(f, s) && s.getSubscribed()) // BRS 2011-02-15 exclude unsubscribed subjects
                    eligibleEmails.add(s.getEmail());
            }
        }

        *
        */

        // BRS 2011-08-29: using new DAO-level eligibility checking

        // Build list of eligible subject IDs (excludes disabled users, includes unsubscribed users)
        for (int i = 0; i < conditions.size(); i++) {
            Condition cond = conditions.get(i);
            Set set = dao.getEligibleSubjectIDs(cond);
            if (i == 0) {
                subjectIds.addAll(set);
            } else {
                subjectIds.retainAll(set);
            }
        }

        // BRS 2012-02-09
        if (includeUnsubscribed == false) {
            subjectIds.removeAll(dao.getUnsubscribedSubjectIDs());
        }

        //log.debug("* subjectIds = " + subjectIds.toString());


        if (subjectIds.size() > 0) {
            // This method does not check subscribed status.
            eligibleEmails = extendedUserDao.getSubjectEmailsByIdSet(subjectIds);
        } else {
            log.debug("* No eligible subjects!");
        }

        log.debug("* returning normally from getEligibleUserEmailsByFilter");
        return eligibleEmails;
    }
    
    public List<Subject> getEligibleSubjectsByFilter(Filter f) throws Exception {
        List<Subject> eligList = new ArrayList();
        List<Subject> subjectList = extendedUserDao.getSubjects();
        
        for(int j = 0; j < subjectList.size(); j++)
            if(isPreEligible(f, subjectList.get(j)))
                eligList.add(subjectList.get(j));
        
        return eligList;
    }
    
    public List<Subject> getEligibleSubjectsByFilter2(Filter f) throws Exception {
        List<Condition> conditions = new ArrayList();
        List<Subject> subjects = new ArrayList();
        Set<Subject> eligSubj = new HashSet(subjects);
        int etCount = 0,qrCount = 0;
        //subjects = extendedUserDao.getSubjects();
        for(int i = 0; i < conditions.size(); i++) {
            if(conditions.get(i) instanceof ExperimentTypeCondition)
                etCount++;
            if(conditions.get(i) instanceof QuestionResultCondition)
                qrCount++;
        }
        Set<Long> subjectIds = new HashSet();
        if(etCount == conditions.size() - qrCount) {
            for(int i = 0; i < conditions.size(); i++) {
                Set<Long> temp = dao.getEligibleSubjectByETC(conditions.get(i).getId());
                subjectIds.addAll(temp);
            }
            List subjIds = new ArrayList(subjectIds);
            return extendedUserDao.getSubjectsByNotInIdSet(subjIds);
        }
        else
            return getEligibleSubjectsByFilter(f);
    }
    
    public void setExtendedUserDao(ExtendedUserDao extendedUserDao) {
        this.extendedUserDao = extendedUserDao;
    }

    public List<String> getEligibleUserEmailsByFilterConditions(String [] condIdList) throws Exception {
        log.debug("** entering getEligibleUserEmailsByFilterConditions");
        //Setup a temporary filter and filter on it
        Filter f = new Filter();
        List<Condition> condList = new ArrayList(condIdList.length);
        for(int j = 0; j < condIdList.length; j++) {
            condList.add(getByIdString(condIdList[j]));
            log.debug("condList[" + j + "] = " + condList.get(j));
        }
        f.setConditions(condList);
        log.debug("** returning from getEligibleUserEmailsByFilterConditions");
        return getEligibleUserEmailsByFilter(f);
    }

    public String getEligibleUserStatisticsByFilterConditions(String [] condIdList) throws Exception {
        log.debug("*** entering getEligibleUserStatisticsByFilterConditions");
        if(condIdList == null || condIdList.length == 0) {
            log.debug("condIdList empty in getEligibleUserStatisticsByFilterConditions");
            return extendedUserDao.getSubjects().size() + " of " + extendedUserDao.getSubjects().size();
        }
        log.debug("condIdList:");
        for (int i = 0; i < condIdList.length; i++) {
            log.debug(condIdList[i]);
        }
        try {
            int eligSize = getEligibleUserEmailsByFilterConditions(condIdList).size();
            //log.debug(eligSize + " are eligible");
            log.debug("*** returning normally from getEligibleUserStatisticsByFilterConditions");
            return eligSize + " of " + extendedUserDao.getSubjects().size();
        }
        catch(Exception e) {
            log.debug("Exception caught in getEligibleUserStatisticsByFilterConditions:");
            System.out.println(e);
            for(int i = 0; i < condIdList.length; i++)
                log.debug("Condition id: " + condIdList[i]);
            return "Error - see logs";
        }
    }

    public void setFilterDao(GenericDao<Filter, Long> filterDao) {
        this.filterDao = filterDao;
    }

    public Filter setupBasicFilterById(String filterId, String experimentTypeId) throws Exception {
        Filter f = filterDao.get(Long.parseLong(filterId));
        if(f.isBasic()) {
            f = f.clone();
            log.debug("Setting up basic filter");
            List<Condition> cList = f.getConditions();
            List<Condition> newCList = new LinkedList<Condition>();
            for(Condition c: cList) {
                if(c.getConditionType().equals("ExperimentTypeCondition")) {
                    ExperimentType et = experimentTypeDao.getExperimentTypeById(Long.parseLong(experimentTypeId));
                    String [] typeIds = {experimentTypeId};

                    ExperimentTypeCondition ETC = (ExperimentTypeCondition)c;
                    createExperimentTypeCondition(c.getName() + " " + et.getName(), typeIds, Boolean.valueOf(ETC.isOnList()).toString(),Boolean.valueOf(ETC.isHistory()).toString());
                }
                else
                    newCList.add(c);
            }
            
            f.setConditions(cList);
        }
        else
            log.debug("setupBasicFilterById was called on a non-basic filter");

        f.setBasic(false);
        return f;
    }
    
}
