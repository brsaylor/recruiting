/*
 * Copyright (C) 1998-2009, <a href="http://www.ssel.caltech.edu">SSEL</a>,
 * at Caltech, Pasadena, CA 91125. 
 *
 * MooreRecruiting is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 * USA.
 *
 */

package edu.caltech.ssel.recruiting.service.impl;

import edu.caltech.ssel.recruiting.model.Experiment;
import edu.caltech.ssel.recruiting.service.AnnouncementManager;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.velocity.app.VelocityEngine;

import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.ui.velocity.VelocityEngineUtils;

import javax.mail.internet.MimeMessage;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Handles announcements for the recruiting system
 */


// BRS 2012-02-02: THIS CLASS IS NOT USED BY AnnouncementFormController

/**
* @author Michael Kolodrubetz, SSEL, Caltech
*/
public class AnnouncementManagerImpl implements AnnouncementManager {
    
    private JavaMailSender mailSender;
    private VelocityEngine velocityEngine;
    
    private static final Log staticlog = LogFactory.getLog(AnnouncementManager.class);
    
    /** Creates a new instance of AnnouncementManagerImpl */
    public AnnouncementManagerImpl() {
    }
    
    public void makeAnnouncement(
            String from,
            List toList,
            String emailSubject,
            Experiment experiment) {
        
        MimeMessagePreparator preparator = new AnnouncementMessage(
                from,
                toList,
                emailSubject,
                experiment);
        mailSender.send(preparator);
    }

    public void setMailSender(JavaMailSender mailSender) {
        this.mailSender = mailSender;
    }

    public void setVelocityEngine(VelocityEngine velocityEngine) {
        this.velocityEngine = velocityEngine;
    }
    
    /**
     * Inner class implementing Spring MimeMessagePreparator
     * for custom mail messages using an email template
     */
    class AnnouncementMessage implements MimeMessagePreparator {
        
        String from;
        List toList;
        String emailSubject;
        Experiment experiment;
        
        AnnouncementMessage(
                String from,
                List toList,
                String emailSubject,
                Experiment experiment) {
            
            this.from = from;
            this.toList = toList;
            this.emailSubject = emailSubject;
            this.experiment = experiment;
        }
        
        public void prepare(MimeMessage mimeMessage) throws Exception {
            MimeMessageHelper message = new MimeMessageHelper(mimeMessage);
            message.setTo((String[])toList.toArray());
            message.setFrom(from); // could be parameterized...
            message.setSubject(emailSubject);
            Map model = new HashMap();
            model.put("experiment", experiment);
            String text = VelocityEngineUtils.mergeTemplateIntoString(
            velocityEngine, "experiment-announcement.vm", model);
            message.setText(text, true);
        }
    }
}
