/*
 * Copyright (C) 1998-2009, <a href="http://www.ssel.caltech.edu">SSEL</a>,
 * at Caltech, Pasadena, CA 91125. 
 *
 * MooreRecruiting is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 * USA.
 *
 */

package edu.caltech.ssel.recruiting.service.impl;

import edu.caltech.ssel.recruiting.dao.SurveyDao;
import edu.caltech.ssel.recruiting.model.Question;
import edu.caltech.ssel.recruiting.model.Survey;
import edu.caltech.ssel.recruiting.service.SurveyManager;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.appfuse.dao.GenericDao;
import org.appfuse.service.impl.GenericManagerImpl;

/**
* @author Michael Kolodrubetz, SSEL, Caltech
*/
public class SurveyManagerImpl extends GenericManagerImpl<Survey, Long> implements SurveyManager {
    
    SurveyDao dao;
    private GenericDao<Question, Long> questionDao;
    private final Log log = LogFactory.getLog(SurveyManagerImpl.class);
    
    /** Creates a new instance of SurveyManagerImpl */
    public SurveyManagerImpl(SurveyDao dao) {
        super(dao);
        this.dao = dao;
    }

    public Survey getProfileSurvey() throws Exception {
        return dao.getProfileSurvey();
    }

    public Survey getByIdString(String id) throws Exception {
        return super.get(Long.parseLong(id));
    }

    public Survey addQuestionToSurvey(String survey_id, String question_id, String ind) throws Exception{
        log.debug("Entering addQuestionToSurvey...");
        Survey s = getByIdString(survey_id);
        log.debug("Initially there are " + s.getQuestions().size() + " questions");
        Question q = questionDao.get(Long.parseLong(question_id));
        int i = Integer.parseInt(ind);
        if(i >= s.getQuestions().size()) {
            s.getQuestions().add(q);
        } else {
            s.getQuestions().add(i, q);
        }
        s = save(s);
        log.debug("Afterwards there are " + s.getQuestions().size() + " questions");
        return s;
    }

    public Survey removeQuestionFromSurvey(String survey_id, String ind) throws Exception{
        log.debug("Entering removeQuestionFromSurvey...");
        Survey s = getByIdString(survey_id);
        log.debug("Initially there are " + s.getQuestions().size() + " questions");
        s.getQuestions().remove(Integer.parseInt(ind));
        s = save(s);
        log.debug("Afterwards there are " + s.getQuestions().size() + " questions");
        return s;
    }

    public void setQuestionDao(GenericDao<Question, Long> questionDao) {
        this.questionDao = questionDao;
    }

}
