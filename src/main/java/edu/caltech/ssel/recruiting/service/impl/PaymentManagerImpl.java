/*
 * Copyright (C) 1998-2009, <a href="http://www.ssel.caltech.edu">SSEL</a>,
 * at Caltech, Pasadena, CA 91125. 
 *
 * MooreRecruiting is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 * USA.
 *
 */

package edu.caltech.ssel.recruiting.service.impl;

import edu.caltech.hss.paygate.SOAP.PaymentGatewaySOAPClient;
import edu.caltech.hss.payment.crypto.Blowfish;
import edu.caltech.ssel.recruiting.dao.PaymentDao;
import edu.caltech.ssel.recruiting.model.ParticipantRecord;
import edu.caltech.ssel.recruiting.model.Payment;
import edu.caltech.ssel.recruiting.service.EmailManager;
import edu.caltech.ssel.recruiting.service.PaymentManager;

import org.appfuse.service.impl.GenericManagerImpl;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


import java.util.HashMap;
import java.util.Map;

import java.io.InputStream;
import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import org.appfuse.Constants;
import org.appfuse.model.User;
import org.springframework.context.support.ResourceBundleMessageSource;

/**
* @author Michael Kolodrubetz, SSEL, Caltech
*/
public class PaymentManagerImpl extends GenericManagerImpl<Payment, Long> implements PaymentManager {
    private static final String s_props = "PMTGWY.properties";
    private static Properties properties;
    private static final Log staticlog = LogFactory.getLog(PaymentManager.class);
    protected static NumberFormat nf = NumberFormat.getCurrencyInstance();

    static {
        properties = new Properties();
        try {
            InputStream is = null;
            ClassLoader cl = PaymentManager.class.getClassLoader();
            if(cl != null) is = cl.getResourceAsStream(s_props);
            if(is == null) is = ClassLoader.getSystemResourceAsStream(s_props);
            properties.load(is);
            staticlog.debug(s_props + " loaded");
        }
        catch(Exception any) {
            staticlog.error("failed to init " + s_props, any);
        }
    }

    PaymentDao dao;

    private EmailManager emailManager;
    private ResourceBundleMessageSource messageSource;
    private String templateName;
    
    
    
        
    /** Creates a new instance of PaymentManagerImpl */
    public PaymentManagerImpl(PaymentDao dao) {
        super(dao);
        this.dao = dao;
    }

    public List<Payment> getByTrackingId(String trackingId) {
        return dao.getByTrackingId(trackingId);
    }
    
    public String sendPayment(ParticipantRecord rec, String message)
    throws Exception {
        
        Payment payment = rec.getPayment();
        PaymentGatewaySOAPClient client = new PaymentGatewaySOAPClient();
        String trackingId = client.callSetupPaymentInfo(
                properties.getProperty("paygate.webservicesURL"),
                payment.getId().toString(),
                properties.getProperty("paygate.authSource"),
                rec.getSubject().getUsername(),
                payment.getAccountInfo(),
                rec.getPayoff().toString(),
                properties.getProperty("paygate.paymentType"),
                properties.getProperty("paygate.authUsername"),
                properties.getProperty("paygate.authPassword"));
        
        payment.setTrackingId(trackingId);
        save(payment);

        // BRS - don't email unsubscribed users
        if (!rec.getSubject().getSubscribed()) {
            log.info("Not emailing unsubscribed subject " + rec.getSubject().getEmail());
            return trackingId;
        }

        Map model = new HashMap();
        model.put(Constants.EMAIL_TO, rec.getSubject().getEmail());
        Object[] params = new Object[1];
        params[0] = rec.getExp().getId();
        model.put(Constants.EMAIL_SUBJECT, message);

        model.put(Constants.USER, (User)rec.getSubject());
        model.put(Constants.EXPERIMENT, rec.getExp());
        model.put("amount", nf.format(rec.getPayoff()));
        model.put("paymentURL", getPaymentURL(trackingId));

        model.put("labName", this.messageSource.getMessage("company.name", null, Locale.getDefault()));
        model.put("labUrlName", this.messageSource.getMessage("company.url", null, Locale.getDefault()));
        model.put("labPhone", this.messageSource.getMessage("company.phone", null, Locale.getDefault()));

        emailManager.sendEmail(templateName, model);

        return trackingId;
    }
    
    public boolean isPayGateConfigured() {
        return new Boolean(properties.getProperty("paygate.active")).booleanValue();
    }
    
    private String getPaymentURL(String trackingId)
    throws Exception {
         Blowfish blowfish = new Blowfish();
        String encryptedText = blowfish.encryptStrWithPass(
                properties.getProperty("paygate.cryptoKey"),
                trackingId);
        return properties.getProperty("paygate.paymentURL")+"?paymentId="+encryptedText;
    }

    /**
     * @param emailManager the emailManager to set
     */
    public void setEmailManager(EmailManager emailManager) {
        this.emailManager = emailManager;
    }

    /**
     * @param messageSource the messageSource to set
     */
    public void setMessageSource(ResourceBundleMessageSource messageSource) {
        this.messageSource = messageSource;
    }

    /**
     * @param templateName the templateName to set
     */
    public void setTemplateName(String templateName) {
        this.templateName = templateName;
    }

}
