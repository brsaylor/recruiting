/*
 * Copyright (C) 1998-2009, <a href="http://www.ssel.caltech.edu">SSEL</a>,
 * at Caltech, Pasadena, CA 91125. 
 *
 * MooreRecruiting is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 * USA.
 *
 */

package edu.caltech.ssel.recruiting.service;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.Timer;
import java.util.TimerTask;

/**
* @author Michael Kolodrubetz, SSEL, Caltech
* @author <a href="mailto:wmyuan@hss.caltech.edu">Walter M. Yuan</a>, SSEL, Caltech
*/
public class ReminderTask  extends TimerTask {
    
    private static final Log log = LogFactory.getLog(ReminderTask.class);
    private ReminderManager reminderManager;
    private static int delay = 10;

    private static int interval = 5;
    
    /** Creates a new instance of ReminderTask */
    public ReminderTask() {

        Timer timer = new Timer("ReminderTimer");
        timer.scheduleAtFixedRate(this, delay * 1000, interval * 60 * 1000);
    }

    /**
     * the task to be run is to find all reminders that need to be processed
     * and process them.
     */
    public void run() {
        try {
            
            reminderManager.processExperimentReminders();
            
        } catch(Exception any) {
            log.error("an error occurred processing reminders", any);
        }
    }

    public void setReminderManager(ReminderManager reminderManager) {
        this.reminderManager = reminderManager;
    }
}
