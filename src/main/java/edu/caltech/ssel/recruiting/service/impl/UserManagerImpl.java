package org.appfuse.service.impl;

import org.springframework.security.providers.dao.DaoAuthenticationProvider;
import org.springframework.security.providers.dao.SaltSource;
import org.springframework.security.providers.encoding.PasswordEncoder;
import org.springframework.security.userdetails.UsernameNotFoundException;
import org.appfuse.dao.UserDao;
import org.appfuse.model.User;
import org.appfuse.service.UserExistsException;
import org.appfuse.service.UserManager;
import org.appfuse.service.UserService;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.dao.DataIntegrityViolationException;

import org.apache.commons.logging.LogFactory;
import org.apache.commons.logging.Log;

import javax.jws.WebService;
import javax.persistence.EntityExistsException;
import java.util.List;


/**
 * Implementation of UserManager interface.
 *
 * @author <a href="mailto:matt@raibledesigns.com">Matt Raible</a>
 */
@WebService(serviceName = "UserService", endpointInterface = "org.appfuse.service.UserService")
public class UserManagerImpl extends UniversalManagerImpl implements UserManager, UserService {
    private UserDao dao;
    private PasswordEncoder passwordEncoder;

    /**
     * Set the Dao for communication with the data layer.
     * @param dao the UserDao that communicates with the database
     */
    @Required
    public void setUserDao(UserDao dao) {
        this.dao = dao;
    }

    /**
     * Set the PasswordEncoder used to encrypt passwords.
     * @param passwordEncoder the PasswordEncoder implementation
     */
    @Required
    public void setPasswordEncoder(PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }

    /**
     * {@inheritDoc}
     */
    public User getUser(String userId) {
        return dao.get(new Long(userId));
    }

    /**
     * {@inheritDoc}
     */
    public List<User> getUsers(User user) {
        return dao.getUsers();
    }
    
    // BRS 2012-01-20: ** IMPORTANT: PASSWORD CHANGING IS BROKEN **
    /**
     * {@inheritDoc}
     */
    public User saveUser(User user) throws UserExistsException {

        //log.warn("BRS - saveUser()");

        if (user.getVersion() == null) {
            // if new user, lowercase userId
            user.setUsername(user.getUsername().toLowerCase());
        }
        
        // Get and prepare password management-related artifacts
        boolean passwordChanged = false;
        log.warn("About to check if passwordEncoder is null");
        if (passwordEncoder != null) {
            log.warn("passwordEncoder is NOT null");
            // Check whether we have to encrypt (or re-encrypt) the password
            if (user.getVersion() == null) {
                // New user, always encrypt
                passwordChanged = true;
                log.warn("New user");
            } else {
                // Existing user, check password in DB
                // String currentPassword = dao.getUserPassword(user.getUsername());
                String currentPassword = dao.get(user.getId()).getPassword(); // BRS 2010-03-30 - see http://issues.appfuse.org/browse/APF-980

                // BRS 2011-02-03: get the current password from the DB, bypassing the cache.
                //String currentPassword = dao.getCurrentPasswordById(user.getId());
                //// BRS 2012-01-20: NOT IMPLEMENTED IN UserDao, so password changing is broken.

                log.warn("currentPassword = " + currentPassword);

                /*
                log.warn("sleeping for 10 secs");
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex) {
                    System.out.println("Error! :(");
                }
                */

                if (currentPassword == null) {
                    passwordChanged = true;
                } else {
                    log.warn("user.getPassword() = " + user.getPassword());
                    if (!currentPassword.equals(user.getPassword())) {
                        passwordChanged = true;
                    }
                }
            }

            if (passwordChanged) {
                log.warn("Password has changed");
            } else {
                log.warn("Password has NOT changed");
            }

            // If password was changed (or new user), encrypt it
            if (passwordChanged) {
                user.setPassword(passwordEncoder.encodePassword(user.getPassword(), null));
            }
        } else {
            log.warn("passwordEncoder is null");
            log.warn("PasswordEncoder not set, skipping password encryption...");
        }
        log.warn("End of password encryption section");
        
        try {
            return dao.saveUser(user);
        } catch (DataIntegrityViolationException e) {
            e.printStackTrace();
            log.warn(e.getMessage());
            throw new UserExistsException("User '" + user.getUsername() + "' already exists!");
        } catch (EntityExistsException e) { // needed for JPA
            e.printStackTrace();
            log.warn(e.getMessage());
            throw new UserExistsException("User '" + user.getUsername() + "' already exists!");
        }
    }
    

    /**
     * {@inheritDoc}
     */
    public void removeUser(String userId) {
        log.warn("removing user: " + userId);
        dao.remove(new Long(userId));
    }

    /**
     * {@inheritDoc}
     * @param username the login name of the human
     * @return User the populated user object
     * @throws UsernameNotFoundException thrown when username not found
     */
    public User getUserByUsername(String username) throws UsernameNotFoundException {
        return (User) dao.loadUserByUsername(username);
    }
}
