/*
 * Copyright (C) 1998-2009, <a href="http://www.ssel.caltech.edu">SSEL</a>,
 * at Caltech, Pasadena, CA 91125.
 *
 * MooreRecruiting is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 * USA.
 *
 */


package edu.caltech.ssel.recruiting.service;

import edu.caltech.ssel.recruiting.dao.ParticipantRecordDao;
import edu.caltech.ssel.recruiting.dao.QuestionResultDao;
import edu.caltech.ssel.recruiting.dao.SurveyDao;
import edu.caltech.ssel.recruiting.dao.ExperimentTypeDao;
import edu.caltech.ssel.recruiting.model.BooleanCondition;
import edu.caltech.ssel.recruiting.model.NoShowCondition; // BRS
import edu.caltech.ssel.recruiting.model.Condition;
import edu.caltech.ssel.recruiting.model.ExperimentTypeCondition;
import edu.caltech.ssel.recruiting.model.Filter;
import edu.caltech.ssel.recruiting.model.PoolCondition;
import edu.caltech.ssel.recruiting.model.QuestionResultCondition;
import edu.caltech.ssel.recruiting.model.Subject;
import java.util.List;
import org.appfuse.service.GenericManager;

/**
 *
 * @author michaelk
 */
public interface ConditionManager extends GenericManager<Condition, Long> {
    public boolean isEligible(Condition c, Subject u) throws Exception;
    public boolean isEligible(Filter f, Subject u) throws Exception;
    public boolean isPreEligible(Condition c, Subject u) throws Exception;
    public void checkEligible(Condition c, List<Subject> lst) throws Exception;
    public void setQuestionResultDao(QuestionResultDao dao);
    public void setSurveyDao(SurveyDao surveyDao);
    public void setParticipantRecordDao(ParticipantRecordDao participantRecordDao);
    public void setExperimentTypeDao(ExperimentTypeDao experimentTypeDao);
    public Condition getByIdString(String id) throws Exception;//For DWR
    public QuestionResultCondition createQuestionResultCondition(String name, String q_id, String[] opts, String type) throws Exception;
    public PoolCondition createPoolCondition(String name, String[] poolIds, String onList) throws Exception;
    public ExperimentTypeCondition createExperimentTypeCondition(String name, String [] typeIds, String onList, String history) throws Exception;
    public BooleanCondition createBooleanCondition(String name, String first_cond_id, String second_cond_id, String op) throws Exception;

    // BRS
    public NoShowCondition createNoShowCondition(String name, String maxNoShows) throws Exception;
    public List<String> getEligibleUserEmailsByFilter(Filter f, boolean includeUnsubscribed) throws Exception;

    public List<String> getEligibleUserEmailsByFilter(Filter f) throws Exception;
    public List<Subject> getEligibleSubjectsByFilter(Filter f) throws Exception;
    public List<String> getEligibleUserEmailsByFilterConditions(String [] filterIdList) throws Exception;
    public String getEligibleUserStatisticsByFilterConditions(String [] condIdList) throws Exception;
    public Filter setupBasicFilterById(String filterId, String experimentTypeId) throws Exception;
}
