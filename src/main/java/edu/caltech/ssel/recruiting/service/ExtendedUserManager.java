/*
 * Copyright (C) 1998-2009, <a href="http://www.ssel.caltech.edu">SSEL</a>,
 * at Caltech, Pasadena, CA 91125.
 *
 * MooreRecruiting is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 * USA.
 *
 */


package edu.caltech.ssel.recruiting.service;

import edu.caltech.ssel.recruiting.model.Experimenter;
import edu.caltech.ssel.recruiting.model.Subject;
import java.util.List;
import org.appfuse.model.User;
import org.appfuse.service.UserManager;

// BRS 2010-08-19
import org.appfuse.service.UserExistsException;
import org.springframework.security.providers.encoding.PasswordEncoder;

/**
* @author Michael Kolodrubetz, SSEL, Caltech
* @author <a href="mailto:wmyuan@hss.caltech.edu">Walter M. Yuan</a>, SSEL, Caltech
*/
public interface ExtendedUserManager extends UserManager {
    public List<Experimenter> getExperimenters();
    public List<String> getExperimenterEmails();
    public Experimenter getExperimenterById(Long id);
    public List<Subject> getSubjects();
    public List<String> getSubjectEmails();
    public Subject getSubjectById(Long id);
    public Long getSubjectIDByParticipationID(Long id);
    public List<Subject> getSubjectBySchoolID(String sid);
    public Subject getSubjectByEmail(String email);
    public List<User> getAdmins();
    public List<User> getStandardUsers();
    public List<User> getEnabledUsers();
    public List<String> getEnabledUserEmails();

    // BRS 2010-08-19 (see ExtenderUserManagerImpl)
    public User saveUser(User user) throws UserExistsException;
    public void setPasswordEncoder(PasswordEncoder passwordEncoder);

    // BRS 2011-11-01
    public User getNonSubjectByEmail(String email);
}
