/*
 * Copyright (C) 1998-2009, <a href="http://www.ssel.caltech.edu">SSEL</a>,
 * at Caltech, Pasadena, CA 91125. 
 *
 * MooreRecruiting is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 * USA.
 *
 */

package edu.caltech.ssel.recruiting.service.impl;

import edu.caltech.ssel.recruiting.dao.MessageDao;
import edu.caltech.ssel.recruiting.model.Message;
import edu.caltech.ssel.recruiting.service.MessageManager;
import java.util.List;
import org.appfuse.model.User;
import org.appfuse.service.impl.GenericManagerImpl;

/**
* @author Michael Kolodrubetz, SSEL, Caltech
*/
public class MessageManagerImpl extends GenericManagerImpl<Message, Long> implements MessageManager {

    private MessageDao dao = null;
    
    public MessageManagerImpl(MessageDao prDao) {
        super(prDao);
        this.dao = prDao;
    }
    
    public List<Message> findSentByCreator(User creator) {
        return dao.findSentByCreator(creator);
    }
    
    public List<Message> findDraftsByCreator(User creator) {
        return dao.findDraftsByCreator(creator);
    }

    /** for DWR convenience
     */
    public Message findByIdString(String id) {
        return dao.get(new Long(id));
    }
}
