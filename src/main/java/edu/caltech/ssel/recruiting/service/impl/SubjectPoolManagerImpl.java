/*
 * Copyright (C) 1998-2009, <a href="http://www.ssel.caltech.edu">SSEL</a>,
 * at Caltech, Pasadena, CA 91125. 
 *
 * MooreRecruiting is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 * USA.
 *
 */

package edu.caltech.ssel.recruiting.service.impl;

import edu.caltech.ssel.recruiting.dao.SubjectPoolDao;
import edu.caltech.ssel.recruiting.model.SubjectPool;
import edu.caltech.ssel.recruiting.service.SubjectPoolManager;
import java.util.Date;
import java.util.List;
import org.appfuse.service.impl.GenericManagerImpl;

/**
* @author Michael Kolodrubetz, SSEL, Caltech
*/
public class SubjectPoolManagerImpl extends GenericManagerImpl<SubjectPool, Long> implements SubjectPoolManager {
    SubjectPoolDao dao;
    
    public SubjectPoolManagerImpl(SubjectPoolDao s){
        super(s);
        dao = s;
    }

    public List<SubjectPool> findByName(String name) {
        return dao.findByName(name);
    }

    public SubjectPool getByString(String id) throws Exception {
        return get(Long.parseLong(id));
    }
    
    public SubjectPool getById(Long id) {
        return dao.getSubjectPoolById(id);
    }

    public SubjectPool addNew(String name, String desc, String consent) {
        SubjectPool s = new SubjectPool();
        s.setName(name);
        s.setConsentInfo(consent);
        s.setDescr(desc);
        s.setRegDate(new Date());
        s.setValid(true);
        return save(s);
    }

    public List<SubjectPool> getValidPools() {
        return dao.getValidPools(); 
    }
}
