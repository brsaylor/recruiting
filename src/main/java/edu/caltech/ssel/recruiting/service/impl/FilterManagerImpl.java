/*
 * Copyright (C) 1998-2009, <a href="http://www.ssel.caltech.edu">SSEL</a>,
 * at Caltech, Pasadena, CA 91125. 
 *
 * MooreRecruiting is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 * USA.
 *
 */

package edu.caltech.ssel.recruiting.service.impl;

import edu.caltech.ssel.recruiting.model.Filter;
import edu.caltech.ssel.recruiting.service.FilterManager;
import org.appfuse.dao.GenericDao;
import org.appfuse.service.impl.GenericManagerImpl;

/**
 *
 * @author michaelk
 */
public class FilterManagerImpl extends GenericManagerImpl<Filter, Long> implements FilterManager {
    
    private GenericDao<Filter, Long> dao;
    
    /** Creates a new instance of FilterManagerImpl */
    public FilterManagerImpl(GenericDao<Filter, Long> filterDao) {
        super(filterDao);
        this.dao = filterDao;
    }

    public Filter getByIdString(String id) throws Exception {
        return dao.get(Long.parseLong(id));
    }
    
}
