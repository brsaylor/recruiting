/*
 * Copyright (C) 1998-2009, <a href="http://www.ssel.caltech.edu">SSEL</a>,
 * at Caltech, Pasadena, CA 91125.
 *
 * MooreRecruiting is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 * USA.
 *
 */

package edu.caltech.ssel.recruiting.service;

import edu.caltech.ssel.recruiting.model.Experiment;
import edu.caltech.ssel.recruiting.model.Filter;
import edu.caltech.ssel.recruiting.model.Survey;
import java.util.Date;
import java.util.List;
import java.util.Map;
import org.appfuse.service.GenericManager;

/**
* @author Michael Kolodrubetz, SSEL, Caltech
* @author <a href="mailto:wmyuan@hss.caltech.edu">Walter M. Yuan</a>, SSEL, Caltech
*/
public interface ExperimentManager extends GenericManager<Experiment, Long> {
    public List<Experiment> getByDate(Date d);
    public List<Experiment> getByDateAndLocationId(Date d, Long locId);
    public List<Experiment> getBySurvey(Survey s);
    public List<Experiment> getBySurveyId(Long id);
    public List<Experiment> getByFilter(Filter f);
    public List<Experiment> getByFilterId(Long id);
    public List<Experiment> getByRunby(Long runbyId);
    public List<Experiment> getByReservedBy(Long reservedBy);
    public List<Experiment> getByPrinciple(Long principle);
    public List<Experiment> getByExperimentType(Long expTypeId);
    public List<Experiment> getByLocation(Long locationId);
    public List<Experiment> getById(Long id);
    public Date getFirstExperimentDate();
    public List<Experiment> getByRunbyAndType(Long runbyId, Long expTypeId);
    public Experiment getByIdString(String expId); // for DWR
    public List<Experiment> getConflicts(
            String dateString,
            String startTimeString,
            String setupBuffer,
            String endTimeString,
            String cleanupBuffer,
            String expId,
            String locId,

            // BRS
            String reserveLaptops)
    throws Exception; // also DWR

    // BRS
    public boolean experimentsOverlapWithoutBuffer(Experiment e1, Experiment e2);
    public boolean periodsOverlap(
            Date st1, // period 1 start date+time
            Date et1, // period 1 end date+time
            Date st2, // period 2 start date+time
            Date et2  // period 2 end date+time
            );

    // BRS 2011-10-28
    public Object getCurrentValue(Long id, String fieldName);

    // BRS 2011-12-08
    // Return some basic info about the experiments of the given type
    public Map[] getExperimentListByType(Long expTypeId);
}
