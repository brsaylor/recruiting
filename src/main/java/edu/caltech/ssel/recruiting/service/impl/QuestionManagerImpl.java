/*
 * Copyright (C) 1998-2009, <a href="http://www.ssel.caltech.edu">SSEL</a>,
 * at Caltech, Pasadena, CA 91125. 
 *
 * MooreRecruiting is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 * USA.
 *
 */

package edu.caltech.ssel.recruiting.service.impl;

import edu.caltech.ssel.recruiting.model.Question;
import edu.caltech.ssel.recruiting.model.Question.QStyle;
import edu.caltech.ssel.recruiting.service.QuestionManager;
import java.util.Arrays;
import org.appfuse.dao.GenericDao;
import org.appfuse.service.impl.GenericManagerImpl;

/**
* @author Michael Kolodrubetz, SSEL, Caltech
*/
public class QuestionManagerImpl extends GenericManagerImpl<Question, Long> implements QuestionManager {

    /** Creates a new instance of QuestionManagerImpl */
    public QuestionManagerImpl(GenericDao<Question, Long> dao) {
        super(dao);
    }

    //For DWR, only accepts String arguments
    public Question getByIdString(String id) {
        return get(Long.parseLong(id));
    }

    public Question saveQuestion(String label, String question, String style, String[] options) {
        Question q = new Question();
        q.setLabel(label);
        q.setQuestion(question);
        q.setStyle(QStyle.valueOf(style));
        q.setOptionList(Arrays.asList(options));
        return super.save(q);
    }

}
