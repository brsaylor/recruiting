/*
 * Copyright (C) 1998-2009, <a href="http://www.ssel.caltech.edu">SSEL</a>,
 * at Caltech, Pasadena, CA 91125.
 *
 * MooreRecruiting is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 * USA.
 *
 */

package edu.caltech.ssel.recruiting.common;

import edu.caltech.ssel.recruiting.model.Experiment;
import java.util.List;
import java.util.Set;
import javax.servlet.http.HttpServletRequest;
import org.displaytag.properties.SortOrderEnum;


/**
* @author Richar Ho, CASSEL, UCLA
* @author <a href="mailto:wmyuan@hss.caltech.edu">Walter M. Yuan</a>, SSEL, Caltech
*/
public class ExtendedPaginatedListImpl implements ExtendedPaginatedList {

    /** current page index, starts at 0 */
    private int index;

    /** number of results per page (number of rows per page to be displayed ) */
    private int pageSize;

    /** total number of results (the total number of rows ) */
    private int fullListSize;

    /** list of results (rows found ) in the current page */
    private List list;

    /** default sorting order */
    private SortOrderEnum sortDirection = SortOrderEnum.ASCENDING;

    /** sort criteria (sorting property name) */
    private String sortCriterion;

    /** Http servlet request * */
    private HttpServletRequest request;
    
    private Set searchIds;
    
    private boolean search;
    
    private Experiment experiment;

    /** default constructor * */
    public ExtendedPaginatedListImpl() {
    }

    /**
     * Create <code>PaginatedListImpl</code> instance using the
     * <code>HttpServletRequest</code> object.
     * 
     * @param request
     *            <code>HttpServletRequest</code> object.
     */
    /**
     * Create <code>PaginatedListImpl</code> instance using the
     * <code>HttpServletRequest</code> object.
     * 
     * @param request
     *            <code>HttpServletRequest</code> object.
     * @param pageSize
     *            the page size - the total number of rows per page.
     */
    public ExtendedPaginatedListImpl(HttpServletRequest request, int pageSize) {
        sortCriterion = request
                .getParameter(ExtendedPaginatedList.IRequestParameters.SORT);
        sortDirection = ExtendedPaginatedList.IRequestParameters.DESC
                .equals(request
                        .getParameter(ExtendedPaginatedList.IRequestParameters.DIRECTION)) ? SortOrderEnum.DESCENDING
                : SortOrderEnum.ASCENDING;
        // pageSize = pageSize != 0 ? pageSize : DEFAULT_PAGE_SIZE;  // BRS 2011-06-09 - remove this to allow pageSize to be 0 (all records)
        String page = request
                .getParameter(ExtendedPaginatedList.IRequestParameters.PAGE);
        index = page == null ? 0 : Integer.parseInt(page) - 1;
    }

    /**
     * Create <code>PaginatedListImpl</code> instance .
     * 
     * @param pageSize
     *            the page size - the total number of rows per page.
     * @return <code>ExtendedPaginatedList</code> instance.
     * @throws Exception -
     *             problem while creating paginatedlist object.
     */
    public ExtendedPaginatedList getPaginatedList(int pageSize)
            throws Exception {

        if (request == null) {
            throw new Exception(
                    "Cannot create paginated list. Depends upon HttpServletRequest.");
        }
        return new ExtendedPaginatedListImpl(request, pageSize);
    }

    /**
     * Set the non-null <code>HttpServletRequest</code> object.
     * 
     * @param request
     *            a <code>HttpServletRequest</code> object.
     */
    public void setRequest(HttpServletRequest request) {
        this.request = request;
    }

    public int getFirstRecordIndex() {
        return index * pageSize;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public List getList() {
        return list;
    }

    public void setList(List results) {
        this.list = results;
    }

    public int getFullListSize() {
        return fullListSize;
    }

    public void setTotalNumberOfRows(int total) {
        this.fullListSize = total;
    }

    public int getTotalPages() {
        return (int) Math.ceil(((double) fullListSize) / pageSize);
    }

    public int getObjectsPerPage() {
        return pageSize;
    }

    public int getPageNumber() {
        return index + 1;
    }
    
    public void setSearchIds(Set<Long> searchIds) {
        this.searchIds = searchIds;
    }
    public Set<Long> getSearchIds() {
        return searchIds;
    }

    // BRS 2012-02-08: required by PaginatedList interface we're now implementing
    public String getSearchId() {
        return null;
    }
    
    public void setSearch(boolean search) {
        this.search = search;
    }
    
    public boolean getSearch() {
        return search;
    }

    public String getSortCriterion() {
        return sortCriterion;
    }

    public SortOrderEnum getSortDirection() {
        return sortDirection;
    }

    public void setSortCriterion(String sortCriterion) {
        this.sortCriterion = sortCriterion;
    }

    public void setSortDirection(SortOrderEnum sortDirection) {
        this.sortDirection = sortDirection;
    }
    
    public void setExperiment(Experiment experiment) {
        this.experiment = experiment;
    }
    
    public Experiment getExperiment() {
        return experiment;
    }
}

