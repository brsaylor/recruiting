/*
 * Copyright (C) 1998-2009, <a href="http://www.ssel.caltech.edu">SSEL</a>,
 * at Caltech, Pasadena, CA 91125.
 *
 * MooreRecruiting is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 * USA.
 *
 */

package edu.caltech.ssel.recruiting.common;

import javax.servlet.http.HttpServletRequest;
import org.displaytag.properties.SortOrderEnum;

/**
* @author Richar Ho, CASSEL, UCLA
* @author <a href="mailto:wmyuan@hss.caltech.edu">Walter M. Yuan</a>, SSEL, Caltech
*/
public class PaginateListFactory {

    public ExtendedPaginatedList getPaginatedListFromRequest(
            HttpServletRequest request) {

        ExtendedPaginatedList paginatedList = new ExtendedPaginatedListImpl();
        String sortCriterion = null;
        String thePage = null;
        int pageSize = 25; // Rows per page
        if (request != null) {
            sortCriterion = request
                    .getParameter(ExtendedPaginatedList.IRequestParameters.SORT);
            paginatedList
                    .setSortDirection(ExtendedPaginatedList.IRequestParameters.DESC
                            .equals(request.getParameter(ExtendedPaginatedList.IRequestParameters.DIRECTION)) 
                            ? SortOrderEnum.DESCENDING : SortOrderEnum.ASCENDING);
            thePage = request
                    .getParameter(ExtendedPaginatedList.IRequestParameters.PAGE);

            // BRS 2011-06-09 - get pageSize from request instead of hard-coding to 25
            if (request.getParameter("pageSize") != null) {
                pageSize = Integer.parseInt(request.getParameter("pageSize"));
            }
        }
        paginatedList.setSortCriterion(sortCriterion);
        paginatedList.setPageSize(pageSize);
        if (thePage != null) {
            int index = paginatedList == null ? 0
                    : Integer.parseInt(thePage) - 1;
            paginatedList.setIndex(index);
        } else {
            paginatedList.setIndex(0);
        }

        return paginatedList;
    }
}


