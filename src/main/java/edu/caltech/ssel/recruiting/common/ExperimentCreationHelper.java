/*
 * Copyright (C) 1998-2009, <a href="http://www.ssel.caltech.edu">SSEL</a>,
 * at Caltech, Pasadena, CA 91125.
 *
 * MooreRecruiting is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 * USA.
 *
 */

package edu.caltech.ssel.recruiting.common;

import edu.caltech.ssel.recruiting.model.Experiment;
import edu.caltech.ssel.recruiting.model.Filter;
import edu.caltech.ssel.recruiting.model.Survey;
import javax.servlet.http.HttpSession;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.appfuse.service.GenericManager;
import org.appfuse.service.MailEngine;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.web.servlet.ModelAndView;

/**
* @author Michael Kolodrubetz, SSEL, Caltech
* @author Walter M. Yuan, SSEL, Caltech
*/
public class ExperimentCreationHelper {
    
    /**
     * Creates a new instance of ECHelper
     */
    public ExperimentCreationHelper() {
    }
    
    private GenericManager<Survey, Long> surveyManager;
    private GenericManager<Filter, Long> filterManager;
    private GenericManager<Experiment, Long> experimentManager;
    private HttpSession session;
    private MailEngine mailEngine;
    private static final Log log = LogFactory.getLog(ExperimentCreationHelper.class);
    
    private static final String experimentString = "redirect:experimentform.html";
    private static final String surveyString = "redirect:surveycreationform.html";
    private static final String filterString = "redirect:filterform.html";
    private static final String announcementString = "redirect:announcementform.html";
    private static final String verificationString = "redirect:experimentverificationform.html";
    
    public ModelAndView getExperimentView() {
        return new ModelAndView(experimentString);
    }
    
    public ModelAndView getSurveyView() {
        Experiment exp = this.getExperiment();
        return new ModelAndView(surveyString).addObject("id", exp.getSurvey().getId());
    }
    
    public ModelAndView getFilterView() {
        Experiment exp = this.getExperiment();
        return new ModelAndView(filterString).addObject("id", exp.getFilter().getId());
    }
    
    public ModelAndView getAnnouncementView() {
        return new ModelAndView(announcementString);
    }
    
    public ModelAndView getVerificationView() {
        return new ModelAndView(verificationString);
    }
    
    public void setUseSurvey(boolean value) {
        session.setAttribute("useSurveyForm", new Boolean(value));
    }
    
    public void setUseFilter(boolean value) {
        session.setAttribute("useFilterForm", new Boolean(value));
    }
    
    public void setUseAnnouncement(boolean value) {
        session.setAttribute("useAnnouncementForm", new Boolean(value));
    }
    
    public boolean useSurvey() {
        return ((Boolean)session.getAttribute("useSurveyForm")).booleanValue();
    }
    
    public boolean useFilter() {
        return ((Boolean)session.getAttribute("useFilterForm")).booleanValue();
    }
    
    public boolean useAnnouncement() {
        return ((Boolean)session.getAttribute("useAnnouncementForm")).booleanValue();
    }
    
    public void cancelExperimentCreation() {
        Experiment e = getExperiment();
        if(e != null) {
            if(e.getSurvey() != null && e.getSurvey().getId() != null && useSurvey()) {
                surveyManager.remove(e.getSurvey().getId());
            }
            if(e.getFilter() != null && e.getFilter().getId() != null && useFilter()) {
                filterManager.remove(e.getFilter().getId());
            }
        }
        clearSession();
    }
    
    public void clearSession() {
        session.removeAttribute("myExperiment");
        session.removeAttribute("myAnnouncement");
        session.removeAttribute("useSurveyForm");
        session.removeAttribute("useFilterForm");
        session.removeAttribute("useAnnouncementForm");
    }
    
    public Experiment finishExperimentCreation() {
        Experiment e = getExperiment();
        e = experimentManager.save(e);
        if(useAnnouncement() && getAnnouncement() != null) {
            try {
                mailEngine.send(getAnnouncement());
            } catch(NullPointerException exc) {
                log.debug("NullPointerException thrown: " + exc.getMessage());
                log.debug("Emails must have at least one recipient");
                System.err.println("Recipientless emails should be checked for in validation");
            }
        }
        clearSession();
        
        return e;
    }
    
    public static SimpleMailMessage getDefaultMessage(Experiment e) {
        SimpleMailMessage msg = new SimpleMailMessage();
        if(e != null && e.getReservedBy() != null)
            msg.setFrom(e.getReservedBy().getEmail());
        msg.setText("Standard Experiment Email Text...");
        msg.setSubject("New Experiment");
        return msg;
    }
    
    public Experiment getExperiment() {
        return (Experiment) session.getAttribute("myExperiment");
    }
    
    public void setExperiment(Experiment e) {
        session.setAttribute("myExperiment", e);
    }
    
    public SimpleMailMessage getAnnouncement() {
        return (SimpleMailMessage) session.getAttribute("myAnnouncement");
    }
    
    public void setAnnouncement(SimpleMailMessage msg) {
        session.setAttribute("myAnnouncement", msg);
    }
    
    public void setSurveyManager(GenericManager<Survey, Long> surveyManager) {
        this.surveyManager = surveyManager;
    }
    
    public void setFilterManager(GenericManager<Filter, Long> filterManager) {
        this.filterManager = filterManager;
    }
    
    public HttpSession getSession() {
        return session;
    }
    
    public void setSession(HttpSession session) {
        this.session = session;
    }
    
    public void setExperimentManager(GenericManager<Experiment, Long> experimentManager) {
        this.experimentManager = experimentManager;
    }
    
    public void setMailEngine(MailEngine mailEngine) {
        this.mailEngine = mailEngine;
    }
    
}
