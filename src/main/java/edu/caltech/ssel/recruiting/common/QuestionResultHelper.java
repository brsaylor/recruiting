/*
 * Copyright (C) 1998-2009, <a href="http://www.ssel.caltech.edu">SSEL</a>,
 * at Caltech, Pasadena, CA 91125.
 *
 * MooreRecruiting is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 * USA.
 *
 */

package edu.caltech.ssel.recruiting.common;

import edu.caltech.ssel.recruiting.model.Question;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
* @author Michael Kolodrubetz, SSEL, Caltech
* @author <a href="mailto:wmyuan@hss.caltech.edu">Walter M. Yuan</a>, SSEL, Caltech
*/
public class QuestionResultHelper {
    
    /**
     * Creates a new instance of QuestionResultHelper
     */
    public QuestionResultHelper() {
    }
    
    private static final Log log = LogFactory.getLog(QuestionResultHelper.class);
    
    public static boolean hasDigit(int num, int dig) {
        for(int j = 0; j < dig; j++)
            num /= 2;
        return (num % 2 == 1);
    }
    
    public static int setDigits(boolean [] dig) {
        int n = 0;
        for(int j = dig.length - 1; j >= 0; j--) {
            n *=2;
            if(dig[j])
                n++;
        }
        return n;
    }
    
    public static int parseAnswer(HttpServletRequest request, Question q, int index) {
        //Parse the answer
        //int ans = 0;
        String s;
        int sz = q.getOptionList().size();
        switch (q.getStyle()) {
            case CHECK_BOX:
                boolean [] blist = new boolean[sz];
                for(int k = 0; k < sz; k++) {
                    s = "question" + index + "," + k;
                    if(request.getParameter(s) != null)
                        blist[k] = true;
                    else
                        blist[k] = false;
                }
                return QuestionResultHelper.setDigits(blist);
            case RADIO:
            case DROP_LIST:
                s = "question" + index;
                return Integer.parseInt(request.getParameter(s));
            default:
                log.debug("There is some sort of problem, since nothing was selected");
                return 0;
        }
    }
}
