/*
 * Copyright (C) 1998-2009, <a href="http://www.ssel.caltech.edu">SSEL</a>,
 * at Caltech, Pasadena, CA 91125.
 *
 * MooreRecruiting is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 * USA.
 *
 */

package edu.caltech.ssel.recruiting.common;

import java.util.Enumeration;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.logging.Log;

/**
* @author Michael Kolodrubetz, SSEL, Caltech
* @author <a href="mailto:wmyuan@hss.caltech.edu">Walter M. Yuan</a>, SSEL, Caltech
*/
public class DebugHelper {
    
    /** Creates a new instance of DebugHelper */
    public DebugHelper() {
    }
    
    public static void displayParams(HttpServletRequest request, Log log) {
        Map params = request.getParameterMap();
        Set keys = params.keySet();
        log.debug("The parameters are...");
        for(Iterator<String> i = keys.iterator(); i.hasNext(); ) {
            String k = (String)i.next();
            String[] vals = (String[])params.get(k);
            for(int j = 0; j < vals.length; j++) {
                log.debug("\t" + k + ": " + vals[j]);
            }
        }
    }
    
    public static void displaySessionAttributes(HttpServletRequest request, Log log) {
        Enumeration en = request.getSession().getAttributeNames();
        log.debug("Displaying session attributes...");
        while(en.hasMoreElements()) {
            log.debug("\t" + en.nextElement());
        }
    }
    
}
