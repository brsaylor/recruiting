/*
 * Copyright (C) 1998-2009, <a href="http://www.ssel.caltech.edu">SSEL</a>,
 * at Caltech, Pasadena, CA 91125.
 *
 * MooreRecruiting is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 * USA.
 *
 */

package edu.caltech.ssel.recruiting.common;

import edu.caltech.ssel.recruiting.model.Experiment;
import java.util.List;
import java.util.Set;
import javax.servlet.http.HttpServletRequest;
import org.displaytag.properties.SortOrderEnum;

// BRS 2012-02-08
import org.displaytag.pagination.PaginatedList;

/**
* @author Richar Ho, CASSEL, UCLA
* @author <a href="mailto:wmyuan@hss.caltech.edu">Walter M. Yuan</a>, SSEL, Caltech
*/
public interface ExtendedPaginatedList

    extends PaginatedList  // BRS 2012-02-08

{
     public interface IRequestParameters{
        String SORT = "sort";
        String PAGE = "page";
        String ASC = "asc";
        String DESC = "desc";
        String DIRECTION = "dir";
    }
    int DEFAULT_PAGE_SIZE = 25;
    public ExtendedPaginatedList getPaginatedList(int pageSize) throws Exception;
    public void setRequest(HttpServletRequest request);
    public int getFirstRecordIndex();
    public int getIndex();
    public void setIndex(int index);
    public int getPageSize();
    public void setPageSize(int pageSize);
    public List getList();
    public void setList(List results);
    public int getFullListSize();
    public void setTotalNumberOfRows(int total);
    public int getTotalPages();
    public int getObjectsPerPage();
    public int getPageNumber();
    public void setSearchIds(Set<Long> searchIds);
    public Set<Long> getSearchIds();
    public void setSearch(boolean search);
    public boolean getSearch();
    public String getSortCriterion();
    public SortOrderEnum getSortDirection();
    public void setSortCriterion(String sortCriterion);
    public void setSortDirection(SortOrderEnum sortDirection);
    public void setExperiment(Experiment experiment);
    public Experiment getExperiment();
}
