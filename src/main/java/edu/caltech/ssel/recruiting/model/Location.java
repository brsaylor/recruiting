/*
 * Copyright (C) 1998-2009, <a href="http://www.ssel.caltech.edu">SSEL</a>,
 * at Caltech, Pasadena, CA 91125.
 *
 * MooreRecruiting is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 * USA.
 *
 */


package edu.caltech.ssel.recruiting.model;
import javax.persistence.*;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.appfuse.model.Address;
import org.appfuse.model.BaseObject;

/**
* @author Michael Kolodrubetz, SSEL, Caltech
* @author <a href="mailto:wmyuan@hss.caltech.edu">Walter M. Yuan</a>, SSEL, Caltech
*/
@Entity
@Table(name="location")
public class Location extends BaseObject {
    
    private Long id;
    private String name;
    private String contact;
    private String contactEmail;
    private String phone;
    private Address addr;
    private String mapLink;
    
    /** Creates a new instance of Location */
    public Location() {
	addr = new Address();
    }
    
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
        .append("id", getId())
        .append("name", getName())
        .append("contact", getContact())
        .append("contactEmail", getContactEmail())
        .append("phone", getPhone())
        .append("addr", getAddr())
        .append("mapLink", getMapLink()).toString();
    }
    
    public int hashCode() {
	return name.hashCode() * contactEmail.hashCode();
    }
    
    public boolean equals(Object o) {
        if(o == null)
            return false;
        if(!(o instanceof Location))
            return false;
        
	Location loc = (Location)o;
	return StringUtils.equals(loc.getName(), getName());
    }
    
    @Column(unique=true)
    public String getName() {
	return name;
    }
    
    public void setName(String name) {
	this.name = name;
    }
    
    public String getContact() {
	return contact;
    }
    
    public void setContact(String contact) {
	this.contact = contact;
    }

    public String getContactEmail() {
        return contactEmail;
    }

    public void setContactEmail(String contactEmail) {
        this.contactEmail = contactEmail;
    }
    
    public String getPhone() {
	return phone;
    }
    
    public void setPhone(String phone) {
	this.phone = phone;
    }
    
    public Address getAddr() {
	return addr;
    }
    
    public void setAddr(Address addr) {
	this.addr = addr;
    }
    
    //Get individual address attributes for AJAX
    @Transient
    public String getAddress() {
        return addr.getAddress();
    }
    
    @Transient
    public String getCity() {
        return addr.getCity();
    }
    
    @Transient
    public String getState() {
        return addr.getProvince();
    }
    
    @Transient
    public String getZip() {
        return addr.getPostalCode();
    }
    
    @Transient
    public String getCountry() {
        return addr.getCountry();
    }
    
    public String getMapLink() {
	return mapLink;
    }
    
    public void setMapLink(String mapLink) {
	this.mapLink = mapLink;
    }
    
    @Id @GeneratedValue(strategy=GenerationType.AUTO)
    public Long getId() {
	return id;
    }
    
    public void setId(Long id) {
	this.id = id;
    }
    
}
