/*
 * Copyright (C) 1998-2009, <a href="http://www.ssel.caltech.edu">SSEL</a>,
 * at Caltech, Pasadena, CA 91125.
 *
 * MooreRecruiting is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 * USA.
  */


package edu.caltech.ssel.recruiting.model;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.*;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.appfuse.model.BaseObject;
import org.appfuse.model.User;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

/**
* @author Michael Kolodrubetz, SSEL, Caltech
* @author <a href="mailto:wmyuan@hss.caltech.edu">Walter M. Yuan</a>, SSEL, Caltech
*/
@Entity
@Table(name="filters")
//Filter is a reserved word in MySQL
public class Filter extends BaseObject{
    
    private List<Condition> conditions;
    private User creator;
    private Long id;
    private String name;
    private boolean checkedIRB=false;
    private boolean basic=false;
    
    /** Creates a new instance of Filter */
    public Filter() {
    }

    /* Functions to be defined later
    public List<Subject> getEligibleList() {
    }
    
    public boolean isEligible(Subject subj) {
        
    }*/
    
//    @ManyToMany(fetch=FetchType.EAGER)
    @ManyToMany(cascade = CascadeType.ALL)  // BRS 2011-11-14 added cascade
    /*
    @JoinTable(
            name="Filters_Conditions",
            joinColumns = { @JoinColumn( name="Filters_id") },
            inverseJoinColumns = @JoinColumn( name="conditions_id")
    )
    @CollectionId(
        columns = @Column(name="filter_condition"), 
        type=@Type(type="long"), 
        generator = "identity"
    )
     */
//    @IndexColumn(name="index_id", base=0)
    @LazyCollection(LazyCollectionOption.FALSE)
    //These Hibernate "LazyCollection" options work.  A previous version had FetchType.EAGER and the
    //join returned too many (erroneous) records - poneil
    public List<Condition> getConditions() {
        return conditions;
    }

    public void setConditions(List<Condition> conditions) {
        this.conditions = conditions;
    }

    public void addCondition(Condition cond) {
        conditions.add(cond);
    }
    
    @Id @GeneratedValue(strategy=GenerationType.AUTO)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(unique=true)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @ManyToOne  //This is a ManyToOne mapping because there are potentially many Filters per User
    public User getCreator() {
        return creator;
    }

    public void setCreator(User creator) {
        this.creator = creator;
    }

    @Column(name="checked_irb", nullable=false)
    public boolean isCheckedIRB() {
        return checkedIRB;
    }

    public void setCheckedIRB(boolean checkedIRB) {
        this.checkedIRB = checkedIRB;
    }

    @Column(name="is_basic", nullable=false)
    public boolean isBasic() {
        return basic;
    }

    public void setBasic(boolean basic) {
        this.basic = basic;
    }

    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
        .append("id", getId())
        .append("name", getName())
        .append("conditions", getConditions()).toString();
    }

    public boolean equals(Object o) {
        if(o == null || !(o instanceof Filter))
            return false;
        
        return StringUtils.equals(((Filter)o).getName(), getName());
    }

    public int hashCode() {
        return (int)(getId() % Integer.MAX_VALUE);
    }
    
    public Filter clone() {
        Filter f = new Filter();
        f.name = name;
        f.conditions = new ArrayList();
        for(int j = 0; j < conditions.size(); j++)
            f.conditions.add(conditions.get(j).clone());
        return f;
    }
    
}
