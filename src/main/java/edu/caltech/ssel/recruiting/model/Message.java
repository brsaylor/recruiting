/*
 * Copyright (C) 1998-2009, <a href="http://www.ssel.caltech.edu">SSEL</a>,
 * at Caltech, Pasadena, CA 91125.
 *
 * MooreRecruiting is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 * USA.
 *
 */

package edu.caltech.ssel.recruiting.model;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import org.appfuse.model.User;
import org.appfuse.model.BaseObject;

/**
* @author Michael Kolodrubetz, SSEL, Caltech
* @author <a href="mailto:wmyuan@hss.caltech.edu">Walter M. Yuan</a>, SSEL, Caltech
*/
@Entity
@Table(name="message")
public class Message extends BaseObject implements java.io.Serializable {

    private Long id;
    private User creator;
    private String subject;
    private String text;
    private Status status;
    private Date whenCreated;
    private Set<UserMessage> messageReferences = new HashSet<UserMessage>(0);
    
    public enum Status {
        DRAFT, SENT, RETRACTED, DELETED;
        public String toString() {
            switch(this) {
                case DRAFT:
                    return "Draft";
                case SENT:
                    return "Sent";
                case RETRACTED:
                    return "Retracted";
                case DELETED:
                    return "Deleted";
            }
            return "";
        }
        //For web stuff
        public String getName() {
            return this.name();
        }
    };

    public Message() {
    }
	
    public Message(Long id, User creator) {
        this.id = id;
        this.creator = creator;
    }
    public Message(Long id, User creator, String subject, String text, Status status, Date whenCreated, Set<UserMessage> messageReferences) {
       this.id = id;
       this.creator = creator;
       this.subject = subject;
       this.text = text;
       this.status = status;
       this.whenCreated = whenCreated;
       this.messageReferences = messageReferences;
    }
   
    @Id @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name="id", unique=true, nullable=false)
    public Long getId() {
        return this.id;
    }
    
    public void setId(Long id) {
        this.id = id;
    }
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="creator_id", nullable=false)
    public User getCreator() {
        return this.creator;
    }
    
    public void setCreator(User creator) {
        this.creator = creator;
    }
    
    @Column(name="subject", length=50)
    public String getSubject() {
        return this.subject;
    }
    
    public void setSubject(String subject) {
        this.subject = subject;
    }
    
    @Column(name="text", length=5000)
    public String getText() {
        return this.text;
    }
    
    public void setText(String text) {
        this.text = text;
    }
    
    @Column(name="status")
    public Status getStatus() {
        return status;
    }
    
    public void setStatus(Status status) {
        this.status = status;
    }
    
    @Column(name="whencreated", length=0)
    @Temporal(value=TemporalType.TIMESTAMP)
    public Date getWhenCreated() {
        return this.whenCreated;
    }
    
    public void setWhenCreated(Date whenCreated) {
        this.whenCreated = whenCreated;
    }
    @OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="message")
    public Set<UserMessage> getMessageReferences() {
        return this.messageReferences;
    }
    
    public void setMessageReferences(Set<UserMessage> messageReferences) {
        this.messageReferences = messageReferences;
    }

    public void addMessageReference(UserMessage um) {
        getMessageReferences().add(um);
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.DEFAULT_STYLE)
            .append("id", id).append("creator", creator).append("subject", subject)
            .append("text", text).append("status", status).append("whenCreated", whenCreated)
            .append("messageReferences", messageReferences).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
            .append(creator).append(subject).append(text).append(status)
            .append(whenCreated).append(messageReferences).toHashCode();
    }

    @Override
    public boolean equals(final Object other) {
        if (this == other)
            return true;
        if (!(other instanceof Message))
            return false;
        Message castOther = (Message) other;
        if (castOther == null)
            return false;
        return new EqualsBuilder()
            .append(creator, castOther.creator).append(subject, castOther.subject)
            .append(text, castOther.text).append(status, castOther.status)
            .append(whenCreated, castOther.whenCreated).append(messageReferences, castOther.messageReferences)
            .isEquals();
    }
    
}