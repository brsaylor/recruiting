/*
 * Copyright (C) 1998-2009, <a href="http://www.ssel.caltech.edu">SSEL</a>,
 * at Caltech, Pasadena, CA 91125.
 *
 * MooreRecruiting is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 * USA.
 *
 */

package edu.caltech.ssel.recruiting.model;

import java.util.ArrayList;
import java.util.List;
import org.appfuse.model.User;
import javax.persistence.*;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.appfuse.model.BaseObject;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

/**
* @author Michael Kolodrubetz, SSEL, Caltech
* @author <a href="mailto:wmyuan@hss.caltech.edu">Walter M. Yuan</a>, SSEL, Caltech
*/
@Entity
@Table(name="survey")
public class Survey extends BaseObject implements Cloneable {
    
    public enum Visibility {
        PUBLIC, PRIVATE;
        public String toString() {
            if(this == PUBLIC)
                return "Public";
            else
                return "Private";
        }
        public String getName() {
            return this.name();
        }
    };
    public enum SurveyType {PROFILE, SURVEY, QUESTIONNAIRE};
    
    private Long id;
    private String name;
    private String introText; // BRS 2012-03-20
    private List<Question> questions;
    private User creator;
    private Visibility visibility;
    private SurveyType type;
    private boolean checkedIRB=false;
    
    /** Creates a new instance of Survey */
    public Survey() {
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.DEFAULT_STYLE)
            .append("id", id).append("name", name).append("questions", questions)
            .append("creator", creator).append("visibility", visibility)
            .append("type", type).toString();
    }
    
    @Id @GeneratedValue(strategy=GenerationType.AUTO)
    public Long getId() {
        return id;
    }
    
    public void setId(Long id) {
        this.id = id;
    }
    
    @Column(unique=true)
    public String getName() {
        return name;
    }
    
    public void setName(String name) {
        this.name = name;
    }

    @Column(columnDefinition="MEDIUMTEXT NULL")
    public String getIntroText() {
        return introText;
    }
    
    public void setIntroText(String introText) {
        this.introText = introText;
    }
    
//    @ManyToMany(fetch=FetchType.EAGER)
    @ManyToMany
    @LazyCollection(LazyCollectionOption.FALSE)
    //These Hibernate "LazyCollection" options work.  A previous version had FetchType.EAGER and the
    //join returned too many (erroneous) records - poneil
    //There are many questions per survey and potentially many surveys that all contain the same question
    public List<Question> getQuestions() {
        return questions;
    }
    
    public void setQuestions(List<Question> questions) {
        this.questions = questions;
    }
    
    public void addQuestion(Question q) {
        questions.add(q);
    }
    
    @ManyToOne  //This is a ManyToOne mapping because there are potentially many Surveys per User
    public User getCreator() {
        return creator;
    }
    
    public void setCreator(User creator) {
        this.creator = creator;
    }
    
    public boolean equals(Object object) {
        if(object==this)
            return true;
        if(!(object instanceof Survey))
            return false;
        
        Survey s = (Survey)object;
        return StringUtils.equals(s.getName(), getName());
    }
    
    public int hashCode() {
        return (int)(id % Integer.MAX_VALUE);
    }
    
    public Visibility getVisibility() {
        return visibility;
    }
    
    public void setVisibility(Visibility visibility) {
        this.visibility = visibility;
    }
    
    public SurveyType getType() {
        return type;
    }
    
    public void setType(SurveyType type) {
        this.type = type;
    }

    @Column(name="checked_irb", nullable=false)
    public boolean isCheckedIRB() {
        return checkedIRB;
    }

    public void setCheckedIRB(boolean checkedIRB) {
        this.checkedIRB = checkedIRB;
    }
    
    public Survey clone() {
        Survey s = new Survey();
        s.name = this.name;
        s.creator = this.creator;
        s.visibility = this.visibility;
        s.type = this.type;
        s.questions = new ArrayList();
        for(int j = 0; j < questions.size(); j++)
            s.questions.add(questions.get(j));
        return s;
    }
    
}
