/*
 * Copyright (C) 1998-2009, <a href="http://www.ssel.caltech.edu">SSEL</a>,
 * at Caltech, Pasadena, CA 91125.
 *
 * MooreRecruiting is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 * USA.
 *
 */

package edu.caltech.ssel.recruiting.model;

import java.util.Date;
import java.util.List;
import java.lang.StringBuilder;
import javax.persistence.*;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.appfuse.model.BaseObject;
import org.appfuse.model.User;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;


/**
* @author Michael Kolodrubetz, SSEL, Caltech
* @author <a href="mailto:wmyuan@hss.caltech.edu">Walter M. Yuan</a>, SSEL, Caltech
*/
@Entity
@Table(name="experimenttype")
public class ExperimentType extends BaseObject {
    
    private Long id;
    private String name;
    private String descr;
    private String instruction;
    private boolean valid;
    private Date regDate;
    private User creator;

    // BRS 2011-02-10
    private List<Experimenter> principalInvestigators;

    // BRS 2011-06-07
    // This text appears above the consent checkbox on the experiment signup page (experimentinfo)
    private String consentText;

    // BRS 2011-06-13
    private Experimenter contactPerson;
    private String accountNumber;

    // BRS 2011-10-28
    private Filter defaultFilter;
    
    /** Creates a new instance of ExperimentType */
    public ExperimentType() {
    }
    
    @Column(unique=true)
    public String getName() {
        return name;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    @Column(columnDefinition="MEDIUMTEXT NULL")
    public String getDescr() {
        return descr;
    }
    
    public void setDescr(String descr) {
        this.descr = descr;
    }
    
    @Column(columnDefinition="MEDIUMTEXT NULL")
    public String getInstruction() {
        return instruction;
    }
    
    public void setInstruction(String instruction) {
        this.instruction = instruction;
    }
    
    public boolean isValid() {
        return valid;
    }
    
    public void setValid(boolean valid) {
        this.valid = valid;
    }
    
    @Temporal(TemporalType.DATE)
    public Date getRegDate() {
        return regDate;
    }
    
    public void setRegDate(Date regDate) {
        this.regDate = regDate;
    }
    
    @ManyToOne  //This is a ManyToOne mapping because there are potentially many ExperimentType's per User
    public User getCreator() {
        return creator;
    }
    
    public void setCreator(User creator) {
        this.creator = creator;
    }
    
    @Id @GeneratedValue(strategy=GenerationType.AUTO)
    public Long getId() {
        return id;
    }
    
    public void setId(Long id) {
        this.id = id;
    }

    // BRS 2011-02-10
    @ManyToMany
    @LazyCollection(LazyCollectionOption.FALSE)
    public List<Experimenter> getPrincipalInvestigators() {
        return this.principalInvestigators;
    }
    public void setPrincipalInvestigators(List<Experimenter> principalInvestigators) {
        this.principalInvestigators = principalInvestigators;
    }

    // BRS 2011-02-11
    // Return a comma-separated list of the names of the principal investigators.
    // If there are none, return an empty string.
    @Transient
    public String getPrincipalInvestigatorsStr() {
        if (this.principalInvestigators == null || this.principalInvestigators.size() == 0) {
            return "";
        } else {
            StringBuilder s = new StringBuilder();
            s.append(this.principalInvestigators.get(0).getFullName());
            for (int i = 1; i < this.principalInvestigators.size(); i++) {
                s.append(", ");
                s.append(this.principalInvestigators.get(i).getFullName());
            }
            return s.toString();
        }
    }

    // BRS 2011-10-28
    @ManyToOne
    public Filter getDefaultFilter() {
        return this.defaultFilter;
    }
    public void setDefaultFilter(Filter defaultFilter) {
        this.defaultFilter = defaultFilter;
    }
    
    @Column(columnDefinition="MEDIUMTEXT NULL")
    public String getConsentText() {
        return consentText;
    }
    
    public void setConsentText(String consentText) {
        this.consentText = consentText;
    }

    // BRS 2011-06-13
    @ManyToOne
    public Experimenter getContactPerson() {
        return contactPerson;
    }
    public void setContactPerson(Experimenter contactPerson) {
        this.contactPerson = contactPerson;
    }

    // BRS 2011-06-13
    @Column(nullable=true)
    public String getAccountNumber() {
        return accountNumber;
    }
    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }
    
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
        .append("id", id.toString())
        .append("name", name)
        .append("descr", descr)
        .append("instruction", instruction)
        .append("valid", valid?"Yes":"No")
        .append("regDate", regDate)
        .append("creator", (creator == null ? null : creator.getFullName())).toString();
    }
    
    public boolean equals(Object object) {
        if(this==object)
            return true;
        if(object == null || !(object instanceof ExperimentType))
            return false;

        ExperimentType e = (ExperimentType)object;
        return StringUtils.equals(e.getName(), getName());
    }
    
    public int hashCode() {
        return (int)(id % Integer.MAX_VALUE);
    }
    
}
