/*
 * Copyright (C) 1998-2009, <a href="http://www.ssel.caltech.edu">SSEL</a>,
 * at Caltech, Pasadena, CA 91125.
 *
 * MooreRecruiting is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 * USA.
 *
 */


package edu.caltech.ssel.recruiting.model;

import java.util.List;
import java.util.GregorianCalendar;

import org.apache.commons.lang.builder.ToStringBuilder;

/**
* @author Michael Kolodrubetz, SSEL, Caltech
* @author <a href="mailto:wmyuan@hss.caltech.edu">Walter M. Yuan</a>, SSEL, Caltech
*/

public class ExperimentCondition extends Condition {
 
    private Experiment experiment;
    private boolean onList;
    
    /** Creates a new instance of ExperimentCondition */
    public ExperimentCondition() {
    }

    public boolean isOnList() {
        return onList;
    }

    public void setOnList(boolean onList) {
        this.onList = onList;
    }

    public Experiment getExperiment() {
        return experiment;
    }

    public void setExperiment(Experiment experiment) {
        this.experiment = experiment;
    }

    public String getConditionType() {
        return "ExperimentCondition";
    }

    // BRS 2011-12-08
    // prList is the list of all ParticipantRecords for the subject being tested for eligibility
    public boolean isEligible(List<ParticipantRecord> prList) {
        boolean history = experiment.getStartDateTime().compareTo(new GregorianCalendar()) < 0;

        // Gets set to true if prList contains a ParticipantRecord for which the following is true:
        // The record's experiment is the condition's experiment
        // AND (
        //   The condition's experiment is in the past and the subject participated
        //   OR
        //   The condition's experiment is in the future and the subject is signed up
        // )
        boolean match = false;

        for(ParticipantRecord p : prList) {
            if (p.getExp().getId() == experiment.getId()) {
                match = (history && p.getStatus() == ParticipantRecord.Status.STATUS_2)
                    || (!history && p.getStatus() == ParticipantRecord.Status.STATUS_3);
                break;
            }
        }

        if(onList)
            return match;
        return !match;
    }

    public String toString() {
        return new ToStringBuilder(this).
                append("id", getId()).
                append("name", getName()).
                append("type", getConditionType()).
                append("experiment_id", getExperiment().getId()).
                append("onList", isOnList()).toString();
    }

    // BRS 2011-12-08
    public String getHumanString() {
        return "Experiment Condition: "
            + (onList ? "Include only " : "Exclude ")
            + "subjects who are signed up for (future) or have participated in experiment #" + experiment.getId() + ".";
    }

    // BRS 2011-11-14
    public ExperimentCondition clone() {
        ExperimentCondition c = new ExperimentCondition();
        c.experiment = experiment;
        c.onList = onList;
        return c;
    }
}
