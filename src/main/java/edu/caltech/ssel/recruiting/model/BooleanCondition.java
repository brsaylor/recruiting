/*
 * Copyright (C) 1998-2009, <a href="http://www.ssel.caltech.edu">SSEL</a>,
 * at Caltech, Pasadena, CA 91125.
 *
 * MooreRecruiting is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 * USA.
 *
 */


package edu.caltech.ssel.recruiting.model;

import org.apache.commons.lang.builder.ToStringBuilder;

/**
* @author Michael Kolodrubetz, SSEL, Caltech
* @author <a href="mailto:wmyuan@hss.caltech.edu">Walter M. Yuan</a>, SSEL, Caltech
*/
public class BooleanCondition extends Condition {
    //Hibernate configuration is in Condition.hbm.xml

    //Have to stores just id's to prevent circular reference
    private Long firstCondition;
    private Long secondCondition;
    private boolean and;
    
    /** Creates a new instance of BooleanCondition */
    public BooleanCondition() {
    }

    public String getConditionType() {
        return "BooleanCondition";
    }

    public String getHumanString() {
        return getConditionType();
    }

    //The only part of the comparison we can do locally is the and/or comparison
    public boolean isEligible(boolean elig1, boolean elig2) {
        if(and) 
            return (elig1 && elig2);
        return (elig1 || elig2);
    }
    
    public boolean isAnd() {
        return and;
    }

    public void setAnd(boolean and) {
        this.and = and;
    }

    public Long getFirstCondition() {
        return firstCondition;
    }

    public void setFirstCondition(Long firstCondition) {
        this.firstCondition = firstCondition;
    }

    public Long getSecondCondition() {
        return secondCondition;
    }

    public void setSecondCondition(Long secondCondition) {
        this.secondCondition = secondCondition;
    }

    public String toString() {
        return new ToStringBuilder(this).
                append("id", getId()).
                append("name", getName()).
                append("type", getConditionType()).
                append("first_id", getFirstCondition()).
                append("second_id", getSecondCondition()).
                append("op", isAnd() ? "AND" : "OR").toString();
    }

    // BRS 2011-11-14
    public BooleanCondition clone() {
        BooleanCondition c = new BooleanCondition();
        c.firstCondition = firstCondition;
        c.secondCondition = secondCondition;
        c.and = and;
        return c;
    }
}
