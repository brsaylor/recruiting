/*
 * Copyright (C) 1998-2009, <a href="http://www.ssel.caltech.edu">SSEL</a>,
 * at Caltech, Pasadena, CA 91125.
 *
 * MooreRecruiting is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 * USA.
 *
 */

package edu.caltech.ssel.recruiting.model;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.JoinColumn;
import javax.persistence.Column;
import javax.persistence.Table;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.appfuse.model.BaseObject;

/**
* @author Michael Kolodrubetz, SSEL, Caltech
* @author <a href="mailto:wmyuan@hss.caltech.edu">Walter M. Yuan</a>, SSEL, Caltech
*/
@Entity
@Table(name="participantrecord")
public class ParticipantRecord extends BaseObject {
    
    /*
     * STATUS_1 = 0 = Show-up
     * STATUS_2 = 1 = Participated
     * STATUS_3 = 2 = Sign-up
     */
    public enum Status {
        STATUS_1, STATUS_2, STATUS_3;
        public String toString() {
            switch(this) {
                case STATUS_1:
                    return "Show-up";
                case STATUS_2:
                    return "Participated";
                case STATUS_3:
                    return "Sign-up";
            }
            return "";
        }
        //For web stuff
        public String getName() {
            return this.name();
        }
    };
    public enum PaymentMethod {
        PAYGATE, MANUAL;
        public String toString() {
            switch(this) {
                case PAYGATE:
                    return "PayGate";
                case MANUAL:
                    return "Manual";
            }
            return "";
        }
        //For web stuff
        public String getName() {
            return this.name();
        }
    };
    
    private Long id;
    private Experiment exp;
    private Subject subject;
    private Integer playerNum;
    private Float payoff;
    private boolean participated;
    private boolean eligible;
    private Status status;
    private PaymentMethod paymentMethod;
    private Payment payment;

    // BRS 2011-06-07
    // This indicates that the subject consented to the experiment.
    private Boolean consent;
    
    /** Creates a new instance of ParticipantRecord */
    public ParticipantRecord() {
        // BRS: manual payment by default
        setPaymentMethod(PaymentMethod.MANUAL);

        // BRS: set no consent by default.
        setConsent(false);
    }
    
    @ManyToOne
    public Experiment getExp() {
        return exp;
    }
    
    public void setExp(Experiment exp) {
        this.exp = exp;
    }
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="subject_id", nullable=false)
    public Subject getSubject() {
        return subject;
    }
    
    public void setSubject(Subject subject) {
        this.subject = subject;
    }
    
    public Status getStatus() {
        return status;
    }
    
    public void setStatus(Status status) {
        this.status = status;
    }
    
    public PaymentMethod getPaymentMethod() {
        return paymentMethod;
    }
    
    public void setPaymentMethod(PaymentMethod paymentMethod) {
        this.paymentMethod = paymentMethod;
    }
    
    @Id @GeneratedValue(strategy=GenerationType.AUTO)
    public Long getId() {
        return id;
    }
    
    public void setId(Long id) {
        this.id = id;
    }
    
    public boolean isParticipated() {
        return participated;
    }
    
    public void setParticipated(boolean participated) {
        this.participated = participated;
    }
    
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
        .append("id", getId())
        .append("experiment_id", getExp().getId())
        .append("paymentMethod", getPaymentMethod())
        .append("payment", getPayment())
        .append("payoff", getPayoff())
        .append("playerNum", getPlayerNum())
        .append("status", getStatus())
        .append("participated", isParticipated())
        .append("eligible", isEligible())
        .append("subject_id", getSubject().getId()).toString();
    }
    
    public boolean equals(Object o) {
        if(o == this)
            return true;
        if(o == null || !(o instanceof ParticipantRecord))
            return false;
        
        return ((ParticipantRecord)o).getId().equals(getId());
    }
    
    public int hashCode() {
        return (int)(getId() % Integer.MAX_VALUE);
    }

    public Integer getPlayerNum() {
        return playerNum;
    }

    public void setPlayerNum(Integer playerNum) {
        this.playerNum = playerNum;
    }

    public Float getPayoff() {
        return payoff;
    }

    public void setPayoff(Float payoff) {
        this.payoff = payoff;
    }

    @OneToOne
    @JoinColumn(name="pmt_id", unique=true)
    public Payment getPayment() {
        return payment;
    }

    public void setPayment(Payment payment) {
        this.payment = payment;
    }

    public boolean isEligible() {
        return eligible;
    }

    public void setEligible(boolean eligible) {
        this.eligible = eligible;
    }

    @Column(nullable=true)
    public Boolean getConsent() {
        return consent;
    }

    public void setConsent(Boolean consent) {
        this.consent = consent;
    }

}
