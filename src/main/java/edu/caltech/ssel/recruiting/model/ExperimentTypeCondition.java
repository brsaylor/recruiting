/*
 * Copyright (C) 1998-2009, <a href="http://www.ssel.caltech.edu">SSEL</a>,
 * at Caltech, Pasadena, CA 91125.
 *
 * MooreRecruiting is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 * USA.
 *
 */


package edu.caltech.ssel.recruiting.model;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.ArrayList;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.StringUtils;

/**
* @author Michael Kolodrubetz, SSEL, Caltech
* @author <a href="mailto:wmyuan@hss.caltech.edu">Walter M. Yuan</a>, SSEL, Caltech
*/
//Hibernate configuration is in Condition.hbm.xml
public class ExperimentTypeCondition extends Condition {

    private List<ExperimentType> types;
    private boolean history; //true if referring to past
    private boolean onList;
    
    /** Creates a new instance of ExperimentTypeCondition */
    public ExperimentTypeCondition() {
    }

    /*
     * Current setup: Eligibility 
     */
    public boolean isEligible(List<ParticipantRecord> prList) {
        int cnt = 0;

        for(int j = 0; j < prList.size(); j++) {
            //Only look at future experiments if !history
            //Check to see if experiment date is greater than participant record date
            //Check to see if experiment time is greater than participant record time
            //System.out.println("isParticipated: "+prList.get(j).isParticipated()+", this.history: "+this.history);
            //System.out.println("onList: "+onList);
            if(prList.get(j).isParticipated() == this.history) {
                //System.out.println("cmp: "+prList.get(j).getExp().getStartTime()+", "+(new GregorianCalendar()).getTime());
                //System.out.println("cmp: "+prList.get(j).getExp().getStartTime().compareTo((new GregorianCalendar()).getTime()));
                //System.out.println("cmpDate: "+prList.get(j).getExp().getExpDate().compareTo(new Date()));
                //System.out.println("Dates: "+prList.get(j).getExp().getExpDate()+", "+new Date());
                //If only looking for future experiments and current participant record is in the past, skip iteration

                //if(!this.history && (prList.get(j).getExp().getExpDate().compareTo(new Date()) < 0) &&
                //        (prList.get(j).getExp().getStartTime().compareTo((new GregorianCalendar()).getTime()) < 0 ) )
                // BRS 2011-10-27 replaced above with below
                if (!this.history && prList.get(j).getExp().getStartDateTime().compareTo(new GregorianCalendar()) < 0)

                    continue;

                if(types.contains(prList.get(j).getExp().getType()))
                    cnt++;
            }
        }

        if(onList)
            return (cnt > 0);
        return cnt == 0;
    }
    
    public boolean isHistory() {
        return history;
    }

    public void setHistory(boolean history) {
        this.history = history;
    }

    public boolean isOnList() {
        return onList;
    }

    public void setOnList(boolean onList) {
        this.onList = onList;
    }

    public List<ExperimentType> getTypes() {
        return types;
    }

    public void setTypes(List<ExperimentType> types) {
        this.types = types;
    }

    public String getConditionType() {
        return "ExperimentTypeCondition";
    }

    public String getHumanString() {
        StringBuilder s = new StringBuilder();
        s.append("Experiment Type Condition: ");
        if (onList) {
            s.append("Include only subjects who ");
        } else {
            s.append("Exclude subjects who ");
        }
        if (history) {
            s.append("have participated in ");
        } else {
            s.append("are signed up for future ");
        }
        if (types.size() == 1) {
            s.append("experiments of type ");
            s.append(types.get(0).getName());
        } else {
            s.append("experiments of the following types: ");
            ArrayList typeNames = new ArrayList();
            for (ExperimentType type : types) {
                typeNames.add(type.getName());
            }
            s.append(StringUtils.join(typeNames, ", "));
        }
        s.append(".");

        return s.toString();
    }

    public String toString() {
        return new ToStringBuilder(this).
                append("id", getId()).
                append("name", getName()).
                append("type", getConditionType()).
                append("types", getTypes()).
                append("history", isHistory()).
                append("onList", isOnList()).toString();
    }

    public ExperimentTypeCondition clone() {
        ExperimentTypeCondition ETC = new ExperimentTypeCondition();
        ETC.history = history;
        ETC.onList = onList;
        ETC.types = new ArrayList();
        for(int j = 0; j < types.size(); j++)
            ETC.types.add(types.get(j));
        return ETC;
    }
    
}
