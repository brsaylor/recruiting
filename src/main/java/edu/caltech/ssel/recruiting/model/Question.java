/*
 * Copyright (C) 1998-2009, <a href="http://www.ssel.caltech.edu">SSEL</a>,
 * at Caltech, Pasadena, CA 91125.
 *
 * MooreRecruiting is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 * USA.
 *
 */


package edu.caltech.ssel.recruiting.model;

import java.util.List;
import javax.persistence.*;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.appfuse.model.BaseObject;
import com.thoughtworks.xstream.XStream;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
* @author Michael Kolodrubetz, SSEL, Caltech
* @author <a href="mailto:wmyuan@hss.caltech.edu">Walter M. Yuan</a>, SSEL, Caltech
*/
@Entity
@Table(name="question")
public class Question extends BaseObject implements Cloneable {
    
    Log log = LogFactory.getLog(Question.class);
    
    public static enum QStyle {
        CHECK_BOX, RADIO, DROP_LIST;
        
        public String toString() {
            switch(this) {
                case CHECK_BOX:
                    return "Checkbox";
                case RADIO:
                    return "Radio button";
                case DROP_LIST:
                    return "Drop-down list";
                default:
                    return null;
                    
            }
        }
        
        //Required for accessing proper name (rather than toString()) from jsp pages
        public String getName() {
            return this.name();
        }
    };
    
    private Long id;
    private String label;
    private String question;
    private QStyle style;
    private String options;
    
    /** Creates a new instance of Question */
    public Question() {
    }
    
    @Transient
    public String getStyleString() {
        switch(style) {
            case CHECK_BOX:
                return "CHECK_BOX";
            case RADIO:
                return "RADIO";
            case DROP_LIST:
                return "DROP_LIST";
            default:
                return null;
        }
    }
    
    @Id @GeneratedValue(strategy=GenerationType.AUTO)
    public Long getId() {
        return id;
    }
    
    public void setId(Long id) {
        this.id = id;
    }
    
    @Column(unique=true)
    public String getLabel() {
        return label;
    }
    
    public void setLabel(String label) {
        this.label = label;
    }
    
    public String getQuestion() {
        return question;
    }
    
    public void setQuestion(String question) {
        this.question = question;
    }
    
    public QStyle getStyle() {
        return style;
    }
    
    public void setStyle(QStyle style) {
        this.style = style;
    }
    
    /*@CollectionOfElements //Cannot set fetch type to eager, or it gives error: "cannot simultaneously fetch multiple bags"
    public List<String> getOptions() {
        return options;
    }
    
    public void setOptions(List<String> options) {
        this.options = options;
    }*/
    
    @Column(columnDefinition="MEDIUMTEXT NULL")
    public String getOptions() {
        return options;
    }
    
    public void setOptions(String s) {
        options = s;
    }

    @Transient
    public List<String> getOptionList() {
        XStream xstream = new XStream();
        xstream.alias("list", List.class);
        return (List<String>) xstream.fromXML(options);
    }
    
    public void setOptionList(List<String> lst) {
        XStream xstream = new XStream();
        xstream.alias("list", List.class);
        options = xstream.toXML(lst);
    }

    public String getNameOf(QStyle t) {
        switch(t) {
            case CHECK_BOX:
                return "Checkbox";
            case RADIO:
                return "Radio button";
            case DROP_LIST:
                return "Drop-down list";
        }
        return null;//Should never reach this point
    }
    
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
        .append("id", id.toString())
        .append("label", label)
        .append("question", question)
        .append("options", options)
        .append("type", getNameOf(style)).toString();
    }
    
    public boolean equals(Object object) {
        if(this==object)
            return true;
        if(!(object instanceof Question))
            return false;
        
        Question q = (Question)object;
        
        //Check equality by "unique" fields (not id, in case of null id)
        return StringUtils.equals(q.getLabel(), this.getLabel());
    }
    
    public int hashCode() {
        return (int)(id % Integer.MAX_VALUE);
    }
    
    public Question clone() {
        Question q = new Question();
        q.id = this.id;
        q.label = this.label;
        q.question = this.question;
        q.style = this.style;
        q.options = this.options;
        return q;
    }
    
}
