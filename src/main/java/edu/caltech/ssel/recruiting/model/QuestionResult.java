/*
 * Copyright (C) 1998-2009, <a href="http://www.ssel.caltech.edu">SSEL</a>,
 * at Caltech, Pasadena, CA 91125.
 *
 * MooreRecruiting is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 * USA.
 *
 */


package edu.caltech.ssel.recruiting.model;

import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.appfuse.model.BaseObject;
import org.appfuse.model.User;

/**
* @author Michael Kolodrubetz, SSEL, Caltech
* @author <a href="mailto:wmyuan@hss.caltech.edu">Walter M. Yuan</a>, SSEL, Caltech
*/
@Entity
@Table(name="questionresult")
//Cannot create class with generic parent class because Hibernate does not support generic type mapping
public class QuestionResult extends BaseObject {
    
    private Long id;
    private Experiment surveyParent;
    private User surveyTaker;
    private Question question;
    private int answerIdx;
    private Date takenDate;
    
    /** Creates a new instance of QuestionResult */
    public QuestionResult() {
    }
    
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
        .append("id", id.toString())
        .append("surveyParent", surveyParent.toString())
        .append("surveyTaker", surveyTaker.toString())
        .append("question", question.toString())
        .append("answerIdx", Integer.toString(answerIdx))
        .append("takenDate", takenDate.toString())
        .toString();
                
    }
    
    public boolean equals(Object o) {
        if(o == this)
            return true;
        if(!(o instanceof QuestionResult))
            return false;
        return ((QuestionResult)o).id.equals(this.id);
    }
    
    public int hashCode() {
        return (int)(id % Integer.MAX_VALUE);
    }
    
    @ManyToOne
    public Experiment getSurveyParent() {
        return surveyParent;
    }
    
    public void setSurveyParent(Experiment surveyParent) {
        this.surveyParent = surveyParent;
    }
    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="surveytaker_id", nullable=false)
    public User getSurveyTaker() {
        return surveyTaker;
    }
    
    public void setSurveyTaker(User surveyTaker) {
        this.surveyTaker = surveyTaker;
    }
    
    @ManyToOne
    public Question getQuestion() {
        return question;
    }
    
    public void setQuestion(Question question) {
        this.question = question;
    }
    
    public int getAnswerIdx() {
        return answerIdx;
    }
    
    public void setAnswerIdx(int answerIdx) {
        this.answerIdx = answerIdx;
    }
    
    @Temporal(TemporalType.TIMESTAMP)
    public Date getTakenDate() {
        return takenDate;
    }
    
    public void setTakenDate(Date takenDate) {
        this.takenDate = takenDate;
    }

    @Id @GeneratedValue(strategy=GenerationType.AUTO)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    
}
