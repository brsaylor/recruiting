/*
 * Copyright (C) 1998-2009, <a href="http://www.ssel.caltech.edu">SSEL</a>,
 * at Caltech, Pasadena, CA 91125.
 *
 * MooreRecruiting is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 * USA.
 *
 */


package edu.caltech.ssel.recruiting.model;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.ArrayList;
import org.apache.commons.lang.builder.ToStringBuilder;

import edu.caltech.ssel.recruiting.model.ParticipantRecord.Status;

/**
* @author Ben Saylor
* based on ExperimentTypeCondition
*/
//Hibernate configuration is in Condition.hbm.xml
public class NoShowCondition extends Condition {

    private int maxNoShows;
    
    public NoShowCondition() {
    }

    /*
     * Current setup: Eligibility 
     */
    public boolean isEligible(List<ParticipantRecord> prList) {
        int noShows = 0;

        // For each ParticipantRecord (in the given list, which is all the records for a particular subject)
        for(int j = 0; j < prList.size(); j++) {

            // Only look at records for which the experiment has passed (taken from ExperimentTypeCondition)
            if ((prList.get(j).getExp().getExpDate().compareTo(new Date()) < 0) &&
                    (prList.get(j).getExp().getStartTime().compareTo((new GregorianCalendar()).getTime()) < 0 ) ) {

                // This participant record is for a past experiment.

                // If the status is STATUS_3 ("Sign-up") (see ParticipantRecord.java), the subject was a no-show.
                if (prList.get(j).getStatus() == Status.STATUS_3) {
                    // No-show.
                    noShows++;
                }
            }
        }

        System.out.println("NoShowCondition:isEligible(): Subject has " + noShows + " no-shows.");

        return noShows <= maxNoShows;
    }
    
    public int getMaxNoShows() {
        return maxNoShows;
    }

    public void setMaxNoShows(int maxNoShows) {
        this.maxNoShows = maxNoShows;
    }

    public String getConditionType() {
        return "NoShowCondition";
    }

    public String getHumanString() {
        return "No-Show Condition: Exclude subjects with more than " + maxNoShows + " no-show" + (maxNoShows == 1 ? "." : "s.");
    }

    public String toString() {
        return new ToStringBuilder(this).
                append("id", getId()).
                append("name", getName()).
                append("type", getConditionType()).
                append("maxNoShows", getMaxNoShows()).toString();
    }

    public NoShowCondition clone() {
        NoShowCondition ETC = new NoShowCondition();
        ETC.maxNoShows = maxNoShows;
        return ETC;
    }
    
}
