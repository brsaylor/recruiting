/*
 * Copyright (C) 1998-2009, <a href="http://www.ssel.caltech.edu">SSEL</a>,
 * at Caltech, Pasadena, CA 91125.
 *
 * MooreRecruiting is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 * USA.
 *
 */


package edu.caltech.ssel.recruiting.model;

import java.util.Date;
import javax.persistence.*;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.appfuse.model.BaseObject;

/**
* @author Michael Kolodrubetz, SSEL, Caltech
* @author <a href="mailto:wmyuan@hss.caltech.edu">Walter M. Yuan</a>, SSEL, Caltech
*/
@Entity
@Table(name="subjectpool")
public class SubjectPool extends BaseObject {

    private Long id;
    private String name;
    private String descr;
    private String consentInfo;
    private boolean valid;
    private Date regDate;

    // BRS 2013-05-21
    // Subjects with matching signupCode will be added to this pool
    private String signupCode;
    
    @Id @GeneratedValue(strategy=GenerationType.AUTO)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(unique=true)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescr() {
        return descr;
    }

    public void setDescr(String descr) {
        this.descr = descr;
    }

    public String getConsentInfo() {
        return consentInfo;
    }

    public void setConsentInfo(String consentInfo) {
        this.consentInfo = consentInfo;
    }

    public boolean isValid() {
        return valid;
    }

    public void setValid(boolean valid) {
        this.valid = valid;
    }

    @Temporal(TemporalType.DATE)
    public Date getRegDate() {
        return regDate;
    }

    public void setRegDate(Date regDate) {
        this.regDate = regDate;
    }
    
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
        .append("id", getId())
        .append("name", getName())
        .append("consentInfo", getConsentInfo())
        .append("description", getDescr())
        .append("regDate", getRegDate())
        .append("valid", isValid())
        .toString();
    }
    
    public boolean equals(Object o) {
        if(o == null || !(o instanceof SubjectPool))
            return false;
        
        return StringUtils.equals(((SubjectPool)o).getName(), getName());
    }
    
    public int hashCode() {
        return (int)(id % Integer.MAX_VALUE);
    }
    
    /**
     * Get signupCode.
     *
     * @return signupCode as String.
     */
    public String getSignupCode()
    {
        return signupCode;
    }
    
    /**
     * Set signupCode.
     *
     * @param signupCode the value to set.
     */
    public void setSignupCode(String signupCode)
    {
        this.signupCode = signupCode;
    }
}
