/*
 * Copyright (C) 1998-2009, <a href="http://www.ssel.caltech.edu">SSEL</a>,
 * at Caltech, Pasadena, CA 91125.
 *
 * MooreRecruiting is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 * USA.
 *
 */

// change

package edu.caltech.ssel.recruiting.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.hibernate.annotations.*;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.List;

import org.appfuse.model.User;

/**
* @author Michael Kolodrubetz, SSEL, Caltech
* @author <a href="mailto:wmyuan@hss.caltech.edu">Walter M. Yuan</a>, SSEL, Caltech
*/
@Entity
@DiscriminatorValue("Subject")
public class Subject extends User {

    private List<SubjectPool> pools;
    private String schoolId;
    private Date birthday;
    private Set<ParticipantRecord> participantRecords = new HashSet<ParticipantRecord>(0);
    /*private boolean male;
    private String major;
    private String ethnicity;*/

    // BRS - added fields
    private Boolean subscribed;
    private Integer idType; // indicates which type of ID is contained in the schoolID field (1 for UA ID, 2 for SSN, 3 for ITIN)
	private Date driversLicenseExpirationDate;
    private String addressLine1;
    private String addressLine2;
    private String city;
    private String state;
    private Integer zip;
    private Boolean usCitizen;
    private String citizenCountry;
    private String birthCountry;
    private String priorCountry;
    private Date priorCountryStartDate;
    private Date priorCountryEndDate;
    private Date firstUsEntryDate;
    private Date currentUsEntryDate;
    private Boolean i94ExpirationDS;
    private Date i94ExpirationDate;
    private String passportNumber;
    private String passportCountryOfIssue;
    private Date passportExpirationDate;
    private String visaType;
	private String visaTypeOther;
	private Date visaExpirationDate;
	private String jvisaCategory;  // hbm2ddl inexplicably capitalized the J when adding the field to the DB (causing a Spring error), until I made the V lower-case.
	private Boolean visaPriorTo7Years;
    private List<Date> arrivalDates;
    private List<Date> departureDates;
    private List<String> visaTypes;
    private List<String> purposes;
	private Boolean passportOnFile;

    // For SMS reminders
    private String mobileNumber;
    private String smsGateway;
    private Boolean receiveSMSReminders;

    // BRS 2012-02-01
    private Date registrationDate; // Date/time this account was created
    private Boolean signedUp; // True if the signup form was used

    // BRS 2013-05-20
    // Subject will be added to subject pools with a matching signupCode
    private String signupCode;

    @Column(nullable=true)
    public Boolean getSubscribed() {

        // Avoid NullPointerExceptions when checking for subscribed status by
        // cheating a little here.
        if (this.subscribed == null)
            return true;

        return this.subscribed;
    }
    public void setSubscribed(Boolean subscribed) {
        this.subscribed = subscribed;
    }

    @Column(nullable=true)
    public Integer getIdType() {
        return this.idType;
    }
    public void setIdType(Integer idType) {
        this.idType = idType;
    }

    @Column(nullable=true)
    public Date getDriversLicenseExpirationDate() {
        return this.driversLicenseExpirationDate;
    }
    public void setDriversLicenseExpirationDate(Date driversLicenseExpirationDate) {
        this.driversLicenseExpirationDate = driversLicenseExpirationDate;
    }

    @Column(nullable=true)
    public String getAddressLine1() {
        return this.addressLine1;
    }
    public void setAddressLine1(String addressLine1) {
        this.addressLine1 = addressLine1;
    }

    @Column(nullable=true)
    public String getAddressLine2() {
        return this.addressLine2;
    }
    public void setAddressLine2(String addressLine2) {
        this.addressLine2 = addressLine2;
    }

    @Column(nullable=true)
    public String getCity() {
        return this.city;
    }
    public void setCity(String city) {
        this.city = city;
    }

    @Column(nullable=true)
    public String getState() {
        return this.state;
    }
    public void setState(String state) {
        this.state = state;
    }

    @Column(nullable=true)
    public Integer getZip() {
        return this.zip;
    }
    public void setZip(Integer zip) {
        this.zip = zip;
    }

    @Column(nullable=true)
    public Boolean getUsCitizen() {
        return this.usCitizen;
    }
    public void setUsCitizen(Boolean usCitizen) {
        this.usCitizen = usCitizen;
    }

    @Column(nullable=true)
    public String getCitizenCountry() {
        return this.citizenCountry;
    }
    public void setCitizenCountry(String citizenCountry) {
        this.citizenCountry = citizenCountry;
    }

    @Column(nullable=true)
    public String getBirthCountry() {
        return this.birthCountry;
    }
    public void setBirthCountry(String birthCountry) {
        this.birthCountry = birthCountry;
    }

    @Column(nullable=true)
    public String getPriorCountry() {
        return this.priorCountry;
    }
    public void setPriorCountry(String priorCountry) {
        this.priorCountry = priorCountry;
    }

    @Column(nullable=true)
    @Temporal(value=TemporalType.DATE)
    public Date getPriorCountryStartDate() {
        return priorCountryStartDate;
    }
    public void setPriorCountryStartDate(Date priorCountryStartDate) {
        this.priorCountryStartDate = priorCountryStartDate;
    }

    @Column(nullable=true)
    @Temporal(value=TemporalType.DATE)
    public Date getPriorCountryEndDate() {
        return priorCountryEndDate;
    }
    public void setPriorCountryEndDate(Date priorCountryEndDate) {
        this.priorCountryEndDate = priorCountryEndDate;
    }

    @Column(nullable=true)
    @Temporal(value=TemporalType.DATE)
    public Date getFirstUsEntryDate() {
        return firstUsEntryDate;
    }
    public void setFirstUsEntryDate(Date firstUsEntryDate) {
        this.firstUsEntryDate = firstUsEntryDate;
    }

    @Column(nullable=true)
    @Temporal(value=TemporalType.DATE)
    public Date getCurrentUsEntryDate() {
        return currentUsEntryDate;
    }
    public void setCurrentUsEntryDate(Date currentUsEntryDate) {
        this.currentUsEntryDate = currentUsEntryDate;
    }

    @Column(nullable=true)
    public Boolean getI94ExpirationDS() {
        return this.i94ExpirationDS;
    }
    public void setI94ExpirationDS(Boolean i94ExpirationDS) {
        this.i94ExpirationDS = i94ExpirationDS;
    }

    @Column(nullable=true)
    @Temporal(value=TemporalType.DATE)
    public Date getI94ExpirationDate() {
        return i94ExpirationDate;
    }
    public void setI94ExpirationDate(Date i94ExpirationDate) {
        this.i94ExpirationDate = i94ExpirationDate;
    }

    @Column(nullable=true)
    public String getPassportNumber() {
        return this.passportNumber;
    }
    public void setPassportNumber(String passportNumber) {
        this.passportNumber = passportNumber;
    }

    @Column(nullable=true)
    public String getPassportCountryOfIssue() {
        return this.passportCountryOfIssue;
    }
    public void setPassportCountryOfIssue(String passportCountryOfIssue) {
        this.passportCountryOfIssue = passportCountryOfIssue;
    }

    @Column(nullable=true)
    @Temporal(value=TemporalType.DATE)
    public Date getPassportExpirationDate() {
        return passportExpirationDate;
    }
    public void setPassportExpirationDate(Date passportExpirationDate) {
        this.passportExpirationDate = passportExpirationDate;
    }

    @Column(nullable=true)
    public String getVisaType() {
        return this.visaType;
    }
    public void setVisaType(String visaType) {
        this.visaType = visaType;
    }

    @Column(nullable=true)
    public String getVisaTypeOther() {
        return this.visaTypeOther;
    }
    public void setVisaTypeOther(String visaTypeOther) {
        this.visaTypeOther = visaTypeOther;
    }

    @Column(nullable=true)
    @Temporal(value=TemporalType.DATE)
    public Date getVisaExpirationDate() {
        return visaExpirationDate;
    }
    public void setVisaExpirationDate(Date visaExpirationDate) {
        this.visaExpirationDate = visaExpirationDate;
    }

    @Column(nullable=true)
    public String getJvisaCategory() {
        return this.jvisaCategory;
    }
    public void setJvisaCategory(String jvisaCategory) {
        this.jvisaCategory = jvisaCategory;
    }

    @Column(nullable=true)
    public Boolean getVisaPriorTo7Years() {
        return this.visaPriorTo7Years;
    }
    public void setVisaPriorTo7Years(Boolean visaPriorTo7Years) {
        this.visaPriorTo7Years = visaPriorTo7Years;
    }

    @CollectionOfElements
    public List<Date> getArrivalDates() {
        return this.arrivalDates;
    }
    public void setArrivalDates(List<Date> arrivalDates) {
        this.arrivalDates = arrivalDates;
    }

    @CollectionOfElements
    public List<Date> getDepartureDates() {
        return this.departureDates;
    }
    public void setDepartureDates(List<Date> departureDates) {
        this.departureDates = departureDates;
    }

    @CollectionOfElements
    public List<String> getVisaTypes() {
        return this.visaTypes;
    }
    public void setVisaTypes(List<String> visaTypes) {
        this.visaTypes = visaTypes;
    }

    @CollectionOfElements
    public List<String> getPurposes() {
        return this.purposes;
    }
    public void setPurposes(List<String> purposes) {
        this.purposes = purposes;
    }

    @Column(nullable=true)
    public Boolean getPassportOnFile() {
        return this.passportOnFile;
    }
    public void setPassportOnFile(Boolean passportOnFile) {
        this.passportOnFile = passportOnFile;
    }

    @Column(nullable=true)
    public String getMobileNumber() {
        return this.mobileNumber;
    }
    public void setMobileNumber(String mobileNumber) {
        if (mobileNumber == null) {
            this.mobileNumber = null;
        } else {
            // Remove all non-digit characters from the number
            this.mobileNumber = mobileNumber.replaceAll("\\D", "");
        }
    }

    @Column(nullable=true)
    public String getSmsGateway() {
        return this.smsGateway;
    }
    public void setSmsGateway(String smsGateway) {
        this.smsGateway = smsGateway;
    }

    @Column(nullable=true)
    public Boolean getReceiveSMSReminders() {
        return this.receiveSMSReminders;
    }
    public void setReceiveSMSReminders(Boolean receiveSMSReminders) {
        this.receiveSMSReminders = receiveSMSReminders;
    }
    
    /** Creates a new instance of Subject */
    public Subject() {
    }
    
    /**
    public Subject(User u) {
        setAccountExpired(u.isAccountExpired());
        setAccountLocked(u.isAccountLocked());
        setAddress(u.getAddress());
        setConfirmPassword(u.getConfirmPassword());
        setCredentialsExpired(u.isCredentialsExpired());
        setEmail(u.getEmail());
        setEnabled(u.isEnabled());
        setFirstName(u.getFirstName());
        setLastName(u.getLastName());
        setPassword(u.getPassword());
        setPasswordHint(u.getPasswordHint());
        setPhoneNumber(u.getPhoneNumber());
        setRoles(u.getRoles());
        setUsername(u.getUsername());
        setVersion(u.getVersion());
        setWebsite(u.getWebsite());
        setMessages(u.getMessages());
        setMessageReferences(u.getMessageReferences());
    }
     */

    @ManyToMany(fetch=FetchType.EAGER)
    @JoinTable(
        name="subject_subjectpool",
        joinColumns=
            @JoinColumn(name="subject_id", referencedColumnName="id"),
        inverseJoinColumns=
            @JoinColumn(name="subjectpool_id", referencedColumnName="id")
    )
    public List<SubjectPool> getPools() {
        return pools;
    }

    public void setPools(List<SubjectPool> pools) {
        this.pools = pools;
    }

    @Column(name="schoolid", length=255)
    public String getSchoolId() {
        return schoolId;
    }

    public void setSchoolId(String schoolId) {

        // Remove all non-digit characters from the ID
        schoolId = schoolId.replaceAll("\\D", "");

        this.schoolId = schoolId;
    }

    @Column(name="birthday", length=0)
    @Temporal(value=TemporalType.TIMESTAMP)
    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    /*public boolean isMale() {
        return male;
    }

    public void setMale(boolean male) {
        this.male = male;
    }

    public String getMajor() {
        return major;
    }

    public void setMajor(String major) {
        this.major = major;
    }

    public String getEthnicity() {
        return ethnicity;
    }

    public void setEthnicity(String ethnicity) {
        this.ethnicity = ethnicity;
    }*/
    
    @OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="subject")
    public Set<ParticipantRecord> getParticipantRecords() {
        return this.participantRecords;
    }

    public void setParticipantRecords(Set<ParticipantRecord> participantRecords) {
        this.participantRecords = participantRecords;
    }

    // BRS 2012-02-01
    @Column(nullable=true)
    public Date getRegistrationDate() {
        return registrationDate;
    }
    public void setRegistrationDate(Date registrationDate) {
        this.registrationDate = registrationDate;
    }

    // BRS 2012-02-01
    @Column(nullable=true)
    public Boolean getSignedUp() {
        return signedUp;
    }
    public void setSignedUp(Boolean signedUp) {
        this.signedUp = signedUp;
    }


    @Override
    public int hashCode() {
        return new HashCodeBuilder()
            .append(pools).append(schoolId).append(birthday).toHashCode();
    }

    @Override
    public boolean equals(final Object other) {
        if (this == other)
            return true;
        if (!(other instanceof Subject))
            return false;
        Subject castOther = (Subject) other;
        return new EqualsBuilder()
            .append(pools, castOther.pools).append(schoolId, castOther.schoolId)
            .append(birthday, castOther.birthday).isEquals();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.DEFAULT_STYLE)
            .append("pools", pools).append("schoolId", schoolId).append("birthday", birthday)
            .toString();
    }

    
    /**
     * Get signupCode.
     *
     * @return signupCode as String.
     */
    @Column(nullable=true)
    public String getSignupCode()
    {
        return signupCode;
    }
    
    /**
     * Set signupCode.
     *
     * @param signupCode the value to set.
     */
    public void setSignupCode(String signupCode)
    {
        this.signupCode = signupCode;
    }
}
