/*
 * Copyright (C) 1998-2009, <a href="http://www.ssel.caltech.edu">SSEL</a>,
 * at Caltech, Pasadena, CA 91125.
 *
 * MooreRecruiting is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 * USA.
 *
 */


package edu.caltech.ssel.recruiting.model;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import org.appfuse.model.User;

/**
* @author Michael Kolodrubetz, SSEL, Caltech
* @author <a href="mailto:wmyuan@hss.caltech.edu">Walter M. Yuan</a>, SSEL, Caltech
*/
@Entity
@DiscriminatorValue("Experimenter")
public class Experimenter extends User {

    //Enumerated type for title - will need to fill in options later
    public static enum Title {MR, DR, MS;
        public String toString() {
            switch(this) {
                case MR:
                    return "Mr.";
                case MS:
                    return "Ms.";
                case DR:
                    return "Dr.";
            }
            return "Other";
        }
        
        //For web part
        public String getName() {
            return this.name();
        }
    };
    
    private String affiliation;
    private Title title;

    private String department; // BRS 2011-06-13
    
    /** Creates a new instance of Experimenter */
    public Experimenter() {
    }

    @Column(name="affiliation", length=255)
    public String getAffiliation() {
        return affiliation;
    }

    public void setAffiliation(String affiliation) {
        this.affiliation = affiliation;
    }

    @Column(name="title")
    public Title getTitle() {
        return title;
    }

    public void setTitle(Title title) {
        this.title = title;
    }

    @Column(name="department", length=255)
    public String getDepartment() {
        return department;
    }
    public void setDepartment(String department) {
        this.department = department;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
            .append(affiliation).append(title).toHashCode();
    }

    @Override
    public boolean equals(final Object other) {
        if (this == other)
            return true;
        if (!(other instanceof Experimenter))
            return false;
        Experimenter castOther = (Experimenter) other;
        return new EqualsBuilder()
            .append(affiliation, castOther.affiliation).append(title, castOther.title)
            .isEquals();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.DEFAULT_STYLE)
            .append("affiliation", affiliation).append("title", title)
            .toString();
    }
    
}
