/*
 * Copyright (C) 1998-2009, <a href="http://www.ssel.caltech.edu">SSEL</a>,
 * at Caltech, Pasadena, CA 91125.
 *
 * MooreRecruiting is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 * USA.
 *
 */

package edu.caltech.ssel.recruiting.model;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import javax.persistence.*;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.appfuse.model.BaseObject;
import org.appfuse.model.User;

/**
* @author Michael Kolodrubetz, SSEL, Caltech
* @author <a href="mailto:wmyuan@hss.caltech.edu">Walter M. Yuan</a>, SSEL, Caltech
*/
@Entity
@Table(name="experiment")
public class Experiment extends BaseObject {
    
    /*
     * FULL = 0
     * CLOSED = 1
     * OPEN = 2
     * FINISHED = 3
     * CANCELLED = 4
     * IMPORTED = 5
     */
    public static enum Status {
        FULL, CLOSED, OPEN, FINISHED, CANCELLED, IMPORTED;
        
        public String toString() {
            switch(this) {
                case FULL:
                    return "Full";
                case CLOSED:
                    return "Closed";
                case OPEN:
                    return "Open";
                case FINISHED:
                    return "Finished";
                case CANCELLED:
                    return "Cancelled";
                case IMPORTED:
                    return "Imported";
                default:
                    return null;
            }
        }
        
        public String getName() {
            return this.name();
        }
        
        public String getString() {
            return this.toString();
        }
    };
    
    private Long id;
    private Experimenter runby;
    private Experimenter principle;
    private User reservedBy;
    private Date expDate;
    private Date startTime;
    private int setupBuffer;
    private Date endTime;
    private int cleanupBuffer;
    private int maxNumSubjects;
    private int minNumSubjects;
    private float showUpFee;
    private String instruction;
    private float signupFreezeTime;
    private Location location;
    private ExperimentType type;
    private Status status;
    private Filter filter;
    private Survey survey;
    private boolean published=false;
    private boolean checkedIRB=false;
    private boolean payGatePayment = false;
    private boolean manualPayment = false;
    private boolean invisible=false;
    private Boolean reserveLaptops;
    
    private static final SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
    
    /** Creates a new instance of Experiment */
    public Experiment() {
    }
    
    @ManyToOne
    public Experimenter getRunby() {
        return runby;
    }
    
    public void setRunby(Experimenter runby) {
        this.runby = runby;
    }
    
    @ManyToOne
    public Experimenter getPrinciple() {
        return principle;
    }
    
    public void setPrinciple(Experimenter principle) {
        this.principle = principle;
    }
    
    @ManyToOne
    public User getReservedBy() {
        return reservedBy;
    }
    
    public void setReservedBy(User reservedBy) {
        this.reservedBy = reservedBy;
    }
    
    @Temporal(TemporalType.DATE)
    public Date getExpDate() {
        return expDate;
    }
    
    public void setExpDate(Date expDate) {
        this.expDate = expDate;
    }
    
    @Temporal(TemporalType.TIME)
    public Date getStartTime() {
        return startTime;
    }
    
    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }
    
    @Temporal(TemporalType.TIME)
    public Date getEndTime() {
        return endTime;
    }
    
    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }
    
    //Get the actual time and date when the experiment signup is frozen
    @Transient
    public GregorianCalendar getFreezeTime() {

        // BRS 2012-04-04
        if (expDate == null || startTime == null) {
            // Can't calculate the freezeTime
            return null;
        }

        //Set "start" to be the absolute start time of the experiment
        GregorianCalendar start = new GregorianCalendar(expDate.getYear()+1900, expDate.getMonth(), expDate.getDate(), startTime.getHours(), startTime.getMinutes());
        //And then subtract the signup freeze time
        start.add(Calendar.MINUTE, -Math.round(signupFreezeTime));
        
        return start;
    }

    // BRS 2011-10-27
    @Transient
    public GregorianCalendar getStartDateTime() {
        GregorianCalendar start = new GregorianCalendar(expDate.getYear()+1900, expDate.getMonth(), expDate.getDate(), startTime.getHours(), startTime.getMinutes());
        return start;
    }
    
    public int getMaxNumSubjects() {
        return maxNumSubjects;
    }
    
    public void setMaxNumSubjects(int maxNumSubjects) {
        this.maxNumSubjects = maxNumSubjects;
    }
    
    public int getMinNumSubjects() {
        return minNumSubjects;
    }
    
    public void setMinNumSubjects(int minNumSubjects) {
        this.minNumSubjects = minNumSubjects;
    }
    
    public float getShowUpFee() {
        return showUpFee;
    }
    
    public void setShowUpFee(float showUpFee) {
        this.showUpFee = showUpFee;
    }
    
    @Column(columnDefinition="MEDIUMTEXT NULL")
    public String getInstruction() {
        return instruction;
    }
    
    public void setInstruction(String instruction) {
        this.instruction = instruction;
    }
    
    public float getSignupFreezeTime() {
        return signupFreezeTime;
    }
    
    public void setSignupFreezeTime(float signupFreezeTime) {
        this.signupFreezeTime = signupFreezeTime;
    }
    
    @ManyToOne
    public Location getLocation() {
        return location;
    }
    
    public void setLocation(Location location) {
        this.location = location;
    }
    
    @ManyToOne
    public ExperimentType getType() {
        return type;
    }
    
    public void setType(ExperimentType type) {
        this.type = type;
    }
    
    public Status getStatus() {
        return status;
    }
    
    public void setStatus(Status status) {
        this.status = status;
    }
    
    @Transient
    public String getStatusString() {
        return status.toString();
    }
    
    public void setStatusString(String s) {
        try {
            status = Status.valueOf(s);
        } catch (Exception e) {
            System.err.println("Failed to set experiment status: " + s + " is not a valid status");
        }
    }
    
    @ManyToOne
    public Filter getFilter() {
        return filter;
    }
    
    public void setFilter(Filter filter) {
        this.filter = filter;
    }
    
    @Id @GeneratedValue(strategy=GenerationType.AUTO)
    public Long getId() {
        return id;
    }
    
    public void setId(Long id) {
        this.id = id;
    }
    
    @ManyToOne
    public Survey getSurvey() {
        return survey;
    }
    
    public void setSurvey(Survey survey) {
        this.survey = survey;
    }

    @Column(name="published")
    public boolean getPublished() {
        return published;
    }

    public void setPublished(boolean published) {
        this.published = published;
    }
    
    @Transient
    public String getFormattedDescription() {
        StringBuffer buf = new StringBuffer();
        try {
            buf.append(id)
               .append(") ")
               .append(formatter.format(expDate))
               .append("; ")
               .append(location.getName());
            if(instruction != null && instruction.length() > 0) {
                buf.append("; ")
                   .append(instruction);
            }
        } catch(Exception any) {
            buf = new StringBuffer();
            buf.append(id);
        }
        
        if(buf.length() > 40)
            return buf.toString().substring(0, 36) + "...";
        else
            return buf.toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
            .append(runby).append(principle).append(reservedBy).append(expDate)
            .append(startTime).append(endTime).append(maxNumSubjects)
            .append(setupBuffer).append(cleanupBuffer)
            .append(minNumSubjects).append(showUpFee).append(instruction)
            .append(signupFreezeTime).append(location).append(type).append(status)
            .append(filter).append(survey).append(published).append(checkedIRB).append(invisible).toHashCode();
    }

    @Override
    public boolean equals(final Object other) {
        if (this == other)
            return true;
        if (!(other instanceof Experiment))
            return false;
        Experiment castOther = (Experiment) other;
        if (castOther == null)
            return false;
        return new EqualsBuilder()
            .append(runby, castOther.runby).append(principle, castOther.principle)
            .append(reservedBy, castOther.reservedBy).append(expDate, castOther.expDate)
            .append(startTime, castOther.startTime).append(endTime, castOther.endTime)
            .append(setupBuffer, castOther.setupBuffer).append(cleanupBuffer, castOther.cleanupBuffer)
            .append(maxNumSubjects, castOther.maxNumSubjects).append(minNumSubjects, castOther.minNumSubjects)
            .append(showUpFee, castOther.showUpFee).append(instruction, castOther.instruction)
            .append(signupFreezeTime, castOther.signupFreezeTime).append(location, castOther.location)
            .append(type, castOther.type).append(status, castOther.status)
            .append(filter, castOther.filter).append(survey, castOther.survey)
            .append(published, castOther.published)
            .append(checkedIRB, castOther.checkedIRB)
            .isEquals();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.DEFAULT_STYLE)
            .append("id", id).append("runby", runby).append("principle", principle)
            .append("reservedBy", reservedBy).append("expDate", expDate)
            .append("startTime", startTime).append("endTime", endTime)
            .append("setupBuffer", setupBuffer).append("cleanupBuffer", cleanupBuffer)
            .append("maxNumSubjects", maxNumSubjects).append("minNumSubjects", minNumSubjects)
            .append("showUpFee", showUpFee).append("instruction", instruction)
            .append("signupFreezeTime", signupFreezeTime).append("location", location)
            .append("type", type).append("status", status).append("filter", filter)
            .append("survey", survey).append("published", published)
            .append("checkedIRB", checkedIRB).toString();
    }

    @Column(name="checked_irb", nullable=false)
    public boolean isCheckedIRB() {
        return checkedIRB;
    }

    public void setCheckedIRB(boolean checkedIRB) {
        this.checkedIRB = checkedIRB;
    }

    @Column(name="manual_payment")
    public boolean isManualPayment() {
        return manualPayment;
    }

    public void setManualPayment(boolean manualPayment) {
        this.manualPayment = manualPayment;
    }

    @Column(name="paygate_payment")
    public boolean isPayGatePayment() {
        return payGatePayment;
    }

    public void setPayGatePayment(boolean payGatePayment) {
        this.payGatePayment = payGatePayment;
    }
    
    @Column(name="invisible")
    public boolean isInvisible() {
        return invisible;
    }

    public void setInvisible(boolean invisible) {
        this.invisible = invisible;
    }

    @Column(name="cleanup_buffer")
    public int getCleanupBuffer() {
        return cleanupBuffer;
    }

    public void setCleanupBuffer(int cleanupBuffer) {
        this.cleanupBuffer = cleanupBuffer;
    }

    @Column(name="setup_buffer")
    public int getSetupBuffer() {
        return setupBuffer;
    }

    public void setSetupBuffer(int setupBuffer) {
        this.setupBuffer = setupBuffer;
    }

    // BRS 2011-02-15 fix for PIs in ExperimentType instead of Experiment
    public boolean isWritable(String username){
        for (User u : this.type.getPrincipalInvestigators()) {
            if (u.getUsername().equals(username))
                return true;
        }
        if(
                this.getReservedBy().getUsername().equals(username) ||
                this.getRunby().getUsername().equals(username))
            return true;
        
        return false;
    }
    
    @Column(nullable=true)
    public Boolean getReserveLaptops() {
        return this.reserveLaptops;
    }
    public void setReserveLaptops(Boolean reserveLaptops) {
        this.reserveLaptops = reserveLaptops;
    }
}
