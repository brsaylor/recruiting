/*
 * Copyright (C) 1998-2009, <a href="http://www.ssel.caltech.edu">SSEL</a>,
 * at Caltech, Pasadena, CA 91125.
 *
 * MooreRecruiting is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 * USA.
 *
 */


package edu.caltech.ssel.recruiting.model;

import java.util.List;
import java.util.ArrayList;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.StringUtils;

/**
* @author Michael Kolodrubetz, SSEL, Caltech
* @author <a href="mailto:wmyuan@hss.caltech.edu">Walter M. Yuan</a>, SSEL, Caltech
*/
//Hibernate configuration is in Condition.hbm.xml
public class PoolCondition extends Condition {
    
    private List<SubjectPool> pools;
    private boolean memberOf;
    
    /**
     * Creates a new instance of PoolCondition
     */
    public PoolCondition() {
    }
    
    public boolean isEligible(Subject subj) {
        int inCnt = 0;
        
        //Count the number of subject's pools that are in this.pools
        for(int j = 0; j < subj.getPools().size(); j++)
                if(pools.contains(subj.getPools().get(j)))
                    inCnt++;
        
        //If memberOf==true, than they must be in at least one of this.pools
        if(memberOf)
            return inCnt > 0;
        //Otherwise they shouldn't be in any
        return inCnt == 0;
    }
    
    public boolean isMemberOf() {
        return memberOf;
    }
    
    public void setMemberOf(boolean memberOf) {
        this.memberOf = memberOf;
    }
    
    public List<SubjectPool> getPools() {
        return pools;
    }
    
    public void setPools(List<SubjectPool> pools) {
        this.pools = pools;
    }

    public String getConditionType() {
        return "PoolCondition";
    }

    public String getHumanString() {
        StringBuilder s = new StringBuilder();
        s.append("Pool Condition: ");
        s.append(memberOf ? "Include only " : "Exclude ");
        s.append("subjects who are members of ");
        if (pools.size() == 1) {
            s.append("the subject pool " + pools.get(0).getName());
        } else {
            s.append("the following subject pools: ");
            ArrayList poolNames = new ArrayList();
            for (SubjectPool pool : pools) {
                poolNames.add(pool.getName());
            }
            s.append(StringUtils.join(poolNames, ", "));
        }
        s.append(".");
            
        return s.toString();
    }

    public String toString() {
        return new ToStringBuilder(this).
                append("id", getId()).
                append("name", getName()).
                append("type", getConditionType()).
                append("pools", getPools()).
                append("menberOf", isMemberOf()).toString();
    }

    // BRS 2011-11-14
    public PoolCondition clone() {
        PoolCondition c = new PoolCondition();
        c.memberOf = memberOf;
        c.pools = pools;
        return c;
    }
}
