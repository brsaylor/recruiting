/*
 * Copyright (C) 1998-2009, <a href="http://www.ssel.caltech.edu">SSEL</a>,
 * at Caltech, Pasadena, CA 91125.
 *
 * MooreRecruiting is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 * USA.
 *
 */

package edu.caltech.ssel.recruiting.model;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import org.appfuse.model.BaseObject;
import org.appfuse.model.User;


/**
* @author Michael Kolodrubetz, SSEL, Caltech
* @author <a href="mailto:wmyuan@hss.caltech.edu">Walter M. Yuan</a>, SSEL, Caltech
*/
@Entity
@Table(name="reminder")
public class Reminder extends BaseObject implements java.io.Serializable {

    private Long id;
    private User user;
    private ReminderType reminderType;
    private Status status;
    private Long refId;
    private Date whenDate;
    private Boolean sms;
     
    public enum ReminderType {
        OTHER, EXPERIMENT;
        public String toString() {
            switch(this) {
                case OTHER:
                    return "Other";
                case EXPERIMENT:
                    return "Experiment";
            }
            return "";
        }
        //For web stuff
        public String getName() {
            return this.name();
        }
    };
    
    public static enum Status {
        UNPROCESSED, PROCESSING, PROCESSED, WONTPROCESS, FAILED;
        
        public String toString() {
            switch(this) {
                case UNPROCESSED:
                    return "Unprocessed";
                case PROCESSING:
                    return "Processing";
                case PROCESSED:
                    return "Processed";
                case WONTPROCESS:
                    return "WontProcess";
                case FAILED:
                    return "Failed";
                default:
                    return null;
            }
        }
        
        public String getName() {
            return this.name();
        }
        
        public String getString() {
            return this.toString();
        }
    };

    public Reminder() {
        this.sms = false;
    }

    public Reminder(User user, ReminderType reminderType, Long refId, Date whenDate) {
        this.user = user;
        this.reminderType = reminderType;
        this.refId = refId;
        this.whenDate = whenDate;
        this.status = Status.UNPROCESSED;
        this.sms = false;
    }
   
    @Id @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name="id", unique=true, nullable=false)
    public Long getId() {
        return this.id;
    }
    
    public void setId(Long id) {
        this.id = id;
    }
    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="user_id", nullable=false)
    public User getUser() {
        return this.user;
    }
    
    public void setUser(User user) {
        this.user = user;
    }
    
    @Column(name="remindertype")
    public ReminderType getReminderType() {
        return this.reminderType;
    }
    
    public void setReminderType(ReminderType reminderType) {
        this.reminderType = reminderType;
    }
    
    @Column(name="status")
    public Status getStatus() {
        return status;
    }
    
    public void setStatus(Status status) {
        this.status = status;
    }
    
    @Column(name="ref_id")
    public Long getRefId() {
        return this.refId;
    }
    
    public void setRefId(Long refId) {
        this.refId = refId;
    }
    
    @Column(name="whendate", length=0)
    @Temporal(value=TemporalType.TIMESTAMP)
    public Date getWhenDate() {
        return this.whenDate;
    }
    
    public void setWhenDate(Date whenDate) {
        this.whenDate = whenDate;
    }

    // BRS
    @Column(nullable=true)
    public Boolean getSms() {
        return this.sms;
    }
    public void setSms(Boolean sms) {
        this.sms = sms;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
            .append(id).append(user).append(reminderType).append(status)
            .append(refId).append(whenDate).toHashCode();
    }

    @Override
    public boolean equals(final Object other) {
        if (this == other)
            return true;
        Reminder castOther = (Reminder) other;
        return new EqualsBuilder()
            .append(id, castOther.id).append(user, castOther.user).append(reminderType, castOther.reminderType)
            .append(status, castOther.status).append(refId, castOther.refId)
            .append(whenDate, castOther.whenDate).isEquals();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.DEFAULT_STYLE)
            .append("id", id).append("user", user).append("reminderType", reminderType)
            .append("status", status).append("refId", refId).append("whenDate", whenDate)
            .toString();
    }
}
