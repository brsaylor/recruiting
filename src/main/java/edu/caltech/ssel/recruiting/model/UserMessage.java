/*
 * Copyright (C) 1998-2009, <a href="http://www.ssel.caltech.edu">SSEL</a>,
 * at Caltech, Pasadena, CA 91125.
 *
 * MooreRecruiting is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 * USA.
 *
 */

package edu.caltech.ssel.recruiting.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import org.appfuse.model.User;
import org.appfuse.model.BaseObject;

/**
* @author Michael Kolodrubetz, SSEL, Caltech
* @author <a href="mailto:wmyuan@hss.caltech.edu">Walter M. Yuan</a>, SSEL, Caltech
*/
@Entity
@Table(name="user_message")
public class UserMessage extends BaseObject implements java.io.Serializable {

    private Long id;
    private Message message;
    private User recipient;
    private Status status;
    
    public enum Status {
        NEW, VIEWED, DELETED;
        public String toString() {
            switch(this) {
                case NEW:
                    return "New";
                case VIEWED:
                    return "Viewed";
                case DELETED:
                    return "Deleted";
            }
            return "";
        }
        //For web stuff
        public String getName() {
            return this.name();
        }
    };

    public UserMessage() {
    }
	
    public UserMessage(Long id, Message message) {
        this.id = id;
        this.message = message;
    }
    public UserMessage(Long id, Message message, User recipient, Status status) {
       this.id = id;
       this.message = message;
       this.recipient = recipient;
       this.status = status;
    }
   
    @Id @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name="id", unique=true, nullable=false)
    public Long getId() {
        return this.id;
    }
    
    public void setId(Long id) {
        this.id = id;
    }
    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="message_id", nullable=false)
    public Message getMessage() {
        return this.message;
    }
    
    public void setMessage(Message message) {
        this.message = message;
    }
    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="recipient_id")
    public User getRecipient() {
        return this.recipient;
    }
    
    public void setRecipient(User recipient) {
        this.recipient = recipient;
    }
    
    @Column(name="status")
    public Status getStatus() {
        return this.status;
    }
    
    public void setStatus(Status status) {
        this.status = status;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
            .append(id).toHashCode();
    }

    @Override
    public boolean equals(final Object other) {
        if (this == other)
            return true;
        if (!(other instanceof UserMessage))
            return false;
        UserMessage castOther = (UserMessage) other;
        if (castOther == null)
            return false;
        return new EqualsBuilder()
            .append(id, castOther.id).isEquals();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.DEFAULT_STYLE)
            .append("id", id).append("message", message).append("recipient", recipient)
            .append("status", status).toString();
    }
   
}