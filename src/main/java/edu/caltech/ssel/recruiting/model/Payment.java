/*
 * Copyright (C) 1998-2009, <a href="http://www.ssel.caltech.edu">SSEL</a>,
 * at Caltech, Pasadena, CA 91125.
 *
 * MooreRecruiting is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 * USA.
 *
 */

package edu.caltech.ssel.recruiting.model;

import java.util.Date;
import javax.persistence.*;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.appfuse.model.BaseObject;

/**
* @author Michael Kolodrubetz, SSEL, Caltech
* @author <a href="mailto:wmyuan@hss.caltech.edu">Walter M. Yuan</a>, SSEL, Caltech
*/
@Entity 
@Table(name="payment")
public class Payment extends BaseObject {
    
    public static enum Status {
        NA, NEW, SENT, PROCESSED, COMPLETED, UNCLAIMED, FAILED;
        
        public String toString() {
            switch(this) {
                case NA:
                    return "NA";
                case NEW:
                    return "New";
                case SENT:
                    return "Sent";
                case PROCESSED:
                    return "Processed";
                case COMPLETED:
                    return "Completed";
                case UNCLAIMED:
                    return "Unclaimed";
                case FAILED:
                    return "Failed";
                default:
                    return null;
            }
        }
        
        public String getName() {
            return this.name();
        }
        
        public String getString() {
            return this.toString();
        }
    };
    
    private Long id;
    private String accountInfo;
    private Status status;
    private String trackingId;
    private Date whenCreated;
    private Date whenUpdated;
    
    /** Creates a new instance of Payment */
    public Payment() {
    }
    
    @Id @GeneratedValue(strategy=GenerationType.AUTO)
    public Long getId() {
        return id;
    }
    
    public void setId(Long id) {
        this.id = id;
    }

    @Column(name="accountInfo", length=255)
    public String getAccountInfo() {
        return accountInfo;
    }

    public void setAccountInfo(String accountInfo) {
        this.accountInfo = accountInfo;
    }
    
    public Status getStatus() {
        return status;
    }
    
    public void setStatus(Status status) {
        this.status = status;
    }
    
    @Transient
    public String getStatusString() {
        return status.toString();
    }
    
    public void setStatusString(String s) {
        try {
            status = Status.valueOf(s);
        } catch (Exception e) {
            System.err.println("Failed to set experiment status: " + s + " is not a valid status");
        }
    }

    public String getTrackingId() {
        return trackingId;
    }

    public void setTrackingId(String trackingId) {
        this.trackingId = trackingId;
    }

    @Temporal(value=TemporalType.TIMESTAMP)
    public Date getWhenCreated() {
        return whenCreated;
    }

    public void setWhenCreated(Date whenCreated) {
        this.whenCreated = whenCreated;
    }

    @Temporal(value=TemporalType.TIMESTAMP)
    public Date getWhenUpdated() {
        return whenUpdated;
    }

    public void setWhenUpdated(Date whenUpdated) {
        this.whenUpdated = whenUpdated;
    }

    @Override
    public boolean equals(final Object other) {
        if (this == other)
            return true;
        if (!(other instanceof Payment))
            return false;
        Payment castOther = (Payment) other;
        if (castOther == null)
            return false;
        return new EqualsBuilder()
            .append(accountInfo, castOther.accountInfo).append(status, castOther.status)
            .append(trackingId, castOther.trackingId).append(whenCreated, castOther.whenCreated)
            .append(whenUpdated, castOther.whenUpdated).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
            .append(accountInfo).append(status).append(trackingId).append(whenCreated)
            .append(whenUpdated).toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.DEFAULT_STYLE)
            .append("id", id).append("accountInfo", accountInfo).append("status", status)
            .append("trackingId", trackingId).append("whenCreated", whenCreated)
            .append("whenUpdated", whenUpdated).toString();
    }
}
