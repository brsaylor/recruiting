/*
 * Copyright (C) 1998-2009, <a href="http://www.ssel.caltech.edu">SSEL</a>,
 * at Caltech, Pasadena, CA 91125.
 *
 * MooreRecruiting is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 * USA.
 *
 */


package edu.caltech.ssel.recruiting.model;

import edu.caltech.ssel.recruiting.common.QuestionResultHelper;
import edu.caltech.ssel.recruiting.model.Question.QStyle;
import com.thoughtworks.xstream.XStream;
import java.util.Set;
import java.util.ArrayList;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
* @author Michael Kolodrubetz, SSEL, Caltech
* @author <a href="mailto:wmyuan@hss.caltech.edu">Walter M. Yuan</a>, SSEL, Caltech
*/
//Hibernate configuration is in Condition.hbm.xml
public class QuestionResultCondition extends Condition {
    
    public static enum FilterType{
        CHECK_ALL, CHECK_SOME, CHECK_NONE, CHECK_NOT_SOME, SELECT, NO_SELECT;
        public String getStringType() {
            // BRS 2011-12-02: changed string representations slightly
            switch(this) {
                case CHECK_ALL:
                    return "check all";
                case CHECK_SOME:
                    return "check some";
                case CHECK_NONE:
                    return "check none";
                case CHECK_NOT_SOME:
                    return "do not check all";
                case SELECT:
                    return "select one";
                case NO_SELECT:
                    return "select none";
            }
            return null;
        }
    };
    
    private static final Log log = LogFactory.getLog(QuestionResultCondition.class);
    private Question question;
    private Set<Integer> filterOptions;
    private FilterType filterType;
    
    /** Creates a new instance of QuestionResultCondition */
    public QuestionResultCondition() {
    }
    
    public boolean isEligible(QuestionResult qr) {
        //Default response is that they are eligible
        if(qr == null || !qr.getQuestion().equals(question))
            return true;
        int cnt = 0;
        int sz = question.getOptionList().size();
        int ans = qr.getAnswerIdx();
        if(question.getStyle() == QStyle.CHECK_BOX) {
            //boolean[] ansarray = new boolean[]
            for(int j = 0; j < sz; j++) {
                //If option j is selected and is one the filter options, increment cnt
                if(QuestionResultHelper.hasDigit(ans, j) && filterOptions.contains(j))
                    cnt++;
            }
        } else {
            cnt = filterOptions.contains(ans) ? 1 : 0;
        }
        
        log.debug("filterOptions == ");
        for(Integer j : filterOptions)
            log.debug("\t" + j.toString());
        log.debug("ans == " + ans);
        log.debug("cnt == " + cnt);
        
        if(filterType == null) {
            System.err.println("filterType is null - Temporarily setting it to a default value...");
            filterType = (question.getStyle() == QStyle.CHECK_BOX) ? FilterType.CHECK_ALL : FilterType.SELECT;
        }
        log.debug("filterType == " + filterType.name());
        
        // BRS: cnt is the number of answers included in filterOptions that were selected by the subject.
        switch(filterType) {
            case CHECK_ALL:
                return (cnt == filterOptions.size());
            case CHECK_SOME:
            case SELECT:
                return (cnt > 0);
            case CHECK_NOT_SOME: // BRS: Subject did not check all options included in filterOptions.
                return (cnt < filterOptions.size());
            default: // BRS: CHECK_NONE, NO_SELECT
                return (cnt == 0);
        }
    }
    
    public Question getQuestion() {
        return question;
    }
    
    public void setQuestion(Question question) {
        this.question = question;
    }
    
    public Set<Integer> getFilterOptions() {
        return filterOptions;
    }
    
    public void setFilterOptions(Set<Integer> filterOptions) {
        this.filterOptions = filterOptions;
    }
    
    public String getFilterOptionXML() {
        XStream xstream = new XStream();
        xstream.alias("set", Set.class);
        return xstream.toXML(filterOptions);
    }
    
    public void setFilterOptionXML(String xml) {
        XStream xstream = new XStream();
        xstream.alias("set", Set.class);
        filterOptions = (Set<Integer>)xstream.fromXML(xml);
    }
    
    public FilterType getFilterType() {
        return filterType;
    }
    
    public void setFilterType(FilterType filterType) {
        this.filterType = filterType;
    }
    
    public String getConditionType() {
        return "QuestionResultCondition";
    }

    public String getHumanString() {
        log.debug("QRC " + getId() + " getHumanString()");
        log.debug("filterOptions = " + filterOptions.toString());
        log.debug("question.getOptionList() = " + question.getOptionList().toString());
        StringBuilder s = new StringBuilder();
        s.append("Question Result Condition: ");
        s.append("Include only subjects who ");
        s.append(filterType.getStringType());
        s.append(" of the following options for question ");
        s.append("\"" + question.getLabel() + "\": ");
        ArrayList<String> filterOptionNames = new ArrayList();
        for (Integer idx : filterOptions) {
            log.debug("idx = " + idx);
            try {
                filterOptionNames.add("\"" + question.getOptionList().get(idx) + "\"");
            } catch(ArrayIndexOutOfBoundsException e) {
                log.warn("caught ArrayIndexOutOfBoundsException, which means this QRC has a filter option invalid for its question.");
            }
        }
        s.append(StringUtils.join(filterOptionNames, ", "));
        s.append(".");
        return s.toString();
    }

    public String toString() {
        return new ToStringBuilder(this).
                append("id", getId()).
                append("name", getName()).
                append("type", getConditionType()).
                append("filterOptions", getFilterOptions()).
                append("question_id", getQuestion().getId()).
                append("filterType", getFilterType()).toString();
    }
    
    // BRS 2011-11-14
    public QuestionResultCondition clone() {
        QuestionResultCondition c = new QuestionResultCondition();
        c.question = question;
        c.filterOptions = filterOptions;
        c.filterType = filterType;
        return c;
    }
}
