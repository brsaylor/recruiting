/*
 * Copyright (C) 1998-2009, <a href="http://www.ssel.caltech.edu">SSEL</a>,
 * at Caltech, Pasadena, CA 91125.
 *
 * MooreRecruiting is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 * USA.
 *
 */

package edu.caltech.ssel.recruiting.dao;

import edu.caltech.ssel.recruiting.model.Message;
import edu.caltech.ssel.recruiting.model.UserMessage;
import org.appfuse.model.User;
import java.util.List;
import org.appfuse.dao.GenericDao;

/**
* 
* @author <a href="mailto:wmyuan@hss.caltech.edu">Walter M. Yuan</a>, SSEL, Caltech
*/
public interface UserMessageDao extends GenericDao<UserMessage, Long> {
    public List<UserMessage> findInboxByRecipient(User recipient);
    public List<UserMessage> findDeletedByRecipient(User recipient);
    public List<UserMessage> findByMessage(Message message);
    public void deleteMessageRecipients(Message message);
}