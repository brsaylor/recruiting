/*
 * Copyright (C) 1998-2009, <a href="http://www.ssel.caltech.edu">SSEL</a>,
 * at Caltech, Pasadena, CA 91125.
 *
 * MooreRecruiting is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 * USA.
 *
 */


package edu.caltech.ssel.recruiting.dao.hibernate;

import edu.caltech.ssel.recruiting.dao.ParticipantRecordDao;
import edu.caltech.ssel.recruiting.model.Experiment;
import edu.caltech.ssel.recruiting.model.ParticipantRecord;
import edu.caltech.ssel.recruiting.model.Subject;
import java.util.List;
import org.appfuse.dao.hibernate.GenericDaoHibernate;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

/**
* @author Michael Kolodrubetz, SSEL, Caltech
* @author <a href="mailto:wmyuan@hss.caltech.edu">Walter M. Yuan</a>, SSEL, Caltech
*/
public class ParticipantRecordDaoHibernate extends GenericDaoHibernate<ParticipantRecord, Long> implements ParticipantRecordDao {
    
    /** Creates a new instance of ParticipantRecordDaoHibernate */
    public ParticipantRecordDaoHibernate() {
        super(ParticipantRecord.class);
    }
    
    public List<ParticipantRecord> getByExperiment(Experiment e) {
        return getHibernateTemplate().find("from ParticipantRecord where exp_id=?", e.getId());
    }
    
    public float getPayoffByExperiment(Experiment e) {
        String ret = String.valueOf(getHibernateTemplate().find("select sum(payoff) from ParticipantRecord where exp_id=?",e.getId()).get(0));
        if(ret.equals("null"))
            return 0.0F;
        return (new Float(ret));
    }
    
    public float getPayoffByExperiments(List<Long> experimentIds) {
        if(experimentIds.size() == 0)
            return 0.0F;
        DetachedCriteria criteria = DetachedCriteria.forClass(ParticipantRecord.class);
        criteria.setProjection(Projections.sum("payoff"))
                .createAlias("exp", "exp")
                .add(Restrictions.in("exp.id", experimentIds));
        String ret = String.valueOf(getHibernateTemplate().findByCriteria(criteria).get(0));
        if(ret.equals("null"))
            return 0.0F;
        return (new Float(ret));
    }
    
    public int getParticipatedByExperiment(Experiment e) {
        return ((Long)(getHibernateTemplate().find("select count(*) from ParticipantRecord where participated=1 and exp_id=?", e.getId()).get(0))).intValue();
    }
    
    public int getParticipatedByExperiments(List<Long> experimentIds) {
        if(experimentIds.size() == 0)
            return 0;
        DetachedCriteria criteria = DetachedCriteria.forClass(ParticipantRecord.class);
        criteria.setProjection(Projections.count("id"))
                .createAlias("exp", "exp")
                .add(Restrictions.eq("participated", true))
                .add(Restrictions.in("exp.id", experimentIds));
        return ((Integer)(getHibernateTemplate().findByCriteria(criteria).get(0))).intValue();
    }
    
    public int getSignupByExperiments(List<Long> experimentIds) {
        if(experimentIds.size() == 0)
            return 0;
        DetachedCriteria criteria = DetachedCriteria.forClass(ParticipantRecord.class);
        criteria.setProjection(Projections.count("id"))
                .createAlias("exp", "exp")
                .add(Restrictions.in("exp.id", experimentIds));
        return ((Integer)(getHibernateTemplate().findByCriteria(criteria).get(0))).intValue();
    }
  
    public List<ParticipantRecord> getBySubject(Subject s) {
        return getHibernateTemplate().find("from ParticipantRecord where subject_id=?", s.getId());
    }
    
    public ParticipantRecord getByExperimentAndSubject(Long expId, Long subId) {
        List<ParticipantRecord> records =
                getHibernateTemplate().find("from ParticipantRecord where exp_id=? and subject_id=?",
                new Long[]{expId, subId});
        if(records.size() == 1)
            return records.get(0);
        else
            return null;
    }
    
    public List<ParticipantRecord> getByPlayerNum(Integer pn) {
        return getHibernateTemplate().find("from ParticipantRecord where playerNum=?", pn);
    }

    public boolean isSignedUp(Subject s, Experiment e) {
        Long [] params = new Long[2];
        params[0] = s.getId();
        params[1] = e.getId();
        List<ParticipantRecord> lst = getHibernateTemplate().find("from ParticipantRecord where subject_id=? and exp_id=?", params);
        if(lst == null || lst.size() == 0)
            return false;
        return true;
    }
    
    // BRS 2010-10-01
    public int getPastCountBySubjectAndStatus(Subject s, ParticipantRecord.Status status) {
        return ((Long) (getHibernateTemplate().find(
                        "select count(*) from ParticipantRecord p "
                        + "where p.subject.id = ? "
                        + "and p.status = ? "
                        + "and curdate() >= p.exp.expDate and curtime() >= p.exp.startTime",
                        new Object[]{s.getId(), status}).get(0))).intValue();
    }
}
