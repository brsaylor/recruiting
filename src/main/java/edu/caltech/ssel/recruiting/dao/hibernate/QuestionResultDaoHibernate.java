/*
 * Copyright (C) 1998-2009, <a href="http://www.ssel.caltech.edu">SSEL</a>,
 * at Caltech, Pasadena, CA 91125.
 *
 * MooreRecruiting is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 * USA.
 *
 */


package edu.caltech.ssel.recruiting.dao.hibernate;

import edu.caltech.ssel.recruiting.dao.QuestionResultDao;
import edu.caltech.ssel.recruiting.model.Experiment;
import edu.caltech.ssel.recruiting.model.Question;
import edu.caltech.ssel.recruiting.model.QuestionResult;
import java.util.List;
import org.appfuse.dao.hibernate.GenericDaoHibernate;
import org.appfuse.model.User;

/**
* @author Michael Kolodrubetz, SSEL, Caltech
* @author <a href="mailto:wmyuan@hss.caltech.edu">Walter M. Yuan</a>, SSEL, Caltech
*/
public class QuestionResultDaoHibernate extends GenericDaoHibernate<QuestionResult, Long> implements QuestionResultDao {
    
    /** Creates a new instance of QuestionResultDaoHibernate */
    public QuestionResultDaoHibernate() {
        super(QuestionResult.class);
    }

    public List<QuestionResult> getAnswersByUser(User u) {
        return getHibernateTemplate().find("from QuestionResult where surveyTaker_id=?", u.getId());
    }

    public List<QuestionResult> getAnswersByExperiment(Experiment e) {
        return getHibernateTemplate().find("from QuestionResult where surveyParent_id=?", e.getId());
    }
    
    public List<QuestionResult> getAnswersByexperimentIdAndQuestionId(Long eid, Long qid) {
        return getHibernateTemplate().find("from QuestionResult where surveyParent_id=? and question_id=?", new Object[]{eid,qid});
    }

    public QuestionResult getMostRecentAnswer(User u, Question q) {
        Long [] params = new Long[2];
        params[0] = u.getId();
        params[1] = q.getId();
        List<QuestionResult> lst = getHibernateTemplate().find("from QuestionResult qr where surveyTaker_id=? and question_id=? order by qr.takenDate desc", params);
        if(lst == null || lst.size() == 0)
            return null;
        return lst.get(0);
    }
    
}
