/*
 * Copyright (C) 1998-2009, <a href="http://www.ssel.caltech.edu">SSEL</a>,
 * at Caltech, Pasadena, CA 91125.
 *
 * MooreRecruiting is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 * USA.
 *
 */


package edu.caltech.ssel.recruiting.dao.hibernate;

import edu.caltech.ssel.recruiting.dao.ExperimentDao;
import edu.caltech.ssel.recruiting.model.Experiment;
import edu.caltech.ssel.recruiting.model.Experimenter;
import edu.caltech.ssel.recruiting.model.Filter;
import edu.caltech.ssel.recruiting.model.Survey;
import java.util.Date;
import java.util.List;
import org.appfuse.dao.hibernate.GenericDaoHibernate;

/**
* @author Michael Kolodrubetz, SSEL, Caltech
* @author <a href="mailto:wmyuan@hss.caltech.edu">Walter M. Yuan</a>, SSEL, Caltech
*/
public class ExperimentDaoHibernate extends GenericDaoHibernate<Experiment, Long> implements ExperimentDao {

    public ExperimentDaoHibernate() {
        super(Experiment.class);
    }
    
    public List<Experiment> getByDate(Date d) {
        return getHibernateTemplate().find("from Experiment where expDate=? order by startTime, id", d);
    }
    
    public List<Experiment> getByDateAndLocationId(Date d, Long locId) {
        return getHibernateTemplate().find("from Experiment where expDate=? and location_id=?", new Object[]{d,locId});
    }

    public List<Experiment> getBySurvey(Survey s) {
        return getHibernateTemplate().find("from Experiment where survey_id=?", s.getId());
    }

    public List<Experiment> getBySurveyId(Long id) {
        return getHibernateTemplate().find("from Experiment where survey_id=?", id);
    }

    public List<Experiment> getByFilter(Filter f) {
        return getHibernateTemplate().find("from Experiment where filter_id=?", f.getId());
    }

    public List<Experiment> getByFilterId(Long id) {
        return getHibernateTemplate().find("from Experiment where filter_id=?", id);
    }

    public List<Experiment> getByRunby(Long runbyId) {
        return getHibernateTemplate().find("from Experiment where runby_id=?", runbyId);
    }

    public List<Experiment> getByReservedBy(Long reservedById) {
        return getHibernateTemplate().find("from Experiment where reservedBy_id=?", reservedById);
    }

    public List<Experiment> getByPrinciple(Long principleId) {
        //return getHibernateTemplate().find("from Experiment where principle_id=?", principleId);

        // BRS 2011-02-16 fix to work with ExperimentType.principalInvestigators
        List<Experimenter> l = getHibernateTemplate().find("from Experimenter where id=?", principleId);
        Experimenter experimenter = l.get(0);
        return getHibernateTemplate().find("from Experiment where ? in elements(type.principalInvestigators)", experimenter);
    }

    public List<Experiment> getByExperimentType(Long expTypeId) {
        return getHibernateTemplate().find("from Experiment where type_id=?", expTypeId);
    }

    public List<Experiment> getByLocation(Long locationId) {
        return getHibernateTemplate().find("from Experiment where location_id=?", locationId);
    }
    
    public List<Experiment> getById(Long id) {
        return getHibernateTemplate().find("from Experiment e where e.id=?", id);
    }
    
    public Date getFirstExperimentDate() {
        return (Date)getHibernateTemplate().find("select min(e.expDate) from Experiment e").get(0);
    }

    public List<Experiment> getByRunbyAndType(Long runbyId, Long expTypeId) {
        return getHibernateTemplate().find("from Experiment where runby_id=? and type_id=?",new Object[]{runbyId,expTypeId});
    }

    // BRS 2011-10-28
    // Get the current value of the given field for the given experiment directly from the database, bypassing the cache.
    public Object getCurrentValue(Long id, String fieldName) {
        List<Object> lst = getSession().createSQLQuery("select " + fieldName + " from experiment where id=" + id).list();
        if(lst.isEmpty())
            return null;
        return lst.get(0);
    }
}
