/*
 * Copyright (C) 1998-2009, <a href="http://www.ssel.caltech.edu">SSEL</a>,
 * at Caltech, Pasadena, CA 91125. 
 *
 * MooreRecruiting is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 * USA.
 *
 */

package edu.caltech.ssel.recruiting.dao;

import edu.caltech.ssel.recruiting.model.Condition;

import edu.caltech.ssel.recruiting.model.BooleanCondition;
import edu.caltech.ssel.recruiting.model.ExperimentCondition;
import edu.caltech.ssel.recruiting.model.ExperimentTypeCondition;
import edu.caltech.ssel.recruiting.model.NoShowCondition;
import edu.caltech.ssel.recruiting.model.PoolCondition;
import edu.caltech.ssel.recruiting.model.QuestionResultCondition;

import org.appfuse.dao.GenericDao;

import java.sql.Date;
import java.util.Set;
import java.util.List;

/**
* @author <a href="mailto:wmyuan@hss.caltech.edu">Walter M. Yuan</a>, SSEL, Caltech
*/
public interface ConditionDao extends GenericDao<Condition, Long> {
    public List<ExperimentTypeCondition> getExperimentTypeConditionByName(String name);
    public Set<Long> getEligibleSubjectByETC(Long etcID);
    public Set<Long> getEligibleSubjectByNotETC(Long etcID);
    public Set<Long> getEligibleSubjectByFutureETC(Long etcID, Date date);
    public Set<Long> getEligibleSubjectByNotFutureETC(Long etcID, Date date);

    // BRS 2011-08-22
    // Return a set of subject IDs of subjects who are eligible based on the given condition.
    public Set<Long> getEligibleSubjectIDs(Condition cond);

    // BRS 2012-02-09
    // Return a set of subject IDs of subjects who are unsubscribed.
    public Set<Long> getUnsubscribedSubjectIDs();
}
