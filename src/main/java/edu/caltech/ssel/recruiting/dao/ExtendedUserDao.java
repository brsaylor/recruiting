/*
 * Copyright (C) 1998-2009, <a href="http://www.ssel.caltech.edu">SSEL</a>,
 * at Caltech, Pasadena, CA 91125.
 *
 * MooreRecruiting is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 * USA.
 *
 */


package edu.caltech.ssel.recruiting.dao;

import edu.caltech.ssel.recruiting.model.Experimenter;
import edu.caltech.ssel.recruiting.model.Subject;
import java.util.List;
import java.util.Set;
import org.appfuse.dao.UserDao;
import org.appfuse.model.LabelValue;
import org.appfuse.model.User;

/**
*
* @author <a href="mailto:wmyuan@hss.caltech.edu">Walter M. Yuan</a>, SSEL, Caltech
*/
public interface ExtendedUserDao extends UserDao {
    //For all of these methods, only eligible users will be returned
    public String getCurrentPasswordById(Long id); // BRS 2011-02-03
    public List<Experimenter> getExperimenters();
    public List<String> getExperimenterEmails();
    public Experimenter getExperimenterById(Long id);
    public List<Subject> getSubjects();
    public List<Subject> getSubjectsByNotInIdSet(List<Long> ids);
    public List<String> getSubjectEmailsByIdSet(Set<Long> ids);
    public List<String> getSubjectEmails();
    public Subject getSubjectById(Long id);
    public Long getSubjectIDByParticipationID(Long id);
    public List<Subject> getSubjectBySchoolID(String sid);
    public Subject getSubjectByEmail(String email);
    public List<User> getAdmins();
    public List<User> getStandardUsers();
    public List<User> getEnabledUsers();
    public List<String> getEnabledUserEmails();

    // BRS 2011-11-01
    public User getNonSubjectByEmail(String email);
}
