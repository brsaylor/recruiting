/*
 * Copyright (C) 1998-2009, <a href="http://www.ssel.caltech.edu">SSEL</a>,
 * at Caltech, Pasadena, CA 91125.
 *
 * MooreRecruiting is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 * USA.
 *
 */


package edu.caltech.ssel.recruiting.dao.hibernate;

import edu.caltech.ssel.recruiting.common.ExtendedPaginatedList;
import edu.caltech.ssel.recruiting.dao.PagingDao;
import edu.caltech.ssel.recruiting.model.Experimenter;
import edu.caltech.ssel.recruiting.model.ExperimentType;
import edu.caltech.ssel.recruiting.model.ParticipantRecord;
import edu.caltech.ssel.recruiting.model.Subject;
import org.appfuse.dao.hibernate.UniversalDaoHibernate;
import org.appfuse.model.User;
import org.displaytag.properties.SortOrderEnum;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Expression;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.dao.DataAccessException;

// BRS 2012-02-07
import org.hibernate.Query;
import org.hibernate.transform.Transformers;
import java.math.BigInteger;

import java.util.List;


/**
*
* @author Richar Ho, CASSEL, UCLA
* @author <a href="mailto:wmyuan@hss.caltech.edu">Walter M. Yuan</a>, SSEL, Caltech
*/
public class PagingDaoHibernate extends UniversalDaoHibernate implements PagingDao {

    // BRS 2012-02-07
    // Named queries are defined in src/main/webapp/WEB-INF/classes/NamedQueries.hbm.xml
    // Paging and sorting are handled in this method, rather than explictly including
    // LIMIT/OFFSET or ORDER BY in the query itself.
    public ExtendedPaginatedList getNamedQueryResults(String queryName, final ExtendedPaginatedList page) throws DataAccessException {

        // Get the named query
        Query query = getSession().getNamedQuery(queryName);

        // Append the ORDER BY clause to the query.
        // For safety, non-alphanumeric, non-underscore characters are removed from the sort column name.
        if (page.getSortCriterion() != null) {
            String sortColumn = page.getSortCriterion().replaceAll("[^A-Za-z0-9_]", "");
            String sortDirection = page.getSortDirection() == SortOrderEnum.DESCENDING ? "desc" : "asc";
            query = getSession().createSQLQuery(query.getQueryString() + "\norder by " + sortColumn + " " + sortDirection);
        }

        // Get a page of results as a List of Maps and put them into the PaginatedList.
        query.setFirstResult(page.getFirstRecordIndex());
        query.setMaxResults(page.getPageSize());
        query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
        page.setList(query.list());

        // Get the count of total results without paging
        query = getSession().getNamedQuery(queryName + "_count");
        page.setTotalNumberOfRows(((BigInteger)query.uniqueResult()).intValue());

        return page;
    }
    
    public ExtendedPaginatedList getAllUsers (final ExtendedPaginatedList page) throws DataAccessException {
        
        String sortCriterion = page.getSortCriterion();
        int index = page.getFirstRecordIndex();
        SortOrderEnum sortDirection = page.getSortDirection();
        
        DetachedCriteria criteria = DetachedCriteria.forClass(User.class);
        if (sortCriterion != null) {
            if (sortDirection.equals(SortOrderEnum.ASCENDING)) {criteria.addOrder(Order.asc(sortCriterion));}
            if (sortDirection.equals(SortOrderEnum.DESCENDING)) {criteria.addOrder(Order.desc(sortCriterion));}
        }
        if(page.getSearch()) {
            criteria.add(Restrictions.in("id", page.getSearchIds()));
            page.setTotalNumberOfRows(page.getSearchIds().size());
        }
        else {
            page.setTotalNumberOfRows(getTotalRowNum("select count(*)from User where DTYPE='Subject'"));
        }
        
        int pageSize = page.getPageSize();
        log.debug("pageSize="+pageSize);
        page.setList(getHibernateTemplate().findByCriteria(criteria, index, pageSize));
        
        return page;
    }
    
    public ExtendedPaginatedList getSubjects(final ExtendedPaginatedList page) throws DataAccessException {
        
        String sortCriterion = page.getSortCriterion();
        int index = page.getFirstRecordIndex();
        SortOrderEnum sortDirection = page.getSortDirection();

        DetachedCriteria ucriteria = DetachedCriteria.forClass(User.class);
        ucriteria.add(Restrictions.eq("enabled", true))
                .setProjection(Projections.groupProperty("id"));
        List<Long>users = getHibernateTemplate().findByCriteria(ucriteria);
        DetachedCriteria criteria = DetachedCriteria.forClass(Subject.class);
        criteria.add(Restrictions.in("id",users));
        if (sortCriterion != null) {
            if (sortDirection.equals(SortOrderEnum.ASCENDING)) {criteria.addOrder(Order.asc(sortCriterion));}
            if (sortDirection.equals(SortOrderEnum.DESCENDING)) {criteria.addOrder(Order.desc(sortCriterion));}
        }
        if(page.getSearch()) {
            criteria.add(Restrictions.in("id", page.getSearchIds()));
            page.setTotalNumberOfRows(page.getSearchIds().size());
        }
        else {
            page.setTotalNumberOfRows(getTotalRowNum("select count(*)from User where DTYPE='Subject' and account_enabled=1"));
        }
        
        int pageSize = page.getPageSize();
        page.setList(getHibernateTemplate().findByCriteria(criteria, index, pageSize)); 
        //page.setTotalNumberOfRows(getTotalRowNum(criteria));
        
        return page;
    }
    
    public ExtendedPaginatedList getExperimenters(final ExtendedPaginatedList page) throws DataAccessException {
        String sortCriterion = page.getSortCriterion();
        int index = page.getFirstRecordIndex();
        SortOrderEnum sortDirection = page.getSortDirection();

        DetachedCriteria ucriteria = DetachedCriteria.forClass(User.class);
        ucriteria.add(Restrictions.eq("enabled", true))
                .setProjection(Projections.groupProperty("id"));
        List<Long>users = getHibernateTemplate().findByCriteria(ucriteria);
        DetachedCriteria criteria = DetachedCriteria.forClass(Experimenter.class);
        criteria.add(Restrictions.in("id", users));
        if (sortCriterion != null  &&  page.getSearchIds().size() > 0) {
            if (sortDirection.equals(SortOrderEnum.ASCENDING)) {criteria.addOrder(Order.asc(sortCriterion));}
            if (sortDirection.equals(SortOrderEnum.DESCENDING)) {criteria.addOrder(Order.desc(sortCriterion));}
        }
        if(page.getSearch()) {
            criteria.add(Restrictions.in("id", page.getSearchIds()));
            page.setTotalNumberOfRows(page.getSearchIds().size());
        }
        else {
            page.setTotalNumberOfRows(getTotalRowNum("select count(*)from User where DTYPE='Experimenter' and account_enabled=1"));
        }
        
        int pageSize = page.getPageSize();
        
        page.setList(getHibernateTemplate().findByCriteria(criteria, index, pageSize));
        
        return page;
    }
    
    public ExtendedPaginatedList getAdmins (final ExtendedPaginatedList page) throws DataAccessException {
        String sortCriterion = page.getSortCriterion();
        int index = page.getFirstRecordIndex();
        SortOrderEnum sortDirection = page.getSortDirection();

        DetachedCriteria ucriteria = DetachedCriteria.forClass(User.class);
        ucriteria.add(Restrictions.eq("enabled", true))
                .setProjection(Projections.groupProperty("id"));
        List<Long>users = getHibernateTemplate().findByCriteria(ucriteria);
        DetachedCriteria criteria = DetachedCriteria.forClass(User.class)
                .createAlias("roles", "role")
                .add(Expression.eq("role.name", "ROLE_ADMIN"))
                .add(Restrictions.in("id",users));
        if (sortCriterion != null) {
            if (sortDirection.equals(SortOrderEnum.ASCENDING)) {criteria.addOrder(Order.asc(sortCriterion));}
            if (sortDirection.equals(SortOrderEnum.DESCENDING)) {criteria.addOrder(Order.desc(sortCriterion));}
        }
        if(page.getSearch()) {
            criteria.add(Restrictions.in("id", page.getSearchIds()));
            page.setTotalNumberOfRows(page.getSearchIds().size());
        }
        else {
            page.setTotalNumberOfRows(getTotalRowNum("select count(*) from User u inner join u.roles as role with role.name='ROLE_ADMIN' and account_enabled=1 order by upper(u.lastName)"));
            //page.setTotalNumberOfRows(getTotalRowNum("select count(*) from User where DTYPE='Experimenter' and roles.name='ROLE_ADMIN' and account_enabled=1"));
        }
        
        int pageSize = page.getPageSize();
        
        page.setList(getHibernateTemplate().findByCriteria(criteria, index, pageSize));
        //page.setTotalNumberOfRows(getTotalRowNum("select count(*)from User u where u.roles.name='ROLE_ADMIN'"));
        //page.setTotalNumberOfRows(getTotalRowNum(criteria));
        
        return page;
    }
    
    public ExtendedPaginatedList getExperimentTypes(final ExtendedPaginatedList page) throws DataAccessException {
        String sortCriterion = page.getSortCriterion();
        int index = page.getFirstRecordIndex();
        SortOrderEnum sortDirection = page.getSortDirection();
        
        DetachedCriteria criteria = DetachedCriteria.forClass(ExperimentType.class);
        if (sortCriterion != null) {
            if (sortDirection.equals(SortOrderEnum.ASCENDING)) { criteria.addOrder(Order.asc(sortCriterion)); }
            if (sortDirection.equals(SortOrderEnum.DESCENDING)) { criteria.addOrder(Order.desc(sortCriterion)); }
        }
        page.setTotalNumberOfRows(getTotalRowNum("select count(*) from ExperimentType"));
        
        int pageSize = page.getPageSize();
        page.setList(getHibernateTemplate().findByCriteria(criteria, index, pageSize));
        
        return page;
    }
    
    public ExtendedPaginatedList getParticipantRecordsByExperiment(final ExtendedPaginatedList page) throws DataAccessException {
        String sortCriterion = page.getSortCriterion();
        int index = page.getFirstRecordIndex();
        SortOrderEnum sortDirection = page.getSortDirection();
        
        DetachedCriteria criteria = DetachedCriteria.forClass(ParticipantRecord.class);
        if(sortCriterion != null) {
            if (sortDirection.equals(SortOrderEnum.ASCENDING)) { criteria.addOrder(Order.asc(sortCriterion)); }
            if (sortDirection.equals(SortOrderEnum.DESCENDING)) { criteria.addOrder(Order.desc(sortCriterion)); }
        }
        page.setTotalNumberOfRows(getTotalRowNum("select count(*) from ParticipantRecord where"));
        return page;
    }
    
    private int getTotalRowNum(final String queryString) throws DataAccessException {
        return ((Long) getHibernateTemplate().find(queryString).get(0)).intValue();
    }
}
