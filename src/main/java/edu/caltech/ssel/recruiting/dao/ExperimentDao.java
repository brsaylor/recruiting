/*
 * Copyright (C) 1998-2009, <a href="http://www.ssel.caltech.edu">SSEL</a>,
 * at Caltech, Pasadena, CA 91125.
 *
 * MooreRecruiting is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 * USA.
 *
 */

package edu.caltech.ssel.recruiting.dao;

import edu.caltech.ssel.recruiting.model.Experiment;
import edu.caltech.ssel.recruiting.model.Filter;
import edu.caltech.ssel.recruiting.model.Survey;
import java.util.Date;
import java.util.List;
import org.appfuse.dao.GenericDao;

/**
* @author <a href="mailto:wmyuan@hss.caltech.edu">Walter M. Yuan</a>, SSEL, Caltech
*/
public interface ExperimentDao extends GenericDao<Experiment, Long> {
    public List<Experiment> getByDate(Date d);
    public List<Experiment> getByDateAndLocationId(Date d, Long l);
    public List<Experiment> getBySurvey(Survey s);
    public List<Experiment> getBySurveyId(Long id);
    public List<Experiment> getByFilter(Filter f);
    public List<Experiment> getByFilterId(Long id);
    public List<Experiment> getByRunby(Long runbyId);
    public List<Experiment> getByReservedBy(Long reservedBy);
    public List<Experiment> getByPrinciple(Long principle);
    public List<Experiment> getByExperimentType(Long expTypeId);
    public List<Experiment> getByLocation(Long locationId);
    public List<Experiment> getById(Long id);
    public Date getFirstExperimentDate();
    public List<Experiment> getByRunbyAndType(Long runbyId, Long expTypeId);
    
    // BRS 2011-10-28
    public Object getCurrentValue(Long id, String fieldName);
}
