/*
 * Copyright (C) 1998-2009, <a href="http://www.ssel.caltech.edu">SSEL</a>,
 * at Caltech, Pasadena, CA 91125.
 *
 * MooreRecruiting is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 * USA.
 *
 */

package edu.caltech.ssel.recruiting.dao.hibernate;

import edu.caltech.ssel.recruiting.dao.SubjectPoolDao;
import edu.caltech.ssel.recruiting.model.SubjectPool;
import java.util.List;
import org.appfuse.dao.hibernate.GenericDaoHibernate;

/**
* @author Michael Kolodrubetz, SSEL, Caltech
* @author Richar Ho, CASSEL, UCLA
* @author <a href="mailto:wmyuan@hss.caltech.edu">Walter M. Yuan</a>, SSEL, Caltech
*/
public class SubjectPoolDaoHibernate extends GenericDaoHibernate<SubjectPool, Long> implements SubjectPoolDao {
    
    public SubjectPoolDaoHibernate() {
        super(SubjectPool.class);
    }
    
    public List<SubjectPool> findByName(String name) {
        return getHibernateTemplate().find("from SubjectPool where name=?", name);
    }
    
    public SubjectPool getSubjectPoolById(Long id) {
        List<SubjectPool> lst = getHibernateTemplate().find("from Subjectpool where id=?", id);
        if(lst.isEmpty())
            return null;
        return lst.get(0);
    }

    public List<SubjectPool> getValidPools() {
        return getHibernateTemplate().find("from SubjectPool where valid=? order by name", true);
    }
}
