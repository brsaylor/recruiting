/*
 * Copyright (C) 1998-2009, <a href="http://www.ssel.caltech.edu">SSEL</a>,
 * at Caltech, Pasadena, CA 91125.
 *
 * MooreRecruiting is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 * USA.
 *
 */

package edu.caltech.ssel.recruiting.dao;

import edu.caltech.ssel.recruiting.model.Experiment;
import edu.caltech.ssel.recruiting.model.ParticipantRecord;
import edu.caltech.ssel.recruiting.model.Subject;
import java.util.List;
import org.appfuse.dao.GenericDao;

/**
*
* @author <a href="mailto:wmyuan@hss.caltech.edu">Walter M. Yuan</a>, SSEL, Caltech
*/
public interface ParticipantRecordDao extends GenericDao<ParticipantRecord, Long> {
    public List<ParticipantRecord> getByExperiment(Experiment e);
    public float getPayoffByExperiment(Experiment e);
    public float getPayoffByExperiments(List<Long> experimentIds);
    public int getParticipatedByExperiment(Experiment e);
    public int getParticipatedByExperiments(List<Long> experimentIds);
    public int getSignupByExperiments(List<Long> experimentIds);
    public List<ParticipantRecord> getBySubject(Subject s);
    public ParticipantRecord getByExperimentAndSubject(Long expId, Long subId);
    public List<ParticipantRecord> getByPlayerNum(Integer pn);
    public boolean isSignedUp(Subject s, Experiment e);
    public int getPastCountBySubjectAndStatus(Subject s, ParticipantRecord.Status status); // BRS
}
