/*
 * Copyright (C) 1998-2009, <a href="http://www.ssel.caltech.edu">SSEL</a>,
 * at Caltech, Pasadena, CA 91125.
 *
 * MooreRecruiting is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 * USA.
 *
 */


package edu.caltech.ssel.recruiting.dao.hibernate;

import edu.caltech.ssel.recruiting.dao.ExtendedUserDao;
import edu.caltech.ssel.recruiting.model.Experimenter;
import edu.caltech.ssel.recruiting.model.Subject;
import java.util.List;
import java.util.Set;
import org.appfuse.dao.hibernate.UserDaoHibernate;
import org.appfuse.model.User;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Expression;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;


/**
* @author Michael Kolodrubetz, SSEL, Caltech
* @author <a href="mailto:wmyuan@hss.caltech.edu">Walter M. Yuan</a>, SSEL, Caltech
*/
public class ExtendedUserDaoHibernate extends UserDaoHibernate implements ExtendedUserDao {
    
    /** Creates a new instance of ExtendedUserDaoHibernate */
    public ExtendedUserDaoHibernate() {
    }

    // BRS 2011-02-03
    // Get the password for the given user directly from the database, bypassing
    // the cache.
    public String getCurrentPasswordById(Long id) {
        List<String> lst = getSession().createSQLQuery("select password from app_user where id=" + id).list();
        if(lst.isEmpty())
            return null;
        return lst.get(0);
    }

    public List<Experimenter> getExperimenters() {
        return getHibernateTemplate().find("from User u where DTYPE='Experimenter' and account_enabled=1 order by upper(u.lastName)");
    }

    public List<String> getExperimenterEmails() {
        return getHibernateTemplate().find("select email from User u where DTYPE='Experimenter' and account_enabled=1 order by upper(u.lastName)");
    }
    
    public Experimenter getExperimenterById(Long id) {
        List<Experimenter> lst = getHibernateTemplate().find("from User u where DTYPE='Experimenter' and u.id=?", id);
        if(lst.isEmpty())
            return null;
        return lst.get(0);
    }

    public List<Subject> getSubjects() {
        return getHibernateTemplate().find("from User u where DTYPE='Subject' and account_enabled=1 order by upper(u.lastName)");
    }
    
    public List<Subject> getSubjectsByNotInIdSet(List<Long> ids) {
        return getHibernateTemplate().find("from User u where u.id not in (?)",ids);
    }
    
    public List<String> getSubjectEmailsByIdSet(Set<Long> ids) {
        //return getHibernateTemplate().find("select email from User u where u.id not in (?)",ids);
        DetachedCriteria criteria = DetachedCriteria.forClass(Subject.class);
        criteria.setProjection(Projections.projectionList().add(Projections.property("email")));
        criteria.add(Restrictions.in("id", ids));
        criteria.add(Restrictions.eq("enabled", true));
        return getHibernateTemplate().findByCriteria(criteria);
    }

    public List<String> getSubjectEmails() {
        // BRS 2011-02-15 exclude unsubscribed subjects
        return getHibernateTemplate().find("select email from User u where DTYPE='Subject' and account_enabled=1 and subscribed=1 order by upper(u.lastName)");
    }
    
    public Subject getSubjectById(Long id) {
        List<Subject> lst = getHibernateTemplate().find("from User u where DTYPE='Subject' and u.id=?", id);
        if(lst.isEmpty())
            return null;
        return lst.get(0);
    }
    
    public Long getSubjectIDByParticipationID(Long id) {
        List<Long> lst = getHibernateTemplate().find("select u.id from User u, ParticipantRecord pr where DTYPE='Subject' and u.id=pr.subject.id and pr.id=?",id);
        if(lst.isEmpty())
            return null;
        return lst.get(0);
    }
    
    public List<Subject> getSubjectBySchoolID(String sid) {
        return getHibernateTemplate().find("from User u where DTYPE='Subject' and schoolid=?",sid);
    }
    
    public Subject getSubjectByEmail(String email) {
        // BRS 2011-11-01: only include Subjects
        List<Subject> lst = getHibernateTemplate().find("from User u where DTYPE='Subject' and email=?",email);
        if(lst.isEmpty())
            return null;
        return lst.get(0);
    }

    // BRS 2011-11-01
    public User getNonSubjectByEmail(String email) {
        List<User> lst = getHibernateTemplate().find("from User u where DTYPE != 'Subject' and email=?",email);
        if(lst.isEmpty())
            return null;
        return lst.get(0);
    }

    public List<User> getStandardUsers() {
        return getHibernateTemplate().find("from User u where DTYPE='User' order by upper(u.lastName)");
    }

    public List<User> getEnabledUsers() {
        return getHibernateTemplate().find("from User u where account_enabled=1 order by upper(u.lastName)");
    }

    public List<String> getEnabledUserEmails() {
        return getHibernateTemplate().find("select email from User u where account_enabled=1 order by upper(u.lastName)");
    }

    public List<User> getAdmins() {
        return getHibernateTemplate().find("select distinct u from User u inner join u.roles as role with role.name='ROLE_ADMIN' and account_enabled=1 order by upper(u.lastName)");
        //return getHibernateTemplate().find("from User u where u.roles.name='ROLE_ADMIN' and account_enabled=1 order by upper(u.lastName)");
        //return getStandardUsers();
    }
    
}
