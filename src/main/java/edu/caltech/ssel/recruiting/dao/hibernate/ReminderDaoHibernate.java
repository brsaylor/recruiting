/*
 * Copyright (C) 1998-2009, <a href="http://www.ssel.caltech.edu">SSEL</a>,
 * at Caltech, Pasadena, CA 91125.
 *
 * MooreRecruiting is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 * USA.
 *
 */

package edu.caltech.ssel.recruiting.dao.hibernate;

import edu.caltech.ssel.recruiting.dao.ReminderDao;
import edu.caltech.ssel.recruiting.model.Reminder;

import org.appfuse.dao.hibernate.GenericDaoHibernate;

import java.util.Date;
import java.util.List;

/**
* @author Michael Kolodrubetz, SSEL, Caltech
* @author <a href="mailto:wmyuan@hss.caltech.edu">Walter M. Yuan</a>, SSEL, Caltech
*/
public class ReminderDaoHibernate extends GenericDaoHibernate<Reminder, Long> implements ReminderDao {
    
    public ReminderDaoHibernate() {
        super(Reminder.class);
    }
    
    public List<Reminder> findRemindersToProcess() {

        /*
        return getHibernateTemplate().find(
                "from Reminder where whenDate > ? and status = ?",
                new Object[]{new Date(), Reminder.Status.UNPROCESSED});
        */

        // BRS: instead of returning all future unprocessed reminders, return
        // all past unprocessed reminders.
        return getHibernateTemplate().find(
                "from Reminder where whenDate <= ? and status = ?",
                new Object[]{new Date(), Reminder.Status.UNPROCESSED});

    }
    public List<Reminder> findUnprocessedRemindersByUserId(Long id) {
        return getHibernateTemplate().find(
                "from Reminder where user_id = ? and status = ?",
                new Object[]{id, Reminder.Status.UNPROCESSED});
    }
    public List<Reminder> findUnprocessedExperimentRemindersByExperimentId(Long id) {
        return getHibernateTemplate().find(
                "from Reminder where reminderType = ? and ref_id = ? and status = ?",
                new Object[]{Reminder.ReminderType.EXPERIMENT, id, Reminder.Status.UNPROCESSED});
    }
}
