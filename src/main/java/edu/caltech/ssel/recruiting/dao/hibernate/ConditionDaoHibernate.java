/*
 * Copyright (C) 1998-2009, <a href="http://www.ssel.caltech.edu">SSEL</a>,
 * at Caltech, Pasadena, CA 91125.
 *
 * MooreRecruiting is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 * USA.
 *
 */
package edu.caltech.ssel.recruiting.dao.hibernate;

import edu.caltech.ssel.recruiting.dao.ConditionDao;
import edu.caltech.ssel.recruiting.model.Condition;
import org.appfuse.dao.hibernate.GenericDaoHibernate;

import java.lang.StringBuilder;
import java.util.ArrayList;
import java.sql.Date;
import java.util.Set;
import java.util.HashSet;
import java.util.List;
import java.util.GregorianCalendar;
import edu.caltech.ssel.recruiting.model.ParticipantRecord;
import edu.caltech.ssel.recruiting.model.ParticipantRecord.Status;
import edu.caltech.ssel.recruiting.model.Experiment;
import edu.caltech.ssel.recruiting.model.BooleanCondition;
import edu.caltech.ssel.recruiting.model.ExperimentType;
import edu.caltech.ssel.recruiting.model.ExperimentCondition;
import edu.caltech.ssel.recruiting.model.ExperimentTypeCondition;
import edu.caltech.ssel.recruiting.model.NoShowCondition;
import edu.caltech.ssel.recruiting.model.PoolCondition;
import edu.caltech.ssel.recruiting.model.QuestionResultCondition;
import edu.caltech.ssel.recruiting.model.Subject;
import edu.caltech.ssel.recruiting.model.SubjectPool;

/**
* @author Michael Kolodrubetz, SSEL, Caltech
* @author <a href="mailto:wmyuan@hss.caltech.edu">Walter M. Yuan</a>, SSEL, Caltech
*/
public class ConditionDaoHibernate extends GenericDaoHibernate<Condition, Long> implements ConditionDao {
    public ConditionDaoHibernate() {
        super(Condition.class);
    }

    public List<ExperimentTypeCondition> getExperimentTypeConditionByName(String name) {
        return getHibernateTemplate().find("from ExperimentTypeCondition etc where etc.name=?", name);
    }

    // BRS 2012-02-09
    // Return a set of subject IDs of subjects who are unsubscribed.
    public Set<Long> getUnsubscribedSubjectIDs() {
        return new HashSet(getHibernateTemplate().find("select s.id from Subject s where s.subscribed=0"));
    }

    // BRS 2011-08-22
    // Return a set of subject IDs of subjects who are eligible based on the given condition.
    // Includes all subjects with enabled accounts, regardless of subscription status.
    
    public Set<Long> getEligibleSubjectIDs(Condition cond) {
        String t = cond.getConditionType();
        if (t.equals("BooleanCondition")) {
            return getEligibleSubjectIDs((BooleanCondition) cond);
        } else if (t.equals("ExperimentCondition")) {
            return getEligibleSubjectIDs((ExperimentCondition) cond);
        } else if (t.equals("ExperimentTypeCondition")) {
            return getEligibleSubjectIDs((ExperimentTypeCondition) cond);
        } else if (t.equals("NoShowCondition")) {
            return getEligibleSubjectIDs((NoShowCondition) cond);
        } else if (t.equals("PoolCondition")) {
            return getEligibleSubjectIDs((PoolCondition) cond);
        } else if (t.equals("QuestionResultCondition")) {
            return getEligibleSubjectIDs((QuestionResultCondition) cond);
        } else {
            System.out.println("getEligibleSubjectIDs() called with unknown condition type");
            return new HashSet();
        }
    }

    public Set<Long> getEligibleSubjectIDs(BooleanCondition cond) {
        System.out.println("getEligibleSubjectIDs(BooleanCondition cond)");
        Set set1 = getEligibleSubjectIDs(get(cond.getFirstCondition()));
        Set set2 = getEligibleSubjectIDs(get(cond.getSecondCondition()));
        if (cond.isAnd()) {
            // Subjects must be eligible for both conditions; return the intersection of the sets.
            set1.retainAll(set2);
        } else {
            // Subjects must be eligible for one or both conditions; return the union of the sets.
            set1.addAll(set2);
        }
        return set1;
    }

    public Set<Long> getEligibleSubjectIDs(ExperimentTypeCondition cond) {
        System.out.println("getEligibleSubjectIDs(ExperimentTypeCondition cond)");

        // Build a comma-separated string of ExperimentType ids
        StringBuilder s = new StringBuilder();
        List<ExperimentType> types = cond.getTypes();
        for (int i = 0; i < types.size(); i++) {
            if (i > 0) {
                s.append(",");
            }
            s.append(types.get(i).getId().toString());
        }
        String typeIdString = s.toString();
        System.out.println("typeIdString = " + typeIdString);

        return new HashSet(getHibernateTemplate().find(
                    "select s.id\n" +
                    "from Subject s\n" +
                    "where s.enabled=1 and s.id " + (cond.isOnList() ? "" : "not") + " in\n" +
                    "  (select p.subject.id from ParticipantRecord p where\n"
                            + (cond.isHistory()
                                ? "p.participated=1"
                                : "(p.exp.expDate > current_date() OR (p.exp.expDate = current_date() AND p.exp.startTime > current_time()))\n")
                            + " and p.exp.type.id in (" + typeIdString + "))"));
    }

    // If the experiment is in the future, returns all subject IDs, because
    // subjects can still sign up and so are potentially eligible.
    // If the experiment is in the past, return the IDs of only those subjects
    // who (did not) participate.
    public Set<Long> getEligibleSubjectIDs(ExperimentCondition cond) {
        System.out.println("getEligibleSubjectIDs(ExperimentCondition cond)");

        boolean history = cond.getExperiment().getStartDateTime().compareTo(new GregorianCalendar()) < 0;
        if (history) {
            return new HashSet(getHibernateTemplate().find(
                        "select s.id from Subject s where s.enabled=1 and s.id " + (cond.isOnList() ? "" : "not") + " in\n" +
                        // All subjects who participated in the experiment
                        "  (select p.subject.id from ParticipantRecord p where p.exp.id = ? and p.status = ?)",
                        new Object[] {
                            cond.getExperiment().getId(),
                            Status.STATUS_2 // "Participated" status
                        }));
        }

        return new HashSet(getHibernateTemplate().find("select s.id from Subject s where s.enabled=1"));
    }

    public Set<Long> getEligibleSubjectIDs(NoShowCondition cond) {
        System.out.println("getEligibleSubjectIDs(NoShowCondition cond)");
        return new HashSet(getHibernateTemplate().find(
                    "select s.id from Subject s where s.enabled=1 and s.id not in " +

                    // Subjects with more than N no-shows
                    "  (select p.subject.id from ParticipantRecord p " +
                    "   where p.status=? and (p.exp.expDate < current_date() OR (p.exp.expDate = current_date() AND p.exp.startTime < current_time())) " +
                    "   group by p.subject.id " +
                    "   having count(*) > ?)",
                    new Object[] {
                        Status.STATUS_3, // "Sign-up" status, which when in the past indicates a no-show
                        new Long(cond.getMaxNoShows())
                    }));
    }

    public Set<Long> getEligibleSubjectIDs(PoolCondition cond) {
        System.out.println("getEligibleSubjectIDs(PoolCondition cond)");

        // Build a comma-separated string of Pool ids
        StringBuilder s = new StringBuilder();
        List<SubjectPool> pools = cond.getPools();
        for (int i = 0; i < pools.size(); i++) {
            if (i > 0) {
                s.append(",");
            }
            s.append(pools.get(i).getId().toString());
        }
        String poolIdString = s.toString();
        System.out.println("poolIdString = " + poolIdString);

        return new HashSet(getHibernateTemplate().find(
                    "select s.id " +
                    "from Subject s " +
                    "where s.enabled=1 and s.id " + (cond.isMemberOf() ? "" : "not") + " in " +
                    "  (select s.id from Subject s left join s.pools as p where p.id in (" + poolIdString + "))"));

    }

    // FIXME? just returns all subjectIDs.
    // This may be appropriate, because in general the eligibility counting is
    // done during experiment setup, and at that point no subjects have answered
    // any questions (except profile questions, which we are not using).
    public Set<Long> getEligibleSubjectIDs(QuestionResultCondition cond) {
        System.out.println("getEligibleSubjectIDs(QuestionResultCondition cond)");
        return new HashSet(getHibernateTemplate().find("select s.id from Subject s where s.enabled=1"));
    }


    // BRS 2011-08-22: Don't use the following methods, which are incorrect and cause crashing.  The above methods replace them.
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public Set<Long> getEligibleSubjectByETC(Long etcID) {

        return new HashSet(getHibernateTemplate().find("select pr.subject.id " +
                "from ParticipantRecord pr,ExperimentTypeCondition etc, Experiment e " +
                "where pr.exp.id=e.id AND e.type=etc.types.id AND pr.participated=1 AND etc.id=?",etcID));
    }
    
    public Set<Long> getEligibleSubjectByNotETC(Long etcID) {
        // BRS 2011-08-05
        return new HashSet(getHibernateTemplate().find(
                "select s.id " +
                "from Subject s " +
                "where s.id not in " +
                "  (select p.subject.id from ParticipantRecord p where p.participated=1 and p.exp.type.id in " +
                "    (select indices(c.types) from ExperimentTypeCondition c where c.id=?))", etcID));


        /*
        return new HashSet(getHibernateTemplate().find("select s.id " +
                "from Subject s " +
                "where s.id not in " +
                "( select pr.subject.id " +
                "from ParticipantRecord pr, ExperimentTypeCondition etc, Experiment e " +
                "where pr.exp.id=e.id AND e.type=etc.types.id AND pr.participated=1 AND etc.id=?)",etcID));
        */
    }
    
    public Set<Long> getEligibleSubjectByFutureETC(Long etcID, Date date) {
        return new HashSet(getHibernateTemplate().find("select pr.subject.id " +
                "from ParticipantRecord pr, ExperimentTypeCondition etc, Experiment e " +
                "where pr.exp.id=e.id AND e.type=etc.types.id AND e.expDate>? and etc.id=?)", new Object[]{date,etcID}));
    }
    
    public Set<Long> getEligibleSubjectByNotFutureETC(Long etcID, Date date) {
        return new HashSet(getHibernateTemplate().find("select s.id " +
                "from Subject s " +
                "where s.id not in " +
                "( select pr.subject.id " +
                "from ParticipantRecord pr, ExperimentTypeCondition etc, Experiment e " +
                "where pr.exp.id=e.id AND e.type=etc.types.id AND e.expDate > ? and etc.id=?)", new Object[]{date,etcID}));
    }


}
