/*
 * Copyright (C) 1998-2009, <a href="http://www.ssel.caltech.edu">SSEL</a>,
 * at Caltech, Pasadena, CA 91125. 
 *
 * MooreRecruiting is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 * USA.
 *
 */

package edu.caltech.ssel.recruiting.service.impl;

import edu.caltech.ssel.recruiting.model.Question;
import edu.caltech.ssel.recruiting.model.Question.QStyle;
import edu.caltech.ssel.recruiting.service.QuestionManager;
import java.util.Arrays;
import org.appfuse.dao.GenericDao;
import org.appfuse.service.impl.BaseManagerMockTestCase;

import org.jmock.Expectations;
import org.junit.Before;
import org.junit.After;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author michaelk
 */
public class QuestionManagerImplTest extends BaseManagerMockTestCase {
    
    private QuestionManager manager = null;
    private GenericDao dao = null;

    @Before
    public void setUp() throws Exception {
        dao = context.mock(GenericDao.class);
        manager = new QuestionManagerImpl(dao);
    }

    @After
    public void tearDown() throws Exception {
        manager = null;
    }

    @Test
    public void testGetByIdString() throws Exception {
        final Question q = new Question();
        //dao.expects(once()).method("get").with(eq(new Long(1))).will(returnValue(q));
        context.checking(new Expectations() {{
            one(dao).get(with(equal(new Long(1))));
            will(returnValue(q));
        }});

        assertEquals(q, manager.getByIdString("1"));
    }

    @Test
    public void testSaveQuestion() {
        final Question q = new Question();
        q.setLabel("label");
        q.setQuestion("question");
        q.setStyle(QStyle.CHECK_BOX);
        String [] arr = new String[1];
        arr[0] = "opt";
        q.setOptionList(Arrays.asList(arr));
        
        //dao.expects(once()).method("save").with(eq(q)).will(returnValue(q));
        context.checking(new Expectations() {{
            one(dao).save(with(equal(q)));
            will(returnValue(q));
        }});

        assertEquals(q, manager.saveQuestion(q.getLabel(), q.getQuestion(), q.getStyle().name(), arr));
    }    
    
}
