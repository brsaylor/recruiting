/*
 * Copyright (C) 1998-2009, <a href="http://www.ssel.caltech.edu">SSEL</a>,
 * at Caltech, Pasadena, CA 91125. 
 *
 * MooreRecruiting is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 * USA.
 *
 */

package edu.caltech.ssel.recruiting.service.impl;

import edu.caltech.ssel.recruiting.dao.ExperimentTypeDao;
import edu.caltech.ssel.recruiting.model.ExperimentType;
import edu.caltech.ssel.recruiting.service.ExperimentTypeManager;
import java.util.ArrayList;
import java.util.List;
import org.appfuse.service.impl.BaseManagerMockTestCase;

import org.jmock.Expectations;
import org.junit.Before;
import org.junit.After;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Administrator
 */
public class ExperimentTypeManagerImplTest extends BaseManagerMockTestCase {
    private ExperimentTypeManager manager = null;
    private ExperimentTypeDao dao = null;
    private ExperimentType expType = null;

    @Before
    public void setUp() throws Exception {
        dao = context.mock(ExperimentTypeDao.class);
        manager = new ExperimentTypeManagerImpl(dao);
    }

    @After
    public void tearDown() throws Exception {
        manager = null;
    }

    @Test
    public void testGetExperimentType(){
        log.debug("testing getExperimentType");
        
        final Long id = 7L;
        expType = new ExperimentType();
        
        //Set expected behavior on dao
        //dao.expects(once()).method("get").with(eq(id)).will(returnValue(expType));
        context.checking(new Expectations() {{
            one(dao).get(with(equal(id)));
            will(returnValue(expType));
        }});

        
        ExperimentType result = manager.get(id);
        assertSame(expType, result);
    }

    @Test
    public void testGetExperimentTypes() {
        log.debug("testing GetExperimentTypes");

        final List types = new ArrayList();

        //set expected behavior on dao
        //dao.expects(once()).method("getAll").will(returnValue(types));
        context.checking(new Expectations() {{
            one(dao).getAll();
            will(returnValue(types));
        }});

        List result = manager.getAll();
        assertSame(types, result);
    }
    
    @Test
    public void testSaveExperimentType() {
        log.debug("testing saveExperimentType");
        expType =  new ExperimentType();

        //dao.expects(once()).method("save").with(same(expType)).will(returnValue(expType));
        context.checking(new Expectations() {{
            one(dao).save(with(same(expType)));
            will(returnValue(expType));
        }});


        manager.save(expType);
    }

    @Test
    public void testRemoveExperimentType() {
        log.debug("testing removeExperimentType");
        final Long id = 11L;
        expType = new ExperimentType();

        //dao.expects(once()).method("remove").with(eq(id)).isVoid();
        context.checking(new Expectations() {{
            one(dao).remove(with(equal(id)));
        }});

        manager.remove(id);
    }
}
