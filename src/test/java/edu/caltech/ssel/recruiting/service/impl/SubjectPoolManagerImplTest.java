/*
 * Copyright (C) 1998-2009, <a href="http://www.ssel.caltech.edu">SSEL</a>,
 * at Caltech, Pasadena, CA 91125. 
 *
 * MooreRecruiting is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 * USA.
 *
 */

package edu.caltech.ssel.recruiting.service.impl;

import edu.caltech.ssel.recruiting.dao.SubjectPoolDao;
import edu.caltech.ssel.recruiting.model.SubjectPool;
import java.util.ArrayList;
import java.util.List;
import org.appfuse.service.impl.BaseManagerMockTestCase;

import org.jmock.Expectations;
import org.junit.Before;
import org.junit.After;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Administrator
 */
public class SubjectPoolManagerImplTest extends BaseManagerMockTestCase {
    private SubjectPoolManagerImpl manager = null;
    private SubjectPoolDao dao = null;
    private SubjectPool pool = null;

    @Before
    public void setUp() throws Exception {
        dao = context.mock(SubjectPoolDao.class);
        manager = new SubjectPoolManagerImpl(dao);
    }

    @After
    public void tearDown() throws Exception {
        manager = null;
    }

    @Test
    public void testGetSubjectPool(){
        log.debug("testing getSubjectPool");
        
        final Long id = 7L;
        pool = new SubjectPool();
        
        //Set expected behavior on dao
        //dao.expects(once()).method("get").with(eq(id)).will(returnValue(pool));
        context.checking(new Expectations() {{
            one(dao).get(with(equal(id)));
            will(returnValue(pool));
        }});

        SubjectPool result = manager.get(id);
        assertSame(pool, result);
    }

    @Test
    public void testGetSubjectPools() {
        log.debug("testing GetSubjectPools");
        
        final List pools = new ArrayList();
        
        //set expected behavior on dao
        //dao.expects(once()).method("getAll").will(returnValue(pools));
        context.checking(new Expectations() {{
            one(dao).getAll();
            will(returnValue(pools));
        }});

        List result = manager.getAll();
        assertSame(pools, result);
    }

    @Test
    public void testFindByName() {
        log.debug("testing findByName");
        
        final List pools = new ArrayList();
        final String lastName = "Smith";
        
        //set dao
        //dao.expects(once()).method("findByName").with(eq(lastName)).will(returnValue(pools));
        context.checking(new Expectations() {{
            one(dao).findByName(with(equal(lastName)));
            will(returnValue(pools));
        }});
        
        List result = manager.findByName(lastName);
        assertSame(pools, result);
    }

    @Test
    public void testSaveSubjectPool() {
        log.debug("testing saveSubjectPool");
        pool =  new SubjectPool();
        
        //dao.expects(once()).method("save").with(same(pool)).will(returnValue(pool));
        context.checking(new Expectations() {{
            one(dao).save(with(same(pool)));
            will(returnValue(pool));
        }});
        
        manager.save(pool);
    }

    @Test
    public void testRemoveSubjectPool() {
        log.debug("testing removeSubjectPool");
        final Long id = 11L;
        pool = new SubjectPool();
        
        //dao.expects(once()).method("remove").with(eq(id)).isVoid();
        context.checking(new Expectations() {{
            one(dao).remove(with(equal(id)));
        }});
        
        manager.remove(id);
    }
}
