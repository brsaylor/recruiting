/*
 * Copyright (C) 1998-2009, <a href="http://www.ssel.caltech.edu">SSEL</a>,
 * at Caltech, Pasadena, CA 91125. 
 *
 * MooreRecruiting is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 * USA.
 *
 */

package edu.caltech.ssel.recruiting.service.impl;

import edu.caltech.ssel.recruiting.dao.SurveyDao;
import edu.caltech.ssel.recruiting.model.Question;
import edu.caltech.ssel.recruiting.model.Survey;
import edu.caltech.ssel.recruiting.service.SurveyManager;
import java.util.ArrayList;
import java.util.List;
import org.appfuse.dao.GenericDao;
import org.appfuse.service.impl.BaseManagerMockTestCase;

import org.jmock.Expectations;
import org.junit.Before;
import org.junit.After;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author michaelk
 */
public class SurveyManagerImplTest extends BaseManagerMockTestCase {

    private SurveyDao dao;
    private GenericDao qdao;
    private SurveyManager manager;

    @Before
    public void setUp() {
        dao = context.mock(SurveyDao.class);
        qdao = context.mock(GenericDao.class);
        manager = new SurveyManagerImpl(dao);
        manager.setQuestionDao(qdao);
    }

    @After
    public void tearDown() {
        manager = null;
    }        

    @Test
    public void testGetProfileSurvey() throws Exception {
        final Survey s = new Survey();
        //dao.expects(once()).method("getProfileSurvey").will(returnValue(s));
        context.checking(new Expectations() {{
            one(dao).getProfileSurvey();
            will(returnValue(s));
        }});
        
        assertEquals(manager.getProfileSurvey(), s);
    }

    @Test
    public void testGetByIdString() throws Exception {
        final Survey s = new Survey();
        //dao.expects(once()).method("get").with(eq(new Long(1))).will(returnValue(s));
        context.checking(new Expectations() {{
            one(dao).get(with(equal(new Long(1))));
            will(returnValue(s));
        }});
        
        assertEquals(s, manager.getByIdString("1"));
    }

    @Test
    public void testAddQuestionToSurvey() throws Exception {
        final Survey s = new Survey();
        final Question q = new Question();
        q.setId(2L);
        List<Question> qlst= new ArrayList();
        s.setQuestions(qlst);

        //dao.expects(once()).method("get").with(eq(new Long(1))).will(returnValue(s));
        context.checking(new Expectations() {{
            one(dao).get(with(equal(new Long(1))));
            will(returnValue(s));
        }});
        //qdao.expects(once()).method("get").with(eq(new Long(4))).will(returnValue(q));
        context.checking(new Expectations() {{
            one(qdao).get(with(equal(new Long(4))));
            will(returnValue(q));
        }});
        //dao.expects(once()).method("save").with(eq(s)).will(returnValue(s));
        context.checking(new Expectations() {{
            one(dao).save(with(equal(s)));
            will(returnValue(s));
        }});

        final Survey s2 = manager.addQuestionToSurvey("1", "4", "1");
        assertNotNull(s2.getQuestions());
        assertEquals(s2.getQuestions().size(),1);
        
        //dao.expects(once()).method("get").with(eq(new Long(1))).will(returnValue(s));
        context.checking(new Expectations() {{
            one(dao).get(with(equal(new Long(1))));
            will(returnValue(s2));
        }});
        //dao.expects(once()).method("save").with(eq(s)).will(returnValue(s));
        context.checking(new Expectations() {{
            one(dao).save(with(equal(s2)));
            will(returnValue(s2));
        }});
        Survey s3 = manager.removeQuestionFromSurvey("1", "0");
        assertEquals(s3.getQuestions().size(),0);
    }

    @Test
    public void testRemoveQuestionFromSurvey() throws Exception {
        final Survey s = new Survey();
        final Question q = new Question();
        q.setId(2L);
        List<Question> qlst= new ArrayList();
        s.setQuestions(qlst);

        //dao.expects(once()).method("get").with(eq(new Long(1))).will(returnValue(s));
        context.checking(new Expectations() {{
            one(dao).get(with(equal(new Long(1))));
            will(returnValue(s));
        }});
        //qdao.expects(once()).method("get").with(eq(new Long(4))).will(returnValue(q));
        context.checking(new Expectations() {{
            one(qdao).get(with(equal(new Long(4))));
            will(returnValue(q));
        }});
        //dao.expects(once()).method("save").with(eq(s)).will(returnValue(s));
        context.checking(new Expectations() {{
            one(dao).save(with(equal(s)));
            will(returnValue(s));
        }});
        
        final Survey s2 = manager.addQuestionToSurvey("1", "4", "1");

        //dao.expects(once()).method("get").with(eq(new Long(1))).will(returnValue(s));
        context.checking(new Expectations() {{
            one(dao).get(with(equal(new Long(1))));
            will(returnValue(s2));
        }});
        //dao.expects(once()).method("save").with(eq(s)).will(returnValue(s));
        context.checking(new Expectations() {{
            one(dao).save(with(equal(s2)));
            will(returnValue(s2));
        }});
        Survey s3 = manager.removeQuestionFromSurvey("1", "0");
        assertEquals(s3.getQuestions().size(),0);
    }
}
