/*
 * Copyright (C) 1998-2009, <a href="http://www.ssel.caltech.edu">SSEL</a>,
 * at Caltech, Pasadena, CA 91125. 
 *
 * MooreRecruiting is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 * USA.
 *
 */

package edu.caltech.ssel.recruiting.service.impl;

import edu.caltech.ssel.recruiting.dao.QuestionResultDao;
import edu.caltech.ssel.recruiting.model.Experiment;
import edu.caltech.ssel.recruiting.model.Question;
import edu.caltech.ssel.recruiting.model.QuestionResult;
import edu.caltech.ssel.recruiting.service.QuestionResultManager;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.appfuse.model.User;
import org.appfuse.service.impl.BaseManagerMockTestCase;

import org.jmock.Expectations;
import org.junit.Before;
import org.junit.After;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Administrator
 */
public class QuestionResultManagerImplTest extends BaseManagerMockTestCase {
    private final Log log = LogFactory.getLog(QuestionResultManagerImplTest.class);
    private QuestionResultManager manager;
    private QuestionResultDao dao;
    private User u;
    private Question q;
    private Experiment e;
    private QuestionResult qr;
    private List<QuestionResult> lst;

    @Before
    public void setUp() throws Exception {
        dao = context.mock(QuestionResultDao.class);
        manager = new QuestionResultManagerImpl(dao);
        
        u = new User();
        u.setId(1L);
        u.setUsername("user");
        
        q = new Question();
        q.setId(1L);
        q.setLabel("Question");
        
        e = new Experiment();
        e.setId(1L);
        e.setInstruction("take experiment");

        qr = new QuestionResult();
        qr.setAnswerIdx(1);
        qr.setQuestion(q);
        qr.setSurveyTaker(u);
        qr.setSurveyParent(e);
        qr.setTakenDate(new Date());
        
        List<QuestionResult> lst = new ArrayList();
        lst.add(qr);
    }

    @After
    public void tearDown() throws Exception {
        manager = null;
    }

    @Test
    public void testGetAnswersByUser() {
        log.debug("Entering testGetAnswersByUser...");

        //dao.expects(once()).method("getAnswersByUser").with(eq(u)).will(returnValue(lst));
        context.checking(new Expectations() {{
            one(dao).getAnswersByUser(with(equal(u)));
            will(returnValue(lst));
        }});

        List<QuestionResult> result = manager.getAnswersByUser(u);
        assertSame(lst, result);
    }

    @Test
    public void testGetAnswersByExperiment() {        
        log.debug("Entering testGetAnswersByExperiment...");
        
        //dao.expects(once()).method("getAnswersByExperiment").with(eq(e)).will(returnValue(lst));
        context.checking(new Expectations() {{
            one(dao).getAnswersByExperiment(with(equal(e)));
            will(returnValue(lst));
        }});

        List<QuestionResult> result = manager.getAnswersByExperiment(e);
        assertSame(lst, result);
    }

    @Test
    public void testGetMostRecentAnswer() {
        log.debug("Entering testGetMostRecentAnswer...");
 
        //dao.expects(once()).method("getMostRecentAnswer").with(eq(u), eq(q)).will(returnValue(qr));
        context.checking(new Expectations() {{
            one(dao).getMostRecentAnswer(with(equal(u)), with(equal(q)));
            will(returnValue(qr));
        }});

        QuestionResult result = manager.getMostRecentAnswer(u, q);
        assertSame(qr, result);
     }


}
