/*
 * Copyright (C) 1998-2009, <a href="http://www.ssel.caltech.edu">SSEL</a>,
 * at Caltech, Pasadena, CA 91125. 
 *
 * MooreRecruiting is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 * USA.
 *
 */

package edu.caltech.ssel.recruiting.service.impl;

import edu.caltech.ssel.recruiting.dao.ExtendedUserDao;
import edu.caltech.ssel.recruiting.model.Experimenter;
import edu.caltech.ssel.recruiting.service.ExtendedUserManager;
import java.util.ArrayList;
import java.util.List;
import org.appfuse.service.impl.BaseManagerMockTestCase;

import org.jmock.Expectations;
import org.junit.Before;
import org.junit.After;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author michaelk
 */
public class ExtendedUserManagerImplTest extends BaseManagerMockTestCase {
    
    private ExtendedUserManager manager = null;
    private ExtendedUserDao dao = null;

    @Before
    public void setUp() throws Exception {
        dao = context.mock(ExtendedUserDao.class);
        manager = new ExtendedUserManagerImpl(dao);
    }

    @After
    public void tearDown() throws Exception {
        manager = null;
    }

    @Test
    public void testGetExperimenters() {
        final List<Experimenter> lst = new ArrayList(1);
        lst.add(new Experimenter());
        //dao.expects(once()).method("getExperimenters").withNoArguments().will(returnValue(lst));

        context.checking(new Expectations() {{
            one(dao).getExperimenters();
            will(returnValue(lst));
        }});
        
        assertNotNull(manager.getExperimenters());
    }
    
    @Test
    public void testGetSubjects() {
        final List<Experimenter> lst = new ArrayList(1);
        lst.add(new Experimenter());
        //dao.expects(once()).method("getSubjects").withNoArguments().will(returnValue(lst));
        context.checking(new Expectations() {{
            one(dao).getSubjects();
            will(returnValue(lst));
        }});
        
        assertNotNull(manager.getSubjects());
    }
    
    @Test
    public void testGetAdmins() {
        final List<Experimenter> lst = new ArrayList(1);
        lst.add(new Experimenter());
        //dao.expects(once()).method("getAdmins").withNoArguments().will(returnValue(lst));
        context.checking(new Expectations() {{
            one(dao).getAdmins();
            will(returnValue(lst));
        }});
        
        assertNotNull(manager.getAdmins());
    }
    
    @Test
    public void testGetStandardUsers() {
        final List<Experimenter> lst = new ArrayList(1);
        lst.add(new Experimenter());
        //dao.expects(once()).method("getStandardUsers").withNoArguments().will(returnValue(lst));
        context.checking(new Expectations() {{
            one(dao).getStandardUsers();
            will(returnValue(lst));
        }});
        
        assertNotNull(manager.getStandardUsers());
    }
    
    @Test
    public void testGetEnabledUsers() {
        final List<Experimenter> lst = new ArrayList(1);
        lst.add(new Experimenter());
        //dao.expects(once()).method("getEnabledUsers").withNoArguments().will(returnValue(lst));
        context.checking(new Expectations() {{
            one(dao).getEnabledUsers();
            will(returnValue(lst));
        }});
        
        assertNotNull(manager.getEnabledUsers());
    }   
}
