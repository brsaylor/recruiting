/*
 * Copyright (C) 1998-2009, <a href="http://www.ssel.caltech.edu">SSEL</a>,
 * at Caltech, Pasadena, CA 91125. 
 *
 * MooreRecruiting is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 * USA.
 *
 */

package edu.caltech.ssel.recruiting.service.impl;

import edu.caltech.ssel.recruiting.common.QuestionResultHelper;
import edu.caltech.ssel.recruiting.dao.ConditionDao;
import edu.caltech.ssel.recruiting.dao.ParticipantRecordDao;
import edu.caltech.ssel.recruiting.dao.QuestionResultDao;
import edu.caltech.ssel.recruiting.model.BooleanCondition;
import edu.caltech.ssel.recruiting.model.Condition;
import edu.caltech.ssel.recruiting.model.Experiment;
import edu.caltech.ssel.recruiting.model.ExperimentType;
import edu.caltech.ssel.recruiting.model.ExperimentTypeCondition;
import edu.caltech.ssel.recruiting.model.Filter;
import edu.caltech.ssel.recruiting.model.ParticipantRecord;
import edu.caltech.ssel.recruiting.model.PoolCondition;
import edu.caltech.ssel.recruiting.model.Question;
import edu.caltech.ssel.recruiting.model.Question.QStyle;
import edu.caltech.ssel.recruiting.model.QuestionResult;
import edu.caltech.ssel.recruiting.model.QuestionResultCondition;
import edu.caltech.ssel.recruiting.service.ConditionManager;
import edu.caltech.ssel.recruiting.model.QuestionResultCondition.FilterType;
import edu.caltech.ssel.recruiting.model.Subject;
import edu.caltech.ssel.recruiting.model.SubjectPool;
import java.util.ArrayList;
import java.util.Date;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.appfuse.service.impl.BaseManagerMockTestCase;

import org.jmock.Expectations;
import org.junit.Before;
import org.junit.After;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.hamcrest.Matchers.*;

/**
 *
 * @author michaelk
 */
public class ConditionManagerImplTest extends BaseManagerMockTestCase {

    private ConditionDao dao;
    private QuestionResultDao qrdao;
    private ParticipantRecordDao prdao;
    private ConditionManager manager;

    @Before
    public void setUp() {
        dao = context.mock(ConditionDao.class);
        qrdao = context.mock(QuestionResultDao.class);
        prdao = context.mock(ParticipantRecordDao.class);
        manager = new ConditionManagerImpl(dao);
        manager.setQuestionResultDao(qrdao);
        manager.setParticipantRecordDao(prdao);
    }
    
    @After
    public void tearDown() {
        manager = null;
    }
        
    @Test
    public void testCheckEligible() throws Exception {
        Long id = 2L;
        final Subject u = new Subject();
        final Question q = new Question();
        final QuestionResult qr = new QuestionResult();
        QuestionResultCondition c = new QuestionResultCondition();
        List<Subject> lst = new ArrayList();
        lst.add(u);

        List<String> slist = new ArrayList();
        slist.add("Option 1");
        slist.add("Option 2");
        q.setOptionList(slist);
        q.setStyle(QStyle.CHECK_BOX);
        
        boolean[] dig = {true, false};//Checked only the first
        qr.setAnswerIdx(QuestionResultHelper.setDigits(dig));
        qr.setQuestion(q);
        
        Set<Integer> set = new HashSet<Integer>();
        set.add(0);
        c.setFilterOptions(set);
        c.setFilterType(FilterType.CHECK_ALL);
        c.setQuestion(q);

        //qrdao.expects(once()).method("getMostRecentAnswer").with(eq(u), eq(q)).will(returnValue(qr));
        context.checking(new Expectations() {{
            one(qrdao).getMostRecentAnswer(with(equal(u)), with(equal(q)));
            will(returnValue(qr));
        }});
        manager.checkEligible(c, lst);
        assertNotNull(lst);
        assertTrue(lst.size() > 0);
    }

    @Test
    public void testIsEligibleQRCondition() throws Exception {
        log.debug("Testing question result condition...");

        Long id = 2L;
        final Subject u = new Subject();
        final Question q = new Question();
        final QuestionResult qr = new QuestionResult();
        QuestionResultCondition c = new QuestionResultCondition();

        List<String> slist = new ArrayList();
        slist.add("Option 1");
        slist.add("Option 2");
        q.setOptionList(slist);
        q.setStyle(QStyle.CHECK_BOX);

        boolean[] dig = {true, false};//Checked only the first
        qr.setAnswerIdx(QuestionResultHelper.setDigits(dig));
        qr.setQuestion(q);

        Set<Integer> set = new HashSet<Integer>();
        set.add(0);
        c.setFilterOptions(set);
        c.setFilterType(FilterType.CHECK_ALL);
        c.setQuestion(q);

        //qrdao.expects(once()).method("getMostRecentAnswer").with(eq(u), eq(q)).will(returnValue(qr));
        context.checking(new Expectations() {{
            one(qrdao).getMostRecentAnswer(with(equal(u)), with(equal(q)));
            will(returnValue(qr));
        }});
        assertTrue(manager.isEligible(c, u));
    }

    @Test
    public void testIsEligiblePoolCondition() throws Exception {
        log.debug("Testing pool condition...");

        Subject u = new Subject();
        SubjectPool sp = new SubjectPool();
        List<SubjectPool> lst = new ArrayList();
        PoolCondition c = new PoolCondition();

        sp.setId(1L);
        lst.add(sp);
        c.setPools(lst);
        c.setMemberOf(true);

        u.setPools(lst);

        assertTrue(manager.isEligible(c, u));

        c.setMemberOf(false);
        assertFalse(manager.isEligible(c, u));
    }

    @Test
    public void testIsEligibleETCondition() throws Exception {
        log.debug("Testing experiment type condition...");

        Long id = 2L;
        final Subject u = new Subject();
        ExperimentType et = new ExperimentType();
        List<ExperimentType> lst = new ArrayList();
        ExperimentTypeCondition c = new ExperimentTypeCondition();
        final List<ParticipantRecord> prlst = new ArrayList();
        ParticipantRecord pr = new ParticipantRecord();
        Experiment e = new Experiment();

        u.setId(1L);

        et.setId(1L);
        lst.add(et);
        et = new ExperimentType();
        et.setId(2L);
        lst.add(et);
        c.setHistory(false);
        c.setOnList(true);
        c.setTypes(lst);

        e.setType(et);
        Date d = new Date();
        d.setTime(d.getTime()+100);
        e.setExpDate(d);
        Calendar timeCal = new GregorianCalendar();
        timeCal.set(Calendar.HOUR_OF_DAY,new Date().getHours()+1);
        timeCal.set(Calendar.MINUTE,0);
        timeCal.set(Calendar.SECOND, 0);
        e.setStartTime(timeCal.getTime());
        pr.setExp(e);
        pr.setParticipated(false);
        prlst.add(pr);

        //prdao.stubs().method("getBySubject").with(eq(u)).will(returnValue(prlst));
        context.checking(new Expectations() {{
            allowing(prdao).getBySubject(with(equal(u)));
            will(returnValue(prlst));
        }});
        assertTrue(manager.isEligible(c, u));

        c.setHistory(true);
        assertFalse(manager.isEligible(c, u));

        c.setHistory(false);
        c.setOnList(false);
        assertFalse(manager.isEligible(c, u));

        timeCal.set(Calendar.HOUR_OF_DAY,new Date().getHours()-1);
        e.setStartTime(timeCal.getTime());
        assertFalse(manager.isEligible(c, u));
    }

    @Test
    public void testIsEligibleBooleanCondition() throws Exception {
        log.debug("Testing boolean condition...");

        Long id = 2L;
        final Subject u = new Subject();
        ExperimentType et = new ExperimentType();
        List<ExperimentType> lst = new ArrayList();
        final ExperimentTypeCondition c = new ExperimentTypeCondition();
        final ExperimentTypeCondition c2 = new ExperimentTypeCondition();
        final List<ParticipantRecord> prlst = new ArrayList();
        ParticipantRecord pr = new ParticipantRecord();
        Experiment e = new Experiment();
        BooleanCondition bc = new BooleanCondition();

        u.setId(1L);

        et.setId(1L);
        lst.add(et);
        et = new ExperimentType();
        et.setId(2L);
        lst.add(et);
        c.setHistory(false);
        c.setOnList(true);
        c.setTypes(lst);
        c.setId(1L);
        c2.setHistory(true);
        c2.setOnList(true);
        c2.setTypes(lst);
        c2.setId(2L);
        //c2 should be ineligible, c should be eligible

        e.setType(et);
        e.setExpDate(new Date(2050, 12, 12));
        Calendar timeCal = new GregorianCalendar();
        timeCal.set(Calendar.HOUR_OF_DAY,11);
        timeCal.set(Calendar.MINUTE,0);
        timeCal.set(Calendar.SECOND, 0);
        e.setStartTime(timeCal.getTime());
        pr.setExp(e);
        prlst.add(pr);

        bc.setAnd(false);
        bc.setFirstCondition(1L);
        bc.setSecondCondition(2L);
        bc.setId(3L);

        //prdao.stubs().method("getBySubject").with(eq(u)).will(returnValue(prlst));
        context.checking(new Expectations() {{
            allowing(prdao).getBySubject(with(equal(u)));
            will(returnValue(prlst));
        }});
        //dao.stubs().method("get").with(eq(new Long(1))).will(returnValue(c));
        context.checking(new Expectations() {{
            allowing(dao).get(with(equal(new Long(1))));
            will(returnValue(c));
        }});
        //dao.stubs().method("get").with(eq(new Long(2))).will(returnValue(c2));
        context.checking(new Expectations() {{
            allowing(dao).get(with(equal(new Long(2))));
            will(returnValue(c2));
        }});
        //dao.stubs().method("exists").withAnyArguments().will(returnValue(true));
        context.checking(new Expectations() {{
            allowing(dao).exists(with(any(Long.class)));
            will(returnValue(true));
        }});

        assertTrue(manager.isEligible(bc, u));

        bc.setAnd(true);
        assertFalse(manager.isEligible(bc, u));
    }

    @Test
    public void testIsEligibleFilter() throws Exception {
        log.debug("Testing filter is eligible...");
        Long id = 2L;
        final Subject u = new Subject();
        final Question q = new Question();
        final QuestionResult qr = new QuestionResult();
        QuestionResultCondition c = new QuestionResultCondition();
        Filter f = new Filter();

        List<String> slist = new ArrayList();
        slist.add("Option 1");
        slist.add("Option 2");
        q.setOptionList(slist);
        q.setStyle(QStyle.CHECK_BOX);

        boolean[] dig = {true, false};//Checked only the first
        qr.setAnswerIdx(QuestionResultHelper.setDigits(dig));
        qr.setQuestion(q);

        Set<Integer> set = new HashSet<Integer>();
        set.add(0);
        c.setFilterOptions(set);
        c.setFilterType(FilterType.CHECK_ALL);
        c.setQuestion(q);

        List<Condition> clist = new ArrayList();
        clist.add(c);
        f.setConditions(clist);

        //qrdao.expects(once()).method("getMostRecentAnswer").with(eq(u), eq(q)).will(returnValue(qr));
        context.checking(new Expectations() {{
            one(qrdao).getMostRecentAnswer(with(equal(u)), with(equal(q)));
            will(returnValue(qr));
        }});
        assertTrue(manager.isEligible(f, u));
    }

    @Test
    public void testDisabledIsEligible() throws Exception {
        log.debug("Testing disabled is eligible...");

        Long id = 2L;
        final Subject u = new Subject();
        ExperimentType et = new ExperimentType();
        List<ExperimentType> lst = new ArrayList();
        ExperimentTypeCondition c = new ExperimentTypeCondition();
        final List<ParticipantRecord> prlst = new ArrayList();
        ParticipantRecord pr = new ParticipantRecord();
        Experiment e = new Experiment();

        u.setId(1L);

        et.setId(1L);
        lst.add(et);
        et = new ExperimentType();
        et.setId(2L);
        lst.add(et);
        c.setHistory(false);
        c.setOnList(true);
        c.setTypes(lst);

        e.setType(et);
        e.setExpDate(new Date());
        Calendar timeCal = new GregorianCalendar();
        timeCal.set(Calendar.HOUR_OF_DAY,new Date().getHours()+1);
        timeCal.set(Calendar.MINUTE,0);
        timeCal.set(Calendar.SECOND, 0);
        e.setStartTime(timeCal.getTime());
        pr.setExp(e);
        pr.setParticipated(false);
        prlst.add(pr);

        //prdao.stubs().method("getBySubject").with(eq(u)).will(returnValue(prlst));
        context.checking(new Expectations() {{
            allowing(prdao).getBySubject(with(equal(u)));
            will(returnValue(prlst));
        }});
        assertTrue(manager.isEligible(c, u));

        c.setHistory(true);
        assertFalse(manager.isEligible(c, u));

        u.setEnabled(false);
        assertFalse(manager.isEligible(c, u));

    }
}
