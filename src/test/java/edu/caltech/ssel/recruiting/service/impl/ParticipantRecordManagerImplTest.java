/*
 * Copyright (C) 1998-2009, <a href="http://www.ssel.caltech.edu">SSEL</a>,
 * at Caltech, Pasadena, CA 91125. 
 *
 * MooreRecruiting is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 * USA.
 *
 */

package edu.caltech.ssel.recruiting.service.impl;

import edu.caltech.ssel.recruiting.dao.ParticipantRecordDao;
import edu.caltech.ssel.recruiting.model.Experiment;
import edu.caltech.ssel.recruiting.model.ParticipantRecord;
import edu.caltech.ssel.recruiting.model.Subject;
import edu.caltech.ssel.recruiting.service.ParticipantRecordManager;
import java.util.ArrayList;
import java.util.List;
import org.appfuse.service.impl.BaseManagerMockTestCase;

import org.jmock.Expectations;
import org.junit.Before;
import org.junit.After;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author michaelk
 */
public class ParticipantRecordManagerImplTest extends BaseManagerMockTestCase {
    
    private ParticipantRecordManager manager;
    private ParticipantRecordDao dao;
    private Experiment e;
    private Subject s;
    private List<ParticipantRecord> lst;

    @Before
    public void setUp() throws Exception {
        dao = context.mock(ParticipantRecordDao.class);
        manager = new ParticipantRecordManagerImpl(dao);
        e = new Experiment();
        s = new Subject();
        lst = new ArrayList();
    }

    @After
    public void tearDown() throws Exception {
        manager = null;
    }

    @Test
    public void testGetByExperiment() {
        log.debug("Entering testGetByExperiment...");
        
        //dao.expects(once()).method("getByExperiment").with(eq(e)).will(returnValue(lst));
        context.checking(new Expectations() {{
            one(dao).getByExperiment(with(equal(e)));
            will(returnValue(lst));
        }});
        List<ParticipantRecord> lst2 = manager.getByExperiment(e);
        
        assertSame(lst2, lst);
    }

    @Test
    public void testGetBySubject() {
        log.debug("Entering testGetBySubject...");
        
        //dao.expects(once()).method("getBySubject").with(eq(s)).will(returnValue(lst));
        context.checking(new Expectations() {{
            one(dao).getBySubject(with(equal(s)));
            will(returnValue(lst));
        }});
        List<ParticipantRecord> lst2 = manager.getBySubject(s);
        
        assertSame(lst2, lst);
    }

    @Test
    public void testGetByPlayerNum() {
        log.debug("Entering testGetByPlayerNum...");
        
        //dao.expects(once()).method("getByPlayerNum").with(eq(new Integer(12))).will(returnValue(lst));
        context.checking(new Expectations() {{
            one(dao).getByPlayerNum(with(equal(new Integer(12))));
            will(returnValue(lst));
        }});
        List<ParticipantRecord> lst2 = manager.getByPlayerNum(12);
        
        assertSame(lst2, lst);
    }
    
}
