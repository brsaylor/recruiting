/*
 * Copyright (C) 1998-2009, <a href="http://www.ssel.caltech.edu">SSEL</a>,
 * at Caltech, Pasadena, CA 91125. 
 *
 * MooreRecruiting is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 * USA.
 *
 */

package edu.caltech.ssel.recruiting.webapp.controller;

import edu.caltech.ssel.recruiting.common.ExperimentCreationHelper;
import edu.caltech.ssel.recruiting.common.RecruitingConstants;
import edu.caltech.ssel.recruiting.model.Experiment;
import edu.caltech.ssel.recruiting.model.Location;
import edu.caltech.ssel.recruiting.service.ExperimentManager;
import org.appfuse.service.GenericManager;
import org.appfuse.webapp.controller.BaseControllerTestCase;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author michaelk
 */
public class ExperimentFormControllerTest extends BaseControllerTestCase {
    
    private ExperimentFormController c;
    private GenericManager<Location, Long> locationManager;
    private ExperimentManager experimentManager;
    private ExperimentCreationHelper ecHelper;

    /** Creates a new instance of ExperimentFormControllerTest */
    public ExperimentFormControllerTest() throws Exception {
    }
    
    public void testLoadNoFlow() throws Exception {
        MockHttpServletRequest request = newGet("/experimentform.html");
        request.addParameter("id", "1");
        request.setRemoteUser("michaelk");
        request.addUserRole(RecruitingConstants.EXPTR_ROLE);
        ModelAndView mv = c.handleRequest(request, new MockHttpServletResponse());
        
        Experiment e = (Experiment) mv.getModel().get(c.getCommandName());
        assertNotNull(e);
        assertEquals(e.getId(), new Long(1));
    }
    
    public void testSubmitNoFlow() throws Exception {
        MockHttpServletRequest samplePost = getSamplePost();
        samplePost.removeParameter("continue");
        samplePost.addParameter("save", "save");
        ecHelper.clearSession();
        samplePost.addParameter("id", "1");
        ModelAndView mv = c.handleRequest(samplePost, new MockHttpServletResponse());
        Errors errors = (Errors) mv.getModel().get(BindException.MODEL_KEY_PREFIX + "experiment");
        if(errors != null)
            log.debug(errors.toString());
        assertNull(errors);
    }
    
    public void testFormBackingObject() throws Exception {
        MockHttpServletRequest request = newGet("/experimentform.html");
        request.setRemoteUser("michaelk");
        request.addUserRole(RecruitingConstants.EXPTR_ROLE);
        ModelAndView mv = c.handleRequest(request, new MockHttpServletResponse());
        
        assertNotNull(mv.getModel().get(c.getCommandName()));
        assertNotNull(mv.getModel().get("exrList"));
        assertNotNull(mv.getModel().get("expTypeList"));
        assertNotNull(mv.getModel().get("locList"));
    }
    
    public void testOnSubmitNoSurvey() throws Exception {
        MockHttpServletRequest samplePost = getSamplePost();
        samplePost.setParameter("surveyRadio", "NO_SURVEY");
        samplePost.setParameter("filterRadio", "NO_FILTER");
        ModelAndView mv = c.handleRequest(samplePost, new MockHttpServletResponse());
        
        Errors errors = (Errors) mv.getModel().get(BindException.MODEL_KEY_PREFIX + "experiment");
        if(errors != null)
            log.debug(errors.toString());
        assertNull(errors);
    }

    public void testOnSubmitExistingSurvey() throws Exception {
        MockHttpServletRequest samplePost = getSamplePost();
        samplePost.setParameter("surveyRadio", "EXISTING_SURVEY");
        samplePost.setParameter("existingSurvey", "1");
        samplePost.setParameter("filterRadio", "NO_FILTER");
        ModelAndView mv = c.handleRequest(samplePost, new MockHttpServletResponse());
        
        Errors errors = (Errors) mv.getModel().get(BindException.MODEL_KEY_PREFIX + "experiment");
        if(errors != null)
            log.debug(errors.toString());
        assertNull(errors);
    }

    public void testOnSubmitNewSurvey() throws Exception {
        MockHttpServletRequest samplePost = getSamplePost();
        samplePost.setParameter("surveyRadio", "NEW_SURVEY");
        samplePost.setParameter("newSurvey", "new");
        samplePost.setParameter("filterRadio", "NO_FILTER");
        ModelAndView mv = c.handleRequest(samplePost, new MockHttpServletResponse());
        
        Errors errors = (Errors) mv.getModel().get(BindException.MODEL_KEY_PREFIX + "experiment");
        if(errors != null)
            log.debug(errors.toString());
        assertNull(errors);

        samplePost.setParameter("surveyRadio", "NEW_SURVEY");
        samplePost.setParameter("newSurvey", "1");
        samplePost.setParameter("filterRadio", "NO_FILTER");
        mv = c.handleRequest(samplePost, new MockHttpServletResponse());
        
        errors = (Errors) mv.getModel().get(BindException.MODEL_KEY_PREFIX + "experiment");
        if(errors != null)
            log.debug(errors.toString());
        assertNull(errors);
    }

    private MockHttpServletRequest getSamplePost() throws Exception {
        Experiment e2 = experimentManager.get(1L);
        Experiment e = new Experiment();
        e.setFilter(null);
        e.setInstruction(e2.getInstruction());
        e.setLocation(e2.getLocation());
        e.setMaxNumSubjects(e2.getMaxNumSubjects());
        e.setMinNumSubjects(e2.getMinNumSubjects());
        //e.setPrinciple(e2.getPrinciple());
        //e.setReservedBy(e2.getReservedBy());
        //e.setRunby(e2.getRunby());
        e.setShowUpFee(e2.getShowUpFee());
        e.setSignupFreezeTime(e2.getSignupFreezeTime());
        //e.setStatus(e2.getStatus());
        //e.setSurvey(e2.getSurvey());
        
        System.out.println("\n\n********* Note: Having difficulty setting status and survey *********\n\n");
        
        MockHttpServletRequest samplePost = newPost("/experimentform.html");
        samplePost.setRemoteUser("michaelk");
        samplePost.addUserRole(RecruitingConstants.EXPTR_ROLE);
        super.objectToRequestParameters(e, samplePost);
        samplePost.addParameter("myExpDate", "12/12/2008");
        samplePost.addParameter("startHours", "12");
        samplePost.addParameter("startMinutes", "0");
        samplePost.addParameter("endHours", "1");
        samplePost.addParameter("endMinutes", "0");
        samplePost.addParameter("startAMPM", "startAM");
        samplePost.addParameter("endAMPM", "endAM");
        samplePost.addParameter("statusSelect", "OPEN");
        samplePost.addParameter("myRunby", "2");
        samplePost.addParameter("myPrinciple", "2");
        samplePost.addParameter("myLoc", "1");
        samplePost.addParameter("myType", "1");
        samplePost.addParameter("continue", "continue");

        ecHelper.setSession(samplePost.getSession());
        ecHelper.setAnnouncement(new SimpleMailMessage());
        ecHelper.setExperiment(experimentManager.get(4L));
        ecHelper.setUseAnnouncement(true);
        ecHelper.setUseFilter(true);
        ecHelper.setUseSurvey(true);
        
        return samplePost;
    }
    
    public void setExperimentFormController(ExperimentFormController c) {
        this.c = c;
    }

    public void setLocationManager(GenericManager<Location, Long> locationManager) {
        this.locationManager = locationManager;
    }

    public void setExperimentManager(ExperimentManager experimentManager) {
        this.experimentManager = experimentManager;
    }

    public void setExperimentCreationHelper(ExperimentCreationHelper ecHelper) {
        this.ecHelper = ecHelper;
    }
    
}
