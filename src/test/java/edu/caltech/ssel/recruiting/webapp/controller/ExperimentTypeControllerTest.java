/*
 * Copyright (C) 1998-2009, <a href="http://www.ssel.caltech.edu">SSEL</a>,
 * at Caltech, Pasadena, CA 91125. 
 *
 * MooreRecruiting is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 * USA.
 *
 */

package edu.caltech.ssel.recruiting.webapp.controller;

import java.util.Set;
import org.appfuse.webapp.controller.BaseControllerTestCase;
import org.springframework.ui.ModelMap;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author Administrator
 */
public class ExperimentTypeControllerTest extends BaseControllerTestCase {
    private ExperimentTypeController experimentTypeController;

    public void testHandleRequest() throws Exception {
        /*ModelAndView mav = experimentTypeController.handleRequest(null, null);
        ModelMap m = mav.getModelMap();
        //removed the two test cases because we use paging now
        assertNotNull(m.get("experimentTypeList"));
        assertTrue(((Set) m.get("experimentTypeList")).size() > 0);*/
    }

    public void setExperimentTypeController(ExperimentTypeController experimentTypeController) {
        this.experimentTypeController = experimentTypeController;
    }
}
