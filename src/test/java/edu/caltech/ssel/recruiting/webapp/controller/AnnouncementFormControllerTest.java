/*
 * Copyright (C) 1998-2009, <a href="http://www.ssel.caltech.edu">SSEL</a>,
 * at Caltech, Pasadena, CA 91125. 
 *
 * MooreRecruiting is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 * USA.
 *
 */

package edu.caltech.ssel.recruiting.webapp.controller;

import edu.caltech.ssel.recruiting.common.ExperimentCreationHelper;
import edu.caltech.ssel.recruiting.common.RecruitingConstants;
import edu.caltech.ssel.recruiting.model.Experiment;
import org.appfuse.model.User;
import org.appfuse.service.GenericManager;
import org.appfuse.service.UserManager;
import org.appfuse.webapp.controller.BaseControllerTestCase;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
/**
 *
 * @author michaelk
 */
public class AnnouncementFormControllerTest extends BaseControllerTestCase {

    private AnnouncementFormController controller;
    private ExperimentCreationHelper ecHelper;
    private GenericManager<Experiment, Long> experimentManager;
    private UserManager userManager;
    
    /** Creates a new instance of AnnouncementFormControllerTest */
    public AnnouncementFormControllerTest() {
    }
    
    /**
     * test stub until tests are fixed - poneil
     */
    public void testGet() throws Exception {
        log.debug("basic test");
        MockHttpServletRequest request = newGet("announcementform.html");
        request.setRemoteUser("admin");
        request.addUserRole(RecruitingConstants.ADMIN_ROLE);
        ModelAndView mv = controller.handleRequest(request, new MockHttpServletResponse());
        assertNotNull(mv);        
    }

    public void testLoadNoFlow() throws Exception {
        log.debug("Entering testLoadNoFlow...");
        
        User u = userManager.getUserByUsername("admin");
        MockHttpServletRequest request = newGet("/announcementform.html");
        request.setRemoteUser(u.getUsername());
        request.addUserRole(RecruitingConstants.ADMIN_ROLE);
        ModelAndView mv = controller.handleRequest(request, new MockHttpServletResponse());
        
        SimpleMailMessage msg = (SimpleMailMessage) mv.getModel().get(controller.getCommandName());
        assertNotNull(msg);
        assertEquals(msg.getReplyTo(), u.getEmail());
    }
    
    public void testSubmitNoFlow() throws Exception {
        log.debug("Entering testSubmitNoFlow...");

        //Note: ideally we would check whether an email has been sent at the end of this test
        //But my attempts at this have failed, even though a message is sent in the current codebase.
        
        MockHttpServletRequest request = newPost("/announcementform.html");
        SimpleMailMessage msg = new SimpleMailMessage();
        request.setRemoteUser("michaelk");
        request.addUserRole(RecruitingConstants.EXPTR_ROLE);
        request.setParameter("send", "send");
        request.setParameter("from", "michaelk@caltech.edu");
        request.setParameter("text", "Test text");
        request.setParameter("subject", "Test subject");
        String [] to = new String[2];
        to[0] = "test@test.com";
        to[1] = "test2@test.com";
        request.setParameter("emailList_to",to); 
        
        ModelAndView mv = controller.handleRequest(request, new MockHttpServletResponse());
        
        assertEquals(mv.getViewName(), controller.getExperimenterView());

    }
    
    public void testLoadAdmin() throws Exception {
        log.debug("Entering testLoadAdmin...");
        
        User u = userManager.getUserByUsername("admin");
        MockHttpServletRequest request = newGet("/announcementform.html");
        request.setRemoteUser("admin");
        ModelAndView mv = controller.handleRequest(request, new MockHttpServletResponse());
        
        SimpleMailMessage msg = (SimpleMailMessage) mv.getModel().get(controller.getCommandName());
        assertNotNull(msg);
        assertEquals(msg.getReplyTo(), u.getEmail());
        assertNull(msg.getSubject());
    }
    
    public void testSubmitAdmin() throws Exception {
        log.debug("Entering testSubmitNoFlow...");

        //Note: ideally we would check whether an email has been sent at the end of this test
        //But my attempts at this have failed, even though a message is sent in the current codebase.

        MockHttpServletRequest request = newPost("/announcementform.html");
        SimpleMailMessage msg = new SimpleMailMessage();
        request.setRemoteUser("admin");
        request.addUserRole(RecruitingConstants.ADMIN_ROLE);
        request.setParameter("send", "send");
        request.setParameter("from", "michaelk@caltech.edu");
        request.setParameter("text", "Test text");
        request.setParameter("subject", "Test subject");
        String [] to = new String[2];
        to[0] = "test@test.com";
        to[1] = "test2@test.com";
        request.setParameter("emailList_to",to); 
        
        ModelAndView mv = controller.handleRequest(request, new MockHttpServletResponse());
        
        assertEquals(mv.getViewName(), controller.getAdminView());
    }
    
    public void testLoad() throws Exception {
        log.debug("Entering testLoad...");
        
        MockHttpServletRequest request = newGet("/announcementform.html");
        request.setRemoteUser("michaelk");
        request.addUserRole(RecruitingConstants.EXPTR_ROLE);
        request.getSession().setAttribute("experiment",experimentManager.get(4L));
        SimpleMailMessage msg = new SimpleMailMessage();

        ModelAndView mv = controller.handleRequest(request, new MockHttpServletResponse());

        SimpleMailMessage msg2 = (SimpleMailMessage) mv.getModel().get(controller.getCommandName());
        assertNotNull(msg2);
        assertNotSame(msg, msg2);
    }
    
    public void testSubmit() throws Exception {
        log.debug("Entering testSubmit...");
        
        MockHttpServletRequest request = newPost("/announcementform.html");
        request.setRemoteUser("michaelk");
        request.addUserRole(RecruitingConstants.EXPTR_ROLE);
        request.getSession().setAttribute("experiment",experimentManager.get(4L));
        SimpleMailMessage msg = new SimpleMailMessage();
        request.setParameter("continue", "continue");
        request.setParameter("from", "michaelk@caltech.edu");
        request.setParameter("text", "Test text");
        request.setParameter("subject", "Test subject");
        String [] to = new String[2];
        to[0] = "test@test.com";
        to[1] = "test2@test.com";
        request.setParameter("emailList_to",to); 
        
        ModelAndView mv = controller.handleRequest(request, new MockHttpServletResponse());
        
        Errors errors = (Errors) mv.getModel().get(BindException.MODEL_KEY_PREFIX + "announcement");
        assertNull(errors);
        assertNotNull(request.getSession().getAttribute("successMessages"));
    }

    public void setAnnouncementFormController(AnnouncementFormController controller) {
        this.controller = controller;
    }

    public void setExperimentCreationHelper(ExperimentCreationHelper ecHelper) {
        this.ecHelper = ecHelper;
    }

    public void setExperimentManager(GenericManager<Experiment, Long> experimentManager) {
        this.experimentManager = experimentManager;
    }

    public void setUserManager(UserManager userManager) {
        this.userManager = userManager;
    }
    
}
