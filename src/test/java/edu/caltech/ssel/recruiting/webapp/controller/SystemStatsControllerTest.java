/*
 * Copyright (C) 1998-2009, <a href="http://www.ssel.caltech.edu">SSEL</a>,
 * at Caltech, Pasadena, CA 91125. 
 *
 * MooreRecruiting is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 * USA.
 *
 */

package edu.caltech.ssel.recruiting.webapp.controller;

import org.appfuse.webapp.controller.BaseControllerTestCase;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author michaelk
 */
public class SystemStatsControllerTest extends BaseControllerTestCase {
    
    private SystemStatsController c;
    
    /** Creates a new instance of SystemStatsControllerTest */
    public SystemStatsControllerTest() {
    }
    
    public void testExperimenter() throws Exception {
        MockHttpServletRequest request = newGet("/systemstats.html");
        request.setParameter("type","experimenter");
        request.setParameter("role","reservedBy");
        ModelAndView mv = c.handleRequest(request, new MockHttpServletResponse());
        assertNotNull(mv.getModel().get("statsList"));
    }
    
    public void testExperimentType() throws Exception {
        MockHttpServletRequest request = newGet("/systemstats.html");
        request.setParameter("type","experimentType");
        ModelAndView mv = c.handleRequest(request, new MockHttpServletResponse());
        assertNotNull(mv.getModel().get("statsList"));
    }
    
    public void testLocation() throws Exception {
        MockHttpServletRequest request = newGet("/systemstats.html");
        request.setParameter("type","location");
        ModelAndView mv = c.handleRequest(request, new MockHttpServletResponse());
        assertNotNull(mv.getModel().get("statsList"));
    }
    
    public void setSystemStatsController(SystemStatsController c) {
        this.c = c;
    }
    
}
