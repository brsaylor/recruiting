/*
 * Copyright (C) 1998-2009, <a href="http://www.ssel.caltech.edu">SSEL</a>,
 * at Caltech, Pasadena, CA 91125. 
 *
 * MooreRecruiting is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 * USA.
 *
 */

package edu.caltech.ssel.recruiting.webapp.controller;

import edu.caltech.ssel.recruiting.model.Location;
import org.appfuse.service.GenericManager;
import org.appfuse.webapp.controller.BaseControllerTestCase;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author Administrator
 */
public class EditLocationControllerTest extends BaseControllerTestCase {
    
    private EditLocationController c;
    private GenericManager<Location, Long> locationManager;
    private static final Long testId = new Long(1);

    public EditLocationControllerTest() {
    }

    
    public void testLoad() throws Exception {
	//First try request with no name
	MockHttpServletRequest request = newGet("/editlocation.html");
	ModelAndView mv = c.handleRequest(request, new MockHttpServletResponse());
        Location result = (Location)mv.getModel().get(c.getCommandName());
	assertNotNull(result);
	
	//Now try to actually set the test data as the backing object
	request.addParameter("id", testId.toString());
	mv = c.handleRequest(request, new MockHttpServletResponse());
        result = (Location)mv.getModel().get(c.getCommandName());
	assertSame(result, locationManager.get(testId));
    }
    
    //Functions implemented by buttons: save (new and old), delete
    
    public void testUpdate() throws Exception {
	log.debug("Testing update...");
	
        Location l = locationManager.get(testId);
	MockHttpServletRequest request = newPost("/subjectpoolform.html");
	super.objectToRequestParameters(l, request);
	request.setParameter("contact", "Updated contact");
	ModelAndView mv = c.handleRequest(request, new MockHttpServletResponse());
        
        assertEquals(mv.getViewName(), c.getSuccessView());
        assertEquals("Updated contact", locationManager.get(testId).getContact());
    }
    
    public void testDelete() throws Exception {
	log.debug("Testing delete...");
	
	MockHttpServletRequest request = newPost("/subjectpoolform.html");
	request.addParameter("delete", "delete");
	request.addParameter("id", testId.toString());
	ModelAndView mv = c.handleRequest(request, new MockHttpServletResponse());
	
        assertEquals(mv.getViewName(), c.getSuccessView());
        assertFalse(locationManager.exists(testId));
    }
    
    public void setLocationManager(GenericManager<Location, Long> locationManager) {
	this.locationManager = locationManager;
    }
    
    public void setEditLocationController(EditLocationController c) {
	this.c = c;
    }
    
}
