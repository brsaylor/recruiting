/*
 * Copyright (C) 1998-2009, <a href="http://www.ssel.caltech.edu">SSEL</a>,
 * at Caltech, Pasadena, CA 91125. 
 *
 * MooreRecruiting is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 * USA.
 *
 */

package edu.caltech.ssel.recruiting.webapp.controller;

import edu.caltech.ssel.recruiting.common.ExperimentCreationHelper;
import edu.caltech.ssel.recruiting.model.Experiment;
import edu.caltech.ssel.recruiting.model.Question;
import edu.caltech.ssel.recruiting.model.Survey;
import org.appfuse.service.GenericManager;
import org.appfuse.webapp.controller.BaseControllerTestCase;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author michaelk
 */
public class SurveyCreationFormControllerTest extends BaseControllerTestCase {
    
    private SurveyCreationFormController c;
    private ExperimentFormController experimentFormController;
    private GenericManager<Survey, Long> surveyManager;
    private GenericManager<Experiment, Long> experimentManager;
    private GenericManager<Question, Long> questionManager;
    private ExperimentCreationHelper ecHelper;
    
    /** Creates a new instance of SurveyCreationFormControllerTest */
    public SurveyCreationFormControllerTest() {
    }
    
    public void testLoadNoFlow() throws Exception {
        MockHttpServletRequest request = newGet("/surveycreationform.html");
        request.setParameter("id", "2");
        request.getSession().setAttribute("experiment", experimentManager.get(1L));
        ModelAndView mv = c.handleRequest(request, new MockHttpServletResponse());

        assertEquals(mv.getModel().get(c.getCommandName()), surveyManager.get(2L));
    }
    
    public void testSaveProfileNoFlow() throws Exception {
        MockHttpServletRequest request = newPost("/surveycreationform.html");
        request.setParameter("id", "1");
        request.setParameter("name", "Profile");
        request.setParameter("save", "save");
        request.setParameter("surveyVisibility", "private");
        request.setRemoteUser("user");
        request.setParameter("qcnt", "2");
        request.setParameter("questionId_0", "1");
        request.setParameter("questionId_1", "3");
        request.setParameter("checkedIRB", "true");
        ModelAndView mv = c.handleRequest(request, new MockHttpServletResponse());

        assertTrue(surveyManager.get(1L).getQuestions().size() == 2);
        assertEquals(mv.getViewName(), c.getProfileSuccessView());
    }
    
    public void testSaveNonProfileNoFlow() throws Exception {
        MockHttpServletRequest request = newPost("/surveycreationform.html");
        request.setParameter("id", "2");
        request.getSession().setAttribute("experiment", experimentManager.get(1L));
        request.setParameter("name", "Test name");
        request.setParameter("save", "save");
        request.setParameter("surveyRadio", "EXISTING_SURVEY");
        request.setParameter("existingSurvey", "2");
        request.setParameter("surveyVisibility", "private");
        request.setRemoteUser("user");
        request.setParameter("qcnt", "2");
        request.setParameter("questionId_0", "1");
        request.setParameter("questionId_1", "3");
        request.setParameter("checkedIRB", "true");
        ModelAndView mv = c.handleRequest(request, new MockHttpServletResponse());

        assertTrue(surveyManager.get(2L).getQuestions().size() == 2);
        assertEquals(mv.getViewName(), c.getCancelView());
    }
    
    public void testLoadFlow() throws Exception {
        MockHttpServletRequest request = newGet("/surveycreationform.html");
        request.setParameter("id", "2");
        ModelAndView mv = c.handleRequest(request, new MockHttpServletResponse());

        assertEquals(mv.getModel().get(c.getCommandName()), surveyManager.get(2L));
    }
    
    public void testContinueFlow() throws Exception {
        MockHttpServletRequest request = newPost("/surveycreationform.html");
        request.getSession().setAttribute("experiment", experimentManager.get(1L));
        request.setParameter("id", "2");
        request.setParameter("surveyRadio", "EXISTING_SURVEY");
        request.setParameter("existingSurvey", "2");
        request.setParameter("name", "New Experiment Survey");
        request.setParameter("next", "next");
        request.setParameter("surveyVisibility", "private");
        request.setRemoteUser("michaelk");
        request.setParameter("qcnt", "2");
        request.setParameter("questionId_0", "1");
        request.setParameter("questionId_1", "3");
        request.setParameter("checkedIRB", "true");
        ModelAndView mv = c.handleRequest(request, new MockHttpServletResponse());

        assertEquals(surveyManager.get(2L).getName(), "New Experiment Survey");
        assertEquals(mv.getViewName(), c.getNextStepView());
    }

    /*
    public void testProfileFilter() throws Exception {
        Survey s = new Survey();
        Experiment e = new Experiment();
        
        List<Question> lst = new ArrayList();
        lst.add(questionManager.get(1L));
        s.setQuestions(lst);
        s.setType(SurveyType.SURVEY);
        s.setVisibility(Visibility.PUBLIC);
        s = surveyManager.save(s);
        
        e.setMaxNumSubjects(0);
        e.setMinNumSubjects(0);
        e.setShowUpFee(0);
        e.setSignupFreezeTime(0);
        e.setStatus(Status.OPEN);
        e.setSurvey(s);
        e = experimentManager.save(e);
        
        //Set the filter to look at question #1, answer 1 is true (answerIdx = 2), CHECK_NONE
        //This should cause "John Doe" to be eligible
        MockHttpServletRequest request = newPost("/surveycreationform.html");
        request.setRemoteUser("michaelk");
        request.setParameter("eligibleCriterion0", "CHECK_NONE");
        request.setParameter("surveyVisibility", "public");
        request.setParameter("save", "Save survey");
        request.setParameter("selectedOptions0", "1");
        request.setParameter("expId", e.getId().toString());
        request.setParameter("styleSelect", "CHECK_BOX");
        request.setParameter("type", "SURVEY");
        request.setParameter("questionSelect", "new");
        request.setParameter("id", s.getId().toString());
        request.setParameter("labelText", "");
        request.setParameter("name", "");
        request.setParameter("questionText", "");
        request.setParameter("filter0", "on");
        request.setParameter("afterSelect", "--add to end--");
        request.setParameter("qcnt", Integer.toString(s.getQuestions().size()));
        for(int j = 0; j < s.getQuestions().size(); j++) {
            request.setParameter("questionId_" + j, s.getQuestions().get(j).getId().toString());
        }
        c.handleRequest(request, new MockHttpServletResponse());
    }*/
    
    public void setSurveyCreationFormController(SurveyCreationFormController c) {
        this.c = c;
    }

    public void setSurveyManager(GenericManager<Survey, Long> surveyManager) {
        this.surveyManager = surveyManager;
    }

    public void setQuestionManager(GenericManager<Question, Long> questionManager) {
        this.questionManager = questionManager;
    }

    public void setExperimentFormController(ExperimentFormController experimentFormController) {
        this.experimentFormController = experimentFormController;
    }

    public void setExperimentManager(GenericManager<Experiment, Long> experimentManager) {
        this.experimentManager = experimentManager;
    }

    public void setExperimentCreationHelper(ExperimentCreationHelper ecHelper) {
        this.ecHelper = ecHelper;
    }
    
}
