/*
 * Copyright (C) 1998-2009, <a href="http://www.ssel.caltech.edu">SSEL</a>,
 * at Caltech, Pasadena, CA 91125. 
 *
 * MooreRecruiting is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 * USA.
 *
 */

package edu.caltech.ssel.recruiting.webapp.controller;

import edu.caltech.ssel.recruiting.common.DebugHelper;
import edu.caltech.ssel.recruiting.common.RecruitingConstants;
import edu.caltech.ssel.recruiting.model.Experiment;
import edu.caltech.ssel.recruiting.model.ParticipantRecord;
import edu.caltech.ssel.recruiting.service.ParticipantRecordManager;
import java.util.List;
import org.appfuse.service.GenericManager;
import org.appfuse.webapp.controller.BaseControllerTestCase;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;

/**
 *
 * @author michaelk
 */
public class ParticipantRecordFormControllerTest extends BaseControllerTestCase {
    
    private ParticipantRecordFormController controller;
    private ParticipantRecordManager participantRecordManager;
    private GenericManager<Experiment, Long> experimentManager;
    
    /** Creates a new instance of ParticipantRecordFormControllerTest */
    public ParticipantRecordFormControllerTest() {
    }
    
    public void testLoadAdd() throws Exception {
        MockHttpServletRequest request = newGet("/participantrecordform.html");
        request.getSession().setAttribute("experiment", experimentManager.get(1L));
        ModelAndView mv = controller.handleRequest(request, new MockHttpServletResponse());
        
        //Check the command object
        ParticipantRecord rec = (ParticipantRecord) mv.getModel().get(controller.getCommandName());
        assertNotNull(rec);
        assertNull(rec.getId());
        
        //Check that the proper reference data exists
        //assertNotNull(mv.getModel().get("participantList"));
        assertNotNull(mv.getModel().get("statusList"));
        assertNotNull(mv.getModel().get("paymentMethodList"));
    }
    
    public void testLoadExisting() throws Exception {
        log.debug("entering testloadexisting...");
        MockHttpServletRequest request = newGet("/participantrecordform.html");
        request.getSession().setAttribute("experiment", experimentManager.get(1L));
        request.setParameter("id", "1");
        ModelAndView mv = controller.handleRequest(request, new MockHttpServletResponse());
        
        //Check the command object
        ParticipantRecord rec = (ParticipantRecord) mv.getModel().get(controller.getCommandName());
        assertNotNull(rec);
        assertEquals(rec.getId(), new Long(1));
        
        //Check that the proper reference data exists
        assertNotNull(mv.getModel().get("participantList"));
        assertNotNull(mv.getModel().get("statusList"));
        assertNotNull(mv.getModel().get("paymentMethodList"));
    }
    
    public void testSaveNew() throws Exception {
        log.debug("entering testSavenew...");
        MockHttpServletRequest request = newPost("/participantrecordform.html");
        request.getSession().setAttribute("experiment", experimentManager.get(1L));
        request.setParameter("save", "save");
        request.setParameter("subject_id", "4");//Add user "twisted"
        request.setParameter("status_name", "STATUS_2");
        request.setParameter("paymentMethod_name", "MANUAL");
        request.setParameter("playerNum", "1");
        request.setParameter("participated", "on");
        DebugHelper.displayParams(request, log);
        ModelAndView mv = controller.handleRequest(request, new MockHttpServletResponse());
        
        //Check success view
        assertEquals(mv.getViewName(), controller.getSuccessView());
        
        //Check that new p.r. has been added
        List<ParticipantRecord> lst = participantRecordManager.getByExperiment(experimentManager.get(1L));
        assertNotNull(lst);
        assertSame(lst.size(), 2);//Was 1 before
    }
    
    public void testEdit() throws Exception {
        log.debug("entering testedit...");
        MockHttpServletRequest request = newPost("/participantrecordform.html");
        request.getSession().setAttribute("experiment", experimentManager.get(1L));
        request.setParameter("save", "save");
        ParticipantRecord rec = participantRecordManager.get(1L);
        rec.setExp(null);
        rec.setSubject(null);
        objectToRequestParameters(rec, request);
        request.removeParameter("status");//Doesn't bind right
        request.removeParameter("paymentMethod");//Doesn't bind right
        request.removeParameter("payment.status");//Doesn't bind right
        request.setParameter("subject_id", "4");//reset subject to "twisted"
        request.setParameter("status_name", "STATUS_2");
        request.setParameter("paymentMethod_name", "MANUAL");
        DebugHelper.displayParams(request, log);
        ModelAndView mv = controller.handleRequest(request, new MockHttpServletResponse());
        Errors errors = (Errors) mv.getModel().get(BindException.MODEL_KEY_PREFIX + "participantRecord");
        if(errors != null)
            log.debug(errors.toString());
        assertNull(errors);
    }
    
    public void testDelete() throws Exception {
        log.debug("entering testdelete...");
        MockHttpServletRequest request = newPost("/participantrecordform.html");
        request.getSession().setAttribute("experiment", experimentManager.get(1L));
        request.setParameter("id", "1");
        request.addParameter("delete", "delete");
        DebugHelper.displayParams(request, log);
        ModelAndView mv = controller.handleRequest(request, new MockHttpServletResponse());
        
        //Check success view
        assertEquals(mv.getViewName(), controller.getSuccessView());
        
        //Check that p.r. has been deleted
        assertFalse(participantRecordManager.exists(1L));
    }

    public void testCancel() throws Exception {
        log.debug("entering testcancel...");
        MockHttpServletRequest request = newPost("/participantrecordform.html");
        request.getSession().setAttribute("experiment", experimentManager.get(1L));
        request.setParameter("id", "1");
        request.addParameter("cancel", "cancel");
        DebugHelper.displayParams(request, log);
        ModelAndView mv = controller.handleRequest(request, new MockHttpServletResponse());
        
        //Check success view
        assertEquals(mv.getViewName(), controller.getSuccessView());
    }
    
    /**
     * test stub until tests are fixed - poneil
     */
    public void testGet() throws Exception {
        log.debug("basic test");
        MockHttpServletRequest request = newGet("participantrecordform.html");
        request.setRemoteUser("admin");
        request.addUserRole(RecruitingConstants.ADMIN_ROLE);
        Experiment e = experimentManager.get(new Long(1));
        assertNotNull(e);
        request.getSession().setAttribute("experiment", e);
        ModelAndView mv = controller.handleRequest(request, new MockHttpServletResponse());
        assertNotNull(mv);        
    }

    public void setParticipantRecordFormController(ParticipantRecordFormController controller) {
        this.controller = controller;
    }

    public void setParticipantRecordManager(ParticipantRecordManager participantRecordManager) {
        this.participantRecordManager = participantRecordManager;
    }

    public void setExperimentManager(GenericManager<Experiment, Long> experimentManager) {
        this.experimentManager = experimentManager;
    }
    
}
