/*
 * Copyright (C) 1998-2009, <a href="http://www.ssel.caltech.edu">SSEL</a>,
 * at Caltech, Pasadena, CA 91125. 
 *
 * MooreRecruiting is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 * USA.
 *
 */

package edu.caltech.ssel.recruiting.webapp.controller;

import edu.caltech.ssel.recruiting.model.ExperimentType;
import org.appfuse.service.GenericManager;
import org.appfuse.webapp.controller.BaseControllerTestCase;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author Administrator
 */
public class ExperimentTypeFormControllerTest extends BaseControllerTestCase {
    private ExperimentTypeFormController c;
    private GenericManager<ExperimentType, Long> experimentTypeManager;
    
    public void setExperimentTypeFormController(ExperimentTypeFormController c){
        this.c = c;
    }
    
    public void testEdit() throws Exception {
        log.debug("testing edit...");
        MockHttpServletRequest request = newGet("/experimenttypeform.html");
        request.addParameter("id", "1");
        
        ModelAndView mv = c.handleRequest(request, new MockHttpServletResponse());
        
        ExperimentType type = (ExperimentType) mv.getModel().get(c.getCommandName());
        assertNotNull(type);
        assertEquals(type.getId(), new Long(1));
    }
    
    public void testSave() throws Exception {
        log.debug("testing save...");
        MockHttpServletRequest request = newGet("/experimenttypeform.html");
        request.addParameter("id", "1");
        ModelAndView mv = c.handleRequest(request, new MockHttpServletResponse());
        
        ExperimentType type = (ExperimentType) mv.getModel().get(c.getCommandName());
        assertNotNull(type);
        
        request = newPost("/experimenttypeform.html");
        super.objectToRequestParameters(type, request);
        request.setParameter("name", "Updated name");
        mv = c.handleRequest(request, new MockHttpServletResponse());
        
        Errors errors = (Errors) mv.getModel().get(BindException.MODEL_KEY_PREFIX + "person");
        assertNull(errors);
        assertNotNull(request.getSession().getAttribute("successMessages"));
        assertEquals(mv.getViewName(), c.getSuccessView());
        
        ExperimentType et = experimentTypeManager.get(1L);
        assertEquals(et.getName(), "Updated name");
    }
    
    public void testRemove() throws Exception {
        log.debug("testing remove...");
        MockHttpServletRequest request = newPost("/experimenttypeform.html");
        request.addParameter("delete", "");
        request.addParameter("id", "1");
        
        c.handleRequest(request, new MockHttpServletResponse());
        
        assertNotNull(request.getSession().getAttribute("successMessages"));
        
        assertFalse(experimentTypeManager.exists(1L));
    }

    public void testCancel() throws Exception {
        log.debug("testing cancel...");
        MockHttpServletRequest request = newPost("/experimenttypeform.html");
        request.addParameter("cancel", "");
        
        assertNotNull(c.handleRequest(request, new MockHttpServletResponse()));
    }

    public void setExperimentTypeManager(GenericManager<ExperimentType, Long> experimentTypeManager) {
        this.experimentTypeManager = experimentTypeManager;
    }
}
