/*
 * Copyright (C) 1998-2009, <a href="http://www.ssel.caltech.edu">SSEL</a>,
 * at Caltech, Pasadena, CA 91125. 
 *
 * MooreRecruiting is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 * USA.
 *
 */

package edu.caltech.ssel.recruiting.webapp.controller;

import edu.caltech.ssel.recruiting.common.ExperimentCreationHelper;
import edu.caltech.ssel.recruiting.model.Experiment;
import org.appfuse.webapp.controller.BaseControllerTestCase;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author michaelk
 */
public class ClearSessionControllerTest extends BaseControllerTestCase {
    
    private ClearSessionController c;
    private ExperimentCreationHelper ecHelper;
    
    /** Creates a new instance of ClearSessionControllerTest */
    public ClearSessionControllerTest() {
    }

    public void testExperiment() throws Exception {
        MockHttpServletRequest request = newGet("/clearsession.html");
        ecHelper.setSession(request.getSession());
        ecHelper.setExperiment(new Experiment());
        request.addParameter("id", "1");
        request.addParameter("view", "experiment");
        ModelAndView mv = c.handleRequest(request, new MockHttpServletResponse());
        
        assertNull(ecHelper.getExperiment());
        assertEquals("1", mv.getModel().get("id"));
        assertEquals(mv.getViewName(), c.VIEW_MAP.get("experiment"));
    }
    
    public void testSurvey() throws Exception {
        MockHttpServletRequest request = newGet("/clearsession.html");
        ecHelper.setSession(request.getSession());
        ecHelper.setExperiment(new Experiment());
        request.addParameter("id", "1");
        request.addParameter("view", "survey");
        ModelAndView mv = c.handleRequest(request, new MockHttpServletResponse());
        
        assertNull(ecHelper.getExperiment());
        assertEquals("1", mv.getModel().get("id"));
        assertEquals(mv.getViewName(), c.VIEW_MAP.get("survey"));
    }
    
    public void testFilter() throws Exception {
        MockHttpServletRequest request = newGet("/clearsession.html");
        ecHelper.setSession(request.getSession());
        ecHelper.setExperiment(new Experiment());
        request.addParameter("id", "1");
        request.addParameter("view", "filter");
        ModelAndView mv = c.handleRequest(request, new MockHttpServletResponse());
        
        assertNull(ecHelper.getExperiment());
        assertEquals("1", mv.getModel().get("id"));
        assertEquals(mv.getViewName(), c.VIEW_MAP.get("filter"));
    }
    
    public void testAnnouncement() throws Exception {
        MockHttpServletRequest request = newGet("/clearsession.html");
        ecHelper.setSession(request.getSession());
        ecHelper.setExperiment(new Experiment());
        request.addParameter("id", "1");
        request.addParameter("view", "announcement");
        ModelAndView mv = c.handleRequest(request, new MockHttpServletResponse());
        
        assertNull(ecHelper.getExperiment());
        assertEquals("1", mv.getModel().get("id"));
        assertEquals(mv.getViewName(), c.VIEW_MAP.get("announcement"));
    }
    
    public void setClearSessionController(ClearSessionController c) {
        this.c = c;
    }

    public void setExperimentCreationHelper(ExperimentCreationHelper ecHelper) {
        this.ecHelper = ecHelper;
    }
    
    
    
}
