/*
 * Copyright (C) 1998-2009, <a href="http://www.ssel.caltech.edu">SSEL</a>,
 * at Caltech, Pasadena, CA 91125. 
 *
 * MooreRecruiting is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 * USA.
 *
 */

package edu.caltech.ssel.recruiting.webapp.controller;
import edu.caltech.ssel.recruiting.common.QuestionAndAnswerList;
import edu.caltech.ssel.recruiting.common.RecruitingConstants;
import edu.caltech.ssel.recruiting.model.Experiment;
import edu.caltech.ssel.recruiting.model.QuestionResultCondition;
import edu.caltech.ssel.recruiting.service.QuestionResultManager;
import java.util.Set;
import org.appfuse.service.GenericManager;
import org.appfuse.webapp.controller.BaseControllerTestCase;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author michaelk
 */
public class SurveyFormControllerTest extends BaseControllerTestCase {
    
    private SurveyFormController c;
    private QuestionResultManager questionResultManager;
    private GenericManager<Experiment, Long> experimentManager;
    
    /**
     * Creates a new instance of SurveyFormControllerTest
     */
    public SurveyFormControllerTest() {
    }
    
    public void testLoad() throws Exception {
        log.debug("Entering testLoad...");
        
        MockHttpServletRequest request = newGet("/surveyform.html");
        request.addParameter("expId", "1");
        request.setRemoteUser("twisted");
        request.addUserRole(RecruitingConstants.SUBJ_ROLE);
        ModelAndView mv = c.handleRequest(request, new MockHttpServletResponse());
        
        QuestionAndAnswerList qal = (QuestionAndAnswerList) mv.getModel().get(c.getCommandName());
        
        assertNotNull(qal);
        assertNotNull(qal.getQuestions());
        assertTrue(qal.getQuestions().size() == 1); //There should be one unanswered question
    }
    
    public void testSubmit() throws Exception {
        log.debug("Entering testSubmit...");
   

        MockHttpServletRequest request;
        request = newGet("/surveyform.html");
        request.addParameter("expId", "1");
        request.setRemoteUser("twisted");
        ModelAndView mv = c.handleRequest(request, new MockHttpServletResponse());

        //See what's up with the filter
        Experiment e = experimentManager.get(1L);
        QuestionResultCondition qrc = (QuestionResultCondition) e.getFilter().getConditions().get(0);
        log.debug("QuestionResultCondition parameters...");
        log.debug("\tfilterType = " + qrc.getFilterType());
        log.debug("\tquestion_id = " + qrc.getQuestion().getId());
        log.debug("\tfilterOptions = ");
        Set<Integer> set = qrc.getFilterOptions();
        for(Integer i : set)
            log.debug("\t\t" + i.toString());
        
        request = newPost("/surveyform.html");
        request.addParameter("expId", "1");
        request.addParameter("question0", "0");
        request.setRemoteUser("twisted");
        mv = c.handleRequest(request, new MockHttpServletResponse());
        
        Errors errors = (Errors) mv.getModel().get(BindException.MODEL_KEY_PREFIX + "survey");
        assertNull(errors);
    }
    
    public void setSurveyFormController(SurveyFormController c) {
        this.c = c;
    }
    
    public void setQuestionResultManager(QuestionResultManager questionResultManager) {
        this.questionResultManager = questionResultManager;
    }
/*
    public void setQuestionAndAnswerListManager(GenericManager<QuestionAndAnswerList, Long> questionAndAnswerListManager) {
        this.questionAndAnswerListManager = questionAndAnswerListManager;
    }*/

    public void setExperimentManager(GenericManager<Experiment, Long> experimentManager) {
        this.experimentManager = experimentManager;
    }

}
