/*
 * Copyright (C) 1998-2009, <a href="http://www.ssel.caltech.edu">SSEL</a>,
 * at Caltech, Pasadena, CA 91125. 
 *
 * MooreRecruiting is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 * USA.
 *
 */

package edu.caltech.ssel.recruiting.webapp.controller;

import edu.caltech.ssel.recruiting.common.RecruitingConstants;
import edu.caltech.ssel.recruiting.model.Experimenter;
import edu.caltech.ssel.recruiting.model.Subject;
import org.springframework.security.AccessDeniedException;
import org.appfuse.service.UserManager;
import org.appfuse.model.User;
import org.appfuse.service.RoleManager;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.web.servlet.ModelAndView;
import org.appfuse.webapp.controller.*;
import org.springframework.orm.ObjectRetrievalFailureException;

public class UserFormControllerTest extends BaseControllerTestCase {
    private UserFormController c = null;
    private MockHttpServletRequest request;
    private ModelAndView mv;
    private UserManager userManager;
    private RoleManager roleManager;
    private static final String newFieldValue = "new field value";

    public void setUserFormController(UserFormController form) {
        this.c = form;
    }

    public void testLoadAddWithoutPermission() throws Exception {
        log.debug("testing add new user...");
        request = newGet("/userform.html");
        request.addParameter("method", "Add");
        request.setRemoteUser("bland");
        request.addUserRole(RecruitingConstants.SUBJ_ROLE);

        try {
            mv = c.handleRequest(request, new MockHttpServletResponse());
            fail("AccessDeniedException not thrown...");
        } catch (AccessDeniedException ade) {
            assertNotNull(ade.getMessage());
        }     
    }
    
    public void testLoadUserWithoutPermission() throws Exception {
        log.debug("testing edit...");
        request = newGet("/userform.html");
        request.addParameter("id", "1"); // tomcat
        request.setRemoteUser("bland");
        request.addUserRole(RecruitingConstants.SUBJ_ROLE);

        try {
            mv = c.handleRequest(request, new MockHttpServletResponse());
            fail("AccessDeniedException not thrown...");
        } catch (AccessDeniedException ade) {
            assertNotNull(ade.getMessage());
        }
    }

    public void testLoadAddSubject() throws Exception {
        log.debug("testing add new subject...");
        request = newGet("/userform.html");
        request.addParameter("method", "Add");
        request.addParameter("from", "list");
        request.addUserRole(RecruitingConstants.ADMIN_ROLE);
        request.setRemoteUser("user");//Pure admin

        mv = c.handleRequest(request, new MockHttpServletResponse());
        
        User user = (User) mv.getModel().get(c.getCommandName());
        assertTrue(user instanceof Subject);
        assertNull(user.getUsername());
        assertTrue(user.getRoles().contains(roleManager.getRole(RecruitingConstants.SUBJ_ROLE)));
    }

    public void testLoadAddExperimenter() throws Exception {
        log.debug("testing add new experimenter...");
        request = newGet("/userform.html");
        request.addParameter("method", "Add");
        request.addParameter("from", "list");
        request.addParameter("addUserType", "Experimenter");
        request.addUserRole(RecruitingConstants.ADMIN_ROLE);
        request.setRemoteUser("user");//Pure admin

        mv = c.handleRequest(request, new MockHttpServletResponse());
        
        User user = (User) mv.getModel().get(c.getCommandName());
        assertTrue(user instanceof Experimenter);
        assertNull(user.getUsername());
        assertTrue(user.getRoles().contains(roleManager.getRole(RecruitingConstants.EXPTR_ROLE)));
    }

    public void testLoadAddAdmin() throws Exception {
        log.debug("testing add new administrator...");
        request = newGet("/userform.html");
        request.addParameter("method", "Add");
        request.addParameter("from", "list");
        request.addParameter("addUserType", "Administrator");
        request.addUserRole(RecruitingConstants.ADMIN_ROLE);
        request.setRemoteUser("user");//Pure admin

        mv = c.handleRequest(request, new MockHttpServletResponse());
        
        User user = (User) mv.getModel().get(c.getCommandName());
        assertFalse(user instanceof Experimenter);
        assertFalse(user instanceof Subject);
        assertNull(user.getUsername());
        assertTrue(user.getRoles().contains(roleManager.getRole(RecruitingConstants.ADMIN_ROLE)));
    }

    public void testLoadFromList() throws Exception {
        log.debug("testing edit user...");
        request = newGet("/userform.html");
        request.addParameter("id", "1"); // tomcat
        request.addParameter("from", "list");
        request.setRemoteUser("user");
        request.addUserRole(RecruitingConstants.ADMIN_ROLE);

        mv = c.handleRequest(request, new MockHttpServletResponse());

        assertEquals("userForm", mv.getViewName());
        User user = (User) mv.getModel().get(c.getCommandName());
        assertEquals("user", user.getUsername());
    }

    public void testLoadSubjectProfile() throws Exception {
        log.debug("testing edit subject profile...");
        request = newGet("/userform.html");
        request.setRemoteUser("bland");
        request.addUserRole(RecruitingConstants.SUBJ_ROLE);

        mv = c.handleRequest(request, new MockHttpServletResponse());

        assertEquals("userForm", mv.getViewName());
        Subject user = (Subject) mv.getModel().get(c.getCommandName());
        assertEquals("bland", user.getUsername());
        assertNotNull(user.getSchoolId());
    }

    public void testLoadExperimenterProfile() throws Exception {
        log.debug("testing edit experimenter profile...");
        request = newGet("/userform.html");
        request.setRemoteUser("michaelk");
        request.addUserRole(RecruitingConstants.EXPTR_ROLE);

        mv = c.handleRequest(request, new MockHttpServletResponse());

        assertEquals("userForm", mv.getViewName());
        Experimenter user = (Experimenter) mv.getModel().get(c.getCommandName());
        assertEquals("michaelk", user.getUsername());
        assertNotNull(user.getAffiliation());
    }

    public void testSubmitDuplicateUsername() throws Exception {
        request = newPost("/userform.html");
        request.setRemoteUser("admin");
        request.addParameter("from", "list");
        request.addUserRole(RecruitingConstants.ADMIN_ROLE);
        User user = userManager.getUserByUsername("michaelk");
        objectToRequestParameters(user, request);
        request.setParameter("username", "admin");
        String [] userRoles = new String[] {RecruitingConstants.ADMIN_ROLE};
        request.addParameter("userRoles", userRoles);
        request.setParameter("title", "MR");
        
        mv = c.handleRequest(request, new MockHttpServletResponse());
        
        //Make sure it catches that the username is the same and doesn't crash
        assertFalse(mv.getViewName().equals(c.getAdminSuccessView()));
    }
    
    public void testAdminSubmitEditAdmin() throws Exception {
        request = newPost("/userform.html");
        request.setRemoteUser("user");
        request.addParameter("from", "list");
        request.addUserRole(RecruitingConstants.ADMIN_ROLE);
        User user = userManager.getUserByUsername("user");
        objectToRequestParameters(user, request);
        String [] userRoles = new String[] {RecruitingConstants.ADMIN_ROLE};
        request.addParameter("userRoles", userRoles);
        request.setParameter("firstName", newFieldValue);
        
        mv = c.handleRequest(request, new MockHttpServletResponse());
        
        assertEquals(mv.getViewName(), c.getAdminSuccessView());
        assertEquals(newFieldValue, userManager.getUserByUsername("user").getFirstName());
    }
    
    public void testAdminSubmitEditExptr() throws Exception {
        request = newPost("/userform.html");
        request.setRemoteUser("user");
        request.addParameter("from", "list");
        request.addUserRole(RecruitingConstants.ADMIN_ROLE);
        User user = userManager.getUserByUsername("michaelk");
        objectToRequestParameters(user, request);
        String [] userRoles = new String[] {RecruitingConstants.EXPTR_ROLE};
        request.addParameter("userRoles", userRoles);
        request.setParameter("affiliation", newFieldValue);
        request.setParameter("title", "DR");
        
        mv = c.handleRequest(request, new MockHttpServletResponse());
        
        assertEquals(mv.getViewName(), c.getAdminSuccessView());
        assertEquals(newFieldValue, ((Experimenter) userManager.getUserByUsername("michaelk")).getAffiliation());
    }
    
    public void testAdminSubmitEditSubj() throws Exception {
        request = newPost("/userform.html");
        request.setRemoteUser("admin");
        request.addParameter("from", "list");
        request.addUserRole(RecruitingConstants.ADMIN_ROLE);
        User user = userManager.getUserByUsername("bland");
        objectToRequestParameters(user, request);
        String [] userRoles = new String[] {RecruitingConstants.SUBJ_ROLE};
        request.addParameter("userRoles", userRoles);
        request.setParameter("schoolId", newFieldValue);
        Subject s = (Subject)user;
        request.setParameter("birthday_day", Integer.toString(s.getBirthday().getDate()));
        request.setParameter("birthday_month", Integer.toString(s.getBirthday().getMonth()));
        request.setParameter("birthday_year", Integer.toString(s.getBirthday().getYear()));
        //Set the answers to the profile questions
        //No checked boxes on question0, answer 0 on the other 2 questions
        request.setParameter("question1", "0");
        request.setParameter("question2", "0");
        
        mv = c.handleRequest(request, new MockHttpServletResponse());
        
        assertEquals(mv.getViewName(), c.getAdminSuccessView());
        assertEquals(newFieldValue, ((Subject)userManager.getUserByUsername("bland")).getSchoolId());
    }
    
    /*public void testAdminSubmitAddAdmin() throws Exception {
        request = newGet("/userform.html");
        request.setRemoteUser("user");
        request.addUserRole(RecruitingConstants.ADMIN_ROLE);
        User user = userManager.getUserByUsername("user");
        objectToRequestParameters(user, request);
        request.removeParameter("id");//Because it's a new user
        //Reset unique fields
        request.setParameter("username", "differentusername");
        request.setParameter("email", "different@email.com");
        request.addParameter("from", "list");
        String [] userRoles = new String[] {RecruitingConstants.ADMIN_ROLE};
        request.addParameter("userRoles", userRoles);
        request.setParameter("firstName", newFieldValue);
        
        mv = c.handleRequest(request, new MockHttpServletResponse());
        
        assertEquals(mv.getViewName(), c.getAdminSuccessView());
        assertEquals(newFieldValue, userManager.getUserByUsername("differentusername").getFirstName());
    }
    
    public void testAdminSubmitAddExptr() throws Exception {
        request = newGet("/userform.html");
        request.setRemoteUser("user");
        request.addUserRole(RecruitingConstants.EXPTR_ROLE);

        String [] userRoles = new String[] {RecruitingConstants.EXPTR_ROLE};
        request.setParameter("userRoles", userRoles);
        request.addParameter("username", "self-registered");
        request.addParameter("password", "Password1");
        request.addParameter("confirmPassword", "Password1");
        request.addParameter("firstName", "First");
        request.addParameter("lastName", "Last");
        request.addParameter("address.city", "Pasadena");
        request.addParameter("address.province", "California");
        request.addParameter("address.country", "USA");
        request.addParameter("address.postalCode", "91007");
        request.addParameter("email", "xpwalter@gmail.com");
        String[] userPools = new String[] {"1"};
        request.setParameter("userPools", userPools);
        request.setParameter("birthday_day","3");
        request.setParameter("birthday_month","3");
        request.setParameter("birthday_year","2007");
        request.setParameter("schoolId","1567863");
        //Set the answers to the profile questions
        //No checked boxes on question0, answer 0 on the other 2 questions
        request.setParameter("question1", "0");
        request.setParameter("question2", "0");
        request.setParameter("affiliation", newFieldValue);
        request.setParameter("title", "DR");
        
        mv = c.handleRequest(request, new MockHttpServletResponse());
        
        assertEquals(mv.getViewName(), c.getAdminSuccessView());
        assertEquals(newFieldValue, ((Experimenter)userManager.getUserByUsername("differentusername")).getAffiliation());
    }
    
    public void testAdminSubmitAddSubj() throws Exception {
        request = newPost("/userform.html");
        request.setRemoteUser("user");
        request.addUserRole(RecruitingConstants.ADMIN_ROLE);
        User user = userManager.getUserByUsername("bland");
        objectToRequestParameters(user, request);
        request.removeParameter("id");//Because it's a new user
        request.setParameter("username", "differentusername");
        request.setParameter("email", "different@email.com");
        request.addParameter("from", "list");
        String [] userRoles = new String[] {RecruitingConstants.SUBJ_ROLE};
        request.addParameter("userRoles", userRoles);
        request.setParameter("schoolId", newFieldValue);
        Subject s = (Subject)user;
        request.setParameter("birthday_day", Integer.toString(s.getBirthday().getDate()));
        request.setParameter("birthday_month", Integer.toString(s.getBirthday().getMonth()));
        request.setParameter("birthday_year", Integer.toString(s.getBirthday().getYear()));
        //Set the answers to the profile questions
        //No checked boxes on question0, answer 0 on the other 2 questions
        request.setParameter("question1", "0");
        request.setParameter("question2", "0");
        
        mv = c.handleRequest(request, new MockHttpServletResponse());
        
        assertEquals(mv.getViewName(), c.getAdminSuccessView());
        assertEquals(newFieldValue, ((Subject)userManager.getUserByUsername("differentusername")).getSchoolId());
    }*/
    
    public void testSubmitEditSubjectProfile() throws Exception {
        request = newPost("/userform.html");
        request.setRemoteUser("bland");
        request.addUserRole(RecruitingConstants.SUBJ_ROLE);
        User user = userManager.getUserByUsername("bland");
        objectToRequestParameters(user, request);
        String [] userRoles = new String[] {RecruitingConstants.SUBJ_ROLE};
        request.addParameter("userRoles", userRoles);
        request.setParameter("schoolId", newFieldValue);
        Subject s = (Subject)user;
        request.setParameter("birthday_day", Integer.toString(s.getBirthday().getDate()));
        request.setParameter("birthday_month", Integer.toString(s.getBirthday().getMonth()));
        request.setParameter("birthday_year", Integer.toString(s.getBirthday().getYear()));
        //Set the answers to the profile questions
        //No checked boxes on question0, answer 0 on the other 2 questions
        request.setParameter("question1", "0");
        request.setParameter("question2", "0");
        
        mv = c.handleRequest(request, new MockHttpServletResponse());
        
        assertEquals(mv.getViewName(), c.getSubjectSuccessView());
        assertEquals(newFieldValue, ((Subject)userManager.getUserByUsername("bland")).getSchoolId());
    }
    
    public void testSubmitEditExperimenterProfile() throws Exception {
        request = newPost("/userform.html");
        request.setRemoteUser("michaelk");
        request.addUserRole(RecruitingConstants.EXPTR_ROLE);
        User user = userManager.getUserByUsername("michaelk");
        objectToRequestParameters(user, request);
        String [] userRoles = new String[] {RecruitingConstants.EXPTR_ROLE};
        request.addParameter("userRoles", userRoles);
        request.setParameter("affiliation", newFieldValue);
        request.setParameter("title", "DR");
        
        mv = c.handleRequest(request, new MockHttpServletResponse());
        
        assertEquals(mv.getViewName(), c.getExperimenterSuccessView());
        assertEquals(newFieldValue, ((Experimenter)userManager.getUserByUsername("michaelk")).getAffiliation());
    }
    
    public void testRemove() throws Exception {
        request = newPost("/userform.html");
        request.addParameter("delete", "delete");
        request.addParameter("id", "2");
        request.setRemoteUser("admin");
        request.addUserRole(RecruitingConstants.ADMIN_ROLE);

        mv = c.handleRequest(request, new MockHttpServletResponse());
        
        assertNotNull(request.getSession().getAttribute("successMessages"));
        try {
            userManager.getUser("2");
            fail("Should have thrown exception");
        } catch(ObjectRetrievalFailureException exc) {
            log.debug("Expected exception - user was deleted");
        }
    }

    public void setUserManager(UserManager userManager) {
        this.userManager = userManager;
    }

    public void setRoleManager(RoleManager roleManager) {
        this.roleManager = roleManager;
    }
}
