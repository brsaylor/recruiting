/*
 * Copyright (C) 1998-2009, <a href="http://www.ssel.caltech.edu">SSEL</a>,
 * at Caltech, Pasadena, CA 91125. 
 *
 * MooreRecruiting is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 * USA.
 *
 */

package edu.caltech.ssel.recruiting.webapp.controller;

import edu.caltech.ssel.recruiting.common.ExperimentCreationHelper;
import edu.caltech.ssel.recruiting.model.Experiment;
import edu.caltech.ssel.recruiting.model.Filter;
import org.appfuse.service.GenericManager;
import org.appfuse.webapp.controller.BaseControllerTestCase;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;

/**
 *
 * @author michaelk
 */
public class FilterFormControllerTest extends BaseControllerTestCase {
    
    private FilterFormController c;
    private GenericManager<Experiment, Long> experimentManager;
    private GenericManager<Filter, Long> filterManager;
    private ExperimentCreationHelper ecHelper;
    
    /** Creates a new instance of FilterFormControllerTest */
    public FilterFormControllerTest() {
    }

    public void setFilterFormController(FilterFormController c) {
        this.c = c;
    }
    
    public void testLoadNoFlow() throws Exception {
        log.debug("testing load no flow...");
        MockHttpServletRequest request = newGet("/filterform.html");
        request.getSession().setAttribute("experiment", experimentManager.get(1L));
        ModelAndView mv = c.handleRequest(request, new MockHttpServletResponse());
        
        Filter f = (Filter)mv.getModel().get(c.getCommandName());
        assertNotNull(f);
    }
    
    public void testPostNoFlow() throws Exception {
        log.debug("testing post no flow...");
        MockHttpServletRequest request = newPost("/filterform.html");
        request.getSession().setAttribute("experiment", experimentManager.get(1L));
        request.setRemoteUser("michaelk");
        request.setParameter("id", "1");
        request.setParameter("savenow", "savenow");
        request.setParameter("fcnt", "2");
        request.setParameter("filter_id_0", "1");
        request.setParameter("filter_id_1", "2");
        request.setParameter("filterRadio", "EXISTING_FILTER");
        request.setParameter("existingFilter", "1");
        request.setParameter("name", "Test Filter 1");
        request.setParameter("checkedIRB", "true");
        ModelAndView mv = c.handleRequest(request, new MockHttpServletResponse());
        Errors errors = (Errors) mv.getModel().get(BindException.MODEL_KEY_PREFIX + "filter");
        if(errors != null)
            log.debug(errors.toString());
        assertNull(errors);
        
    }

    public void testLoadFlow() throws Exception {
        log.debug("testing loadflow...");
        MockHttpServletRequest request = newGet("/filterform.html");
        ecHelper.setSession(request.getSession());
        ecHelper.setUseSurvey(true);
        ecHelper.setUseFilter(false);
        ecHelper.setUseAnnouncement(false);
        ecHelper.setExperiment(experimentManager.get(3L));
        request.setParameter("id", "1");
        ModelAndView mv = c.handleRequest(request, new MockHttpServletResponse());
        
        Filter f = (Filter)mv.getModel().get(c.getCommandName());
        assertEquals(f.getId(), new Long(1));
        assertNotNull(mv.getModel().get("qlist"));
        assertNotNull(mv.getModel().get("typeLVList"));
        assertNotNull(mv.getModel().get("poolLVList"));
    }
    
    public void testPostFlow() throws Exception {
        log.debug("testing post flow...");
        MockHttpServletRequest request = newPost("/filterform.html");
        request.getSession().setAttribute("experiment", experimentManager.get(1L));
        request.setRemoteUser("michaelk");
        request.setParameter("id", "1");
        request.setParameter("next", "next");
        request.setParameter("fcnt", "2");
        request.setParameter("filter_id_0", "1");
        request.setParameter("filter_id_1", "2");
        request.setParameter("filterRadio", "EXISTING_FILTER");
        request.setParameter("existingFilter", "1");
        request.setParameter("name", "Test Filter 1");
        request.setParameter("checkedIRB", "true");
        ModelAndView mv = c.handleRequest(request, new MockHttpServletResponse());
        Errors errors = (Errors) mv.getModel().get(BindException.MODEL_KEY_PREFIX + "filter");
        if(errors != null)
            log.debug(errors.toString());
        assertNull(errors);
    }

    public void setExperimentManager(GenericManager<Experiment, Long> experimentManager) {
        this.experimentManager = experimentManager;
    }

    public void setExperimentCreationHelper(ExperimentCreationHelper ecHelper) {
        this.ecHelper = ecHelper;
    }

    public void setFilterManager(GenericManager<Filter, Long> filterManager) {
        this.filterManager = filterManager;
    }
    
}
