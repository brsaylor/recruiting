/*
 * Copyright (C) 1998-2009, <a href="http://www.ssel.caltech.edu">SSEL</a>,
 * at Caltech, Pasadena, CA 91125. 
 *
 * MooreRecruiting is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 * USA.
 *
 */

package edu.caltech.ssel.recruiting.webapp.controller;

import edu.caltech.ssel.recruiting.common.RecruitingConstants;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.web.servlet.ModelAndView;
import org.appfuse.webapp.controller.BaseControllerTestCase;

/**
 *
 * @author michaelk
 */
public class OnExpSelControllerTest extends BaseControllerTestCase {
    
    private OnExpSelController c; 
    
    /** Creates a new instance of OnExpSelControllerTest */
    public OnExpSelControllerTest() {
    }
    
    public void testSubjectForward() throws Exception {
        MockHttpServletRequest request = newGet("/onexpsel.html");
        request.setRemoteUser("bland");
        request.addUserRole(RecruitingConstants.SUBJ_ROLE);
        request.addParameter("expId", "1");
        
        ModelAndView mv = c.handleRequest(request, new MockHttpServletResponse());
        
        assertEquals(mv.getModel().get("expId"), "1");
        assertEquals(mv.getViewName(), c.getSubjectSuccessView());
    }

    public void testExperimenterForward() throws Exception {
        MockHttpServletRequest request = newGet("/onexpsel.html");
        request.setRemoteUser("michaelk");
        request.addUserRole(RecruitingConstants.EXPTR_ROLE);
        request.addParameter("expId", "1");
        
        ModelAndView mv = c.handleRequest(request, new MockHttpServletResponse());
        
        assertEquals(mv.getModel().get("id"), "1");
        assertEquals(mv.getViewName(), c.getExperimenterSuccessView());
    }

    public void testAdministratorForward() throws Exception {
        MockHttpServletRequest request = newGet("/onexpsel.html");
        request.setRemoteUser("user");
        request.addUserRole(RecruitingConstants.ADMIN_ROLE);
        request.addParameter("expId", "1");
        
        ModelAndView mv = c.handleRequest(request, new MockHttpServletResponse());
        
//        assertEquals(mv.getViewName(), c.getAdministratorSuccessView());
        assertEquals(mv.getViewName(), c.getExperimenterSuccessView());
    }

    public void setOnExpSelController(OnExpSelController c) {
        this.c = c;
    }
    
}
