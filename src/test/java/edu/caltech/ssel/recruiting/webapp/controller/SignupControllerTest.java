/*
 * Copyright (C) 1998-2009, <a href="http://www.ssel.caltech.edu">SSEL</a>,
 * at Caltech, Pasadena, CA 91125. 
 *
 * MooreRecruiting is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 * USA.
 *
 */

package edu.caltech.ssel.recruiting.webapp.controller;

import edu.caltech.ssel.recruiting.model.Subject;
import edu.caltech.ssel.recruiting.model.SubjectPool;
import edu.caltech.ssel.recruiting.service.SubjectPoolManager;
import java.text.SimpleDateFormat;
import javax.servlet.http.HttpServletResponse;

import org.appfuse.Constants;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.servlet.ModelAndView;
import org.subethamail.wiser.Wiser;
import org.springframework.security.context.SecurityContextHolder;
import org.appfuse.service.GenericManager;
import org.appfuse.service.UserManager;
import org.appfuse.webapp.controller.*;

public class SignupControllerTest extends BaseControllerTestCase {
    private SignupController c = null;
    private GenericManager<SubjectPool,Long> subjectPoolManager;
    private UserManager userManager;
    
    public void setSignupController(SignupController signup) {
        this.c = signup;
    }
    
    public void testDisplayForm() throws Exception {
        MockHttpServletRequest request = newGet("/signup.html");
        HttpServletResponse response = new MockHttpServletResponse();
        ModelAndView mv = c.handleRequest(request, response);
        assertTrue("returned correct view name", mv.getViewName().equals("signup"));
        
        //Check that the reference data has been set
        assertNotNull(mv.getModel().get("availLVPools"));
        assertNotNull(mv.getModel().get("profile_qs"));
        assertNotNull(mv.getModel().get("monthList"));
    }
    
    public void testSignupUser() throws Exception {
        MockHttpServletRequest request = newPost("/signup.html");
        request.addParameter("username", "self-registered");
        request.addParameter("password", "Password1");
        request.addParameter("confirmPassword", "Password1");
        request.addParameter("firstName", "First");
        request.addParameter("lastName", "Last");
        request.addParameter("address.city", "Pasadena");
        request.addParameter("address.province", "California");
        request.addParameter("address.country", "USA");
        request.addParameter("address.postalCode", "91007");
        request.addParameter("email", "xpwalter@gmail.com");
        String[] userPools = new String[] {"1"};
        request.setParameter("userPools", userPools);
        request.setParameter("birthday_day","3");
        request.setParameter("birthday_month","3");
        request.setParameter("birthday_year","2007");
        request.setParameter("schoolId","1567863");
        //Set the answers to the profile questions
        //No checked boxes on question0, answer 0 on the other 2 questions
        request.setParameter("question1", "0");
        request.setParameter("question2", "0");
        
        HttpServletResponse response = new MockHttpServletResponse();
        
        // start SMTP Server
        /*Wiser wiser = new Wiser();
        wiser.setPort(2525);
        wiser.start();
        */
        ModelAndView mv = c.handleRequest(request, response);
        Errors errors = (Errors) mv.getModel().get(BindException.MODEL_KEY_PREFIX + "user");
        assertTrue("no errors returned in model", errors == null);
        
        // verify an account information e-mail was sent
        //wiser.stop();
        //assertTrue(wiser.getMessages().size() == 1);
        
        // verify that success messages are in the request
        assertNotNull(request.getSession().getAttribute("successMessages"));
        assertNotNull(request.getSession().getAttribute(Constants.REGISTERED));
        
        SecurityContextHolder.getContext().setAuthentication(null);

        //Check birthday (random field) of user
        Subject subj = (Subject)userManager.getUserByUsername("self-registered");
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
        assertEquals(sdf.format(subj.getBirthday()), "04/03/2007");
    }

    public void setSubjectPoolManager(SubjectPoolManager subjectPoolManager) {
        this.subjectPoolManager = subjectPoolManager;
    }

    public void setUserManager(UserManager userManager) {
        this.userManager = userManager;
    }
}
