/*
 * Copyright (C) 1998-2009, <a href="http://www.ssel.caltech.edu">SSEL</a>,
 * at Caltech, Pasadena, CA 91125. 
 *
 * MooreRecruiting is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 * USA.
 *
 */

package edu.caltech.ssel.recruiting.webapp.controller;

import edu.caltech.ssel.recruiting.model.Experiment;
import edu.caltech.ssel.recruiting.model.ParticipantRecord;
import java.util.List;
import org.appfuse.service.GenericManager;
import edu.caltech.ssel.recruiting.common.RecruitingConstants;
import org.appfuse.webapp.controller.BaseControllerTestCase;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author michaelk
 */
public class ParticipantRecordControllerTest extends BaseControllerTestCase {
    
    private ParticipantRecordController controller;
    private GenericManager<Experiment, Long> experimentManager;
    
    /** Creates a new instance of ParticipantRecordControllerTest */
    public ParticipantRecordControllerTest() {
    }

    public void testLoad() throws Exception {
        MockHttpServletRequest request = newGet("/participantrecord.html");
        request.getSession().setAttribute("experiment", experimentManager.get(1L));
        ModelAndView mv = controller.handleRequest(request, new MockHttpServletResponse());
        
        List<ParticipantRecord> lst = (List<ParticipantRecord>)mv.getModel().get("participantRecordList");
        assertNotNull(lst);
        assertTrue(lst.size() > 0);//Should have user bland signed up
    }
    
    /**
     * test stub until tests are fixed - poneil
     */
    public void testGet() throws Exception {
        log.debug("basic test");
        MockHttpServletRequest request = newGet("participantrecord.html");
        request.setRemoteUser("admin");
        request.addUserRole(RecruitingConstants.ADMIN_ROLE);
        ModelAndView mv = controller.handleRequest(request, new MockHttpServletResponse());
        assertNotNull(mv);        
    }

    public void setParticipantRecordController(ParticipantRecordController controller) {
        this.controller = controller;
    }

    public void setExperimentManager(GenericManager<Experiment, Long> experimentManager) {
        this.experimentManager = experimentManager;
    }
    
}
