/*
 * Copyright (C) 1998-2009, <a href="http://www.ssel.caltech.edu">SSEL</a>,
 * at Caltech, Pasadena, CA 91125. 
 *
 * MooreRecruiting is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 * USA.
 *
 */

package edu.caltech.ssel.recruiting.webapp.controller;

import edu.caltech.ssel.recruiting.common.RecruitingConstants;
import org.appfuse.webapp.controller.BaseControllerTestCase;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;

/**
 *
 * @author michaelk
 */
public class ExperimenterControllerTest extends BaseControllerTestCase {
    
    private ExperimenterController experimenterController;
    
    /** Creates a new instance of ExperimenterControllerTest */
    public ExperimenterControllerTest() {
    }
    
    public void testWorks() throws Exception {
        //Make sure that it doesn't randomly crash for some reason
        MockHttpServletRequest request = newGet("/experimenter.html");
        request.setRemoteUser("michaelk");
        request.addUserRole(RecruitingConstants.EXPTR_ROLE);
        experimenterController.handleRequest(request, new MockHttpServletResponse());
    }
    
    //There is room here for tests later in case we add functionality to this form

    public void setExperimenterController(ExperimenterController experimenterController) {
        this.experimenterController = experimenterController;
    }
    
}
