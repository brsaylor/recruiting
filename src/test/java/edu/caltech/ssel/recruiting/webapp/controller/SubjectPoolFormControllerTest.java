/*
 * Copyright (C) 1998-2009, <a href="http://www.ssel.caltech.edu">SSEL</a>,
 * at Caltech, Pasadena, CA 91125. 
 *
 * MooreRecruiting is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 * USA.
 *
 */

package edu.caltech.ssel.recruiting.webapp.controller;

import edu.caltech.ssel.recruiting.model.SubjectPool;
import org.appfuse.service.GenericManager;
import org.appfuse.webapp.controller.BaseControllerTestCase;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author Administrator
 */
public class SubjectPoolFormControllerTest extends BaseControllerTestCase {
    private SubjectPoolFormController c;
    private GenericManager<SubjectPool, Long> subjectPoolManager;
    
    public void setSubjectPoolFormController(SubjectPoolFormController c){
        this.c = c;
    }
    
    public void testEdit() throws Exception {
        log.debug("testing edit...");
        MockHttpServletRequest request = newGet("/subjectpoolform.html");
        request.addParameter("id", "1");
        
        ModelAndView mv = c.handleRequest(request, new MockHttpServletResponse());
        
        SubjectPool pool = (SubjectPool) mv.getModel().get(c.getCommandName());
        assertNotNull(pool);
        assertEquals(new Long(1), pool.getId());
    }
    
    public void testSave() throws Exception {
        log.debug("testing save...");
        MockHttpServletRequest request = newGet("/subjectpoolform.html");
        request.addParameter("id", "1");
        ModelAndView mv = c.handleRequest(request, new MockHttpServletResponse());
        
        SubjectPool pool = (SubjectPool) mv.getModel().get(c.getCommandName());
        assertNotNull(pool);
        
        request = newPost("/subjectpoolform.html");
        super.objectToRequestParameters(pool, request);
        request.setParameter("name", "Updated name");
        request.addParameter("save", "save");
        mv = c.handleRequest(request, new MockHttpServletResponse());
        
        Errors errors = (Errors) mv.getModel().get(BindException.MODEL_KEY_PREFIX + "person");
        assertNull(errors);
        assertNotNull(request.getSession().getAttribute("successMessages"));
        
        assertEquals(c.getSuccessView(), mv.getViewName());
        assertEquals(subjectPoolManager.get(1L).getName(), "Updated name");
    }
    
    public void testRemove() throws Exception {
        MockHttpServletRequest request = newPost("/subjectpoolform.html");
        request.addParameter("delete", "delete");
        request.addParameter("id", "2");
        
        ModelAndView mv = c.handleRequest(request, new MockHttpServletResponse());
        
        assertNotNull(request.getSession().getAttribute("successMessages"));

        assertEquals(c.getSuccessView(), mv.getViewName());
        assertFalse(subjectPoolManager.exists(2L));
    }

    public void setSubjectPoolManager(GenericManager<SubjectPool, Long> subjectPoolManager) {
        this.subjectPoolManager = subjectPoolManager;
    }
}
