/*
 * Copyright (C) 1998-2009, <a href="http://www.ssel.caltech.edu">SSEL</a>,
 * at Caltech, Pasadena, CA 91125. 
 *
 * MooreRecruiting is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 * USA.
 *
 */

package edu.caltech.ssel.recruiting.webapp.controller;

import edu.caltech.ssel.recruiting.common.QuestionAndAnswerList;
import edu.caltech.ssel.recruiting.common.RecruitingConstants;
import org.appfuse.webapp.controller.BaseControllerTestCase;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author michaelk
 */
public class CheckProfileFormControllerTest extends BaseControllerTestCase {
    
    private CheckProfileFormController controller;
    
    /** Creates a new instance of CheckProfileFormControllerTest */
    public CheckProfileFormControllerTest() {
    }
    
    public void testLoadSubject() throws Exception {
        MockHttpServletRequest request = newGet("/checkprofileform.html");
        request.setRemoteUser("twisted");//User id = 4, no questions answered
        request.addUserRole(RecruitingConstants.SUBJ_ROLE);
        ModelAndView mv = controller.handleRequest(request, new MockHttpServletResponse());

        QuestionAndAnswerList qal = (QuestionAndAnswerList) mv.getModel().get(controller.getCommandName());
        log.debug("Question list size: " + qal.getQuestions().size());
        assertNotNull(qal);
        assertTrue(qal.getQuestions().size() > 0);
    }

    public void testLoadNonSubject() throws Exception {
        MockHttpServletRequest request = newGet("/checkprofileform.html");
        request.setRemoteUser("michaelk");
        request.addUserRole(RecruitingConstants.EXPTR_ROLE);
        ModelAndView mv = controller.handleRequest(request, new MockHttpServletResponse());

        assertEquals(mv.getViewName(), "redirect:" + controller.getExperimenterDefaultView());
    }
    
    public void testSubmit() throws Exception {
        MockHttpServletRequest request = newGet("/checkprofileform.html");
        request.setRemoteUser("twisted");//User id = 4, no questions answered

        controller.handleRequest(request, new MockHttpServletResponse());
        
        request = newPost("/checkprofileform.html");
        request.setRemoteUser("twisted");
        request.addUserRole(RecruitingConstants.SUBJ_ROLE);
        //No checked boxes on question0, answer 0 on the other 2 questions
        request.setParameter("question1", "0");
        request.setParameter("question2", "0");
        
        ModelAndView mv = controller.handleRequest(request, new MockHttpServletResponse());
        
        assertEquals(mv.getViewName(), "redirect:" + controller.getSubjectDefaultView());
        
        //Assert that there are now no questions left unanswered
        request = newGet("/checkprofileform.html");
        request.setRemoteUser("twisted");//User id = 4, no questions answered
        request.addUserRole(RecruitingConstants.SUBJ_ROLE);
        mv = controller.handleRequest(request, new MockHttpServletResponse());

        QuestionAndAnswerList qal = (QuestionAndAnswerList) mv.getModel().get(controller.getCommandName());
        assertTrue(qal == null || qal.getQuestions() == null || qal.getQuestions().size() == 0);
        assertEquals(mv.getViewName(), "redirect:" + controller.getSubjectDefaultView());
    }

    public void setCheckProfileFormController(CheckProfileFormController controller) {
        this.controller = controller;
    }
    
}
