/*
 * Copyright (C) 1998-2009, <a href="http://www.ssel.caltech.edu">SSEL</a>,
 * at Caltech, Pasadena, CA 91125. 
 *
 * MooreRecruiting is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 * USA.
 *
 */

package edu.caltech.ssel.recruiting.webapp.controller;

import edu.caltech.ssel.recruiting.common.RecruitingConstants;
import org.appfuse.model.User;
import org.appfuse.service.UserManager;
import org.appfuse.webapp.controller.BaseControllerTestCase;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.web.servlet.ModelAndView;
import org.subethamail.wiser.Wiser;

/**
 *
 * @author michaelk
 */
public class ResetPasswordControllerTest extends BaseControllerTestCase {
    
    private ResetPasswordController controller;
    private UserManager userManager;
    
    /** Creates a new instance of ResetPasswordFormControllerTest */
    public ResetPasswordControllerTest() {
    }
    
    public void testLoad() throws Exception {
        ModelAndView mv = controller.handleRequest(newGet("/resetpassword.html"), new MockHttpServletResponse());
        
        assertNotNull(mv.getModel().get(controller.getCommandName()));
    }
    
    public void testSubmit() throws Exception {
        // start SMTP Server
        Wiser wiser = new Wiser();
        wiser.setPort(2525);
        wiser.start();

        MockHttpServletRequest request = newPost("/resetpassword.html");
        request.setParameter("username", "bland");
        request.setParameter("email", "john@blah.com");
        request.setParameter("firstName", "John");
        request.setParameter("lastName", "Doe");
        
        ModelAndView mv = controller.handleRequest(request, new MockHttpServletResponse());

        User u = userManager.getUserByUsername("bland");
        assertEquals(mv.getViewName(), controller.getSuccessView());
        assertFalse(u.getPassword().equals("d033e22ae348aeb5660fc2140aec35850c4da997"));
    }

    public void setResetPasswordController(ResetPasswordController controller) {
        this.controller = controller;
    }

    public void setUserManager(UserManager userManager) {
        this.userManager = userManager;
    }
    
}
