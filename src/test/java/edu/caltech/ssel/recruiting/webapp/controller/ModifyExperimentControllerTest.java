/*
 * Copyright (C) 1998-2009, <a href="http://www.ssel.caltech.edu">SSEL</a>,
 * at Caltech, Pasadena, CA 91125. 
 *
 * MooreRecruiting is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 * USA.
 *
 */

package edu.caltech.ssel.recruiting.webapp.controller;

import edu.caltech.ssel.recruiting.common.RecruitingConstants;
import edu.caltech.ssel.recruiting.model.Experiment;
import org.appfuse.webapp.controller.BaseControllerTestCase;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;

/**
 *
 * @author michaelk
 */
public class ModifyExperimentControllerTest extends BaseControllerTestCase {
    
    private ModifyExperimentController controller;
    
    /** Creates a new instance of ModifyExperimentControllerTest */
    public ModifyExperimentControllerTest() {
    }
    
    public void testLoadValidExp() throws Exception {
        MockHttpServletRequest request = newGet("/modifyexperiment.html");
        request.setRemoteUser("michaelk");
        request.addParameter("id", "1");
        ModelAndView mv = controller.handleRequest(request, new MockHttpServletResponse());
        Errors errors = (Errors) mv.getModel().get(BindException.MODEL_KEY_PREFIX + "experiment");
        if(errors != null)
            log.debug(errors.toString());
        assertNull(errors);
    }

    public void testLoadInvalidExp() throws Exception {
        MockHttpServletRequest request = newGet("/modifyexperiment.html");
        request.setRemoteUser("michaelk");
        request.addParameter("id", "0");
        ModelAndView mv = controller.handleRequest(request, new MockHttpServletResponse());
        
        Experiment e = (Experiment)request.getSession().getAttribute("experiment");
        assertNull(e);
    }
    
    /**
     * test stub until tests are fixed - poneil
     */
    public void testGet() throws Exception {
        log.debug("basic test");
        MockHttpServletRequest request = newGet("modifyexperiment.html");
        request.setRemoteUser("admin");
        request.addUserRole(RecruitingConstants.ADMIN_ROLE);
        ModelAndView mv = controller.handleRequest(request, new MockHttpServletResponse());
        assertNotNull(mv);        
    }

    //The button is not a submit, but rather a link, so no testSubmit needed
    //May change that method later
    
    public void setModifyExperimentController(ModifyExperimentController controller) {
        this.controller = controller;
    }
    
}
