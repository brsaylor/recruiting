/*
 * Copyright (C) 1998-2009, <a href="http://www.ssel.caltech.edu">SSEL</a>,
 * at Caltech, Pasadena, CA 91125. 
 *
 * MooreRecruiting is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 * USA.
 *
 */

package edu.caltech.ssel.recruiting.webapp.controller;

import edu.caltech.ssel.recruiting.common.ExperimentCreationHelper;
import edu.caltech.ssel.recruiting.common.RecruitingConstants;
import edu.caltech.ssel.recruiting.model.Experiment;
import java.util.Date;
import java.util.List;
import org.appfuse.webapp.controller.BaseControllerTestCase;
import org.appfuse.service.GenericManager;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.subethamail.wiser.Wiser;

/**
 *
 * @author michaelk
 */
public class ExperimentVerificationFormControllerTest extends BaseControllerTestCase {
    
    private ExperimentVerificationFormController c;
    private GenericManager<Experiment, Long> experimentManager;
    private ExperimentCreationHelper ecHelper;
    
    /** Creates a new instance of ExperimentVerificationFormControllerTest */
    public ExperimentVerificationFormControllerTest() {
    }
    
    public void testLoad() throws Exception {
        MockHttpServletRequest request = newGet("/experimentverificationform.html");
        request.getSession().setAttribute("experiment", experimentManager.get(4L));
        ModelAndView mv = c.handleRequest(request, new MockHttpServletResponse());
        
        Experiment e = (Experiment)mv.getModel().get("experiment");
        assertNotNull(e);
        assertEquals(e.getId(), new Long(4));
    }
    
    public void testSubmit() throws Exception {
        // start SMTP Server
        Wiser wiser = new Wiser();
        wiser.setPort(12525);
        wiser.start();
        
        MockHttpServletRequest request = newPost("/experimentverificationform.html");
        request.setParameter("publish", "publish");
        Experiment e = new Experiment();
        e.setExpDate(new Date());
        String s = "This probably won't be copied";
        e.setInstruction(s);
        request.getSession().setAttribute("experiment", e);
        ModelAndView mv = c.handleRequest(request, new MockHttpServletResponse());
        
        assertNotNull(request.getSession().getAttribute("successMessages"));
        Errors errors = (Errors) mv.getModel().get(BindException.MODEL_KEY_PREFIX + "experiment");
        if(errors != null)
            log.debug(errors.toString());
        assertNull(errors);
        
        //Make sure the experiment has been saved
        List<Experiment> lst = experimentManager.getAll();
        for(Experiment exp : lst) {
            if(exp.getInstruction().equals(s))
                return;
        }
        fail("The experiment was not saved...");
        
        // verify an e-mail was sent
        wiser.stop();
        assertTrue(wiser.getMessages().size() == 1);
    }
    
    /**
     * test stub until tests are fixed - poneil
     */
    public void testGet() throws Exception {
        log.debug("basic test");
        MockHttpServletRequest request = newGet("experimentverificationform.html");
        request.setRemoteUser("admin");
        request.addUserRole(RecruitingConstants.ADMIN_ROLE);
        request.getSession().setAttribute("experiment",experimentManager.get(4L));
        ModelAndView mv = c.handleRequest(request, new MockHttpServletResponse());
        assertNotNull(mv);        
    }
    
    public void setExperimentVerificationFormController(ExperimentVerificationFormController c) {
        this.c = c;
    }
    
    public void setExperimentManager(GenericManager<Experiment, Long> experimentManager) {
        this.experimentManager = experimentManager;
    }
    
    public void setExperimentCreationHelper(ExperimentCreationHelper ecHelper) {
        this.ecHelper = ecHelper;
    }
    
}
