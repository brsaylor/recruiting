/*
 * Copyright (C) 1998-2009, <a href="http://www.ssel.caltech.edu">SSEL</a>,
 * at Caltech, Pasadena, CA 91125. 
 *
 * MooreRecruiting is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 * USA.
 *
 */

package edu.caltech.ssel.recruiting.dao;

import edu.caltech.ssel.recruiting.model.Condition;
import edu.caltech.ssel.recruiting.model.Filter;
import edu.caltech.ssel.recruiting.model.PoolCondition;
import edu.caltech.ssel.recruiting.model.SubjectPool;
import java.util.ArrayList;
import java.util.List;
import org.appfuse.dao.BaseDaoTestCase;
import org.appfuse.dao.GenericDao;
import org.springframework.dao.DataAccessException;

/**
 *
 * @author michaelk
 */
public class FilterDaoTest extends BaseDaoTestCase {
    
    private GenericDao<Filter, Long> filterDao = null;
    private GenericDao<Condition, Long> conditionDao = null;
    private SubjectPoolDao subjectPoolDao = null;
    
    /** Creates a new instance of FilterDaoTest */
    public FilterDaoTest() {
    }

    public void testCRUD() {
        log.debug("Entering testCRUD...");
        
        log.debug("Testing create...");
        Filter f = new Filter();
        f.setConditions(conditionDao.getAll());
        
        Long id = filterDao.save(f).getId();
        
        log.debug("Testing retrieve...");
        flush(); //Prevent it from just being retrieved locally
        f = filterDao.get(id);
        assertNotNull(f);
        assertEquals(f.getId(), id);
        
        log.debug("Testing update...");
        PoolCondition c = new PoolCondition();
        List<SubjectPool> lst = new ArrayList();
        lst.add(subjectPoolDao.get(subjectPoolDao.getAll().get(0).getId()));
        c.setMemberOf(false);
        c.setPools(lst);
        Long cid = conditionDao.save(c).getId();
        f = filterDao.get(id);
        f.addCondition(conditionDao.get(cid));
        filterDao.save(f);
        
        log.debug("Testing delete...");
        filterDao.remove(f.getId());
        flush();  //Prevent it from just being retrieved locally
        
        try{
            filterDao.get(f.getId());
            fail("Filter Found in Database");
        } catch (DataAccessException dae) {
            log.debug("Expected exception: " + dae.getMessage());
            assertNotNull(dae);
        }
    }
    
    public void setFilterDao(GenericDao<Filter, Long> filterDao) {
        this.filterDao = filterDao;
    }

    public void setConditionDao(GenericDao<Condition, Long> conditionDao) {
        this.conditionDao = conditionDao;
    }

    public void setSubjectPoolDao(SubjectPoolDao subjectPoolDao) {
        this.subjectPoolDao = subjectPoolDao;
    }
    
}
