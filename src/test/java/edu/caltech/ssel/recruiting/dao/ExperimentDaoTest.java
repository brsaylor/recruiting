/*
 * Copyright (C) 1998-2009, <a href="http://www.ssel.caltech.edu">SSEL</a>,
 * at Caltech, Pasadena, CA 91125. 
 *
 * MooreRecruiting is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 * USA.
 *
 */

package edu.caltech.ssel.recruiting.dao;

import edu.caltech.ssel.recruiting.model.Experiment;
import edu.caltech.ssel.recruiting.model.ExperimentType;
import edu.caltech.ssel.recruiting.model.Experimenter;
import edu.caltech.ssel.recruiting.model.Filter;
import edu.caltech.ssel.recruiting.model.Location;
import edu.caltech.ssel.recruiting.model.Survey;
import java.text.SimpleDateFormat;
import java.util.List;
import org.appfuse.dao.BaseDaoTestCase;
import org.appfuse.dao.GenericDao;
import org.appfuse.model.User;
import org.springframework.dao.DataAccessException;

/**
 *
 * @author michaelk
 */
public class ExperimentDaoTest extends BaseDaoTestCase {
    
    private ExperimentDao experimentDao = null;
    private GenericDao<Location, Long> locationDao = null;
    private GenericDao<Filter, Long> filterDao = null;
    private GenericDao<Survey, Long> surveyDao = null;
    private ExtendedUserDao extendedUserDao = null;
    private ExperimentTypeDao experimentTypeDao = null;
    
    /** Creates a new instance of ExperimentDaoTest */
    public ExperimentDaoTest() {
    }
    
    public void testCRUD() throws Exception {
        log.debug("Entering ExperimentDao testCRUD...");
        
        log.debug("Testing create...");

        Filter f = filterDao.get(filterDao.getAll().get(0).getId());
        Location l = locationDao.get(locationDao.getAll().get(0).getId());
        Experimenter er = (Experimenter) extendedUserDao.get(extendedUserDao.getExperimenters().get(0).getId());
        ExperimentType et = experimentTypeDao.get(experimentTypeDao.getAll().get(0).getId());
        
        Experiment e = new Experiment();
        e.setFilter(f);
        e.setInstruction("Do this experiment");
        e.setLocation(l);
        e.setMaxNumSubjects(12);
        e.setMinNumSubjects(1);
        e.setPrinciple(er);
        e.setReservedBy((User)er);
        e.setRunby(er);
        e.setShowUpFee(20);
        e.setSignupFreezeTime(12);
        e.setStatus(Experiment.Status.OPEN);
        e.setType(et);
        Long id = experimentDao.save(e).getId();
        
        log.debug("Testing retrieve...");
        flush(); //Prevent it from just being retrieved locally
        e = experimentDao.get(id);
                
        assertNotNull(e);
        assertNotNull(e.getId());
        assertSame(e.getInstruction().compareTo("Do this experiment"), 0);
        
        log.debug("Testing update...");
        Experiment e2 = experimentDao.get(experimentDao.getAll().get(0).getId());
        e2.setInstruction("New instruction");
        experimentDao.save(e2);
        
        log.debug("Testing delete...");
        experimentDao.remove(e.getId());
        flush();  //Prevent it from just being retrieved locally
        
        try{
            experimentDao.get(e.getId());
            fail("Experiment Found in Database");
        } catch (DataAccessException dae) {
            log.debug("Expected exception: " + dae.getMessage());
            assertNotNull(dae);
        }
    }

    public void testGetByDate() throws Exception {
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
        List<Experiment> lst = experimentDao.getByDate(sdf.parse("01/24/2008"));//"2007-06-29"));
        assertNotNull(lst);
        assertEquals(lst.size(), 2);
        
        //lst = experimentDao.getByDate(new Date("2007-06-30"));
        //assertTrue(lst == null || lst.size() == 0);
    }

    public void testGetBySurvey() {
        List<Experiment> lst = experimentDao.getBySurvey(surveyDao.get(2L));
        assertNotNull(lst);
        assertTrue(lst.size() == 3);
    }

    public void testGetByFilter() {
        List<Experiment> lst = experimentDao.getByFilter(filterDao.get(1L));
        assertNotNull(lst);
        assertTrue(lst.size() == 2);
    }

    public void setExperimentDao(ExperimentDao experimentDao) {
        this.experimentDao = experimentDao;
    }

    public void setExperimentTypeDao(ExperimentTypeDao experimentTypeDao) {
        this.experimentTypeDao = experimentTypeDao;
    }

    public void setLocationDao(GenericDao<Location, Long> locationDao) {
        this.locationDao = locationDao;
    }

    public void setFilterDao(GenericDao<Filter, Long> filterDao) {
        this.filterDao = filterDao;
    }

    public void setExtendedUserDao(ExtendedUserDao extendedUserDao) {
        this.extendedUserDao = extendedUserDao;
    }

    public void setSurveyDao(GenericDao<Survey, Long> surveyDao) {
        this.surveyDao = surveyDao;
    }
}
