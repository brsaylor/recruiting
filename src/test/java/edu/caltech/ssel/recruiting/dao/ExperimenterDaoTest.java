/*
 * Copyright (C) 1998-2009, <a href="http://www.ssel.caltech.edu">SSEL</a>,
 * at Caltech, Pasadena, CA 91125. 
 *
 * MooreRecruiting is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 * USA.
 *
 */

package edu.caltech.ssel.recruiting.dao;

import edu.caltech.ssel.recruiting.model.Experimenter;
import java.util.Date;
import java.util.List;
import org.appfuse.dao.BaseDaoTestCase;
import org.appfuse.dao.GenericDao;
import org.appfuse.model.Address;
import org.appfuse.model.User;

/*
 *
 * @author michaelk
 */
public class ExperimenterDaoTest extends BaseDaoTestCase {
    
    private GenericDao<User, Long> dao;
    
    /** Creates a new instance of ExperimenterDaoTest */
    public ExperimenterDaoTest() {
    }
    
    public void setNewUserDao(GenericDao<User, Long> u) {
        dao = u;
    }
    
    public void testAddExperimenter() {
        log.debug("Entering testAddExperimenter...");
        
        Experimenter er = new Experimenter();
        er.setFirstName("Indiana");
        er.setLastName("Jones");
        er.setUsername("arch1245");
        er.setPassword("bones");
        er.setEmail("indiana@ng.com");
        er.setAccountExpired(false);
        er.setAccountLocked(false);
        er.setCredentialsExpired(false);
        Address a = new Address();
        a.setCity("Paris");
        a.setPostalCode("12345");
        er.setAddress(a);    
        er.setAffiliation("Caltech");
        er.setTitle(Experimenter.Title.DR);
        
        Long id = dao.save(er).getId();
        flush();
        
        Experimenter er2 = (Experimenter) dao.get(id);
        
        assertNotNull(er2);
        assertEquals(er.getAffiliation(), er2.getAffiliation());
        assertEquals(er.getTitle(), er2.getTitle());
    }
}
