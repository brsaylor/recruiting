/*
 * Copyright (C) 1998-2009, <a href="http://www.ssel.caltech.edu">SSEL</a>,
 * at Caltech, Pasadena, CA 91125. 
 *
 * MooreRecruiting is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 * USA.
 *
 */

package edu.caltech.ssel.recruiting.dao;

import edu.caltech.ssel.recruiting.model.SubjectPool;
import java.util.Date;
import java.util.List;
import org.appfuse.dao.BaseDaoTestCase;

/**
 *
 * @author Administrator
 */
public class SubjectPoolDaoTest extends BaseDaoTestCase {
    private SubjectPoolDao subjectPoolDao = null;
    
    public void setSubjectPoolDao(SubjectPoolDao subjectPoolDao){
        this.subjectPoolDao = subjectPoolDao;
    }
    
    public void testFindByName() throws Exception {
        List<SubjectPool> subjectPools = subjectPoolDao.findByName("Caltech");
        assertTrue(subjectPools.size() > 0);
    }
    
    public void testCRUD() throws Exception {
        SubjectPool s = new SubjectPool();
        s.setName("Random name");
        s.setDescr("Random students");
        s.setConsentInfo("Dost thou want to?");
        s.setValid(true);
        s.setRegDate(new Date(2007,5,31));
        
        s = subjectPoolDao.save(s);

        flush();
        s = subjectPoolDao.get(s.getId());
        
        assertEquals("Random name", s.getName());
    }
}
