/*
 * Copyright (C) 1998-2009, <a href="http://www.ssel.caltech.edu">SSEL</a>,
 * at Caltech, Pasadena, CA 91125. 
 *
 * MooreRecruiting is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 * USA.
 *
 */

package edu.caltech.ssel.recruiting.dao;

import edu.caltech.ssel.recruiting.model.BooleanCondition;
import edu.caltech.ssel.recruiting.model.Condition;
import edu.caltech.ssel.recruiting.model.Experiment;
import edu.caltech.ssel.recruiting.model.ExperimentCondition;
import edu.caltech.ssel.recruiting.model.ExperimentType;
import edu.caltech.ssel.recruiting.model.ExperimentTypeCondition;
import edu.caltech.ssel.recruiting.model.PoolCondition;
import edu.caltech.ssel.recruiting.model.Question;
import edu.caltech.ssel.recruiting.model.QuestionResultCondition;
import edu.caltech.ssel.recruiting.model.QuestionResultCondition.FilterType;
import edu.caltech.ssel.recruiting.model.SubjectPool;
import edu.caltech.ssel.recruiting.model.Survey;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import org.appfuse.dao.BaseDaoTestCase;
import org.appfuse.dao.GenericDao;

/**
 *
 * @author michaelk
 */
public class ConditionDaoTest extends BaseDaoTestCase {
    
    private GenericDao<Condition, Long> conditionDao = null;
    private GenericDao<Experiment, Long> experimentDao = null;
    private GenericDao<Question, Long> questionDao = null;
    private ExperimentTypeDao experimentTypeDao = null;
    private SubjectPoolDao subjectPoolDao = null;
    private GenericDao<Survey, Long> surveyDao = null;
    
    /** Creates a new instance of ConditionDaoTest */
    public ConditionDaoTest() {
    }

    public void testAddExperimentCondition () {
        log.debug("Entering testAddExperimentCondition...");
        ExperimentCondition ec = new ExperimentCondition();
        ec.setExperiment(experimentDao.get(experimentDao.getAll().get(0).getId()));
        ec.setOnList(true);

        ec = (ExperimentCondition) conditionDao.save(ec);
        
        flush();
        
        ExperimentCondition ec2 = (ExperimentCondition) conditionDao.get(ec.getId());
        assertNotNull(ec);
        assertNotNull(ec2);
        assertTrue(ec2.isOnList());
    }

    public void testAddExperimentTypeCondition() {
        log.debug("Entering testAddExperimentTypeCondition...");
        ExperimentTypeCondition etc = new ExperimentTypeCondition();
        List<ExperimentType> lst = new ArrayList();
        lst.add(experimentTypeDao.get(experimentTypeDao.getAll().get(0).getId()));
        etc.setTypes(lst);
        etc.setHistory(true);
        etc.setOnList(true);

        etc = (ExperimentTypeCondition) conditionDao.save(etc);
        
        flush();
        
        ExperimentTypeCondition etc2 = (ExperimentTypeCondition) conditionDao.get(etc.getId());
        assertNotNull(etc);
        assertNotNull(etc2);
        assertTrue(etc2.isHistory());
    }
    
    public void testAddPoolCondition() {
        log.debug("Entering testAddPoolCondition...");
        PoolCondition pc = new PoolCondition();
        List<SubjectPool> lst = new ArrayList();
        lst.add(subjectPoolDao.get(subjectPoolDao.getAll().get(0).getId()));
        pc.setPools(lst);
        pc.setMemberOf(true);

        pc = (PoolCondition) conditionDao.save(pc);
        
        flush();
        
        PoolCondition pc2 = (PoolCondition) conditionDao.get(pc.getId());
        assertNotNull(pc);
        assertNotNull(pc2);
        assertTrue(pc2.isMemberOf());
    }
    
    /*
    public void testAddSurveyCondition() {
        log.debug("Entering testAddSurveyCondition...");
        SurveyCondition sc = new SurveyCondition();
        sc.setSurvey(surveyDao.get(surveyDao.getAll().get(0).getId()));
        sc.setExperiment(experimentDao.get(experimentDao.getAll().get(0).getId()));

        sc = (SurveyCondition) conditionDao.save(sc);
        
        flush();
        
        SurveyCondition sc2 = (SurveyCondition) conditionDao.get(sc.getId());
        assertNotNull(sc);
        assertNotNull(sc2);
        assertSame(sc.getId(), sc2.getId());
    }
     */

    public void testAddQuestionResultCondition() {
        log.debug("Entering testAddQuestionResultCondition...");
        QuestionResultCondition qrc = new QuestionResultCondition();
        qrc.setQuestion(questionDao.get(1L));
        qrc.setFilterOptions(new HashSet());
        qrc.setFilterType(FilterType.CHECK_ALL);
        
        qrc = (QuestionResultCondition) conditionDao.save(qrc);
        
        flush();
        
        QuestionResultCondition qrc2 = (QuestionResultCondition) conditionDao.get(qrc.getId());
        assertNotNull(qrc);
        assertNotNull(qrc2);
        assertEquals(qrc.getId(), qrc2.getId());
    }
    
    //Pool And Question Result
    public void testAddBooleanCondition() {
        log.debug("Entering testAddBooleanCondition");
        BooleanCondition bc = new BooleanCondition();
        QuestionResultCondition qrc = new QuestionResultCondition();
        PoolCondition pc = new PoolCondition();
        
        //Set First condition
        List<SubjectPool> lst = new ArrayList();
        lst.add(subjectPoolDao.get(subjectPoolDao.getAll().get(0).getId()));
        pc.setPools(lst);
        pc.setMemberOf(true);

        pc = (PoolCondition) conditionDao.save(pc);
        flush();
        bc.setFirstCondition(pc.getId());
        
        bc = (BooleanCondition) conditionDao.save(bc);
        flush();
        
        assertNotNull(bc);
        BooleanCondition bc2 = (BooleanCondition) conditionDao.get(bc.getId());
        assertEquals(bc.getId(),bc2.getId());
        
        //Set Boolean(AND)
        bc.setAnd(true);
        //assertNotSame(bc.getId(),bc2.getId());
        BooleanCondition bc3 = (BooleanCondition) conditionDao.get(bc.getId());
        
        //Set Second condition
        qrc.setQuestion(questionDao.get(1L));
        qrc.setFilterOptions(new HashSet());
        qrc.setFilterType(FilterType.CHECK_ALL);
        
        qrc = (QuestionResultCondition) conditionDao.save(qrc);
        flush();
        bc.setSecondCondition(qrc.getId());
        
        bc = (BooleanCondition) conditionDao.save(bc);
        flush();
        
        assertNotNull(bc);
        //assertNotSame(bc.getId(),bc3.getId());
    }
    
    //Pool Or Question Result
    public void testAddBooleanCondition2() {
        log.debug("Entering testAddBooleanCondition2");
        BooleanCondition bc = new BooleanCondition();
        QuestionResultCondition qrc = new QuestionResultCondition();
        PoolCondition pc = new PoolCondition();
        
        //Set First condition
        List<SubjectPool> lst = new ArrayList();
        lst.add(subjectPoolDao.get(subjectPoolDao.getAll().get(0).getId()));
        pc.setPools(lst);
        pc.setMemberOf(true);

        pc = (PoolCondition) conditionDao.save(pc);
        flush();
        bc.setFirstCondition(pc.getId());
        
        bc = (BooleanCondition) conditionDao.save(bc);
        flush();
        
        assertNotNull(bc);
        BooleanCondition bc2 = (BooleanCondition) conditionDao.get(bc.getId());
        assertEquals(bc.getId(),bc2.getId());
        
        //Set Boolean(OR)
        bc.setAnd(false);
        //assertNotSame(bc.getId(),bc2.getId());
        BooleanCondition bc3 = (BooleanCondition) conditionDao.get(bc.getId());
        
        //Set Second condition
        qrc.setQuestion(questionDao.get(1L));
        qrc.setFilterOptions(new HashSet());
        qrc.setFilterType(FilterType.CHECK_ALL);
        
        qrc = (QuestionResultCondition) conditionDao.save(qrc);
        flush();
        bc.setSecondCondition(qrc.getId());
        
        bc = (BooleanCondition) conditionDao.save(bc);
        flush();
        
        assertNotNull(bc);
        //assertNotSame(bc.getId(),bc3.getId());
    }
    
    //Pool Or Pool
    public void testAddBooleanCondition3() {
        log.debug("Entering testAddBooleanCondition3");
        BooleanCondition bc = new BooleanCondition();
        PoolCondition pc = new PoolCondition();
        PoolCondition pc2 = new PoolCondition();
        
        //Set First condition
        List<SubjectPool> lst = new ArrayList();
        lst.add(subjectPoolDao.get(subjectPoolDao.getAll().get(0).getId()));
        pc.setPools(lst);
        pc.setMemberOf(true);

        pc = (PoolCondition) conditionDao.save(pc);
        flush();
        bc.setFirstCondition(pc.getId());
        
        bc = (BooleanCondition) conditionDao.save(bc);
        flush();
        
        assertNotNull(bc);
        BooleanCondition bc2 = (BooleanCondition) conditionDao.get(bc.getId());
        assertEquals(bc.getId(),bc2.getId());
        
        //Set Boolean(OR)
        bc.setAnd(false);
        //assertNotSame(bc.getId(),bc2.getId());
        BooleanCondition bc3 = (BooleanCondition) conditionDao.get(bc.getId());
        
        //Set Second condition
        List<SubjectPool> lst2 = new ArrayList();
        lst2.add(subjectPoolDao.get(subjectPoolDao.getAll().get(1).getId()));
        pc2.setPools(lst2);
        pc2.setMemberOf(true);

        pc2 = (PoolCondition) conditionDao.save(pc2);
        flush();
        bc.setSecondCondition(pc2.getId());
        
        bc = (BooleanCondition) conditionDao.save(bc);
        flush();
        
        assertNotNull(bc);
        //assertNotSame(bc.getId(),bc3.getId());
    }
    
    //Question Result And Question Result
    public void testAddBooleanCondition4() {
        log.debug("Entering testAddBooleanCondition4");
        BooleanCondition bc = new BooleanCondition();
        QuestionResultCondition qrc = new QuestionResultCondition();
        QuestionResultCondition qrc2 = new QuestionResultCondition();
        
        //Set First condition
        qrc.setQuestion(questionDao.get(1L));
        qrc.setFilterOptions(new HashSet());
        qrc.setFilterType(FilterType.CHECK_ALL);
        
        qrc = (QuestionResultCondition) conditionDao.save(qrc);
        flush();
        bc.setFirstCondition(qrc.getId());
        
        bc = (BooleanCondition) conditionDao.save(bc);
        flush();
        
        assertNotNull(bc);
        BooleanCondition bc2 = (BooleanCondition) conditionDao.get(bc.getId());
        assertEquals(bc.getId(),bc2.getId());
        
        //Set Boolean(AND)
        bc.setAnd(true);
        //assertNotSame(bc.getId(),bc2.getId());
        BooleanCondition bc3 = (BooleanCondition) conditionDao.get(bc.getId());
        
        //Set Second condition
        qrc2.setQuestion(questionDao.get(1L));
        qrc2.setFilterOptions(new HashSet());
        qrc2.setFilterType(FilterType.CHECK_ALL);
        
        qrc2 = (QuestionResultCondition) conditionDao.save(qrc2);
        flush();
        bc.setSecondCondition(qrc2.getId());
        
        bc = (BooleanCondition) conditionDao.save(bc);
        flush();
        
        assertNotNull(bc);
        //assertNotSame(bc.getId(),bc3.getId());
    }
    
    //And
    public void testAddBooleanCondition5() {
        log.debug("Entering testAddBooleanCondition5...");
        BooleanCondition bc = new BooleanCondition();
        bc.setAnd(true);
        bc = (BooleanCondition) conditionDao.save(bc);
        flush();
        assertNotNull(bc);
    }
    
    public GenericDao<Condition, Long> getConditionDao() {
        return conditionDao;
    }

    public void setConditionDao(GenericDao<Condition, Long> conditionDao) {
        this.conditionDao = conditionDao;
    }

    public GenericDao<Experiment, Long> getExperimentDao() {
        return experimentDao;
    }

    public void setExperimentDao(GenericDao<Experiment, Long> experimentDao) {
        this.experimentDao = experimentDao;
    }

    public ExperimentTypeDao getExperimentTypeDao() {
        return experimentTypeDao;
    }

    public void setExperimentTypeDao(ExperimentTypeDao experimentTypeDao) {
        this.experimentTypeDao = experimentTypeDao;
    }

    public SubjectPoolDao getSubjectPoolDao() {
        return subjectPoolDao;
    }

    public void setSubjectPoolDao(SubjectPoolDao subjectPoolDao) {
        this.subjectPoolDao = subjectPoolDao;
    }

    public GenericDao<Survey, Long> getSurveyDao() {
        return surveyDao;
    }

    public void setSurveyDao(GenericDao<Survey, Long> surveyDao) {
        this.surveyDao = surveyDao;
    }

    public void setQuestionDao(GenericDao<Question, Long> questionDao) {
        this.questionDao = questionDao;
    }
    
}
