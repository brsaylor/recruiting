/*
 * Copyright (C) 1998-2009, <a href="http://www.ssel.caltech.edu">SSEL</a>,
 * at Caltech, Pasadena, CA 91125. 
 *
 * MooreRecruiting is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 * USA.
 *
 */

package edu.caltech.ssel.recruiting.dao;

import edu.caltech.ssel.recruiting.model.Question;
import edu.caltech.ssel.recruiting.model.Survey;
import edu.caltech.ssel.recruiting.model.Survey.SurveyType;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.appfuse.dao.BaseDaoTestCase;
import org.appfuse.dao.GenericDao;
import org.appfuse.model.User;
import org.springframework.dao.DataAccessException;

/**
 *
 * @author michaelk
 */
public class SurveyDaoTest extends BaseDaoTestCase {

    private SurveyDao dao;
    private GenericDao<Question, Long> qdao;
    private GenericDao<User, Long> udao;
    
    /** Creates a new instance of SurveyDaoTest */
    public SurveyDaoTest() {
    }

    public void testGetAll() throws Exception {
        log.debug("Entering testGetAll...");
        if(dao.getAll().size() != 2)
            fail("There were not 2 surveys, but rather " + dao.getAll().size());
    }
    
    public void testCRUD() throws Exception {
        log.debug("Entering testCRUD...");
        
        log.debug("Testing create...");

        //Populate the question list
        Question q = new Question();
        q.setLabel("Test question");
        q.setQuestion("Do you love me?");
        q.setStyle(Question.QStyle.DROP_LIST);
        List<String> lst = new ArrayList();
        lst.add("Yes");
        lst.add("No");
        lst.add("...Now that I can dance");
        q.setOptionList(lst);
        qdao.save(q);
        
        q = new Question();
        q.setLabel("Test question 2");
        q.setQuestion("Why did the chicken cross the road?");
        q.setStyle(Question.QStyle.RADIO);
        lst = new ArrayList();
        lst.add("To get to the other side");
        lst.add("Why not?");
        lst.add("As a protest against the communist onslaught");
        q.setOptionList(lst);
        qdao.save(q);
        
        Survey s = new Survey();
        s.setCreator(udao.get(1L));
        s.setName("Test survey");
        s.setQuestions(qdao.getAll());
        s.setType(Survey.SurveyType.PROFILE);
        s.setVisibility(Survey.Visibility.PUBLIC);
        Long id = dao.save(s).getId();
        
        log.debug("Testing retrieve...");
        flush(); //Prevent it from just being retrieved locally
        s = dao.get(id);
        assertNotNull(s.getId());
        assertEquals("Test survey", s.getName());
        for(Iterator i = s.getQuestions().iterator(), j = qdao.getAll().iterator(); i.hasNext() && j.hasNext(); ) {
            assertEquals(i.next(), j.next());
        }
        
        log.debug("Testing update...");
        Survey s2 = dao.get(dao.getAll().get(0).getId());
        s2.setName("New Name");
        dao.save(s2);
        
        log.debug("Testing delete...");
        dao.remove(s.getId());
        flush();  //Prevent it from just being retrieved locally
        
        try{
            dao.get(s.getId());
            fail("Experiment Type Found in Database");
        } catch (DataAccessException dae) {
            log.debug("Expected exception: " + dae.getMessage());
            assertNotNull(dae);
        }
    }
    
    public void testGetProfileSurvey() {
        log.debug("Testing getProfileSurvey...");
        
        assertTrue(dao.getProfileSurvey().getType() == SurveyType.PROFILE);
    }
    
    public void setSurveyDao(SurveyDao d) {
        this.dao = d;
    }
    
    public void setQuestionDao(GenericDao<Question, Long> d) {
        this.qdao = d;
    }
    
    public void setUserDao(GenericDao<User, Long> d) {
        this.udao = d;
    }
    
}
