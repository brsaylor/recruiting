/*
 * Copyright (C) 1998-2009, <a href="http://www.ssel.caltech.edu">SSEL</a>,
 * at Caltech, Pasadena, CA 91125. 
 *
 * MooreRecruiting is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 * USA.
 *
 */

package edu.caltech.ssel.recruiting.dao;

import edu.caltech.ssel.recruiting.dao.hibernate.ExperimentTypeDaoHibernate;
import edu.caltech.ssel.recruiting.model.ExperimentType;
import java.util.Date;
import java.util.List;
import org.appfuse.dao.BaseDaoTestCase;
import org.appfuse.dao.UserDao;
import org.appfuse.model.User;
import org.springframework.dao.DataAccessException;

/**
 *
 * @author michaelk
 */
public class ExperimentTypeDaoTest extends BaseDaoTestCase {
    
    private ExperimentTypeDao dao;
    //Originally tried to use UserManager instead of UserDao, but it threw off errors
    private UserDao udao = null;
    
/*        log.debug("Entering setUserManager...");
        this.mgr = userManager;
        List<User> users = mgr.getUsers(null);
        log.debug("Users.size() == " + users.size());*/
    
    /** Creates a new instance of ExperimentTypeDaoTest */
    public ExperimentTypeDaoTest() {
    }
    
    public void testCRUD() throws Exception {
        log.debug("Entering testCRUD...");
        
        log.debug("Testing create...");
        ExperimentType eT = new ExperimentType();
        eT.setDescr("Boring experiment");
        eT.setInstruction("Go to sleep");
        eT.setName("Naptime");
        eT.setValid(true);
        eT.setRegDate(new Date());
        List<User> users = udao.getAll();
        eT.setCreator(udao.get(users.get(0).getId()));
        
        Long id = dao.save(eT).getId();
        
        log.debug("Testing retrieve...");
        flush(); //Prevent it from just being retrieved locally
        eT = dao.get(id);
        assertNotNull(eT.getId());
        assertEquals("Naptime", eT.getName());
        
        log.debug("Testing update...");
        ExperimentType eT2 = dao.get(dao.getAll().get(0).getId());
        eT2.setName("New Name");
        dao.save(eT2);
        
        log.debug("Testing delete...");
        dao.remove(eT.getId());
        flush();  //Prevent it from just being retrieved locally
        
        try{
            dao.get(eT.getId());
            fail("Experiment Type Found in Database");
        } catch (DataAccessException dae) {
            log.debug("Expected exception: " + dae.getMessage());
            assertNotNull(dae);
        }
    }
    
    public void setExperimentTypeDao(ExperimentTypeDao dao) {
        this.dao = dao;
    }
    
    public void setUserDao(UserDao udao) {
        this.udao = udao;
    }
    
}
