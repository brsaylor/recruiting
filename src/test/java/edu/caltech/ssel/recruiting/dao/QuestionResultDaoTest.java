/*
 * Copyright (C) 1998-2009, <a href="http://www.ssel.caltech.edu">SSEL</a>,
 * at Caltech, Pasadena, CA 91125. 
 *
 * MooreRecruiting is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 * USA.
 *
 */

package edu.caltech.ssel.recruiting.dao;

import edu.caltech.ssel.recruiting.model.Experiment;
import edu.caltech.ssel.recruiting.model.Question;
import edu.caltech.ssel.recruiting.model.QuestionResult;
import java.util.Date;
import java.util.List;
import org.appfuse.dao.BaseDaoTestCase;
import org.appfuse.dao.GenericDao;
import org.appfuse.dao.UserDao;
import org.appfuse.model.User;

/**
 *
 * @author michaelk
 */
public class QuestionResultDaoTest extends BaseDaoTestCase {
    
    private QuestionResultDao dao = null;
    private UserDao udao = null;
    private GenericDao<Question, Long> questionDao = null;
    private GenericDao<Experiment, Long> experimentDao = null;
    
    /** Creates a new instance of QuestionResultDaoTest */
    public QuestionResultDaoTest() {
    }

    //Sets it to the proper dao
    public void setQuestionResultDao(QuestionResultDao dao) {
        this.dao = dao;
    }
    
    public void setUserDao(UserDao udao) {
        this.udao = udao;
    }
    
    public void testGetAnswersByUser() {
        log.debug("Entering testGetAnswersByUser...");

        User u = udao.get(udao.getUsers().get(0).getId());

/*        QuestionResult qr = new QuestionResult();
        qr.setAnswerIdx(1);
        qr.setQuestion(questionDao.get(questionDao.getAll().get(0).getId()));
        qr.setSurveyParent(experimentDao.get(experimentDao.getAll().get(0).getId()));
        qr.setSurveyTaker(u);
        dao.save(qr);

        flush();
        
        qr = new QuestionResult();
        qr.setAnswerIdx(2);
        qr.setQuestion(questionDao.get(questionDao.getAll().get(0).getId()));
        qr.setSurveyParent(experimentDao.get(experimentDao.getAll().get(0).getId()));
        qr.setSurveyTaker(udao.get(udao.getUsers().get(1).getId()));
        dao.save(qr);

        flush();*/
        
        List<QuestionResult> lst = dao.getAnswersByUser(u);
        assertNotNull(lst);
        //assertTrue(lst.size() == 1);
    }

    public void testGetAnswersByExperiment() {
        log.debug("Entering testGetAnswersByExperiment...");
        
        QuestionResult qr = new QuestionResult();
        qr.setAnswerIdx(1);
        qr.setQuestion(questionDao.get(questionDao.getAll().get(0).getId()));
        Experiment e = experimentDao.get(experimentDao.getAll().get(0).getId());
        qr.setSurveyParent(e);
        qr.setSurveyTaker(udao.get(udao.getUsers().get(0).getId()));
        dao.save(qr);

        flush();

        qr = new QuestionResult();
        qr.setAnswerIdx(2);
        qr.setQuestion(questionDao.get(questionDao.getAll().get(0).getId()));
        e = new Experiment();
        e.setInstruction("blah");
        e.setMaxNumSubjects(1);
        e.setMinNumSubjects(1);
        e.setShowUpFee(1);
        e.setSignupFreezeTime(1);
        e.setStatus(Experiment.Status.CLOSED);
        e = experimentDao.save(e);
        flush();
        qr.setSurveyParent(e);
        qr.setSurveyTaker(udao.get(udao.getUsers().get(0).getId()));
        dao.save(qr);

        flush();
        
        List<QuestionResult> lst = dao.getAnswersByExperiment(e);
        assertNotNull(lst);
        assertTrue(lst.size() == 1);
        assertSame(lst.get(0).getAnswerIdx(), 2);
    }

    public void testGetMostRecentAnswer() {
        log.debug("Entering testGetMostRecentAnswer...");
 
        log.debug("The first user has id " + udao.getUsers().get(0).getId());
        
        //first case is correct user, correct question, earlier date
        QuestionResult qr = new QuestionResult();
        qr.setAnswerIdx(1);
        Question q = questionDao.get(questionDao.getAll().get(0).getId());
        qr.setQuestion(q);
        qr.setSurveyParent(experimentDao.get(experimentDao.getAll().get(0).getId()));
        User u = udao.get(udao.getUsers().get(0).getId());
        qr.setSurveyTaker(u);
        qr.setTakenDate(new Date());
        dao.save(qr);

        flush();

        //second case will be what we want
        qr = new QuestionResult();
        qr.setAnswerIdx(2);
        qr.setQuestion(questionDao.get(questionDao.getAll().get(0).getId()));
        qr.setSurveyParent(experimentDao.get(experimentDao.getAll().get(0).getId()));
        qr.setSurveyTaker(u);
        qr.setTakenDate(new Date(2007, 7, 21));
        dao.save(qr);

        flush();

        //third case has later date, but wrong question
        qr = new QuestionResult();
        qr.setAnswerIdx(3);
        Question q2 = new Question();
        q2.setLabel("new question");
        q2.setOptionList((List<String>)null);
        q2.setQuestion("test");
        q2.setStyle(Question.QStyle.CHECK_BOX);
        q2 = questionDao.save(q2);
        flush();
        qr.setQuestion(q2);
        qr.setSurveyParent(experimentDao.get(experimentDao.getAll().get(0).getId()));
        qr.setSurveyTaker(u);
        qr.setTakenDate(new Date(2007, 7, 23));
        dao.save(qr);

        flush();

        //fourth case has latest date, but wrong user
        QuestionResult qr2 = new QuestionResult();
        qr2.setAnswerIdx(4);
        qr2.setQuestion(questionDao.get(questionDao.getAll().get(0).getId()));
        qr2.setSurveyParent(experimentDao.get(experimentDao.getAll().get(0).getId()));
        //log.debug("The second user has id " + udao.getUsers().get(1).getId());
        User u2 = udao.get(udao.getUsers().get(1).getId());
        //log.debug("Now it is " + u2.getId());
        qr2.setSurveyTaker(u2);
        //log.debug("Here the program thinks the id is " + u2.getId());
        qr2.setTakenDate(new Date(2007, 7, 24));
        qr2 = dao.save(qr2);
        //log.debug("But the program thinks it now is " + qr2.getSurveyTaker().getId());
        
        flush();
        
        List<QuestionResult> lst = dao.getAll();
        
        qr = dao.getMostRecentAnswer(u, q);
        assertNotNull(qr);
        assertSame(qr.getAnswerIdx(), 2);
    }

    public GenericDao<Question, Long> getQuestionDao() {
        return questionDao;
    }

    public void setQuestionDao(GenericDao<Question, Long> questionDao) {
        this.questionDao = questionDao;
    }

    public GenericDao<Experiment, Long> getExperimentDao() {
        return experimentDao;
    }

    public void setExperimentDao(GenericDao<Experiment, Long> experimentDao) {
        this.experimentDao = experimentDao;
    }
    
}
