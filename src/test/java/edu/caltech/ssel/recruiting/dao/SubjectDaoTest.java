/*
 * Copyright (C) 1998-2009, <a href="http://www.ssel.caltech.edu">SSEL</a>,
 * at Caltech, Pasadena, CA 91125. 
 *
 * MooreRecruiting is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 * USA.
 *
 */

package edu.caltech.ssel.recruiting.dao;

import edu.caltech.ssel.recruiting.model.Subject;
import java.util.Date;
import org.appfuse.dao.BaseDaoTestCase;
import org.appfuse.dao.GenericDao;
import org.appfuse.model.Address;
import org.appfuse.model.User;

/*
 *
 * @author michaelk
 */
public class SubjectDaoTest extends BaseDaoTestCase {
    
    private GenericDao<User, Long> dao;
    
    /** Creates a new instance of SubjectDaoTest */
    public SubjectDaoTest() {
    }
    
    public void setNewUserDao(GenericDao<User, Long> u) {
        dao = u;
    }
    
    public void setUserDao(GenericDao<User, Long> u) {
    }
    
    public void testAddSubject() {
        log.debug("Entering testAddSubject...");

        Subject s = new Subject();
        s.setFirstName("Indiana");
        s.setLastName("Jones");
        s.setUsername("arch1245");
        s.setPassword("bones");
        s.setEmail("indiana@ng.com");
        s.setAccountExpired(false);
        s.setAccountLocked(false);
        s.setCredentialsExpired(false);
        s.setBirthday(new Date());
        s.setPassword("testpassword");
        Address a = new Address();
        a.setCity("Paris");
        a.setPostalCode("12345");
        s.setAddress(a);    
        
        Long id = dao.save(s).getId();
        flush();
        
        Subject s2 = (Subject) dao.get(id);
        
        assertNotNull(s2);
    }
    
    public void testFiller() {
        log.debug("Entering testFiller...");
    }
}
