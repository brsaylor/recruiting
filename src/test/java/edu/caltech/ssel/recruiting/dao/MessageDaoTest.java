/*
 * Copyright (C) 1998-2009, <a href="http://www.ssel.caltech.edu">SSEL</a>,
 * at Caltech, Pasadena, CA 91125. 
 *
 * MooreRecruiting is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 * USA.
 *
 */

package edu.caltech.ssel.recruiting.dao;

import java.util.List;

import org.appfuse.dao.BaseDaoTestCase;
import edu.caltech.ssel.recruiting.model.Message;
import edu.caltech.ssel.recruiting.model.Experimenter;
import org.springframework.dao.DataAccessException;
import org.appfuse.dao.UserDao;
import org.appfuse.model.User;

public class MessageDaoTest extends BaseDaoTestCase {
    
    private MessageDao messageDao = null;
    private UserDao userDao = null;
    
    public void testFindDraftsByCreator() throws Exception {
        User user = userDao.get(3L);
        List<Message> messages = messageDao.findDraftsByCreator(user);
        assertTrue(messages.size() > 0);
    }
    
    public void testFindSentByCreator() throws Exception {
        User user = userDao.get(1L);
        List<Message> messages = messageDao.findSentByCreator(user);
        assertTrue(messages.size() > 0);
    }
    
    public void testAddAndRemoveMessage() throws Exception {
        User user = userDao.get(1L);
        Message message = new Message();
        message.setCreator(user);
        message.setStatus(Message.Status.DRAFT);
        message.setSubject("Draft Test Message");
        message.setText("this is just a unit test message");
        message.setWhenCreated(new java.util.Date());
        
        message = messageDao.save(message);
        flush();

        message = messageDao.get(message.getId());

        assertEquals("Draft Test Message", message.getSubject());
        assertNotNull(message.getId());

        log.debug("removing message...");

        messageDao.remove(message.getId());
        flush();

        try {
            messageDao.get(message.getId());
            fail("Message found in database");
        } catch (DataAccessException dae) {
            log.debug("Expected exception: " + dae.getMessage());
            assertNotNull(dae);
        }
    }
    
    public void setMessageDao(MessageDao messageDao) {
        this.messageDao = messageDao;
    }

    public void setUserDao(UserDao userDao) {
        this.userDao = userDao;
    }
}
