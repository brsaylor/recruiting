/*
 * Copyright (C) 1998-2009, <a href="http://www.ssel.caltech.edu">SSEL</a>,
 * at Caltech, Pasadena, CA 91125. 
 *
 * MooreRecruiting is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 * USA.
 *
 */

package edu.caltech.ssel.recruiting.dao;

import edu.caltech.ssel.recruiting.model.Experiment;
import edu.caltech.ssel.recruiting.model.ParticipantRecord;
import edu.caltech.ssel.recruiting.model.Subject;
import java.util.List;
import org.appfuse.dao.BaseDaoTestCase;
import org.appfuse.dao.GenericDao;

/**
 *
 * @author michaelk
 */
public class ParticipantRecordDaoTest extends BaseDaoTestCase {
    
    private ParticipantRecordDao participantRecordDao;
    private GenericDao<Experiment, Long> experimentDao;
    private ExtendedUserDao extendedUserDao;
    
    /**
     * Creates a new instance of ParticipantRecordDaoTest
     */
    public ParticipantRecordDaoTest() {
    }

    public void testGetByExperiment() {
        log.debug("Entering testGetByExperiment...");
        
        List<ParticipantRecord> lst = participantRecordDao.getByExperiment(experimentDao.get(1L));
        assertNotNull(lst);
        assertTrue(lst.size() == 1);
    }
    
    public void testGetBySubject() {
        log.debug("Entering testGetBySubject...");

        List<ParticipantRecord> lst = participantRecordDao.getBySubject((Subject)extendedUserDao.get(5L));
        assertNotNull(lst);
        assertTrue(lst.size() == 1);
        assertTrue(lst.get(0).getSubject().getId() == 5L);
    }
    
    public void testGetByPlayerNum() {
        log.debug("Entering testGetByPlayerNum...");
        
        List<ParticipantRecord> lst = participantRecordDao.getByPlayerNum(new Integer(1));
        assertNotNull(lst);
        assertTrue(lst.size() == 1);
        assertTrue(lst.get(0).getPlayerNum() == 1);
    }

    public void testIsSignedUp() {
        log.debug("Entering testIsSignedUp...");
        
        Subject s = (Subject)extendedUserDao.get(5L);
        Experiment e = experimentDao.get(1L);
        
        assertTrue(participantRecordDao.isSignedUp(s, e));
        
        s = (Subject)extendedUserDao.get(4L);
        assertFalse(participantRecordDao.isSignedUp(s, e));
    }
    
    public void setParticipantRecordDao(ParticipantRecordDao participantRecordDao) {
        this.participantRecordDao = participantRecordDao;
    }

    public void setExperimentDao(GenericDao<Experiment, Long> experimentDao) {
        this.experimentDao = experimentDao;
    }

    public void setExtendedUserDao(ExtendedUserDao extendedUserDao) {
        this.extendedUserDao = extendedUserDao;
    }
    
}
