/*
 * Copyright (C) 1998-2009, <a href="http://www.ssel.caltech.edu">SSEL</a>,
 * at Caltech, Pasadena, CA 91125. 
 *
 * MooreRecruiting is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 * USA.
 *
 */

package edu.caltech.ssel.recruiting.dao;

import java.util.List;

import org.appfuse.dao.BaseDaoTestCase;
import edu.caltech.ssel.recruiting.model.Payment;
import org.springframework.dao.DataAccessException;

public class PaymentDaoTest extends BaseDaoTestCase {
    
    private PaymentDao paymentDao = null;
    
    public void testGetByTrackingId() throws Exception {
        List<Payment> payments = paymentDao.getByTrackingId("59b42ffbc9beb6725f0368a1d58bda");
        assertTrue(payments.size() > 0);
    }
    
    public void testAddAndRemovePayment() throws Exception {
        Payment payment = new Payment();
        payment.setStatus(Payment.Status.NEW);
        payment.setAccountInfo("wmyuan@caltech.edu");
        payment.setWhenCreated(new java.util.Date());

        payment = paymentDao.save(payment);
        flush();

        payment = paymentDao.get(payment.getId());

        assertEquals("wmyuan@caltech.edu", payment.getAccountInfo());
        assertNotNull(payment.getId());

        log.debug("removing payment...");

        paymentDao.remove(payment.getId());
        flush();

        try {
            paymentDao.get(payment.getId());
            fail("Payment found in database");
        } catch (DataAccessException dae) {
            log.debug("Expected exception: " + dae.getMessage());
            assertNotNull(dae);
        }
    }
    
    public void setPaymentDao(PaymentDao paymentDao) {
        this.paymentDao = paymentDao;
    }
}
