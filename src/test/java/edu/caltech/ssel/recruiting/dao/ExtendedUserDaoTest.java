/*
 * Copyright (C) 1998-2009, <a href="http://www.ssel.caltech.edu">SSEL</a>,
 * at Caltech, Pasadena, CA 91125. 
 *
 * MooreRecruiting is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 * USA.
 *
 */

package edu.caltech.ssel.recruiting.dao;

import edu.caltech.ssel.recruiting.model.Experimenter;
import edu.caltech.ssel.recruiting.model.Subject;
import java.util.List;
import org.appfuse.dao.BaseDaoTestCase;
import org.appfuse.model.User;

/**
 *
 * @author michaelk
 */
public class ExtendedUserDaoTest extends BaseDaoTestCase {

    private ExtendedUserDao extendedUserDao = null;
    
    /** Creates a new instance of ExtendedUserDaoTest */
    public ExtendedUserDaoTest() {
        
    }

    //Instances of User, Experimenter, and Subject can be found in the sample data
    public void testGetExperimenters() {
        List<Experimenter> lst = extendedUserDao.getExperimenters();
        assertNotNull(lst);
        assertSame(lst.size(), 2);
    }
    
    public void testGetSubjects() {
        List<Subject> lst = extendedUserDao.getSubjects();
        assertNotNull(lst);
        assertSame(lst.size(), 2);
    }
    
    public void testGetStandardUsers() {
        List<User> lst = extendedUserDao.getStandardUsers();
        assertNotNull(lst);
        assertSame(lst.size(), 1);
    }

    public void testGetEnabledUsers() {
        User u = new User();
        u.setAccountExpired(false);
        u.setAccountLocked(false);
        u.setEmail("email@email.com");
        u.setEnabled(false);
        u.setUsername("username");
        u.setPassword("password");
        u.setFirstName("first name");
        u.setLastName("last name");
        u = extendedUserDao.save(u);
        //Save a disabled user and make sure they are not on the list
        assertFalse(extendedUserDao.getEnabledUsers().contains(u));
    }
    
    public void testGetAdmins() {
        List<User> lst = extendedUserDao.getAdmins();
        assertNotNull(lst);
        assertSame(lst.size(), 2);
    }

    
    public void setExtendedUserDao(ExtendedUserDao extendedUserDao) {
        this.extendedUserDao = extendedUserDao;
    }
    
}
