/*
 * Copyright (C) 1998-2009, <a href="http://www.ssel.caltech.edu">SSEL</a>,
 * at Caltech, Pasadena, CA 91125. 
 *
 * MooreRecruiting is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 * USA.
 *
 */

package edu.caltech.ssel.recruiting.dao;

import java.util.List;

import org.appfuse.dao.BaseDaoTestCase;
import edu.caltech.ssel.recruiting.model.UserMessage;
import edu.caltech.ssel.recruiting.model.Message;
import org.springframework.dao.DataAccessException;
import org.appfuse.dao.UserDao;
import org.appfuse.model.User;

public class UserMessageDaoTest extends BaseDaoTestCase {
    
    private UserMessageDao userMessageDao = null;
    private UserDao userDao = null;
    private MessageDao messageDao = null;
    
    public void testFindInboxByRecipient() throws Exception {
        User user = userDao.get(3L);
        List<UserMessage> userMessages = userMessageDao.findInboxByRecipient(user);
        assertTrue(userMessages.size() > 0);
    }
    
    public void testAddAndRemoveUserMessage() throws Exception {
        User user = userDao.get(1L);
        Message message = messageDao.get(1L);
        UserMessage userMessage = new UserMessage();
        userMessage.setRecipient(user);
        userMessage.setMessage(message);
        userMessage.setStatus(UserMessage.Status.NEW);

        userMessage = userMessageDao.save(userMessage);
        flush();

        userMessage = userMessageDao.get(userMessage.getId());

        assertNotNull(userMessage.getId());

        log.debug("removing userMessage...");

        userMessageDao.remove(userMessage.getId());
        flush();

        try {
            userMessageDao.get(userMessage.getId());
            fail("UserMessage found in database");
        } catch (DataAccessException dae) {
            log.debug("Expected exception: " + dae.getMessage());
            assertNotNull(dae);
        }
    }
    
    public void setUserMessageDao(UserMessageDao userMessageDao) {
        this.userMessageDao = userMessageDao;
    }

    public void setUserDao(UserDao userDao) {
        this.userDao = userDao;
    }

    public void setMessageDao(MessageDao messageDao) {
        this.messageDao = messageDao;
    }
}
