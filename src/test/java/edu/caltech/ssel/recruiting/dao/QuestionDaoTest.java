/*
 * Copyright (C) 1998-2009, <a href="http://www.ssel.caltech.edu">SSEL</a>,
 * at Caltech, Pasadena, CA 91125. 
 *
 * MooreRecruiting is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 * USA.
 *
 */

package edu.caltech.ssel.recruiting.dao;

import edu.caltech.ssel.recruiting.model.Question;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.appfuse.dao.BaseDaoTestCase;
import org.appfuse.dao.GenericDao;
import org.springframework.dao.DataAccessException;

/**
 *
 * @author michaelk
 */
public class QuestionDaoTest extends BaseDaoTestCase {
    
    private GenericDao<Question, Long> dao = null;

    /** Creates a new instance of QuestionDaoTest */
    public QuestionDaoTest() {
    }
    
    public void testCRUD() throws Exception {
        log.debug("Entering testCRUD...");
        
        log.debug("Testing create...");
        Question q = new Question();
        q.setLabel("Test question");
        q.setQuestion("Do you love me?");
        q.setStyle(Question.QStyle.DROP_LIST);
        List<String> lst = new ArrayList();
        lst.add("Yes");
        lst.add("No");
        lst.add("...Now that I can dance");
        q.setOptionList(lst);
        
        Long id = dao.save(q).getId();
        
        //log.debug("The original question is: " + q.toString() + "\n ...with id = " + id);
        //log.debug("The original id = " + id);
        
        log.debug("Testing retrieve...");
        flush(); //Prevent it from just being retrieved locally
        q = dao.get(id);
        //log.debug("The retrieved question has properties: " + q.toString());
        //log.debug("The options are: ");
        /*for(Iterator i = q.getOptions().iterator(); i.hasNext(); ) {
            log.debug(i.next());
        }*/
        assertNotNull(q.getId());
        assertEquals("Test question", q.getLabel());
        List<String> lst2 = q.getOptionList();
        for(Iterator i = lst2.iterator(), j = lst.iterator(); i.hasNext() && j.hasNext(); ) {
            assertEquals(i.next(), j.next());
        }
        
        log.debug("Testing update...");
        Question q2 = dao.get(dao.getAll().get(0).getId());
        q2.setLabel("New Name");
        dao.save(q2);
        
        log.debug("Testing delete...");
        dao.remove(q.getId());
        flush();  //Prevent it from just being retrieved locally
        
        try{
            dao.get(q.getId());
            fail("Experiment Type Found in Database");
        } catch (DataAccessException dae) {
            log.debug("Expected exception: " + dae.getMessage());
            assertNotNull(dae);
        }
    }
    
    public void setQuestionDao(GenericDao<Question, Long> questionDao) {
        this.dao = questionDao;
    }
}
