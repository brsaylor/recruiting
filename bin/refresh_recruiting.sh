#!/bin/bash

export CATALINA_HOME=/home/dknoern/apache-tomcat-5.5.23
#export JAVA_OPTS="-Djavax.net.debug=ssl,handshake,data,trustmanager"

LOGFILE="/home/dknoern/logfiles/casselweb3__`date +%Y%m%d%H%M%S`.out"
JDBC_PASSWORD=
PAYGATE_URL="https://sealinux02.us.logicalis.com/paygate"
MESSAGE=

function usage() {
cat << ! >&2
This script will completely refresh the application from source code to database and redeploy recruiting to ${CATALINA_HOME}/webapps

Usage: $ME [-m "notification message"] [-h]
        -m:   [optional] a message to include with the refresh notification email
        -h:   display help
!
}

###############################################
# Get the arguments
while getopts m:h VALUE
do
        case $VALUE in

        m)
                MESSAGE=$OPTARG
                ;;
        h)
                usage
                exit 0
                ;;
        *)
                echo "$ME: unrecognized parameter [-$OPTARG]" >&2
                usage
                exit 1
                ;;
        esac
done

echo "mailing developers."
MAILFILE=mailfile.txt.$$
echo "Subject: Refreshing sealinux02: CASSELWEB3" > ${MAILFILE}
echo "To: ssel_devs@caltech.edu" >> ${MAILFILE}
echo >> ${MAILFILE}
if [ -n "$MESSAGE" ]
then
    echo "Deployment Message:" >> ${MAILFILE}
    echo $MESSAGE >> ${MAILFILE}
    echo >> ${MAILFILE}
fi
/usr/lib/sendmail -f $USER Patrick.ONeil@us.logicalis.com < ${MAILFILE}
rm ${MAILFILE}

echo "stopping tomcat."
$CATALINA_HOME/bin/shutdown.sh

echo "clean tomcat log dir."
rm -f ${CATALINA_HOME}/logs/*

echo "removing old deployment."
rm -fr recruiting

echo -e "Do you want to drop the recruiting database? [y/n]: \c "; read drop
[ "$drop" = "y" ] &&
mysql -u root <<EOF 2>&1
  drop database recruiting;
EOF

echo "exporting recruiting TRUNK from source repository."
svn export https://david.ssel.caltech.edu/svn/casselWeb2/recruiting/trunk recruiting > /dev/null

cd recruiting

# change jdbc.password
cat pom.xml| sed "s#ssel123#${JDBC_PASSWORD}#g" > pom.xml.mod
mv pom.xml.mod pom.xml

# change PMTGWY.properties URLs
cat src/main/resources/PMTGWY.properties| sed "s#http://localhost:8080#${PAYGATE_URL}#g" > src/main/resources/PMTGWY.properties.mod
mv src/main/resources/PMTGWY.properties.mod src/main/resources/PMTGWY.properties

if [ "$drop" = "y" ]
then
  echo "running mvn package. sending output to ${LOGFILE}"
  echo "please wait..."
  mvn package >> ${LOGFILE} 2>&1
else
  echo "running mvn -Dmaven.test.skip=true package. sending output to ${LOGFILE}"
  echo "please wait..."
  mvn -Dmaven.test.skip=true package >> ${LOGFILE} 2>&1
fi

cd ..
echo "archiving old recruiting instance to archived_webapps/recruiting_`date +%Y%m%d%H%M%S`"
mv -f ${CATALINA_HOME}/webapps/recruiting archived_webapps/recruiting_`date +%Y%m%d%H%M%S`
cp -r recruiting/target/recruiting-1.0-SNAPSHOT ${CATALINA_HOME}/webapps/recruiting

echo "starting tomcat.  logging goes to ${CATALINA_HOME}/logs/catalina.out"
$CATALINA_HOME/bin/startup.sh

echo "done."
