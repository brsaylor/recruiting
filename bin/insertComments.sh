#!/bin/bash
#
# simple script to insert comments into
# java files found below the current directory
#

function usage() {
cat << ! >&2
Simple script to insert comments into java files found below the current directory

Usage: $ME [-h]
        -h:   display help
!
}

###############################################
# Get the arguments
while getopts t:h VALUE
do
        case $VALUE in

       t)
               TEST_ARG=$OPTARG
               echo "test arg"
               exit 0
               ;;
        h)
                usage
                exit 0
                ;;
        *)
                echo "$ME: unrecognized parameter [-$OPTARG]" >&2
                usage
                exit 1
                ;;
        esac
done

for file in `find . -name \*.java`
do
  echo "adding comments to $file"
  cat - $file > tempfile <<EOF
/**
 *   `basename $file`
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
EOF
  mv -f tempfile $file
done
